#include "baloon.h"
#include <iostream>
#include <string>
#include <algorithm>
#include <boost/regex.hpp>
#include <boost/algorithm/string.hpp>
#include <cryptographer/evp.h>
#include <acommon/acode.h>
#include <Services/TaskMan/taskman_auto.h>
#include <Services/TaskMan/tm.h>
#include <reporter_auto.h>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <Paygate/paygate_auto.h>
#include "spd.h"

namespace BALOON {

using namespace std;
using namespace PAYGATE;
using namespace Effi;

#define BALOON_AGENT_INVOICE_CLASS "AgentInvoice"



Int GetCurrentAgent(Agent &agent, Value &current_user) {
	current_user = AUTHORIZER::AUTHORIZERUsers::GetCurrent();
	Int64 userid = current_user["TokenID"].As<Int64>();
	Selector sel;
	sel << agent;
	sel.Where (agent->userTokenID == userid);
	DataSet ds = sel.Execute(rdb_);
	ds.Fetch();
	Int agentid = agent.id;
	return agentid;
}

Int GetCurrentAgentID() {
	Agent agent;
	Value current_user;
	return GetCurrentAgent(agent, current_user);
}

Int CheckAndGetCurrentAgentID(const Int check_agentid=Int()) {
	Int agentid = GetCurrentAgentID();
	if (!Defined(agentid)) throw Exception(Message("Not an agent account. ").What());
	if (Defined(check_agentid) && check_agentid != agentid) {
		throw Exception(Message("Agent has no access to given document. "));
	}
	return agentid;
}


string createSpdTransactionByCode(const Str card_code, const Decimal amount, const string currency, const string comment) {
    SpdTransaction transaction;
    transaction.eid = to_upper(Effi::GenerateGUID());
    transaction.currency = currency;
    transaction.summ = amount;
    transaction.comment = comment;
    transaction.date = TIME_NOW /* + get_utc_offset() */;

    SpdClient client;
    client.identifier = card_code.get_value_or("");
    client.transactions.push_back(transaction);

    SendSpdRequest(client);
    return transaction.eid;
}

SpdResponse spdFillup(const Str code, const Decimal amount,  const string comment) {
	SpdTransaction transaction;
	transaction.eid = to_upper(Effi::GenerateGUID());
	transaction.currency = "RUB";
	transaction.summ = amount;
	transaction.comment = comment;
	SpdClient client;
	client.identifier = code.get_value_or("");
	client.transactions.push_back(transaction);
	return SendSpdRequest(client);
}

SpdResponse spdSaleIdentifier(const Str code, const Str other_card_code, const Time valid_from, const Time valid_to, const Str permanent_rulename, const Str categoryname, const Str ratename, const Decimal cost, const Decimal amount,  const string comment) {
	SpdIdentifier identifier;
	identifier.is_new		  = true;
	identifier.code		      = code.get_value_or("");
	identifier.valid_from	  = valid_from;
	identifier.valid_to	      = valid_to;
	identifier.permanent_rule = permanent_rulename.get_value_or("");
	identifier.category	      = categoryname.get_value_or("");
	identifier.tariff		  = ratename.get_value_or("");
	identifier.cost           = cost;

	SpdClient client(other_card_code.get_value_or("").empty());
	if (!other_card_code.get_value_or("").empty()) client.identifier = other_card_code.get_value_or("");
	client.identifiers.push_back(identifier);

    if (amount < cost) throw Exception(Message("Сумма пополнения не может быть меньше стоимости карты. ").What());
    if ((amount-cost) > 0) {
		SpdTransaction transaction;
		transaction.eid = to_upper(Effi::GenerateGUID());
		transaction.currency = "RUB";
		transaction.summ = amount - cost;
		transaction.comment = comment;
		client.transactions.push_back(transaction);
	}
	return SendSpdRequest(client);
}

SpdResponse spdSalePackage(const Str code, const Str service_rulename, const string comment) {
	SpdPackage package;
	package.identifier = code.get_value_or("");
	package.rule_service = service_rulename.get_value_or("");
	package.comment = comment;
	SpdClient client;
	client.identifier = code.get_value_or("");
	client.packages.push_back(package);
	return SendSpdRequest(client);
}


Double getAgentCommission(const Int agentid) {
	Agent agent(agentid);
	agent.Select(rdb_);
	if (Defined(agent.commission)) return agent.commission;
	BaloonProvider provider;
	provider.Select(rdb_);
	if (Defined(provider.agent_commission)) return provider.agent_commission;
	return 0;
}

const Value BALOONAgent::Register_API(const Str name, const Str phone, const Str email, const Str password, const Str password2) {
	if (!(Defined (name) && Defined(email) && Defined (password) && Defined (password2))) {
		throw Exception (Message ("All fields are mandatory. ").What());
	}
	if (*password != *password2) throw Exception (Message ("Passwords are not equal. ").What());
	if (loginExists (rdb_, email)) throw Exception (Message ("Given login already registered. ").What());

	Agent agent;
	agent.name = name;
	agent.phone = phone;
	agent.email = email;
    agent.is_default = BOOL_FALSE;
		
	Value groups;
	groups[0] = static_cast<int64_t>(200);
	Value res = AUTHORIZER::AUTHORIZERUsers::Add(Int64(), name, email, password, password2, Str("enabled"), groups);
	agent.userTokenID = res["TokenID"].As<int64_t>();
	Int agentid = agent.Insert(rdb_);
	AddMessage(Message() << Message("Agent ").What() << " " << agentid << " added. ");
	return Value();	
}

void UncheckOtherAgentsAsDefault(Connection* rdb, const Int default_agentid) {
    Agent agent;
    Updater upd(agent);
    upd << agent->is_default(BOOL_FALSE);
    upd.Where(agent->id != default_agentid);
    upd.Execute(rdb);
}

const Value BALOONAgent::Update(const Int id, const optional<Str> name, const optional<Str> phone, const optional<Str> email, const optional<Str> comments, const optional<Double> commission, const optional<Str> is_default) {
    try {
        Agent aAgent;
        aAgent.id=id;
        aAgent.Select(rdb_);
        if(Defined(name)) aAgent.name=*name;
        if(Defined(phone)) aAgent.phone=*phone;
        if(Defined(email)) aAgent.email=*email;
        if(Defined(comments)) aAgent.comments=*comments;
        if(Defined(commission)) aAgent.commission=*commission;
        if(Defined(is_default)) {
            aAgent.is_default=*is_default;
            if(*is_default == BOOL_TRUE) {
                UncheckOtherAgentsAsDefault(rdb_, id);
            }
        }
        aAgent.Update(rdb_);        
        AddMessage(Message()<<Message("Agent").What()<<" "<<aAgent.id<<" updated. ");
        return Value();
    }
    catch (Exception& exc) {
        Exception e;
        if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
            e << Message("Duplicate entry. ").What();
        }
        else {
            e << Message(exc.what()).What();
        }
        throw e;
    }
}

const Value BALOONAgent::GetCurrent() {
	Value current_user;
	Agent agent;
	Int agentid = GetCurrentAgent(agent, current_user);

	Value res;
	res["id"] = agent.id;
	res["userTokenID"] = agent.userTokenID;
	res["TokenName"] = current_user["TokenName"];
	res["name"] = agent.name;
	res["phone"] = agent.phone;
	res["email"] = agent.email;
	res["comments"] = agent.comments;
	res["commission"] = agent.commission;
	return res;
}

const Value BALOONAgentOrder::AgentOrderListGet(const optional<Int> agentid, const optional<ADate> first_day, const optional<ADate> last_day) {
	Data::DataList lr;
	lr.AddColumn("id", Data::INTEGER);
	lr.AddColumn("agentid", Data::INTEGER);
	lr.AddColumn("agentname", Data::STRING);
	lr.AddColumn("code", Data::STRING);
	lr.AddColumn("amount", Data::DECIMAL);
	lr.AddColumn("commission", Data::DECIMAL);
	lr.AddColumn("filed_at", Data::DATETIME);
	lr.AddColumn("comment", Data::STRING);
	lr.AddColumn("status", Data::STRING, AGENT_ORDER_STATUSValues());
	lr.AddColumn("AgentOrderType", Data::STRING, AgentOrder_TYPEValues());
	AgentOrder order;
	Agent aagent;
	lr.Bind(order.id, "id");
	lr.Bind(order.agentid, "agentid");
	lr.Bind(aagent.name, "agentname");
	lr.Bind(order.code, "code");
	lr.Bind(order.amount, "amount");
	lr.Bind(order.commission, "commission");
	lr.Bind(order.filed_at, "filed_at");
	lr.Bind(order.comment, "comment");
	lr.Bind(order.status, "status");
	lr.Bind(order.AgentOrderType, "AgentOrderType");
	Selector sel;
	sel << order << aagent;
	sel.Join(aagent).On(order->agentid==aagent->id);
	sel.Where(
		(Defined(agentid) && Defined(*agentid) ? order->agentid==*agentid : Expression()) &&
		(Defined(first_day) ? order->filed_at >= *first_day : Expression()) &&
		(Defined(last_day) ? order->filed_at < (*last_day + boost::gregorian::days(1)) : Expression())
	);
	DataSet data=sel.Execute(rdb_);
	while(data.Fetch()) lr.AddRow();
	Value res=lr;
	return res;
}

void checkIdentifierForReplenishment(Connection *rdb, const Str code) {
	SpdClient client;
    client.identifier = convertPrintCodeToSpd(code.get_value_or(""));
	SpdResponse response = SendSpdRequest(client);
	if (response.clients.size() == 0 || response.clients[0].identifiers.size() == 0) {
		throw Exception(Message("Card code not found. ").What());
	}
	SpdIdentifier aIdentifier = response.clients[0].identifiers[0];
	IdentifierRule rule;
	rule.name = aIdentifier.permanent_rule;
	rule.Select(rdb);
	if (rule.replenish_enabled != ENABLED) {
		throw Exception(Message("Card can not be replenished. ").What());
	}
}

const Value BALOONAgentOrder::Add(const Int agentid, const Str code, const Decimal amount, const Str comment) {
	Exception e((Message("Cannot add ") << Message("Refill Order").What() << ". ").What());
	bool error=false;
	error |= Defined(agentid)  && !__CheckIDExists(Agent()->id, *agentid, "Agent", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		checkIdentifierForReplenishment(rdb_, code);
		spdFillup(convertPrintCodeToSpd(code.get_value_or("")), amount, "Пополнение счёта агентом");

		AgentOrder aAgentOrder;
		aAgentOrder.agentid=agentid;
		aAgentOrder.code=code;
		aAgentOrder.amount=amount;
		aAgentOrder.commission = amount * getAgentCommission(agentid) / 100;
		aAgentOrder.filed_at=TIME_NOW;
		aAgentOrder.comment=comment;
		aAgentOrder.status = AGENT_ORDER_STATUS_OFFLINE;
		aAgentOrder.AgentOrderType="AgentOrder";
		Int sk=aAgentOrder.Insert(rdb_);
		Value res;
		res["id"]=sk;
		AddMessage(Message()<<Message("Refill Order").What()<<" "<<sk<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONAgentOrder::Update(const Int id, const optional<Str> comment) {
	Exception e((Message("Cannot update ") << Message("Refill Order").What() << ". ").What());
	bool error=false;
	if(error) {
		throw(e);
	}
	try {
		AgentOrder aAgentOrder;
		aAgentOrder.id=id;
		aAgentOrder.Select(rdb_);
		if(Defined(comment)) aAgentOrder.comment=*comment;
		aAgentOrder.Update(rdb_);
		AddMessage(Message()<<Message("Refill Order").What()<<" "<<aAgentOrder.id<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}


const Value BALOONIdentifierAgentOrder::Add(const Int agentid, const Str code, const Decimal amount, const Str comment, const Time valid_from, const Time valid_to, const Str permanent_rulename, const Str categoryname, const Str ratename, const Str other_card_code) {
	Exception e((Message("Cannot add ") << Message("Identifier Order").What() << ". ").What());
	bool error=false;
	error |= Defined(agentid)  && !__CheckIDExists(Agent()->id, *agentid, "Agent", e, rdb_);
	error |= Defined(permanent_rulename)  && !__CheckIDExists(IdentifierRule()->name, *permanent_rulename, "Правило пользования", e, rdb_);
	error |= Defined(categoryname)  && !__CheckIDExists(IdentifierCategory()->name, *categoryname, "Категория счета", e, rdb_);
	error |= Defined(ratename)  && !__CheckIDExists(IdentifierRate()->name, *ratename, "Тариф", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		BaloonProvider provider;
		provider.Select(rdb_);
		spdSaleIdentifier(convertPrintCodeToSpd(code.get_value_or("")), other_card_code, valid_from, valid_to, permanent_rulename, categoryname, ratename, provider.ident_sale_price, amount, "Продажа карты агентом");

		AgentOrder aAgentOrder;
		aAgentOrder.agentid=agentid;
		aAgentOrder.code=code;
		aAgentOrder.amount=amount;
		aAgentOrder.commission = amount * getAgentCommission(agentid) / 100;
		aAgentOrder.filed_at=TIME_NOW;
		aAgentOrder.comment=comment;
		aAgentOrder.status = AGENT_ORDER_STATUS_OFFLINE;
		aAgentOrder.AgentOrderType="AgentOrder";
		Int sk=aAgentOrder.Insert(rdb_);

		IdentifierAgentOrder ident_order;
		ident_order.id = sk;
		ident_order.valid_from = valid_from;
		ident_order.valid_to = valid_to;
		ident_order.permanent_rulename = permanent_rulename;
		ident_order.categoryname = categoryname;
		ident_order.ratename = ratename;
		ident_order.other_card_code = other_card_code;
		ident_order.Insert(rdb_);

		Value res;
		res["id"]=sk;
		AddMessage(Message() << "New card " << code << " sold. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONIdentifierAgentOrder::Update(const Int id, const optional<Str> comment) {
	Exception e((Message("Cannot update ") << Message("Identifier Order").What() << ". ").What());
	bool error=false;
	if(error) {
		throw(e);
	}
	try {
		AgentOrder aAgentOrder;
		aAgentOrder.id=id;
		aAgentOrder.Select(rdb_);
		if(Defined(comment)) aAgentOrder.comment=*comment;
		aAgentOrder.Update(rdb_);
		AddMessage(Message()<<Message("Identifier Order").What()<<" "<<aAgentOrder.id<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}


const Value BALOONPackageAgentOrder::Add(const Int agentid, const Str code, const Str service_rulename, const Str comment) {
	Exception e((Message("Cannot add ") << Message("Package Order").What() << ". ").What());
	bool error=false;
	error |= Defined(agentid)  && !__CheckIDExists(Agent()->id, *agentid, "Agent", e, rdb_);
	error |= Defined(service_rulename)  && !__CheckIDExists(SpdServiceRule()->name, *service_rulename, "Правило оформления", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		checkIdentifierForReplenishment(rdb_, code);
		SpdServiceRule service_rule;
		service_rule.name = service_rulename;
		service_rule.Select(rdb_);
		if (!service_rule.package_cost.IsNull()) {
			spdFillup(convertPrintCodeToSpd(code.get_value_or("")), service_rule.package_cost, "Пополнение счёта агентом для приобретения пакета");
		}
		spdSalePackage(convertPrintCodeToSpd(code.get_value_or("")), service_rulename, "Приобретение через агента");

		AgentOrder aAgentOrder;
		aAgentOrder.agentid=agentid;
		aAgentOrder.code=code;
		aAgentOrder.amount = service_rule.package_cost;
		aAgentOrder.commission = service_rule.package_cost * getAgentCommission(agentid) / 100;
		aAgentOrder.filed_at=TIME_NOW;
		aAgentOrder.comment=comment;
		aAgentOrder.status = AGENT_ORDER_STATUS_OFFLINE;
		aAgentOrder.AgentOrderType="PackageAgentOrder";
		Int sk=aAgentOrder.Insert(rdb_);
		PackageAgentOrder aPackageAgentOrder;
		aPackageAgentOrder.service_rulename=service_rulename;
		aPackageAgentOrder.id=sk;
		aPackageAgentOrder.Insert(rdb_);
		Value res;
		res["id"]=sk;
		AddMessage(Message()<<Message("Package Order").What()<<" "<<sk<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONPackageAgentOrder::Update(const Int id, const optional<Str> comment) {
	Exception e((Message("Cannot update ") << Message("Package Order").What() << ". ").What());
	bool error=false;
	if(error) {
		throw(e);
	}
	try {
		AgentOrder aAgentOrder;
		aAgentOrder.id=id;
		aAgentOrder.Select(rdb_);
		if(Defined(comment)) aAgentOrder.comment=*comment;
		aAgentOrder.Update(rdb_);

		AddMessage(Message()<<Message("Package Order").What()<<" "<<id<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}



const Value BALOONAgentOrder::CurrentAgentOrderListGet() {
	Int agentid = CheckAndGetCurrentAgentID();
	Data::DataList lr;
	lr.AddColumn("id", Data::INTEGER);
	lr.AddColumn("agentid", Data::INTEGER);
	lr.AddColumn("agentname", Data::STRING);
	lr.AddColumn("code", Data::STRING);
	lr.AddColumn("amount", Data::DECIMAL);
	lr.AddColumn("commission", Data::DECIMAL);
	lr.AddColumn("filed_at", Data::DATETIME);
	lr.AddColumn("comment", Data::STRING);
	lr.AddColumn("status", Data::STRING, AGENT_ORDER_STATUSValues());
	lr.AddColumn("AgentOrderType", Data::STRING, AgentOrder_TYPEValues());
	AgentOrder aAgentOrder;
	Agent aagent;
	lr.Bind(aAgentOrder.id, "id");
	lr.Bind(aAgentOrder.agentid, "agentid");
	lr.Bind(aagent.name, "agentname");
	lr.Bind(aAgentOrder.code, "code");
	lr.Bind(aAgentOrder.amount, "amount");
	lr.Bind(aAgentOrder.commission, "commission");
	lr.Bind(aAgentOrder.filed_at, "filed_at");
	lr.Bind(aAgentOrder.comment, "comment");
	lr.Bind(aAgentOrder.status, "status");
	lr.Bind(aAgentOrder.AgentOrderType, "AgentOrderType");
	Selector sel;
	sel << aAgentOrder << aagent;
	sel.Join(aagent).On(aAgentOrder->agentid==aagent->id);
	sel.Where(aAgentOrder->agentid==agentid);
	DataSet data=sel.Execute(rdb_);
	while(data.Fetch()) lr.AddRow();
	Value res=lr;
	return res;
}

const Value BALOONAgentOrder::CurrentAgentOrderListGet_API(const optional<Int> id) {
	Int agentid = CheckAndGetCurrentAgentID();
	Data::DataList lr;
	lr.AddColumn("id", Data::INTEGER);
	lr.AddColumn("agentid", Data::INTEGER);
	lr.AddColumn("agentname", Data::STRING);
	lr.AddColumn("code", Data::STRING);
	lr.AddColumn("amount", Data::DECIMAL);
	lr.AddColumn("commission", Data::DECIMAL);
	lr.AddColumn("filed_at", Data::DATETIME);
	lr.AddColumn("comment", Data::STRING);
	lr.AddColumn("status", Data::STRING, AGENT_ORDER_STATUSValues());
	lr.AddColumn("AgentOrderType", Data::STRING, AgentOrder_TYPEValues());
	AgentOrder aAgentOrder;
	Agent aagent;
	lr.Bind(aAgentOrder.id, "id");
	lr.Bind(aAgentOrder.agentid, "agentid");
	lr.Bind(aAgentOrder.code, "code");
	lr.Bind(aAgentOrder.amount, "amount");
	lr.Bind(aAgentOrder.commission, "commission");
	lr.Bind(aAgentOrder.filed_at, "filed_at");
	lr.Bind(aAgentOrder.comment, "comment");
	lr.Bind(aAgentOrder.status, "status");
	lr.Bind(aAgentOrder.AgentOrderType, "AgentOrderType");
	Selector sel;
	sel << aAgentOrder << aagent;
	sel.Join(aagent).On(aAgentOrder->agentid==aagent->id);
	sel.Where(
		aAgentOrder->agentid==agentid &&
		(Defined(id) ? aAgentOrder->id == *id : Expression())
	);
	DataSet data=sel.Execute(rdb_);
	
	Value res;
	int32_t i = 0;
	while(data.Fetch()) {
		Value row;
		row["id"] = aAgentOrder.id;
		row["agentid"] = aAgentOrder.agentid;
		row["code"] = aAgentOrder.code;
		row["amount"] = aAgentOrder.amount;
		row["commission"] = aAgentOrder.commission;
		row["filed_at"] = aAgentOrder.filed_at;
		row["comment"] = aAgentOrder.comment;
		row["status"] = aAgentOrder.status;
		row["AgentOrderType"] = aAgentOrder.AgentOrderType;
		res[i++] = row;
	}
	return res;
}

const Value BALOONAgentOrder::CurrentGet(const Int id) {
	Value res;
	AgentOrder aAgentOrder;
	aAgentOrder.id=id;
	if (!aAgentOrder.Select(rdb_)) {
		throw Exception(Message("No order with given ID found. ").What());
	}
	CheckAndGetCurrentAgentID(aAgentOrder.agentid);

	res["id"]=aAgentOrder.id;
	res["agentid"]=aAgentOrder.agentid;
	res["code"]=aAgentOrder.code;
	res["amount"]=aAgentOrder.amount;
	res["filed_at"]=aAgentOrder.filed_at;
	res["comment"]=aAgentOrder.comment;
	res["status"] = aAgentOrder.status;
	res["AgentOrderType"]=aAgentOrder.AgentOrderType;
	return res;
}

const Value BALOONAgentOrder::CurrentGet_API(const Int id) {
	Value res = BALOONAgentOrder::CurrentGet(id);
	return res;
}


Value getInvoicesByOrder(Connection *rdb, const Int orderid) {
	AgentInvoice invoice;
	Selector sel;
	sel << invoice->id;
	sel.Where(invoice->orderid == orderid);
	DataSet data = sel.Execute(rdb);
	
	Value res;
	int32_t i = 0;
	while (data.Fetch()) {
		res[i++] = invoice.id;
	}
	return res;
}

Str updateAgentOrderByInvoice(Connection *rdb, const Int id, const Str status, const Decimal amount, bool &updated_status) {
	AgentInvoice a_invoice(id);
	a_invoice.Select(rdb);
	AgentOrder order(a_invoice.orderid);
	order.Select(rdb);
	updated_status = false;

	if (status == PAYMENT_STATUS_PAYED) {
		if (order.status == AGENT_ORDER_STATUS_WAITING) {
			order.status = AGENT_ORDER_STATUS_PAYED;
			order.Update(rdb);
			updated_status = true;
		}
	}
	else if (status == PAYMENT_STATUS_REJECTED) {
		if (order.status != AGENT_ORDER_STATUS_REJECTED) {
			order.status = AGENT_ORDER_STATUS_REJECTED;
			order.Update(rdb);
			updated_status = true;
		}
	}
	else if (status == PAYMENT_STATUS_EXPIRED || status == PAYMENT_STATUS_CANCELED) {
		if (order.status == AGENT_ORDER_STATUS_WAITING) {
			order.status = AGENT_ORDER_STATUS_EXPIRED;
			order.Update(rdb);
			updated_status = true;
		}
	}

	return order.status;
}

const Value BALOONAgentOrder::CurrentSyncAndGet_API(const Int id) {
	AgentOrder order;
	order.id=id;
	if (!order.Select(rdb_)) {
		throw Exception(Message("No order with given ID found. ").What());
	}
	CheckAndGetCurrentAgentID(order.agentid);

	Value args = getInvoicesByOrder(rdb_, id);
	Value invoices = RootMethodInvoker("Paygate", "PayGate", "SyncInvoices") [
		PName<Value>("invoices") = args
	];

	Value res;
	res["orderid"] = id;
	res["orderstatus"] = order.status;
	res["error_message"] = Str();

	for(unsigned i=0; i<invoices.Size(); ++i) {
		Int invoiceid = invoices[i]["id"].As<Int>();
		Str status = invoices[i]["status"].As<Str>();
		Str error_message = invoices[i]["error_message"].As<Str>();
		Decimal amount = invoices[i]["amount"].As<Decimal>();
		bool updated_status = false;
		res["orderstatus"] = updateAgentOrderByInvoice(rdb_, invoiceid, status, amount, updated_status);

		if (status == PAYMENT_STATUS_PAYED && updated_status) {
			BALOONAgentInvoice::SyncSpd(invoiceid);
			break;
		}
	}
	
	return res;
}


const Value BALOONAgentOrder::CurrentAdd(const Str code, const Decimal amount, const Str comment) {
	Int agentid = CheckAndGetCurrentAgentID();
	return BALOONAgentOrder::Add(agentid, code, amount, comment);
}
const Value BALOONAgentOrder::CurrentAdd_API(const Str code, const Decimal amount, const Str comment) {
	Int agentid = CheckAndGetCurrentAgentID();
	return BALOONAgentOrder::Add(agentid, code, amount, comment);
}

const Value BALOONAgentOrder::OnlineCurrentAdd_API(const Str code, const Decimal amount, const Str comment, const Str success_url, const Str fail_url) {
	SpdClient client;
	client.identifier = convertPrintCodeToSpd(code.get_value_or(""));
	checkIdentifierForReplenishment(rdb_, code);

	Int agentid = CheckAndGetCurrentAgentID();
	AgentOrder order;
	order.agentid = agentid;
	order.code = code;
	order.amount = amount;
	order.commission = amount * getAgentCommission(agentid) / 100;
	order.filed_at = TIME_NOW;
	order.comment = comment;
	order.status = AGENT_ORDER_STATUS_WAITING;
	order.AgentOrderType = "AgentOrder";
	Int orderid = order.Insert(rdb_);

	BaloonProvider provider;
	provider.Select(rdb_);
	if (!Defined(provider.default_paygateid)) {
		throw Exception(Message("Unable to raise invoice: please setup the default payment gateway. ").What());
	}

	Value res = BALOONAgentInvoice::RaiseInvoice(provider.default_paygateid, orderid, amount, success_url, fail_url);
	res["agent_orderid"] = orderid;
	return res;
}


const Value BALOONAgentOrder::CurrentUpdate(const Int id, const optional<Str> comment) {
	Exception e((Message("Cannot update ") << Message("Refill Order").What() << ". ").What());
	bool error=false;
	if(error) {
		throw(e);
	}
	try {
		AgentOrder aAgentOrder;
		aAgentOrder.id=id;
		aAgentOrder.Select(rdb_);
		CheckAndGetCurrentAgentID(aAgentOrder.agentid);

		if(Defined(comment)) aAgentOrder.comment=*comment;
		aAgentOrder.Update(rdb_);
		AddMessage(Message()<<Message("Refill Order").What()<<" "<<aAgentOrder.id<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONIdentifierAgentOrder::CurrentGet(const Int id) {
	Value res;
	AgentOrder aAgentOrder;
	aAgentOrder.id=id;
	aAgentOrder.Select(rdb_);
	CheckAndGetCurrentAgentID(aAgentOrder.agentid);

	res["id"]=aAgentOrder.id;
	res["agentid"]=aAgentOrder.agentid;
	res["code"]=aAgentOrder.code;
	res["amount"]=aAgentOrder.amount;
	res["filed_at"]=aAgentOrder.filed_at;
	res["comment"]=aAgentOrder.comment;
	res["status"] = aAgentOrder.status;
	res["AgentOrderType"]=aAgentOrder.AgentOrderType;
	IdentifierAgentOrder ident_order;
	ident_order.id=id;
	ident_order.Select(rdb_);
	res["valid_from"]=ident_order.valid_from;
	res["valid_to"]=ident_order.valid_to;
	res["permanent_rulename"]=ident_order.permanent_rulename;
	res["categoryname"]=ident_order.categoryname;
	res["ratename"]=ident_order.ratename;
	res["other_card_code"]=ident_order.other_card_code;
	return res;
}

const Value BALOONIdentifierAgentOrder::CurrentAdd(const Str code, const Decimal amount, const Str comment, const Time valid_from, const Time valid_to, const Str permanent_rulename, const Str categoryname, const Str ratename, const Str other_card_code) {
	Int agentid = CheckAndGetCurrentAgentID();
	return BALOONIdentifierAgentOrder::Add(agentid, code, amount, comment, valid_from, valid_to, permanent_rulename, categoryname, ratename, other_card_code);
}
const Value BALOONIdentifierAgentOrder::CurrentAdd_API(const Str code, const Decimal amount, const Str comment, const Time valid_from_, const Time valid_to_, const Str permanent_rulename, const Str categoryname, const Str ratename, const Str other_card_code) {
	Int agentid = CheckAndGetCurrentAgentID();
	Time valid_from = (valid_from_.is_not_a_date_time() ? TIME_NOW : valid_from_);
	Time valid_to = (valid_to_.is_not_a_date_time() ? TIME_NOW + boost::gregorian::years(1) : valid_to_);
	return BALOONIdentifierAgentOrder::Add(agentid, code, amount, comment, valid_from, valid_to, permanent_rulename, categoryname, ratename, other_card_code);
}

const Value BALOONIdentifierAgentOrder::CurrentUpdate(const Int id, const optional<Str> comment) {
	Exception e((Message("Cannot update ") << Message("Identifier Order").What() << ". ").What());
	bool error=false;
	if(error) {
		throw(e);
	}
	try {
		AgentOrder aAgentOrder;
		aAgentOrder.id=id;
		aAgentOrder.Select(rdb_);
		CheckAndGetCurrentAgentID(aAgentOrder.agentid);
		if(Defined(comment)) aAgentOrder.comment=*comment;
		aAgentOrder.Update(rdb_);

		AddMessage(Message()<<Message("Identifier Order").What()<<" "<<id<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}


const Value BALOONPackageAgentOrder::CurrentGet(const Int id) {
	Value res;
	AgentOrder aAgentOrder;
	aAgentOrder.id=id;
	aAgentOrder.Select(rdb_);
	CheckAndGetCurrentAgentID(aAgentOrder.agentid);

	res["id"]=aAgentOrder.id;
	res["agentid"]=aAgentOrder.agentid;
	res["code"]=aAgentOrder.code;
	res["amount"]=aAgentOrder.amount;
	res["filed_at"]=aAgentOrder.filed_at;
	res["comment"]=aAgentOrder.comment;
	res["status"] = aAgentOrder.status;
	res["AgentOrderType"]=aAgentOrder.AgentOrderType;
	PackageAgentOrder aPackageAgentOrder;
	aPackageAgentOrder.id=id;
	aPackageAgentOrder.Select(rdb_);
	res["service_rulename"]=aPackageAgentOrder.service_rulename;
	return res;
}

const Value BALOONPackageAgentOrder::CurrentAdd(const Str code, const Str service_rulename, const Str comment) {
	Int agentid = CheckAndGetCurrentAgentID();
	return BALOONPackageAgentOrder::Add(agentid, code, service_rulename, comment);
}
const Value BALOONPackageAgentOrder::CurrentAdd_API(const Str code, const Str service_rulename, const Str comment) {
	Int agentid = CheckAndGetCurrentAgentID();
	return BALOONPackageAgentOrder::Add(agentid, code, service_rulename, comment);
}
const Value BALOONPackageAgentOrder::OnlineCurrentAdd_API(const Str code, const Str service_rulename, const Str comment, const Str success_url, const Str fail_url) {
	Exception e((Message("Cannot add ") << Message("Package Order").What() << ". ").What());
	bool error=false;
	error |= Defined(service_rulename)  && !__CheckIDExists(SpdServiceRule()->name, *service_rulename, "Правило оформления", e, rdb_);
	if (error) throw(e);
	checkIdentifierForReplenishment(rdb_, code);

	try {
		Int agentid = CheckAndGetCurrentAgentID();

		SpdClient client;
		client.identifier = convertPrintCodeToSpd(code.get_value_or(""));
		SendSpdRequest(client);

		SpdServiceRule service_rule;
		service_rule.name = service_rulename;
		service_rule.Select(rdb_);

		AgentOrder aAgentOrder;
		aAgentOrder.agentid = agentid;
		aAgentOrder.code = code;
		aAgentOrder.amount = service_rule.package_cost;
		aAgentOrder.commission = service_rule.package_cost * getAgentCommission(agentid) / 100;
		aAgentOrder.filed_at = TIME_NOW;
		aAgentOrder.comment = comment;
		aAgentOrder.status = AGENT_ORDER_STATUS_WAITING;
		aAgentOrder.AgentOrderType = "PackageAgentOrder";
		Int orderid = aAgentOrder.Insert(rdb_);
		PackageAgentOrder aPackageAgentOrder;
		aPackageAgentOrder.service_rulename = service_rulename;
		aPackageAgentOrder.id = orderid;
		aPackageAgentOrder.Insert(rdb_);

		BaloonProvider provider;
		provider.Select(rdb_);
		if (!Defined(provider.default_paygateid)) {
			throw Exception(Message("Unable to raise invoice: please setup the default payment gateway. ").What());
		}

		Value res = BALOONAgentInvoice::RaiseInvoice(provider.default_paygateid, orderid, service_rule.package_cost, success_url, fail_url);
		res["agent_orderid"] = orderid;
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONPackageAgentOrder::CurrentUpdate(const Int id, const optional<Str> comment) {
	Exception e((Message("Cannot update ") << Message("Package Order").What() << ". ").What());
	bool error=false;
	if(error) {
		throw(e);
	}
	try {
		AgentOrder aAgentOrder;
		aAgentOrder.id=id;
		aAgentOrder.Select(rdb_);
		CheckAndGetCurrentAgentID(aAgentOrder.agentid);
		if(Defined(comment)) aAgentOrder.comment=*comment;
		aAgentOrder.Update(rdb_);

		AddMessage(Message()<<Message("Package Order").What()<<" "<<id<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONAgentInvoice::SyncSpd(const Int id) {
	Invoice invoice;
	AgentInvoice a_invoice;
	AgentOrder order;
	PackageAgentOrder package_order;
	Selector sel;
	sel << invoice << a_invoice << order << package_order;
	sel.From(a_invoice).
		Join(invoice).On(invoice->id == a_invoice->id).
		Join(order).On(order->id == a_invoice->orderid).
		LeftJoin(package_order).On(package_order->id == order->id);
	sel.Where(a_invoice->id == id);
	DataSet data = sel.Execute(rdb_);
	if (!data.Fetch()) {
		throw Exception(Message("Given invoice not found. ").What());
	}

	if (!a_invoice.eid.get_value_or("").empty()) {
		throw Exception(Message("Invoice already synchronized. ").What());
	}
	
	if (order.AgentOrderType.get_value_or("") == "PackageAgentOrder") {
		SpdServiceRule service_rule;
		service_rule.name = package_order.service_rulename;
		service_rule.Select(rdb_);
		if (!service_rule.package_cost.IsNull()) {
			a_invoice.eid = createSpdTransactionByCode(convertPrintCodeToSpd(order.code.get_value_or("")), service_rule.package_cost, "RUB", "Пополнение счёта агентом для приобретения пакета");
			a_invoice.Update(rdb_);
		}
		spdSalePackage(convertPrintCodeToSpd(order.code.get_value_or("")), package_order.service_rulename, "Приобретение через агента");
		package_order.Update(rdb_);
	}
	else if (order.AgentOrderType.get_value_or("") == "AgentOrder") {
		a_invoice.eid = createSpdTransactionByCode(convertPrintCodeToSpd(order.code.get_value_or("")), invoice.amount, "RUB", "Пополнение через интернет через агента");
		a_invoice.Update(rdb_);
	}
	else {
		throw Exception(Message("Invalid AgentOrderType. ").What());
	}

	AddMessage((Message() << "Invoice " << invoice.barcode << " synchronized. "));

	Value res;
	res["eid"] = a_invoice.eid;
	res["orderid"] = order.id;
	res["orderstatus"] = order.status;
	return Value();
}

const Value BALOONAgentInvoice::StatusUpdated(const Int id, const Str barcode, const Str status, const Decimal amount) {
	bool updated_status = false;
	updateAgentOrderByInvoice(rdb_, id, status, amount, updated_status);

	if (status == PAYMENT_STATUS_PAYED && updated_status) {
		BALOONAgentInvoice::SyncSpd(id);
	}
	return Value();
}

string nextAgentInvoiceBarcode(Connection *rdb, const Int orderid) {
	AgentInvoice a_invoice;
	int32_t invoices_count = 0;
	Selector sel;
	sel << a_invoice->id.Count().Bind(invoices_count);
	sel.Where(a_invoice->orderid == orderid);
	DataSet data = sel.Execute(rdb);
	if (!data.Fetch()) {
		invoices_count = 0;
	}
	stringstream ss;
	ss << "BAI-" << setfill('0') << setw(6) << orderid << "-" << setw(2) << (invoices_count+1);
	return ss.str();
}

Locker raiseAgentInvoiceLocker_;
const Value BALOONAgentInvoice::RaiseInvoice(const Int paygateid, const Int orderid, const Decimal amount, const Str success_url, const Str fail_url) {
	SafeLock lock(raiseAgentInvoiceLocker_);
	Value res = RootMethodInvoker("Paygate", "Invoice", "RaiseInvoice") [
		PName<Int>("paygateid") = paygateid,
		PName<Str>("barcode") = nextAgentInvoiceBarcode(rdb_, orderid),
		PName<Decimal>("amount") = amount,
		PName<Str>("order_type") = BALOON_AGENT_INVOICE_CLASS,
		PName<Str>("success_url") = success_url,
		PName<Str>("fail_url") = fail_url
	];

	const Int invoiceid = res["INVOICEID@@"].As<Int>();
	AgentInvoice a_invoice;
	a_invoice.id = invoiceid;
	a_invoice.orderid = orderid;
	a_invoice.Insert(rdb_);

	return res;
}



const Value BALOONReports::CurrentMonth() {
	ADate today = TODAY_LOCAL,
		first_day = ADate(today.year(), today.month(), 1),
		last_day = today;
	Value res;
	res["first_day"] = first_day;
	res["last_day"] = last_day;
	return res;
}
const Value BALOONReports::PreviousMonth() {
	ADate today = TODAY_LOCAL,
		first_day = ADate(today.year(), today.month(), 1) - boost::gregorian::months(1),
		last_day = first_day + boost::gregorian::months(1) - boost::gregorian::days(1);
	Value res;
	res["first_day"] = first_day;
	res["last_day"] = last_day;
	return res;
}

class ReportRow {
public:
	Decimal total_amount;
	Decimal total_commission;
	int32_t orders_count;

	ReportRow() : total_amount(0), total_commission(0), orders_count(0) {}
	ReportRow(Decimal _total_amount, Decimal _total_commission, int32_t _orders_count=1) : total_amount(_total_amount), total_commission(_total_commission), orders_count(_orders_count) {}
};

const Value BALOONReports::AgentSalesReportListGet(const ADate first_day, const ADate last_day,  const Int agentid) {
	AgentOrder order;
	Decimal result_cost;
	
	ADate current_date;
	Decimal total_date_amount, date_commission, avarage_date_order_amount;
	Decimal total_amount=0, total_commission=0;
	int32_t orders_date_count, orders_count = 0;

	Data::DataList lr;
	lr.AddColumn("current_date", Data::DATETIME);
	lr.AddColumn("total_date_amount", Data::DECIMAL);
	lr.AddColumn("date_commission", Data::DECIMAL);
	lr.AddColumn("orders_date_count", Data::INTEGER);
	lr.AddColumn("avarage_date_order_amount", Data::DECIMAL);
	lr.Bind(current_date, "current_date");
	lr.Bind(total_date_amount, "total_date_amount");
	lr.Bind(date_commission, "date_commission");
	lr.Bind(orders_date_count, "orders_date_count");
	lr.Bind(avarage_date_order_amount, "avarage_date_order_amount");
	if (first_day.is_not_a_date() || last_day.is_not_a_date()) return lr;

	std::map<ADate, ReportRow> dates;
	Selector sel;
	sel << order->id << order->amount << order->commission << order->filed_at;
	sel.Where(
		(order->status == AGENT_ORDER_STATUS_PAYED || order->status == AGENT_ORDER_STATUS_OFFLINE) &&
		(order->filed_at >= first_day && order->filed_at < last_day + boost::gregorian::days(1)) &&
		(Defined(agentid) ? order->agentid == agentid : Expression())
	);
	DataSet data=sel.Execute(rdb_);
	while(data.Fetch()) {
		// result_cost = order.total_cost - (order.total_cost * order.discount / 100);
		ADate date = order.filed_at.date();
		if (dates.find(date) == dates.end()) dates[date] = ReportRow(order.amount, order.commission);
		else {
			dates[date].total_amount = dates[date].total_amount + order.amount;
			dates[date].total_commission = dates[date].total_commission + order.commission;
			dates[date].orders_count++;
		}
	}

	for (current_date=first_day; current_date<=last_day; current_date += boost::gregorian::days(1)) {
		total_date_amount = 0;
		date_commission = 0;
		orders_date_count = 0;
		if (dates.find(current_date) != dates.end()) {
			total_date_amount = dates[current_date].total_amount;
			date_commission = dates[current_date].total_commission;
			orders_date_count = dates[current_date].orders_count;
		}
		avarage_date_order_amount = total_date_amount / orders_date_count;
		total_amount = total_amount + total_date_amount;
		total_commission = total_commission + date_commission;
		orders_count += orders_date_count;
		lr.AddRow();
	}
	Value res;
	res["list"] = lr;
	res["total_amount"] = total_amount;
	res["total_commission"] = total_commission;
	res["orders_count"] = orders_count;
	res["avarage_order_amount"] = (orders_count>0 ? (total_amount / orders_count) : Decimal(0));
	return res;
}


const Value BALOONReports::CurrentAgentSalesReportListGet(const ADate first_day, const ADate last_day) {
	const Int agentid = CheckAndGetCurrentAgentID();
	return BALOONReports::AgentSalesReportListGet(first_day, last_day, agentid);
}



}
