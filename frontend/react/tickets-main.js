import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { format, formatDistance, formatRelative, subDays, addDays} from 'date-fns'
import { SelectPicker, TagPicker, InputPicker, DatePicker, Sidebar } from 'rsuite';
import { Container, Header, Content, Panel, PanelGroup } from 'rsuite';
import { Popover, Whisper, Icon, Toggle, Modal, Dropdown } from 'rsuite';
import { ControlLabel, Button, IconButton, ButtonGroup, ButtonToolbar  } from 'rsuite';
import { Input, InputGroup, InputNumber } from 'rsuite';
import { Grid, Row, Col } from 'rsuite';
 
class BuyTicketsMenu extends React.Component {

  constructor(props) {
    super(props);

    this.pay = this.pay.bind(this);

    this.next = this.next.bind(this);
    this.prev = this.prev.bind(this);

    this.close = this.close.bind(this);
    this.open = this.open.bind(this);

    this.min = 0;
    this.max = 50;
    this.step = 1;

    this.state = {
      show: false,
      amount: 0,
      htmlpage: this.renderpage(1)
    };

  }
  totalamount()
  {
    var amount = 0;
    tariffs.forEach(element => {
      amount = amount + element['count'] * element['price'];
    });
    return amount;
  }
  userscount()
  {
    var count = 0;
    tariffs.forEach(element => {
      count = count + element['count'];
    });
    return count;
  }
  reload() {
    this.setState( { amount: 0 } );
    this.setState( { htmlpage: this.renderpage(1) } );
  }
  close() {
    this.setState( { show: false } );
  }
  label_customers(usercount) {
    var text = "";
    if ( usercount == 0 )
      text = "Посетителей нет";
    else if ( usercount == 1 || ( ( (usercount % 10) == 1 ) && (usercount != 11) ) )
      text = usercount + " посетитель";
    else if ( usercount == 2 || usercount == 3 || usercount == 4 || ( ( (u4sercount % 10) == 2 ) && (usercount != 12) ) || ( ( (usercount % 10) == 3 ) && (usercount != 13) ) || ( ( (usercount % 10) == 4 ) && (usercount != 14) ) )
      text = usercount + " посетителя";
    else
      text = usercount + " посетителей";
    return text;
  }
  inner_customers(bbbb,usercount) {
    bbbb.innerText = this.label_customers(usercount);
  }
  open(event) {
    this.setState( { htmlpage: this.renderpage(1) } );
    this.setState( { show: true } );
  }
  next(event) {
    if ( this.userscount() > 0 ) { 
      this.setState( { htmlpage: this.renderpage(2) } );
    }
  }
  prev(event) {
    this.setState( { htmlpage: this.renderpage(1) } );
  }
  pay(event) {
    var email = document.getElementById("email");
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if( re.test(email.value) ) { 
      requestemail = email.value;
      protocol_placeany_fe( requestdate, requestemail );
    }
  }
  renderpage(page) {
    var _this = this;
    if (page == 1)
    { return (
          <Grid fluid>
            <Row> 
              <Col xs={8}>
                <ControlLabel><h6 style={{ marginBottom: 10 }}>Дата посещения:</h6></ControlLabel>
                <DatePicker isoWeek oneTap
                  format={ "DD.MM.YYYY" }
                  defaultValue={ requestdate }
                  size="lg"
                  placement="bottomStart"     
                  placeholder="Текущая дата"
                  style={ { width: 180 } } 
                  cleanable={ false }
                  disabledDate={ (date) => { 
			var decision = false;
			var ttt1 = parseInt(date.getDate());
			var ttt2 = parseInt(date.getMonth() + 1);
			var ttt3 = parseInt(date.getFullYear());
			holidays.forEach( ( item, i ) => {
				var uuu1 = parseInt(item['adate'].getDate());
				var uuu2 = parseInt(item['adate'].getMonth() + 1);
				var uuu3 = parseInt(item['adate'].getFullYear());
				if ( (ttt1 == uuu1) && (ttt2 == uuu2) && (ttt3 == uuu3) )
				{	decision = true;
				}
			});
			return decision;
                  }  }
                  onChange = { (date) => {
                        requestdate = date;
			protocol_datelistget_fe( requestdate, ( data ) => { 
				_this.reload(); 

				var usercount = parseInt(_this.userscount(),10);
                                var bbbb = document.getElementById('commit');

                                _this.inner_customers( bbbb, usercount );
			} );
                  } }
                  ranges={ [
                    {
                      label: 'Сегодня',
                      value: new Date(),
                      closeOverlay: true
                    },
                    {
                      label: 'Завтра',
                      value: addDays(new Date(), 1),
                      closeOverlay: true
                    }
                  ] }
                  locale={ {
                    sunday: 'Вс',
                    monday: 'Пн',
                    tuesday: 'Вт',
                    wednesday: 'Ср',
                    thursday: 'Чт',
                    friday: 'Пт',
                    saturday: 'Сб',
                    ok: 'OK',
                    today: 'Сегодня',
                    yesterday: 'Вчера',
                    tomorrow: 'Завтра',
                    hours: 'Часы',
                    minutes: 'Минуты',
                    seconds: 'Секунды'
                } } ></DatePicker>
              </Col>
              <Col xs={8}>
                <ControlLabel><h6 style={{ marginBottom: 10 }}>Выбор тарифа:</h6></ControlLabel>
                <Whisper
                  trigger="click"
                  placement="bottomStart"
                  speaker={ ( 
                    <Popover style={{ width: 400 }}> 
                      <ControlLabel><h5>Выберите количество билетов:</h5></ControlLabel>
                      <hr/>
                      <Grid fluid>
                      {tariffs.map((item, index) => (
                        <Row key={index} index={index} style={{ marginBottom: 10 }} className="show-grid">
                          <Col xs={16}><h6>Тариф «{ tariffs[index]['name'] }» :</h6></Col>
                          <Col xs={8}>
                            <InputGroup>  
                              <InputGroup.Button onClick={ function() {
                                  var aaaa = document.getElementById('a' + tariffs[index]['id']);
                            
                                  tariffs[index]['count'] = parseInt(tariffs[index]['count'],10) - parseInt(_this.step,10);
                                  if ( parseInt(tariffs[index]['count'],10) < parseInt(_this.min,10) )
                                  	tariffs[index]['count'] = parseInt(0,10);

                                  aaaa.value = parseInt(tariffs[index]['count'],10);

				  var usercount = parseInt(_this.userscount(),10);
                                  var bbbb = document.getElementById('commit');

                                  _this.inner_customers( bbbb, usercount );

                              } }>-</InputGroup.Button>
                              <Input
                                      id={ "a" + tariffs[index]['id'] }
                                      disabled={ true }
                                      className={ "custom-input-number" }
                                      value={ tariffs[index]['count'] }
                              />
                              <InputGroup.Button onClick={ function() {
                                  var aaaa = document.getElementById('a' + tariffs[index]['id']);
                            
                                  tariffs[index]['count'] = parseInt(tariffs[index]['count'],10) + parseInt(_this.step,10);
                                  if ( parseInt(tariffs[index]['count'],10) > parseInt(_this.max,10) )
                                  	tariffs[index]['count'] = parseInt(50,10);

                                  aaaa.value = parseInt(tariffs[index]['count'],10);

				  var usercount = parseInt(_this.userscount(),10);
                                  var bbbb = document.getElementById('commit');

                                  _this.inner_customers( bbbb, usercount );

                              } }>+</InputGroup.Button>
                            </InputGroup>
                          </Col>
                        </Row>
                      ))}
                      </Grid>                      
                    </Popover> 
                  ) } >
                  <Button id="commit" size="lg" 
                    onClick={ () => {
                        this.setState( { htmlpage: this.renderpage(1) } );
                    } } appearance="primary">{ _this.label_customers( parseInt( _this.userscount(), 10 ) ) }</Button>
                </Whisper>
              </Col>
              <Col xs={8} style={{ textAlign: "right" }}>
                <ControlLabel><h6 style={{ marginBottom: 10 }}>&nbsp;</h6></ControlLabel>
                <IconButton 
                  size="lg"
                  icon={<Icon icon="arrow-right" />} 
                  placement="right" 
                  appearance="primary"
                  onClick={ this.next }>Продолжить</IconButton>
              </Col>
            </Row>
          </Grid>
      );
    }
    else if ( page == 2 )
    {
      return (
          <div>
          <Grid fluid>
           {tariffs.map((item, index) => (
            <Row style={{ marginBottom: 10 }} className="show-grid">
              <Col xs={10}><h6>Тариф «{ tariffs[index]['name'] }» :</h6>
                <ControlLabel id={ '_b' + tariffs[index]['id'] }>( Цена: { tariffs[index]['price'] } руб )</ControlLabel>
              </Col>
              <Col xs={6}>
                <InputGroup>  
                  <InputGroup.Button onClick={ function() {
                    var aaaa = document.getElementById('a' + tariffs[index]['id']);
                            
                    tariffs[index]['count'] = parseInt(tariffs[index]['count'],10) - parseInt(_this.step,10);
                    if ( parseInt(tariffs[index]['count'],10) < parseInt(_this.min,10) )
	                    tariffs[index]['count'] = parseInt(0,10);

                    aaaa.value = parseInt(tariffs[index]['count'],10);
                    
                    var cccc = document.getElementById('_ctotal');
                    cccc.innerText = _this.totalamount() + " руб";

                    var bbbb = document.getElementById('_btotal');
                    bbbb.innerText = "Оплатить " + _this.totalamount() + " руб";

                    var cc = document.getElementById('c' + tariffs[index]['id']);
                    cc.innerText = (tariffs[index]['count'] * tariffs[index]['price']) + " руб";

                  } }>-</InputGroup.Button>
                  <Input
                    id={ "a" + tariffs[index]['id'] }
                    disabled={ true }
                    className={ 'custom-input-number' }
                    value={ tariffs[index]['count'] }
                  />
                  <InputGroup.Button onClick={ function() {
                    var aaaa = document.getElementById('a' + tariffs[index]['id']);
                            
                    tariffs[index]['count'] = parseInt(tariffs[index]['count'],10) + parseInt(_this.step,10);
                    if ( parseInt(tariffs[index]['count'],10) > parseInt(_this.max,10) )
        	            tariffs[index]['count'] = parseInt(_this.max,10);

                    aaaa.value = parseInt(tariffs[index]['count'],10);
 
                    var cccc = document.getElementById('_ctotal');
                    cccc.innerText = _this.totalamount() + " руб";

                    var bbbb = document.getElementById('_btotal');
                    bbbb.innerText = "Оплатить " + _this.totalamount() + " руб";

                    var cc = document.getElementById('c' + tariffs[index]['id']);
                    cc.innerText = (tariffs[index]['count'] * tariffs[index]['price']) + " руб";

                  } }>+</InputGroup.Button>
                </InputGroup>
              </Col>
              <Col xs={6} style={{ marginLeft: 38 }}>
                <ControlLabel><h6>Итого по тарифу:</h6></ControlLabel>
                <div id={ 'c' + tariffs[index]['id'] }>{ tariffs[index]['count'] * tariffs[index]['price'] } руб</div>
              </Col>
          </Row>
          ))}
          </Grid>
          <br/>
          <ControlLabel><h6 style={{ marginBottom: 10 }}>E-mail для получения билетов:</h6></ControlLabel>
          <InputGroup style={{ width: 320 }}>
            <InputGroup.Addon> @</InputGroup.Addon>
            <Input id="email"/>
          </InputGroup>
          <br/>
          <Grid fluid>
            <Row className="show-grid">
              <Col xs={12}>
                <ControlLabel><h6 style={{ marginBottom: 6 }}>Итого к оплате:</h6></ControlLabel>
                <div><h6 id='_ctotal'>{ _this.totalamount() } руб</h6></div>
              </Col>
              <Col xs={12} style={{ textAlign: "right" }}>
                <IconButton 
                  id='_btotal'
                  size="lg"
                  placement="center" 
                  style={ { width: 200 } }
                  appearance="primary"
                onClick={this.pay}>Оплатить { _this.totalamount() } руб</IconButton>
              </Col>
            </Row>
          </Grid>
          <br />
          <Grid fluid>
            <Row>	
                <Col xs={12}>
                    <IconButton 
                        size="lg"
                        icon={<Icon icon="arrow-left" />} 
                        placement="left" 
                        appearance="primary"
                        onClick={ this.prev }>Вернуться</IconButton>
                </Col>
            </Row>
          </Grid>

        </div>
      );
    }
  }
  render() {
    const { show } = this.state;
    return (
      <div className="modal-container">        
        <Modal 
          backdrop="static" 
          size="sm"
          show={ true } onHide={this.close}>
          <Modal.Header closeButton={ false }>
            <Modal.Title><h4>Покупка билетов</h4></Modal.Title>
            <a href="https://showmaket.ru">На основную страницу</a>
          </Modal.Header>
          <hr />
          <Modal.Body>
            { this.state.htmlpage }
          </Modal.Body>
          <hr />
          <Modal.Footer>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

const container = document.querySelector("#root");
ReactDOM.render(React.createElement(BuyTicketsMenu, { data: tariffs } ), container);
