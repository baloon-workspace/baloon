import React from 'react';
import ReactDOM from 'react-dom';

import { ButtonGroup, Button, IconButton, ButtonToolbar } from 'rsuite';
import { Grid, Row, Col } from 'rsuite';
import { Input, InputNumber, InputGroup } from 'rsuite';
import { Tag, TagGroup } from 'rsuite';
import { FlexboxGrid } from 'rsuite';
import { Form, FormControl, FormGroup } from 'rsuite';
import { Panel, Icon, ControlLabel } from 'rsuite';
import { SelectPicker, TagPicker, InputPicker, DatePicker, Sidebar } from 'rsuite';
import { format, formatDistance, formatRelative, subDays, addDays} from 'date-fns'
import { SvgIcons } from 'components'

var externalerror = {
	textAlign: "center",
	display: "none",
	color: "red"
};

var outoftickets = {
	textAlign: "center",
	display: "none",
	color: "red"
};

var outofemail = {
	textAlign: "center",
	display: "none",
	color: "red"
};

var outoftime = {
	textAlign: "center",
	display: "none",
	color: "red"
};

var outofbuytickets = {
	textAlign: "center",
	display: "none",
	color: "red"
};

var main = {
	width: "100%",
	marginBottom: "40px",
	marginTop: "40px"
};

var menuttariff_a = {
	marginTop: "2px",
	marginLeft: "2px",
	marginBottom: "2px",
	marginRight: "2px"
};

var menuttariff_b = {
	marginTop: "2px",
	marginLeft: "2px",
	marginBottom: "2px",
	marginRight: "2px",
	width: "118px"
};

var menuttariff_c = {
	marginTop: "2px",
	marginLeft: "2px",
	marginBottom: "2px",
	marginRight: "2px",
	width: "100%"
};

var contentcenter = {
	width: "100%",
	display: "inline-block",
	textAlign: "center",
	verticalAlign: "middle"
};

var contentleft = {
	width: "100%",
	display: "inline-block",
	textAlign: "left",
	verticalAlign: "middle"
};

var extraoptions = {
	height: "120px",
	width: "100%",
	whiteSpace: "normal",
	wordWrap: "break-word"
};

var marginstyle = {
	marginTop: "2px",
	marginLeft: "2px",
	marginBottom: "2px",
	marginRight: "2px",
};

var taCenter = {
	textAlign: "center"
};

var taLeft = {
	textAlign: "left"
};

var taRight = {
	textAlign: "right"
};

const TicketGroupControl = ({ number, appearance, size, ...props }) => (
	<Panel { ...props } bordered style={{ position: "relative" }}>
		<IconButton icon={ <Icon icon="close" /> } 
			style={{ position: "absolute", zIndex: "1000", top:"2px", right: "2px" }} 
			appearance={ appearance } size="md" onClick={ function() {

				document.getElementById('add-1').style.display = 'none';

				tickets.forEach( ( ticketitem, i ) => {
					paidcategory.forEach( ( paiditem, j ) => {
						var objectcolor = document.getElementById("buttonpaid" + i + j);
						if ( objectcolor.classList.contains("item-selected") ) {
							$( "#buttonpaid" + i + j ).removeClass("item-selected");
						}
					});
				});

				tickets.splice( number, 1 );

				setTimeout(() => {								
					_this.update();
				}, 200);

			} }/>
		<Grid fluid>
			<Row>
				<Col style={{ marginBottom: "8px" }} xs={24}>
					<h6>БИЛЕТ №{ number + 1 }</h6>
				</Col>
			</Row>
			<Row>
				<Col style={{ marginBottom: "8px" }} xs={24}>
					<Button appearance="default" size={ size } style={{ width: "100%", height: "42px" }}>Вход свободный</Button>
				</Col>
			</Row>
			<Row>
				<Col xs={24}>
					<div style={{ marginBottom: "8px" }}>Дополнительные услуги:</div>
					<hr />
				</Col>
			</Row>
			<Row>
				{ ( typeof paidcategory != "undefined" && paidcategory != null && paidcategory.length != null && paidcategory.length > 0 && !effiarrayisempty(paidcategory) ) ? (
					paidcategory.map(( item, index ) => (
						<Col xs={ 24 / effiarraylength( paidcategory ) }>
							<Button style={ extraoptions } id={ "buttonpaid" + number + index  } size={ size } onClick={ function() {
									var objectcolor = document.getElementById("buttonpaid" + number + index);
									if ( objectcolor.classList.contains("item-selected") ) {
										$( "#buttonpaid" + number + index ).removeClass("item-selected");
										tickets[number].ticket[index] = 0;
										extracount = extracount - 1;
									} else {
										if ( weektariff >= 0 )
										{
											$( "#buttonpaid" + number + index ).addClass("item-selected");
											tickets[number].ticket[index] = 1;
											extracount = extracount + 1;
										}
									} 
							}}>{ ( weektariff < 0 ) ? "Выберите время посещения" : ( ( description( index ) != '' ) ? description( index ) : "Платные услуги отсутствуют" ) }<p>{ ( weektariff < 0 ) ? "" : ( ( price( index ) > 0 ) ? ( price( index ) + " руб" ) : "" ) }</p></Button>
						</Col>
					) )
				) : (	
					<Col xs={ 24 }><div style={{ width: "100%", textAlign: "center" }}>Платные услуги отсутствуют</div></Col>
				) }
			</Row>
		</Grid>
	</Panel>
);

const OnlineMenuButtonGroup = ({ appearance, size, labelleft, labelright, ...props }) => (
	<ButtonGroup { ...props } >
		<Button appearance={appearance} style={{ width: "100% - 70px", textAlign: "center" }}>{ labelleft }</Button>
		<Button appearance={appearance} style={{ width: "70px", textAlign: "center" }}>{ labelright }</Button>
	</ButtonGroup>
);

const PlusInputGroupWidthButton = ({ appearance, size, label, ...props }) => (
	<ButtonGroup { ...props } inside >
		<IconButton icon={ <Icon icon="plus" /> } appearance={ appearance } circle size={ size } />
		<Button appearance="default" size={ size }>{ label }</Button>
	</ButtonGroup>                     
);

const CustomInputGroupWidthButton = ({ placeholder, appearance, ...props }) => (
	<InputGroup { ...props } inside >
		<Input appearance={ appearance } placeholder={ placeholder } />
		<InputGroup.Button>
			<Icon icon="rub" />
		</InputGroup.Button>
	</InputGroup>
);

class TicketsMenuComponent extends React.Component {
	constructor(props) {
		super(props);

    		this.pay = this.pay.bind(this);

		this.state = {
			page: this.RenderTickets()
		};

		_this = this;
	}
	
	update() {
		this.setState( { page: this.RenderTickets() } );

		extracount = 0;
		tickets.forEach( ( ticketitem, i ) => {
			paidcategory.forEach( ( paiditem, j ) => {
				if( tickets[i].ticket[j] > 0 ) {
					extracount = extracount + 1;
					$( "#buttonpaid" + i + j ).addClass("item-selected");
				}
			});
		});
	}

	pay(event) {
		console.log(tickets);

		if ( weektariff == -1 ) {
			document.getElementById('weektariff-1').style.display = 'block';
			document.getElementById('add-1').style.display = 'none';
			document.getElementById('email-1').style.display = 'none';
			return;
		} else {
	                document.getElementById('weektariff-1').style.display = 'none';
		}
		if ( tickets.length == 0 ) {
			document.getElementById('add-1').style.display = 'block';
			document.getElementById('weektariff-1').style.display = 'none';
			document.getElementById('email-1').style.display = 'none';
			return;
		} else {
			document.getElementById('add-1').style.display = 'none';
		}

		var email = document.getElementById("emailinput");

		const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		if( re.test(email.value) ) { 
			document.getElementById('email-1').style.display = 'none';
			requestemail = email.value;
			document.getElementById('submitbutton').disabled = true;

			ProtocolPlaceAmount( currentdate, requestemail );
		} else {
			document.getElementById('email-1').style.display = 'block';
			document.getElementById('add-1').style.display = 'none';
			document.getElementById('weektariff-1').style.display = 'none';
			return;
		}		
  	}

	DateTimeString( date ) {
		var options = {
			day: 'numeric',
			month: 'numeric',
			year: 'numeric',
		};
		return date.toLocaleString("ru", options);
	}

	RenderTickets() {

		var options = new Array();

		var count = tickets.length;
		for ( var i = 0; i < count; ++i )
			options.push( { id: i, object: "card", ticket: tickets[i].ticket } );

		tickets = options.slice(0);
		options.push( { id: count, object: "selector", ticket: null } );

		var pageobjects = ( 
	<Panel bordered>
		<div style={{ width: "100%" }}>	
			<h5>Выбранные билеты</h5>
			<hr />
	                { options.map((item, index) => (
				<div class="ticket-width">
					{ ( item.object == "card" ) ? (
						<TicketGroupControl appearance="default" size="sm" number={ index } 
							style={{ width: "100%", marginBottom: "12px" }}  />
					) : (
						<PlusInputGroupWidthButton appearance="default" size="md" label="Добавить билет" 
							style={{ width: "100%", marginTop: "2px" }} onClick={ function() { 
								document.getElementById('add-1').style.display = 'none';
								if ( tickets.length <= 5 ) {
									var t = Array();
									for ( var i = 0; i < effiarraylength( paidcategory ); i++ )
										t.push(0);
									tickets.push( { id: index, object: "card", ticket: t } );
									_this.update();
								}
							} } />
					) }
				</div>
			) ) } 
		</div>
	</Panel> );

		return pageobjects;
	}

	render() { 
		return (
<div style={ main }>
        <FlexboxGrid justify="center" align="top">
		<FlexboxGrid.Item colspan={18}>
			<div class="lpart-content" style={{ textAlign: "justify" }}>
				<h5>Режим работы:</h5>
				<br />
				<div>С ПН по ВС с 10:00 до 22:00</div>
				<br />
				<p><b>Бронирование билетов:</b></p>
				<div>Вход в помещение пункта проката только в масках и перчатках. Время сеансов в будние и выходние дни отличаются.</div>
				<div>Онлайн-билеты на выбранный сеанс можно приобрести за два часа, а так же на кассе за час до начала сеанса.</div>
			</div>
			<div class="rpart-content">
				<h5>Онлайн - экран на { this.DateTimeString( initdate ) }</h5>
				<br />
				<Panel bordered style={{ textAlign: "center" }}>
					<OnlineMenuButtonGroup style={ menuttariff_a } appearance="default" size="sm" labelleft="Время сеанса" labelright="Билеты" />
					<hr />
					{ ( tariffstoday.length > 0 ) ? (
						tariffstoday.map(( item, index ) => (
							<OnlineMenuButtonGroup style={ menuttariff_a } appearance="default" size="sm" labelleft={ effitimetostring( item.time_from ) +  " - " + effitimetostring( item.time_upto ) } labelright={ item.free } />
						) )
					) : (	
						<div style={{ width: "100%", textAlign: "center" }}>На этот день сеансы не предусмотрены</div>
					) }
				</Panel>
			</div>
		</FlexboxGrid.Item>
	</FlexboxGrid>
	<br />
        <FlexboxGrid justify="center" align="top">
		<FlexboxGrid.Item colspan={18}>
			<div class="lpart-content" style={{ textAlign: "justify" }}>
				<h5>Дата бронирования:</h5>
				<br />
				<DatePicker isoWeek oneTap
					format={ "DD.MM.YYYY" }
					defaultValue={ currentdate }
                	  		size="lg"
					placement="bottomStart"     
					placeholder="Текущая дата"
					style={{ width: "100%" }} 
					cleanable={ false }
					disabledDate={ ( date ) => { 
						var decision = false;

						var ttt1 = parseInt(date.getDate());
						var ttt2 = parseInt(date.getMonth() + 1);
						var ttt3 = parseInt(date.getFullYear());
												
						holidays.forEach( ( item, i ) => {
							var uuu1 = parseInt(item['adate'].getDate());
							var uuu2 = parseInt(item['adate'].getMonth() + 1);
							var uuu3 = parseInt(item['adate'].getFullYear());
							if ( (ttt1 == uuu1) && (ttt2 == uuu2) && (ttt3 == uuu3) )
							{	decision = true;
							}
						});

						var yesterday = subDays(new Date(), 1);
						if ( date < yesterday )	decision = true;

						return decision;
	        	          	}  }
        	          		onChange = { ( date ) => {
						ProtocolDateListGet( date, ( data ) => {
							document.getElementById('weektariff-1').style.display = 'none'; 
							weektariff = -1;
							currentdate = date;
							this.setState( { page: this.RenderTickets() } );
						});
					} }
					ranges={ [
                    				{
							label: 'Сегодня',
							value: new Date(),
							closeOverlay: true
						},
						{
							label: 'Завтра',
							value: addDays(new Date(), 1),
							closeOverlay: true
                	    			}
        	          		] }
					locale={ {
						sunday: 'Вс',
						monday: 'Пн',
						tuesday: 'Вт',
						wednesday: 'Ср',
						thursday: 'Чт',
						friday: 'Пт',
						saturday: 'Сб',
						ok: 'OK',
						today: 'Сегодня',
						yesterday: 'Вчера',
						tomorrow: 'Завтра',
						hours: 'Часы',
						minutes: 'Минуты',
						seconds: 'Секунды'
					} } >
				</DatePicker>
			</div>
			<div class="rpart-content">
				<h5>Выберите сеанс:</h5>
				<div id="freetickets">Сеанс не выбран</div>
				<br />
				<div id="outoftickets-1" style={ outoftickets }>Онлайн-билеты на выбранный сеанс закончились. Билеты можно приобрести на кассе за час до начала сеанса.</div>
				<Panel bordered style={{ textAlign: "center" }}>
					{ ( tariffs.length > 0 ) ? (
						tariffs.map(( item, index ) => (
							<Button style={ menuttariff_b } id={ item.id } disabled={ ( isdisabled( index ) != true ) ? false : true } active={ ( weektariff == index ) ? true : false } appearance="default" size="sm" onClick={ function() {

								var itemdate = effiappendtimetoanydate( item.time_from, currentdate );

								var date = new Date();

								var ttt1 = parseInt(date.getDate());
								var ttt2 = parseInt(date.getMonth() + 1);
								var ttt3 = parseInt(date.getFullYear());

								var ttt4 = parseInt(date.getHours());
								var ttt5 = parseInt(date.getMinutes());
								var ttt6 = parseInt(date.getSeconds());
							
								var uuu1 = parseInt(itemdate.getDate());
								var uuu2 = parseInt(itemdate.getMonth() + 1);
								var uuu3 = parseInt(itemdate.getFullYear());
				
								var uuu4 = parseInt(itemdate.getHours());
								var uuu5 = parseInt(itemdate.getMinutes());
								var uuu6 = parseInt(itemdate.getSeconds());

								weektariff = -1;

								var d = "Продажи на это время отсутствуют.";
								var e = "Билеты на выбранный сеанс закончились.";
								var f = "На сеанс c " + effitimetostring( item.time_from ) + " по " + effitimetostring( item.time_upto ) + " осталось " + item.free + " билетов.";

								document.getElementById('outoftickets-1').style.display = 'none';

								if ( item.free == 0 ) {
									document.getElementById('freetickets').innerText = e;
								} else {
									document.getElementById('freetickets').innerText = f;
								}

								var f_b = false;

								if ( ttt3 >= uuu3 && f_b == false ) {
									if ( ttt3 > uuu3 ) f_b = true;

									if ( ttt2 >= uuu2 || f_b == true ) {
										if ( ttt2 > uuu2 ) f_b = true;

										if ( ttt1 >= uuu1 || f_b == true ) {
											if ( ttt1 > uuu1 ) f_b = true;				
											if ( f_b == false ) {

												var ttt = ttt4 * 60 + ttt5;
												var uuu = uuu4 * 60 + uuu5;
								
												var ddd = 120;
                                	
												if ( (( uuu - ttt ) <= ddd) && (( uuu - ttt ) >= 0) ) {
													if ( item.free > 0 ) {
														document.getElementById('outoftickets-1').style.display = 'none';
														document.getElementById('freetickets').innerText = f;
														weektariff = index;
													} else {
														document.getElementById('outoftickets-1').style.display = 'block';
														document.getElementById('freetickets').innerText = e;
													}
												} else {
													document.getElementById('freetickets').innerText = d;
												}
											}
										} else {
											document.getElementById('freetickets').innerText = d;
										}
									} else {
										document.getElementById('freetickets').innerText = d;
									}
								} else {
									document.getElementById('freetickets').innerText = d;
								}
			
								_this.update();

							} } >{ effitimetostring( item.time_from ) } - { effitimetostring( item.time_upto ) }</Button>
						) )
					) : (	
						<div style={{ width: "100%", textAlign: "center" }}>На этот день сеансы не предусмотрены</div>
					) }
				</Panel>
			</div>
		</FlexboxGrid.Item>
	</FlexboxGrid>
	<br />
        <FlexboxGrid justify="center" align="top">
		<FlexboxGrid.Item colspan={18}>
			{ this.state.page }
		</FlexboxGrid.Item>
	</FlexboxGrid> 
	<br />
        <FlexboxGrid justify="center" align="top">
		<FlexboxGrid.Item style={{ textAlign: "center" }} colspan={18}>
			<div id="email-1" style={ outofemail }>Проверьте правильность ввода e-mail адреса.</div>
			<div id="weektariff-1" style={ outoftime }>Выберите время посещения.</div>
			<div id="add-1" style={ outofbuytickets }>Не добавлено ни одного билета.</div>
			<div id="outofservice" style={ externalerror }>Произошла ошибка на стороне сервера.</div>
		</FlexboxGrid.Item>
	</FlexboxGrid> 
	<br />
        <FlexboxGrid justify="center" align="bottom">
		<FlexboxGrid.Item style={{ textAlign: "left", verticalAlign: "bottom" }} colspan={18}>
			<div class="lpart-content">
				<h5>E-mail для регистрации:</h5>
				<br />
	        		<InputGroup size="lg" >
					<InputGroup.Addon> @</InputGroup.Addon>
					<Input id="emailinput"/>
				</InputGroup>
			</div>
			<div class="rpart-content" style={{ textAlign: "right", verticalAlign: "bottom" }}>
				<h5>&nbsp;</h5>
				<br />
				<IconButton id="submitbutton" appearance="primary" size="lg" icon={<Icon icon="angle-right" />} placement="right" onClick={ this.pay } >
					Бронировать
				</IconButton>
			</div>
		</FlexboxGrid.Item>
	</FlexboxGrid> 
	<br />
        <FlexboxGrid justify="center" align="bottom">
		<FlexboxGrid.Item style={{ textAlign: "left", verticalAlign: "bottom" }} colspan={18}>
			<div class="lpart-content" style={{ textAlign: "left" }}></div>	
			<div class="rpart-content" style={{ textAlign: "right" }}>
				<div id="externalmsg" style={{ color: "red", width: "100%", display: "none" }}>
					<div style={{ textAlign: "left", borderBottom: "1px solid red" }}>ОБЯЗАТЕЛЬНО</div>
					<div style={{ textAlign: "right" }}>сохраняйте билеты на ваш смартфон</div>
				</div>
			</div>
		</FlexboxGrid.Item>
	</FlexboxGrid> 
	<br />
        <FlexboxGrid justify="center" align="bottom">
		<FlexboxGrid.Item style={{ textAlign: "left", verticalAlign: "bottom" }} colspan={18}>
			<div class="lpart-content" style={{ textAlign: "left" }}></div>	
			<div class="rpart-content" style={{ textAlign: "right" }}>
				<IconButton appearance="default" id="externalorder" size="lg" style={{ width: "100%", display: "none" }}  icon={<Icon icon="angle-right" />} placement="right" onClick={ function() {
					ProtocolDownloadTickets(externalorderid);
				}} >Скачать билет(ы)</IconButton>
			</div>
		</FlexboxGrid.Item>
	</FlexboxGrid> 
</div> ) }
}

var _this = null;

ReactDOM.render(React.createElement(TicketsMenuComponent, 0 ), document.getElementById('root') );
                                        