import React from 'react';
import ReactDOM from 'react-dom';

import { ButtonGroup, Button, IconButton, ButtonToolbar } from 'rsuite';
import { Grid, Row, Col } from 'rsuite';
import { Input, InputNumber, InputGroup } from 'rsuite';
import { Tag, TagGroup, Message } from 'rsuite';
import { FlexboxGrid, Whisper, Popover } from 'rsuite';
import { Form, FormControl, FormGroup } from 'rsuite';
import { Panel, Icon, ControlLabel } from 'rsuite';
import { SelectPicker, TagPicker, InputPicker, DatePicker, Sidebar } from 'rsuite';
import { format, formatDistance, formatRelative, subDays, addDays} from 'date-fns'
import { SvgIcons } from 'components'

var main = {
	width: "100%"
};

var sectionleft = {
	paddingRight: "22px",
	paddingBottom: "22px",
	paddingTop: "22px",
	paddingLeft: "22px",
	minWidth: "32%",
	float: "left"
};

var sectionright = {
	width: "auto",
	textAlign: "right",
};

var contentcenter = {
	width: "100%",
	display: "inline-block",
	textAlign: "center",
	verticalAlign: "middle"
};

var contentleft = {
	width: "100%",
	display: "inline-block",
	textAlign: "left",
	verticalAlign: "middle"
};

var image = {
	height: "100%",
};

var footer = {
	clear: "both",
	marginBottom: "12px"
};

var amount = {
	width: "100%"
};

var email = {
	width: "100%"
};

var number = {
	width: "100%"
};

var pay = {
	width: "100%",
};

var paysize = {
	width: "200px",
	readonly: "readonly"
};

var payamount = {
	width: "100%",
	textAlign: "left"
};

var taCenter = {
	textAlign: "center"
};

var taLeft = {
	textAlign: "left"
};

var taRight = {
	textAlign: "right"
};


var _this;
var _index = 0;
var _options;

// 	<Panel { ...props } style={{ position: "relative", background: "linear-gradient(60deg, rgba(197, 61, 255, 1) 0%, rgba(0, 172, 193, 1) 100% )" }} bordered>

const SkipassHillGroupControl = ({ number, appearance, size, onButtonClick, ...props }) => (
	<Panel { ...props } style={{ position: "relative", background: "linear-gradient(130deg, rgb(0, 172, 220) 30%,rgba(255, 0, 93, 30) 100% )" }} bordered>
		<Grid fluid>
			<Row>
				<Col style={{ width: "100%", textAlign: "left" }} xs={24}>
					<div style={{ width: "100%", fontWeight: "bold", fontSize: "140%", paddingBottom: "6px", borderBottom: "1px solid #575757" }}>
						«{ _options[ number ]["skipass"]["permanent_rule"] }»
					</div>
				</Col>
			</Row>
			<Row>
				<Col xs={24}>
					<Panel style={{ width: "100%", textAlign: "left", cursor: "pointer" }} onClick={ onButtonClick }>
						<div style={{ fontWeight: "normal", fontSize: "120%", display: "inline" }}>
							{ _options[ number ]["skipass"]["code"] }
						</div>
						<br />
						<div style={{ fontWeight: "normal", fontSize: "80%", display: "inline" }}>
							Доступно: { ( _options[ number ]["skipass"]["balance"][0]["balance"] != "" ) ? _options[ number ]["skipass"]["balance"][0]["balance"] : "0" }&nbsp;{ ( _options[ number ]["skipass"]["balance"][0]["currency"] != "" ) ? _options[ number ]["skipass"]["balance"][0]["currency"] : "руб" }
						</div>
					</Panel>
				</Col>
			</Row>
		</Grid>
	</Panel>
);

const PlusInputGroupButtonControl = ({ cid, placeholder, onButtonClick, ...props }) => (
	<Panel { ...props } style={{ position: "relative", borderColor: "#22b14c" }} bordered>
		<Grid fluid>
			<Row>
				<div style={{ width: "100%", textAlign: "left", marginTop: "12px", marginBottom: "12px" }} >
					<div style={{ width: "100%", fontWeight: "bold", fontSize: "120%", marginBottom: "12px" }}>Добавление карты:</div>
					<InputGroup { ...props } inside style={{ borderColor: "#22b14c" }} size="lg" >
						<Input id={ cid } placeholder={ placeholder } />
						<InputGroup.Button onClick={ onButtonClick }>
							<Icon style={{ color: "#22b14c" }} icon="plus" />
						</InputGroup.Button>
					</InputGroup>
					<div style={{ width: "100%", fontWeight: "normal", fontSize: "80%", marginTop: "4px" }}>Номер карты вида: 00120034</div>
					<div id="plusregcodefailed" style={{ width: "100%", textAlign: "left", color: "red", fontWeight: "normal", fontSize: "80%", marginTop: "12px", display: "none" }}>Ошибка при добавлении карты</div>
				</div>
			</Row>
		</Grid>
	</Panel>
);

class SkipassMenuComponent extends React.Component {
	constructor( props ) {

		super( props );

		this.auth = false;
		this.state = {
			page: this.RenderMenu( "login" )
		};

		_this = this;
	}

	update( pagename ) {

		_this.setState( { page: ( <div style={{ width: "100%" }}>&nbsp;</div> ) } );

		setTimeout( function() {
	 		_this.setState( { page: _this.RenderMenu( pagename ) } );
		}, 400);

	}

	RenderSkipass() {

		var options = new Array();

		var count = skipasshill.length;

		for ( var i = 0; i < count; ++i )
			options.push( { id: i, object: "card", skipass: skipasshill[i] } );

		options.push( { id: count, object: "selector", skipass: null } );

		_options = options.slice(0);

		var pageobjects = ( 
			<Panel bordered>
				<div style={{ width: "100%" }}>	
					<h5>Мои карты</h5>
					<hr />
			                { _options.map((item, index) => (
						<div class="ticket-width">
							{ ( item.object == "card" ) ? (
								<SkipassHillGroupControl appearance="default" size="sm" number={ index } 
									style={{ width: "100%", marginBottom: "12px" }} 
									onButtonClick={ function() {
										_index = index;
                                                                        	_this.update( "refill" );
									}} />
							) : (
								<PlusInputGroupButtonControl cid="plusregcode" appearance="default" size="lg" placeholder="Номер карты" 
									style={{ width: "100%", marginTop: "2px" }} defaultValue={ "" } prevValue={ "" }

									onChange={ function() {

										document.getElementById('plusregcodefailed').style.display = "none";

										if ( !isNaN( document.getElementById('plusregcode').value ) ) {
											// number
											document.getElementById('plusregcode').prevValue = document.getElementById('plusregcode').value;
										} else {
											// string										
											document.getElementById('plusregcode').value = ( !isNaN( document.getElementById('plusregcode').prevValue ) ) ? document.getElementById('plusregcode').prevValue : "";
										}

									} }

									onButtonClick={ function() { 

										var v_regcode = document.getElementById('plusregcode').value;
											
										if ( v_regcode.length == 8 ) {

											document.getElementById('plusregcodefailed').style.display = "none";

											ProtocolBindIdentifierToCurrentPerson( v_regcode, function ( data1 ) {

												if ( data1 == "false" ) {
													document.getElementById('plusregcodefailed').style.display = "block";
												} else {
													ProtocolCurrentIdentifierListGet( function( data3 ) {
														if ( data3 != "false" ) {
															skipasshill = data3.slice(0);

															for ( var iiiii = 0; iiiii < skipasshill.length; iiiii++ ) {
																var e = false;

																if ( ( iiiii + 1 ) == skipasshill.length ) e = true;

																ProtocolGetBalance( skipasshill[ iiiii ]['code'], function ( data4 ) {
	                                        	                                	                                        if ( data4 != "false" ) {
																		var _t = new Array();
										
																		for ( var jjjjj = 0; jjjjj < data4.length; jjjjj++ )
																		{
																			_t[jjjjj] = [];
																			_t[jjjjj]['currency'] = data4[jjjjj]['currency'];
																			_t[jjjjj]['balance'] = data4[jjjjj]['balance'];
																		}

																		for ( var _z = 0; _z < skipasshill.length; _z++ ) {
																			if ( skipasshill[ _z ]['code'] == data4['code'] ) {
																				skipasshill[ _z ]['balance'] = _t;
																				break;
																			}
																		}
																			
																		ProtocolIdentifierTransactionList( data4['code'], function( data5 ) {
																			if ( data5 != "false" )
																			{
																				skipasshistory.push( data5 );
																			}

		                                                                                							if ( e == true ) {
																				_this.auth = true;
																				_this.update( "cards" );
																			}
																		} );
																	}
																} );
															}
									
															//////////////////////////////////////////////////////////////
															//: ProtocolIdentifierTransactionList
															//: console.log( "data: test" );
															//////////////////////////////////////////////////////////////
														}
													} );
												} 
											} )
										}
									} } 
								/>
							) }
						</div>
					) ) } 
				</div>
			</Panel> );
		return pageobjects;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// RenderMenu
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	RenderMenu(pagename) {

		var pageobjects = ( <div>&nbsp;</div> );

		if ( pagename == "history" ) {
			pageobjects = (	
				<div style={{ width: "100%" }}>
				        <FlexboxGrid justify="center" align="top">
						<FlexboxGrid.Item colspan={24}>
							{ ( skipasshistory.length == 0 ) ? (   
								<Panel bordered style={{ position: "relative" }}>
									<div style={{ width: "100%", fontSize: "120%", textAlign: "center" }} >История отсутствует</div> 
								</Panel>
							) : ( skipasshistory.map((item, index) => (
								<div style={{ width: "100%" }} >
									<Message type="info" description={
										<p>
											Сумма: { item["amount"] }&nbsp;{ ( item["currency"] != "" ) ? item["currency"] : "руб" } <br />
											Комментарий: { item["comment"] } <br />
											Дата: { item["date"] } <br />
										</p>
									} />
									<br />								
								</div>
							) ) ) }
						</FlexboxGrid.Item>
					</FlexboxGrid> 
				</div>
			)
		} else if ( pagename == "cards" ) {
			pageobjects = (	
				<div style={{ width: "100%" }}>
				        <FlexboxGrid justify="center" align="top">
						<FlexboxGrid.Item colspan={24}>
							{ _this.RenderSkipass() }
						</FlexboxGrid.Item>
					</FlexboxGrid> 
				</div>
			)
		} else if ( pagename == "login" ) {
			pageobjects = (	
				<div style={{ width: "100%" }}>
				        <FlexboxGrid justify="center" align="top">
						<FlexboxGrid.Item justify="left" align="top" colspan={24}>
							<h3>Вход в аккаунт</h3>
							<br />
							<br />
							<h6>Заполните все поля для авторизации</h6>
							<br />
							<Input id="userlogin" defaultValue={ "" } size="lg" placeholder="Введите имя или e-mail" />
							<br />						
							<Input id="userpassword" defaultValue={ "" } type="password" size="lg" placeholder="Пароль" />
							<br />
							<br />
						</FlexboxGrid.Item>
        	                        </FlexboxGrid>
				        <FlexboxGrid justify="center" align="top">
						<FlexboxGrid.Item justify="center" align="top" colspan={24}>
							<Button id="tm-enter" style={{ width: "100%" }} appearance="ghost" size="lg" onClick={ function() {

								var v_login = document.getElementById('userlogin').value;
								var v_password = document.getElementById('userpassword').value;

								document.getElementById('loginfailed').style.display='none';

								if ( v_login.length < 5 ) {
                							document.getElementById('loginfailed').innerText = "Длина имени пользователя минимум пять символов";
									document.getElementById('loginfailed').style.display='block';
									return;
								}

								if ( v_password.length < 6 ) {
                							document.getElementById('loginfailed').innerText = "Длина пароля минимум шесть символов";
									document.getElementById('loginfailed').style.display='block';
									return;
								}

								ProtocolLogin( v_login, v_password, function( data ) {

									if ( data != "false" ) {

										ProtocolGetCurrent( function( data2 ) {

											if ( data2 != "false" ) {

												profile = data2;

												if ( v_login == "admin" ) {
													profile.email = v_login;
												}

												ProtocolCurrentIdentifierListGet( function( data3 ) 
												{
													if ( data3 != "false" ) 
													{
														skipasshill = data3.slice(0);

														for ( var iiiii = 0; iiiii < skipasshill.length; iiiii++ )
														{
															var e = false;

															if ( ( iiiii + 1 ) == skipasshill.length ) e = true;

															ProtocolGetBalance( skipasshill[ iiiii ]['code'], function ( data4 )
															{
	                                                                                                                	if ( data4 != "false" )
																{
																	var _t = new Array();

																	for ( var jjjjj = 0; jjjjj < data4.length; jjjjj++ )
																	{
																		_t[jjjjj] = [];
																		_t[jjjjj]['currency'] = data4[jjjjj]['currency'];
																		_t[jjjjj]['balance'] = data4[jjjjj]['balance'];
																	}

																	for ( var _z = 0; _z < skipasshill.length; _z++ ) {
																		if ( skipasshill[ _z ]['code'] == data4['code'] ) {
																			skipasshill[ _z ]['balance'] = _t;
																			break;
																		}
																	}
																	
																	ProtocolIdentifierTransactionList( data4['code'], function( data5 ) 
																	{
																		if ( data5 != "false" )
																		{
																			skipasshistory.push( data5 );
																		}

																		if ( e == true ) {
																			_this.auth = true;
																			_this.update( "cards" );
																		}
																	} );
																}
															} );
														}

														//////////////////////////////////////////////////////////////
														//: ProtocolIdentifierTransactionList
														//: console.log( "data: test" );
														//////////////////////////////////////////////////////////////
													}
												} );
											}
										} )
									} else {
										document.getElementById('loginfailed').innerText = "Имя польльзователя или пароль введены неверно";
										document.getElementById('loginfailed').style.display='block';
									}
								} );

							} } >Войти</Button>
							<br />
							<Button style={{ textAlign: "center", width: "100%" }} appearance="link" size="lg" onClick={ function() {
								_this.update( "registration" );
							} } >Нет аккаунта? Зарегистрируйтесь</Button>
						</FlexboxGrid.Item>
	                                </FlexboxGrid>
					<div style={{ width: "100%" }}>
						<div id="loginfailed" style={{ textAlign: "center", color: "red", display: "none" }}>Имя польльзователя или пароль введены неверно</div>
					</div>
				</div>
			)

		} else if ( pagename == "registration" ) {
			pageobjects = (	
				<div style={{ width: "100%" }}>
				        <FlexboxGrid justify="center" align="top">
						<FlexboxGrid.Item justify="left" align="top" colspan={24}>
							<h3>Регистрация</h3>
							<br />
							<br />
							<h6>Заполните все поля для регистрации</h6>
							<br />
							<Input id="userlogin" defaultValue={ "" } size="lg" placeholder="Введите имя или e-mail" />
							<br />						
							<Input id="regcode1" defaultValue={ "" } size="lg" placeholder="Введите регистрационный код" prevValue={ "" } onChange={ function() {
								if ( !isNaN( document.getElementById('regcode1').value ) ) {
									// number
									document.getElementById('regcode1').prevValue = document.getElementById('regcode1').value;
								} else {
									// string										
									document.getElementById('regcode1').value = ( !isNaN( document.getElementById('regcode1').prevValue ) ) ? document.getElementById('regcode1').prevValue : "";
								}	
							} } />
							<br />						
							<Input id="usernpwd1" defaultValue={ "" } size="lg" type="password" placeholder="Пароль" />
							<br />
							<Input id="usernpwd2" defaultValue={ "" } size="lg" type="password" placeholder="Повторите пароль" />
							<br />
						</FlexboxGrid.Item>
        	                        </FlexboxGrid>
					<br />
				        <FlexboxGrid justify="center" align="top">
						<FlexboxGrid.Item justify="center" align="top" colspan={24}>
							<Button id="tm-register" style={{ width: "100%" }} appearance="ghost" size="lg" onClick={ function() {

								var v_regcode1 = document.getElementById('regcode1').value;
								var v_login = document.getElementById('userlogin').value;

								var v_password1 = document.getElementById('usernpwd1').value;
								var v_password2 = document.getElementById('usernpwd2').value;

								document.getElementById('registrationfailed').style.display = 'none';

								if ( v_login.length < 4 ) {
                							document.getElementById('registrationfailed').innerText = "Длина имени пользователя минимум четыре символа";
									document.getElementById('registrationfailed').style.display = 'block';
									return;
								}

								if ( v_regcode1.length != 8 ) {
                							document.getElementById('registrationfailed').innerText = "Длина регистрационного кода должна быть восемь символов";
									document.getElementById('registrationfailed').style.display = 'block';
									return;
								}
								
								if ( isNaN( v_regcode1 ) ) {
	               							document.getElementById('registrationfailed').innerText = "Регистрационный кода должен содержать только цифры";
									document.getElementById('registrationfailed').style.display = 'block';
									return;
								}
	
								if ( v_password1.length < 6 ) {
                							document.getElementById('registrationfailed').innerText = "Длина пароля минимум шесть символов";
									document.getElementById('registrationfailed').style.display = 'block';
									return;
								}

								if ( v_password2.length < 6 ) {
                							document.getElementById('registrationfailed').innerText = "Длина пароля минимум шесть символов";
									document.getElementById('registrationfailed').style.display = 'block';
									return;
								}

								ProtocolRegisterNewClient( v_regcode1, v_login, v_password1, v_password2, "", "", "", function( data ) {
									if ( data != "false" ) {
										_this.update( "login" );
									} else {
        	        							document.getElementById('registrationfailed').innerText = "Произошла ошибка регистрации, проверьте введенные данные";
										document.getElementById('registrationfailed').style.display = 'block';
									}
								} )

							} } >Зарегистрироваться</Button>
							<br />
							<Button style={{ textAlign: "center", width: "100%" }} appearance="link" size="lg" onClick={ function() {
								_this.update( "login" );
							}} >Уже есть аккаунт? Войдите в кабинет</Button>
						</FlexboxGrid.Item>
	                                </FlexboxGrid>
					<div style={{ width: "100%" }}>
						<div id="registrationfailed" style={{ textAlign: "center", color: "red", display: "none" }}>Имя польльзователя или пароль введены неверно</div>
					</div>
				</div>
			)
		
		} else if ( pagename == "refillwologin" ) {
			pageobjects = (	
				<div style={{ width: "100%" }}>
				        <FlexboxGrid justify="center" align="top">
						<FlexboxGrid.Item justify="left" align="top" colspan={24}>
				                	<h3>Пополнение баланса SKI PASS</h3>
							<br />
							<br />
							<h6>Для пополения SKI PASS заполните поля формы</h6>
							<br />
							<Input id="c_number" size="lg" defaultValue={ "" } prevValue={ "" }  placeholder="Введите номер SKI PASS" onChange={ function() {
									if ( !isNaN( document.getElementById('c_number').value ) ) {
										// number
										document.getElementById('c_number').prevValue = document.getElementById('c_number').value;
									} else {
										// string
										document.getElementById('c_number').value = ( isNaN(document.getElementById('c_number').prevValue) == false ) ? document.getElementById('c_number').prevValue : "";
									}

									document.getElementById('c_number').innerText = document.getElementById('c_number').value;
								} }
							/>
						</FlexboxGrid.Item>
				        </FlexboxGrid>
					<br />
				        <FlexboxGrid justify="center" align="top">
						<FlexboxGrid.Item justify="left" align="top" colspan={8}>
							<InputGroup size="lg" style={{ borderColor: "#22b14c" }} inside>
								<Input id="c_amount" appearance="subtle" defaultValue={ "1000" } prevValue={ "" } placeholder="Сумма" onChange={ function() {
									if ( !isNaN( document.getElementById('c_amount').value ) ) {
										// number
										document.getElementById('c_amount').prevValue = document.getElementById('c_amount').value;
									} else {
										// string								
										document.getElementById('c_amount').value = (!isNaN(document.getElementById('c_amount').prevValue) ) ? document.getElementById('c_amount').prevValue : "";
									}

									document.getElementById('t_amount').innerText = document.getElementById('c_amount').value;
								} } />
								<InputGroup.Button>
									<Icon style={{ color: "#22b14c" }} icon="rub" />
								</InputGroup.Button>
							</InputGroup>
						</FlexboxGrid.Item>
						<FlexboxGrid.Item justify="right" align="top" colspan={16}>
							<div style={{ width: "100%", paddingLeft: "24px", textAlign: "right" }}>
								<Input size="lg" id="c_email" placeholder="Введите e-mail" defaultValue={ profile["email"] } onChange={ function() {
									var v_email = document.getElementById("c_email").value;
									const pattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
									if( pattern.test( v_email ) ) { 
										document.getElementById('emailfailed').style.display = 'none';
									} else {
										document.getElementById('emailfailed').style.display = 'block';
									}
								}} />
							</div>
						</FlexboxGrid.Item>
        	                        </FlexboxGrid>
					<br />
					<div style={{ width: "100%" }}>
						<div id="numberfailed" style={{ textAlign: "center", color: "red", display: "none" }}>Введите корректный номер SKI PASS</div>
						<div id="amountfailed" style={{ textAlign: "center", color: "red", display: "none" }}>Введите корректную сумму пополнения</div>
						<div id="paidfailed" style={{ textAlign: "center", color: "red", display: "none" }}>Ошибка оплаты, проверьте введенные данные</div>
						<div id="emailfailed" style={{ textAlign: "center", color: "red", display: "none" }}>Введите корректный e-mail в виде example@email.ru</div>
					</div>
					<br />
					<FlexboxGrid justify="center" align="top">
						<FlexboxGrid.Item justify="left" align="top" style={{ position: "relative" }} colspan={24}>
							<h6>Итоговая сумма:</h6>
							<div style={{ fontWeight: "bold", fontSize: "200%", display: "inline" }} id="t_amount">1000</div><Icon size="lg" icon="rub" />

							<Button id="tm-pay" style={{ position: "absolute", right: "0px", bottom: "0px" }} size="lg" appearance="ghost" onClick={ function() {

									var t_amount = document.getElementById("t_amount").innerText;
									var c_number = document.getElementById("c_number").value;
									var v_email = document.getElementById("c_email").value;

									document.getElementById('emailfailed').style.display = 'none';
									document.getElementById('numberfailed').style.display = 'none';
									document.getElementById('amountfailed').style.display = 'none';
									document.getElementById('paidfailed').style.display = 'none';

									if ( isNaN( c_number ) ) {
										document.getElementById('numberfailed').style.display = 'block';
										return;
									}

									if ( c_number.length != 8 ) {
										document.getElementById('numberfailed').style.display = 'block';
										return;
									}

									const pattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
									if( !pattern.test( v_email ) ) { 
										document.getElementById('emailfailed').style.display = 'block';
										return;
									}

									if ( isNaN( t_amount ) ) {
										document.getElementById('amountfailed').style.display = 'block';
										return;
									}

									if ( t_amount <= 0 ) {
										return;
									}
																		
									ProtocolNologinIdentifierRaiseInvoice( c_number, t_amount, function( data ) {
										if ( data != 'false' ) {
											_this.update("cards");
										} else {
											document.getElementById('paidfailed').style.display = 'block';
										}
									} );

							}}>Оплатить</Button>
						</FlexboxGrid.Item>
        	                        </FlexboxGrid>
				</div>
			)

		} else if ( pagename == "refill" ) {
			pageobjects = (	
				<div style={{ width: "100%" }}>
				        <FlexboxGrid justify="center" align="top">
						<FlexboxGrid.Item justify="left" align="top" colspan={24}>
				                	<h3>Пополнение баланса SKI PASS</h3>
							<br />
							<br />
							<h6>Для пополения SKI PASS заполните поля формы</h6>
							<br />
							<Input id="c_number" size="lg" defaultValue={ _options[ _index ]["skipass"]["code"] } prevValue={ _options[ _index ]["skipass"]["code"] } readonly={ "readonly" } placeholder="Введите номер SKI PASS" onChange={ function() {
									if ( !isNaN( document.getElementById('c_number').value ) ) {
										// number
										document.getElementById('c_number').prevValue = document.getElementById('c_number').value;
									} else {
										// string
										document.getElementById('c_number').value = ( isNaN(document.getElementById('c_number').prevValue) == false ) ? document.getElementById('c_number').prevValue : "";
									}
									document.getElementById('c_number').innerText = document.getElementById('c_number').value;
								} }
							/>
						</FlexboxGrid.Item>
				        </FlexboxGrid>
					<br />
				        <FlexboxGrid justify="center" align="top">
						<FlexboxGrid.Item justify="left" align="top" colspan={8}>
							<InputGroup size="lg" style={{ borderColor: "#22b14c" }} inside>
								<Input id="c_amount" appearance="subtle" defaultValue={ "1000" } prevValue={ "" } placeholder="Сумма" onChange={ function() {
									if ( !isNaN( document.getElementById('c_amount').value ) ) {
										// number
										document.getElementById('c_amount').prevValue = document.getElementById('c_amount').value;
									} else {
										// string								
										document.getElementById('c_amount').value = (!isNaN(document.getElementById('c_amount').prevValue) ) ? document.getElementById('c_amount').prevValue : "";
									}

									document.getElementById('t_amount').innerText = document.getElementById('c_amount').value;
								} } />
								<InputGroup.Button>
									<Icon style={{ color: "#22b14c" }} icon="rub" />
								</InputGroup.Button>
							</InputGroup>
						</FlexboxGrid.Item>
						<FlexboxGrid.Item justify="right" align="top" colspan={16}>
							<div style={{ width: "100%", paddingLeft: "24px", textAlign: "right" }}>
								<Input size="lg" id="c_email" placeholder="Введите e-mail" defaultValue={ profile["email"] } readonly={ "readonly" } onChange={ function() {
									var v_email = document.getElementById("c_email").value;
									const pattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
									if( pattern.test( v_email ) ) { 
										document.getElementById('emailfailed').style.display = 'none';
									} else {
										document.getElementById('emailfailed').style.display = 'block';
									}
								}} />
							</div>
						</FlexboxGrid.Item>
        	                        </FlexboxGrid>
					<br />
					<FlexboxGrid justify="center" align="top">
						<FlexboxGrid.Item justify="left" align="top" colspan={24}>
							<h6>Итоговая сумма:</h6>
							<div style={{ fontWeight: "bold", fontSize: "200%", display: "inline" }} id="t_amount">1000</div><Icon size="lg" icon="rub" />

							<Button id="tm-pay" style={{ position: "absolute", right: "0px", bottom: "0px" }} size="lg" appearance="ghost" onClick={ function() {

									var t_amount = document.getElementById("t_amount").innerText;
									var c_number = document.getElementById("c_number").value;
									var v_email = document.getElementById("c_email").value;

									document.getElementById('emailfailed').style.display = 'none';
									document.getElementById('numberfailed').style.display = 'none';
									document.getElementById('amountfailed').style.display = 'none';
									document.getElementById('paidfailed').style.display = 'none';

									if ( isNaN( c_number ) ) {
										document.getElementById('numberfailed').style.display = 'block';
										return;
									}

									if ( c_number.length != 8 ) {
										document.getElementById('numberfailed').style.display = 'block';
										return;
									}

									const pattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
									if( !pattern.test( v_email ) ) { 
										document.getElementById('emailfailed').style.display = 'block';
										return;
									}

									if ( isNaN( t_amount ) ) {
										document.getElementById('amountfailed').style.display = 'block';
										return;
									}

									if ( t_amount <= 0 ) {
										return;
									}
																		
									ProtocolNologinIdentifierRaiseInvoice( c_number, t_amount, function( data ) {
										if ( data != 'false' ) {
											_this.update("cards");
										} else {
											document.getElementById('paidfailed').style.display = 'block';
										}
									} );

							}}>Оплатить</Button>
						</FlexboxGrid.Item>
        	                        </FlexboxGrid>
					<br />
					<div style={{ width: "100%" }}>
						<div id="numberfailed" style={{ textAlign: "center", color: "red", display: "none" }}>Введите корректный номер SKI PASS</div>
						<div id="amountfailed" style={{ textAlign: "center", color: "red", display: "none" }}>Введите корректную сумму пополнения</div>
						<div id="paidfailed" style={{ textAlign: "center", color: "red", display: "none" }}>Ошибка оплаты, проверьте введенные данные</div>
						<div id="emailfailed" style={{ textAlign: "center", color: "red", display: "none" }}>Введите корректный e-mail в виде example@email.ru</div>
					</div>
					<br />
				</div>
			)
		}

		return pageobjects;
	}

	render() { 
		return (
			<div style={{ width: "100%", textAlign: "left" }}>
				<div id="left-content">
					<div style={{ padding: "46px 92px 46px 92px" }}>
				        	<FlexboxGrid justify="left" align="top">
							<FlexboxGrid.Item colspan={24}>
								{ ( _this.auth == false ) ? (
									<FlexboxGrid justify="left" align="top">
										<FlexboxGrid.Item style={{ textAlign: "left", position: "relative" }} colspan={ 24 }>
											<IconButton id="tm-login" appearance="subtle" size="lg" placement="left" disabled={ ( _this.auth == true ) ? true : false }  icon={ <Icon icon="avatar" size="lg" /> } onClick={ function() {
												_this.update( "login" );
											}}>Вход</IconButton>
											<Button id="tm-reg" appearance="ghost" size="lg" disabled={ ( _this.auth == true ) ? true : false } onClick={ function() {
												_this.update( "registration" );
											}}>Регистрация</Button>
											<Button id="tm-refill" appearance="ghost" size="lg" onClick={ function() {
	        	                                                                	_this.update( "refillwologin" );
											}}>Пополнение баланса</Button>
										</FlexboxGrid.Item>
									</FlexboxGrid>
								) : (
									<FlexboxGrid justify="left" align="top">
										<FlexboxGrid.Item style={{ textAlign: "left", position: "relative" }} colspan={ 24 }>
											<Whisper
												trigger="focus"
												placement="bottomStart"
												speaker={ ( 
													<Popover style={{ width: "400px" }} > 
														<div style={{ width: "100%" }}>
															<IconButton appearance="link" style={{ color: "gray" }} placement="left" size="lg" icon={ <Icon style={{ color: "#22b14c" }} icon="credit-card" size="lg" /> } onClick={ function() {
																_this.update( "cards" );
															}}>Мои карты</IconButton>
														</div>
														<div style={{ width: "100%" }} >
															<IconButton appearance="link" style={{ color: "gray" }} placement="left" size="lg" icon={ <Icon style={{ color: "#22b14c" }} icon="history" size="lg" /> } onClick={ function() {
																_this.update( "history" );
															}}>История операций</IconButton>
														</div>
													</Popover> 
												) } >
												<IconButton id="tm-menu" appearance="default" placement="right" size="lg" icon={ <Icon icon="angle-down" size="lg" /> }>{ ( ( profile.first_name + " " + profile.last_name ).length >= 5  ) ? ( profile.first_name + " " + profile.last_name ) : ( profile.email /* TokenName */ ) }</IconButton>
											</Whisper>
											<IconButton id="tm-exit" appearance="default" placement="right" size="lg" icon={ <Icon icon="sign-out" size="lg" /> } disabled={ ( _this.auth == false ) ? true : false } onClick={ function() {
												_this.auth = false;
												_this.update( "login" );
											}}>Выход</IconButton>
										</FlexboxGrid.Item>
									</FlexboxGrid>
								) }
							</FlexboxGrid.Item>
						</FlexboxGrid>
					        <FlexboxGrid justify="left" align="top">
							<FlexboxGrid.Item colspan={24}>
								<br />
								<br />
								{ this.state.page }
							</FlexboxGrid.Item>
						</FlexboxGrid>
					        <FlexboxGrid justify="left" align="top">
							<FlexboxGrid.Item colspan={24}>
								<br />
								<br />
								<Message type="info" description={
									<div style={{ textAlign: "left" }}>
									<p>
										Регистрируясь на этом веб-сайте, вы принимаете наши. <a href="#">Условия использования</a> и нашу <a href="#">Политику конфиденциальности</a>.
									</p>
									</div>
								} />
							</FlexboxGrid.Item>
						</FlexboxGrid>
					</div>
				</div>
				<div id="right-content">
					<div id="rs-side">&nbsp;</div>
				</div>
			</div> 
		) 
	}
}

ReactDOM.render(React.createElement(SkipassMenuComponent, 0 ), document.getElementById('root') );
