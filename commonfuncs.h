#ifndef EXTRAFUNCS_H
#define EXTRAFUNCS_H
#include <wand/MagickWand.h>
#include <qrencode.h>
#include "baloon_auto.h"
#include <string>
#include <sstream>
#include <vector>

namespace BALOON {

Effi::ShPtr<Effi::Blob> generateQRCode(std::string data, double scale);

std::string b64encode(const std::string& str);
std::string b64decode(const std::string& str64);

std::vector<std::string> split(const std::string &source, char delim); /// Функция разбиения строки
//str::string operator=(const std::string& dest, const Effi::Str source); /// Преобразование к строке
}//namespace BALOON
#endif /*EXTRAFUNCS_H*/
