#include <iostream>
#include <string>
#include <algorithm>
#include <boost/regex.hpp>
#include <boost/algorithm/string.hpp>
#include <cryptographer/evp.h>
#include <acommon/acode.h>
#include <Services/TaskMan/taskman_auto.h>
#include <Services/TaskMan/tm.h>
#include <reporter_auto.h>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <Paygate/paygate_auto.h>
#include <Fiscal/fiscal_auto.h>
#include "spd.h"
#include "commonfuncs.h"
#include <Fiscal/fiscal.h>
#include <Paygate/paygate.h>
#include <regex>
#include <jsoncpp/json/reader.h>
#include <jsoncpp/json/value.h>
#include <map>
#include <vector>
#include <mutex>
#include <stdlib.h>

namespace BALOON {

using namespace std;
using namespace PAYGATE;
using namespace Effi;

#define BALOON_IDENTIFIER_INVOICE_CLASS "IdentifierInvoice"
const string BALOON_PERSON_INVOICE_CLASS("PersonInvoice");

const string success_url_("/user#/success");
const auto fail_url_("/user#/fail");

const Double EPSILON(0.01);
const Double ONE_HUNDRED_PERCENT(100.0);

const Str ticket_templatename{"cable_car_ticket"};

std::string GetFullname(const Str last_name_, const Str first_name_, const Str middle_name_) {
    auto last_name(last_name_.get_value_or(string()));
    auto first_name(first_name_.get_value_or(string()));
    auto middle_name(middle_name_.get_value_or(string()));
    
    boost::algorithm::trim(last_name);
    boost::algorithm::trim(first_name);
    boost::algorithm::trim(middle_name);
    
    std::string fullname = last_name;
    if (!fullname.empty() && !first_name.empty()) fullname += " ";
    fullname += first_name;
    if (!fullname.empty() && !middle_name.empty()) fullname += " ";
    fullname += middle_name;
    return fullname;
}

std::string GetFullname(const Person& obj) {
    return GetFullname(obj.last_name, obj.first_name, obj.middle_name);
}

bool loginExists(Connection *rdb, const Str email) {
    AUTHORIZER::Users user;
    Selector sel;
    sel << user->TokenID;
    sel.Where(user->Login == email);
    DataSet data = sel.Execute(rdb);
    return data.Fetch();
}

void checkPersonOid(Connection *rdb, const string oid) {
    Person person;
    Selector sel;
    sel << person->id;
    sel.Where(person->oid == oid);
    DataSet data = sel.Execute(rdb_);
    if(data.Fetch()) {
        throw Exception(Message("Account with given code already registered. ").What());
    }
}

bool is_numeric(char const *str) {
    int sizeOfString = strlen(str);
    int iteration = 0;
    bool isNumeric = true;

    while(iteration < sizeOfString) {
        if(!isdigit(str[iteration])) {
            isNumeric = false;
            break;
        }
        iteration++;
    }
    return isNumeric;
}

string to_hex(string dec) {
    int decimal_value = boost::lexical_cast<int>(dec);
    std::stringstream ss;
    ss << std::hex << decimal_value; // int decimal_value
    std::string res(ss.str());
    return res;
}

string hex_to_decimal(string hex, size_t length) {
    int decimal_value;
    std::stringstream ss;
    ss << std::hex << hex;
    Log(5) << "hex_to_decimal hex=" << hex << " -> ss=" << ss.str() << endl;
    ss >> decimal_value;
    Log(5) << "  decimal_value=" << decimal_value << endl;
    string result = boost::lexical_cast<string>(decimal_value);
    while (result.length() < length) result = "0" + result;
    return result;
}

string hpadz(string hex) {
    if (hex.size()<2) hex = "0" + hex;
    return hex;
}

string swap_hex_word(string hex) {
    int sz = hex.length();
    if (sz<3) return hex;
    string res = "";
    for (int i=sz; i>0; i-=2) {
        if (i==1) res += hpadz(hex.substr(0, 1));
        else res += hpadz(hex.substr(i-2, 2));
    }
    return res;
}

string to_print_code(string print_code) {
    size_t sz = print_code.length();
    if (sz>10 || sz<6 || !is_numeric(print_code.c_str())) {
        throw Exception(Message("Field print_code is not valid. ").What());
    }
    Log(5) << "convert_print_code print_code=" << print_code << ", size=" << sz << endl;
    string db1 = print_code.substr(0, sz-5),
        db2 = print_code.substr(sz-5);
    string res = to_upper(swap_hex_word(to_hex(db2)) + swap_hex_word(to_hex(db1)));
    Log(5) << "  res=" << res << endl;
    return res;
}

bool string_is_hex(string print_code) {
    boost::algorithm::trim(print_code);
    if (print_code.empty()) return false;
    boost::regex pattern("[^0-9A-Fa-f]");
    boost::match_results<std::string::const_iterator> matcher;
    if (boost::regex_search(print_code, matcher, pattern, boost::match_default | boost::match_any)) {
        return false;
    }
    return true;
}

string from_print_code(string print_code) {
    if (!string_is_hex(print_code)) throw Exception(Message("System error: field print_code is not valid. ").What());

    size_t sz = print_code.length();
    if (sz > 6) {
        print_code = print_code.substr(0, 6);
        sz = 6;
    }
    if (sz < 6) {
        throw Exception(Message("System error: field print_code is not valid. ").What());
    }
    string hb1 = print_code.substr(0, 2),
        hb2 = print_code.substr(2, 2),
        hb3 = print_code.substr(4, 2);
    string result = hex_to_decimal(hb3, 3) + hex_to_decimal(hb2 + hb1, 5);
    Log(5) << "from_print_code " << print_code << " -> " << result << endl;
    return result;
}

string convertPrintCodeToSpd(string print_code) {
    Log(5) << "convertPrintCodeToSpd print_code=" << print_code << endl;
    BaloonProvider provider;
    provider.Select(rdb_);
    if (Defined(provider.code_conversion_algo) && CODE_CONVERSION_ALGO_REVERSE_HEX == provider.code_conversion_algo) {
        return from_print_code(print_code);
    }
    return print_code;
}

string convertSpdCodeToPrint(string code, const Str code_conversion_algo) {
    string algo = code_conversion_algo.get_value_or("");
    if (algo.empty()) {
        BaloonProvider provider;
        provider.Select(rdb_);
        algo = provider.code_conversion_algo.get_value_or("");
    }
    if (CODE_CONVERSION_ALGO_REVERSE_HEX == algo) {
        return to_print_code(code);
    }
    return code;
}

void BindIdentifier(Connection* rdb, const SpdClient& client, const Int& personid) {
    try {
        const auto spd_identifier = client.identifiers.at(0);
        const auto identifier_rulename = spd_identifier.permanent_rule;
        CheckIdentifierRuleEnabled(rdb, identifier_rulename);
        Identifier ident;
        ident.personid = personid;
        ident.code = spd_identifier.code;
        ident.valid_from = spd_identifier.valid_from;
        ident.valid_to = spd_identifier.valid_to;
        ident.permanent_rulename = identifier_rulename;
        ident.categoryname = spd_identifier.category;
        ident.ratename = spd_identifier.tariff;
        ident.clientoid = client.oid;
        ident.status = IDENTIFIER_STATUS_OK;
        ident.Insert(rdb);
    }
    catch(std::out_of_range&) {
        throw Exception(Message("No identifiers in SPD response").What());
    }
}

void CheckIdentifierRuleEnabled(Connection* rdb, const Str& identifier_rulename) {
    IdentifierRule rule(identifier_rulename);
    rule.Select(rdb);
    if(rule.enabled != ENABLED) {
        throw Exception(Message() << Message("Cannot register card of this type").What() << ": " << identifier_rulename.get_value_or(string()));
    }
}

int64_t CreateUser(Connection* rdb, const Str& fullname, const Str& email, const Str& password, const Str& password2, int groupid) {
    Value groups;
    groups[0] = static_cast<int64_t>(groupid);
    Value res = AUTHORIZER::AUTHORIZERUsers::Add(Int64(), fullname, email, password, password2, Str("enabled"), groups);
    const auto tokenid = res["TokenID"].As<int64_t>();
    return tokenid;
}

int64_t CreateUser(Connection* rdb, const Person& person, const Str& password, const Str& password2, int groupid) {
    return CreateUser(rdb, GetFullname(person), person.email, password, password2, groupid);
}

void CheckFields(Connection* rdb, const Str& email, const Str& password, const Str& password2) {
    if (!Defined(email) || !Defined(password) || !Defined(password2)) {
        throw Exception(Message("All fields are mandatory. ").What());
    }
    if (*password != *password2) {
        throw Exception(Message("Passwords are not equal. ").What());
    }
    if (loginExists(rdb, email)) {
        throw Exception(Message("Given login already registered. ").What());
    }
}

const int CLIENT_GROUPID(100);
const int MANAGER_GROUPID(20000);
const int PARTNER_ADMIN_GROUPID(2);

SpdGate GetDefaultGate(Connection* rdb) {
    SpdGate gate;
    Selector sel;
    sel << gate;
    sel.Where(gate->enabled == ENABLED);
    sel.OrderDesc(gate->id);
    sel.Limit(1);
    auto ds = sel.Execute(rdb);
    ds.Fetch();
    return gate;
}

std::vector<SpdGate> GetAllEnabledGates(Connection* rdb) {
    SpdGate gate;
    Selector sel;
    sel << gate;
    sel.Where(gate->enabled == ENABLED);
    sel.OrderDesc(gate->id);
    auto ds = sel.Execute(rdb);
    vector<SpdGate> gates;
    while(ds.Fetch()) {
        gates.push_back(gate);
    }
    return gates;
}

const Value BALOONPushNotification::DeviceRegisterApple_FE(const Str token, const Str first_name, const Str last_name, const Str middle_name, const Str description)
{
    if ( !Defined(token) ) {
	throw Exception(Message("Mobile token not defined.").What());
    }

    if ( !Defined(first_name) ) {
	throw Exception(Message("Field first_name not defined.").What());
    }

    if ( !Defined(last_name) ) {
	throw Exception(Message("Field last_name not defined.").What());
    }

    try {

        std::string _token(token.get_value_or(string()));
	std::string _first_name(first_name.get_value_or(string()));
        std::string _last_name(last_name.get_value_or(string()));
	std::string _middle_name(middle_name.get_value_or(string()));
        std::string _description(description.get_value_or(string()));

	PushNotification notify;

        Selector sel;
	sel << notify;
        sel.Where(notify->token == _token && notify->token_type == 100);

	auto ds = sel.Execute(rdb_);

        uint count = 0;

	while(ds.Fetch()) {
	    count = 1;
	    break;
        }

	if ( count > 0 )
	{
    	    try {
		notify.first_name = _first_name;
		notify.last_name = _last_name;
		notify.middle_name = _middle_name;
                notify.description = _description;

		notify.last_register = TIME_NOW;

		notify.Update(rdb_);

		Value res;
		res["id"] = notify.id;
		res["enabled"] = notify.enabled;

		return res;
	    }
    	    catch(const Exception& e) {
		throw e;
    	    }
	}
        else if ( count == 0 )
	{
    	    try {
		notify.token = _token;
		notify.token_type = 100;

		notify.first_name = _first_name;
		notify.last_name = _last_name;
		notify.middle_name = _middle_name;
        	notify.description = _description;

        	notify.enabled = ENABLED;

		notify.valid_from = TIME_NOW;
		notify.last_register = TIME_NOW;

		auto id = notify.Insert(rdb_);

		Value res;
		res["id"] = id;
		res["enabled"] = notify.enabled;

		return res;
	    } catch( const Exception& e ) {
		throw e;
    	    }
	}
	throw Exception(Message("Internal server error.").What());
    }
    catch(const Exception& e) {
	Log(5) << e.What() << std::endl;
	throw Exception(Message("Could not add token identifier.").What());
    }
    catch(...) {
	throw Exception(Message("Internal server error.").What());
    }
}

const Value BALOONPushNotification::DeviceRegisterAndroid_FE(const Str token, const Str first_name, const Str last_name, const Str middle_name, const Str description)
{
    if( !Defined(token) ) {
	throw Exception(Message("Mobile token not defined.").What());
    }

    if ( !Defined(first_name) ) {
	throw Exception(Message("Field first_name not defined.").What());
    }

    if ( !Defined(last_name) ) {
	throw Exception(Message("Field last_name not defined.").What());
    }

    try {

        std::string _token(token.get_value_or(string()));
	std::string _first_name(first_name.get_value_or(string()));
        std::string _last_name(last_name.get_value_or(string()));
	std::string _middle_name(middle_name.get_value_or(string()));
        std::string _description(description.get_value_or(string()));

	PushNotification notify;

        Selector sel;
	sel << notify;
        sel.Where(notify->token == _token && notify->token_type == 101);

	auto ds = sel.Execute(rdb_);

        uint count = 0;

	while(ds.Fetch()) {
	    count = 1;
	    break;
        }

	if ( count > 0 )
        {
	    try {
	        notify.first_name = _first_name;
		notify.last_name = _last_name;
	        notify.middle_name = _middle_name;
        	notify.description = _description;

		notify.last_register = TIME_NOW;

		notify.Update(rdb_);

		Value res;

		res["id"] = notify.id;
		res["enabled"] = notify.enabled;

		return res;

	    } catch( const Exception& e ) {
		throw e;
    	    }
	}
	else if ( count == 0 )
	{
    	    try {
		notify.token = _token;
		notify.token_type = 101;

		notify.first_name = _first_name;
		notify.last_name = _last_name;
		notify.middle_name = _middle_name;
        	notify.description = _description;

        	notify.enabled = ENABLED;

		notify.valid_from = TIME_NOW;
		notify.last_register = TIME_NOW;

		auto id = notify.Insert(rdb_);

		Value res;
		res["id"] = id;
		res["enabled"] = notify.enabled;

		return res;

	    }
    	    catch(const Exception& e) {
		throw e;
    	    }
	}
	throw Exception(Message("Internal server error.").What());
    }
    catch(const Exception& e) {
	Log(5) << e.What() << std::endl;
	throw Exception(Message("Could not add token identifier.").What());
    }
    catch(...) {
	throw Exception(Message("Internal server error.").What());
    }
}

const Value BALOONPushNotification::DeviceRegisterWindows_FE(const Str token, const Str first_name, const Str last_name, const Str middle_name, const Str description )
{
    if( !Defined( token ) ) {
	throw Exception(Message("Mobile token not defined.").What());
    }

    if ( !Defined(first_name) ) {
	throw Exception(Message("Field first_name not defined.").What());
    }

    if ( !Defined(last_name) ) {
	throw Exception(Message("Field last_name not defined.").What());
    }

    try {

        std::string _token(token.get_value_or(string()));
	std::string _first_name(first_name.get_value_or(string()));
        std::string _last_name(last_name.get_value_or(string()));
	std::string _middle_name(middle_name.get_value_or(string()));
        std::string _description(description.get_value_or(string()));

	PushNotification notify;

        Selector sel;
	sel << notify;
        sel.Where(notify->token == _token && notify->token_type == 102);

	auto ds = sel.Execute(rdb_);

        uint count = 0;

	while(ds.Fetch()) {
	    count = 1;
	    break;
        }

	if ( count > 0 ) {
	    try {
	        notify.first_name = _first_name;
		notify.last_name = _last_name;
		notify.middle_name = _middle_name;
		notify.description = _description;

		notify.last_register = TIME_NOW;

		notify.Update( rdb_ );

		Value res;
		res["id"] = notify.id;
		res["enabled"] = notify.enabled;

		return res;
	    }
	    catch(const Exception& e) {
		throw e;
    	    }
	}
	else if ( count == 0 ) {
    	    try {
		notify.token = _token;
		notify.token_type = 102;

		notify.first_name = _first_name;
		notify.last_name = _last_name;
		notify.middle_name = _middle_name;
        	notify.description = _description;

        	notify.enabled = ENABLED;

		notify.valid_from = TIME_NOW;
		notify.last_register = TIME_NOW;

		auto id = notify.Insert(rdb_);

		Value res;
		res["id"] = id;
		res["enabled"] = notify.enabled;

		return res;
	    }
	    catch( const Exception& e ) {
		throw e;
	    }
	}
	throw Exception(Message("Internal server error.").What());
    }
    catch( const Exception& e ) {
	Log(5) << e.What() << std::endl;
	throw Exception(Message("Could not add token identifier.").What());
    }
    catch( ... ) {
	throw Exception(Message("Internal server error.").What());
    }
}

const Value BALOONPushNotification::Delete(const Int id)
{
	try {
	    PushNotification notify;
	
	    Deleter del(notify);
	    del.Where(notify->id == id);

	    del.Execute(rdb_);
	    return Value();
	}
        catch( const Exception& e ) {
	    Log(5) << e.What() << std::endl;
	    throw Exception(Message("Could not delete record by given id.").What());
        }
	catch( ... ) {
    	    throw Exception(Message("Internal server error").What());
	}
}

const Value BALOONPushNotificationMessages::PushNotificationMessageAdd_FE( const Str title, const Str message, const Str enabled )
{
	if( !Defined( title ) ) {
	    throw Exception(Message("Field title not defined.").What());
	}

	if ( !Defined( message ) ) {
	    throw Exception(Message("Field message not defined.").What());
	}

	try {
		std::string _title(title.get_value_or(string()));
		std::string _message(message.get_value_or(string()));
		std::string _enabled(enabled.get_value_or(string()));

		PushNotificationMessages notify_msg;

		notify_msg.title = _title;
		notify_msg.message = _message;

		notify_msg.enabled = _enabled;
		notify_msg.sent = DISABLED;

		notify_msg.message_date = TIME_NOW;

		auto id = notify_msg.Insert(rdb_);

		Value res;
		res["id"] = id;
		res["enabled"] = notify_msg.enabled;

		return res;
	}
        catch( const Exception& e ) {
		Log(5) << e.What() << std::endl;
		throw Exception(Message("Could not add by given fields.").What());
        }
	catch( ... ) {
		throw Exception(Message("Internal server error.").What());
	}
}

const Value BALOONPushNotificationMessages::PushNotificationMessageUpdate_FE( const Int id, const Str title, const Str message, const Str enabled )
{
	if( !Defined( title ) ) {
	    throw Exception(Message("Field title not defined.").What());
	}

	if ( !Defined( message ) ) {
	    throw Exception(Message("Field message not defined.").What());
	}

	try {
		std::string _title(title.get_value_or(string()));
		std::string _message(message.get_value_or(string()));
		std::string _enabled(enabled.get_value_or(string()));

		PushNotificationMessages notify_msg;

		Selector sel;

		sel << notify_msg;
		sel.Where(notify_msg->id == id);

    		auto ds = sel.Execute(rdb_);

		uint count = 0;
		while(ds.Fetch()) {
			count = 1;
			break;
		}

		if ( count > 0 ) {
			notify_msg.title = _title;
			notify_msg.message = _message;

			notify_msg.enabled = _enabled;
			notify_msg.message_date = TIME_NOW;

			notify_msg.Update(rdb_);

			Value res;

			res["id"] = notify_msg.id;
			res["enabled"] = notify_msg.enabled;

			return res;
		}

		throw Exception(Message("Could not update by given id field.").What());
	}
        catch( const Exception& e ) {
		Log(5) << e.What() << std::endl;
		throw Exception(Message("Could not update by given fields.").What());
        }
	catch( ... ) {
		throw Exception(Message("Internal server error.").What());
	}
}

#if 0

const Value BALOONPushNotificationMessages::Delete( const Int id ) 
{
	try {
		PushNotificationMessages notify_msg;
	
		Deleter del(notify_msg);
		del.Where(notify_msg->id == id);

		del.Execute(rdb_);

		return Value();
	}
        catch( const Exception& e ) {
		Log(5) << e.What() << std::endl;
		throw Exception(Message("Could not delete record by given id.").What());
        }
	catch( ... ) {
		throw Exception(Message("Internal server error.").What());
	}
}

#endif

#if 0

const Value BALOONPushNotificationMessages::Get( const Int id ) 
{
	try {
		PushNotificationMessages notify_msg;
	
		Selector sel;
		sel << notify_msg;

		sel.Where(notify_msg->id == id);
		sel.Limit(1);

		Value res;

		auto ds = sel.Execute(rdb_);
		while( ds.Fetch() )
		{
		    res["id"] = notify_msg.id;
		    res["title"] = notify_msg.title;
		    res["message"] = notify_msg.message;
		    res["sent"] = notify_msg.sent;
		    res["enabled"] = notify_msg.enabled;
		    res["message_date"] = notify_msg.message_date;

		    break;
		}

		return res;
	}
        catch( const Exception& e ) {
		Log(5) << e.What() << std::endl;
		throw Exception(Message("Could not get record by given id.").What());
        }
	catch( ... ) {
		throw Exception(Message("Internal server error.").What());
	}
}

#endif

const Value BALOONPushNotificationMessages::SendNotifications( const Int id, const Str sent, const Str enabled ) 
{
	try {
		std::string _sent(sent.get_value_or(string()));
		std::string _enabled(enabled.get_value_or(string()));

		std::string _title;
		std::string _body;

		PushNotificationMessages notify_msg;
	
		Selector sel;
		sel << notify_msg;

		sel.Where(notify_msg->id == id &&
              			notify_msg->enabled == ENABLED &&
				notify_msg->sent == DISABLED);

		uint count = 0;
		auto ds = sel.Execute(rdb_);
		while(ds.Fetch()) {
			count = 1;
			break;
		}

		if ( count > 0 ) {
			const Str title = notify_msg.title;
			const Str body = notify_msg.message;
			
			notify_msg.sent = ENABLED;
			notify_msg.message_date = TIME_NOW;

			/* action */

			_title = 		b64encode(std::string(title.get_value_or(std::string())));
			_body = 		b64encode(std::string(body.get_value_or(std::string())));

			char shell_line[ 2048 ];

			::memset( shell_line, 0, sizeof( shell_line ) );
			sprintf( shell_line, "notification-id%u", id.get() );

			std::string		_tokens_file_name( shell_line + std::string(".tokens") );
			std::string		_body_file_name( shell_line + std::string(".body") );
			std::string		_title_file_name( shell_line + std::string(".title") );

			::memset( shell_line, 0, sizeof( shell_line ) );
			sprintf( shell_line, "echo %s >> %s", _title.c_str(), _title_file_name.c_str() );
			::system( shell_line );

			::memset( shell_line, 0, sizeof( shell_line ) );
			sprintf( shell_line, "echo %s >> %s", _body.c_str(), _body_file_name.c_str() );
			::system( shell_line );

    			PushNotification notify;

			Selector sel;
			sel << notify;
			sel.Where(notify->enabled == ENABLED);

			FILE* f = fopen(_tokens_file_name.c_str(), "w+");

			auto ds = sel.Execute(rdb_);
			while(ds.Fetch()) {

			    std::string 	_token(notify.token.get_value_or(string()));
			    unsigned int 	_token_type(notify.token_type.get());

   			    std::string 	_first_name(notify.first_name.get_value_or(string()));
    			    std::string 	_last_name(notify.last_name.get_value_or(string()));
			    std::string 	_middle_name(notify.middle_name.get_value_or(string()));

			    std::string 	_description(notify.description.get_value_or(string()));
			
			    unsigned int 	deviceid(notify.id.get());
			    unsigned int 	messageid(id.get());

			    ::memset( shell_line, 0, sizeof( shell_line ) );

			    char* token = const_cast<char*>(_token.c_str());
			    char* destination = 0;

			    if ( _token_type == 102 ) {
				destination = const_cast<char*>("windows");
			    } else if ( _token_type == 100 ) {
				destination = const_cast<char*>("apple");
			    } else if (_token_type == 101 ) {
				destination = const_cast<char*>("android");
			    }

			    if ( token != 0 ) {
				fprintf( f, "%s %u %u %s\n", token, deviceid, messageid, destination );
			    }
			}
			
			::memset( shell_line, 0, sizeof( shell_line ) );
			sprintf( shell_line, "./process.sh \"%s\" \"%s\" \"%s\" &", _tokens_file_name.c_str(), _title_file_name.c_str(), _body_file_name.c_str() );

			::system( shell_line );

			/* action */

			notify_msg.Update(rdb_);

			Value res;
			res["sent"] = ENABLED;
			return res;
		}

		Value res;
		res["sent"] = _sent;
		return res;
	}
        catch( const Exception& e ) {
		Log(5) << e.What() << std::endl;
		throw Exception(Message("Could not notificate record by given id.").What());
        }
	catch( ... ) {
		throw Exception(Message("Internal Server Error").What());
	}
}


const Value BALOONPushNotificationMessages::SendAllNotifications()
{
    try {
	PushNotificationMessages notify_msg;
	
	Selector sel;
	sel << notify_msg;
	sel.Where( notify_msg->enabled == ENABLED &&
		notify_msg->sent == DISABLED);

	auto ds = sel.Execute(rdb_);

	while(ds.Fetch()) {

	    const Int id = notify_msg.id;
	    const Str sent = notify_msg.sent;
	    const Str enabled = notify_msg.enabled;

	    SendNotifications( id, sent, enabled );
	}

	return Value();
    }
    catch( const Exception& e ) {
	Log(5) << e.What() << std::endl;
	throw Exception(Message("Could not notificate records.").What());
    }
    catch( ... ) {
	throw Exception(Message("Internal Server Error").What());
    }
} 


const Value BALOONPushNotificationDeliveries::ResendNotification( const Int id )
{
    try {
	PushNotificationDeliveries notify_delivery;
	
	Selector sel;

	sel << notify_delivery;
	sel.Where(notify_delivery->id == id);

	auto ds = sel.Execute(rdb_);

	Int deviceid = 0;
	Int messageid = 0;

	uint count = 0;

	while(ds.Fetch()) {
	    deviceid = notify_delivery.deviceid;
	    messageid = notify_delivery.messageid;

	    count = 1;
	    break;
	}

	if ( count > 0 ) {

	    PushNotificationMessages notify_msg;
	
	    Selector sel;
	    sel << notify_msg;
	    sel.Where(notify_msg->id == messageid);

	    count = 0;

	    auto ds = sel.Execute(rdb_);
	    while(ds.Fetch()) {
	    	count = 1;
		break;
	    }

	    if ( count > 0 ) {

	        const Str title = notify_msg.title;
	        const Str message = notify_msg.message;

		std::string _title;
		std::string _body;

		_title = b64encode(std::string(title.get_value_or(string())));
		_body = b64encode(std::string(message.get_value_or(string())));

		PushNotification notify;

		Selector sel;
		sel << notify;
		sel.Where(notify->id == deviceid);

		::system( "truncate --size 0 tokens.list" );
		::system( "truncate --size 0 notification.title" );
		::system( "truncate --size 0 notification.body" );

		char shell_line[ 2048 ];

		memset( shell_line, 0, sizeof( shell_line ) );
		sprintf( shell_line, "echo %s >> notification.title", _title.c_str() );

		::system( shell_line );

		memset( shell_line, 0, sizeof( shell_line ) );
		sprintf( shell_line, "echo %s >> notification.body", _body.c_str() );

		::system( shell_line );

		count = 0;

		auto ds = sel.Execute(rdb_);
		while(ds.Fetch()) 
		{
		    std::string 	_token(notify.token.get_value_or(string()));
		    unsigned int 	_token_type(notify.token_type.get());

   		    std::string 	_first_name(notify.first_name.get_value_or(string()));
		    std::string 	_last_name(notify.last_name.get_value_or(string()));
		    std::string 	_middle_name(notify.middle_name.get_value_or(string()));
		    std::string 	_description(notify.description.get_value_or(string()));
			
		    unsigned int 	_deviceid (deviceid.get());
		    unsigned int 	_messageid (messageid.get());

		    ::memset( shell_line, 0, sizeof( shell_line ) );

		    char* token = const_cast<char*>(_token.c_str());
		    char* destination = 0;

		    if ( _token_type == 102 ) {
			destination = const_cast<char*>("windows");
		    } else if (_token_type == 100 ) {
			destination = const_cast<char*>("apple");
		    } else if (_token_type == 101 ) {
			destination = const_cast<char*>("android");
		    } 

		    if ( token != 0 ) {		
			sprintf( shell_line, "echo %s %u %u %s >> tokens.list", token, _deviceid, _messageid, destination );
			::system( shell_line );
		    }

		    break;
		}
			
		::system( "./process.sh tokens.list notification.title notification.body &" );

		Value res;
		res["id"] = id;
		return res;
	    }
	}

	Value res;
	res["id"] = -1;
	return res;
    } 
    catch( const Exception& e ) {
	Log(5) << e.What() << std::endl;
	throw Exception(Message("Could not notificate record by given id.").What());
    }
    catch( ... ) {
	throw Exception(Message("Internal Server Error").What());
    }
}

const Value BALOONPushNotificationDeliveries::PushNotificationDeliveryAdd_FE( const Int messageid, const Int deviceid, const Str sendresult ) 
{
	try {
		PushNotificationDeliveries notify_delivery;

		notify_delivery.messageid = messageid;
		notify_delivery.deviceid = deviceid;
		notify_delivery.sendresult = sendresult;
		notify_delivery.delivery_date = TIME_NOW; // + Computerica::get_utc_offset();

		auto id = notify_delivery.Insert(rdb_);

		Value res;
		res["id"] = id;
		return res;
	}
        catch( const Exception& e ) {
		Log(5) << e.What() << std::endl;
		throw Exception(Message("Could not delete by given id.").What());
        }
	catch( ... ) {
		throw Exception(Message("Internal server error.").What());
	}
}


const Value BALOONPerson::Register_API(const Str regcode, const Str email, const Str password, const Str password2, const Str phone) {
    if(!Computerica::isEmail(email)) {
        throw Exception(Message("Email is not correct. ").What());
    }
    CheckFields(rdb_, email, password, password2);
    SpdClient send_client;
    const auto with_regcode(Defined(regcode) && !regcode->empty());
    if(with_regcode) {
        send_client.identifier = convertPrintCodeToSpd(*regcode); 
    }
    else {
        send_client.is_new = true;
    }
    const auto gate = GetDefaultGate(rdb_);
    
    send_client.spd_host = gate.host;
    send_client.spd_port = gate.port;
    
    const auto response = SendSpdRequest(send_client);
    if (response.clients.empty()) {
        throw Exception(Message("Client not found.").What());
    }

    SpdClient client = response.clients[0];
    checkPersonOid(rdb_, client.oid);

    Person person;
    person.last_name = client.last_name;
    person.first_name = client.first_name;
    person.middle_name = client.middle_name;
    person.phone = client.phone;
    person.birthdate = client.birthdate;
    person.regcode = regcode;
    person.phone = phone;
    person.email = email;
    person.last_sync = TIME_NOW;
    person.oid = client.oid;
    person.userTokenID = CreateUser(rdb_, person, password, password2, CLIENT_GROUPID);
    person.registered_at = (TIME_NOW /* + Computerica::get_utc_offset() */ ).date();
    const auto personid = person.Insert(rdb_);

    if(with_regcode) {
        BindIdentifier(rdb_, client, personid);
    }

    Value res;
    res["id"] = personid;
    return res;
}
//------------------------------------------------------------------------------
/// Зарегистрировать нового клиента
const Value BALOONPerson::RegisterNewClient_API(const Str regcode, const Str email, const Str password, const Str password2, const Str phone, const Str last_name, const Str first_name, const Str middle_name, const ADate birthdate, const Str gender) 
{
	if(!Computerica::isEmail(email)) {
		throw Exception(Message("Email is not correct.").What());
	}

	CheckFields(rdb_, email, password, password2);

	const auto _email = NormalizeEmail(email);

	std::string _regcode(regcode.get_value_or(string()));

	bool with_regcode = false;

	if ( Defined( regcode ) ) 
	{
		with_regcode = true; 
    	
		if ( _regcode.size() != 8 ) 
		{
			throw Exception(Message("Field regcode length is not correct.").What());    	
		}
	    
		for ( int i = 0; i < (int)_regcode.size(); i++ ) 
		{
        		if ( ::isdigit( _regcode.c_str()[i] ) == 0 ) 
			{
		        	throw Exception(Message("Field regcode is not only digits.").What());    	
			}
	        }
	}

	SpdClient send_client;

	if( with_regcode ) 
	{
		send_client.identifier = convertPrintCodeToSpd(*regcode); 
	} 
	else 
	{
/*
		if (Defined(last_name)) {
			send_client.last_name = last_name.get(); }

		if (Defined(first_name)) {
			send_client.first_name = first_name.get(); }

		if (Defined(middle_name)) {
			send_client.middle_name = middle_name.get(); }

		if (Defined(phone)) {
			send_client.phone = phone.get(); }

		send_client.birthdate = birthdate;
*/
	        send_client.is_new = true;
	}

	const auto gate = GetDefaultGate(rdb_);
    
	send_client.spd_host = gate.host;
	send_client.spd_port = gate.port;

	auto response = SendSpdRequest(send_client);

	if ( response.clients.empty() ) 
	{
		with_regcode = false; 

		SpdClient send_any;
/*
		if (Defined(last_name)) {
			send_any.last_name = last_name.get(); }
		if (Defined(first_name)) {
			send_any.first_name = first_name.get(); }
		if (Defined(middle_name)) {
			send_any.middle_name = middle_name.get(); }
		if (Defined(phone)) {
			send_any.phone = phone.get(); }

		send_any.birthdate = birthdate;
*/
	        send_any.is_new = true;

		send_any.spd_host = gate.host;
	        send_any.spd_port = gate.port;

		response = SendSpdRequest(send_any);
	}

	if ( response.clients.empty() ) {
		throw Exception( Message( "Client not found." ).What() );
	}

	SpdClient client = response.clients[0];

	Person person;
	Selector sel;

	sel << person->id;
	sel.Where(person->oid == client.oid);

	DataSet data = sel.Execute(rdb_);
	if( data.Fetch() ) {
/*
		Value res;

		res["id"] = person.id;
		res["login"] = _email;
		
		return res;
*/
    		throw Exception(Message("Account with given code already registered. ").What());
	}

//========================================================
//    checkPersonOid(rdb_, client.oid);
//========================================================

	Value res;

	person.email = _email;

	if( with_regcode ) {
		person.regcode = _regcode; } //???

	if ( Defined( phone ) ) {
		person.phone = phone; }

	if ( Defined( first_name ) ) {
		person.first_name = first_name; }

	if ( Defined( middle_name ) ) {
		person.middle_name = middle_name; }

	if ( Defined( last_name ) ) {
		person.last_name = last_name; }

	if ( Defined( gender ) ) {
		person.gender = gender; }

	person.birthdate = birthdate;
    
	person.last_sync = TIME_NOW;
	person.oid = client.oid;

	person.userTokenID = CreateUser(rdb_, person, password, password2, CLIENT_GROUPID);
	person.registered_at = (TIME_NOW).date();
		
	const auto personid = person.Insert(rdb_);

	if( with_regcode ) {
		BindIdentifier(rdb_, client, personid);
	}

	res["id"] = personid;
	res["login"] = _email;

	return res;
}
//------------------------------------------------------------------------------
const Value BALOONPerson::CreateNewClient_API(const Str email, const Str password, const Str password2, const Str last_name, const Str first_name, const Str middle_name, const ADate birthdate, const Str gender) {
    const auto n_email = NormalizeEmail(email);
    CheckFields(rdb_, n_email, password, password2);
    Person person;
    person.last_name = last_name;
    person.first_name = first_name;
    person.middle_name = middle_name;
    person.email = n_email;
    person.birthdate = birthdate;
    person.gender = gender;
    person.userTokenID = CreateUser(rdb_, person, password, password2, CLIENT_GROUPID);
    const auto personid = person.Insert(rdb_);
    Value res;
    res["id"] = personid;
    res["login"] = n_email;
    return res;
}

Int CreateOrFindUser(Connection* rdb, const Str& email, const Str& last_name, const Str& first_name) {
    const auto norm_email = NormalizeEmail(email);
    Person person;
    Selector sel;
    sel << person;
    sel.Where(person->email == norm_email);
    sel.Limit(1);
    auto ds = sel.Execute(rdb);
    if(ds.Fetch()) {
        person.last_name = last_name;
        person.first_name = first_name;
        person.Update(rdb);
        return person.id;
    }
    person.last_name = last_name;
    person.first_name = first_name;
    person.email = norm_email;
    
    const auto UPPER(true);
    const auto LOWER(true);
    const auto PWDLENGTH(4);
    
    const auto password = Computerica::randomCode(PWDLENGTH, UPPER, !LOWER);
    person.userTokenID = CreateUser(rdb, person, password, password, CLIENT_GROUPID);
    const auto personid = person.Insert(rdb);
    return personid;
}

const Value BALOONSpdServiceRule::SpdServiceRuleMenuListGet() {
    return BALOONSpdServiceRule::SpdServiceRuleListGet();
}
const Value BALOONIdentifierRule::IdentifierRuleMenuListGet() {
    return BALOONIdentifierRule::IdentifierRuleListGet();
}
const Value BALOONIdentifierCategory::IdentifierCategoryMenuListGet() {
    return BALOONIdentifierCategory::IdentifierCategoryListGet();
}
const Value BALOONIdentifierRate::IdentifierRateMenuListGet() {
    return BALOONIdentifierRate::IdentifierRateListGet();
}

template<class T>
const Value SpdNameEnabledListGet(Connection *rdb) {
	Data::DataList lr;
	lr.AddColumn("name", Data::STRING);
	lr.AddColumn("extid", Data::INTEGER);
	lr.AddColumn("extcode", Data::INTEGER);
	lr.AddColumn("comment", Data::STRING);
	T obj;
	lr.Bind(obj.name, "name");
	lr.Bind(obj.extid, "extid");
	lr.Bind(obj.extcode, "extcode");
	lr.Bind(obj.comment, "comment");
	Selector sel;
	sel << obj;
	sel.Where(obj->enabled == ENABLED);
	sel.Where();
	DataSet data=sel.Execute(rdb);
	while(data.Fetch()) lr.AddRow();
	Value res=lr;
	return res;
}
const Value BALOONSpdServiceRule::SpdServiceRuleEnabledListGet() {
    return SpdNameEnabledListGet<SpdServiceRule>(rdb_);
}
const Value BALOONIdentifierRule::IdentifierRuleEnabledListGet() {
    return SpdNameEnabledListGet<IdentifierRule>(rdb_);
}
const Value BALOONIdentifierCategory::IdentifierCategoryEnabledListGet() {
    return SpdNameEnabledListGet<IdentifierCategory>(rdb_);
}
const Value BALOONIdentifierRate::IdentifierRateEnabledListGet() {
    return SpdNameEnabledListGet<IdentifierRate>(rdb_);
}

template<class T>
const Value NameListGet_API(Connection *rdb) {
    T obj;
    Selector sel;
    sel << obj->name << obj->enabled;
    sel.Where(obj->enabled == ENABLED);
    DataSet data = sel.Execute(rdb);

    Value res;
    int32_t i=0;
    while (data.Fetch()) {
        Value r;
        r["name"] = obj.name.get_value_or("");
        r["enabled"] = obj.enabled.get_value_or(ENABLED);
        res[i++] = r;
    }
    return res;
}

/// Получаем список услуг из СПД
/// Передаем в во внешние источники (мою.приложения и т.п.)
const Value BALOONSpdServiceRule::SpdServiceRuleListGet_API(const Str identifier_rulename) {
    SpdServiceRule service_rule;    /// "Правила оформления"=Услуги в Контуре
    IdentifierServiceRule is_rule;  /// Настройки правил оформления
    
    Selector sel;
    sel << service_rule->name
        << service_rule->enabled
        << service_rule->package_cost;
    sel.From(service_rule).Join(is_rule).On(is_rule->service_rulename == service_rule->name); /// Склеиваем по имени сервиса
    sel.Where(service_rule->enabled == ENABLED &&
              is_rule->enabled == ENABLED &&
              is_rule->identifier_rulename == identifier_rulename);
    auto ds = sel.Execute(rdb_);
    Value res;
    int32_t i=0;
    while (ds.Fetch()) {
        Value entry;
        entry["name"] = service_rule.name.get_value_or("");
        entry["enabled"] = service_rule.enabled.get_value_or(ENABLED);
        entry["package_cost"] = service_rule.package_cost;
        res[i] = entry;
        ++i;
    }
    return res;
}
const Value BALOONIdentifierRule::IdentifierRuleListGet_API() {
    return NameListGet_API<IdentifierRule>(rdb_);
}
const Value BALOONIdentifierCategory::IdentifierCategoryListGet_API() {
    return NameListGet_API<IdentifierCategory>(rdb_);
}
const Value BALOONIdentifierRate::IdentifierRateListGet_API() {
    return NameListGet_API<IdentifierRate>(rdb_);
}

template<class T>
map<string, T> getDictionaryMap(Connection *rdb) {
    T table;
    Selector sel;
    sel << table;
    DataSet data = sel.Execute(rdb);
    
    map<string, T> res;
    while (data.Fetch()) {
        res[table.name.get_value_or("")] = table;
    }
    return res;
}

template<class T>
void syncDictionary(Connection *rdb, string name) {

    string request =
#ifdef USE_UTF_8
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
#else
        "<?xml version=\"1.0\" encoding=\"Windows-1251\"?>"
#endif
        "<spd-xml-api>"
        "   <request version=\"1.0\" ruid=\"" + to_upper(Effi::GenerateGUID()) + "\">"
        "      <dictionaries>"
        "         <dictionary name=\"" + name + "\"/>"
        "      </dictionaries>"
        "   </request>"
        "</spd-xml-api>";
        
    Log(5) << "DICT REQUEST: " << request << endl;
    const auto gate = GetDefaultGate(rdb);
    
#ifdef USE_UTF_8
    string reply = SendSpdRequest(request, gate.host, gate.port);
#else    
    string reply = SendSpdRequest(utf8_to_win1251_dyn(request), gate.host, gate.port);
#endif

    Log(5) << "DICT REPLY: " << reply << endl;
    SpdResponse response = SpdResponse::parse(reply);

    map<string, T> existing_dictionary = getDictionaryMap<T>(rdb);

    vector<SpdDictionaryRecord> recs = response.dictionaries[name];
    vector<SpdDictionaryRecord>::iterator it = recs.begin();
    for (; it!=recs.end(); it++) {
        if (existing_dictionary.find(it->name) != existing_dictionary.end()) {
            T erecord = existing_dictionary[it->name];
            erecord.extid = it->id;
            erecord.extcode = it->code;
            erecord.comment = it->comment;
            erecord.Update(rdb);
        }
        else {
            T record;
            record.name = it->name;
            record.enabled = DISABLED;
            record.extid = it->id;
            record.extcode = it->code;
            record.comment = it->comment;
            record.Insert(rdb);
        }
    }
}

const Value BALOONSpdServiceRule::SyncDictionary() {
    //syncDictionary<SpdServiceRule>(rdb_, "Правила оформления");

    const string request(
#ifdef USE_UTF_8 
    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
#else
    "<?xml version=\"1.0\" encoding=\"Windows-1251\"?>"
#endif
    "<spd-xml-api>"
      "<request version=\"1.0\" ruid=\"" + to_upper(Effi::GenerateGUID()) + "\">"
        "<dictionaries>"
          "<dictionary name=\"Правила оформления\"/>"
        "</dictionaries>"
      "</request>"
    "</spd-xml-api>");
    
    const auto gate = GetDefaultGate(rdb_);

#ifdef USE_UTF_8 
    const auto reply = SendSpdRequest(request, gate.host, gate.port);
#else
    const auto reply = SendSpdRequest(Computerica::utf8_to_win1251(request), gate.host, gate.port);
#endif

    const auto response = SpdServiceRuleRecord::parse(reply);    
    auto existing_dictionary = getDictionaryMap<SpdServiceRule>(rdb_);
    
    for(const auto& rec : response) {
        auto iter = existing_dictionary.find(rec.name);
        if(iter != existing_dictionary.cend()) {
            auto& rule = iter->second;
            rule.extid = rec.id;
            rule.extcode = rec.code;
            rule.comment = rec.comment;
            rule.package_cost = rec.cost;
            rule.Update(rdb_);
        }
        else {
            SpdServiceRule rule;
            rule.name = rec.name;
            rule.extid = rec.id;
            rule.extcode = rec.code;
            rule.comment = rec.comment;
            rule.package_cost = rec.cost;
            rule.enabled = DISABLED;
            rule.Insert(rdb_);
        }
    }
    return Value();
}

const Value BALOONIdentifierRule::SyncDictionary() {
    syncDictionary<IdentifierRule>(rdb_, "Правила пользования");
    return Value();
}
const Value BALOONIdentifierCategory::SyncDictionary() {
    syncDictionary<IdentifierCategory>(rdb_, "Категории счетов");
    return Value();
}
const Value BALOONIdentifierRate::SyncDictionary() {
    syncDictionary<IdentifierRate>(rdb_, "Тарифы");
    return Value();
}

void UncheckIdentifierCategoriesAsDefault(Connection* rdb, const Str& default_category) {
    IdentifierCategory cat;
    Updater upd(cat);
    upd << cat->is_default(BOOL_FALSE);
    upd.Where(cat->name != default_category);
    upd.Execute(rdb);
}

const Value BALOONIdentifierCategory::Update(const Str name, const optional<Str> enabled, const optional<Str> comment, const optional<Str> is_default) {
    IdentifierCategory cat(name);
    cat.Select(rdb_);
    if(Defined(enabled)) cat.enabled = *enabled;
    if(Defined(comment)) cat.comment = *comment;
    if(Defined(is_default)) {
        if(*is_default == BOOL_TRUE) {        
            UncheckIdentifierCategoriesAsDefault(rdb_, name);
        }
        cat.is_default = *is_default;
    }
    cat.Update(rdb_);
    AddMessage(Message("Identifier category updated. ").What());
    return Value();
}

const Value BALOONIdentifierServiceRule::IdentifierServiceRuleListGet_API() {
    IdentifierServiceRule isrule;
    SpdServiceRule service_rule;
    Selector sel;
    sel << isrule;
    sel.Where(isrule->enabled == ENABLED);
    auto ds = sel.Execute(rdb_);
    Value res;
    uint i = 0;
    while(ds.Fetch()) {
        Value item;
        item["identifier_rulename"] = isrule.identifier_rulename;
        item["service_rulename"] = isrule.service_rulename;
        item["name"] = isrule.name;
        item["enabled"] = isrule.enabled;
        item["weekday_cost"] = isrule.weekday_cost;
        item["weekend_cost"] = isrule.weekend_cost;
        res[i] = item;
        ++i;
    }
    return res;
}

const Value BALOONIdentifierServiceRule::Delete(const Str identifier_rulename, const Str service_rulename) {
    IdentifierServiceRule rule;
    Selector sel;
    sel << rule;
    const auto expression(rule->identifier_rulename == identifier_rulename && rule->service_rulename == service_rulename);
    sel.Where(expression);
    auto ds = sel.Execute(rdb_);
    if(!ds.Fetch()) {
        throw Exception(Message("Identifier service rule not found. ").What());
    }
    //TODO - save to arc table before deleting
    Deleter del(rule);
    del.Where(expression);
    del.Execute(rdb_);
    AddMessage(Message("Identifier service rule deleted. ").What());
    return Value();
}

const Value BALOONPerson::AddToContour(const Int id) {
    Person person(id);
    person.Select(rdb_);
    SpdClient client(true);
    // if (!person.oid.get_value_or("").empty()) {
    //     client.is_new = false;
    //     client.oid = person.oid.get_value_or("");
    // }
    
    client.last_name = person.last_name.get_value_or("");
    client.first_name = person.first_name.get_value_or("");
    client.middle_name = person.middle_name.get_value_or("");
    client.birthdate = person.birthdate;

    const auto gate = GetDefaultGate(rdb_);
    client.spd_host = gate.host;
    client.spd_port = gate.port;
    
    SpdResponse response = SendSpdRequest(client);
    if (response.clients.empty()) {
        throw Exception(Message("Не удалось добавить клиента в системе Контур. "));
    }

    person.oid = response.clients[0].oid;
    person.Update(rdb_);

    AddMessage(Message()<<"Client "<<person.email<<" saved to Contour. ");
    return Value();
}

Int getCurrentPersonId(Connection *rdb) {
    Value current_user = AUTHORIZER::AUTHORIZERUsers::GetCurrent();
    Int64 userid = current_user["TokenID"].As<Int64>();
    
    Person person;
    Selector sel;
    sel << person->id;
    sel.Where(person->userTokenID == userid);
    DataSet data = sel.Execute(rdb_);
    data.Fetch();
    return person.id;
}

/// Проверить и получить текущий ID клиента
Int checkAndGetCurrentPersonId(Connection *rdb) {
    Int personid = getCurrentPersonId(rdb);
    if (!Defined(personid)) {
        throw Exception(Message("Current user has no personal account. ").What());
    }
    return personid;
}

const Value BALOONPerson::GetCurrent() {
    Int personid = getCurrentPersonId(rdb_);

    Value current_user = AUTHORIZER::AUTHORIZERUsers::GetCurrent();
    Int64 userid = current_user["TokenID"].As<Int64>();

    Person person(personid);
    person.Select(rdb_);

    Value res;
    res["id"] = person.id;
    res["userTokenID"] = person.userTokenID;
    res["TokenName"] = current_user["TokenName"];
    res["last_name"] = person.last_name;
    res["first_name"] = person.first_name;
    res["middle_name"] = person.middle_name;
    res["birthdate"] = person.birthdate;
    res["gender"] = person.gender;
    res["phone"] = person.phone;
    res["email"] = person.email;
    res["regcode"] = person.regcode;
    return res;
}

const Value BALOONPerson::UpdateCurrent(const optional<Str> last_name, const optional<Str> first_name, const optional<Str> middle_name, const optional<ADate> birthdate, const optional<Str> gender, const optional<Str> phone) {
    Exception e((Message("Cannot update ") << Message("Client").What() << ". ").What());
    try {
        Int personid = checkAndGetCurrentPersonId(rdb_);

        Person aPerson;
        aPerson.id = personid;
        aPerson.Select(rdb_);
        if(Defined(last_name)) aPerson.last_name=*last_name;
        if(Defined(first_name)) aPerson.first_name=*first_name;
        if(Defined(middle_name)) aPerson.middle_name=*middle_name;
        if(Defined(birthdate)) aPerson.birthdate=*birthdate;
        if(Defined(gender)) aPerson.gender=*gender;
        if(Defined(phone)) aPerson.phone=*phone;
        aPerson.Update(rdb_);
        AddMessage(Message()<<Message("Client").What()<<" "<<aPerson.id<<" updated. ");
        return Value();
    }
    catch (Exception& exc) {
        if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
            e << Message("Duplicate entry. ").What();
        }
        else {
            e << Message(exc.what()).What();
        }
        throw e;
    }
}

const Value BALOONPerson::ChangeCurrentPassword(const Str old_password_, const Str password_, const Str password2_) {
    string old_password = old_password_.get_value_or(""),
           password = password_.get_value_or(""),
           password2 = password2_.get_value_or("");
    if (old_password.empty() || password.empty()) throw Exception(Message("All fields are mandatory. ").What());
    if (password != password2) throw Exception(Message("Passwords are not equal. ").What());
    
    Int personid = checkAndGetCurrentPersonId(rdb_);
    Person person(personid);
    person.Select(rdb_);
    AUTHORIZER::Users user(person.userTokenID);
    user.Select(rdb_);
    if (user.Password.get_value_or("") != old_password) {
         throw Exception(Message("Incorrent old password. ").What());
    }

    AUTHORIZER::AUTHORIZERUsers::Update(user.TenantID, user.TokenID, boost::none, user.Login, password_, password2_, boost::none, boost::none);
    AddMessage(Message("Password changed. ").What());
    return Value();
}

SpdResponse requestPersonFromSPD(const Int personid, bool request_transactions=false) {
    Person person(personid);
    person.Select(rdb_);
    if (!Defined(person.oid)) {
        throw Exception(Message("Given account is not synchronized to PAS. ").What());
    }
    SpdClient client;
    client.oid = person.oid.get_value_or("");
    if (request_transactions) {

    }
    const auto gate = GetDefaultGate(rdb_);
    client.spd_host = gate.host;
    client.spd_port = gate.port;
    SpdResponse response = SendSpdRequest(client, request_transactions);

    if (response.clients.size() == 0) {
        throw Exception(Message("Error loading data from PAS. ").What());
    }
    return response;
}

const Value BALOONPerson::GetCurrentAccounts() {
    Int personid = checkAndGetCurrentPersonId(rdb_);
    SpdResponse response = requestPersonFromSPD(personid);

    Value res;
    vector<SpdAccountCurrency>::iterator it = response.clients.at(0).account.currencies.begin();
    for (; it!=response.clients.at(0).account.currencies.end(); it++) {
        res[it->currency] = it->balance;
    }
    return res;
}


const Value BALOONPerson::ShowRegistration() {
    return Computerica::HtmlLayout().setTitle(Message("Personal account registraion").What()).render("register.html");
}

const Value BALOONPerson::ShowUserPanel() {
    return Computerica::HtmlLayout().setTitle(Message("Personal account").What()).render("user-panel.html");
}

const Value BALOONPerson::ShowTicketsShop() {
    return Computerica::HtmlLayout().setTitle(Message("Tickets shop").What()).render("tickets-shop.html");
}

const Value BALOONPerson::ShowSuccessDownloadPayment() {
    return Computerica::HtmlLayout().setTitle(Message("Tickets shop").What()).render("tickets-success-download.html");
}
const Value BALOONPerson::ShowSuccessPayment() {
    return Computerica::HtmlLayout().setTitle(Message("Tickets shop").What()).render("tickets-success.html");
}


const Value BALOONPerson::ShowSuccessPaymentIOS() {
    return Computerica::HtmlLayout().setTitle(Message("Tickets shop").What()).render("tickets-success-mobile.html");
}

const Value BALOONPerson::ShowFailedPayment() {
    return Computerica::HtmlLayout().setTitle(Message("Tickets shop").What()).render("tickets-fail.html");
}

const Value BALOONIdentifier::OurIdentifierListGet() {
    const auto partnerid = CurrentPartnerID(rdb_);
    Identifier identifier;
    PartnerOrderline oline;
    PartnerOrder order;
    Selector sel;
    sel << identifier->id
        << identifier->code
        << identifier->name
        << identifier->valid_from
        << identifier->valid_to
        << identifier->status
        << identifier->comment;
    sel.From(identifier)
        .Join(oline).On(oline->identifierid == identifier->id)
        .Join(order).On(oline->orderid == order->id);
    sel.Where(order->partnerid == partnerid);
    auto ds = sel.Execute(rdb_);
    Data::DataList lr;
    lr.AddColumn("id", Data::INTEGER);
    lr.AddColumn("code", Data::STRING);
    lr.AddColumn("name", Data::STRING);
    lr.AddColumn("valid_from", Data::DATETIME);
    lr.AddColumn("valid_to", Data::DATETIME);
    lr.AddColumn("status", Data::STRING);
    lr.AddColumn("comment", Data::STRING);
    
    lr.Bind(identifier.id, "id");
    lr.Bind(identifier.code, "code");
    lr.Bind(identifier.name, "name");
    lr.Bind(identifier.valid_from, "valid_from");
    lr.Bind(identifier.valid_to, "valid_to");
    lr.Bind(identifier.status, "status");
    lr.Bind(identifier.comment, "comment");
    
    while(ds.Fetch()) {
        lr.AddRow();
    }
    return lr;
}

void UpdateIdentifierStatus(Connection* rdb, Identifier& identifier, const Str& status, const Str& comment) {
    identifier.status = status;
    identifier.comment = comment;
    identifier.Update(rdb);
}

const Value BALOONIdentifier::OurPushToContour(const Int id) {
    const auto partnerid = CurrentPartnerID(rdb_);    
    PartnerOrderline oline;
    Selector sel;
    sel << oline;
    sel.Where(oline->identifierid == id);
    sel.Limit(1);
    auto ds = sel.Execute(rdb_);
    if(!ds.Fetch()) {
        throw Exception(Message("Could not find orderline. ").What());
    }    
    PartnerOrder order(oline.orderid);
    order.Select(rdb_);
    if(order.partnerid != partnerid) {
        throw Exception(Message("Access denied. ").What());
    }
    Identifier identifier(id);
    identifier.Select(rdb_);
    if(identifier.status != IDENTIFIER_STATUS_CANCELED) {
        throw Exception(Message("Could not push to Contour. ").What());
    }
    const auto gate = GetDefaultGate(rdb_);
    SpdClient client;
    client.spd_host = gate.host;
    client.spd_port = gate.port;

//    const auto offset = Computerica::get_utc_offset();
    const auto offset = boost::posix_time::hours(0);

    FullfillClientForRequest(client, offset, oline, identifier);
    try {
        const auto response = SendSpdRequest(client);
        UpdateIdentifierStatus(rdb_, identifier, IDENTIFIER_STATUS_OK, {});
    }
    catch(const Exception& e) {
        UpdateIdentifierStatus(rdb_, identifier, IDENTIFIER_STATUS_CANCELED, e.What());
    }
    catch(...) {
        UpdateIdentifierStatus(rdb_, identifier, IDENTIFIER_STATUS_CANCELED, Str("Неизвестная ошибка выдачи идентификатора"));
    }
    AddMessage(Message("Identifier status updated.").What());
    return Value();
}

const Value BALOONIdentifier::Add(const Int personid, const Str code, const Time valid_from, const Time valid_to, const Str permanent_rulename, const Str categoryname, const Str ratename) {    
    CheckIdentifierFields(code, valid_from, valid_to, permanent_rulename, categoryname, ratename);
    CheckIdentifierBound(rdb_, code);
    const auto response = CreateSpdIdentifier(*code, valid_from, valid_to, *permanent_rulename, *categoryname, *ratename);
    
    Identifier identifier;
    identifier.code = code;
    identifier.personid = personid;
    identifier.valid_from = valid_from;
    identifier.valid_to = valid_to;
    identifier.permanent_rulename = permanent_rulename;
    identifier.categoryname = categoryname;
    identifier.ratename = ratename;
    identifier.clientoid = response.first;
    try {
        const auto sk = identifier.Insert(rdb_);
        Value res;
        res["id"] = sk;
        AddMessage(Message("Identifier created.").What());
        return res;
    }
    catch(const Exception& e) {
        Log(5) << e.What() << std::endl;
        throw Exception(Message("Could not add identifier.").What());
    }
}
//------------------------------------------------------------------------------
/// Проверяем, привязан ли идентификатор?
void CheckIdentifierBound(Connection* rdb, const Str& code) {
    Identifier identifier;
    Selector sel;
    sel << identifier->id;
    sel.Where(identifier->code == code);
    auto ds = sel.Execute(rdb);
    if(ds.Fetch()) {
        throw Exception(Message("Identifier already bound.").What());
    }
}
//------------------------------------------------------------------------------
/// Привязать идентификатор к заданной персоне
const Value BALOONIdentifier::BindIdentifierToCurrentPerson_FE(const Str code) {
    if(StrEmpty(code)) {
        throw Exception(Message("Identifier is empty.").What());
    }

    const auto personid = checkAndGetCurrentPersonId(rdb_);

    const auto gate = GetDefaultGate(rdb_);
    
    CheckIdentifierBound(rdb_, code);
    
    SpdClient client;
    client.spd_host = gate.host;
    client.spd_port = gate.port;
    
    client.identifier = *code;
    const auto response = SendSpdRequest(client);
    try {
        const auto& client = response.clients.at(0);
        const auto& identifiers = client.identifiers;
        auto iter = std::find_if(identifiers.cbegin(), identifiers.cend(), [code](const SpdIdentifier& item) { return item.code == *code; });
        if(iter == identifiers.cend()) {
            throw Exception(Message("No identifiers found.").What());
        }
        const auto permanent_rule = iter->permanent_rule;
        CheckIdentifierRuleEnabled(rdb_, permanent_rule);
        Identifier identifier;
        identifier.personid = personid;
        identifier.code = iter->code;
        identifier.valid_from = iter->valid_from;
        identifier.valid_to = iter->valid_to;
        identifier.permanent_rulename = permanent_rule;
        identifier.categoryname = iter->category;
        identifier.ratename = iter->tariff;
        identifier.clientoid = client.oid;
        identifier.status = IDENTIFIER_STATUS_OK;
        const auto sk = identifier.Insert(rdb_);

        AddMessage(Message("Identifier bound successfully.").What());

        Value res;
        res["id"] = sk;

        return res;
    }
    catch(const std::out_of_range&) {
        throw Exception(Message("No clients in Spd response.").What());
    }
}
//------------------------------------------------------------------------------
Value GetCurrentIdentifierList(const Int& personid, bool distribute_by_accounts, bool active_only) {
    BaloonProvider provider;
    provider.Select(rdb_);

    Identifier identifier;
    Selector sel;
    sel << identifier;
    sel.Where(identifier->personid == personid &&
             (active_only? identifier->status == IDENTIFIER_STATUS_OK : Expression()));

    sel.OrderAsc(identifier->clientoid);
    auto ds = sel.Execute(rdb_);
    Value res;
    uint i = 0;
    Str clientoid;
    while(ds.Fetch()) {
        Value item;
        item["id"] = identifier.id;
        item["code"] = convertSpdCodeToPrint(identifier.code.get_value_or(string()), provider.code_conversion_algo);
        item["valid_from"] = identifier.valid_from /*+ Computerica::get_utc_offset()*/;
        item["valid_to"] = identifier.valid_to /*+ Computerica::get_utc_offset()*/;
        item["permanent_rule"] = identifier.permanent_rulename;
        item["category"] = identifier.categoryname;
        item["rate"] = identifier.ratename;
        item["name"] = identifier.name;
        
        if(distribute_by_accounts) {
            const auto account_changed = (Defined(clientoid) && clientoid != identifier.clientoid);
            item["is_new_account"] = account_changed? BOOL_TRUE : BOOL_FALSE;
        }
        clientoid = identifier.clientoid;
        res[i] = item;
        ++i;
    }
    return res;
}

const auto DISTRIBUTE_BY_ACCOUNTS = true;
const bool ACTIVE_ONLY = true;

const Value BALOONIdentifier::CurrentIdentifierListGet_FE() {
    const auto personid = checkAndGetCurrentPersonId(rdb_);
    return GetCurrentIdentifierList(personid, not DISTRIBUTE_BY_ACCOUNTS, ACTIVE_ONLY);
}

const Value BALOONIdentifier::IdentifierListDistributedByAccountsGet_FE() {
    const auto personid = checkAndGetCurrentPersonId(rdb_);
    return GetCurrentIdentifierList(personid, DISTRIBUTE_BY_ACCOUNTS, ACTIVE_ONLY);
}

void AddZeroBalance(Value& entry) {
    entry["balance"] = 0.0;
    entry["currency"] = string("RUB");
}

Value GetBalancedIdentifierList(const Int& personid) {
    auto lr = GetCurrentIdentifierList(personid, not DISTRIBUTE_BY_ACCOUNTS, ACTIVE_ONLY);
    const auto size = lr.Size();
    if(!size) {
        return lr;
    }
    const auto gate = GetDefaultGate(rdb_);
    for(uint i = 0; i < size; ++i) {
        auto& entry = lr[i];
        const auto code = entry["code"].As<string>();
        if(code.empty()) {
            AddZeroBalance(entry);
            continue;
        }
        SpdClient client;
        client.spd_host = gate.host;
        client.spd_port = gate.port;
        client.identifier = code;
        const auto response = SendSpdRequest(client);
        try {
            const auto account = response.clients.at(0).account;    
            const auto currencies = account.currencies;
            if(!currencies.size()) {
                AddZeroBalance(entry);
                continue;
            }
            const auto item = currencies.at(0);
            entry["balance"] = item.balance;
            entry["currency"] = item.currency;
        }
        catch(std::out_of_range&) {
            throw Exception(Message() << Message("Could not find identifier ").What() << code << ". ");
        }
    }
    return lr;
}

const Value BALOONIdentifier::BalancedIdentifierListGet_FE() {
    const auto personid = checkAndGetCurrentPersonId(rdb_);
    return GetBalancedIdentifierList(personid);
}

const Value BALOONIdentifier::IdentifierNameUpdate_FE(const Str code, const Str name) {
    if(StrEmpty(name)) {
        return Value();
    }
    Identifier identifier;
    Updater upd(identifier);
    upd << identifier->name(name);
    upd.Where(identifier->code == code);
    upd.Execute(rdb_);
    return Value();
}

const Value BALOONIdentifier::SetDateTimeFields() {
    Value result;
    const long int daysInYear = 365;
    boost::posix_time::ptime now = TIME_NOW; // + boost::posix_time::hours(3);
    result["valid_from"] = now;
    result["valid_to"] = now + boost::gregorian::days (daysInYear);
    return result;
}

Identifier GetIdentifierByCode(Connection* rdb, const Str& code) {
    Identifier identifier;
    Selector sel;
    sel << identifier;
    sel.Where(identifier->code == code);
    sel.Limit(1);
    auto ds = sel.Execute(rdb);
    if(!ds.Fetch()) {
        throw Exception(Message("Could not find identifier. ").What());
    }
    return identifier;
}

const Value BALOONIdentifier::GetDetails_API(const Str code) {
    const auto identifier = GetIdentifierByCode(rdb_, code);
     
    const auto gate = GetDefaultGate(rdb_);
    SpdClient client;
    client.identifier = convertPrintCodeToSpd(code.get_value_or(""));
    client.spd_host = gate.host;
    client.spd_port = gate.port;

    SpdResponse response = SendSpdRequest(client);
    if (response.clients.size() == 0) throw Exception(Message("Card code not found. ").What());
    BaloonProvider provider;
    provider.Select(rdb_);

    Value res;
    if (response.clients[0].identifiers.size() > 0) {
        SpdIdentifier aIdentifier = response.clients[0].identifiers[0];
        IdentifierRule rule;
        rule.name = aIdentifier.permanent_rule;
        rule.Select(rdb_);

        res["id"] = identifier.id;
        res["name"] = identifier.name;
        res["code"] = convertPrintCodeToSpd(client.identifier);
        res["valid_from"] = aIdentifier.valid_from;
        res["valid_to"] = aIdentifier.valid_to;
        res["permanent_rule"] = aIdentifier.permanent_rule;
        res["category"] = aIdentifier.category;
        res["rate"] = aIdentifier.tariff;
        res["expired"] = aIdentifier.valid_to < TIME_NOW ? BOOL_TRUE : BOOL_FALSE;
        res["replenish_enabled"] = ((rule.replenish_enabled == ENABLED) ? ENABLED : DISABLED);

        Value packages;
        vector<SpdPackage>::iterator p = aIdentifier.packages.begin();
        int32_t i = 0;
        for (; p!=aIdentifier.packages.end(); p++) {
            Value row;
            row["state"] = p->state;
            row["date"] = p->date;
            row["use_start"] = p->use_start;
            row["use_end"] = p->use_end;
            row["valid_end"] = p->valid_end;
            row["duration"] = p->duration;
            row["type"] = p->type;
            row["rule_use"] = p->rule_use;
            row["tariff"] = p->tariff;
            row["description"] = p->description;
            row["use_count"] = p->use_count;
            row["used_count"] = p->used_count;
            packages[i++] = row;
        }
        res["packages"] = packages;
    }
    else throw Exception(Message("Card code not found int SPD. ").What());

    Value accounts;
    vector<SpdAccountCurrency>::iterator it = response.clients.at(0).account.currencies.begin();
    for (; it!=response.clients.at(0).account.currencies.end(); it++) {
        accounts[it->currency] = it->balance;
    }
    res["accounts"] = accounts;
    return res;
}

const Value BALOONIdentifier::GetBalance_API(const Str code, const Str print_code) {
    SpdClient client;
    client.identifier = convertPrintCodeToSpd(code.get_value_or(""));
    if (!print_code.get_value_or("").empty()) {
        client.identifier = from_print_code(print_code.get_value_or(""));
    }
    const auto gate = GetDefaultGate(rdb_);
    client.spd_host = gate.host;
    client.spd_port = gate.port;

    SpdResponse response = SendSpdRequest(client);
    if (response.clients.size() == 0) throw Exception(Message("Card code not found. ").What());

    Value res;
    vector<SpdAccountCurrency>::iterator it = response.clients.at(0).account.currencies.begin();
    for (; it!=response.clients.at(0).account.currencies.end(); it++) {
        res[it->currency] = it->balance;
    }
    return res;
}

const Value BALOONIdentifier::GetBalance_FE(const Str code, const Str print_code) {
    SpdClient client;
    client.identifier = convertPrintCodeToSpd(code.get_value_or(""));
    if (!print_code.get_value_or("").empty()) {
        client.identifier = from_print_code(print_code.get_value_or(""));
    }
    const auto gate = GetDefaultGate(rdb_);
    client.spd_host = gate.host;
    client.spd_port = gate.port;
    const auto response = SendSpdRequest(client);
    try {
        const auto& currencies = response.clients.at(0).account.currencies;
        Value res;
        uint i = 0;
        if(currencies.empty()) {
            Value entry;
            entry["currency"] = Str();
            entry["balance"] = Decimal(0.0);
            res[i] = entry;
        }
        else for(const auto& item : currencies) {
            Value entry;
            entry["currency"] = item.currency;
            entry["balance"] = item.balance;
            res[i] = entry;
            ++i;
        }
        return res;
    }
    catch(const std::out_of_range&) {
        throw Exception(Message("Card code not found. ").What());
    }
}

const Value BALOONIdentifier::PackagesListGet_API(const Str code, const Str print_code) {
    SpdClient client;
    client.identifier = convertPrintCodeToSpd(code.get_value_or(""));
    if (!print_code.get_value_or("").empty()) {
        client.identifier = from_print_code(print_code.get_value_or(""));
    }
    
    const auto gate = GetDefaultGate(rdb_);
    client.spd_host = gate.host;
    client.spd_port = gate.port;
    
    SpdResponse response = SendSpdRequest(client);
    if (response.clients.empty()) {
        throw Exception(Message("Card code not found. ").What());
    }
    Value res;
    int32_t i = 0;
    vector<SpdIdentifier> identifiers = response.clients[0].identifiers;
    vector<SpdIdentifier>::iterator it = identifiers.begin();
    for (; it!=identifiers.end(); it++) {
        SpdIdentifier identifier = *it;
        if (client.identifier != identifier.code && client.identifier != identifier.value) {
            continue;
        }

        vector<SpdPackage>::iterator p = identifier.packages.begin();
        for (; p!=identifier.packages.end(); p++) {
            Value row;
            row["comment"] = p->comment;
            row["state"] = p->state;
            row["date"] = p->date /* + Computerica::get_utc_offset()*/;
            row["use_start"] = p->use_start /*+ Computerica::get_utc_offset()*/;
            row["use_end"] = p->use_end /*+ Computerica::get_utc_offset()*/;
            row["valid_end"] = p->valid_end /*+ Computerica::get_utc_offset()*/ ;
            row["duration"] = p->duration;
            row["type"] = p->type;
            row["rule_use"] = p->rule_use;
            row["tariff"] = p->tariff;
            row["description"] = p->description;
            row["use_count"] = p->use_count;
            row["used_count"] = p->used_count;
            res[i++] = row;
        }
    }
    return res;
}

const Value BALOONIdentifier::ConvertPrintCode(const Str print_code) {
    Value res;
    res["code"] = from_print_code(print_code.get_value_or(""));
    return res;
}

const Value BALOONIdentifier::ShowConvertPrintCode(const Str print_code_) {
    string code = "",
            error = "",
            print_code = print_code_.get_value_or("");
    if (!print_code.empty()) try {
        code = from_print_code(print_code);
    }
    catch (Exception e) {
        error = e.What();
    }
    return Computerica::HtmlLayout().
            setTitle(Message("Card Code Conversion").What()).
            setArgs({
                {"print_code", print_code},
                {"code", code},
                {"error", error}
            }).
            render("card-convertion.html");
}

pair<string, string> CreateSpdIdentifier(const std::string& code, const Effi::Time& valid_from, const Effi::Time& valid_to, const std::string& permanent_rulename, const std::string& categoryname, const std::string& ratename) {
    const auto NEW = true;
    SpdClient client(NEW);
    
    const auto gate = GetDefaultGate(rdb_);
    client.spd_host = gate.host;
    client.spd_port = gate.port;
    
    SpdIdentifier identifier;
    identifier.is_new = NEW;
    identifier.code = convertPrintCodeToSpd(code);
    identifier.permanent_rule = permanent_rulename;
    identifier.category = categoryname;
    identifier.tariff = ratename;
    identifier.valid_from = valid_from;
    identifier.valid_to = valid_to;    
    client.identifiers.push_back(identifier);
    const auto response = SendSpdRequest(client);
    try {
        const auto client = response.clients.at(0);
        const auto identifier = client.identifiers.at(0);
        const auto clientoid = client.oid;
        return make_pair(clientoid, identifier.code);
    }
    catch(const std::out_of_range&) {
        throw Exception("Could not create identifier in spd. ");
    }
}

Int64 InsertIdentifier(Connection* rdb, const string& email, const std::string& code, const Effi::Time& valid_from, const Effi::Time& valid_to, const std::string& permanent_rulename, const std::string& categoryname, const std::string& ratename) {
    Person person;
    Selector sel;
    sel << person->id;
    sel.Where(person->email == email);
    sel.Limit(1);
    auto ds = sel.Execute(rdb);
    if(!ds.Fetch()) {
        throw Exception("Unknown email. ");
    }    
    Identifier identifier;
    identifier.personid = person.id;
    identifier.code = code;
    identifier.valid_from = valid_from;
    identifier.valid_to = valid_to;
    identifier.permanent_rulename = permanent_rulename;
    identifier.categoryname = categoryname;
    identifier.ratename = ratename;
    identifier.name = Str("TEST");
    identifier.status = IDENTIFIER_STATUS_OK;
    const auto sk = identifier.Insert(rdb);
    return sk;
}

void CheckIdentifierFields(const Effi::Str& code, const Effi::Time& valid_from, const Effi::Time& valid_to, const Effi::Str& permanent_rulename, const Effi::Str& categoryname, const Effi::Str& ratename) {
    if(!(Defined(code) && Defined(valid_from) && Defined(valid_to) && Defined(permanent_rulename) && Defined(categoryname) && Defined(ratename))) {
        throw Exception(Message("All fields are mandatory. ").What());
    }
}

const Value BALOONIdentifier::CreateUnpersonalizedIdentifierInSpd(const Str email, const Str code, const Time valid_from, const Time valid_to, const Str permanent_rulename, const Str categoryname, const Str ratename, const Decimal amount) {
    CheckIdentifierFields(code, valid_from, valid_to, permanent_rulename, categoryname, ratename);
    const auto clientoid_code_pair = CreateSpdIdentifier(*code, valid_from, valid_to, *permanent_rulename, *categoryname, *ratename);
    createSpdTransaction(*code, amount, "RUB", "Тестовое пополнение карты");
    
    if(Defined(email) && !email->empty()) {
        InsertIdentifier(rdb_, *email, *code, valid_from, valid_to, *permanent_rulename, *categoryname, *ratename);
    }
    
    AddMessage(Message("Unpersonalized identifier created in Contour. ").What());
    Value res;
    res["code"] = Str(clientoid_code_pair.second);
    return res;
}

const Value BALOONIdentifier::MyPayedIdentifierListGet_FE() {
    const auto my_personid = checkAndGetCurrentPersonId(rdb_);
    PersonOrder order;
    Identifier identifier;
    PersonIdentifierOrderline orderline;
    Selector sel;
    sel << identifier->id
        << identifier->code
        << identifier->valid_from
        << identifier->valid_to
        << identifier->name;
    sel.From(orderline)
        .Join(identifier).On(orderline->identifierid == identifier->id)
        .Join(order).On(orderline->orderid == order->id);
    sel.Where(order->status == PERSON_ORDER_STATUS_PAYED &&
              order->personid == my_personid);
    sel.OrderDesc(identifier->valid_to);
    auto ds = sel.Execute(rdb_);
    Value res;
    uint i = 0;
    while(ds.Fetch()) {
        Value entry;
        entry["id"] = identifier.id;
        entry["code"] = identifier.code;
        entry["name"] = identifier.name;
        entry["valid_from"] = identifier.valid_from;
        entry["valid_to"] = identifier.valid_to;
        res[i] = entry;
        ++i;
    }
    return res;
}

const double QRCODE_RESOLUTION(4.0);

const Value BALOONIdentifier::DownloadQRCode(const Str code) {
    ShPtr<Blob> blob = generateQRCode(code.get_value_or(""), QRCODE_RESOLUTION);
    BlobFile file;
    std::stringstream name;
    name << "qr-" << *code << ".png";
    file.SetFileName(name.str());
    file.SetMimeType("image/png");
    file.Content() = blob;
    return file;
}

const Value BALOONIdentifier::GenerateQRCode(const Str code) {
    Log(5) << "-----" << code << endl;
    ShPtr<Blob> blob = generateQRCode(code.get_value_or(""), QRCODE_RESOLUTION);
    Value res;
    res["qrcode"] = *blob;
    return res;
}

const Value BALOONIdentifier::DownloadBarcode(const Str code) {
    Value params;
    params["identifiercode"] = code;
    std::string text = Computerica::Templating::processReporter("/barcode", params);
    Log(5) << "-----text: " << text << endl;
    BlobFile res;
    res.SetMimeType("text/html");
    res.Content() = Blob(text.size(), text.c_str());
    return res;
}

const Value BALOONIdentifier::GenerateBarcode(const Str code) {
    if(StrEmpty(code)) {
        throw Exception(Message("Identifier code cannot be empty. ").What());
    }
    const auto barcode = Computerica::code128c_font_encode(*code);
    Value res;
    res["barcode"] = barcode;
    return res;
}

const Value BALOONIdentifier::Cancel_FE(const Str code) {
    Identifier identifier;
    Selector sel;
    sel << identifier;
    sel.Where(identifier->code == code);
    sel.Limit(1);
    auto ds = sel.Execute(rdb_);
    if(ds.Fetch()) {
        identifier.status = IDENTIFIER_STATUS_CANCELED;
        identifier.Update(rdb_);
    }
    return Value();
}

const Value BALOONIdentifier::Enable(const Int id) {
    Identifier identifier(id);
    identifier.Select(rdb_);
    identifier.status = IDENTIFIER_STATUS_OK;
    identifier.Update(rdb_);
    AddMessage(Message("Identifier enabled. ").What());
    return Value();
}

Decimal GetPrice(Connection* rdb, const IdentifierServiceRule& rule, const ADate& adate) {
    Holiday holiday(adate);
    if(holiday.Select(rdb)) {
        return rule.weekend_cost;
    }
    const auto day_of_week = static_cast<int>(adate.day_of_week());
    BaloonProvider provider;
    provider.Select(rdb);    
    std::vector<Str> day_types { provider.sun, provider.mon, provider.tue, provider.wed, provider.thu, provider.fri, provider.sat };
    const auto day_type = day_types[day_of_week];
    if(day_type == WEEKDAY) {
        return rule.weekday_cost;
    }
    return rule.weekend_cost;
}

std::mutex identifier_code_mutex;
string GenerateCode(Connection* rdb) {
    const auto LOWER_LETTERS(true);
    const auto UPPER_LETTERS(true);
    const auto IDENTIFIER_LENGTH(16);
    const auto attempts_nr(16);
    for(int i = 0; i < attempts_nr; ++i) {
        lock_guard<mutex> lock(identifier_code_mutex);
        const auto code = Computerica::randomCode(IDENTIFIER_LENGTH, not UPPER_LETTERS, not LOWER_LETTERS);
        Identifier identifier;
        Selector sel;
        sel << identifier->id;
        sel.Where(identifier->code == code);
        auto ds = sel.Execute(rdb);
        if(!ds.Fetch()) {
            return code;
        }
    }
    Log(5) << "~~~ERROR: could not generate unique code. " << std::endl;
    throw Exception("Could not generate unique code. ");
}

const string RUB_CURRENCY("RUB");

Str ParkingTicketRegisterInContour(Connection* rdb, const Int& orderid) {
    ParkingOrderline oline;
    Identifier identifier;
    SpdGate spdgate;
    Selector sel;
    sel << oline
        << identifier->code
        << spdgate;
    sel.From(oline)
        .Join(identifier).On(oline->identifierid == identifier->id)
        .Join(spdgate).On(identifier->spdgateid == spdgate->id);
    sel.Where(oline->orderid == orderid);
    auto ds = sel.Execute(rdb);
    if(!ds.Fetch()) {
        return Str();
    }
    const auto code = identifier.code.get_value_or(string());
    SpdClient client;
    client.spd_host = spdgate.host;
    client.spd_port = spdgate.port;
    client.identifier = code;
    
    SpdIdentifier spdidentifier;
    spdidentifier.calc_time_cost_get = true;
    spdidentifier.calc_time_cost_pay = true;
    spdidentifier.code = code;
    spdidentifier.value = code;
    spdidentifier.eid = to_upper(Effi::GenerateGUID());
    
    client.identifiers.push_back(spdidentifier);
    const auto response = SendSpdRequest(client);
    try {
        const auto client = response.clients.at(0);
        return client.oid;
    }
    catch(const out_of_range&) {
        Log(5) << "~~~spd ERROR: no clients in spd response. Order id = " << orderid << std::endl;
        throw Exception("No clients in spd response. ");
    }
}

Str RegisterInContour(Connection* rdb, const Int& orderid, const SpdGate& spd_gate, bool free = false) {
    Log(5) << "-----RegisterInContour order id = " << orderid << endl;
    Identifier identifier;
    PersonIdentifierOrderline oline;
    Selector sel;
    sel << identifier
        << oline;
    sel.From(identifier).Join(oline).On(oline->identifierid == identifier->id);
    sel.Where(oline->orderid == orderid);
    auto ds = sel.Execute(rdb);
    if(!ds.Rows()) {
        return Str();
    }
    
    const auto NEW(true);
    SpdClient client(NEW);
    client.spd_host = spd_gate.host;
    client.spd_port = spd_gate.port;
    
//    const auto offset = Computerica::get_utc_offset();
//    const auto offset = boost::posix_time::hours(0);
    while(ds.Fetch()) {
        SpdIdentifier spd_identifier;
        spd_identifier.is_new = NEW;
        spd_identifier.eid = to_upper(Effi::GenerateGUID());
        spd_identifier.code = identifier.code.get_value_or(string());
        spd_identifier.valid_from = identifier.valid_from;
        spd_identifier.valid_to = identifier.valid_to;
        spd_identifier.permanent_rule = identifier.permanent_rulename.get_value_or(string());
        spd_identifier.category = identifier.categoryname.get_value_or(string());
        spd_identifier.tariff = identifier.ratename.get_value_or(string());
        
        if(!free && oline.make_transaction == BOOL_TRUE) {
            const string comment("Пополнение счёта");
            SpdTransaction transaction;
            transaction.currency = RUB_CURRENCY;
            transaction.comment = comment;
            transaction.eid = to_upper(Effi::GenerateGUID());
            transaction.summ = oline.price;
            client.transactions.push_back(transaction);
        }
        if(Defined(oline.service_rulename)) {
            SpdPackage package;
            package.identifier = spd_identifier.code;
            package.rule_service = *oline.service_rulename;
            package.eid = to_upper(Effi::GenerateGUID());
            client.packages.push_back(package);
        }        
        client.identifiers.push_back(spd_identifier);
    }
    const auto response = SendSpdRequest(client);
    try {
        const auto client = response.clients.at(0);
        return client.oid;
    }
    catch(const std::out_of_range&) {
        Log(5) << "~~~spd ERROR: no clients in spd response. Order id = " << orderid << std::endl;
        throw Exception("No clients in spd response. ");
    }
}

IdentifierCategory GetDefaultIdentifierCategory(Connection* rdb) {
    IdentifierCategory cat;
    Selector sel;
    sel << cat;
    sel.Where(cat->is_default == BOOL_TRUE);
    auto ds = sel.Execute(rdb);
    if(ds.Fetch()) {
        return cat;
    }
    throw Exception(Message("Default identifier category is not defined. ").What());
}
//----------------------------------------------------------------------
/// Передать карту клиенту (Персонализировать ее)
/// TODO: Не реализована передача карты в контуре  
const Value BALOONIdentifierOwners::GiveToClient(const Str code)
{
    if(StrEmpty(code))
        {
        throw Exception(Message("Identifier is empty.").What());
        }

    const auto personid = checkAndGetCurrentPersonId(rdb_); // Получаем текущего пользователя

    /// Подразумевается, что основной владелец admin имеет personid=1
    /// До версии Baloon-01.00-138 это не предусматривалось
    /// В системах, где используются анонимные пропуска, нужно перенастраивать пользователя(Person) с ID=1
    /// Это должен быть пользователь Admin
    Int main_owner_id=Int(1);
    
    Identifier identifier;
    Selector sel;
    sel << identifier;
    
    sel.Where(identifier->code == code && identifier->personid==main_owner_id);
    auto ds = sel.Execute(rdb_);
    if(ds.Fetch())
        {
        identifier.personid = personid;
        identifier.Update(rdb_);
        IdentifierOwners id_owner;

        id_owner.identid=identifier.id;
        id_owner.old_ownerid=main_owner_id;
        id_owner.new_ownerid=personid;
        id_owner.tdate=Today();
        //id_owner.ttime=(TIME_NOW);
        id_owner.Insert(rdb_);
        }
    else
        throw Exception(Message("Identifier not bound to main owner.").What());

    Value res;
    res["result"] = BOOL_TRUE;
    return res;
    
    /*
    const auto gate = GetDefaultGate(rdb_); // Получаем шлюз по умолчанию
    
    //CheckIdentifierBound(rdb_, code);
    
    SpdClient client;
    client.spd_host = gate.host;
    client.spd_port = gate.port;
    
    client.identifier = *code;

    const auto response = SendSpdRequest(client); // Шлем запрос
    
    try {
        const auto& client = response.clients.at(0);
        const auto& identifiers = client.identifiers;
        // Ищем среди идентификаторов нужный с кодом
        auto iter = std::find_if(identifiers.cbegin(), identifiers.cend(), [code](const SpdIdentifier& item)
            { return item.code == *code; });
        if(iter == identifiers.cend())
            {
            throw Exception(Message("No identifiers found.").What());
            }

        const auto permanent_rule = iter->permanent_rule;
        CheckIdentifierRuleEnabled(rdb_, permanent_rule);
        Identifier identifier;
        
        identifier.personid = personid;
        identifier.code = iter->code;
        identifier.valid_from = iter->valid_from;
        identifier.valid_to = iter->valid_to;
        identifier.permanent_rulename = permanent_rule;
        identifier.categoryname = iter->category;
        identifier.ratename = iter->tariff;
        identifier.clientoid = client.oid;
        identifier.status = IDENTIFIER_STATUS_OK;
        const auto sk = identifier.Insert(rdb_);

        AddMessage(Message("Identifier bound successfully.").What());

        Value res;
        res["id"] = sk;

        return res;
    }
    catch(const std::out_of_range&) {
        throw Exception(Message("No clients in Spd response.").What());
    }
    */ 
}
//----------------------------------------------------------------------
/// Забрать карту у клиента(Анонимизировать ее)
const Value BALOONIdentifierOwners::TakeFromClient(const Int identid)
{
    return Value();    
}
//----------------------------------------------------------------------
/// Создаем заказ в БД и возращаем его
PersonOrder InsertOrder(Connection* rdb, const Int& personid, const Decimal& amount, bool free, const Str& promocode, Double discount, const Str& sitecode) {
    PersonOrder order;
    order.personid = personid;
    order.filed_at = TIME_NOW;
    order.amount = amount;
    order.promocode = promocode;
    order.final_amount = free? Decimal(0.0) : amount - (amount * discount) / 100;    
    order.status = free? PERSON_ORDER_STATUS_PAYMENT_NOT_REQUIRED : PERSON_ORDER_STATUS_WAITING;
    order.sitecode = sitecode;
    const auto id = order.Insert(rdb);
    order.id = id;
    return order;
}
//----------------------------------------------------------------------

IdentifierServiceRule GetIdentifierServiceRule(Connection* rdb, const Str& name) {
    IdentifierServiceRule is_rule;
    Selector sel;
    sel << is_rule;
    sel.Where(is_rule->name == name);
    sel.Limit(1);
    auto ds = sel.Execute(rdb);
    if(ds.Fetch()) {
        return is_rule;
    }
    throw Exception(Message() << Message("Identifier service rule setting not found: ").What() << name.get_value_or(string()) << ". ");
}

template<typename T>
struct Prefix;
template<>
struct Prefix<IdentifierInvoice> {
    static constexpr const char* const value = "NIBC-";
};
template<>
struct Prefix<PersonInvoice> {
    static constexpr const char* const value = "VG-";
};
template<>
struct Prefix<FISCAL::FiscalDoc> {
    static constexpr const char* const value = "FD-";
};

template<typename T>
string nextInvoiceBarcode(Connection *rdb) {
    const int BARCODE_WIDTH = 12;
    T table;
    int32_t max_id = 0;
    Selector sel;
    sel << table->id.Max().Bind(max_id);
    auto data = sel.Execute(rdb);
    data.Fetch();
    stringstream ss;
    ss << Prefix<T>::value << setfill('0') << setw(BARCODE_WIDTH) << (max_id + 1);
    const auto barcode = ss.str();
    return barcode;
}
//------------------------------------------------------------------------------
/// Создание платежа(счета) на стороне платежной системы
const Value BALOONPersonInvoice::RaiseInvoice_FE(const Int orderid, const Decimal amount/*Not used*/, const Str success_url, const Str fail_url) {
    if(!Defined(orderid)) {
        Log(5) << "-----ERROR: Order id is not defined. " << endl;
        throw Exception(Message("Order id is not defined. ").What());
    }
    
    PersonOrder order(orderid); // Заказ, привязанный к персоне
    order.Select(rdb_);
    Person person(order.personid); // Персона
    person.Select(rdb_);
    
    const auto origin = MainConfig->Get("WWW/Origin").get_value_or("");
    const auto paygateid = GetDefaultPaygate(rdb_);
    
    const auto barcode = PAYGATE::NextBarcode(rdb_);
    
    const string p_success{"/tickets-success-download.html"};
    const string p_fail{"/tickets-fail.html"};
    
    Value extra_params;
    extra_params["email"] = person.email;

    // Вызываем метод из модуля Paygate
    Value res = RootMethodInvoker("Paygate", "Invoice", "RaiseInvoice") [
        PName<Int>("paygateid") = paygateid,
        PName<Str>("barcode") = barcode,
        PName<Decimal>("amount") = order.final_amount,
        PName<Str>("ext_orderid") = Str(std::to_string(*orderid)),
        PName<Str>("order_type") = BALOON_PERSON_INVOICE_CLASS,
        PName<Str>("success_url") = Defined(success_url)? success_url : Str(origin + p_success),
        PName<Str>("fail_url") = Defined(fail_url)? fail_url : Str(origin + p_fail),
        PName<Value>("extra_params") = extra_params
    ];
    const auto invoiceid = res["INVOICEID@@"].As<Int>(); // Получить номер счета
    
    PersonInvoice p_invoice; // Получаем счет, связанный с заказом
    p_invoice.id = invoiceid;
    p_invoice.orderid = orderid;
    p_invoice.Insert(rdb_); // Вставляем счет в БД

    res["ORDERID@@"] = orderid;
    res["ORDER_AMOUNT@@"] = order.amount;
    
    return res;
}
//------------------------------------------------------------------------------
const Value BALOONPersonOrder::Add(const Int personid, const ADate adate, const Str identifier_service_rulename, const Str identifier_ratename, const Int qty) {
    //const auto res = PlaceOrder(rdb_, personid, adate, identifier_service_rulename, identifier_ratename, qty);
    return Value();
}

const Value BALOONPersonOrder::Place_API(const Str email, const Str last_name, const Str first_name, const ADate adate, const Str identifier_service_rulename, const Int qty) {
    throw Exception("По техническим причинам online-продажи временно приостановлены. ");
    /*const auto personid = CreateOrFindUser(rdb_, email, last_name, first_name);
    const auto res = PlaceOrder(rdb_, personid, adate, identifier_service_rulename, Str(), qty);
    return res;*/
}

Value EmptyPromotion() {
    Value empty;
    empty["barcode"] = Str();
    empty["discount"] = 0.0;
    empty["is_active"] = BOOL_FALSE;
    return empty;
}

const Value BALOONPersonOrder::MyPlace_API(const ADate adate, const Str identifier_service_rulename, const Int qty) {
    throw Exception("По техническим причинам online-продажи временно приостановлены. ");
    /*const auto personid = checkAndGetCurrentPersonId(rdb_);
    const auto res = PlaceOrder(rdb_, personid, adate, identifier_service_rulename, Str(), qty);
    return res;*/
}

const vector<string> WEEKDAYS {SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY};
const int DEFAULT_TIMEOUT_MIN(15);

struct IdentifierDuration {
    Time from;
    Time to;
};

inline
IdentifierDuration GetIdentifierDuration(const WeekTariff& tariff, const ADate& adate) {
    IdentifierDuration dur;

/*
    std::string tttt1(tariff.time_from.get());

    std::string hh;
    std::string mm;
    std::string ss;

    hh = tttt1.substr(0,2);
    mm = tttt1.substr(2,4);
    ss = tttt1.substr(4,6);

    int hhhh = 0;
    int mmmm = 0;
    int ssss = 0;

    hhhh = strtol( hh.c_str(), 0, 10 );
    mmmm = strtol( mm.c_str(), 0, 10 );
    ssss = strtol( ss.c_str(), 0, 10 );

    boost::posix_time::time_duration t1(hhhh,mmmm,ssss);

    Time pt1 = Time( adate, t1 );

    std::string tttt2(tariff.time_upto.get());

    hh = tttt2.substr(0,2);
    mm = tttt2.substr(2,4);
    ss = tttt2.substr(4,6);

    hhhh = strtol( hh.c_str(), 0, 10 );
    mmmm = strtol( mm.c_str(), 0, 10 );
    ssss = strtol( ss.c_str(), 0, 10 );

    boost::posix_time::time_duration t2(hhhh,mmmm,ssss);
*/

//    Log(0) << "TIME: IdentifierDuration [ " << adate << ", " << tariff.time_from << " ] " << endl;
//    Log(0) << "TIME: IdentifierDuration [ " << adate << ", " << tariff.time_upto << " ] " << endl;

    Time pt1 = Time( adate, tariff.time_from );
    Time pt2 = Time( adate, tariff.time_upto );

    dur.from = pt1; // - Computerica::get_utc_offset();
    dur.to = pt2; // - Computerica::get_utc_offset();

    Log(0) << "TIME: IdentifierDuration [ " << pt1 << " ] " << endl;
    Log(0) << "TIME: IdentifierDuration [ " << pt2 << " ] " << endl;

    return dur;
}

//----------------------------------------------------------------------
/// Генерируем список заказов
void GenerateOrderlines(Connection* rdb, const Int& personid, const Int orderid, const ADate& adate, const WeekTariff& tariff, int qty) 
{
    const auto val = BALOONBaloonProvider::Get();
    const auto _timeout = val["timeout_before"].As<Int>();

    const boost::posix_time::minutes timeout(_timeout.get_value_or(DEFAULT_TIMEOUT_MIN));

    const auto dur = GetIdentifierDuration(tariff, adate);

    for(int i = 0; i < qty; ++i) {
        Identifier identifier;
        identifier.code = GenerateCode(rdb);
        identifier.valid_from = dur.from - timeout;
        identifier.valid_to = dur.to + timeout;
        identifier.personid = personid;
        identifier.permanent_rulename = tariff.identifier_rulename;
        identifier.categoryname = tariff.identifier_categoryname;
        identifier.ratename = tariff.ratename;
        identifier.name = tariff.external_name;
        const auto identifierid = identifier.Insert(rdb);

        PersonIdentifierOrderline oline;
        oline.orderid = orderid;
        oline.identifierid = identifierid;
        oline.price = tariff.price;
        oline.service_rulename = tariff.service_rulename;
        oline.make_transaction = BOOL_TRUE;
        oline.Insert(rdb);
    }
}
//----------------------------------------------------------------------

//----------------------------------------------------------------------
/// Проверка на количество карт
void CheckCardsNumber(const Int& qty) {
    if(!Defined(qty) || *qty < 1) {
        throw Exception(Message("Incorrect number of cards. ").What());
    }
}
//----------------------------------------------------------------------
string NormalizeEmail(const Str& email) {
    auto email_ = email.get_value_or(string());
    boost::trim(email_);
    const auto normalized = Computerica::to_lower(email_);    
    const auto is_email = Computerica::isEmail(Str(normalized));
    if(is_email) {
        return normalized;
    }
    throw Exception(Message("Given email is not ok. ").What());
}

//----------------------------------------------------------------------
/// Получить скидку на указанную дату
Value GetDiscount(const Str& barcode, const ADate& adate) {
    try { /// Вызвать метод из другого модуля
        const auto res = RootMethodInvoker("Loyalty", "PromotionCode", "GetDiscount")[
            PName<Str>("barcode") = barcode,
            PName<ADate>("adate") = adate
        ];
        return res;
    }
    catch(const Exception& e) { /// В случае ошибки выдать нулевую скидку
        Value res;
        res["barcode"] = Str();
        res["discount"] = 0.0;
        res["is_active"] = BOOL_FALSE;
        return res;
    }
}
//----------------------------------------------------------------------
/// Разместить заказ в соответствии с данными
Value PlaceOrder(Connection* rdb, const Str& email, const Str& last_name, const Str& first_name, const ADate& adate, const Int& week_tariffid, const Int& qty, const Str& promocode, Double discount, const Str& sitecode) {
    if(!Defined(week_tariffid)) { /// Проверка на то, описан ли тариф или нет?
        throw Exception(Message("Week tariff is not selected. ").What());
    }
    CheckCardsNumber(qty); /// Проверка на количество карт
    const auto today = (TIME_NOW /* + Computerica::get_utc_offset() */ ).date();
    if(adate < today) {
        throw Exception(Message("Cannot place order of previous date. ").What()); /// Проверка на актуальность даты
    }
    const auto personid = CreateOrFindUser(rdb_, email, last_name, first_name); /// Ищем персону
    WeekTariff tariff(week_tariffid); /// Определяем тариф
    tariff.Select(rdb_);
    const auto amount = tariff.price * (*qty); ///(!!) Осуществляем окончательный расчет(без учета скидки :-( )
    const auto FREE(true);
    
    const auto order = InsertOrder(rdb_, personid, amount, !FREE, promocode, discount, sitecode); /// Записываем заказ
    GenerateOrderlines(rdb_, personid, order.id, adate, tariff, *qty); /// Генерируем строки с заказами
    Value res;
    res["id"] = order.id;
    res["amount"] = amount;
    res["final_amount"] = order.final_amount;
    res["free"] = BOOL_FALSE;
    return res;
}
//----------------------------------------------------------------------

template<typename OLINE>
void UpdateIdentifierStatus(Connection* rdb, const Int& orderid, const string& status, const Str& comment, const Str& clientoid) {
    OLINE oline;
    Identifier identifier;
    Selector sel;
    sel << identifier;
    sel.From(identifier).Join(oline).On(oline->identifierid == identifier->id);
    sel.Where(oline->orderid == orderid);
    auto ds = sel.Execute(rdb);    
    while(ds.Fetch()) {
        identifier.status = status;
        identifier.comment = comment;
        if(Defined(clientoid)) {
            identifier.clientoid = clientoid;
        }
        identifier.Update(rdb);
    }
}

template<typename T>
void DeliverNotification(const Str& email, const Str& templatename, const T& order) {
    Value params;
    params["id"] = order.id;
    RootMethodInvoker("Mailbox", "MailboxMessage", "CreateEmailFromTemplate") [
        PName<Str>("email") = Str(email),
        PName<Str>("templatename") = Str(templatename),
        PName<Value>("params") = params,
        PName<Int>("priority") = 1
    ];
    RootEvent("Deliver Mailbox", Value());
}

Value PlaceFreeOrder(Connection* rdb, const Str& email, const Str& last_name, const Str& first_name, const ADate& adate, const Int& week_tariffid, const Int& qty, const Str& promocode) {
    const auto personid = CreateOrFindUser(rdb_, email, last_name, first_name);
    CheckCardsNumber(qty);
    const auto FREE(true);
    const Double discount(0.0);
    WeekTariff tariff(week_tariffid);
    tariff.Select(rdb_);
    const auto amount = tariff.price * (*qty);
    const auto order = InsertOrder(rdb_, personid, amount, FREE, promocode, discount, {});
    
    GenerateOrderlines(rdb_, personid, order.id, adate, tariff, *qty);
    
    Person person(personid);
    person.Select(rdb_);
    
    const auto gates = GetAllEnabledGates(rdb_);
    auto at_least_one_gate_ok(false);
    Str clientoid;
    for(const auto& gate : gates) {
        try {
            clientoid = RegisterInContour(rdb_, order.id, gate, FREE);
            at_least_one_gate_ok = true;
        }
        catch(const Exception& e) {
            Log(5) << "SPD ERROR: Could not register identifier in spd: " << gate.host.get_value_or("") << ":" << gate.port.get_value_or("") << " " << e.What() << endl;
        }
    }
    if(at_least_one_gate_ok) {
        DeliverNotification<PersonOrder>(person.email, ticket_templatename, order);
        Str comment("Бесплатный");
        UpdateIdentifierStatus<PersonIdentifierOrderline>(rdb_, order.id, IDENTIFIER_STATUS_OK, comment, clientoid);
        AddMessage(Message("Free order placed successfully. ").What());
    }
    else {
        Str comment("Ошибка выдачи идентификатора");
        UpdateIdentifierStatus<PersonIdentifierOrderline>(rdb_, order.id, IDENTIFIER_STATUS_CANCELED, comment, Str());
        AddMessage(Message("Error while placing free order occured. ").What());
    }
    
    Value res;
    res["id"] = order.id;
    res["amount"] = Decimal(0.0);
    res["final_amount"] = Decimal(0.0);  
    res["free"] = BOOL_TRUE;
    return res;
}

//----------------------------------------------------------------------
/// Разместить любой заказ
const Value PlaceAnyOrder(Connection* rdb, const Str email, const ADate adate, const Str orderdesc) 
{
    auto amount = 0;

    if(!Computerica::isEmail(email)) {
        throw Exception(Message("Email is not correct.").What());
    }

    std::string _email(email.get_value_or(string())); /// Получаем email
    std::string _orderdesc(orderdesc.get_value_or(string())); /// Получаем описание заказов

    std::vector< std::pair<long, long> > pairs; /// Создаем пару (long,long)   

    char* p = (char*)_orderdesc.c_str();
    do ///...Разбираем строку в пару....................................
        { 
        long _id = 0;
        long _count = 0;

        _id = strtol(p, &p, 10);
        if (*p == 0) 
            throw Exception(Message("Not valid order description string.").What());

        ++p;

        _count = strtol(p, &p, 10);
        if (*p != 0) ++p;

        pairs.push_back(std::make_pair(_id, _count));
        }
    while (*p != 0); ///...Разбираем строку в пару......................

    if ( pairs.size() == 0 )
        throw Exception(Message("Not valid order description string.").What());

    const auto personid = CreateOrFindUser(rdb_, email, Str(), Str()); /// Ищем или создаем пользователя

    for (size_t i = 0; i < pairs.size(); i++) /// Обходим все пары в описании
        {   
        if(!Defined(pairs[i].first))
                throw Exception(Message("Week tariff is not selected.").What());

            CheckCardsNumber(pairs[i].second); /// Проверяем количество карт

            const auto today = (TIME_NOW /*+ Computerica::get_utc_offset()*/ ).date();
            if(adate < today) 
                throw Exception(Message("Cannot place order of previous date.").What()); /// Проверяем дату

            WeekTariff tariff(pairs[i].first);
            tariff.Select(rdb_);

            amount += tariff.price * pairs[i].second; /// Считаем окончательную стоимость
        }

    const auto FREE(true);

    const auto order = InsertOrder(rdb_, personid, amount, !FREE, Str(), Double(0.0), {}); /// Создаем заказ

    for (size_t i = 0; i < pairs.size(); i++) /// Создаем список заказов (идентификаторов)
        {
        WeekTariff tariff(pairs[i].first);

        tariff.Select(rdb_);
        GenerateOrderlines(rdb_, personid, order.id, adate, tariff, pairs[i].second);
        }

    Value res;
    res["id"] = order.id;
    res["amount"] = amount;
    res["final_amount"] = order.final_amount;
    res["free"] = BOOL_FALSE;

    return res;
}

//----------------------------------------------------------------------
/// Реализация создания заказа с промокодом
const Value PlaceAnyOrderOnePromocode(Connection* rdb, const Str& email, const ADate& adate, const Str& orderdesc, const Str& promocode)
{
   auto amount = 0;

    if(!Computerica::isEmail(email)) {
        throw Exception(Message("Email is not correct.").What());
    }
    
    const auto promotion = GetDiscount(promocode, adate); /// Получаем промоакцию на заданую дату по коду
    const auto discount = promotion["discount"].As<Double>(); /// Получаем скидку

    /// Проверка на размер скидки
    //if(discount > (ONE_HUNDRED_PERCENT - EPSILON)) ///- 100 - 0.01, т.е. скидка около 100%, и только 1 билет может иметь эту скидку 
    //    throw Exception(Message("All tickets on 100% discount! ").What()); /// Слишком большая скидка
    
    std::string _email(email.get_value_or(string())); /// Получаем email
    std::string _orderdesc(orderdesc.get_value_or(string())); /// Получаем описание заказов

    Log(5) << __func__ <<" discount: " << discount << endl;
    Log(5) << __func__ <<" order description: " << _orderdesc << endl;

    std::vector< std::pair<long, long> > pairs; /// Создаем пару (long,long)   

    char* p = (char*)_orderdesc.c_str();
    do ///...Разбираем строку в пару....................................
        { 
        long _id = 0;
        long _count = 0;

        _id = strtol(p, &p, 10);
        if (*p == 0) 
            throw Exception(Message("Not valid order description string.").What());

        ++p;

        _count = strtol(p, &p, 10);
        if (*p != 0) ++p;

        pairs.push_back(std::make_pair(_id, _count));
        }
    while (*p != 0); ///...Разбираем строку в пару......................

    if ( pairs.size() == 0 )
        throw Exception(Message("Not valid order description string.").What());

    const auto personid = CreateOrFindUser(rdb_, email, Str(), Str()); /// Ищем или создаем пользователя

    for (size_t i = 0; i < pairs.size(); i++) /// Обходим все пары в описании
        {   
        if(!Defined(pairs[i].first))
                throw Exception(Message("Week tariff is not selected.").What());

            CheckCardsNumber(pairs[i].second); /// Проверяем количество карт

            const auto today = (TIME_NOW /*+ Computerica::get_utc_offset()*/ ).date();
            if(adate < today) 
                throw Exception(Message("Cannot place order of previous date.").What()); /// Проверяем дату

            WeekTariff tariff(pairs[i].first);
            tariff.Select(rdb_);

            amount += tariff.price * pairs[i].second; /// Считаем окончательную стоимость
        }

    const auto FREE(true);
    const auto order = InsertOrder(rdb_, personid, amount, !FREE, promocode, discount, {}); /// Создаем заказ
    
    InvalidatePromocode(promocode); /// Пометить использованным промокод 
        /// TODO:(!) Не совсем верно. Инвалидация кода должна быть после проведения платежа 
        
    for (size_t i = 0; i < pairs.size(); i++) /// Создаем список заказов (идентификаторов)
        {
        WeekTariff tariff(pairs[i].first);

        tariff.Select(rdb_);
        GenerateOrderlines(rdb_, personid, order.id, adate, tariff, pairs[i].second);
        }

    Value res;
    res["id"] = order.id;
    res["amount"] = amount;
    res["final_amount"] = order.final_amount;
    res["free"] = BOOL_FALSE;
    res["discount"] = discount;

    return res;
}
//----------------------------------------------------------------------
const Value PlaceAmountOrder(Connection* rdb, const Str email, const ADate adate, const Str timeoffset, const Str orderdesc) 
{
	auto amount = 0;

	boost::posix_time::time_duration offset = get_utc_offset();
    
	if (!Defined(timeoffset)) {
        	throw Exception(Message("Time offset is not defined.").What());
	}

	std::string _timeoffset(timeoffset.get_value_or(string("0")));

	long nsoff = ( offset.hours() * 60 + offset.minutes() ) * ( offset.is_negative() ? 1 : -1 );
	long ncoff = strtol( _timeoffset.c_str(), 0, 10 );

	Log(0) << "CLIENT offset: " << ncoff << endl;
	Log(0) << "SERVER offset: " << nsoff << endl;

	if ( nsoff != ncoff ) {
        	throw Exception(Message("Error in time offset.").What());
	}
	
//	Computerica::get_utc_offset()

	if(!Computerica::isEmail(email)) {
        	throw Exception(Message("Email is not correct.").What());
	}

	std::string _email(email.get_value_or(string()));
	std::string _orderdesc(orderdesc.get_value_or(string()));

	std::vector< std::pair<long, long> > pairs;    

	char* p = (char*)_orderdesc.c_str();
	do
	{
		long _id = 0;
		long _count = 0;

		_id = strtol(p, &p, 10);

		if (*p == 0) throw Exception(Message("Not valid order description string.").What());
		++p;

		_count = strtol(p, &p, 10);
		if (*p != 0) ++p;

		pairs.push_back(std::make_pair(_id, _count));

	} while (*p != 0);

	if ( pairs.size() == 0 ) throw Exception(Message("Not valid order description string.").What());

	const auto personid = CreateOrFindUser(rdb_, email, Str(), Str());

	for (size_t i = 0; i < pairs.size(); i++)
	{
		if(!Defined(pairs[i].first)) throw Exception(Message("Week tariff is not selected.").What());

		CheckCardsNumber(pairs[i].second);

		WeekTariff tariff(pairs[i].first);
		tariff.Select(rdb_);

		if ( tariff.pricetype == BOOL_FALSE ) {
			amount += tariff.price * pairs[i].second;
		}
	}

	const auto today = (TIME_NOW /*+ Computerica::get_utc_offset()*/ ).date();

//	if(adate < today)

	if(adate < today)
        	throw Exception(Message("Cannot place order on another date.").What());

    	const auto order = InsertOrder(rdb_, personid, amount, (amount == 0) ? true : false, Str(), Double(0.0), {});

    	for (size_t i = 0; i < pairs.size(); i++)
	{	WeekTariff tariff(pairs[i].first);

        	tariff.Select(rdb_);
		GenerateOrderlines(rdb_, personid, order.id, adate, tariff, pairs[i].second);
	}

	if ( amount == 0 ) {
		Str clientoid;

		bool gate_ok = false;

		const auto gates = GetAllEnabledGates(rdb_);
		for(const auto& gate : gates)
		{
			try {
				clientoid = RegisterInContour(rdb_, order.id, gate, true);
				gate_ok = true;
			} catch(const Exception& e) {
				Value res;

				res["description"] = "Could not register in contour";
				res["error"] = BOOL_TRUE;

				Log(1) << "Error while placing free order occured. [ SPD ERROR: registration params is ( " << gate.host.get_value_or("") << ":" << gate.port.get_value_or("") << " " << e.What() << " ) ]" << endl;

				return res;
			}
		}

		if( gate_ok ) {

			Str comment("Бесплатный");
			UpdateIdentifierStatus<PersonIdentifierOrderline>(rdb_, order.id, IDENTIFIER_STATUS_OK, comment, clientoid);

			for (size_t i = 0; i < pairs.size(); i++)
			{
				WeekTariff tariff(pairs[i].first);
				tariff.Select(rdb_);

				if ( tariff.counttype == BOOL_FALSE ) {
					if ( tariff.free.get() < pairs[i].second ) {
						Value res;
						res["error"] = BOOL_TRUE;
						res["description"] = "Insufficient condition";

						Log(1) << "Error while placing free order occured. [ tickets insufficient condition ]" << endl;

						return res;
					}
				}

				int _freetickets = tariff.free.get() - pairs[i].second;
				tariff.free = _freetickets;

				tariff.Update(rdb_);
			}

//			ticket_templatename
//			std::string name("ticket.template");
//			DeliverNotification<PersonOrder>(email, ticket_templatename, order);

			Value res;
			res["id"] = order.id;
			res["amount"] = Decimal(0.0);
			res["final_amount"] = Decimal(0.0);
			res["error"] = BOOL_FALSE;

			return res;

		} else {
			Str comment("Ошибка выдачи идентификатора");
			UpdateIdentifierStatus<PersonIdentifierOrderline>(rdb_, order.id, IDENTIFIER_STATUS_CANCELED, comment, Str());

			Value res;
			res["description"] = "Update Identifier Status( status_canceled )";
			res["error"] = BOOL_TRUE;

			Log(3) << "Error while placing free order occured. [ update identifier status( status_canceled ) ]" << endl;

			return res;
		}
	}

	for (size_t i = 0; i < pairs.size(); i++)
	{
		WeekTariff tariff(pairs[i].first);
		tariff.Select(rdb_);

		if ( tariff.counttype == BOOL_FALSE ) {
			if ( tariff.free.get() < pairs[i].second ) {
				Value res;
				res["error"] = BOOL_TRUE;
				res["description"] = "Insufficient condition";

				Log(3) << "Error while placing free order occured. [ tickets insufficient condition ]" << endl;

				return res;
			}

			int _freetickets = tariff.free.get() - pairs[i].second;
			tariff.free = _freetickets;

			tariff.Update(rdb_);
		}
	}

	Value res;

	res["id"] = order.id;
	res["amount"] = amount;
	res["final_amount"] = order.final_amount;
	res["error"] = BOOL_FALSE;

	return res;
}


const Value BALOONPersonOrder::PlaceAmount_FE(const Str email, const ADate adate, const Str timeoffset, const Str orderdesc) { 
    return PlaceAmountOrder(rdb_, email, adate, timeoffset, orderdesc);
}

//----------------------------------------------------------------------
/// Разместить любой заказ
const Value BALOONPersonOrder::PlaceAny_FE(const Str email, const ADate adate, const Str orderdesc) { 
    return PlaceAnyOrder(rdb_, email, adate, orderdesc);
}
//----------------------------------------------------------------------
/// Разместить любой заказ c промокодом на ВЕСЬ заказ
const Value BALOONPersonOrder::PlaceAnyOnePromocode(const Str email, const ADate adate, const Str orderdesc, const Str promocode) { 
    return PlaceAnyOrderOnePromocode(rdb_, email, adate, orderdesc, promocode);
}
//----------------------------------------------------------------------
const Value BALOONPersonOrder::Place_FE(const Str email, const Str last_name, const Str first_name, const ADate adate, const Int week_tariffid, const Int qty) { 
    return PlaceOrder(rdb_, email, last_name, first_name, adate, week_tariffid, qty, Str(), Double(0.0), {});
}

bool SiteCodeExists(Connection* rdb, const Str& code) {
    if(!Defined(code) || code->empty()) {
        return true;
    }
    OrderSite osite(code);
    return osite.Select(rdb);
}

//----------------------------------------------------------------------
/// Разместить заказ со скидкой на нужную дату
const Value BALOONPersonOrder::Place_WEB(const Str email, const Str last_name, const Str first_name, const ADate adate, const Int week_tariffid, const Int qty, const Str promocode, const Str sitecode) {
    const auto promotion = GetDiscount(promocode, adate); /// Получаем промоакцию на заданую дату по коду
    const auto discount = promotion["discount"].As<Double>(); /// Получаем скидку
    
    Value res;
    if(discount > (ONE_HUNDRED_PERCENT - EPSILON)) {///- 100 - 0.01, т.е. скидка около 100%, и только 1 билет может иметь эту скидку 
         if(qty.get_value_or(0) != 1) {
             throw Exception(Message("Only one ticket on 100% discount. ").What()); /// Слишком большая скидка
         }
         res = PlaceFreeOrder(rdb_, email, last_name, first_name, adate, week_tariffid, qty, promocode); /// Разместить свободный заказ?
         InvalidatePromocode(promocode); /// Пометить использованным промокод?
    }
    else {
        const auto sitecode_ = SiteCodeExists(rdb_, sitecode)? sitecode : Str(); /// Код сайта (предположительно)
        res = PlaceOrder(rdb_, email, last_name, first_name, adate, week_tariffid, qty, promocode, discount, sitecode_); // Размещаем заказ
    }
    return res;
}
//----------------------------------------------------------------------
const Value BALOONPersonOrder::PlaceFree(const Str email, const Str last_name, const Str first_name, const ADate adate, const Int week_tariffid, const Int qty) {
    return PlaceFreeOrder(rdb_, email, last_name, first_name, adate, week_tariffid, qty, Str());
}

void GenerateOrderlines(Connection* rdb, const Int& personid, const Int orderid, const SkipassConfiguration& config, int qty) {
    for(int i = 0; i < qty; ++i) {
        Identifier identifier;
        identifier.code = GenerateCode(rdb);
        identifier.valid_from = config.valid_from;
        identifier.valid_to = config.valid_to;
        identifier.personid = personid;
        identifier.permanent_rulename = config.identifier_rulename;
        identifier.categoryname = config.identifier_categoryname;
        identifier.ratename = config.ratename;
        identifier.name = config.name;
        identifier.groupname = config.groupname;
        const auto identifierid = identifier.Insert(rdb);

        PersonIdentifierOrderline oline;
        oline.orderid = orderid;
        oline.identifierid = identifierid;
        oline.price = config.price;
        oline.service_rulename = config.service_rulename;
        oline.make_transaction = BOOL_FALSE;
        oline.Insert(rdb);
    }
}

//----------------------------------------------------------------------
/// Проверка промокода на заданную дату
const Value BALOONPersonOrder::CheckPromocode_API(const Str promocode, const ADate adate) {
    return GetDiscount(promocode, adate);
}
//----------------------------------------------------------------------
Value BuySkipass(Connection* rdb, const Int& personid, const Int& skipass_configurationid, const Int& qty) {
    CheckCardsNumber(qty);
    SkipassConfiguration config(skipass_configurationid);
    config.Select(rdb_);
    if(config.enabled != ENABLED) {
        throw Exception(Message("Skipass of selected type cannot be sold. ").What());
    }
    const auto amount = config.price * (*qty);
    const auto FREE(true);
    const Str promocode;
    const Double discount(0.0);
    const auto order = InsertOrder(rdb, personid, amount, !FREE, promocode, discount, {});
    GenerateOrderlines(rdb, personid, order.id, config, *qty);

    Value res;
    res["id"] = order.id;
    res["amount"] = amount;
    return res;
}

const Value BALOONPersonOrder::BuySkipass_API(const Str email, const Str last_name, const Str first_name, const Int skipass_configurationid, const Int qty) {
    const auto personid = CreateOrFindUser(rdb_, email, last_name, first_name);
    return BuySkipass(rdb_, personid, skipass_configurationid, qty);
}

const Value BALOONPersonOrder::MyBuySkipass_API(const Int skipass_configurationid, const Int qty) {
    const auto personid = checkAndGetCurrentPersonId(rdb_);
    return BuySkipass(rdb_, personid, skipass_configurationid, qty);
}

const Value BALOONPersonOrder::RequestPaymentStatus() {
    Event("Synchronize Payments", Value());
    return Value();
}

const Value BALOONPersonOrder::GetPreliminaryOrderAmount_FE(const ADate adate, const Str identifier_rulename, const Str service_rulename, const Int qty) {
    Value res;
    if(!Defined(adate) || !Defined(qty) || !(*qty)) {
        res["amount"] = Decimal(0.);
        return res;
    }
    Selector sel;
    IdentifierServiceRule is_rule;
    sel << is_rule;
    sel.Where(is_rule->identifier_rulename == identifier_rulename && is_rule->service_rulename == service_rulename);
    auto ds = sel.Execute(rdb_);
    ds.Fetch();
    const auto itemprice = GetPrice(rdb_, is_rule, adate);
    const auto amount = itemprice * (*qty);
    res["amount"] = amount;
    return res;
}

const Value BALOONPersonOrder::FindByIdentifier(const Str code) {
    Data::DataList lr;
    lr.AddColumn("id", Data::INTEGER);
    lr.AddColumn("personemail", Data::STRING);
    lr.AddColumn("filed_at", Data::DATETIME);
    lr.AddColumn("amount", Data::DECIMAL);
    lr.AddColumn("status", Data::STRING);
    
    if(!Defined(code)) {
        return lr;
    }
    
    PersonIdentifierOrderline oline;
    Identifier identifier;
    PersonOrder order;
    Person person;
    
    lr.Bind(order.id, "id");
    lr.Bind(person.email, "personemail");
    lr.Bind(order.filed_at, "filed_at");
    lr.Bind(order.amount, "amount");
    lr.Bind(order.status, "status");
    
    Selector sel;
    
    sel << Distinct()
        << order->id
        << person->email
        << order->filed_at
        << order->amount
        << order->status;
    sel.From(order)
        .Join(person).On(order->personid == person->id)
        .Join(oline).On(oline->orderid == order->id)
        .Join(identifier).On(oline->identifierid == identifier->id);
    sel.Where(identifier->code.Like(string("%") + *code + "%"));
    auto ds = sel.Execute(rdb_);
    if(!ds.Rows()) {
        AddMessage(Message("No orders found. ").What());
        return lr;
    }
    while(ds.Fetch()) {
        lr.AddRow();
    }
    return lr;
}

const Value BALOONPersonOrder::MyGetStatus_FE(const Int id) {
    PersonOrder order(id);
    order.Select(rdb_);
    Value res;
    res["status"] = order.status;
    return res;
}

Value SyncPaymentStatus(Connection* rdb, const Int& id) {
    PersonInvoice invoice;
    Selector sel;
    sel << invoice->id;
    sel.Where(invoice->orderid == id);
    sel.Limit(1);
    auto ds = sel.Execute(rdb);
    if(!ds.Fetch()) {
        return  Value();
    }
    const auto res = RootMethodInvoker("Paygate", "Invoice", "SyncInvoice")[
        PName<Int>("id") = invoice.id
    ];
    return res;
}

const unsigned int CHECKABLE_EXCEPTION = 0; //<---do not modify this value
const unsigned int NON_CHECKABLE_EXCEPTION = 1; 

PersonOrder GetOrderByTransaction(Connection* rdb, const Str trxid) {
    Invoice invoice;
    PersonInvoice personinvoice;
    PersonOrder order;

    Selector sel;
    sel << order;
    sel.From(order)
        .Join(personinvoice).On(personinvoice->orderid == order->id)
        .Join(invoice).On(invoice->id == personinvoice->id);
    sel.Where(invoice->trxid == trxid);
    auto ds = sel.Execute(rdb_);
    
    if(!ds.Fetch()) {
        Log(5) << "-----ERROR: No orders correspond to given trxid: " << trxid << endl;
        const auto no_orders_msg = Message("No orders correspond to given id. ").What();
        throw Exception(NON_CHECKABLE_EXCEPTION, no_orders_msg.c_str());
    }
    return order;
}

const Value BALOONPersonOrder::MySyncStatus_FE(const Int id) {
    return SyncPaymentStatus(rdb_, id);
}

PAYGATE::Invoice GetInvoice(Connection* rdb, const Str& trxid) {
    PAYGATE::Invoice invoice;
    Selector sel;
    sel << invoice;
    sel.Where(invoice->trxid == trxid);
    auto ds = sel.Execute(rdb);
    if(!ds.Fetch()) {
        const auto message("No invoice found: trxid = " + trxid.get_value_or(string()));
    }
    return invoice;
}

Int GetOrderId(Connection* rdb, const Int& invoiceid) {
    PersonInvoice orderinv;
    Selector sel;
    sel << orderinv->orderid;
    sel.Where(orderinv->id == invoiceid);
    auto ds = sel.Execute(rdb);
    ds.Fetch();
    return orderinv.orderid;
}

const std::string ProcessReporterText(const std::string& text, const Value& params) {
    BlobFile blobFile;
    blobFile.Content() = Blob(text.size(), text.c_str());
    blobFile.SetFileName("body.txt");
    BlobFile res = RootMethodInvoker("Reporter", "PublicApi", "GetReport") [
        PName<string>("InFormat") = "plaintext",
        PName<BlobFile>("QueryFile") = blobFile,
        PName<Value>("QueryArgs") = params
    ];
    return std::string(res.Content().Data(), res.Content().Size());
}

std::string RepeatValue( std::string param, std::string value )
{
    return std::string();
}

std::string ReplaceValue( std::string param, std::string value )
{
    return std::string();
}

std::string ProcessTemplateReporterText( std::string text, const Value params )
{
    return std::string();
}

BlobFile MakeTicketsBlobFile(Connection* rdb, const Int& orderid, const string& ticket_template) 
{
    Value params;

    params["id"] = orderid;
    const auto path("templates/" + ticket_template);

    const auto prepared = Computerica::Templating::processFile(path, params);
  
//    std::string body = ProcessTemplateReporterText( prepared, params );

    const auto body = ProcessReporterText(prepared, params);
    auto& converter = Computerica::HtmlToPdfConverter::Instance();
    const auto blob = converter.ConvertKit(body);    
    BlobFile res;
    const string filename("tickets.pdf");
    res.SetFileName(filename);
    res.Content() = *blob;
    return res;
}

const Value BALOONPersonOrder::Download_FE(const Str trxid) {
    auto invoice = GetInvoice(rdb_, trxid);
    const auto status = invoice.status;
    if(status == PAYMENT_STATUS_WAITING) {
        throw Exception(CHECKABLE_EXCEPTION, "Invoice status was not updated. ");
    }
    else if(status == PAYMENT_STATUS_PAYED) {
        const auto orderid = GetOrderId(rdb_, invoice.id);
        const auto res = MakeTicketsBlobFile(rdb_, orderid, *ticket_templatename);
        return res;
    }
    return Value();
}

const Value BALOONPersonOrder::MyOrderListGet_FE() {
    const auto personid = checkAndGetCurrentPersonId(rdb_);
    PersonOrder order;
    PersonInvoice person_invoice;
    Invoice invoice;
    Selector sel;
    sel << order->id
        << order->filed_at
        << order->amount;
    sel.From(order)
        .Join(person_invoice).On(person_invoice->orderid == order->id)
        .Join(invoice).On(person_invoice->id == invoice->id);
    sel.Where(order->personid == personid && invoice->status == PAYMENT_STATUS_PAYED);
    auto ds = sel.Execute(rdb_);
    Value res;
    uint i = 0;
    while(ds.Fetch()) {
        Value item;
        
        item["id"] = order.id;
        item["filed_at"] = order.filed_at;
        item["amount"] = order.amount;
        
        res[i] = item;
        ++i;
    }
    return res;
}

void CheckOrderIsMine(Connection* rdb, const Int& orderid) {
    PersonOrder order(orderid);
    order.Select(rdb_);
    const auto personid = checkAndGetCurrentPersonId(rdb_);
    if(order.personid != personid) {
        throw Exception("Invalid order ID. ");
    }
}

void CheckOrderIsPayed(Connection* rdb, const Int& orderid) {
    PersonInvoice person_invoice;
    Invoice invoice;
    Selector sel;
    sel << invoice->status;
    sel.From(invoice).Join(person_invoice).On(invoice->id == person_invoice->id);
    sel.Where(person_invoice->orderid == orderid);
    auto ds = sel.Execute(rdb_);
    if(!ds.Fetch() || invoice.status != PAYMENT_STATUS_PAYED) {
        throw Exception("Invoice is not payed or does not exist. ");
    }
}

const Value BALOONPersonOrder::MyDownload_FE(const Int id) {

    std::string name("ticket.template");

//    CheckOrderIsMine(rdb_, id);
//    CheckOrderIsPayed(rdb_, id);
    const auto res = MakeTicketsBlobFile(rdb_, id, name);
    return res;
}

/*
    Value params;
    params["id"] = id;

    const std::string path("templates/ticket.template");
    const auto prepared = Computerica::Templating::processFile(path, params);

    BlobFile res;
    const string filename("tickets.pdf");
    res.SetFileName(filename);
    res.Content() = blob;
    return res;
*/

//    CheckOrderIsMine(rdb_, id);
//    CheckOrderIsPayed(rdb_, id);

//    Value params;
//    params["id"] = id;


//    Log(5) << "Templating::processFile " << path << endl;

//    std::string body = "";
//    if (path.length() <= 1024 && boost::filesystem::exists(path)) 
//	body = ReadFile(path);
  //  else body = path;


//    const auto body = Computerica::Templating::processFile(path, params);
//    Blob blob(body.length(), body.c_str());


//    const auto body = ProcessReporterText(prepared, params);
//    const auto body = prepared;

//    auto& converter = Computerica::HtmlToPdfConverter::Instance();

//    const auto blob = converter.ConvertKit(body);    
//    BlobFile res;
//    const string filename("tickets.pdf");
//    res.SetFileName(filename);
//    res.Content() = blob;

//    const auto res = MakeTicketsBlobFile(rdb_, id, *ticket_templatename);
//    return res;

//    return res;
//}

Time DateToTime(const ADate& adate) {
    return Time(adate, boost::posix_time::hours(0) );
}

Expression OrderInInterval(const PersonOrder& order, const ADate& startd, const ADate& endd) {
    return !order->promocode.IsNull() &&
            order->filed_at >= DateToTime(startd) &&
            order->filed_at <= DateToTime(endd + boost::gregorian::days(1)) &&
            order->status == PERSON_ORDER_STATUS_PAYED;
}

map<Str, int> GetNumberOfTicketsByPromocode(Connection* rdb, const ADate& start_date, const ADate& end_date) {
    PersonIdentifierOrderline oline;
    PersonOrder order;
    Selector sel;
    
    int count;
    sel << oline->identifierid.Count().Bind(count)
        << order->promocode;
    sel.From(oline).Join(order).On(oline->orderid == order->id);
    sel.Where(OrderInInterval(order, start_date, end_date));
    sel.GroupBy(order->promocode);
    auto ds = sel.Execute(rdb);
    map<Str, int> res;
    while(ds.Fetch()) {
        res.insert({ order.promocode, count });
    }
    return res;
}

//----------------------------------------------------------------------
/// Генерация отчета по промокодам
const Value BALOONPersonOrder::PromotionSaleListGet(const ADate start_date, const ADate end_date) {
    PersonOrder order;
    PersonIdentifierOrderline oline;
    Selector sel;
    Decimal total_amount;
    Double total_amount_;
    Int use_count;
    int orderline_count;
    
    sel << order->promocode
        << order->final_amount.Sum().Bind(total_amount_)
        << order->id.Count().Bind(use_count);
    sel.Where(OrderInInterval(order, start_date, end_date));
    sel.GroupBy(order->promocode);
    auto ds = sel.Execute(rdb_);
    
    Data::DataList lr;
    lr.AddColumn("promocode", Data::STRING);
    lr.AddColumn("use_count", Data::INTEGER);
    lr.AddColumn("orderline_count", Data::INTEGER);
    lr.AddColumn("total_amount", Data::DECIMAL);
    
    lr.Bind(order.promocode, "promocode");
    lr.Bind(use_count, "use_count");
    lr.Bind(orderline_count, "orderline_count");
    lr.Bind(total_amount, "total_amount");
    
    auto olines = GetNumberOfTicketsByPromocode(rdb_, start_date, end_date);
    
    while(ds.Fetch()) {
        auto iter = olines.find(order.promocode);
        orderline_count = (iter == olines.cend()? 0 : iter->second);
        total_amount = Decimal(total_amount_);
        lr.AddRow();
    }
    return lr;
}
//----------------------------------------------------------------------
const Value BALOONParkingArea::ParkingAreaListGet_API() {
    ParkingArea area;
    Selector sel;
    sel << area->id
        << area->name
        << area->enter_timeout
        << area->exit_timeout;
    auto ds = sel.Execute(rdb_);
    uint i(0);
    Value res;
    while(ds.Fetch()) {
        Value item;
        item["id"] = area.id;
        item["name"] = area.name;
        item["enter_timeout"] = area.enter_timeout;
        item["exit_timeout"] = area.exit_timeout;
        res[i] = item;
        ++i;
    }
    return res;
}

time_t ToUnixTime(const Time& t) {
    const Time epoch(boost::gregorian::date(1970, 1, 1));
    const boost::posix_time::time_duration dur(t - epoch);
    return dur.total_seconds();
}

auto ResultValue = [](const Time& t = {}, int val = 0) {
    Value v;
    const auto end_time_unix = Defined(t)? static_cast<long int>(ToUnixTime(t)) : 0l;
    v["end_time"] = end_time_unix;
    v["seconds_left"] = val;
    return v;
};

const Value BALOONParkingTimer::TimeLeftGet_API(const Str code) {
    Identifier identifier;
    ParkingOrderline oline;
    Selector sel;
    sel << oline->orderid;
    sel.From(oline).Join(identifier).On(oline->identifierid == identifier->id);
	sel.Where(identifier->code == code);
    sel.OrderDesc(oline->id);
    sel.Limit(1);
    auto ds = sel.Execute(rdb_);
    if(!ds.Fetch()) {
        return ResultValue();
    }
    ParkingTimer ptimer(oline.orderid);
    if(!ptimer.Select(rdb_)) {
        return ResultValue();
    }
    const auto now = TIME_NOW;
    const auto end_time = ptimer.end_time;

    if(now >= end_time) {
        return ResultValue(end_time);
    }
    const boost::posix_time::time_period tp(now, end_time);
    return ResultValue(end_time, tp.length().total_seconds());
}

ParkingArea ParkingAreaGet(Connection* rdb, const Int& areaid) {
    ParkingArea area(areaid);
    area.Select(rdb);
    return area;
}

Time SaveParkingTimer(Connection* rdb, const Int& orderid, const Time& now, const Int& exit_timeout_min) {
    ParkingTimer timer;
    timer.orderid = orderid;
    const auto end_time = now + boost::posix_time::minutes(exit_timeout_min.get_value_or(0));
    timer.end_time = end_time;
    timer.Insert(rdb);
    return end_time;
}

const Value BALOONParkingTimer::Start_API(const Str code, const Str sekretkey) {
    if(sekretkey != Str("Qp47E22XSrCXO0y2YFzqFnpOxwMWOlBq")) {
        throw Exception("Access denied. ");
    }
    ParkingOrderline oline;
    Identifier identifier;
    Selector sel;
    sel << oline->areaid
        << oline->orderid;
    sel.From(oline).Join(identifier).On(oline->identifierid == identifier->id);
    sel.Where(identifier->code == code);
    sel.OrderDesc(oline->id);
    sel.Limit(1);
    auto ds = sel.Execute(rdb_);
    if(!ds.Fetch()) {
        return ResultValue();
    }
    const auto areaid = oline.areaid;
    if(!Defined(areaid)) {
        return ResultValue();
    }
    const auto area = ParkingAreaGet(rdb_, areaid);
    
    const auto now = TIME_NOW;
    const auto end_time = SaveParkingTimer(rdb_, oline.orderid, now, area.exit_timeout);
    const boost::posix_time::time_period tp(now, end_time);
    return ResultValue(end_time, tp.length().total_seconds());
}

SpdGate ParkingAreaGateGet(Connection* rdb, const Int areaid) {
    SpdGate spdgate;
    ParkingArea area;
    Selector sel;
    sel << spdgate;
    sel.From(spdgate).Join(area).On(area->spdgateid == spdgate->id);
    sel.Where(area->id == areaid);
    auto ds = sel.Execute(rdb);
    ds.Fetch();
    return spdgate;
}

SpdIdentifier RequestParkingIdentifierInfo(const std::string& code, const SpdGate& spdgate) {
    SpdClient client;
    client.spd_host = spdgate.host;
    client.spd_port = spdgate.port;
    client.identifier = code;
    
    SpdIdentifier identifier;
    identifier.calc_time_cost_get = true;
    identifier.calc_time_cost_pay = false;
    identifier.code = code;
    identifier.value = code;
    client.identifiers.push_back(identifier);
    const auto response = SendSpdRequest(client);
    if(response.clients.empty()) {
        throw Exception(Message("No clients found in SPD response. ").What());
    }
    const auto& identifiers = response.clients[0].identifiers;
    if(identifiers.empty()) {
        throw Exception(Message("No identifiers found in SPD response. ").What());
    }
    return identifiers[0];
}

Int SaveParkingOrder(Connection* rdb, const Int& personid, const Decimal& cost) {
    PersonOrder order;
    order.personid = personid;
    order.filed_at = TIME_NOW;
    order.status = PERSON_ORDER_STATUS_WAITING;
    order.amount = cost;
    order.final_amount = cost;
    const auto orderid = order.Insert(rdb);
    return orderid;
}

Int FindParkingIdentifier(Connection* rdb, const Int& personid, const Str& code, const SpdGate& spdgate) {
    Identifier identifier;
    Selector sel;
    sel << identifier->id
        << identifier->personid
        << identifier->spdgateid;
    sel.Where(identifier->code == code);
    auto ds = sel.Execute(rdb);
    if(!ds.Fetch()) {
        return {};
    }
    if(identifier.personid != personid) {
        throw Exception(Message("Identifier error: selected identifier does not belong to current user. ").What());
    }
    if(identifier.spdgateid != spdgate.id) {
        throw Exception(Message("Identifier error: selected identifier does not exist in current parking area. ").What());
    }
    return identifier.id;
}

Int SaveParkingIdentifier(Connection* rdb, const Int& personid, const SpdIdentifier& spd_identifier, const SpdGate& spdgate) {
    Identifier identifier;
    identifier.code = spd_identifier.code;
    identifier.valid_from = spd_identifier.valid_from;
    identifier.valid_to = spd_identifier.valid_to;
    identifier.personid = personid;
    identifier.name = Str("Parking");
    identifier.comment = spd_identifier.comment;
    identifier.permanent_rulename = spd_identifier.permanent_rule;
    identifier.categoryname = spd_identifier.category;
    identifier.ratename = spd_identifier.tariff;
    identifier.status = IDENTIFIER_STATUS_OK;
    identifier.spdgateid = spdgate.id;
    const auto id = identifier.Insert(rdb);
    return id;
}

Int FindOrCreateParkingIdentifier(Connection* rdb, const Int& personid, const SpdIdentifier& spd_identifier, const SpdGate& spdgate) {
    const auto identifierid = FindParkingIdentifier(rdb, personid, spd_identifier.code, spdgate);
    if(Defined(identifierid)) {
        return identifierid;
    }
    return SaveParkingIdentifier(rdb, personid, spd_identifier, spdgate);
}

Int SaveParkingOrderline(Connection* rdb, const Int& orderid, const Int& areaid, const Int identifierid, const SpdIdentifier& spd_identifier) {
    ParkingOrderline oline;
    oline.orderid = orderid;
    oline.identifierid = identifierid;
    oline.time_from = spd_identifier.start_time /* - Computerica::get_utc_offset() */;
    oline.time_upto = spd_identifier.end_time /*- Computerica::get_utc_offset() */;
    oline.price = spd_identifier.pay;
    oline.areaid = areaid;
    const auto id = oline.Insert(rdb);
    return id;
}

const Value BALOONPersonOrder::ParkingOrderPlace_API(const Int areaid, const Str email, const Str code) {
    if(StrEmpty(code)) {
        throw Exception(Message("Identifier code is not defined. ").What());
    }
    if(StrEmpty(email)) {
        throw Exception(Message("Client email is not defined. ").What());
    }
    const Str last_name("Parking");
    const Str first_name("Client");
    const auto spdgate = ParkingAreaGateGet(rdb_, areaid);
    
    const auto personid = CreateOrFindUser(rdb_, email, last_name, first_name);
    const auto spd_identifier = RequestParkingIdentifierInfo(*code, spdgate);
    const auto amount = spd_identifier.pay;
    
    const auto orderid = SaveParkingOrder(rdb_, personid, amount);    
    const auto identifierid = FindOrCreateParkingIdentifier(rdb_, personid, spd_identifier, spdgate);
    SaveParkingOrderline(rdb_, orderid, areaid, identifierid, spd_identifier);
    
    Value res;
    res["id"] = orderid;
    res["amount"] = amount;
    res["final_amount"] = amount;
    res["free"] = BOOL_FALSE;
    return res;
}

const Value BALOONPersonOrder::ParkingIdentifierInfoGet_API(const Int areaid, const Str code) {
    if(!Defined(code)) {
        throw Exception(Message("Identifier is not defined. ").What());
    }
    if(!Defined(areaid)) {
        throw Exception(Message("Parking area has not been selected. ").What());
    }
    SpdGate gate;
    ParkingArea area;
    Selector sel;
    sel << gate->host
        << gate->port;
    sel.From(gate).Join(area).On(area->spdgateid == gate->id);
    sel.Where(area->id == areaid);
    auto ds = sel.Execute(rdb_);
    if(!ds.Fetch()) {
        throw Exception(Message("Parking area not found. See system settings. ").What());
    }
    const auto identifier = RequestParkingIdentifierInfo(*code, gate);
    Value res;
    const auto time_from = identifier.start_time;
    const auto time_upto = identifier.end_time;
    const auto time_period = boost::posix_time::time_period(time_from, time_upto);
    const auto seconds = time_period.length().total_seconds();
    
    res["time_from"] = time_from;
    res["time_upto"] = time_upto;
    const auto MINUTES_IN_HOUR(60);
    res["duration_minutes"] = seconds / MINUTES_IN_HOUR;
    res["amount"] = identifier.pay;
    res["message"] = identifier.comment;
    return res;
}

const string ttimeToString(ATime time) {
    std::ostringstream os;
    boost::posix_time::time_facet* facet = new boost::posix_time::time_facet();
    facet->format("%H:%M");
    os.imbue(std::locale(std::locale::classic(), facet));
    os << time;
    return os.str();
}

const Value BALOONPersonIdentifierOrderline::PersonIdentifierOrderlineListGet(const optional<Int> orderid, const optional<Int> identifierid) {
    Data::DataList lr;
    lr.AddColumn("orderid", Data::INTEGER);
    lr.AddColumn("identifiercode", Data::STRING);
    lr.AddColumn("identifierpermanent_rulename", Data::STRING);
    lr.AddColumn("identifiervalid_from", Data::DATETIME);
    lr.AddColumn("identifiervalid_to", Data::DATETIME);
    lr.AddColumn("identifierratename", Data::STRING);
    lr.AddColumn("identifiername", Data::STRING);
    lr.AddColumn("identifiergroupname", Data::STRING);
    lr.AddColumn("price", Data::DECIMAL);
    lr.AddColumn("identifierstatus", Data::STRING);
    lr.AddColumn("identifiercomment", Data::STRING);
    lr.AddColumn("service_rulename", Data::STRING);
    lr.AddColumn("qr_base64", Data::STRING);
    lr.AddColumn("date", Data::STRING);
    lr.AddColumn("barcode", Data::STRING);
//    lr.AddColumn("tarifftime_from", Data::DATETIME);
//    lr.AddColumn("tarifftime_upto", Data::DATETIME);
    
    PersonOrder order;
    Identifier identifier;
    PersonIdentifierOrderline oline;

    string qr_base64;
    string adate;
    string tarifftime_from;
    string tarifftime_upto;
    string barcode;

    WeekTariff t(orderid.get());
    t.Select(rdb_);

    tarifftime_from = ttimeToString(t.time_from);
    tarifftime_upto = ttimeToString(t.time_upto);

    lr.Bind(order.id, "orderid");
    lr.Bind(identifier.code, "identifiercode");
    lr.Bind(identifier.permanent_rulename, "identifierpermanent_rulename");
    lr.Bind(identifier.valid_from, "identifiervalid_from");
    lr.Bind(identifier.valid_to, "identifiervalid_to");
    lr.Bind(identifier.ratename, "identifierratename");
    lr.Bind(identifier.name, "identifiername");
    lr.Bind(identifier.groupname, "identifiergroupname");
    lr.Bind(oline.price, "price");
    lr.Bind(identifier.status, "identifierstatus");
    lr.Bind(identifier.comment, "identifiercomment");
    lr.Bind(oline.service_rulename, "service_rulename");
    lr.Bind(qr_base64, "qr_base64");
    lr.Bind(adate, "date");
    lr.Bind(barcode, "barcode");
//    lr.Bind(tarifftime_from, "tarifftime_from");
//    lr.Bind(tarifftime_upto, "tarifftime_upto");
    
    Selector sel;
    sel << order->id
        << identifier->code
        << identifier->permanent_rulename
        << identifier->valid_from
        << identifier->valid_to
        << identifier->ratename
        << identifier->name
        << identifier->groupname
        << identifier->status
        << identifier->comment
        << oline->price
        << oline->service_rulename;
    sel.From(oline)
        .Join(order).On(oline->orderid == order->id)
        .Join(identifier).On(oline->identifierid == identifier->id);
    sel.Where((Defined(orderid) ? oline->orderid == *orderid : Expression()) && 
              (Defined(identifierid) ? oline->identifierid == *identifierid : Expression()));
    auto ds = sel.Execute(rdb_);
    while(ds.Fetch()) {
        const auto code = identifier.code;
        if(Defined(code)) try {
            const auto blob = generateQRCode(*code, QRCODE_RESOLUTION);
            qr_base64 = blob->ToBase64();
            barcode = Computerica::code128c_font_encode(*code);
        }
        catch(const Exception& e) {
            Log(5) << "~~~~ERROR: " << e.What() << std::endl;
        }
        adate = dateToString((identifier.valid_from /* + Computerica::get_utc_offset()*/).date());
        lr.AddRow();
        qr_base64.clear();
    }
    return lr;
}

Value MyOrderlineListGet(Connection* rdb, const Int& orderid, bool with_qr_code) {
    if(!Defined(orderid)) {
        return Value();
    }
    PersonOrder order(orderid);
    order.Select(rdb_);
    
    if(order.status != PERSON_ORDER_STATUS_PAYED) {
        throw Exception(Message("Order has no payed status. ").What());
    }
    const auto personid = checkAndGetCurrentPersonId(rdb);
    if(order.personid != personid) {
        throw Exception(Message("MyOrderlinListGet(): Forbidden. ").What());
    }
    
    Identifier identifier;
    PersonIdentifierOrderline oline;
    Selector sel;
    sel << identifier
        << oline->price;
    sel.From(identifier).Join(oline).On(oline->identifierid == identifier->id);
    sel.Where(oline->orderid == orderid);
    auto ds = sel.Execute(rdb_);
    Value res;
    uint i = 0;
    while(ds.Fetch()) {
        Value entry;
        const auto code = identifier.code;
        entry["name"] = identifier.name;
        entry["code"] = code;
        if(with_qr_code) {
            const auto qr_blob = generateQRCode(*code, QRCODE_RESOLUTION);
            entry["qr_base64"] = *qr_blob;
        }
        entry["valid_from"] = identifier.valid_from;
        entry["valid_to"] = identifier.valid_to;
        
        res[i] = entry;
        ++i;
    }
    return res;
}

const auto WITH_QR_CODE(true);

const Value BALOONPersonIdentifierOrderline::PersonIdentifierOrderlineListGet_FE(const Int orderid) {
    return MyOrderlineListGet(rdb_, orderid, WITH_QR_CODE);
}

const Value BALOONPersonIdentifierOrderline::MyOrderlineListGet_FE(const Int orderid) {
    return MyOrderlineListGet(rdb_, orderid, !WITH_QR_CODE);
}

template<typename OL>
vector<Value> MakeItems(Connection* rdb, const PersonOrder& order, vector<Value>& items) {
    OL oline;
    Identifier identifier;
    Selector sel;
    sel << oline
        << identifier->name;
    sel.From(oline).Join(identifier).On(oline->identifierid == identifier->id);
    sel.Where(oline->orderid == order.id);
    auto ds = sel.Execute(rdb);
    while(ds.Fetch()) {
        Value entry;
        entry["name"] = identifier.name;
        entry["qty"] = Int(1);
        entry["price"] = oline.price;
        entry["amount"] = oline.price;
        entry["payment_object"] = Str(FISCAL::FISCAL_PAYMENT_OBJECT_SERVICE);
        items.push_back(entry);
    }
    return items;
}

Value MakeItems(Connection* rdb, const PersonOrder& order) {
    vector<Value> items;
    MakeItems<PersonIdentifierOrderline>(rdb, order, items);
    MakeItems<ParkingOrderline>(rdb, order, items);
    Value res;
    uint i(0);
    for(const auto& item : items) {
        res[i] = item;
        ++i;
    }
    return res;
}

const Str SELL_TYPE("sell");
const Str REFUND_TYPE("sell_refund");

Value FiscalizeOrder(Connection* rdb, PersonOrder& order, const Invoice& invoice, const Str& client_email, const Str doctype) {
    const auto fiscalgateid = GetDefaultFiscalGate(rdb);
    Value res = RootMethodInvoker("Fiscal", "FiscalDoc", "RaiseFiscalDoc") [
        PName<Int>("fiscalgateid") = fiscalgateid,
        PName<Str>("doc_type") = doctype,
        PName<Decimal>("total_amount") = order.final_amount,
        PName<Str>("client_email") = client_email,
        PName<Str>("ext_orderid") = ToString(*order.id),
        PName<Str>("ext_order_type") = invoice.order_type,
        PName<Str>("comment") = Str(),
        PName<Value>("items") = MakeItems(rdb, order)
    ];
    return res;
}

struct Orderline {
    string identifier_code;
    Decimal orderline_price;
};

map<string, vector<Orderline>> GetClientIdentifiers(Connection* rdb, const Int& orderid) {
    PersonIdentifierOrderline oline;
    Identifier identifier;
    Selector sel;
    sel << identifier->code
        << identifier->clientoid
        << oline->price;
    sel.From(oline).Join(identifier).On(oline->identifierid == identifier->id);
    sel.Where(oline->orderid == orderid &&
              !identifier->clientoid.IsNull() &&
              (identifier->status.IsNull() || identifier->status != IDENTIFIER_STATUS_CANCELED));
    auto ds = sel.Execute(rdb);
    
    map<string, vector<Orderline>> olines_by_clientoid;
    while(ds.Fetch()) {
        const auto clientoid = identifier.clientoid.get_value_or(string());
        auto& olines = olines_by_clientoid[clientoid];
        Orderline item;
        item.identifier_code = identifier.code.get_value_or(string());
        item.orderline_price = oline.price;
        olines.push_back(item);
    }
    return olines_by_clientoid;
}

void CancelAll(Connection* rdb, map<string, vector<Orderline>>& olines_by_clientoid, const string& cancel_reason, const vector<SpdGate>& gates) {
    if(gates.empty()) {
        return;
    }
    const auto& gate = gates[0];
    
    for(auto& item : olines_by_clientoid) {
        const auto clientoid = item.first;
        auto& olines = item.second;
        SpdClient client;
        client.oid = clientoid;
        client.eid = Effi::GenerateGUID();
        client.spd_host = gate.host;
        client.spd_port = gate.port;
        for(auto& oline : olines) {
            SpdTransaction transaction;
            transaction.summ = -oline.orderline_price;
            transaction.currency = RUB_CURRENCY;
            transaction.eid = Effi::GenerateGUID();
            transaction.comment = cancel_reason;
            
            SpdIdentifier spd_identifier;
            spd_identifier.cancel = true;
            spd_identifier.comment = cancel_reason;
            spd_identifier.code = oline.identifier_code;
            spd_identifier.eid = Effi::GenerateGUID();
            
            client.transactions.push_back(transaction);
            client.identifiers.push_back(spd_identifier);
        }
        SendSpdRequest(client);
    }
}

void InvalidateIdentifiers(Connection* rdb, const Int& orderid, const string& cancel_reason, const vector<SpdGate>& gates) {
    auto identifiers_to_cancel = GetClientIdentifiers(rdb, orderid);
    CancelAll(rdb, identifiers_to_cancel, cancel_reason, gates);
}

void SavePersonOrderFiscalDoc(Connection* rdb, const Int& orderid, const Int& fiscal_docid) {
    PersonOrderFiscalDoc pofd;
    pofd.orderid = orderid;
    pofd.fiscal_docid = fiscal_docid;
    pofd.Insert(rdb);
}

//----------------------------------------------------------------------
/// Пометить промокод как использованный
void InvalidatePromocode(const Str& promocode) {
    if(!Defined(promocode)) {
        return;
    }
    try {
        RootMethodInvoker("Loyalty", "PromotionCode", "SetUsed")[
            PName<Str>("barcode") = promocode
        ];
    }
    catch(const Exception& e) {
        
    }
}
//----------------------------------------------------------------------
class order_payment {
public:
    order_payment(PersonOrder* po, Invoice* invoice, Connection* rdb) : order_(po), invoice_(invoice), conn_(rdb), res_() {
        res_["orderid"] = order_->id;
    }
    virtual void UpdateOrderStatus() = 0;
    virtual void FiscalizeOrder(const Str& email) {}
    virtual void PushToContour() {}
    virtual void DeliverNotification(const Str& email) {}
    virtual void InvalidatePromocode() {}
    Value Result() const { return res_; }
protected:
    PersonOrder* order_;
    Invoice* invoice_;
    Connection* conn_;
    Value res_;
protected:
    void Fiscalize(const Str& email, const Str& doctype);
};

void order_payment::Fiscalize(const Str& email, const Str& doctype) {
    const auto fd = BALOON::FiscalizeOrder(conn_, *order_, *invoice_, email, doctype);
    const auto fiscal_docid = fd["docid"].As<Int>();
    SavePersonOrderFiscalDoc(conn_, order_->id, fiscal_docid);
    if(fd.Exists("error")) {
        Log(5) << "FiscalGate ERROR: " << fd["error"].As<string>() << endl;
    }
}

class order_payment_rejected : public order_payment {
public:
    order_payment_rejected(PersonOrder* po, Invoice* invoice, Connection* rdb) : order_payment(po, invoice, rdb) {}
    virtual void UpdateOrderStatus() override;
};

void order_payment_rejected::UpdateOrderStatus() {
    order_->status = PERSON_ORDER_STATUS_REJECTED;
    order_->Update(conn_);
    res_["status"] = PERSON_ORDER_STATUS_REJECTED;
}

class order_payment_expired : public order_payment {
public:
    order_payment_expired(PersonOrder* po, Invoice* invoice, Connection* rdb) : order_payment(po, invoice, rdb) {}
    virtual void UpdateOrderStatus() override;
};

void order_payment_expired::UpdateOrderStatus() {
    order_->status = PERSON_ORDER_STATUS_EXPIRED;
    order_->Update(conn_);
    res_["status"] = PERSON_ORDER_STATUS_EXPIRED;
}

class order_payment_payed : public order_payment {
public:
    order_payment_payed(PersonOrder* po, Invoice* invoice, Connection* rdb) : order_payment(po, invoice, rdb) {}
    virtual void UpdateOrderStatus() override;
    virtual void FiscalizeOrder(const Str& email) override;
    virtual void PushToContour() override;
    virtual void DeliverNotification(const Str& email) override;
    virtual void InvalidatePromocode() override;
private:
    Str clientoid;
};

void order_payment_payed::UpdateOrderStatus() {
    order_->status = PERSON_ORDER_STATUS_PAYED;
    order_->Update(conn_);
    res_["status"] = PERSON_ORDER_STATUS_PAYED;
}

void order_payment_payed::PushToContour() {
    Str clientoid;
    try {
        clientoid = ParkingTicketRegisterInContour(conn_, order_->id);
        if(Defined(clientoid)) {
            UpdateIdentifierStatus<PersonIdentifierOrderline>(conn_, order_->id, IDENTIFIER_STATUS_OK, Str(), clientoid);
            return;
        }
    }
    catch(const Exception& e) {
        UpdateIdentifierStatus<PersonIdentifierOrderline>(conn_, order_->id, IDENTIFIER_STATUS_CANCELED, e.What(), Str());
        return;
    }
    const auto gates = GetAllEnabledGates(conn_);
    Str comment;
    bool at_least_one_gate_ok(false);
    for(const auto& gate : gates) {
        try {
            clientoid = RegisterInContour(conn_, order_->id, gate);            
            at_least_one_gate_ok = true;
        }
        catch(const Exception& e) {
            Log(5) << "spd ERROR: Gate: " << gate.host.get_value_or("") << ":" << gate.port.get_value_or("") << " " << e.What() << endl;
        }
    }
    string identifierstatus;
    if(at_least_one_gate_ok) {
        identifierstatus = IDENTIFIER_STATUS_OK;
    }
    else {
        identifierstatus = IDENTIFIER_STATUS_CANCELED;
        comment = Str("Ошибка выдачи идентификатора");
    }
    UpdateIdentifierStatus<PersonIdentifierOrderline>(conn_, order_->id, identifierstatus, comment, clientoid);
}

void order_payment_payed::FiscalizeOrder(const Str& email) {
    Fiscalize(email, SELL_TYPE);
}

void order_payment_payed::DeliverNotification(const Str& email) {
    try {
        BALOON::DeliverNotification(email, ticket_templatename, *order_);
    }
    catch(const Exception& ex) {
        Log(5) << "Email delivery ERROR: " << ex.What() << endl;
    }
}

void order_payment_payed::InvalidatePromocode() {
    BALOON::InvalidatePromocode(order_->promocode);
}

class order_payment_refunded : public order_payment {
public:
    order_payment_refunded(PersonOrder* po, Invoice* invoice, Connection* rdb) : order_payment(po, invoice, rdb) {}
    virtual void UpdateOrderStatus() override;
    virtual void FiscalizeOrder(const Str& email) override;
    virtual void PushToContour() override;
};

void order_payment_refunded::UpdateOrderStatus() {
    order_->status = PERSON_ORDER_STATUS_REFUNDED;
    order_->Update(rdb_);
    res_["status"] = PERSON_ORDER_STATUS_REFUNDED;
}

void order_payment_refunded::FiscalizeOrder(const Str& email) {
    Fiscalize(email, REFUND_TYPE);
}

void order_payment_refunded::PushToContour() {
    const auto gates = GetAllEnabledGates(conn_);
    string identifierstatus;
    string comment("Аннулирование в связи с возвратом");
    try {
        identifierstatus = IDENTIFIER_STATUS_CANCELED;
        InvalidateIdentifiers(rdb_, order_->id, comment, gates);
    }
    catch(const Exception& e) {
        Log(5) << "spd ERROR: " << e.What() << endl;
        comment = string("Ошибка аннулирования идентификатора: ") + e.What();
        identifierstatus = IDENTIFIER_STATUS_OK;
    }
    UpdateIdentifierStatus<PersonIdentifierOrderline>(rdb_, order_->id, identifierstatus, comment, Str());
}

struct OrderPaymentTables;
using oOrderPaymentTables = boost::optional<OrderPaymentTables>;
struct OrderPaymentTables {    
    PersonOrder porder_;
    Invoice invoice_;
    Person person_;
    static oOrderPaymentTables GetTables(Connection* rdb, const Int& invoiceid);
};

oOrderPaymentTables OrderPaymentTables::GetTables(Connection* rdb, const Int& invoiceid) {
    PersonInvoice person_invoice;
    Invoice invoice;
    PersonOrder order;
    Person person;
    Selector sel;
    sel << invoice
        << order
        << person;        
    sel.From(invoice)
        .Join(person_invoice).On(person_invoice->id == invoice->id)
        .Join(order).On(person_invoice->orderid == order->id)
        .Join(person).On(order->personid == person->id);
    sel.Where(invoice->id == invoiceid);
    auto ds = sel.Execute(rdb);
    return ds.Fetch()? oOrderPaymentTables({ order, invoice, person }) : oOrderPaymentTables();
}

const Value BALOONPersonInvoice::StatusUpdated(const Int id, const Str barcode, const Str status, const Decimal amount) {
    if(!Defined(status)) {
        return Value();
    }
    auto tables = OrderPaymentTables::GetTables(rdb_, id);
    if(!tables) {
        return Value();
    }
    auto& order = tables->porder_;
    auto& invoice = tables->invoice_;
    auto& person = tables->person_;
    
    order_payment_rejected rej(&order, &invoice, rdb_);
    order_payment_expired expir(&order, &invoice, rdb_);
    order_payment_payed payed(&order, &invoice, rdb_);
    order_payment_refunded refund(&order, &invoice, rdb_);
    
    std::map<string, order_payment*> payment_statuses = {
        { PAYMENT_STATUS_REJECTED, &rej },
        { PAYMENT_STATUS_EXPIRED, &expir },
        { PAYMENT_STATUS_PAYED, &payed },
        { PAYMENT_STATUS_REFUNDED, &refund }
    };
    auto* opayment = payment_statuses[*status];
    if(opayment == nullptr) {
        return Value();
    }
    opayment->PushToContour();
    opayment->DeliverNotification(person.email);
    opayment->InvalidatePromocode();
    opayment->UpdateOrderStatus();
    opayment->FiscalizeOrder(person.email);
    return opayment->Result();
}

/*const Value BALOONPersonInvoice::StatusUpdated(const Int id, const Str barcode, const Str status, const Decimal amount) {
    if(!Defined(status)) {
        return Value();
    }    
    PersonInvoice person_invoice;
    Invoice invoice;
    PersonOrder order;
    Person person;
    Selector sel;
    sel << invoice
        << person_invoice
        << order
        << person->email;
        
    sel.From(invoice)
        .Join(person_invoice).On(person_invoice->id == invoice->id)
        .Join(order).On(person_invoice->orderid == order->id)
        .Join(person).On(order->personid == person->id);
    sel.Where(invoice->id == id);
    auto ds = sel.Execute(rdb_);
    if(!ds.Fetch()) {
        return Value();
    };    
    Value res;
    res["orderid"] = order.id;
    
    if(status == PAYMENT_STATUS_REJECTED) {
        order.status = PERSON_ORDER_STATUS_REJECTED;
        order.Update(rdb_);
        res["status"] = PERSON_ORDER_STATUS_REJECTED;
        return res;
    }    
    if(status == PAYMENT_STATUS_EXPIRED) {
        order.status = PERSON_ORDER_STATUS_EXPIRED;
        order.Update(rdb_);
        res["status"] = PERSON_ORDER_STATUS_EXPIRED;
        return res;
    }
    
    Str doctype;
    Str orderstatus;
    string identifierstatus;
    Str clientoid;
    Str comment;
    
    const auto gates = GetAllEnabledGates(rdb_);
    
    if(status == PAYMENT_STATUS_PAYED) {
        doctype = SELL_TYPE;
        orderstatus = PERSON_ORDER_STATUS_PAYED;
        auto at_least_one_gate_ok(false);
        InvalidatePromocode(order.promocode);
        for(const auto& gate : gates) {
            try {
                clientoid = RegisterInContour(rdb_, order.id, gate);
                at_least_one_gate_ok = true;
            }
            catch(const Exception& e) {
                Log(5) << "spd ERROR: Gate: " << gate.host.get_value_or("") << ":" << gate.port.get_value_or("") << " " << e.What() << endl;
            }
        }
        if(at_least_one_gate_ok) {
            identifierstatus = IDENTIFIER_STATUS_OK;
            try {
                DeliverNotification(person.email, ticket_templatename, order);
            }
            catch(const Exception& ex) {
                Log(5) << "Email delivery ERROR: " << ex.What() << endl;
            }
        }
        else {
            comment = string("Ошибка выдачи идентификатора: ");
            identifierstatus = IDENTIFIER_STATUS_CANCELED;
        }
    }
    else if(status == PAYMENT_STATUS_REFUNDED) {
        doctype = REFUND_TYPE;
        orderstatus = PERSON_ORDER_STATUS_REFUNDED;
        try {
            comment = Str("Аннулирование в связи с возвратом");
            identifierstatus = IDENTIFIER_STATUS_CANCELED;
            
            InvalidateIdentifiers(rdb_, order.id, *comment, gates);
        }
        catch(const Exception& e) {
            Log(5) << "spd ERROR: " << e.What() << endl;
            comment = string("Ошибка аннулирования идентификатора: ") + e.What();
            identifierstatus = IDENTIFIER_STATUS_OK;
        }
    }
    else {
        return res;
    }
    const auto fd = FiscalizeOrder(rdb_, order, invoice, person.email, doctype);
    const auto fiscal_docid = fd["docid"].As<Int>();
    SavePersonOrderFiscalDoc(rdb_, order.id, fiscal_docid);
    
    if(fd.Exists("error")) {
        Log(5) << "FiscalGate ERROR: " << fd["error"].As<string>() << endl;
    }
    
    UpdateIdentifierStatus(rdb_, order.id, identifierstatus, comment, clientoid);
    
    order.status = orderstatus;
    order.Update(rdb_);
    
    res["status"] = orderstatus;
    return res;
}*/

set<string> GetIdentifiers(Connection* rdb, const Int& personid) {
    Identifier identifier;
    Selector sel;
    sel << identifier;
    sel.Where(identifier->personid == personid);
    auto ds = sel.Execute(rdb);
    set<string> res;
    while(ds.Fetch()) {
        const auto code = identifier.code;
        if(Defined(code)) {
            res.insert(*code);
        }
    }
    return res;
}
//------------------------------------------------------------------------------
/// \brief Создание транзакции в СПД
/// \details Внесение суммы на лицевой счет в СПД в xml-пакете transaction 
/// \param code
/// \param amount
/// \param currency
/// \param comment
/// \param cost - необязательное поле, передающее необязательный параметр cost
string createSpdTransaction(const Str code, const Decimal amount, const string currency, const string comment,
                            const Decimal cost)
{
    SpdTransaction transaction;
    transaction.eid = to_upper(Effi::GenerateGUID());
    transaction.currency = currency;
    transaction.summ = amount;
    transaction.comment = comment;
    transaction.date = TIME_NOW /*+ get_utc_offset()*/;
    transaction.cost = cost; /// Если cost>0, то соответствующий атрибут будет добавлен в пакет

    SpdClient client;
    const auto gate = GetDefaultGate(rdb_);
    client.identifier = code.get_value_or("");
    client.transactions.push_back(transaction);
    client.spd_host = gate.host;
    client.spd_port = gate.port;

    SendSpdRequest(client);
    return transaction.eid;
}
//------------------------------------------------------------------------------
/// \brief Синхронизируется счет(Invoice)
/// \details При вызове этой функции происходит пополнение баланса в СПД
const Value BALOONIdentifierInvoice::SyncInvoice(const Int id) {
    Invoice invoice;
    IdentifierInvoice idn_invoice;
    Identifier identifier;
    Selector sel;
    sel << invoice
        << idn_invoice
        << identifier->code;
    sel.From(idn_invoice).
        Join(invoice).On(invoice->id == idn_invoice->id).
        Join(identifier).On(identifier->id == idn_invoice->identifierid);
    sel.Where(idn_invoice->id == id);
    auto data = sel.Execute(rdb_);
    if (!data.Fetch()) {
        throw Exception(Message("Given invoice not found. ").What());
    }
    if (!idn_invoice.eid.get_value_or("").empty()) {
        throw Exception(Message("Invoice already synchronized. ").What());
    }

    const Str sep_payments = MainConfig->Get("SPD-XML-API/AllowSeparatePayments").get_value_or(CFG_SEPARATE_PAYMENTS);

    if ((sep_payments==Str(CONFIG_TRUE_CONST)) || (sep_payments==Str(CONFIG_YES_CONST))) // Разрешены раздельные платежи
        {
        Log(5)<<__func__<<"Separate payments enabled";
        try
            {
            /// Разбираем описание заказа
            /// Формируем различные пакеты оплаты    
            vector<string> items=split(ToString(idn_invoice.contents),CFG_ITEMS_DELIMITER); // Получаем описание заказа

            Decimal subtotal_amount=0; /// Промежуточный итог, для подсчета суммы по позициям, где не указан cost
            IdentifierInvoice subtotal=idn_invoice;
            idn_invoice.Delete(rdb_);
            
            for (auto it = std::begin(items); it!=std::end(items); ++it) // Просматриваем заказ
                {
                vector<string> item=split(*it,CFG_ORDER_DELIMITER); // Разбираем позицию
                if (item.size()==2)
                    {
                    Str descr=item[0]; // Название позиции
                    Decimal quantity=0;
                    quantity=::atof((item[1]).c_str()); // Количество (?) Преобразование может быть не верным
                    if (quantity>0)
                        { 
                        IdentifierServiceRule rule; /// Ищем описание услуги
                        Selector selector;
                        selector<<rule;
                        selector.From(rule);
                        selector.Where(rule->name == descr);

                        auto query = selector.Execute(rdb_);
                        if (!query.Fetch())
                            {
                            throw Exception(Message("Service rule not found. ").What());
                            }
                        else // Услуга найдена
                            {
                            if (Defined(rule.cost)) // COST указан
                                {
                                /// TODO: (!) Сумма подсчитывается только для РАБОЧИХ дней
                                /// Нуждается в доработке
                                IdentifierInvoice II=subtotal;
                                II.eid = createSpdTransaction(identifier.code, rule.weekday_cost, "RUB", DEFAULT_TRANSACTION_DESCRIPTION_WITH_COST, rule.cost);
                                II.Insert(rdb_);  
                                }
                            else // COST не указан
                                {
                                /// TODO: (!) Сумма подсчитывается только для РАБОЧИХ дней
                                /// Нуждается в доработке
                                subtotal_amount=subtotal_amount+rule.weekday_cost;
                                //subtotal_amount+=rule.weekend_cost;    
                                }                                
                            }
                        }
                    } //if
                } //for
            if (subtotal_amount>0)
                {
                subtotal.eid = createSpdTransaction(identifier.code, subtotal_amount, "RUB", DEFAULT_TRANSACTION_DESCRIPTION);
                subtotal.Insert(rdb_);    
                }
            }
        catch(...)
            {
            Log(5)<<__func__<<"IdentifierInvoice/RaiseInvoce_FE() has incorrect param 'contents'";            
            throw Exception(Message("Incorrect order description").What());            
            }
        }
    else // Оплачиваем все одним платежом
        {
        /// Пополняем лицевой счет в SPD
        idn_invoice.eid = createSpdTransaction(identifier.code, invoice.amount, "RUB", DEFAULT_TRANSACTION_DESCRIPTION);
        idn_invoice.Update(rdb_);        
        }
    
    AddMessage((Message() << "Invoice " << invoice.barcode << " synchronized. "));

    //Value res;
    //res["eid"] = idn_invoice.eid;
    return Value();
}
//------------------------------------------------------------------------------
Int GetDefaultFiscalGate(Connection* rdb) {
    const auto val = BALOONBaloonProvider::Get();
    const auto fiscal_gateid = val["default_fiscalgateid"].As<Int>();
    return fiscal_gateid;
}

Str GetClientEmail(Connection* rdb, const Int& invoiceid) {
    Log(5) << "Call GetClientEmail(" << invoiceid << ")" << endl;
    IdentifierInvoice invoice;
    Identifier identifier;
    Person person;
    Selector sel;
    sel << person->email;
    sel.From(person)
        .Join(identifier).On(identifier->personid == person->id)
        .Join(invoice).On(invoice->identifierid == identifier->id);
    sel.Where(invoice->id == invoiceid);
    auto ds = sel.Execute(rdb);
    if(!ds.Fetch()) {
        throw Exception(Message("Client is not found for invoice. ").What());
    }
    return person.email;
}

Value MakeItem(const Decimal& sum) {
    const Str FILL_SKIPASS("Пополнение ски-пасса");
    Value item;
    item["name"] = FILL_SKIPASS;
    item["price"] = sum;
    item["qty"] = Int(1);
    item["amount"] = sum;

    Value res;
    res[0] = item;
    return res;
}

void Fiscalize(Connection* rdb, const Int& invoiceid, const Str& doc_type, const Decimal& amount) {
    const auto fiscalgateid = GetDefaultFiscalGate(rdb);
    if(!Defined(fiscalgateid)) {
        Log(5) << "~~~ERROR: Fiscal gate is not defined. " << endl;
        return;
    }    
    Invoice invoice(invoiceid);
    invoice.Select(rdb);
    IdentifierInvoice identifier_invoice(invoiceid);
    identifier_invoice.Select(rdb);
        
    Value res = RootMethodInvoker("Fiscal", "FiscalDoc", "RaiseFiscalDoc") [
        PName<Int>("fiscalgateid") = fiscalgateid,
        PName<Str>("doc_type") = doc_type,
        PName<Decimal>("total_amount") = amount,
        PName<Str>("client_email") = GetClientEmail(rdb, invoiceid),
        PName<Str>("ext_orderid") = ToString(invoice.id.get_value_or(0)),
        PName<Str>("ext_order_type") = invoice.order_type,
        PName<Str>("comment") = Str(),
        PName<Value>("items") = MakeItem(amount)
    ];
    const auto fiscal_docid = res["docid"].As<Int>();
    identifier_invoice.fiscal_docid = fiscal_docid;
    identifier_invoice.Update(rdb);
}
//------------------------------------------------------------------------------
/// \brief Обновляем статус счета
/// Если счет оплаченный, то передаем данные в SPD
/// Вызывается как событие по расписанию, из TM_TaskList | TM_EventList
const Value BALOONIdentifierInvoice::StatusUpdated(const Int id, const Str barcode, const Str status, const Decimal amount)
{
    if (status == PAYMENT_STATUS_PAYED)
        {
        BALOONIdentifierInvoice::SyncInvoice(id);
        const Str doctype("sell");
        Fiscalize(rdb_, id, doctype, amount);
        }
    return Value();
}
//------------------------------------------------------------------------------
/// \brief Создать счет привязанный к пропуску
/// Вызывается мобильными приложениями через обертку RaiseInvoice_FE
const Value BALOONIdentifierInvoice::RaiseInvoice(const Int paygateid, const Int identifierid, const Decimal amount,
        const Str success_url, const Str fail_url, const Str contents=Str())
{    
    const auto origin = MainConfig->Get("WWW/Origin").get_value_or(""); // Получение начала ссылки
    const auto barcode = PAYGATE::NextBarcode(rdb_); // Получаем Штрих код

    //cout << "-----Amount = " << amount << endl;
    
    Value res = RootMethodInvoker("Paygate", "Invoice", "RaiseInvoice") [ // Вызываем метод из динамической библиотеки
        PName<Int>("paygateid") = paygateid,
        PName<Str>("barcode") = barcode,
        PName<Decimal>("amount") = amount,
        PName<Str>("order_type") = BALOON_IDENTIFIER_INVOICE_CLASS,
        PName<Str>("success_url") = Defined(success_url)? success_url : Str(origin + success_url_),
        PName<Str>("fail_url") = Defined(fail_url)? fail_url : Str(origin + fail_url_)
    ];

    const Int invoiceid = res["INVOICEID@@"].As<Int>(); // Получаем ид счета
    IdentifierInvoice i_invoice; // Создаем счет
    i_invoice.id = invoiceid;
    i_invoice.identifierid = identifierid;
    if (Defined(contents))
        i_invoice.contents=contents;
    i_invoice.Insert(rdb_); // Пишем счет в БД

    return res;
}
//------------------------------------------------------------------------------
/// \brief Создать счет для пропуска
/// Вызывается из мобильных приложений для оплаты заказа
const Value BALOONIdentifierInvoice::RaiseInvoice_FE(const Str code, const Decimal amount, const Str success_url,
        const Str fail_url, const Str contents=Str())
{
    const auto paygateid = GetDefaultPaygate(rdb_); // Получить платежный шлюз по умолчанию
    Identifier identifier; // Создаем идентификатор
    Selector sel; // Создаем запрос на поиск
    sel << identifier->id;
    sel.Where(identifier->code == code); // Описываем условие в операторе SELECT
    auto ds = sel.Execute(rdb_); // Выполняем запрос
    if(!ds.Fetch()) { // Если ничего не получено
        throw Exception(Message("Identifier has not been bound to client. ").What());
    }
    return RaiseInvoice(paygateid, identifier.id, amount, success_url, fail_url, contents);
}
//------------------------------------------------------------------------------
Int GetDefaultPaygate(Connection* rdb) {
    BaloonProvider provider;
    provider.Select(rdb_);
    const auto paygateid = provider.default_paygateid;
    if (!Defined(paygateid)) {
        throw Exception(Message("Unable to raise invoice: please setup the default payment gateway. ").What());
    }
    return paygateid;
}

Int GetLatestIdentifier(Connection* rdb, const Int& personid) {
    Identifier identifier;
    Selector sel;
    sel << identifier->id;
    sel.Where(identifier->personid == personid);
    sel.OrderDesc(identifier->valid_to);
    sel.Limit(1);
    auto ds = sel.Execute(rdb);
    if(!ds.Fetch()) {
        throw Exception(Message("No identifiers bound to current user. ").What());
    }
    return identifier.id;
}

/*DEPRECATED*/
const Value BALOONIdentifierInvoice::RaiseInvoiceForCurrentUser(const Decimal amount, const Str success_url, const Str fail_url) {
    const auto paygateid = GetDefaultPaygate(rdb_);
    const auto personid = checkAndGetCurrentPersonId(rdb_);
    const auto latest_identifierid = GetLatestIdentifier(rdb_, personid);
    return BALOONIdentifierInvoice::RaiseInvoice(paygateid, latest_identifierid, amount, success_url, fail_url);
}
//------------------------------------------------------------------------------

const Value BALOONIdentifierInvoice::CurrentInvoicesGetList_FE() {
    Int personid = checkAndGetCurrentPersonId(rdb_);
    IdentifierInvoice aIdentifierInvoice;
    Invoice aInvoice;
    PayGate agate;
    Identifier aIdentifier;
    PayGate apaygate;
    Selector sel;
    sel << aIdentifierInvoice->id
        << aInvoice->barcode
        << aInvoice->paygateid
        << agate->name 
        << aInvoice->currency
        << aInvoice->amount
        << aInvoice->status
        << aInvoice->comment;
    sel.From(aIdentifierInvoice)
        .Join(aInvoice).On(aIdentifierInvoice->id == aInvoice->id)
        .Join(aIdentifier).On(aIdentifierInvoice->identifierid == aIdentifier->id)
        .LeftJoin(apaygate).On(aInvoice->paygateid==apaygate->id);
    sel.Where(aIdentifier->personid == personid);
    auto data = sel.Execute(rdb_);

    Value res;
    int32_t i = 0;
    while(data.Fetch()) {
        Value row;
        row["id"] = aIdentifierInvoice.id;
        row["barcode"] = aInvoice.barcode;
        row["paygateid"] = aInvoice.paygateid;
        row["gatename"] = agate.name;
        row["currency"] = aInvoice.currency;
        row["amount"] = aInvoice.amount;
        row["status"] = aInvoice.status;
        row["comment"] = aInvoice.comment;
        res[i++] = row;
    }
    return res;
}

const Value BALOONBaloonProvider::IdentifierPriceGet() {
    BaloonProvider provider;
    provider.Select(rdb_);
    Value res;
    res["ident_sale_price"] = provider.ident_sale_price;
    return res;
}

/// Получить список внешних ресурсов
const Value BALOONExternalResource::ExternalResourceListGet_API() {
    ExternalResource eres;
    Selector sel;
    sel << eres->id
        << eres->name
        << eres->uri;
    sel.Where(eres->enabled == ENABLED);
    Value val;
    uint i(0);
    auto ds = sel.Execute(rdb_);
    while(ds.Fetch()) {
        Value item;
        item["id"] = eres.id;
        item["name"] = eres.name;
        item["uri"] = eres.uri;
        val[i] = item;
        ++i;
    }
    return val;
}

struct ReportRow {
    Decimal total_date_amount;
    int orders_date_count;
    ReportRow() : total_date_amount(0.0), orders_date_count(0) {}
    ReportRow(const Decimal& amount, int orders_count) : total_date_amount(amount), orders_date_count(orders_count) {}
};

struct Interval {
    Time begin;
    Time end;
    Interval(const ADate& first, const ADate& last);
    Expression GetExpression(const PersonOrder&);
};

Interval::Interval(const ADate& first, const ADate& last) {
    const auto offset = boost::posix_time::hours(0); //Computerica::get_utc_offset();
    begin = Time(first, -offset );
    end = Time(last,  boost::posix_time::hours(24) - offset);
}

Expression Interval::GetExpression(const PersonOrder& order) {
    return Expression(order->filed_at >= begin &&
                      order->filed_at <= end &&
                      order->status == PERSON_ORDER_STATUS_PAYED);
}

map<ADate, ReportRow> PreparePersonSalesReport(Connection* rdb, const ADate& first_day, const ADate& last_day) {
    PersonOrder order;
    
    Selector sel;
    sel << order->filed_at
        << order->amount
        << order->id;
        
    Interval interval(first_day, last_day);    
    sel.Where(interval.GetExpression(order));
    auto ds = sel.Execute(rdb);
    map<ADate, ReportRow> res;
    while(ds.Fetch()) {
        const auto date = (order.filed_at /*+ Computerica::get_utc_offset()*/).date();
        auto& row = res[date];
        row.total_date_amount = row.total_date_amount + order.amount;
        ++row.orders_date_count;
        
        Log(5) << "----- date: " << date << " count: " << row.orders_date_count << endl;
    }
    return res;
}

const Value BALOONReports::CurrentDate() {
    const auto day = (TIME_NOW /* +  Computerica::get_utc_offset()*/ ).date();
    Value res;
    res["current_date"] = day;
    return res;
}

const Value BALOONReports::PersonSalesReportListGet(const ADate first_day, const ADate last_day) {
    Data::DataList lr;
    
    lr.AddColumn("current_date", Data::DATETIME);
    lr.AddColumn("total_date_amount", Data::DECIMAL);
    lr.AddColumn("orders_date_count", Data::INTEGER);
    
    ADate current_date;
    ReportRow row;
    
    lr.Bind(current_date, "current_date");
    lr.Bind(row.total_date_amount, "total_date_amount");
    lr.Bind(row.orders_date_count, "orders_date_count");
    
    if(!(Defined(first_day) && Defined(last_day))) {
        return lr;
    }    
    auto prepared_report = PreparePersonSalesReport(rdb_, first_day, last_day);
    boost::gregorian::day_iterator iter(first_day);
    while(iter <= last_day) {
        current_date = *iter;
        row = prepared_report[current_date];
        lr.AddRow();
        ++iter;
    }
    return lr;
}

const Value BALOONReports::IdentifierReportListGet(const ADate current_date) {
    PersonOrder order;
    Identifier identifier;
    PersonIdentifierOrderline oline;
    Selector sel;
    
    Interval interval(current_date, current_date);
    
    Int identifier_count;
    sel << identifier->id.Count().Bind(identifier_count)
        << identifier->name;
    sel.From(identifier)
        .Join(oline).On(oline->identifierid == identifier->id)
        .Join(order).On(oline->orderid == order->id);
    sel.Where(interval.GetExpression(order));
    sel.GroupBy(identifier->name);
    auto ds = sel.Execute(rdb_);
    Data::DataList lr;
    lr.AddColumn("identifiername", Data::STRING);
    lr.AddColumn("identifier_count", Data::INTEGER);
    
    lr.Bind(identifier.name, "identifiername");
    lr.Bind(identifier_count, "identifier_count");
    
    while(ds.Fetch()) {
        lr.AddRow();
    }
    return lr;
}

const Value BALOONAccount::AccountListGet(const Int personid) {
    SpdAccountCurrency account;
    Data::DataList lr;
    lr.AddColumn("currency", Data::STRING);
    lr.AddColumn("balance", Data::DECIMAL);
    lr.Bind(account.currency, "currency");
    lr.Bind(account.balance, "balance");

    SpdResponse response = requestPersonFromSPD(personid);
    for (SpdAccountCurrency acc : response.clients.at(0).account.currencies) {
        account = acc;
        lr.AddRow();
    }
    Value res=lr;
    return res;
}

const Value BALOONPackageOrder::Add(const Int identifierid, const Str service_rulename) {
    if(!Defined(service_rulename)) {
        throw Exception("Service rule not defined. ");
    }
    Identifier identifier(identifierid);
    if(!identifier.Select(rdb_)) {
        throw Exception("Identifier not found. ");
    }
    SpdPackage package;
    package.identifier = *identifier.code;
    package.rule_service = *service_rulename;
    
    const auto gate = GetDefaultGate(rdb_);

    SpdClient client;
    client.identifier = *identifier.code;
    client.packages.push_back(package);
    client.spd_host = gate.host;
    client.spd_port = gate.port;

    SendSpdRequest(client);
    const auto sk = SavePackageOrder(rdb_, identifier.id, service_rulename);
    Value res;
    res["id"] = sk;
    return res;
}

/// Выдать список заказов
const Value BALOONPackageOrder::MyPackageListGet_API(const Str code) {
    const auto identifier = GetIdentifierByCode(rdb_, code);
    const auto personid = checkAndGetCurrentPersonId(rdb_);
    if(identifier.personid != personid) {
        throw Exception("Forbidden. ");
    }
    PackageOrder porder;
    SpdServiceRule srule;
    Selector sel;
    sel << porder->id
        << porder->service_rulename
        << porder->filed_at
        << srule->package_cost;
    sel.From(porder).Join(srule).On(porder->service_rulename == srule->name);
    sel.Where(porder->identifierid == identifier.id);
    auto ds = sel.Execute(rdb_);
    uint i(0);
    Value res;
    while(ds.Fetch()) {
        Value entry;
        entry["id"] = porder.id;
        entry["service_rulename"] = porder.service_rulename;
        entry["filed_at"] = porder.filed_at;
        entry["package_cost"] = srule.package_cost;
        res[i] = entry;
        ++i;
    }
    return res;
}

const Value BALOONTransaction::TransactionListGet(const Int identifierid) {
    if(!Defined(identifierid)) {
        throw Exception(Message("Identifier code is not specified. ").What());
    }
    Identifier identifier(identifierid);
    identifier.Select(rdb_);
    
	Data::DataList lr;
	lr.AddColumn("currency", Data::STRING);
	lr.AddColumn("eid", Data::STRING);
	lr.AddColumn("amount", Data::DECIMAL);
	lr.AddColumn("comment", Data::STRING);
	SpdTransaction aTransaction;
	Person aPerson;
	lr.Bind(aTransaction.currency, "currency");
	lr.Bind(aTransaction.eid, "eid");
	lr.Bind(aTransaction.summ, "amount");
	lr.Bind(aTransaction.comment, "comment");

    const auto gate = GetDefaultGate(rdb_);
    SpdClient client;
    client.identifier = *identifier.code;
    client.spd_host = gate.host;
    client.spd_port = gate.port;
    
    const auto REQUEST_TRANSACTIONS(true);
    const auto response = SendSpdRequest(client, REQUEST_TRANSACTIONS);
    try {
        const auto& transactions = response.clients.at(0).transactions;
        for (SpdTransaction trans : transactions) {
            aTransaction = trans;
            lr.AddRow();
        }
    }
    catch(const std::out_of_range&) {
        throw Exception("Contour did not return clients in response. ");
    }
	return lr;
}

const Value BALOONTransaction::CurrentTransactionListGet_FE() {
    Int personid = checkAndGetCurrentPersonId(rdb_);
    Value res;
    
    SpdResponse response = requestPersonFromSPD(personid, true);
    const auto& clients = response.clients;
    
    if(clients.empty()) {
        throw Exception(Message("No clients in spd response. ").What());
    }
    int i = 0;
    for (const auto& tr : clients[0].transactions) {
        Value r;
        r["currency"] = tr.currency;
        r["amount"] = tr.summ;
        r["comment"] = tr.comment;
        r["date"] = tr.date;
        res[i] = r;
        ++i;
    }
    return res;
}

const Value BALOONTransaction::IdentifierTransactionListGet_FE(const Str code) {
    if(StrEmpty(code)) {
        throw Exception(Message("Identifier code is empty. ").What());
    }
    const auto personid = checkAndGetCurrentPersonId(rdb_);
    Value res;
    uint i = 0;
    SpdClient client;
    client.identifier = *code;
    const auto WITH_TRANSACTIONS = true;
    const auto gate = GetDefaultGate(rdb_);
    client.spd_host = gate.host;
    client.spd_port = gate.port;
    const auto response = SendSpdRequest(client, WITH_TRANSACTIONS);
    try {
        const auto& transactions = response.clients.at(0).transactions;
        for(const auto& transaction : transactions) {
            Value entry;
            entry["currency"] = transaction.currency;
            entry["amount"] = transaction.summ;
            entry["comment"] = transaction.comment;
            entry["date"] = transaction.date;
            res[i] = entry;
            ++i;
        }
    }
    catch(const std::out_of_range&) {
        Log(5) << "ERROR: No clients in Spd response. " << endl;
    }
    return res;
}

//========================================================
// TODO: earlier permanent_rule was rule_use
//========================================================
const Value BALOONTransaction::SaleIdentifier_API(
        const Str code, const Str print_code, 
        const Time valid_from, const Time valid_to, const Str permanent_rulename, const Str category, const Str rate, 
        const Str currency, const Decimal amount) {
    if ((!Defined(code) && !Defined(print_code))) throw Exception (Message("code or print_code field is mandatory. ").What());
    // if (!Defined (valid_from)) throw Exception (Message("valid_from field is mandatory. ").What());
    // if (!Defined (valid_to)) throw Exception (Message("valid_to field is mandatory. ").What());
    if (!Defined (permanent_rulename)) throw Exception (Message("permanent_rulename field is mandatory. ").What());
    if (!Defined (currency)) throw Exception (Message("Currency not defined. ").What());
    if (!Defined (amount)) throw Exception (Message("Amount not defined. ").What());

    SpdIdentifier identifier;
    identifier.is_new = true;
    identifier.code = code.get_value_or("");
    if (!print_code.get_value_or("").empty()) {
        identifier.code = from_print_code(print_code.get_value_or(""));
    }
    identifier.valid_from = (valid_from.is_not_a_date_time() ? TIME_NOW : valid_from);
    identifier.valid_to = (valid_to.is_not_a_date_time() ? TIME_NOW + boost::gregorian::years(1) : valid_to);
    identifier.permanent_rule = permanent_rulename.get_value_or("");
    identifier.category = category.get_value_or("Полный");
    if (Defined(rate)) identifier.tariff = rate.get_value_or("Все тарифы");

    SpdTransaction transaction;
    transaction.eid = to_upper(Effi::GenerateGUID());
    transaction.currency = currency.get_value_or("");
    transaction.summ = amount;
    transaction.comment = "Пополнение счёта агентом при продаже карты";
    transaction.date = TIME_NOW /*+ get_utc_offset();*/ ;

    SpdClient client(true);
    client.identifiers.push_back(identifier);
    client.transactions.push_back(transaction);
    
    const auto gate = GetDefaultGate(rdb_);

    SpdRequest request;
    request.clients.push_back(client);
    string data = request.serialize();

    Log(2) << "REQUEST: " << data << endl;

#ifdef USE_UTF_8
    string reply = SendSpdRequest(data, gate.host, gate.port);
#else
    string reply = SendSpdRequest(utf8_to_win1251(data), gate.host, gate.port);
#endif    

    Log(2) << "RESPONSE: " << reply << endl;
    SpdResponse response = SpdResponse::parse(reply);

    Value res;
    res["iid"] = response.clients.at(0).transactions.at(0).iid;
    return res;
}

Int SavePackageOrder(Connection* rdb, const Int& identifierid, const Str& service_rulename) {
    PackageOrder porder;
    porder.identifierid = identifierid;
    porder.service_rulename = service_rulename;
    porder.filed_at = TIME_NOW;
    const auto sk = porder.Insert(rdb);
    return sk;
}

//------------------------------------------------------------------------------
/// \brief Продажа пакета услуг
/// \details Вызывается только из фронта
/// \param code Код пропуска
/// \param print_code
/// \param categoryname Текстовое название услуги (Правило сервиса)
const Value BALOONTransaction::SalePackage_API(const Str code, const Str print_code, const Str categoryname)
{
    if ((!Defined(code) && !Defined(print_code)))
        {
        throw Exception (Message("Code or print_code field is mandatory. ").What());
        }
    if (!Defined (categoryname))
        {
        throw Exception (Message("Categoryname field is mandatory. ").What());  
        }
    const auto identifier = GetIdentifierByCode(rdb_, code);
    const auto personid = checkAndGetCurrentPersonId(rdb_);
    if(identifier.personid != personid)
        {
        throw Exception("Forbidden sale.");
        }
    string strcode = code.get_value_or("");
    if (!print_code.get_value_or("").empty())
        {
        strcode = from_print_code(print_code.get_value_or(""));
        }

    const Str sep_payments = MainConfig->Get("SPD-XML-API/AllowSeparatePayments").get_value_or(CFG_SEPARATE_PAYMENTS);

    bool need_to_send=true;
    
    if ((sep_payments==Str(CONFIG_TRUE_CONST)) || (sep_payments==Str(CONFIG_YES_CONST))) // Разрешены раздельные платежи
        {
        IdentifierServiceRule rule; /// Ищем описание услуги
        Selector selector;
        selector<<rule;
        selector.From(rule);
        selector.Where(rule->name == categoryname);
        auto query = selector.Execute(rdb_);

        if (!query.Fetch())
            {
            throw Exception(Message("Service rule not found. ").What());
            }
        else
            need_to_send=false; /// Если указан cost, то мы не шлем заказ услуги в СПД
            /// TODO: (!) Это не совсем верная логика, нужно дорабатывать
        }

    Value res; // Возращаемое значение
    
    if (need_to_send)
        {
        const auto gate = GetDefaultGate(rdb_);

        SpdPackage package;
        const auto service_rule = categoryname.get_value_or(string());
        package.identifier = strcode;
        package.rule_service = service_rule;
        package.comment = service_rule;
        
        SpdClient client;
        client.identifier = strcode;
        client.packages.push_back(package);
        client.spd_host = gate.host;
        client.spd_port = gate.port;

        SendSpdRequest(client);
        const auto sk = SavePackageOrder(rdb_, identifier.id, categoryname);
        res["id"] = sk;
        }

    return res;
}
//------------------------------------------------------------------------------
const Value BALOONTransaction::MassSalePackages_FE(const Str code, const Str print_code, const Value categories) {

    const uint size = categories.Size();
    if(!categories.IsArray() || !size) {
        return Value();
    }

    string strcode = code.get_value_or("");
    if (!print_code.get_value_or("").empty()) {
        strcode = from_print_code(print_code.get_value_or(""));
    }

    const auto gate = GetDefaultGate(rdb_);
    SpdClient client;
    client.identifier = strcode;
    client.spd_host = gate.host;
    client.spd_port = gate.port;
    for(uint i = 0; i < size; ++i) {
        const auto entry = categories[i];
        const auto categoryname = entry["categoryname"].As<string>();
        if(categoryname.empty()) {
            continue;
        }
        SpdPackage package;
        package.identifier = strcode;
        package.rule_service = categoryname;
        client.packages.push_back(package);
    }
    if(!client.packages.empty()) {
        SendSpdRequest(client);
    }
    return Value();
}

const Value BALOONTransaction::FillupAccount_API(const Str code, const Str print_code, const Str currency, const Decimal amount) {
    if ((!Defined(code) && !Defined(print_code)) || !Defined(currency) || !Defined(amount)) {
        throw Exception(Message("Some of the arguments are missing. ").What());
    }

    SpdTransaction transaction;
    transaction.eid = to_upper(Effi::GenerateGUID());
    transaction.currency = currency.get_value_or("");
    transaction.summ = amount;
    transaction.comment = "Пополнение счёта агентом";

    SpdClient client;
    client.identifier = code.get_value_or("");
    if (!print_code.get_value_or("").empty()) {
        client.identifier = from_print_code(print_code.get_value_or(""));
    }
    client.transactions.push_back(transaction);
    
    const auto gate = GetDefaultGate(rdb_);

    SpdRequest request;
    request.clients.push_back(client);
    string data = request.serialize();
    Log(5) << "REQUEST: " << data << endl;

#ifdef USE_UTF_8
    string reply = SendSpdRequest(data, gate.host, gate.port);
#else
    string reply = SendSpdRequest(utf8_to_win1251(data), gate.host, gate.port);
#endif   

    Log(5) << "RESPONSE: " << reply << endl;
    SpdResponse response = SpdResponse::parse(reply);

    Value res;
    res["iid"] = response.clients.at(0).transactions.at(0).iid;
    return res;
}

const Value BALOONAdvertisement::NewsArticleListGet() {
    BASICSITE::NewsArticle article;
    Selector sel;
    sel << article;
    sel.Where(article->content_image_refID.IsNull());
    sel.OrderDesc(article->priority);
    sel.OrderDesc(article->posted_at);
    auto ds = sel.Execute(rdb_);
    Data::DataList lr;
    lr.AddColumn("id", Data::INTEGER);
    lr.AddColumn("title", Data::STRING);
    lr.AddColumn("text_content", Data::STRING);
    lr.AddColumn("posted_at", Data::DATETIME);
    lr.AddColumn("priority", Data::INTEGER);
    
    lr.Bind(article.id, "id");
    lr.Bind(article.title, "title");
    lr.Bind(article.content, "text_content");
    lr.Bind(article.posted_at, "posted_at");
    lr.Bind(article.priority, "priority");
    
    while(ds.Fetch()) {
        lr.AddRow();
    }
    return lr;
}

const Value BALOONAdvertisement::NewsArticleAdd(const Str title, const Str text_content, const Int priority) {
    BASICSITE::NewsArticle article;
    article.title = title;
    article.content = text_content;
    article.posted_at = TIME_NOW;
    article.priority = Defined(priority)? priority : Int(0);
    const auto sk = article.Insert(rdb_);
    Value res;
    res["id"] = sk;
    return res;
}

const Value BALOONAdvertisement::NewsArticleGet(const Int id) {
    BASICSITE::NewsArticle article(id);
    article.Select(rdb_);
    Value res;
    res["id"] = id;
    res["title"] = article.title;
    res["text_content"] = article.content;
    res["priority"] = article.priority;
    return res;
}

const Value BALOONAdvertisement::NewsArticleUpdate(const Int id, const optional<Str> title, const optional<Str> text_content, const optional<Int> priority) {
    BASICSITE::NewsArticle article(id);
    article.Select(rdb_);
    if(Defined(title)) article.title = *title;
    if(Defined(text_content)) article.content = *text_content;
    if(Defined(priority)) article.priority = *priority;
    article.Update(rdb_);
    return Value();
}

const Value BALOONAdvertisement::Delete(const Int id) {
    BALOONInfoPlacement::Delete(id);
    BASICSITE::BASICSITENewsArticle::Delete(id);
    return Value();
}

const Value BALOONAdvertisement::LatestArticleListGet_FE(const Int items_qty) {
    BASICSITE::NewsArticle article;
    Selector sel;
    sel << article;
    sel.Where(article->content_image_refID.IsNull());
    sel.OrderDesc(article->priority);
    sel.OrderDesc(article->posted_at);
    if(Defined(items_qty)) {
        sel.Limit(*items_qty);
    }
    auto ds = sel.Execute(rdb_);
    Value res;
    uint i = 0;
    while(ds.Fetch()) {
        Value item;
        item["id"] = article.id;
        item["title"] = article.title;
        item["content"] = article.content;
        res[i] = item;
        ++i;
    }
    return res;
}

const Value BALOONAdvertisement::VisualItemListGet_FE(const Int items_qty) {
    BASICSITE::NewsArticle article;
    MULTIMEDIA::MMFile image;
    
    Selector sel;
    sel << article
        << image->FileBody;
    sel.From(article)
        .Join(image).On(article->content_image_refID == image->ID);
    sel.Where(!article->content_image_refID.IsNull());
    sel.OrderAsc(article->priority);
    sel.OrderDesc(article->posted_at);
    if(Defined(items_qty)) {
        sel.Limit(*items_qty);
    }
    auto ds = sel.Execute(rdb_);
    Value res;
    uint i = 0;
    while(ds.Fetch()) {
        Value item;
        item["id"] = article.id;
        item["title"] = article.title;
        item["html_link"] = article.html_link;
        item["image"] = image.FileBody;
        res[i] = item;
        ++i;
    }
    return res;
}

const Value BALOONAdvertisement::VisualItemListGet_FE_1_13(const Int items_qty) {
    BASICSITE::NewsArticle article;
    MULTIMEDIA::MMFile image;
    InfoPlacement placement;
    
    Selector sel;
    sel << article
        << image->FileBody
        << placement->widget;
    sel.From(article)
        .Join(image).On(article->content_image_refID == image->ID)
        .Join(placement).On(article->id == placement->id);
    sel.Where(!article->content_image_refID.IsNull());
    sel.OrderDesc(article->priority);
    sel.OrderDesc(article->posted_at);
    
    auto ds = sel.Execute(rdb_);
    Value res;
    uint i = 0;
    while(ds.Fetch()) {
        Value item;
        item["id"] = article.id;
        item["title"] = article.title;
        item["html_link"] = article.html_link;
        item["image"] = image.FileBody;
        item["widget"] = placement.widget;
        res[i] = item;
        ++i;
    }
    return res;
}

const Value BALOONAdvertisement::VisualItemListGet() {
    BASICSITE::NewsArticle article;
    InfoPlacement placement;
    Selector sel;
    sel << article
        << placement;
    sel.From(article).LeftJoin(placement).On(placement->id == article->id);
    sel.Where(!article->content_image_refID.IsNull());
    auto ds = sel.Execute(rdb_);
    Data::DataList lr;
    lr.AddColumn("id", Data::INTEGER);
    lr.AddColumn("title", Data::STRING);
    lr.AddColumn("html_link", Data::STRING);
    lr.AddColumn("posted_at", Data::DATETIME);
    lr.AddColumn("widget", Data::STRING);
    lr.AddColumn("priority", Data::INTEGER);
    
    lr.Bind(article.id, "id");
    lr.Bind(article.title, "title");
    lr.Bind(article.html_link, "html_link");
    lr.Bind(article.posted_at, "posted_at");
    lr.Bind(placement.widget, "widget");
    lr.Bind(article.priority, "priority");
    
    while(ds.Fetch()) {
        lr.AddRow();
    }
    return lr;
}

const Value BALOONAdvertisement::VisualItemGet(const Int id) {
    BASICSITE::NewsArticle article(id);
    article.Select(rdb_);
    InfoPlacement placement(id);
    placement.Select(rdb_);
    
    Value res;
    res["id"] = article.id;
    res["title"] = article.title;
    res["html_link"] = article.html_link;
    res["posted_at"] = article.posted_at;
    res["widget"] = placement.widget;
    res["priority"] = article.priority;
    return res;
}

void CheckImageSize(const optional<BlobFile>& image) {
    const auto img_size = image->Size();
    const string MAX_IMG_SIZE("149504"); //bytes
    const auto max_size_ = MainConfig->Get("HillPark/MaxImgSize").get_value_or(MAX_IMG_SIZE);
    const auto max_size = boost::lexical_cast<uint>(max_size_);
    
    if(img_size > max_size) {
        throw Exception(Message() << Message("Image too large ").What()
                                  << "(" << boost::lexical_cast<string>(img_size) << "). "
                                  << Message("Max size is ").What() << max_size_ << " bytes. ");
    }
}

void CheckHttpProtocol(const Str& html_link) {
    if(StrEmpty(html_link)) {
        return;
    }
    const auto MESSAGE = Message("Invalid link. ").What();
    const string http("http://");
    const string https("https://");

    if(html_link->size() <= https.size()) {
        throw Exception(MESSAGE);
    }
    const auto http_prefix = html_link->substr(0, http.size());
    const auto https_prefix = html_link->substr(0, https.size());
    
    if(http_prefix == http || https_prefix == https) {
        return;
    }
    throw Exception(MESSAGE);
}

const Value BALOONAdvertisement::VisualItemAdd(const Str title, const Str html_link, const optional<BlobFile> image, const Str widget, const Int priority) {
    if(!Defined(image) || !image->Size()) {
        throw Exception(Message("No image selected. ").What());
    }
    CheckImageSize(image);
    CheckHttpProtocol(html_link);
    const auto res = BASICSITE::BASICSITENewsArticle::Add(title, Str(), html_link, priority, image);
    const auto id = res["id"].As<int64_t>();
    BALOONInfoPlacement::Add(id, widget);
    return res;
}


const Value BALOONAdvertisement::VisualItemUpdate(const Int id, const optional<Str> title, const optional<Str> html_link, const optional<BlobFile> image, const optional<Str> widget, const optional<Int> priority) {
    CheckImageSize(image);
    if(Defined(html_link)) {
        CheckHttpProtocol(*html_link);
    }
    BASICSITE::BASICSITENewsArticle::Update(id, title, optional<Str>(), html_link, priority, image);
    
    InfoPlacement placement(id);
    if(Defined(widget)) {
        if(placement.Select(rdb_)) {
            placement.widget = *widget;
            placement.Update(rdb_);
        }
        else {
            placement.id = id;
            placement.widget = *widget;
            placement.Insert(rdb_);
        }
    }
    
    AddMessage(Message("Visual item updated. ").What());
    return Value();
}

const Value BALOONHoliday::MassAdd(const Value holidays) {
    const auto size = holidays.Size();
    if(!holidays.IsArray() || !size) {
        return Value();
    }
    for(uint i = 0; i < size; ++i) {
        const auto& item = holidays[i];
        const auto date = item["adate"].As<ADate>();
        const auto out_of_service = item["out_of_service"].As<string>();
        const auto comment = item["comment"].As<Str>();
        if(!Defined(date)) {
            continue;
        }
        Holiday ho;
        ho.adate = date;
        
        if(ho.Select(rdb_)) {
            ho.out_of_service = out_of_service.empty()? Str() : Str(out_of_service);
            ho.comment = comment;
            ho.Update(rdb_);
        }
        else {
            ho.out_of_service = out_of_service.empty()? Str() : Str(out_of_service);
            ho.comment = comment;
            ho.Insert(rdb_);
        }
    }
    return Value();
}

template<typename T>
bool NameExists(Connection* rdb, const Str& name) {
    T table;
    Selector sel;
    sel << table;
    sel.Where(table->name == name);
    auto ds = sel.Execute(rdb);
    return ds.Fetch();
}

void SetTariffGroupsNotDefaultExcept(Connection* rdb, const Int id) {
    WeekTariffGroup wtg;
    Updater upd(wtg);
    upd << wtg->is_default(BOOL_FALSE);
    if(Defined(id)) {
        upd.Where(wtg->id != id);
    }
    upd.Execute(rdb);
}

Int SaveTariffGroup(Connection* rdb, const Str& agroupname, const Str description, const Str& is_default) {
    WeekTariffGroup tgroup;
    if(StrEmpty(agroupname)) {
        throw Exception(Message("Tariff group name is not defined. ").What());
    }
    if(NameExists<WeekTariffGroup>(rdb, agroupname)) {
        throw Exception(Message("Tariff group already exists. ").What());
    }
    if(is_default == BOOL_TRUE) {
        SetTariffGroupsNotDefaultExcept(rdb_, Int());
    }
    WeekTariffGroup agroup;
    agroup.name = agroupname;
    agroup.description = description;
    agroup.is_default = Defined(is_default)? is_default : Str(BOOL_FALSE);
    const auto id = agroup.Insert(rdb);
    return id;
}

const Value BALOONWeekTariffGroup::PartnerWeekTariffGroupListGet() {
    WeekTariffGroup tariffgroup;
    PartnerWeekTariffGroup pwtg;
    Partner partner;
    Selector sel;
    sel << tariffgroup->id
        << partner->name
        << tariffgroup->name
        << tariffgroup->description;
    sel.From(tariffgroup)
        .Join(pwtg).On(pwtg->tariffgroupid == tariffgroup->id)
        .Join(partner).On(pwtg->partnerid == partner->id);
    auto ds = sel.Execute(rdb_);
    Data::DataList lr;
    lr.AddColumn("id", Data::INTEGER);
    lr.AddColumn("partnername", Data::STRING);
    lr.AddColumn("name", Data::STRING);
    lr.AddColumn("description", Data::STRING);
    
    lr.Bind(tariffgroup.id, "id");
    lr.Bind(partner.name, "partnername");
    lr.Bind(tariffgroup.name, "name");
    lr.Bind(tariffgroup.description, "description");
    
    while(ds.Fetch()) {
        lr.AddRow();
    }
    return lr;
}

const Value BALOONWeekTariffGroup::Add(const Str name, const Str description, const Str is_default) {
    const auto id = SaveTariffGroup(rdb_, name, description, is_default);
    Value res;
    res["id"] = id;
    AddMessage(Message("Week tariff group saved. ").What());
    return res;
}

void UpdateWeekTariffGroup(Connection* rdb, const Int id, const optional<Str> name, const optional<Str> description, const optional<Str> is_default) {
    WeekTariffGroup agroup(id);
    agroup.Select(rdb_);
    if(Defined(description)) agroup.description = *description;
    if(Defined(name)) {
        const auto& name_ = *name;
        if(NameExists<WeekTariffGroup>(rdb_, name_) && name_ != agroup.name) {
            throw Exception(Message("Tariff group already exists. ").What());
        }
        agroup.name = name_;
    }
    if(Defined(is_default)) {
        const auto is_default_ = *is_default;
        if(is_default_ == BOOL_TRUE) {
            SetTariffGroupsNotDefaultExcept(rdb_, id);
        }
        agroup.is_default = Defined(is_default_)? is_default_ : Str(BOOL_FALSE);
    }
    agroup.Update(rdb_);
}

const Value BALOONWeekTariffGroup::Update(const Int id, const optional<Str> name, const optional<Str> description, const optional<Str> is_default) {
    UpdateWeekTariffGroup(rdb_, id, name, description, is_default);
    AddMessage(Message("Tariff group updated. ").What());
    return Value();
}

const Value BALOONWeekTariffGroup::ForPartnerAdd(const Int partnerid, const Str name, const Str description) {
    const auto tariffgroupid = SaveTariffGroup(rdb_, name, description, {});
    PartnerWeekTariffGroup pwtg;
    pwtg.partnerid = partnerid;
    pwtg.tariffgroupid = tariffgroupid;
    pwtg.Insert(rdb_);
    AddMessage(Message("Tariff group saved. ").What());
    Value res;
    res["id"] = tariffgroupid;
    return res;
}

const Value BALOONWeekTariffGroup::ForPartnerUpdate(const Int id, const optional<Str> name, const optional<Str> description) {
    UpdateWeekTariffGroup(rdb_, id, name, description, {});
    AddMessage(Message("Tariff group updated. ").What());
    return Value();
}

const Value BALOONWeekTariffGroup::ForPartnerGet(const Int id) {
    WeekTariffGroup tariffgroup;
    PartnerWeekTariffGroup pwtg;
    Partner partner;
    Selector sel;
    sel << tariffgroup->id
        << pwtg->partnerid
        << tariffgroup->name
        << tariffgroup->description;
    sel.From(tariffgroup)
        .Join(pwtg).On(pwtg->tariffgroupid == tariffgroup->id);
    sel.Where(tariffgroup->id == id);
    auto ds = sel.Execute(rdb_);
    ds.Fetch();
    Value res;
    res["id"] = tariffgroup.id;
    res["partnerid"] = pwtg.partnerid;
    res["name"] = tariffgroup.name;
    res["description"] = tariffgroup.description;
    return res;
}

const Value BALOONWeekTariffGroup::ForPartnerDelete(const Int id) {
    WeekTariffGroup tgroup(id);
    tgroup.Select(rdb_);
    
    WeekTariff wtariff;
    Deleter del(wtariff);
    del.Where(wtariff->agroupid == id);
    del.Execute(rdb_);
    
    PartnerWeekTariffGroup pwtg;
    Deleter del2(pwtg);
    del2.Where(pwtg->tariffgroupid == id);
    del2.Execute(rdb_);
    
    tgroup.Delete(rdb_);
    AddMessage(Message("Tariff group deleted. ").What());
    return Value();
}

Data::DataList GetWeekTariffList(Connection* rdb, const optional<Int> agroupid, bool exclude_default) {
    WeekTariff wtariff;
    WeekTariffGroup tgroup;
    Selector sel;
    sel << wtariff
        << tgroup->name;
    sel.From(wtariff).Join(tgroup).On(wtariff->agroupid == tgroup->id);
    Expression ex;
    if(Defined(agroupid)) {
        ex = ex && wtariff->agroupid == *agroupid;
    }
    if(exclude_default) {
        ex = ex && (tgroup->is_default.IsNull() || tgroup->is_default != BOOL_TRUE);
    }
    sel.Where(ex);
    sel.OrderAsc(wtariff->week_day);
    sel.OrderAsc(wtariff->time_from);
    auto ds = sel.Execute(rdb);
    
    Data::DataList lr;
    lr.AddColumn("id", Data::INTEGER);
    lr.AddColumn("agroupname", Data::STRING);
    lr.AddColumn("week_day", Data::STRING);
    lr.AddColumn("time_from", Data::STRING);
    lr.AddColumn("time_upto", Data::STRING);
    lr.AddColumn("price", Data::DECIMAL);
    lr.AddColumn("pricetype", Data::STRING);
    lr.AddColumn("free", Data::INTEGER);
    lr.AddColumn("total", Data::INTEGER);
    lr.AddColumn("counttype", Data::STRING);

    lr.AddColumn("external_name", Data::STRING);
    lr.AddColumn("enabled", Data::STRING);
    lr.AddColumn("ratename", Data::STRING);
    
    lr.Bind(wtariff.id, "id");
    lr.Bind(tgroup.name, "agroupname");
    lr.Bind(wtariff.week_day, "week_day");
    lr.Bind(wtariff.time_from, "time_from");
    lr.Bind(wtariff.time_upto, "time_upto");
    lr.Bind(wtariff.price, "price");
    lr.Bind(wtariff.pricetype, "pricetype");
    lr.Bind(wtariff.free, "free");
    lr.Bind(wtariff.total, "total");
    lr.Bind(wtariff.counttype, "counttype");
    lr.Bind(wtariff.external_name, "external_name");
    lr.Bind(wtariff.enabled, "enabled");
    lr.Bind(wtariff.ratename, "ratename");
    
    while(ds.Fetch()) {
        lr.AddRow();
    }
    return lr;
}

const Value BALOONWeekTariff::WeekTariffListGet(const optional<Int> agroupid) {
    constexpr auto EXCLUDE_DEFAULT = false;
    return GetWeekTariffList(rdb_, agroupid, EXCLUDE_DEFAULT);
}

const Value  BALOONWeekTariff::PartnerWeekTariffListGet(const optional<Int> agroupid) {
    constexpr auto EXCLUDE_DEFAULT = true;
    return GetWeekTariffList(rdb_, agroupid, EXCLUDE_DEFAULT);
}

void WeekTariffMassAdd(Connection* rdb, const Int& agroupid, const Value& week_days, const Value& tariffs) {
    if(!tariffs.IsArray() || !week_days.IsArray()) {
        return;
    }
    const auto weekdays_size = week_days.Size();
    const auto tariffs_size = tariffs.Size();
    for(uint i = 0; i < weekdays_size; ++i) {
        const auto week_day = week_days[i]["week_day"].As<Str>();
        for(uint j = 0; j < tariffs_size; ++j) {
            const auto& entry = tariffs[j];
        
            WeekTariff wtariff;
            wtariff.agroupid = agroupid;
            wtariff.week_day = week_day;
            wtariff.time_from = entry["time_from"].As<ATime>();
            wtariff.time_upto = entry["time_upto"].As<ATime>();
            wtariff.price = entry["price"].As<Decimal>();
            wtariff.pricetype = entry["pricetype"].As<Str>();
            wtariff.free = entry["free"].As<Int>();
            wtariff.total = entry["total"].As<Int>();
            wtariff.counttype = entry["counttype"].As<Str>();
            wtariff.identifier_rulename = entry["identifier_rulename"].As<Str>();
            
            const auto service_rulename = entry["service_rulename"].As<Str>();
            
            wtariff.service_rulename = (!Defined(service_rulename) || service_rulename->empty())? Str() : service_rulename;
            wtariff.identifier_categoryname = entry["identifier_categoryname"].As<Str>();
            wtariff.ratename = entry["ratename"].As<Str>();
            wtariff.external_name = entry["external_name"].As<Str>();
            wtariff.enabled = BOOL_TRUE;

            wtariff.Insert(rdb_);
        }
    }
}

const Value BALOONWeekTariff::MassAdd(const Int agroupid, const Value week_days, const Value tariffs) {
    WeekTariffMassAdd(rdb_, agroupid, week_days, tariffs);
    AddMessage(Message("Week tariffs added. ").What());
    return Value();
}

Str GetWeekday(Connection* rdb, const ADate& adate) {
    Holiday hol(adate);
    const auto is_holiday = hol.Select(rdb);
    if(!is_holiday) {
        return Str(WEEKDAYS[static_cast<int>(adate.day_of_week())]);
    }
    if(hol.out_of_service == BOOL_TRUE) {
        return Str();
    }
    return Str(HIGHDAY);
}

Value GetHolidays(Connection* rdb, const ADate adate, const Str identifier_rulename, const Str identifier_categoryname, const Str ratename) {

    Holiday hol;

    Selector sel;
    sel << hol->adate
	<< hol->comment;
    sel.From(hol);
    sel.Where(hol->out_of_service == BOOL_TRUE &&
	((Defined(adate)) ? hol->adate >= adate : Expression()));
    sel.OrderAsc(hol->adate);

    auto ds = sel.Execute(rdb_);
    Value res;
    uint i = 0;
    while(ds.Fetch()) {
        Value entry;
        entry["adate"] = hol.adate;
        entry["comment"] = hol.comment;
        res[i] = entry;
        ++i;
    }
    return res;

}

Value GetAllSeanses(Connection* rdb, const ADate adate, const Str agroupname, const Str is_default, const Str identifier_rulename, const Str identifier_categoryname, const Str ratename) {
    const auto weekday = GetWeekday(rdb_, adate);
    if(!Defined(weekday)) {
        return Value();
    }
    WeekTariff tariff;
    WeekTariffGroup agroup;

    Selector sel;
    sel << tariff->id
        << tariff->time_from
        << tariff->time_upto
	<< agroup->id
	<< agroup->name
	<< agroup->description
        << tariff->price
	<< tariff->pricetype
        << tariff->free
        << tariff->total
	<< tariff->counttype
        << tariff->external_name
	<< agroup->id
	<< agroup->description
	<< agroup->name;

    sel.From(tariff).Join(agroup).On(tariff->agroupid == agroup->id);
    sel.Where(  tariff->week_day == weekday &&
                tariff->enabled == BOOL_TRUE &&
              ((Defined(is_default) && !is_default->empty()) ? agroup->is_default == is_default : Expression() ) &&
              ((Defined(agroupname) && !agroupname->empty()) ? agroup->name == agroupname : Expression() ) &&
              ((Defined(identifier_rulename) && !identifier_rulename->empty()) ? tariff->identifier_rulename == identifier_rulename : Expression()) &&
              ((Defined(identifier_categoryname) && !identifier_categoryname->empty()) ? tariff->identifier_categoryname == identifier_categoryname : Expression()) &&
              ((Defined(ratename) && !ratename->empty()) ? tariff->ratename == ratename : Expression()));
    sel.OrderAsc(tariff->time_from);
    
    auto ds = sel.Execute(rdb_);
    Value res;
    uint i = 0;
    while(ds.Fetch()) {
        Value entry;
        entry["id"] = tariff.id;
        entry["time_from"] = tariff.time_from;
        entry["time_upto"] = tariff.time_upto;
        entry["price"] = tariff.price;
	entry["pricetype"] = tariff.pricetype;
        entry["free"] = tariff.free;
        entry["total"] = tariff.total;
	entry["counttype"] = tariff.counttype;
        entry["name"] = tariff.external_name;
	entry["gid"] = agroup.id;
	entry["gdescription"] = agroup.description;
	entry["gname"] = agroup.name;
        res[i] = entry;
        ++i;
    }
    return res;
}


Value GetSeanses(Connection* rdb, const ADate adate, const Str identifier_rulename, const Str identifier_categoryname, const Str ratename) {
    const auto weekday = GetWeekday(rdb_, adate);
    if(!Defined(weekday)) {
        return Value();
    }
    WeekTariff tariff;
    WeekTariffGroup agroup;
    Selector sel;
    
    sel << tariff->id
        << tariff->time_from
        << tariff->time_upto
        << tariff->price
	<< tariff->pricetype
        << tariff->free
	<< tariff->counttype
        << tariff->total
        << tariff->external_name;
    sel.From(tariff).Join(agroup).On(tariff->agroupid == agroup->id);
    sel.Where(agroup->is_default == BOOL_TRUE &&
                tariff->week_day == weekday &&
                tariff->enabled == BOOL_TRUE &&
              ((Defined(identifier_rulename) && !identifier_rulename->empty()) ? tariff->identifier_rulename == identifier_rulename : Expression()) &&
              ((Defined(identifier_categoryname) && !identifier_categoryname->empty()) ? tariff->identifier_categoryname == identifier_categoryname : Expression()) &&
              ((Defined(ratename) && !ratename->empty()) ? tariff->ratename == ratename : Expression()));
    sel.OrderAsc(tariff->time_from);
    
    auto ds = sel.Execute(rdb_);
    Value res;
    uint i = 0;
    while(ds.Fetch()) {
        Value entry;
        entry["id"] = tariff.id;
        entry["time_from"] = tariff.time_from;
        entry["time_upto"] = tariff.time_upto;
        entry["price"] = tariff.price;
	entry["pricetype"] = tariff.pricetype;
        entry["free"] = tariff.free;
        entry["total"] = tariff.total;
	entry["counttype"] = tariff.counttype;
        entry["name"] = tariff.external_name;
	entry["gid"] = agroup.id;
	entry["gdescription"] = agroup.description;
	entry["gname"] = agroup.name;

        res[i] = entry;
        ++i;
    }
    return res;
}

const Value RestoreCounters(Connection* rdb)
{
    ADate adate = (TIME_NOW /*+ Computerica::get_utc_offset()*/).date();

    const auto weekday = GetWeekday(rdb_, adate);
    if(!Defined(weekday)) {
        return Value();
    }

    WeekTariff tariff;

    Selector sel;
    sel << tariff->id
        << tariff->price
	<< tariff->pricetype
        << tariff->free
        << tariff->total
	<< tariff->counttype;
    sel.From(tariff);
    sel.Where(  tariff->week_day != weekday &&
                tariff->enabled == BOOL_TRUE );
    sel.OrderAsc(tariff->time_from);

    auto ds = sel.Execute(rdb_);
    while(ds.Fetch()) {

        WeekTariff t(tariff.id);
        t.Select(rdb_);

	if ( t.counttype != BOOL_TRUE ) {
		t.free = t.total;
	}

	t.Update(rdb_);
    }

    return Value();
}

const Value BALOONHoliday::HolidaysFromDateListGet_FE(const ADate adate) {
    return GetHolidays(rdb_, adate, Str(), Str(), Str());
}

const Value BALOONWeekTariff::AsyncRestoreCounters() {
    return RestoreCounters(rdb_);
}

const Value BALOONWeekTariff::SeansesAllByDateListGet_FE(const ADate adate, const Str agroupname, const Str is_default ) {
    return GetAllSeanses(rdb_, adate, agroupname, is_default, Str(), Str(), Str());
}

const Value BALOONWeekTariff::SeansesByDateListGet_FE(const ADate adate) {
    return GetSeanses(rdb_, adate, Str(), Str(), Str());
}

const Value BALOONWeekTariff::SeansesListGet_FE(const ADate adate, const Str identifier_rulename, const Str identifier_categoryname, const Str ratename) {
    return GetSeanses(rdb_, adate, identifier_rulename, identifier_categoryname, ratename);
}

const Value BALOONWeekTariff::SeansesByDateListGet(const ADate adate) {
    WeekTariff tariff;
    WeekTariffGroup agroup;
    Data::DataList lr;
    lr.AddColumn("id", Data::INTEGER);
    lr.AddColumn("time_from", Data::STRING);
    lr.AddColumn("time_upto", Data::STRING);
    lr.AddColumn("price", Data::DECIMAL);
    lr.AddColumn("pricetype", Data::STRING);
    lr.AddColumn("free", Data::INTEGER);
    lr.AddColumn("total", Data::INTEGER);
    lr.AddColumn("counttype", Data::STRING);
    lr.AddColumn("name", Data::STRING);

    lr.Bind(tariff.id, "id");
    lr.Bind(tariff.time_from, "time_from");
    lr.Bind(tariff.time_upto, "time_upto");
    lr.Bind(tariff.price, "price");
    lr.Bind(tariff.pricetype, "pricetype");
    lr.Bind(tariff.free, "free");
    lr.Bind(tariff.total, "total");
    lr.Bind(tariff.counttype, "counttype");

    lr.Bind(tariff.external_name, "name");
    
    const auto weekday = GetWeekday(rdb_, adate);
    if(!Defined(weekday)) {
        return lr;
    }
    Selector sel;
    sel << tariff->id
        << tariff->time_from
        << tariff->time_upto
        << tariff->price
        << tariff->pricetype
        << tariff->free
        << tariff->total
        << tariff->counttype
        << tariff->external_name;
    sel.From(tariff).Join(agroup).On(tariff->agroupid == agroup->id);
    sel.Where(tariff->week_day == weekday && agroup->is_default == BOOL_TRUE && tariff->enabled == BOOL_TRUE);
    sel.OrderAsc(tariff->time_from);
    auto ds = sel.Execute(rdb_);
    while(ds.Fetch()) {
        lr.AddRow();
    }
    return lr;
}

const Value BALOONWeekTariff::ForPartnerDelete(const Int id) {
    WeekTariff wtariff(id);
    wtariff.Delete(rdb_);
    AddMessage(Message("Week tariff deleted. ").What());
    return Value();
}

map<Int, WeekTariff> GetPartnerSeansesByDate(Connection* rdb, const Int& partnerid, const ADate& adate) {
    if(!Defined(partnerid) || !Defined(adate)) {
        return {};
    }
    const auto weekday = GetWeekday(rdb_, adate);
    if(!Defined(weekday)) {
        return {};
    }
    WeekTariff wtariff;
    WeekTariffGroup tgroup;
    PartnerWeekTariffGroup partner_group;
    Selector sel;
    sel << wtariff;
    sel.From(wtariff)
        .Join(tgroup).On(wtariff->agroupid == tgroup->id)
        .Join(partner_group).On(partner_group->tariffgroupid == tgroup->id);
    sel.Where(partner_group->partnerid == partnerid && wtariff->week_day == weekday && wtariff->enabled == BOOL_TRUE);
    auto ds = sel.Execute(rdb);
    map<Int, WeekTariff> res;
    while(ds.Fetch()) {
        res.insert({wtariff.id, wtariff});
    }
    return res;
}

map<Int, WeekTariff> GetPublicSeansesByDate(Connection* rdb, const ADate& adate) {
    const auto weekday = GetWeekday(rdb_, adate);
    if(!Defined(weekday)) {
        return {};
    }
    WeekTariff wtariff;
    WeekTariffGroup tgroup;
    Selector sel;
    sel << wtariff;
    sel.From(wtariff)
        .Join(tgroup).On(wtariff->agroupid == tgroup->id);
    sel.Where(tgroup->is_default == BOOL_TRUE && wtariff->week_day == weekday && wtariff->enabled == BOOL_TRUE);
    auto ds = sel.Execute(rdb);
    map<Int, WeekTariff> res;
    while(ds.Fetch()) {
        res.insert({wtariff.id, wtariff});
    }
    return res;
}

const Value BALOONWeekTariff::OurSeansesByDateListGet(const ADate adate) {
    Int wtariffid;
    string external_name;
    
    Data::DataList lr;
    lr.AddColumn("id", Data::INTEGER);
    lr.AddColumn("external_name", Data::STRING);
    
    lr.Bind(wtariffid, "id");
    lr.Bind(external_name, "external_name");
    
    const auto partnerid = CurrentPartnerID(rdb_);
    const auto partner_tariffs = GetPartnerSeansesByDate(rdb_, partnerid, adate);
    for(const auto& item : partner_tariffs) {
        const auto& wtariff = item.second;
        wtariffid = wtariff.id;
        external_name = wtariff.external_name.get_value_or("") + " (Партнёрский)";
        lr.AddRow();
    }
    const auto public_tariffs = GetPublicSeansesByDate(rdb_, adate);
    for(const auto& item : public_tariffs) {
        const auto& wtariff = item.second;
        wtariffid = wtariff.id;
        external_name = wtariff.external_name.get_value_or("") + " (Публичный)";
        lr.AddRow();
    }
    return lr;
}

Int PartnerByToken(Connection* rdb, const Str& token) {
    Partner partner;
    Selector sel;
    sel << partner->id;
    sel.Where(partner->token == token);
    sel.Limit(1);
    auto ds = sel.Execute(rdb);
    if(!ds.Fetch()) {
        throw Exception(Message("Wrong token. ").What());
    }
    return partner.id;
}

const Value BALOONWeekTariff::OurSeansesByDateListGet_API(const ADate adate, const Str token) {
    const auto partnerid = PartnerByToken(rdb_, token);
    const auto partner_tariffs = GetPartnerSeansesByDate(rdb_, partnerid, adate);
    Value res;
    uint i(0);
    for(const auto& ptariff : partner_tariffs) {
        Value entry;
        const auto& wtariff = ptariff.second;
        entry["id"] = wtariff.id;
        entry["external_name"] = wtariff.external_name;
        res[i] = entry;
        ++i;
    }
    return res;
}

const Value BALOONSkipassConfiguration::MassAdd(const Str groupname, const Value configurations) {
    const auto size = configurations.Size();
    if(!configurations.IsArray() || !size) {
        throw Exception(Message("No skipass configurations defined. ").What());
    }
    for(uint i = 0; i < size; ++i) {
        const auto row = configurations[i];
        
        const auto name = row["name"].As<Str>();
        const auto identifier_rule = row["identifier_rulename"].As<Str>();
        const auto service_rule = row["service_rulename"].As<Str>();
        const auto identifier_category = row["identifier_categoryname"].As<Str>();
        const auto identifier_rate = row["ratename"].As<Str>();
        const auto valid_from = row["valid_from"].As<Time>();
        const auto valid_to = row["valid_to"].As<Time>();
        const auto make_transaction = row["make_transaction"].As<string>();
        const auto price = row["price"].As<Decimal>();
        
        if(!Defined(name)) {
            AddMessage(Message("Name is not defined. Continue. ").What());
            continue;
        }        
        if(!Defined(identifier_rule)) {
            throw Exception(Message("Identifier rule is mandatory. ").What());
        }
        
        SkipassConfiguration conf;
        conf.groupname = groupname;
        conf.name = name;
        conf.identifier_rulename = identifier_rule;
        conf.service_rulename = service_rule;
        conf.identifier_categoryname = identifier_category;
        conf.ratename = identifier_rate;
        conf.valid_from = valid_from;
        conf.valid_to = valid_to;
        conf.price = price;
        conf.make_transaction = make_transaction == BOOL_TRUE? BOOL_TRUE : BOOL_FALSE;
        conf.enabled = ENABLED;
        
        conf.Insert(rdb_);
    }
    AddMessage(Message("Skipass configurations saved. ").What());
    return Value();
}

const Value BALOONSkipassConfiguration::SkipassConfigurationGroupNameListGet_API() {
    SkipassConfiguration conf;
    Selector sel;
    sel << Distinct()
        << conf->groupname;
    sel.Where(conf->enabled == ENABLED);
    auto ds = sel.Execute(rdb_);
    Value res;
    uint i = 0;
    while(ds.Fetch()) {
        Value item;
        item["groupname"] = conf.groupname;
        res[i] = item;
        ++i;
    }
    return res;
}

const Value BALOONSkipassConfiguration::SkipassConfigurationListGet_API(const Str groupname) {
    SkipassConfiguration conf;
    Selector sel;
    sel << conf->name
        << conf->price
        << conf->id;
    sel.Where(conf->enabled == ENABLED &&
             (Defined(groupname)? conf->groupname == groupname : Expression()));
    sel.OrderAsc(conf->valid_from);
    sel.OrderAsc(conf->id);
    auto ds = sel.Execute(rdb_);
    Value res;
    uint i = 0;
    while(ds.Fetch()) {
        Value item;
        item["id"] = conf.id;
        item["name"] = conf.name;
        item["price"] = conf.price;
        res[i] = item;
        ++i;
    }
    return res;
}

template<typename T>
std::set<Str> cache_table(Connection* rdb) {
    T table;
    Selector sel;
    sel << Distinct()
        << table->name;
    auto ds = sel.Execute(rdb);
    set<Str> res;
    while(ds.Fetch()) {
        res.insert(table.name);
    }
    return res;
}

template<typename T>
std::map<Str, Int> cache_table_by_name(Connection* rdb) {
    T table;
    Selector sel;
    sel << Distinct()
        << table->id
        << table->name;
    auto ds = sel.Execute(rdb);
    map<Str, Int> res;
    while(ds.Fetch()) {
        res.insert({table.name, table.id});
    }
    return res;
}

template<typename T>
Value MassAddEntity(Connection* rdb, const Value& names) {
    const auto size = names.Size();
    if(!names.IsArray() || !size) {
        return Value();
    }
    const auto existing_names = cache_table<T>(rdb);
    auto end_iter = existing_names.cend();
    
    for(uint i(0); i < size; ++i) {
        const auto& entry = names[i];
        const auto name = entry["name"].As<Str>();
        
        if(Defined(name) && existing_names.find(name) == end_iter) {
            T table;
            table.name = name;
            table.status = ENABLED;
            table.Insert(rdb);
        }
    }
    return Value();
}

const Value BALOONPaidServiceCategory::MassAdd(const Value names) {
    return MassAddEntity<PaidServiceCategory>(rdb_, names);
}

const Value BALOONPaidServiceTariff::MassAdd(const Value names) {
    return MassAddEntity<PaidServiceTariff>(rdb_, names);
}

const Value BALOONPaidServiceHour::MassAdd(const Value names) {
    return MassAddEntity<PaidServiceHour>(rdb_, names);
}

const Value BALOONPaidService::MassAdd(const Value services) {
    const auto size = services.Size();
    if(!services.IsArray() || !size) {
        return Value();
    }
    for(uint i(0); i < size; ++i) {
        const auto& item = services[i];
        PaidService pservice;
        pservice.categoryid = item["category"].As<Int>();
        pservice.name = item["name"].As<Str>();
        pservice.tariffid = item["tariff"].As<Int>();
        pservice.hourid = item["hour"].As<Int>();
        pservice.description = item["description"].As<Str>();
        pservice.price = item["price"].As<Decimal>();
        pservice.status = item["status"].As<Str>();
        try {
            pservice.Insert(rdb_);
        }
        catch(const Exception&) {}
    }
    AddMessage(Message("Pricelist created. ").What());
    return Value();
}

template<typename T>
Int GetOrCreateEntity(Connection* rdb, const Str& name) {
    if(StrEmpty(name)) {
        return Int();
    }
    static auto cached_data = cache_table_by_name<T>(rdb);
    auto iter = cached_data.find(name);
    Int id;
    if(iter == cached_data.cend()) {
        T table;
        table.name = name;
        table.status = ENABLED;
        id = table.Insert(rdb);
        cached_data.insert({name, id});
    }
    else {
        id = iter->second;
    }
    return id;
}

const Value BALOONPaidService::ImportPricelist(const BlobFile pricelist) {
    if(!pricelist.Size()) {
        return Value();
    }
    const auto categories = cache_table_by_name<PaidServiceCategory>(rdb_);
    const auto tariffs = cache_table_by_name<PaidServiceTariff>(rdb_);
    const auto hours = cache_table_by_name<PaidServiceHour>(rdb_);
    
    const auto& blob = pricelist.Content();
    const auto data = Computerica::parseCSV(blob, ",");
    const size_t CATEGORY(0);
    const size_t NAME(1);
    const size_t HOUR(2);
    const size_t TARIFF(3);
    const size_t PRICE(4);
    const size_t STATUS(5);
    for(const auto& row : data) {
        const auto categoryname = row[CATEGORY];
        const auto categoryid = GetOrCreateEntity<PaidServiceCategory>(rdb_, categoryname);
        
        const auto hourname = row[HOUR];
        const auto hourid = GetOrCreateEntity<PaidServiceHour>(rdb_, hourname);
        
        const auto tariffname = row[TARIFF];
        const auto tariffid = GetOrCreateEntity<PaidServiceTariff>(rdb_, tariffname);
        
        PaidService service;
        service.categoryid = categoryid;
        service.name = row[NAME];
        service.hourid = hourid;
        service.tariffid = tariffid;
        service.price = row[PRICE];
        service.status = row[STATUS];
        service.Insert(rdb_);
    }
    return Value();
}

const Value BALOONPaidServiceCategory::PaidServiceCategoryListGet_FE() {
    PaidServiceCategory category;
    Selector sel;
    sel << category->id
        << category->name;
    sel.Where(category->status == ENABLED);
    auto ds = sel.Execute(rdb_);
    Value res;
    uint i(0);
    while(ds.Fetch()) {
        Value item;
        item["id"] = category.id;
        item["name"] = category.name;
        res[i] = item;
        ++i;
    }
    return res;
}

const Value BALOONPaidService::PaidServiceListGet_FE(const Int categoryid) {
    PaidService pservice;
    PaidServiceCategory pscategory;
    PaidServiceHour pshour;
    PaidServiceTariff pstariff;
    
    Selector sel;
    sel << pservice->id
        << pservice->name
        << pservice->description
        << pservice->price
        << pscategory->name
        << pscategory->id
        << pstariff->name
        << pshour->name;
    sel.From(pservice)
        .Join(pscategory).On(pservice->categoryid == pscategory->id)
        .Join(pstariff).On(pservice->tariffid == pstariff->id)
        .Join(pshour).On(pservice->hourid == pshour->id);
    if(Defined(categoryid)) {
        sel.Where(pservice->categoryid == categoryid);
    }
    auto ds = sel.Execute(rdb_);
    Value res;
    uint i(0);
    while(ds.Fetch()) {
        Value entry;
        entry["id"] = pservice.id;
        entry["name"] = pservice.name;
        entry["description"] = pservice.description;
        entry["price"] = pservice.price;
        entry["categoryid"] = pscategory.id;
        entry["categoryname"] = pscategory.name;
        entry["tariffname"] = pstariff.name;
        entry["pshourname"] = pshour.name;
        res[i] = entry;
        ++i;
    }
    return res;
}

struct PSKey {
    Str name;
    Str categoryname;
    Str tariffname;
    bool operator<(const PSKey& other) const;
    PSKey(const Str& name_, const Str& categoryname_, const Str& tariffname_) : name(name_), categoryname(categoryname_), tariffname(tariffname_) {}
    PSKey() = default;
private:
    tuple<Str, Str, Str> ToTuple() const { return tuple<Str, Str, Str>{ name, categoryname, tariffname }; }
};

bool PSKey::operator<(const PSKey& other) const {
    const auto tpl = ToTuple();
    return (tpl < other.ToTuple());
}

struct PSValue {
    PSValue() = default;
    PSValue(const Str& hourname_, const Decimal& price_) : hourname(hourname_), price(price_) {}
    Str hourname;
    Decimal price;
};

map<PSKey, vector<PSValue>> GetServicesClassifiedByHour(Connection* rdb, const Int& categoryid) {
    PaidService pservice;
    PaidServiceCategory pscategory;
    PaidServiceHour pshour;
    PaidServiceTariff pstariff;
    Selector sel;
    sel << pservice->name
        << pservice->price
        << pscategory->name
        << pstariff->name
        << pshour->name;
    sel.From(pservice)
        .Join(pscategory).On(pservice->categoryid == pscategory->id)
        .LeftJoin(pstariff).On(pservice->tariffid == pstariff->id)
        .Join(pshour).On(pservice->hourid == pshour->id);
    if(Defined(categoryid)) {
        sel.Where(pservice->categoryid == categoryid);
    }
    auto ds = sel.Execute(rdb_);
    map<PSKey, vector<PSValue>> res;
    while(ds.Fetch()) {
        const PSKey key(pservice.name, pscategory.name, pstariff.name);
        auto& psvalues = res[key];
        psvalues.push_back({ pshour.name, pservice.price });
    }
    return res;
}

/// Возратить список услуг с категориями и часами
const Value BALOONPaidService::PaidServiceListGet_API(const Int categoryid) {
    const auto services = GetServicesClassifiedByHour(rdb_, categoryid);
    Value res;
    uint i(0);
    for(const auto& item : services) {
        const auto pskey = item.first;
        const auto prices = item.second;
        
        Value entry;
        entry["name"] = pskey.name;
        entry["categoryname"] = pskey.categoryname;
        entry["tariffname"] = pskey.tariffname;
        
        uint j(0);
        for(const auto& psvalue : prices) {
            Value pricevalue;
            pricevalue["hourname"] = psvalue.hourname;
            pricevalue["price"] = psvalue.price;
            entry["prices"][j] = pricevalue;
            ++j;
        }
        res[i] = entry;
        ++i;
    }
    return res;
}

std::mutex partner_token_mutex;
string GetUniqueToken(Connection* rdb) {
    constexpr int ATTEMPS_NR  = 16;
    constexpr int32_t CODE_LENGTH = 10;
    for(int i(0); i < ATTEMPS_NR; ++i) {
        std::lock_guard<std::mutex> lock(partner_token_mutex);
        const auto token = Computerica::randomCode(CODE_LENGTH);
        Partner partner;
        Selector sel;
        sel << partner->id;
        sel.Where(partner->token == token);
        auto ds = sel.Execute(rdb);
        if(!ds.Fetch()) {
            return token;
        }
    }
    throw Exception(Message("Could not generate unique token. ").What());
}

Int SavePartner(Connection* rdb, const Str name, const Str legal_address, const Str post_address, const Str inn,
                               const Str kpp, const Str payment_account, const Str correspondent_account, const Str bank,
                               const Str bik, const Str okpo, const Str okato, const Str okved,
                               const Str ogrn, const Str principal, const Str phone, const Str fax, const Str email, const Str notification_email) {
    Partner partner;
    partner.name = name;
    partner.status = ENABLED;
    partner.legal_address = legal_address;
    partner.post_address = post_address;
    partner.inn = inn;
    partner.kpp = kpp;
    partner.payment_account = payment_account;
    partner.correspondent_account = correspondent_account;
    partner.bank = bank;
    partner.bik = bik;
    partner.okpo = okpo;
    partner.okato = okato;
    partner.okved = okved;
    partner.ogrn = ogrn;
    partner.principal = principal;
    partner.phone = phone;
    partner.fax = fax;
    partner.email = email;
    partner.token = GetUniqueToken(rdb);
    partner.notification_email = notification_email;
    const auto id = partner.Insert(rdb);
    return id;
}

Int SaveContract(Connection* rdb, const Int& partnerid, const Str& barcode, const ADate& start_date, const ADate& end_date, const Str& benefit_type, const Decimal& benefit_amount) {
    PartnerContract contract;
    contract.partnerid = partnerid;
    contract.barcode = barcode;
    contract.start_date = start_date;
    contract.end_date = end_date;
    contract.benefit_type = benefit_type;
    contract.benefit_amount = benefit_amount;
    const auto id = contract.Insert(rdb);
    return id;
}

Int SaveAccount(Connection* rdb, const Int& partnerid, const Str& name, const Decimal& amount) {
    PartnerAccount account;
    account.partnerid = partnerid;
    account.name = name;
    account.atype = DEBIT_ACCOUNT;
    account.amount = amount;
    account.status = ACTIVE;
    const auto id = account.Insert(rdb);
    return id;
}

enum class transaction_type {
    REFILL, EXPENSE
};

template<transaction_type>
struct TransactionType;
template<>
struct TransactionType<transaction_type::REFILL> {
    static const std::string name;
};
const std::string TransactionType<transaction_type::REFILL>::name = REFILL;
template<>
struct TransactionType<transaction_type::EXPENSE> {
    static const std::string name;
};
const std::string TransactionType<transaction_type::EXPENSE>::name = EXPENSE;

template<transaction_type T>
Int SaveAccountTransaction(Connection* rdb, const Int& accountid, const Decimal& amount, const Str& reason) {
    AccountTransaction transaction;
    transaction.accountid = accountid;
    transaction.amount = amount;
    transaction.reason = reason;
    transaction.cache_flow = TransactionType<T>::name;
    const auto id = transaction.Insert(rdb);
    return id;
}

Int SaveManager(Connection* rdb, const Int& partnerid, const Str& fullname, const Str& passport_number, const ADate& issue_date, const Str& issued_by,
                                 const Str& department_code, const Str& phone, const Str& email, const Str& password, const Str& password2) {
    const auto userid = CreateUser(rdb, fullname, email, password, password2, MANAGER_GROUPID);
    PartnerManager manager;
    manager.auserTokenID = userid;
    manager.partnerid = partnerid;
    manager.fullname = fullname;
    manager.passport_number = passport_number;
    manager.issue_date = issue_date;
    manager.issued_by = issued_by;
    manager.department_code = department_code;
    manager.phone = phone;
    const auto id = manager.Insert(rdb);
    return id;
}

enum class account_type {
    DEBIT, CREDIT
};

template<account_type T>
struct PartnerAccountType;
template<>
struct PartnerAccountType<account_type::DEBIT> {
    static const string name;
    static constexpr const char* const accountid = "debitaccountid";
    static constexpr const char* const accountname = "debitaccountname";
    static constexpr const char* const accountatype = "debitaccountatype";
    static constexpr const char* const accountstatus = "debitaccountstatus";
    static constexpr const char* const accountamount = "debitaccountamount";
    static constexpr const char* const accountexists = "debitaccountexists";
};

template<>
struct PartnerAccountType<account_type::CREDIT> {
    static const string name;
    static constexpr const char* const accountid = "creditaccountid";
    static constexpr const char* const accountname = "creditaccountname";
    static constexpr const char* const accountatype = "creditaccountatype";
    static constexpr const char* const accountstatus = "creditaccountstatus";
    static constexpr const char* const accountamount = "creditaccountamount";
    static constexpr const char* const accountexists = "creditaccountexists";
};

const string PartnerAccountType<account_type::DEBIT>::name = DEBIT_ACCOUNT;
const string PartnerAccountType<account_type::CREDIT>::name = CREDIT_ACCOUNT;

template<account_type T>
map<Int, Double> GetPartnersAccountAmount(Connection* rdb) {
    PartnerAccount account;
    Selector sel;
    sel << account->partnerid
        << account->amount;
    sel.Where(account->atype == PartnerAccountType<T>::name);
    auto ds = sel.Execute(rdb);
    map<Int, Double> res;
    while(ds.Fetch()) {
        res.insert({account.partnerid, account.amount});
    }
    return res;
}

map<Int, pair<Str, ADate>> GetPartnerContracts(Connection* rdb) {
    PartnerContract contract;
    Selector sel;
    ADate end_date;
    sel << contract->end_date.Max().Bind(end_date)
        << contract->partnerid
        << contract->benefit_type;
    sel.GroupBy(contract->partnerid);
    auto ds = sel.Execute(rdb);
    map<Int, pair<Str, ADate>> res;
    while(ds.Fetch()) {
        res.insert({contract.partnerid, {contract.benefit_type, end_date}});
    }
    return res;
}

const Value BALOONPartner::PartnerListGet() {
    Partner partner;
    Selector sel;
    sel << partner->id
        << partner->name
        << partner->status
        << partner->token;
    Data::DataList lr;
    lr.AddColumn("id", Data::INTEGER);
    lr.AddColumn("name", Data::STRING);
    lr.AddColumn("status", Data::STRING);
    lr.AddColumn("debitaccountamount", Data::DECIMAL);
    lr.AddColumn("creditaccountamount", Data::DECIMAL);
    lr.AddColumn("contractend_date", Data::DATETIME);
    lr.AddColumn("contractbenefit_type", Data::STRING);
    lr.AddColumn("token", Data::STRING);
    
    Decimal debitaccountamount;
    Decimal creditaccountamount;
    ADate contractend_date;
    Str contractbenefit_type;
    
    lr.Bind(partner.id, "id");
    lr.Bind(partner.name, "name");
    lr.Bind(partner.status, "status");
    lr.Bind(debitaccountamount, "debitaccountamount");
    lr.Bind(creditaccountamount, "creditaccountamount");
    lr.Bind(contractend_date, "contractend_date");
    lr.Bind(contractbenefit_type, "contractbenefit_type");
    lr.Bind(partner.token, "token");
    
    auto balance = GetPartnersAccountAmount<account_type::DEBIT>(rdb_);
    auto contracts = GetPartnerContracts(rdb_);
    auto ds = sel.Execute(rdb_);
    while(ds.Fetch()) {
        const auto partnerid = partner.id;
        debitaccountamount = balance[partnerid];
        creditaccountamount = {};//TODO
        const auto type_date = contracts[partnerid];
        contractend_date = type_date.second;
        contractbenefit_type = type_date.first;
        lr.AddRow();
    }
    return lr;
}

const Value BALOONPartner::Add(const Str name, const Str legal_address, const Str post_address, const Str inn,
                               const Str kpp, const Str payment_account, const Str correspondent_account, const Str bank,
                               const Str bik, const Str okpo, const Str okato, const Str okved,
                               const Str ogrn, const Str principal, const Str phone, const Str fax,
                               const Str email, const Str notification_email, const Str contractbarcode, const ADate start_date, const ADate end_date,
                               const Str benefit_type, const Decimal benefit_amount, const Str accountname, const Decimal amount,
                               const Str reason, const Str managerfullname, const Str passport_number, const ADate issue_date,
                               const Str issued_by, const Str department_code, const Str managerphone, const Str manageremail,
                               const Str password, const Str password2) {
    const auto partnerid = SavePartner(rdb_, name, legal_address, post_address, inn,
                                       kpp, payment_account, correspondent_account, bank,
                                       bik, okpo, okato, okved,
                                       ogrn, principal, phone, fax, email, notification_email);
    const auto contractid = SaveContract(rdb_, partnerid, contractbarcode, start_date, end_date, benefit_type, benefit_amount);
    const auto accountid = SaveAccount(rdb_, partnerid, accountname, amount);
    const auto transactionid = SaveAccountTransaction<transaction_type::REFILL>(rdb_, accountid, amount, reason);
    const auto managerid = SaveManager(rdb_, partnerid, managerfullname, passport_number, issue_date, issued_by, department_code, managerphone, manageremail, password, password2);
    Value res;
    res["id"] = partnerid;
    AddMessage(Message("Partner saved. ").What());
    return res;
}

const Value BALOONPartnerManager::Add(const Int partnerid, const Str fullname, const Str passport_number, const ADate issue_date, const Str issued_by,
                                      const Str department_code, const Str phone, const Str email, const Str password, const Str password2) {
    const auto id = SaveManager(rdb_, partnerid, fullname, passport_number, issue_date, issued_by, department_code, phone, email, password, password2);
    Value res;
    res["id"] = id;
    AddMessage(Message("Manager saved. ").What());
    return res;
}

const Value BALOONPartner::OurGet() {
    const auto partnerid = CurrentPartnerID(rdb_);
    Value res;
    Partner partner(partnerid);
    partner.Select(rdb_);
    res["id"] = partner.id;
    res["name"] = partner.name;
    res["status"] = partner.status;
    res["legal_address"] = partner.legal_address;
    res["post_address"] = partner.post_address;
    res["inn"] = partner.inn;
    res["kpp"] = partner.kpp;
    res["payment_account"] = partner.payment_account;
    res["correspondent_account"] = partner.correspondent_account;
    res["bank"] = partner.bank;
    res["bik"] = partner.bik;
    res["okpo"] = partner.okpo;
    res["okato"] = partner.okato;
    res["okved"] = partner.okved;
    res["ogrn"] = partner.ogrn;
    res["principal"] = partner.principal;
    res["phone"] = partner.phone;
    res["fax"] = partner.fax;
    res["email"] = partner.email;
    res["token"] = partner.token;
    return res;
}

template<typename T>
Value UserEntityGetCurrent(Connection* rdb) {
    const auto current_user = AUTHORIZER::AUTHORIZERUsers::GetCurrent();
    const auto userid = current_user["TokenID"].As<Int64>();
    T entity(userid);
    Value res;
    if(entity.Select(rdb)) {
        res["id"] = userid;
    }
    return res;
}

const Value BALOONPartnerManager::GetCurrent() {
    return UserEntityGetCurrent<PartnerManager>(rdb_);
}

const Value BALOONPartnerManager::MyGet() {
    const auto current_user = AUTHORIZER::AUTHORIZERUsers::GetCurrent();
    const auto userid = current_user["TokenID"].As<Int64>();
    PartnerManager manager(userid);
    Value res;
    if(manager.Select(rdb_)) {
       res["fullname"] = manager.fullname;
       res["passport_number"] = manager.passport_number;
       res["issue_date"] = manager.issue_date;
       res["issued_by"] = manager.issued_by;
       res["department_code"] = manager.department_code;
       res["phone"] = manager.phone;
       res["email"] = current_user["Login"].As<Str>();
    }
    return res;
}

const Value BALOONPartnerManager::MyUpdate(const optional<Str> passport_number, const optional<ADate> issue_date, const optional<Str> issued_by, const optional<Str> department_code, const optional<Str> phone) {
    const auto current_user = AUTHORIZER::AUTHORIZERUsers::GetCurrent();
    const auto userid = current_user["TokenID"].As<Int64>();
    PartnerManager manager(userid);
    if(manager.Select(rdb_)) {
       if(Defined(passport_number)) manager.passport_number = *passport_number;
       if(Defined(issue_date)) manager.issue_date = *issue_date;
       if(Defined(issued_by)) manager.issued_by = *issued_by;
       if(Defined(department_code)) manager.department_code = *department_code;
       if(Defined(phone)) manager.phone = *phone;
       manager.Update(rdb_);
       AddMessage(Message("Personal data updated. ").What());
    }
    return Value();
}

const Value BALOONPartnerAdministrator::Add(const Str fullname, const Str email, const Str password, const Str password2) {
    const auto userid = CreateUser(rdb_, fullname, email, password, password2, PARTNER_ADMIN_GROUPID);
    
    PartnerAdministrator admin;
    admin.auserTokenID = userid;
    admin.fullname = fullname;
    admin.Insert(rdb_);
    Value res;
    res["id"] = userid;
    AddMessage(Message("Partner administrator saved. ").What());
    return res;
}

const Value BALOONPartnerAdministrator::GetCurrent() {
    return UserEntityGetCurrent<PartnerAdministrator>(rdb_);
}

Partner CurrentPartner(Connection* rdb) {
    const auto current_user = AUTHORIZER::AUTHORIZERUsers::GetCurrent();
    const auto userid = current_user["TokenID"].As<Int64>();
    
    PartnerManager manager;
    Partner partner;
    Selector sel;
    
    sel << partner;
    sel.From(manager).Join(partner).On(manager->partnerid == partner->id);
    sel.Where(manager->auserTokenID == userid);
    auto ds = sel.Execute(rdb);
    if(!ds.Fetch()) {
        throw Exception(Message("Partner not found for current user. ").What());
    }
    return partner;
}

Int CurrentPartnerID(Connection* rdb) {
    const auto partner = CurrentPartner(rdb);
    return partner.id;
}

const Value BALOONPartnerContract::OurPartnerContractListGet() {
    const auto current_partnerid = CurrentPartnerID(rdb_);
    PartnerContract contract;
    Selector sel;
    sel << contract->id
        << contract->barcode
        << contract->start_date
        << contract->end_date
        << contract->benefit_type
        << contract->benefit_amount;
    sel.Where(contract->partnerid == current_partnerid);
    auto ds = sel.Execute(rdb_);
    
    Data::DataList lr;
    lr.AddColumn("id", Data::INTEGER);
    lr.AddColumn("barcode", Data::STRING);
    lr.AddColumn("start_date", Data::DATETIME);
    lr.AddColumn("end_date", Data::DATETIME);
    lr.AddColumn("benefit_type", Data::STRING, PARTNER_BENEFITValues());
    lr.AddColumn("benefit_amount", Data::DECIMAL);
    
    lr.Bind(contract.id, "id");
    lr.Bind(contract.barcode, "barcode");
    lr.Bind(contract.start_date, "start_date");
    lr.Bind(contract.end_date, "end_date");
    lr.Bind(contract.benefit_type, "benefit_type");
    lr.Bind(contract.benefit_amount, "benefit_amount");
    
    while(ds.Fetch()) {
        lr.AddRow();
    }
    return lr;
}

const Value BALOONPartnerContract::OurGet(const Int id) {
    const auto current_partnerid = CurrentPartnerID(rdb_);
    PartnerContract contract(id);
    contract.Select(rdb_);
    if(contract.partnerid != current_partnerid) {
        throw Exception(Message("Forbidden. ").What());
    }
    Value res;
    res["id"] = id;
    res["barcode"] = contract.barcode;
    res["start_date"] = contract.start_date;
    res["end_date"] = contract.end_date;
    res["benefit_type"] = contract.benefit_type;
    res["benefit_amount"] = contract.benefit_amount;
    return res;
}

std::mutex account_;
template<transaction_type T>
void DoAccountTransaction(Connection* rdb, const Int& accountid, const Decimal& amount, const Str& reason) {
    PartnerAccount account(accountid);
    std::lock_guard<std::mutex> lock_(account_);
    account.Select(rdb_);
    if(account.status == BLOCKED) {
        throw Exception(Message("Cannot operate on blocked account. ").What());
    }
    const auto current_amount = account.amount;
    const auto new_amount = (T == transaction_type::REFILL? current_amount + amount : current_amount - amount);
    if(new_amount.Negative()) {
        throw Exception(Message("Insufficient funds. ").What());
    }
    account.amount = new_amount;
    account.Update(rdb_);
    SaveAccountTransaction<T>(rdb_, accountid, amount, reason);
    rdb_->Commit();
}

const Value BALOONPartnerAccount::Refill(const Int id, const Decimal refill_amount, const Str reason) {
    if(!Defined(reason) || reason->empty()) {
        throw Exception(Message("Refill reason is not defined. ").What());
    }
    DoAccountTransaction<transaction_type::REFILL>(rdb_, id, refill_amount, reason);
    AddMessage(Message("Account refilled. ").What());
    return Value();
}

const Value BALOONPartnerAccount::Block(const Int id) {
    PartnerAccount account(id);
    account.Select(rdb_);
    account.status = BLOCKED;
    account.Update(rdb_);
    AddMessage(Message("Account blocked. ").What());
    return Value();
}

template<account_type T>
Value GetAccount(Connection* rdb, const Int& partnerid) {
    PartnerAccount account;
    Selector sel;
    sel << account;
    sel.Where(account->partnerid == partnerid && account->atype == PartnerAccountType<T>::name);
    auto ds = sel.Execute(rdb);
    Value res;
    if(!ds.Fetch()) {
        res[PartnerAccountType<T>::accountexists] = BOOL_FALSE;
        return res;
    }
    res[PartnerAccountType<T>::accountid] = account.id;
    res[PartnerAccountType<T>::accountname] = account.name;
    res[PartnerAccountType<T>::accountatype] = account.atype;
    res[PartnerAccountType<T>::accountstatus] = account.status;
    res[PartnerAccountType<T>::accountamount] = account.amount;
    res[PartnerAccountType<T>::accountexists] = BOOL_TRUE;
    return res;
}

const Value BALOONPartnerAccount::DebitGet(const Int partnerid) {
    const auto res = GetAccount<account_type::DEBIT>(rdb_, partnerid);
    return res;
}

const Value BALOONPartnerAccount::CreditGet(const Int partnerid) {
    const auto res = GetAccount<account_type::CREDIT>(rdb_, partnerid);
    return res;
}

const Value BALOONPartnerAccount::OurDebitGet() {
    const auto current_partnerid = CurrentPartnerID(rdb_);
    const auto res = GetAccount<account_type::DEBIT>(rdb_, current_partnerid);
    return res;
}

const Value BALOONPartnerAccount::OurCreditGet() {
    const auto current_partnerid = CurrentPartnerID(rdb_);
    const auto res = GetAccount<account_type::CREDIT>(rdb_, current_partnerid);
    return res;
}

const Value BALOONPartnerAccount::OurPartnerAccountListGet() {
    const auto current_partnerid = CurrentPartnerID(rdb_);
    auto AccountName = [](const Str& type)->string {
        if(type == DEBIT_ACCOUNT) {
            return string("Дебетовый");
        }
        if(type == CREDIT_ACCOUNT) {
            return string("Кредитный");
        }
        return {};
    };
    PartnerAccount account;
    Selector sel;
    sel << account->id
        << account->name
        << account->atype
        << account->amount;
    sel.From(account);
    sel.Where(account->partnerid == current_partnerid && account->status == ACTIVE);
    auto ds = sel.Execute(rdb_);
    Data::DataList lr;
    Str description;
    lr.AddColumn("id", Data::INTEGER);
    lr.AddColumn("description", Data::STRING);
    lr.Bind(account.id, "id");
    lr.Bind(description, "description");
    while(ds.Fetch()) {
        description = AccountName(account.atype) + " " + ToString(account.amount.Trunc())  + " руб.";
        lr.AddRow();
    }
    return lr;
}

template<account_type T>
Data::DataList GetAccountTransactionList(Connection* rdb, const Int& partnerid) {
    PartnerAccount account;
    AccountTransaction transaction;
    
    Data::DataList lr;
    lr.AddColumn("id", Data::INTEGER);
    lr.AddColumn("DateArc", Data::DATETIME);
    lr.AddColumn("amount", Data::DECIMAL);
    lr.AddColumn("cache_flow", Data::STRING);
    lr.AddColumn("reason", Data::STRING);
    
    
    lr.Bind(transaction.id, "id");
    lr.Bind(transaction.DateArc, "DateArc");
    lr.Bind(transaction.amount, "amount");
    lr.Bind(transaction.cache_flow, "cache_flow");
    lr.Bind(transaction.reason, "reason");
    
    Selector sel;
    sel << account;
    sel.Where(account->partnerid == partnerid && account->atype == PartnerAccountType<T>::name);
    auto ds = sel.Execute(rdb);
    if(!ds.Fetch()) {
        return lr;
    }
    
    Selector sel2;
    sel2 << transaction;
    sel2.Where(transaction->accountid == account.id);
    auto ds2 = sel2.Execute(rdb);
    while(ds2.Fetch()) {
        lr.AddRow();
    }
    return lr;
}

const Value BALOONAccountTransaction::OurDebitAccountTransactionListGet() {
    const auto partnerid = CurrentPartnerID(rdb_);
    return GetAccountTransactionList<account_type::DEBIT>(rdb_, partnerid);
}

const Value BALOONAccountTransaction::OurCreditAccountTransactionListGet() {
    const auto partnerid = CurrentPartnerID(rdb_);
    return GetAccountTransactionList<account_type::CREDIT>(rdb_, partnerid);
}

const Value BALOONAccountTransaction::DebitAccountTransactionListGet(const Int partnerid) {
    if(!Defined(partnerid)) {
        throw Exception(Message("Partner is not defined. ").What());
    }
    return GetAccountTransactionList<account_type::DEBIT>(rdb_, partnerid);
}

const Value BALOONAccountTransaction::CreditAccountTransactionListGet(const Int partnerid) {
    if(!Defined(partnerid)) {
        throw Exception(Message("Partner is not defined. ").What());
    }
    return GetAccountTransactionList<account_type::CREDIT>(rdb_, partnerid);
}

const Value BALOONPartnerOrder::Get(const Int id) {
    Value res;
    PartnerOrder order(id);
    order.Select(rdb_);
    res["id"] = order.id;
    res["partnerid"] = order.partnerid;
    res["initial_cost"] = order.initial_cost;
    res["discount"] = order.discount;
    res["total_cost"] = order.total_cost;
    res["week_tariffid"] = order.week_tariffid;
    res["DateArc"] = order.DateArc;
    return res;
}

const Value BALOONPartnerOrder::OurPartnerOrderListGet() {
    const auto partnerid = CurrentPartnerID(rdb_);
    WeekTariff week_tariff;
    WeekTariffGroup agroup;
    PartnerOrder order;
    PartnerOrderline oline;
    int tickets_count(0);
    Selector sel;
    sel << order->id
        << order->DateArc
        << week_tariff->external_name
        << agroup->is_default
        << order->initial_cost
        << order->discount
        << order->total_cost
        << oline->orderid.Count().Bind(tickets_count);
    sel.From(order)
        .Join(week_tariff).On(order->week_tariffid == week_tariff->id)
        .Join(agroup).On(week_tariff->agroupid == agroup->id)
        .Join(oline).On(oline->orderid == order->id);
    sel.Where(order->partnerid == partnerid);
    sel.GroupBy(order->id);
    sel.GroupBy(order->DateArc);
    sel.GroupBy(week_tariff->external_name);
    sel.GroupBy(agroup->is_default);
    sel.GroupBy(order->initial_cost);
    sel.GroupBy(order->discount);
    sel.GroupBy(order->total_cost);
    auto ds = sel.Execute(rdb_);
    
    Data::DataList lr;
    lr.AddColumn("id", Data::INTEGER);
    lr.AddColumn("DateArc", Data::DATETIME);
    lr.AddColumn("week_tariffexternal_name", Data::STRING);
    lr.AddColumn("is_public", Data::STRING);
    lr.AddColumn("initial_cost", Data::DECIMAL);
    lr.AddColumn("discount", Data::DECIMAL);
    lr.AddColumn("total_cost", Data::DECIMAL);
    lr.AddColumn("tickets_count", Data::INTEGER);
    
    lr.Bind(order.id, "id");
    lr.Bind(order.DateArc, "DateArc");
    lr.Bind(week_tariff.external_name, "week_tariffexternal_name");
    lr.Bind(agroup.is_default, "is_public");
    lr.Bind(order.initial_cost, "initial_cost");
    lr.Bind(order.discount, "discount");
    lr.Bind(order.total_cost, "total_cost");
    lr.Bind(tickets_count, "tickets_count");
    
    while(ds.Fetch()) {
        lr.AddRow();
    }
    return lr;
}

inline void InsertIdentifier(Connection* rdb, const IdentifierDuration& dur, const WeekTariff& tariff, const Int& orderid, const Str& code) {
    Identifier identifier;
    identifier.code = code;
    identifier.valid_from = dur.from;
    identifier.valid_to = dur.to;
    identifier.permanent_rulename = tariff.identifier_rulename;
    identifier.categoryname = tariff.identifier_categoryname;
    identifier.ratename = tariff.ratename;
    identifier.name = tariff.external_name;
    identifier.status = IDENTIFIER_STATUS_WAITING;
    const auto identifierid = identifier.Insert(rdb);
    
    PartnerOrderline oline;
    oline.identifierid = identifierid;
    oline.orderid = orderid;
    oline.price = tariff.price;
    oline.service_rulename = tariff.service_rulename;
    oline.Insert(rdb);
}

void SavePartnerOrderlines(Connection* rdb, const ADate& adate, const WeekTariff& tariff, const Int& orderid, int count) {
    const auto dur = GetIdentifierDuration(tariff, adate);
    for(int i(0); i < count; ++i) {
        const auto code = GenerateCode(rdb);
        InsertIdentifier(rdb, dur, tariff, orderid, code);
    }
}

void SavePartnerOrderlines(Connection* rdb, const ADate& adate, const WeekTariff& tariff, const Int& orderid, const Value& codes) {
    const auto size = codes.Size();
    const auto dur = GetIdentifierDuration(tariff, adate);
    for(uint i(0); i < size; ++i) {
        const auto& code = codes[i].As<string>();
        InsertIdentifier(rdb, dur, tariff, orderid, code);
    }
}

bool WeekTariffIsPublic(Connection* rdb, const Int& week_tariffid) {
    WeekTariff wtariff;
    WeekTariffGroup wtgroup;
    Selector sel;
    sel << wtgroup->is_default;
    sel.From(wtgroup).Join(wtariff).On(wtariff->agroupid == wtgroup->id);
    sel.Where(wtariff->id == week_tariffid);
    auto ds = sel.Execute(rdb);
    if(ds.Fetch()) {
        return (wtgroup.is_default == BOOL_TRUE);
    }
    return false;
}

Decimal GetPartnerDiscount(Connection* rdb, const Int& partnerid) {
    const auto today = Today();
    PartnerContract contract;
    Selector sel;
    sel << contract->benefit_amount;
    sel.Where(contract->start_date <= today && contract->end_date >= today && contract->partnerid == partnerid);
    sel.Limit(1);
    auto ds = sel.Execute(rdb);
    if(!ds.Fetch()) {
        throw Exception(Message("Contract not found. "));
    }
    return contract.benefit_amount;
}

const Value BALOONPartnerOrder::OurCreate(const ADate adate, const Int week_tariffid, const Int identifiers_count, const Int accountid, const Str comment) {
    if(!Defined(identifiers_count) || !(*identifiers_count)) {
        throw Exception(Message("Wrong identifiers count. ").What());
    }
    const auto partner = CurrentPartner(rdb_);
    const auto partnerid = partner.id;
    const auto discount = GetPartnerDiscount(rdb_, partnerid);
    const auto week_tariff_is_public = WeekTariffIsPublic(rdb_, week_tariffid);
    
    const auto wtariffs = week_tariff_is_public? GetPublicSeansesByDate(rdb_, adate) : GetPartnerSeansesByDate(rdb_, partnerid, adate);
    auto iter = wtariffs.find(week_tariffid);
    if(iter == wtariffs.cend()) {
        throw Exception(Message("Wrong week tariff. ").What());
    }
    const WeekTariff& week_tariff = iter->second;
    const auto total = week_tariff.price * (*identifiers_count);
    PartnerOrder order;
    order.partnerid = partnerid;
    order.week_tariffid = week_tariffid;
    order.initial_cost = total;
    if(week_tariff_is_public) {
        order.discount = discount;
        order.total_cost = total - (total*discount / ONE_HUNDRED_PERCENT);
    }
    else {
        order.discount = 0.0;
        order.total_cost = total;
    }    
    const auto orderid = order.Insert(rdb_);
    order.id = orderid;
    
    SavePartnerOrderlines(rdb_, adate, week_tariff, orderid, *identifiers_count);
    DoAccountTransaction<transaction_type::EXPENSE>(rdb_, accountid, total, comment);
    
    if(Defined(partner.notification_email)) {
        const string templatename("partner_order_notification");
        DeliverNotification(partner.notification_email, templatename, order);
    }    
    Value res;
    res["id"] = orderid;
    AddMessage("Order created. ");
    return res;
}

PartnerOrder CheckAndGetOrder(Connection* rdb, const Int& orderid) {
    const auto partnerid = CurrentPartnerID(rdb);
    PartnerOrder order(orderid);
    order.Select(rdb_);
    if(order.partnerid != partnerid) {
        throw Exception(Message("Forbidden. ").What());
    }
    return order;
}

const char* partner_templatename = "partner_ticket_template";

const Value BALOONPartnerOrder::OurTicketsDownload(const Int id) {
    const auto order = CheckAndGetOrder(rdb_, id);
    const auto res = MakeTicketsBlobFile(rdb_, id, partner_templatename);
    return res;
}

const Value BALOONPartnerOrder::TicketsDownload(const Int id) {
    PartnerOrder order(id);
    order.Select(rdb_);
    const auto res = MakeTicketsBlobFile(rdb_, id, partner_templatename);
    return res;
}

const Value BALOONPartnerOrder::Place_API(const ADate adate, const Value identifiers, const Int week_tariffid, const Str token) {
    if(!identifiers.IsArray() || !identifiers.Size()) {
        throw Exception(Message("No identifiers found in request. ").What());
    }
    WeekTariff wtariff(week_tariffid);
    if(!wtariff.Select(rdb_)) {
        throw Exception(Message("Wrong week tariff. ").What());
    }
    if(wtariff.enabled != BOOL_TRUE) {
        throw Exception(Message("Selected week tariff is not active. ").What());
    }
    const auto partnerid = PartnerByToken(rdb_, token);
    PartnerOrder porder;
    porder.partnerid = partnerid;
    porder.week_tariffid = week_tariffid;
    porder.initial_cost = 0.0;
    porder.total_cost = 0.0;
    porder.discount = 0.0;
    const auto orderid = porder.Insert(rdb_);
    
    SavePartnerOrderlines(rdb_, adate, wtariff, orderid, identifiers);
    Value res;
    res["id"] = orderid;
    return res;
}

const Value BALOONPartnerOrder::DetailsGet(const Int id) {
    PartnerOrder order(id);
    order.Select(rdb_);
    Partner partner(order.partnerid);
    partner.Select(rdb_);
    
    int count(0);
    PartnerOrderline oline;
    Selector sel;
    sel << oline->identifierid.Count().Bind(count);
    sel.Where(oline->orderid == id);
    auto ds = sel.Execute(rdb_);
    ds.Fetch();
    
    Value res;
    res["id"] = order.id;
    res["total_cost"] = order.total_cost;
    res["partnername"] = partner.name;
    res["count"] = count;
    
    return res;
}

Data::DataList GetPartnerOrderlines(Connection* rdb, const Int& orderid) {
    PartnerOrderline oline;
    Identifier identifier;
    Selector sel;
    sel << identifier->id
        << identifier->code
        << identifier->valid_from
        << identifier->valid_to
        << identifier->permanent_rulename
        << identifier->categoryname
        << identifier->ratename
        << identifier->status
        << oline->price;
    sel.From(identifier).Join(oline).On(oline->identifierid == identifier->id);
    sel.Where(oline->orderid == orderid);
    auto ds = sel.Execute(rdb);
    Data::DataList lr;
    lr.AddColumn("identifierid", Data::INTEGER);
    lr.AddColumn("identifiercode", Data::STRING);
    lr.AddColumn("price", Data::DECIMAL);
    lr.AddColumn("identifiervalid_from", Data::DATETIME);
    lr.AddColumn("identifiervalid_to", Data::DATETIME);
    lr.AddColumn("identifierpermanent_rulename", Data::STRING);
    lr.AddColumn("identifiercategoryname", Data::STRING);
    lr.AddColumn("identifierratename", Data::STRING);
    lr.AddColumn("identifierstatus", Data::STRING);
    
    lr.Bind(identifier.id, "identifierid");
    lr.Bind(identifier.code, "identifiercode");
    lr.Bind(oline.price, "price");
    lr.Bind(identifier.valid_from, "identifiervalid_from");
    lr.Bind(identifier.valid_to, "identifiervalid_to");
    lr.Bind(identifier.permanent_rulename, "identifierpermanent_rulename");
    lr.Bind(identifier.categoryname, "identifiercategoryname");
    lr.Bind(identifier.ratename, "identifierratename");
    lr.Bind(identifier.status, "identifierstatus");
    while(ds.Fetch()) {
        lr.AddRow();
    }
    return lr;
}

const Value BALOONPartnerOrderline::OurTicketListGet(const Int orderid) {
    CheckAndGetOrder(rdb_, orderid);
    return GetPartnerOrderlines(rdb_, orderid);
}

const Value BALOONPartnerOrderline::OurPartnerOrderlineListGet(const Int orderid) {
    const auto order = CheckAndGetOrder(rdb_, orderid);
    const auto lr = GetPartnerOrderlines(rdb_, orderid);
    
    Value res;
    res["id"] = order.id;
    res["total_cost"] = order.total_cost;
    res["DateArc"] = order.DateArc;
    res["count"] = lr.Rows();
    res["OurPartnerOrderlineList"] = lr;
    return res;
}

void UpdateIdentifierStatus(Connection* rdb, const set<Int>& identifier_ids, const string& status, const Str& comment) {
    for(const auto& id : identifier_ids) {
        Identifier identifier;
        Updater upd(identifier);
        upd << identifier->status(status)
            << identifier->comment(comment);
        upd.Where(identifier->id == id);
        upd.Execute(rdb);
    }
}

void FullfillClientForRequest(SpdClient& client, const posix_time::time_duration& offset, const PartnerOrderline& oline, const Identifier& identifier) {
    SpdIdentifier spd_identifier;
    spd_identifier.is_new = true;
    spd_identifier.eid = to_upper(Effi::GenerateGUID());
    spd_identifier.code = identifier.code.get_value_or(string());
    spd_identifier.valid_from = identifier.valid_from + offset;
    spd_identifier.valid_to = identifier.valid_to + offset;
    spd_identifier.permanent_rule = identifier.permanent_rulename.get_value_or(string());
    spd_identifier.category = identifier.categoryname.get_value_or(string());
    spd_identifier.tariff = identifier.ratename.get_value_or(string());
    
    if(!Defined(oline.service_rulename)) {
        const string comment("Пополнение счёта");
        SpdTransaction transaction;
        transaction.currency = RUB_CURRENCY;
        transaction.comment = comment;
        transaction.eid = to_upper(Effi::GenerateGUID());
        transaction.summ = oline.price;
        client.transactions.push_back(transaction);
    }
    else {
        SpdPackage package;
        package.identifier = spd_identifier.code;
        package.rule_service = *oline.service_rulename;
        package.eid = to_upper(Effi::GenerateGUID());
        client.packages.push_back(package);
    }
    client.identifiers.push_back(spd_identifier);
}

const Value BALOONPartnerOrderline::AsyncRegisterInContour() {
    const auto gate = GetDefaultGate(rdb_);
    constexpr bool NEW(true);
    PartnerOrderline oline;
    Identifier identifier;
    Selector sel;
    sel << identifier
        << oline->price
        << oline->service_rulename;
        
    sel.From(identifier).Join(oline).On(oline->identifierid == identifier->id);
    sel.Where(identifier->status == IDENTIFIER_STATUS_WAITING);
    auto ds = sel.Execute(rdb_);
    SpdClient client(NEW);
    client.spd_host = gate.host;
    client.spd_port = gate.port;
    const auto offset = boost::posix_time::hours(0); // Computerica::get_utc_offset();
    set<Int> identifier_ids;
    while(ds.Fetch()) {
        FullfillClientForRequest(client, offset, oline, identifier);
        identifier_ids.insert(identifier.id);
    }
    if(client.identifiers.empty()) {
        return Value();
    }
    try {
        const auto response = SendSpdRequest(client);
        UpdateIdentifierStatus(rdb_, identifier_ids, IDENTIFIER_STATUS_OK, {});
    }
    catch(const Exception& e) {
        Log(5) << "---ERROR: " << e.What() << std::endl;
        UpdateIdentifierStatus(rdb_, identifier_ids, IDENTIFIER_STATUS_CANCELED, e.What());
    }
    catch(...) {
        const auto comment = Str("Ошибка выдачи идентификатора");
        UpdateIdentifierStatus(rdb_, identifier_ids, IDENTIFIER_STATUS_CANCELED, comment);
    }
    return Value();
}

ADate Today() {
    return (TIME_NOW /*+ Computerica::get_utc_offset()*/ ).date();
}

const Value BALOONAuxiliary::CurrentDate() {
    Value res;
    res["adate"] = Today();
    return res;
}
//------------------------------------------------------------------------------
/// Запись в лог от внешних источников (приложений)
const Value BALOONAuxiliary::WriteToLog(const Str initiator, const Str logmessage)
{
    const Str allow_log = MainConfig->Get("Environment/Set/ALLOW_LOG_EXT_APP").get_value_or(CFG_ALLOW_LOG_EXT_APP);

    if ((allow_log==Str(CONFIG_TRUE_CONST)) || (allow_log==Str(CONFIG_YES_CONST)))
        Log(3)<<__func__<<":"<<initiator<<":"<<logmessage;
        
    return Value();
}
//------------------------------------------------------------------------------
/// Проверка, разрешениа ли отладка внешних приложений
const Value BALOONAuxiliary::IsDebugEnabled()
{
    Value res;
    res["debug"]=MainConfig->Get("Environment/Set/ALLOW_LOG_EXT_APP").get_value_or(CFG_ALLOW_LOG_EXT_APP);
    return res;
}
//------------------------------------------------------------------------------
} // namespace BALOON
