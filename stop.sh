#!/bin/bash
# Baloon stop script
function stop_module() {
	if [ -f $1.pid ] && ps `cat $1.pid` | grep $1 &> /dev/null ; then
		echo "Stopping $1..."
		kill -9 `cat $1.pid`
		rm -f $1.pid
	else
		echo "Module '$1' is not started."
	fi
}

EFFI_ROOT="/usr/local/effi"
EFFI_BIN="$EFFI_ROOT/bin"
PROJECT_ROOT="`dirname $0`"
PROJECT_NAME="Baloon"

cd "$PROJECT_ROOT"
stop_module sws
stop_module dlloader
stop_module arbd
