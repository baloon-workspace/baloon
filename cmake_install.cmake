# Install script for directory: /home/vvzababurin/application/src/baloon

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/usr/local/baloon")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "1")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/wnd/Baloon" TYPE DIRECTORY FILES "/home/vvzababurin/application/src/baloon/result/")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/doc/types" TYPE FILE OPTIONAL FILES
    "/home/vvzababurin/application/src/baloon/BaloonautoTYPE.xml"
    "/usr/local/effi/doc/types/BASETYPE.xml"
    "/home/vvzababurin/application/src/baloon/BALOONTYPE.xml"
    "/usr/local/effi/include/Services/Authorizer/AuthorizerautoTYPE.xml"
    "/usr/local/effi/include/Services/Authorizer/../BASETYPE.xml"
    "/usr/local/effi/include/Services/Authorizer/AUTHORIZERTYPE.xml"
    "/usr/local/computerica/include/Paygate/PaygateautoTYPE.xml"
    "/usr/local/effi/doc/types/BASETYPE.xml"
    "/usr/local/computerica/include/Paygate/PAYGATETYPE.xml"
    "/usr/local/computerica/include/Basicsite/BasicsiteautoTYPE.xml"
    "/usr/local/effi/doc/types/BASETYPE.xml"
    "/usr/local/computerica/include/Basicsite/BASICSITETYPE.xml"
    "/usr/local/effi/include/Services/Multimedia/MULTIMEDIATYPE.xml"
    "/usr/local/computerica/include/Fiscal/FiscalautoTYPE.xml"
    "/usr/local/effi/doc/types/BASETYPE.xml"
    "/usr/local/computerica/include/Fiscal/FISCALTYPE.xml"
    "/usr/local/effi/include/Services/Multimedia/MultimediaautoTYPE.xml"
    "/usr/local/effi/include/Services/Multimedia/../BASETYPE.xml"
    "/usr/local/effi/include/Services/Multimedia/MULTIMEDIATYPE.xml"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  IF(EXISTS "$ENV{DESTDIR}/usr/local/baloon/libBaloon.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/baloon/libBaloon.so")
    FILE(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/baloon/libBaloon.so"
         RPATH "/usr/local/effi/lib")
  ENDIF()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/baloon/libBaloon.so")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/usr/local/baloon" TYPE SHARED_LIBRARY FILES "/home/vvzababurin/application/src/baloon/libBaloon.so")
  IF(EXISTS "$ENV{DESTDIR}/usr/local/baloon/libBaloon.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/baloon/libBaloon.so")
    FILE(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/usr/local/baloon/libBaloon.so"
         OLD_RPATH "/usr/local/effi/lib:/usr/local/reporter/lib:/usr/local/computerica/lib:"
         NEW_RPATH "/usr/local/effi/lib")
    IF(CMAKE_INSTALL_DO_STRIP)
      EXECUTE_PROCESS(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/baloon/libBaloon.so")
    ENDIF(CMAKE_INSTALL_DO_STRIP)
  ENDIF()
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/baloon/doc/css;/usr/local/baloon/doc/flash;/usr/local/baloon/doc/images;/usr/local/baloon/doc/js;/usr/local/baloon/doc/skins;/usr/local/baloon/doc/xml;/usr/local/baloon/doc/xslt;/usr/local/baloon/doc/computerica;/usr/local/baloon/doc/thirdparty;/usr/local/baloon/doc/public;/usr/local/baloon/doc/custom;/usr/local/baloon/doc/assets")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/usr/local/baloon/doc" TYPE DIRECTORY FILES
    "/home/vvzababurin/application/src/baloon/doc/css"
    "/home/vvzababurin/application/src/baloon/doc/flash"
    "/home/vvzababurin/application/src/baloon/doc/images"
    "/home/vvzababurin/application/src/baloon/doc/js"
    "/home/vvzababurin/application/src/baloon/doc/skins"
    "/home/vvzababurin/application/src/baloon/doc/xml"
    "/home/vvzababurin/application/src/baloon/doc/xslt"
    "/home/vvzababurin/application/src/baloon/doc/computerica"
    "/home/vvzababurin/application/src/baloon/doc/thirdparty"
    "/home/vvzababurin/application/src/baloon/doc/public"
    "/home/vvzababurin/application/src/baloon/doc/custom"
    "/home/vvzababurin/application/src/baloon/doc/assets"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/baloon/doc/types")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/usr/local/baloon/doc" TYPE DIRECTORY FILES "/home/vvzababurin/application/src/baloon/doc/types" REGEX "BALOONTYPE.xml|BaloonautoTYPE.xml" EXCLUDE)
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/baloon/doc/types/BALOONTYPE.xml;/usr/local/baloon/doc/types/BaloonautoTYPE.xml")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/usr/local/baloon/doc/types" TYPE FILE FILES
    "/home/vvzababurin/application/src/baloon/BALOONTYPE.xml"
    "/home/vvzababurin/application/src/baloon/BaloonautoTYPE.xml"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/baloon/doc/index.html")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/usr/local/baloon/doc" TYPE FILE FILES "/home/vvzababurin/application/src/baloon/doc/index.html")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/baloon/dbpatches;/usr/local/baloon/utils")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/usr/local/baloon" TYPE DIRECTORY FILES
    "/home/vvzababurin/application/src/baloon/dbpatches"
    "/home/vvzababurin/application/src/baloon/utils"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/baloon/version;/usr/local/baloon/CONFIG.example;/usr/local/baloon/sws.conf;/usr/local/baloon/locale.en_US;/usr/local/baloon/locale.ru_RU;/usr/local/baloon/locale.ru_RU.Multimedia;/usr/local/baloon/locale.ru_RU.Baloon;/usr/local/baloon/locale.ru_RU.Reporter;/usr/local/baloon/locale.en_US.Reporter;/usr/local/baloon/locale.ru_RU.Mailbox;/usr/local/baloon/locale.ru_RU.Paygate;/usr/local/baloon/reporter-version.Baloon-01.00;/usr/local/baloon/effi-version.Baloon-01.00;/usr/local/baloon/computerica-version.Baloon-01.00")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/usr/local/baloon" TYPE FILE FILES
    "/home/vvzababurin/application/src/baloon/version"
    "/home/vvzababurin/application/src/baloon/etc/CONFIG.example"
    "/home/vvzababurin/application/src/baloon/sws.conf"
    "/home/vvzababurin/application/src/baloon/locale.en_US"
    "/home/vvzababurin/application/src/baloon/locale.ru_RU"
    "/home/vvzababurin/application/src/baloon/locale.ru_RU.Multimedia"
    "/home/vvzababurin/application/src/baloon/locale.ru_RU.Baloon"
    "/home/vvzababurin/application/src/baloon/locale.ru_RU.Reporter"
    "/home/vvzababurin/application/src/baloon/locale.en_US.Reporter"
    "/home/vvzababurin/application/src/baloon/locale.ru_RU.Mailbox"
    "/home/vvzababurin/application/src/baloon/locale.ru_RU.Paygate"
    "/home/vvzababurin/application/src/baloon/reporter-version.Baloon-01.00"
    "/home/vvzababurin/application/src/baloon/effi-version.Baloon-01.00"
    "/home/vvzababurin/application/src/baloon/computerica-version.Baloon-01.00"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  execute_process(COMMAND ln -sf /usr/local/effi/wnd/BASE $ENV{DESTDIR}/usr/local/baloon/wnd/BASE)
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  execute_process(COMMAND ln -sf /usr/local/effi/wnd/Authorizer $ENV{DESTDIR}/usr/local/baloon/wnd/Authorizer)
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  execute_process(COMMAND ln -sf /usr/local/effi/wnd/TaskMan $ENV{DESTDIR}/usr/local/baloon/wnd/TaskMan)
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  execute_process(COMMAND ln -sf /usr/local/effi/wnd/Multimedia $ENV{DESTDIR}/usr/local/baloon/wnd/Multimedia)
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  execute_process(COMMAND ln -sf /usr/local/reporter/wnd/Reporter $ENV{DESTDIR}/usr/local/baloon/wnd/Reporter)
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  execute_process(COMMAND ln -sf /usr/local/computerica/wnd/Paygate $ENV{DESTDIR}/usr/local/baloon/wnd/Paygate)
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  execute_process(COMMAND ln -sf /usr/local/computerica/wnd/Uniteller $ENV{DESTDIR}/usr/local/baloon/wnd/Uniteller)
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  execute_process(COMMAND ln -sf /usr/local/computerica/wnd/Sberbank $ENV{DESTDIR}/usr/local/baloon/wnd/Sberbank)
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  execute_process(COMMAND ln -sf /usr/local/computerica/wnd/Fiscal $ENV{DESTDIR}/usr/local/baloon/wnd/Fiscal)
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  execute_process(COMMAND ln -sf /usr/local/computerica/wnd/AtolOnline $ENV{DESTDIR}/usr/local/baloon/wnd/AtolOnline)
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/baloon/patch.sh;/usr/local/baloon/start.sh;/usr/local/baloon/stop.sh;/usr/local/baloon/process.sh;/usr/local/baloon/process-dblog")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/usr/local/baloon" TYPE PROGRAM FILES
    "/home/vvzababurin/application/src/baloon/patch.sh"
    "/home/vvzababurin/application/src/baloon/start.sh"
    "/home/vvzababurin/application/src/baloon/stop.sh"
    "/home/vvzababurin/application/src/baloon/process.sh"
    "/home/vvzababurin/application/src/baloon/process-dblog"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/etc/ld.so.conf.d/baloon.conf")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/etc/ld.so.conf.d" TYPE FILE RENAME "baloon.conf" FILES "/home/vvzababurin/application/src/baloon/etc/ld.so.conf.example")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/etc/logrotate.d/baloon")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/etc/logrotate.d" TYPE FILE RENAME "baloon" FILES "/home/vvzababurin/application/src/baloon/etc/logrotate.example")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(MAKE_DIRECTORY $ENV{DESTDIR}/usr/local/baloon/calls)
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(CMAKE_INSTALL_COMPONENT)
  SET(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
ELSE(CMAKE_INSTALL_COMPONENT)
  SET(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
ENDIF(CMAKE_INSTALL_COMPONENT)

FILE(WRITE "/home/vvzababurin/application/src/baloon/${CMAKE_INSTALL_MANIFEST}" "")
FOREACH(file ${CMAKE_INSTALL_MANIFEST_FILES})
  FILE(APPEND "/home/vvzababurin/application/src/baloon/${CMAKE_INSTALL_MANIFEST}" "${file}\n")
ENDFOREACH(file)
