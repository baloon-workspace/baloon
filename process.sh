#!/bin/sh

SendWNS()
{
	echo "windows device:" >> process.log
	echo $1 $2 >> process.log
}

SendAPN()
{
    message='{ "aps" : { "alert" : { "title" : "'$2'", "body" : "'$3'" }, "badge" : 1, "sound" : "default" } }'

    epoch="$(( `date +%s` + 7200 ))"
    retbody=`curl -s -d "$message" -H "apns-topic: com.computerica.hillpark" -H "apns-push-type: alert" -H "apns-expiration: ${epoch}" -H "apns-priority: 5" --http2 --cert-type p12 --cert credentials.aps:test "https://api.push.apple.com:443/3/device/${1}"`

    if [ -z "$retbody" ]; then
	sendresult="success"
    else
	sendresult="failed : ${retbody}"
    fi

    ./process-dblog "${4}" "${5}" "${sendresult}"
}

SendFCM()
{
    key=`cat credentials.fcm`
    message='{ "to" : "'$1'", "priority" : "normal", "notification" : { "title" : "'$2'", "text": "'$3'", "badge" : 1, "sound": "default" } }'

    retbody=`curl -s -d "$message" -H "content-type: application/json" -H "authorization: key=${key}" "https://fcm.googleapis.com/fcm/send"`
    if [ -z "$retbody" ]; then
	sendresult="success"
    else
	sendresult="failed : ${retbody}"
    fi

    ./process-dblog "${4}" "${5}" "${sendresult}"
}

if [ -n "$1" ]; then
    if [ -n "$2" ]; then
	title=`cat $2 | base64 -d`
	if [ -n "$3" ]; then
	    message=`cat $3 | base64 -d`
	    while read -r devicetoken deviceid messageid os; do
		if [ "$os" = "apple" ]; then
			if [ -f "credentials.aps" ]; then
				SendAPN "$devicetoken" "$title" "$message" "$deviceid" "$messageid"
			fi
		elif [ "$os" = "android" ]; then
			if [ -f "credentials.fcm" ]; then
				SendFCM "$devicetoken" "$title" "$message" "$deviceid" "$messageid"
			fi
		elif [ "$os" = "windows" ]; then
			if [ -f "credentials.wns" ]; then
				SendWNS "$devicetoken" "$title" "$message" "$deviceid" "$messageid"
			fi
		fi
	    done < $1
	    rm ./$1
	    rm ./$2
	    rm ./$3
	fi
    fi
fi

exit 0

