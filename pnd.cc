/**
 * ASoft DataBase conection library Test Suite
 * @file baloon/pnd.cc
 */

#ifdef _MSC_VER
    # include <winsock2.h>
    # include <math.h>
    inline double roundf(double x) { return floor(x + 0.5); }
#else
    # include <sys/wait.h>
#endif

#include <boost/optional/optional_io.hpp>

#include <signal.h>
#include <fcntl.h>

#include "commonfuncs.h"

#include "acommon/alogger.h"
#include "acommon/aexception.h"
#include "alink/alink.h"

#include "ardbms/convertor.h"
#include "ardbms/ardbms.h"
#include "ardbms/domain.h"

#include "unicode/ucnv.h"
#include "unicode/utypes.h"

using namespace std;
using namespace boost;
using namespace Effi;
using namespace boost::posix_time;


#define TIME_NOW Effi::Time(boost::posix_time::microsec_clock::local_time())
#define TODAY_LOCAL Effi::ADate(boost::gregorian::day_clock::local_day())

#define LoggingComponent "process-dblog"

class GlobalContext : public ContextIF {
	public:
		const std::type_info& Type() const { return typeid(*this); }
		const std::string &Lang() const { return lang_;}
		int64_t TenantID() const { return tenantID_; }
		
		void SetTenantID(int64_t tenantID) { tenantID_ = tenantID;}
		void SetGUserID(int64_t gUserID) {}
		void SetGUserTime(const Time& gUserTime) {}
		void SetLang(const std::string &lang) {lang_ = lang;}
	private:
		int64_t tenantID_;
		string lang_;
};

static GlobalContext globalContext = GlobalContext();

ContextIF* GetGlobalContext() {
	return &globalContext;
};

class PushNotificationDeliveries : public Domain {
public :
	struct PushNotificationDeliveriesDomain {
		Column id;
		Column messageid;
		Column deviceid;
		Column sendresult;
		Column delivery_date;
		PushNotificationDeliveriesDomain( const SimpleTable& table )
			: id(table, "id")
			, messageid(table, "messageid")
			, deviceid(table, "deviceid")
			, sendresult(table, "sendresult")
			, delivery_date(table, "delivery_date") {}
	} Domain_;
	boost::optional<int> id;
	boost::optional<int> messageid;
	boost::optional<int> deviceid;
	boost::optional<string> sendresult;
	Time delivery_date;
	PushNotificationDeliveries() : Domain("PushNotificationDeliveries"), Domain_(*this) {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.messageid.Bind(messageid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.deviceid.Bind(deviceid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.sendresult.Bind(sendresult), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.delivery_date.Bind(delivery_date), COLUMN_ORDINARY));
	}
	PushNotificationDeliveries(const PushNotificationDeliveries& copy) : Domain("PushNotificationDeliveries"), Domain_(*this)
		, id(copy.id)
		, messageid(copy.messageid)
		, deviceid(copy.deviceid)
		, sendresult(copy.sendresult)
		, delivery_date(copy.delivery_date) {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.messageid.Bind(messageid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.deviceid.Bind(deviceid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.sendresult.Bind(sendresult), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.delivery_date.Bind(delivery_date), COLUMN_ORDINARY));
	}
	PushNotificationDeliveries& operator= (const PushNotificationDeliveries& copy) {
		id=copy.id;
		messageid=copy.messageid;
		deviceid=copy.deviceid;
		sendresult=copy.sendresult;
		delivery_date=copy.delivery_date;
		return *this;
	}
 	bool operator==(const PushNotificationDeliveries& other) const {
 		return id==other.id;
 	}
 	bool operator<(const PushNotificationDeliveries& other) const {
 		return id<other.id;
 	}
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}

	virtual ~PushNotificationDeliveries() {}

	PushNotificationDeliveriesDomain* operator->() { return &Domain_; }
};


int main( int argc, char* argv[] ) 
{
/*
    Time pt;

    std::string tttt("100000");

    std::string hh = tttt.substr(0,2);
    std::string mm = tttt.substr(2,4);
    std::string ss = tttt.substr(4,6);

    int hhhh = strtol( hh.c_str(), 0, 10 );
    int mmmm = strtol( mm.c_str(), 0, 10 );
    int ssss = strtol( ss.c_str(), 0, 10 );

    boost::posix_time::time_duration t(hhhh,mmmm,ssss);
    boost::gregorian::date d(2021,1,5);

    pt = Time( d, t );

    std::cout << "time duaration: " << pt << endl;

    return 0;
*/
/*
    boost::posix_time::ptime tt = boost::posix_time::microsec_clock::universal_time();
*/

/*
    boost::posix_time::ptime t1 = boost::posix_time::microsec_clock::local_time();
    boost::posix_time::ptime t2 = boost::posix_time::microsec_clock::universal_time();

    std::ostringstream t_os;

    boost::posix_time::time_facet* t_fmt = new boost::posix_time::time_facet();
    t_fmt->format("%d.%m.%Y %H:%M:%S");

    t_os.imbue(std::locale(std::locale::classic(), t_fmt));
    t_os << t1;

    std::cout << t_os.str() << std::endl;

    boost::gregorian::date::ymd_type y = t1.year_month_day();

    std::ostringstream d_os;

    boost::gregorian::date_facet* d_fmt = new boost::gregorian::date_facet("%Y %m %d");

    d_os.imbue(std::locale(d_os.getloc(), d_fmt));
    d_os << t1.date();

    std::cout << d_os.str() << std::endl;

    return 0;
*/
    if( argc != 4 ) {
	Log(0) << "Too few parameters." << endl;
	return 0;
    }

    std::map<std::string, std::string> dbParams;
    std::string dbType = "MySQL";

    std::string libPath = "../effi/lib";
    std::string libName = "libardbms_mysql";

    dbParams["EmulateSequences"]="no";
    dbParams["AutoCommit"]="off";

    if( dbType == "MySQL" ) {
	dbParams["DSN"]="mysql";
    } else if( dbType == "Oracle" ) {
	dbParams["Database"]="baloon";
	dbParams["UserName"]="baloon";
	dbParams["Password"]="1q2w3e";
	dbParams["Port"]="1521";
	dbParams["Host"]="localhost";
	dbParams["SID"]="XE";
	libName="libardbms_oracle";
    } else if( dbType == "MSSQL" ) {
	dbParams["DSN"]="nmssql";
	dbParams["Database"]="baloon";
	dbParams["UserName"]="sa";
	dbParams["Password"]="1q2w3e";
	libName="libardbms_mssql";
    } else if ( dbType== "SQLite" ) {
	dbParams["Database"]="baloon";
	libName="libardbms_sqlite";
    } else {
	cerr << "Please supply one of MySQL, MSSQL, SQLite or Oracle as a testing parameter. MySQL is assumed if no parameter is present. " <<	 endl;
	return 0;
    }

    DataBase* database = new DataBase(libPath + "/" + libName, dbParams);

    Connection::CacheConstructor = new DomainCacheFactory();

    ShPtr<Connection> _rdb = database->GetConnection();

    std::string deviceid = argv[1];
    std::string messageid = argv[2];
    std::string senddevice = argv[3];

    Int _deviceid = strtod(deviceid.c_str(),0);
    Int _messageid = strtod(messageid.c_str(),0);	
    Str _sendresult = senddevice;

    Time _delivery_date = TIME_NOW;

    PushNotificationDeliveries notify_delivery;

    notify_delivery.messageid = _messageid;
    notify_delivery.deviceid = _deviceid;
    notify_delivery.sendresult = _sendresult;
    notify_delivery.delivery_date = _delivery_date;
	
//	auto id = notify_delivery.Insert(_rdb);
    notify_delivery.Insert(_rdb);
    _rdb->Commit();

//	Log(0) << "Database insert record with id=" << id << endl;

    database->ReleaseConnection(_rdb);

    delete database;
    return 0;
}
