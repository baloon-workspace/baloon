import { observable, computed } from 'mobx';
import { EffiProtocol, serializeAURL, format_effi_date } from 'lib/effi_protocol';

export default class TicketsState {
	@observable order = {
		email: "",
		date: new Date(),
		count: 0,
		price: 0,
		tariffName: '',
		day: '',
		id: 0,
		rule: {
			index: 0,
			text: ""
		}
	};
	@observable tariffs = [];
	@observable days = [];
	@observable costs = [];
	effi = new EffiProtocol();

	constructor(opts) {
		opts = opts || {};
		
		this.effi = new EffiProtocol({
			host: opts.host
		});
		this.host = opts.host;
	}

	setup = () => {
		// requestCurrentPerson();
	}

	/*requestServiceRules(callback) {
		this.effi.request({
			url: '/nologin/srv/Baloon/IdentifierServiceRule/IdentifierServiceRuleListGet_API',
			success: (data) => {
				console.log(data);
				this.rules = data || [];
				this.costs = this.parseCosts(data);
				callback(data);
				
			},
			error: (err) => {
				console.log(err);
			}
		});
	}*/
	
	requestServiceTariffs(day,callback) {
		let data = `groupname=s:${day['text']}`;
		this.effi.request({
			data: data,
			url: '/nologin/srv/Baloon/SkipassConfiguration/SkipassConfigurationListGet_API',
			success: (data) => {
				this.tariffs = data || [];
				callback(data);
                console.log(data)
			},
			error: (err) => {
				console.log(err);
			}
		});
    }
    requestServiceDays(callback) {
        this.effi.request({
            url: '/nologin/srv/Baloon/SkipassConfiguration/SkipassConfigurationGroupNameListGet_API',
            success: (data) => {
                this.days = data || [];
                callback(data);

            },
            error: (err) => {
                console.log(err);
            }
        });
    }


	requestPriceByDate(date){
		let data = `adate=ADate:s:${format_effi_date(date)}`;
		this.effi.request({
			url: '/nologin/srv/Baloon/WeekTariff/SeansesByDateListGet_FE',
			data: data,
			success: (data) => {
				console.log(data);
				for(let i =0; i< data.length; i++){
					if(data[i].name === this.order.rule.text){
						this.order.id = data[i].id
                        this.order.price = data[i].price;
					}
				}
			},
			error: (err) => {
				console.log(err);
			}
		});
	}

	requestPlaceAPI(callback) {
		let data = `email=s:${this.order.email}&skipass_configurationid=i:${this.order.id}&qty=i:${this.order.count}`;
		console.log(data);
		this.effi.request({
			url: '/nologin/srv/Baloon/PersonOrder/BuySkipass_API',
			data: data,
			success: (data) => {
				callback(data);
				console.log(data);
				this.requestRaiseInvoice(data.amount, data.id);
			},
			error: (err) => {
				console.log(err);
			}
		});
	}

	requestRaiseInvoice(amount, orderid) {
		let data = `orderid=i:${orderid}&amount=decimal:s:${amount}&&`;
		data += `success_url=s:${encodeURIComponent(this.host + "/tickets-success-download")}&fail_url=s:${this.host + "/tickets-fail"}&`;
		console.log(data);
		this.effi.request({
			url: '/nologin/srv/Baloon/PersonInvoice/RaiseInvoice_FE',
			data: data,
			success: (data) => {
				console.log(data);
				window.location.href = data.action_url;
			},
			error: (err) => {
				console.log(err);
			}
		});
	}

	openCheckout = () => {
		window.location.href = "#/checkout";
	}


	parseCosts = (data) => {
		let costs = [];
		data.map((item, i) => {
			costs.push({
				index: i,
				name: item.name,
				price: item.price,
			})
		})
		return costs;
	}

}