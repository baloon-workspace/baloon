webpackJsonp([4],{

/***/ 382:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _effi_protocol = __webpack_require__(72);

window.effi = new _effi_protocol.EffiProtocol();

/***/ }),

/***/ 72:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.prepareDataList = exports.format_effi_time = exports.format_effi_date = exports.serializeAURLAsync = exports.serializeAURL = exports.EffiProtocol = undefined;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; // import jQuery from 'jquery'


var _base = __webpack_require__(74);

var _base2 = _interopRequireDefault(_base);

var _ajax = __webpack_require__(76);

var _ajax2 = _interopRequireDefault(_ajax);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// import Ajax from 'simple-ajax';

if (typeof String.prototype.startsWith != 'function') {
	String.prototype.startsWith = function (str) {
		return this.indexOf(str) === 0;
	};
}

if (!Array.isArray) {
	Array.isArray = function (arg) {
		return Object.prototype.toString.call(arg) === '[object Array]';
	};
}
function isObject(obj) {
	return Object.prototype.toString.call(obj) === '[object Object]';
}

var parseXml = void 0;

if (typeof window.DOMParser != "undefined") {
	parseXml = function parseXml(xmlStr) {
		return new window.DOMParser().parseFromString(xmlStr, "text/xml");
	};
} else if (typeof window.ActiveXObject != "undefined" && new window.ActiveXObject("Microsoft.XMLDOM")) {
	parseXml = function parseXml(xmlStr) {
		var xmlDoc = new window.ActiveXObject("Microsoft.XMLDOM");
		xmlDoc.async = "false";
		xmlDoc.loadXML(xmlStr);
		return xmlDoc;
	};
} else {
	throw new Error("No XML parser found");
}

function EffiProtocol(opts) {
	opts = opts || {};
	this.login = opts.login || 'barn';
	this.password = opts.password || (this.login == 'barn' ? 'barn' : '');
	this.language = 'ru_RU';
	this.host = opts.host || '';
	this.authenticated = false;
	this.event_subscribers = [];
	this.logout_subscribers = [];
	this.polling = false;

	var context = this;

	function parseStructure(obj) {
		var result = {};
		for (var i = 0; i < obj.children.length; i += 2) {
			var key = obj.children[i].textContent,
			    value = parseValue(obj.children[i + 1]);
			result[key] = value;
		}
		return result;
	}

	function parseArray(obj) {
		var result = [];
		for (var i = 0; i < obj.children.length; i++) {
			result[i] = parseValue(obj.children[i]);
		}
		return result;
	}

	function parseException(obj) {
		var result = {
			ExceptionText: "",
			ErrorCode: undefined
		};
		if (obj.children.length < 1) return result;
		result.ExceptionText = obj.children[0].textContent;
		if (obj.children.length > 1) result.ErrorCode = parseValue(obj.children[1]);
		return result;
	}

	function parseTime(val) {
		var found = val.match(/(\d\d\d\d)(\d\d)(\d\d)T(\d\d)(\d\d)(\d\d)/);
		if (found != null) {
			return new Date(parseInt(found[1]), parseInt(found[2]) - 1, parseInt(found[3]), parseInt(found[4]), parseInt(found[5]), parseInt(found[6]));
		}
		return null;
	}
	function parseDate(val) {
		var found = val.match(/(\d\d\d\d)(\d\d)(\d\d)/);
		if (found != null) {
			return new Date(parseInt(found[1]), parseInt(found[2]) - 1, parseInt(found[3]));
		}
		return null;
	}

	function parseValue(obj) {
		if (obj == null || null == obj.children[0]) return null;
		var container = obj.children[0];
		var nodeName = container.nodeName.toUpperCase();
		if (nodeName == 'STRUCTURE') return parseStructure(container);else if (nodeName == 'ARRAY') return parseArray(container);else if (nodeName == 'UL' || nodeName == 'OPTIONAL' || nodeName == 'U') return parseValue(container);else if (nodeName == 'INT64_T' || nodeName == 'INT') return parseInt(container.textContent);else if (nodeName == 'TIME') return parseTime(container.children[0].textContent);else if (nodeName == 'ADATE') return parseDate(container.children[0].textContent);else if (nodeName == 'DECIMAL') return parseFloat(container.textContent);else if (nodeName == 'DOUBLE') return parseFloat(container.textContent);else if (nodeName == 'EXCEPTION') return parseException(container);else if (nodeName == 'VALUE') return parseValue(container);else return container.textContent;
		return null;
	}

	function parseAXML(data) {
		var apacket_re = /APacket\(\d+ ,"","","","","",\{"ResponseTo":ul\(0 \)\},(.*)\)/;
		var result = data,
		    $xml = parseXml(data);
		if (data.startsWith('APacket')) {
			result = result.replace(apacket_re, '$1');
		}

		// var values = $xml.select('APacket > Value');
		var values = $xml.evaluate('//APacket/Value', $xml, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
		if (!values || values.snapshotLength == 0) {
			if ($xml.children.length < 1) return;
			return parseValue($xml.children[0]);
		}
		var content = values.snapshotItem(values.snapshotLength - 1);
		return parseValue(content);
	}

	function _parse(data, xhr) {
		var ct = xhr.getResponseHeader("content-type") || "";
		var res = data;
		if (ct.indexOf('text/xml') > -1) {
			res = parseAXML(data);
		} else if (ct.indexOf('application/json') > -1) {
			res = JSON.parse(data);
		}
		return res;
	}

	this.auth = function (opts) {
		var login = opts.login || context.login,
		    password = opts.password || context.password,
		    lang = opts.lang || 'ru_RU',
		    skin = opts.skin || 'materio',
		    time_zone = opts.time_zone || new Date().getTimezoneOffset();
		return new Promise(function (resolve, reject) {
			var request = (0, _ajax2.default)({
				url: context.host + '/auth/login',
				method: 'POST',
				dataType: 'text',
				data: 'Login=s:' + login + '&Password=s:' + password + '&Lang=s:' + lang + '&Skin=s:' + skin + '&TimeZone=i:' + time_zone + '&'
			}).then(function (resp) {
				var res = parseAXML(resp);
				context.authenticated = true;
				context.login = login;
				context.password = password;
				if (typeof res == 'string') context.language = res;
				if (typeof opts.success != 'undefined') opts.success(context.language, res);
				resolve(context.language, res);
			}).catch(function (resp) {
				var responseError = parseAXML(resp);
				console.error(responseError.ErrorCode, responseError.ExceptionText);
				if (typeof opts.error != 'undefined') opts.error(responseError);
				reject(responseError);
			});
		});
	};

	this.request = function (opts) {
		return new Promise(function (resolve, reject) {
			var data = opts.data || "dummy=none:&";
			(0, _ajax2.default)().post(context.host + opts.url, data).then(function (resp, xhr) {
				var res = _parse(resp, xhr);
				if (typeof opts.success != 'undefined') opts.success(res);
				resolve(res);
			}).catch(function (resp, xhr) {
				var responseError = parseAXML(resp);
				console.error(responseError.ErrorCode, responseError.ExceptionText);
				if (responseError.ErrorCode == 101 || responseError.ErrorCode == 100) {
					context.auth({
						success: function success() {
							context.request(opts).then(function (res) {
								resolve(res);
							}).catch(function (err) {
								reject(err);
							});
						},
						error: function error() {
							context.fireLogout();
						}
					});
				} else {
					if (typeof opts.error != 'undefined') opts.error(responseError);
					reject(responseError);
				}
			});
		});
	};

	this.onLogout = function (caller) {
		if (typeof caller != 'function') throw "EffiProtocol.onLogout: argument is not a function. ";
		this.logout_subscribers.push(caller);
	};
	this.fireLogout = function () {
		for (var i = 0; i <= this.logout_subscribers.length; i++) {
			var caller = this.logout_subscribers[i];
			caller(this);
		}
	};

	this.pollEvents = function (cnt) {
		cnt = cnt || 0;
		this.polling = true;
		(0, _ajax2.default)(context.host + '/srv/WWW/WWWWorker/GetEvent').get().then(function (data) {
			context.eventsTimeout = setTimeout(function () {
				context.pollEvents(1);
			}, 1);
			var res = parseAXML(data);
			// console.log('event success:', res);
			for (var e = 0; e < res.length; e++) {
				var event = res[e];
				// console.log(context.event_subscribers)
				for (var i = 0; i < context.event_subscribers.length; i++) {
					var subscr = context.event_subscribers[i];
					if (!subscr.type || subscr.type == '*' || subscr.type == event.Type) {
						subscr.callback(event);
					}
				}
			}
		}).catch(function (resp, xhr) {
			var responseError = parseAXML(resp);
			if (!responseError) {
				if (cnt < 4) {
					context.eventsTimeout = setTimeout(function () {
						context.pollEvents(cnt + 1);
					}, 2000);
				} else {
					clearTimeout(context.eventsTimeout);
					console.error("Connection to server lost. ");
				}
				return;
			}
			console.error(cnt, responseError.ErrorCode, responseError.ExceptionText);
			if (cnt < 4 && (responseError.ErrorCode == 101 || responseError.ErrorCode == 100)) {
				context.auth({
					success: function success() {
						console.log('auth done');
						context.pollEvents(1);
					}
				});
			} else {
				clearTimeout(context.eventsTimeout);
				console.error("Connection to server lost. ");
				throw responseError;
			}
		});
	};

	this.subscribe = function (ctx, event_name, callback) {
		if (typeof event_name == 'function') {
			event_name = '*';
			callback = event_name;
		}
		context.event_subscribers.push({ ctx: ctx, type: event_name, callback: callback });
		if (!context.polling) {
			if (typeof SubscribeOnEvent !== 'undefined') {
				// console.log('current window SubscribeOnEvent');
				SubscribeOnEvent(event_name, "document", context.processPrototypeEvent, context);
				context.polling = true;
			} else if (typeof window.opener !== 'undefined' && window.opener && typeof window.opener.SubscribeOnEvent !== 'undefined') {
				// console.log('parent window SubscribeOnEvent');
				window.opener.SubscribeOnEvent(event_name, "document", context.processPrototypeEvent, context);
				context.polling = true;
			} else {
				// console.log('separate window');
				context.eventsTimeout = setTimeout(function () {
					context.pollEvents(1);
				}, 1);
			}
		}
	};
	this.unsubscribe = function (ctx, event_name) {
		// console.log('unsubscribe', ctx, event_name)
		for (var i = 0; i < this.event_subscribers.length; i++) {
			var subscr = this.event_subscribers[i];
			if (subscr.ctx == ctx && (!event_name || event_name == '*' || event_name == subscr.type)) {
				// console.log('  drop subscriber', subscr);
				this.event_subscribers.splice(i, 1);
			}
		}
		// if (UnsubscribeFromEvent) {
		// 	UnsubscribeFromEvent(event_name, "document", this.processPrototypeEvent, this)
		// }
	};

	this.normalizePrototypeObject = function (obj) {
		var res = {};
		for (var k in obj) {
			if (_typeof(obj[k]) == 'object') {
				if ('oValue_' in obj[k]) {
					res[k] = obj[k].oValue_;
					if (_typeof(obj[k].oValue_) == 'object' && 'value_' in obj[k].oValue_) res[k] = obj[k].oValue_.value_;
				}
				// else if ('value_' in obj[k]) res[k] = obj[k].value_
				else res[k] = null;
			} else res[k] = obj[k];
		}
		return res;
	};
	this.processPrototypeEvent = function (e) {
		var event_data = this.normalizePrototypeObject(e.oEventData_),
		    event = { Type: e.sEventName_, Data: event_data };
		for (var i = 0; i < context.event_subscribers.length; i++) {
			var subscr = context.event_subscribers[i];
			if (!subscr.type || subscr.type == '*' || subscr.type.toUpperCase() == e.sEventName_) {
				subscr.callback(event);
			}
		}
	};

	function delete_cookie(name) {
		document.cookie = name + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
	}

	this.logout = function () {
		clearTimeout(context.eventsTimeout);
		delete_cookie('_1024_sid');
		this.authenticated = false;
	};
}

function paddy(n, p, c) {
	var pad_char = typeof c !== 'undefined' ? c : '0';
	var pad = new Array(1 + p).join(pad_char);
	return (pad + n).slice(-pad.length);
}
function format_effi_date(date) {
	if (date == null) return 'not-a-date';
	return paddy(date.getFullYear(), 4) + paddy(date.getMonth() + 1, 2) + paddy(date.getDate(), 2);
}
function format_effi_time(date) {
	if (date == null) return 'not-a-date-time';
	return paddy(date.getFullYear(), 4) + paddy(date.getMonth() + 1, 2) + paddy(date.getDate(), 2) + 'T' + paddy(date.getHours(), 2) + paddy(date.getMinutes(), 2) + paddy(date.getSeconds(), 2);
}

function encodeAURLComponent(obj) {
	var serialized = '';
	if (typeof obj == 'undefined' || obj == null) return 'none:&';else if (obj.type == 'array' || Array.isArray(obj)) {
		var s = '';
		for (var i = 0; i < obj.length; i++) {
			var o = obj[i];
			s += encodeAURLComponent(o);
		}
		serialized += 'Value:Array:' + s + '&&';
	} else if (obj.type == 'optionalInt') serialized = 'optional:i:' + obj.value + '&&';else if (obj.type == 'float' || typeof obj.value == 'float') serialized = 'd:' + obj.value + '&';else if (obj.type == 'int' || typeof obj.value == 'number') serialized = 'i:' + obj.value + '&';else if (obj.type == 'date' || obj.type == 'ADate') serialized = 'ADate:s:' + format_effi_date(obj.value) + '&&';else if (obj.type == 'datetime' || obj.type == 'Time' || obj.value instanceof Date) serialized = 'Time:s:' + format_effi_time(obj.value) + '&&';else if (obj.type == 'checkbox') serialized = 's:' + obj.value + '&';else if (obj.type == 'optionalString') serialized = 'optional:s:' + obj.value.replace(/ /g, '\\w') + '&&';else if (obj.type == 'binary') serialized = 'b:' + _base2.default.Base64Encode(_base2.default.UTF8Encode(obj.value)) + '&';else {
		var v = typeof obj.value == 'undefined' ? obj : obj.value;
		serialized = 's:' + v.replace(/ /g, '\\w') + '&';
	}
	return serialized;
}

function encodePlain(obj) {
	var serialized = '';
	if (typeof obj == 'undefined' || obj == null) return 'none:&';else if (obj.type == 'optionalInt') serialized = 'optional:i:' + obj.value + '&&';else if (obj.type == 'float' || typeof obj.value == 'float') serialized = 'd:' + obj.value + '&';else if (obj.type == 'int' || typeof obj.value == 'number') serialized = 'i:' + obj.value + '&';else if (obj.type == 'date' || obj.type == 'ADate') serialized = 'ADate:s:' + format_effi_date(obj.value) + '&&';else if (obj.type == 'datetime' || obj.type == 'Time' || obj.value instanceof Date) serialized = 'Time:s:' + format_effi_time(obj.value) + '&&';else if (obj.type == 'checkbox') serialized = 's:' + obj.value + '&';else if (obj.type == 'optionalString') serialized = 'optional:s:' + obj.value.replace(/ /g, '\\w') + '&&';else if (obj.type == 'binary') serialized = 'b:' + _base2.default.Base64Encode(obj.value) + '&';else {
		var v = typeof obj.value == 'undefined' ? obj : obj.value;
		serialized = 's:' + v.replace(/ /g, '\\w') + '&';
	}
	return serialized;
}
function encodeBlob(file, callback) {
	var reader = new FileReader();

	reader.onload = function (readerEvt) {
		console.log('loaded file size=' + readerEvt.target.result.length);
		var serialized = 'b:' + _base2.default.Base64Encode(readerEvt.target.result) + '&';
		callback(serialized);
	};

	reader.readAsBinaryString(file);
}
function encodeBlobFile(file, callback) {
	encodeBlob(file, function (serialized_blob) {
		var serialized = 'BlobFile:' + encodeAURLComponent({ value: file.name, type: 'string' }) + encodeAURLComponent({ value: file.type, type: 'string' }) + serialized_blob + encodeAURLComponent(null) + encodeAURLComponent({ value: file.lastModifiedDate, type: 'Time' }) + '&';
		callback(serialized);
	});
}

function reduceSerializationArray(serialized, array, i, callback) {
	if (array.length == 0) {
		callback(serialized);
		return;
	}
	var obj = array.splice(0, 1);
	encodeAURLComponentAsync(serialized, obj, function (v) {
		reduceSerializationArray(v, array, ++i, callback);
	});
}
function reduceSerializationStructure(serialized, array, object, i, callback) {
	if (array.length == 0) {
		callback(serialized);
		return;
	}
	var key = array.splice(0, 1);
	var obj = object[key];
	var s = serialized + key + '=';
	encodeAURLComponentAsync(s, obj, function (v) {
		reduceSerializationStructure(v, array, object, ++i, callback);
	});
}

function encodeAURLComponentAsync(serialized, obj, callback) {
	// console.log(obj);
	if (obj.type == 'BlobFile' || obj.value instanceof File) {
		encodeBlobFile(obj.value, function (serialized_file) {
			callback(serialized + serialized_file);
		});
	} else if (obj.type == 'Array' || Object.prototype.toString.call(obj) === '[object Array]') {
		var o = obj.value || obj;
		reduceSerializationArray(serialized + 'Value:Array:', o, 0, function (v) {
			callback(v + '&&');
		});
	} else if (obj.type == 'Structure') {
		var keys = [];
		for (var k in obj.value) {
			keys.push(k);
		}var s = serialized + 'Value:Structure:';
		reduceSerializationStructure(s, keys, obj.value, 0, function (v) {
			callback(v + '&&');
		});
	} else {
		var s3 = encodePlain(obj);
		callback(serialized + s3);
	}
}

function serializeAURL(a) {
	var result = '';
	for (var key in a) {
		var o = a[key];
		result += key + "=" + encodeAURLComponent(o);
	}
	return result;
};
function serializeAURLAsync(a, callback) {
	var keys = [];
	for (var k in a) {
		keys.push(k);
	}reduceSerializationStructure('', keys, a, 0, callback);
}

function prepareDataList(data) {
	if (!data || data.length == 0) throw "Invalid DataList argument. ";
	var header = data[0];
	var result = {
		header: header,
		data: []
	};
	for (var i = 1; i < data.length; i++) {
		var r = data[i],
		    row = {};
		for (var c = 0; c < r.length; c++) {
			row[header[c]] = r[c];
		}
		result.data.push(row);
	}
	return result;
}

/*
jQuery.fn.extend({
	serializeAURL: function() {
		return serializeAURL( this.serializeTypedArray() );
	},
	serializeTypedArray: function() {
		var rCRLF = /\r?\n/g,
			rsubmitterTypes = /^(?:submit|button|image|reset|file)$/i,
			rsubmittable = /^(?:input|select|textarea|keygen)/i,
			rcheckableType = /^(?:checkbox|radio)$/i;
		
		function getv(val, type, isArray) {
			type = type || 'string';
			var v = "";
			if (type == 'date') {
				var found = val.match(/(\d\d)\.(\d\d)\.(\d\d\d\d)( (\d\d):(\d\d)(:(\d\d))?)?/);
				if (found != null) {
					// var h = found[5] ? parseInt(found[5]) : undefined, 
					// 	m = found[6] ? parseInt(found[6]) : undefined, 
					// 	s = found[8] ? parseInt(found[8]) : undefined;
					v = new Date(parseInt(found[3]), parseInt(found[2])-1, parseInt(found[1]));
					// console.log(parseInt(found[3]), parseInt(found[2])-1, parseInt(found[1]), h, m, s, '->', v);
				}
				else v = null;
			}
			// else if (type == 'checkbox') 
			else if (type == 'int') v = parseInt(val);
			else if (type == 'checkbox' && !isArray) {
				v = (val == 'on' ? true : false);
			}
			else v = val.replace( rCRLF, "\r\n" );
			return v;
		}

		var list = this.map(function() {
			// Can add propHook for "elements" to filter or add form elements
			var elements = jQuery.prop( this, "elements" );
			return elements ? jQuery.makeArray( elements ) : this;
		})
		.filter(function() {
			var type = this.type;

			// Use .is( ":disabled" ) so that fieldset[disabled] works
			return this.name && !jQuery( this ).is( ":disabled" ) &&
				rsubmittable.test( this.nodeName ) && !rsubmitterTypes.test( type ) &&
				( this.checked || !rcheckableType.test( type ) );
		})
		.map(function( i, elem ) {
			var val = jQuery( this ).val(),
				type = jQuery( this ).attr('data-type') || this.type,
				isArray = jQuery( this ).attr('data-array') == 'true' || false;

			return val == null ?
				null :
				jQuery.isArray( val ) ?
					jQuery.map( val, function( val ) {
						return { name: elem.name, value: getv(val, type, isArray), type: type, isArray: isArray };
					}) :
					{ name: elem.name, value: getv(val, type, isArray), type: type, isArray: isArray };
		}).get();

		var s = {}
		for (var i=0; i<list.length; i++) {
			var o = list[i];
			if (o.isArray || o.name in s) {
				if (!(o.name in s)) s[o.name] = [o];
				else s[o.name].push(o);
			}
			else s[o.name] = o;
		}
		return s;
	}
});
*/

exports.EffiProtocol = EffiProtocol;
exports.serializeAURL = serializeAURL;
exports.serializeAURLAsync = serializeAURLAsync;
exports.format_effi_date = format_effi_date;
exports.format_effi_time = format_effi_time;
exports.prepareDataList = prepareDataList;

/***/ }),

/***/ 73:
/***/ (function(module, exports, __webpack_require__) {

/*!
 * Bowser - a browser detector
 * https://github.com/ded/bowser
 * MIT License | (c) Dustin Diaz 2015
 */

!function (root, name, definition) {
  if (typeof module != 'undefined' && module.exports) module.exports = definition()
  else if (true) __webpack_require__(75)(name, definition)
  else root[name] = definition()
}(this, 'bowser', function () {
  /**
    * See useragents.js for examples of navigator.userAgent
    */

  var t = true

  function detect(ua) {

    function getFirstMatch(regex) {
      var match = ua.match(regex);
      return (match && match.length > 1 && match[1]) || '';
    }

    function getSecondMatch(regex) {
      var match = ua.match(regex);
      return (match && match.length > 1 && match[2]) || '';
    }

    var iosdevice = getFirstMatch(/(ipod|iphone|ipad)/i).toLowerCase()
      , likeAndroid = /like android/i.test(ua)
      , android = !likeAndroid && /android/i.test(ua)
      , nexusMobile = /nexus\s*[0-6]\s*/i.test(ua)
      , nexusTablet = !nexusMobile && /nexus\s*[0-9]+/i.test(ua)
      , chromeos = /CrOS/.test(ua)
      , silk = /silk/i.test(ua)
      , sailfish = /sailfish/i.test(ua)
      , tizen = /tizen/i.test(ua)
      , webos = /(web|hpw)(o|0)s/i.test(ua)
      , windowsphone = /windows phone/i.test(ua)
      , samsungBrowser = /SamsungBrowser/i.test(ua)
      , windows = !windowsphone && /windows/i.test(ua)
      , mac = !iosdevice && !silk && /macintosh/i.test(ua)
      , linux = !android && !sailfish && !tizen && !webos && /linux/i.test(ua)
      , edgeVersion = getSecondMatch(/edg([ea]|ios)\/(\d+(\.\d+)?)/i)
      , versionIdentifier = getFirstMatch(/version\/(\d+(\.\d+)?)/i)
      , tablet = /tablet/i.test(ua) && !/tablet pc/i.test(ua)
      , mobile = !tablet && /[^-]mobi/i.test(ua)
      , xbox = /xbox/i.test(ua)
      , result

    if (/opera/i.test(ua)) {
      //  an old Opera
      result = {
        name: 'Opera'
      , opera: t
      , version: versionIdentifier || getFirstMatch(/(?:opera|opr|opios)[\s\/](\d+(\.\d+)?)/i)
      }
    } else if (/opr\/|opios/i.test(ua)) {
      // a new Opera
      result = {
        name: 'Opera'
        , opera: t
        , version: getFirstMatch(/(?:opr|opios)[\s\/](\d+(\.\d+)?)/i) || versionIdentifier
      }
    }
    else if (/SamsungBrowser/i.test(ua)) {
      result = {
        name: 'Samsung Internet for Android'
        , samsungBrowser: t
        , version: versionIdentifier || getFirstMatch(/(?:SamsungBrowser)[\s\/](\d+(\.\d+)?)/i)
      }
    }
    else if (/Whale/i.test(ua)) {
      result = {
        name: 'NAVER Whale browser'
        , whale: t
        , version: getFirstMatch(/(?:whale)[\s\/](\d+(?:\.\d+)+)/i)
      }
    }
    else if (/MZBrowser/i.test(ua)) {
      result = {
        name: 'MZ Browser'
        , mzbrowser: t
        , version: getFirstMatch(/(?:MZBrowser)[\s\/](\d+(?:\.\d+)+)/i)
      }
    }
    else if (/coast/i.test(ua)) {
      result = {
        name: 'Opera Coast'
        , coast: t
        , version: versionIdentifier || getFirstMatch(/(?:coast)[\s\/](\d+(\.\d+)?)/i)
      }
    }
    else if (/focus/i.test(ua)) {
      result = {
        name: 'Focus'
        , focus: t
        , version: getFirstMatch(/(?:focus)[\s\/](\d+(?:\.\d+)+)/i)
      }
    }
    else if (/yabrowser/i.test(ua)) {
      result = {
        name: 'Yandex Browser'
      , yandexbrowser: t
      , version: versionIdentifier || getFirstMatch(/(?:yabrowser)[\s\/](\d+(\.\d+)?)/i)
      }
    }
    else if (/ucbrowser/i.test(ua)) {
      result = {
          name: 'UC Browser'
        , ucbrowser: t
        , version: getFirstMatch(/(?:ucbrowser)[\s\/](\d+(?:\.\d+)+)/i)
      }
    }
    else if (/mxios/i.test(ua)) {
      result = {
        name: 'Maxthon'
        , maxthon: t
        , version: getFirstMatch(/(?:mxios)[\s\/](\d+(?:\.\d+)+)/i)
      }
    }
    else if (/epiphany/i.test(ua)) {
      result = {
        name: 'Epiphany'
        , epiphany: t
        , version: getFirstMatch(/(?:epiphany)[\s\/](\d+(?:\.\d+)+)/i)
      }
    }
    else if (/puffin/i.test(ua)) {
      result = {
        name: 'Puffin'
        , puffin: t
        , version: getFirstMatch(/(?:puffin)[\s\/](\d+(?:\.\d+)?)/i)
      }
    }
    else if (/sleipnir/i.test(ua)) {
      result = {
        name: 'Sleipnir'
        , sleipnir: t
        , version: getFirstMatch(/(?:sleipnir)[\s\/](\d+(?:\.\d+)+)/i)
      }
    }
    else if (/k-meleon/i.test(ua)) {
      result = {
        name: 'K-Meleon'
        , kMeleon: t
        , version: getFirstMatch(/(?:k-meleon)[\s\/](\d+(?:\.\d+)+)/i)
      }
    }
    else if (windowsphone) {
      result = {
        name: 'Windows Phone'
      , osname: 'Windows Phone'
      , windowsphone: t
      }
      if (edgeVersion) {
        result.msedge = t
        result.version = edgeVersion
      }
      else {
        result.msie = t
        result.version = getFirstMatch(/iemobile\/(\d+(\.\d+)?)/i)
      }
    }
    else if (/msie|trident/i.test(ua)) {
      result = {
        name: 'Internet Explorer'
      , msie: t
      , version: getFirstMatch(/(?:msie |rv:)(\d+(\.\d+)?)/i)
      }
    } else if (chromeos) {
      result = {
        name: 'Chrome'
      , osname: 'Chrome OS'
      , chromeos: t
      , chromeBook: t
      , chrome: t
      , version: getFirstMatch(/(?:chrome|crios|crmo)\/(\d+(\.\d+)?)/i)
      }
    } else if (/edg([ea]|ios)/i.test(ua)) {
      result = {
        name: 'Microsoft Edge'
      , msedge: t
      , version: edgeVersion
      }
    }
    else if (/vivaldi/i.test(ua)) {
      result = {
        name: 'Vivaldi'
        , vivaldi: t
        , version: getFirstMatch(/vivaldi\/(\d+(\.\d+)?)/i) || versionIdentifier
      }
    }
    else if (sailfish) {
      result = {
        name: 'Sailfish'
      , osname: 'Sailfish OS'
      , sailfish: t
      , version: getFirstMatch(/sailfish\s?browser\/(\d+(\.\d+)?)/i)
      }
    }
    else if (/seamonkey\//i.test(ua)) {
      result = {
        name: 'SeaMonkey'
      , seamonkey: t
      , version: getFirstMatch(/seamonkey\/(\d+(\.\d+)?)/i)
      }
    }
    else if (/firefox|iceweasel|fxios/i.test(ua)) {
      result = {
        name: 'Firefox'
      , firefox: t
      , version: getFirstMatch(/(?:firefox|iceweasel|fxios)[ \/](\d+(\.\d+)?)/i)
      }
      if (/\((mobile|tablet);[^\)]*rv:[\d\.]+\)/i.test(ua)) {
        result.firefoxos = t
        result.osname = 'Firefox OS'
      }
    }
    else if (silk) {
      result =  {
        name: 'Amazon Silk'
      , silk: t
      , version : getFirstMatch(/silk\/(\d+(\.\d+)?)/i)
      }
    }
    else if (/phantom/i.test(ua)) {
      result = {
        name: 'PhantomJS'
      , phantom: t
      , version: getFirstMatch(/phantomjs\/(\d+(\.\d+)?)/i)
      }
    }
    else if (/slimerjs/i.test(ua)) {
      result = {
        name: 'SlimerJS'
        , slimer: t
        , version: getFirstMatch(/slimerjs\/(\d+(\.\d+)?)/i)
      }
    }
    else if (/blackberry|\bbb\d+/i.test(ua) || /rim\stablet/i.test(ua)) {
      result = {
        name: 'BlackBerry'
      , osname: 'BlackBerry OS'
      , blackberry: t
      , version: versionIdentifier || getFirstMatch(/blackberry[\d]+\/(\d+(\.\d+)?)/i)
      }
    }
    else if (webos) {
      result = {
        name: 'WebOS'
      , osname: 'WebOS'
      , webos: t
      , version: versionIdentifier || getFirstMatch(/w(?:eb)?osbrowser\/(\d+(\.\d+)?)/i)
      };
      /touchpad\//i.test(ua) && (result.touchpad = t)
    }
    else if (/bada/i.test(ua)) {
      result = {
        name: 'Bada'
      , osname: 'Bada'
      , bada: t
      , version: getFirstMatch(/dolfin\/(\d+(\.\d+)?)/i)
      };
    }
    else if (tizen) {
      result = {
        name: 'Tizen'
      , osname: 'Tizen'
      , tizen: t
      , version: getFirstMatch(/(?:tizen\s?)?browser\/(\d+(\.\d+)?)/i) || versionIdentifier
      };
    }
    else if (/qupzilla/i.test(ua)) {
      result = {
        name: 'QupZilla'
        , qupzilla: t
        , version: getFirstMatch(/(?:qupzilla)[\s\/](\d+(?:\.\d+)+)/i) || versionIdentifier
      }
    }
    else if (/chromium/i.test(ua)) {
      result = {
        name: 'Chromium'
        , chromium: t
        , version: getFirstMatch(/(?:chromium)[\s\/](\d+(?:\.\d+)?)/i) || versionIdentifier
      }
    }
    else if (/chrome|crios|crmo/i.test(ua)) {
      result = {
        name: 'Chrome'
        , chrome: t
        , version: getFirstMatch(/(?:chrome|crios|crmo)\/(\d+(\.\d+)?)/i)
      }
    }
    else if (android) {
      result = {
        name: 'Android'
        , version: versionIdentifier
      }
    }
    else if (/safari|applewebkit/i.test(ua)) {
      result = {
        name: 'Safari'
      , safari: t
      }
      if (versionIdentifier) {
        result.version = versionIdentifier
      }
    }
    else if (iosdevice) {
      result = {
        name : iosdevice == 'iphone' ? 'iPhone' : iosdevice == 'ipad' ? 'iPad' : 'iPod'
      }
      // WTF: version is not part of user agent in web apps
      if (versionIdentifier) {
        result.version = versionIdentifier
      }
    }
    else if(/googlebot/i.test(ua)) {
      result = {
        name: 'Googlebot'
      , googlebot: t
      , version: getFirstMatch(/googlebot\/(\d+(\.\d+))/i) || versionIdentifier
      }
    }
    else {
      result = {
        name: getFirstMatch(/^(.*)\/(.*) /),
        version: getSecondMatch(/^(.*)\/(.*) /)
     };
   }

    // set webkit or gecko flag for browsers based on these engines
    if (!result.msedge && /(apple)?webkit/i.test(ua)) {
      if (/(apple)?webkit\/537\.36/i.test(ua)) {
        result.name = result.name || "Blink"
        result.blink = t
      } else {
        result.name = result.name || "Webkit"
        result.webkit = t
      }
      if (!result.version && versionIdentifier) {
        result.version = versionIdentifier
      }
    } else if (!result.opera && /gecko\//i.test(ua)) {
      result.name = result.name || "Gecko"
      result.gecko = t
      result.version = result.version || getFirstMatch(/gecko\/(\d+(\.\d+)?)/i)
    }

    // set OS flags for platforms that have multiple browsers
    if (!result.windowsphone && (android || result.silk)) {
      result.android = t
      result.osname = 'Android'
    } else if (!result.windowsphone && iosdevice) {
      result[iosdevice] = t
      result.ios = t
      result.osname = 'iOS'
    } else if (mac) {
      result.mac = t
      result.osname = 'macOS'
    } else if (xbox) {
      result.xbox = t
      result.osname = 'Xbox'
    } else if (windows) {
      result.windows = t
      result.osname = 'Windows'
    } else if (linux) {
      result.linux = t
      result.osname = 'Linux'
    }

    function getWindowsVersion (s) {
      switch (s) {
        case 'NT': return 'NT'
        case 'XP': return 'XP'
        case 'NT 5.0': return '2000'
        case 'NT 5.1': return 'XP'
        case 'NT 5.2': return '2003'
        case 'NT 6.0': return 'Vista'
        case 'NT 6.1': return '7'
        case 'NT 6.2': return '8'
        case 'NT 6.3': return '8.1'
        case 'NT 10.0': return '10'
        default: return undefined
      }
    }

    // OS version extraction
    var osVersion = '';
    if (result.windows) {
      osVersion = getWindowsVersion(getFirstMatch(/Windows ((NT|XP)( \d\d?.\d)?)/i))
    } else if (result.windowsphone) {
      osVersion = getFirstMatch(/windows phone (?:os)?\s?(\d+(\.\d+)*)/i);
    } else if (result.mac) {
      osVersion = getFirstMatch(/Mac OS X (\d+([_\.\s]\d+)*)/i);
      osVersion = osVersion.replace(/[_\s]/g, '.');
    } else if (iosdevice) {
      osVersion = getFirstMatch(/os (\d+([_\s]\d+)*) like mac os x/i);
      osVersion = osVersion.replace(/[_\s]/g, '.');
    } else if (android) {
      osVersion = getFirstMatch(/android[ \/-](\d+(\.\d+)*)/i);
    } else if (result.webos) {
      osVersion = getFirstMatch(/(?:web|hpw)os\/(\d+(\.\d+)*)/i);
    } else if (result.blackberry) {
      osVersion = getFirstMatch(/rim\stablet\sos\s(\d+(\.\d+)*)/i);
    } else if (result.bada) {
      osVersion = getFirstMatch(/bada\/(\d+(\.\d+)*)/i);
    } else if (result.tizen) {
      osVersion = getFirstMatch(/tizen[\/\s](\d+(\.\d+)*)/i);
    }
    if (osVersion) {
      result.osversion = osVersion;
    }

    // device type extraction
    var osMajorVersion = !result.windows && osVersion.split('.')[0];
    if (
         tablet
      || nexusTablet
      || iosdevice == 'ipad'
      || (android && (osMajorVersion == 3 || (osMajorVersion >= 4 && !mobile)))
      || result.silk
    ) {
      result.tablet = t
    } else if (
         mobile
      || iosdevice == 'iphone'
      || iosdevice == 'ipod'
      || android
      || nexusMobile
      || result.blackberry
      || result.webos
      || result.bada
    ) {
      result.mobile = t
    }

    // Graded Browser Support
    // http://developer.yahoo.com/yui/articles/gbs
    if (result.msedge ||
        (result.msie && result.version >= 10) ||
        (result.yandexbrowser && result.version >= 15) ||
		    (result.vivaldi && result.version >= 1.0) ||
        (result.chrome && result.version >= 20) ||
        (result.samsungBrowser && result.version >= 4) ||
        (result.whale && compareVersions([result.version, '1.0']) === 1) ||
        (result.mzbrowser && compareVersions([result.version, '6.0']) === 1) ||
        (result.focus && compareVersions([result.version, '1.0']) === 1) ||
        (result.firefox && result.version >= 20.0) ||
        (result.safari && result.version >= 6) ||
        (result.opera && result.version >= 10.0) ||
        (result.ios && result.osversion && result.osversion.split(".")[0] >= 6) ||
        (result.blackberry && result.version >= 10.1)
        || (result.chromium && result.version >= 20)
        ) {
      result.a = t;
    }
    else if ((result.msie && result.version < 10) ||
        (result.chrome && result.version < 20) ||
        (result.firefox && result.version < 20.0) ||
        (result.safari && result.version < 6) ||
        (result.opera && result.version < 10.0) ||
        (result.ios && result.osversion && result.osversion.split(".")[0] < 6)
        || (result.chromium && result.version < 20)
        ) {
      result.c = t
    } else result.x = t

    return result
  }

  var bowser = detect(typeof navigator !== 'undefined' ? navigator.userAgent || '' : '')

  bowser.test = function (browserList) {
    for (var i = 0; i < browserList.length; ++i) {
      var browserItem = browserList[i];
      if (typeof browserItem=== 'string') {
        if (browserItem in bowser) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * Get version precisions count
   *
   * @example
   *   getVersionPrecision("1.10.3") // 3
   *
   * @param  {string} version
   * @return {number}
   */
  function getVersionPrecision(version) {
    return version.split(".").length;
  }

  /**
   * Array::map polyfill
   *
   * @param  {Array} arr
   * @param  {Function} iterator
   * @return {Array}
   */
  function map(arr, iterator) {
    var result = [], i;
    if (Array.prototype.map) {
      return Array.prototype.map.call(arr, iterator);
    }
    for (i = 0; i < arr.length; i++) {
      result.push(iterator(arr[i]));
    }
    return result;
  }

  /**
   * Calculate browser version weight
   *
   * @example
   *   compareVersions(['1.10.2.1',  '1.8.2.1.90'])    // 1
   *   compareVersions(['1.010.2.1', '1.09.2.1.90']);  // 1
   *   compareVersions(['1.10.2.1',  '1.10.2.1']);     // 0
   *   compareVersions(['1.10.2.1',  '1.0800.2']);     // -1
   *
   * @param  {Array<String>} versions versions to compare
   * @return {Number} comparison result
   */
  function compareVersions(versions) {
    // 1) get common precision for both versions, for example for "10.0" and "9" it should be 2
    var precision = Math.max(getVersionPrecision(versions[0]), getVersionPrecision(versions[1]));
    var chunks = map(versions, function (version) {
      var delta = precision - getVersionPrecision(version);

      // 2) "9" -> "9.0" (for precision = 2)
      version = version + new Array(delta + 1).join(".0");

      // 3) "9.0" -> ["000000000"", "000000009"]
      return map(version.split("."), function (chunk) {
        return new Array(20 - chunk.length).join("0") + chunk;
      }).reverse();
    });

    // iterate in reverse order by reversed chunks array
    while (--precision >= 0) {
      // 4) compare: "000000009" > "000000010" = false (but "9" > "10" = true)
      if (chunks[0][precision] > chunks[1][precision]) {
        return 1;
      }
      else if (chunks[0][precision] === chunks[1][precision]) {
        if (precision === 0) {
          // all version chunks are same
          return 0;
        }
      }
      else {
        return -1;
      }
    }
  }

  /**
   * Check if browser is unsupported
   *
   * @example
   *   bowser.isUnsupportedBrowser({
   *     msie: "10",
   *     firefox: "23",
   *     chrome: "29",
   *     safari: "5.1",
   *     opera: "16",
   *     phantom: "534"
   *   });
   *
   * @param  {Object}  minVersions map of minimal version to browser
   * @param  {Boolean} [strictMode = false] flag to return false if browser wasn't found in map
   * @param  {String}  [ua] user agent string
   * @return {Boolean}
   */
  function isUnsupportedBrowser(minVersions, strictMode, ua) {
    var _bowser = bowser;

    // make strictMode param optional with ua param usage
    if (typeof strictMode === 'string') {
      ua = strictMode;
      strictMode = void(0);
    }

    if (strictMode === void(0)) {
      strictMode = false;
    }
    if (ua) {
      _bowser = detect(ua);
    }

    var version = "" + _bowser.version;
    for (var browser in minVersions) {
      if (minVersions.hasOwnProperty(browser)) {
        if (_bowser[browser]) {
          if (typeof minVersions[browser] !== 'string') {
            throw new Error('Browser version in the minVersion map should be a string: ' + browser + ': ' + String(minVersions));
          }

          // browser version and min supported version.
          return compareVersions([version, minVersions[browser]]) < 0;
        }
      }
    }

    return strictMode; // not found
  }

  /**
   * Check if browser is supported
   *
   * @param  {Object} minVersions map of minimal version to browser
   * @param  {Boolean} [strictMode = false] flag to return false if browser wasn't found in map
   * @param  {String}  [ua] user agent string
   * @return {Boolean}
   */
  function check(minVersions, strictMode, ua) {
    return !isUnsupportedBrowser(minVersions, strictMode, ua);
  }

  bowser.isUnsupportedBrowser = isUnsupportedBrowser;
  bowser.compareVersions = compareVersions;
  bowser.check = check;

  /*
   * Set our detect method to the main bowser object so we can
   * reuse it to test other user agents.
   * This is needed to implement future tests.
   */
  bowser._detect = detect;

  /*
   * Set our detect public method to the main bowser object
   * This is needed to implement bowser in server side
   */
  bowser.detect = detect;
  return bowser
});


/***/ }),

/***/ 74:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _bowser = __webpack_require__(73);

var _bowser2 = _interopRequireDefault(_bowser);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Encoder = {
	sKeyStr_: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
	bInitialized_: false,

	initialize: function initialize() {
		if (!this.bInitialized_) {
			// perform initialization
			this.aHashTab_ = new Array();
			for (var i = 0; i < this.sKeyStr_.length; ++i) {
				var c = this.sKeyStr_.charCodeAt(i);
				this.aHashTab_[c] = i; // store char index
			}
			this.bInitialized_ = true;
		}
	},

	Base64Encode: function Base64Encode(sInput) {
		// If available, use Mozilla/Safari/Chrome fast native base64 encoder
		if (typeof btoa != "undefined") return btoa(sInput);

		// ensure hash table is initialized
		this.initialize();
		var aOut = new Array();
		var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
		var i = 0;
		var di = 0;
		var len = sInput.length;
		while (i < len) {
			chr1 = sInput.charCodeAt(i++);
			chr2 = sInput.charCodeAt(i++);
			chr3 = sInput.charCodeAt(i++);

			enc1 = chr1 >> 2;
			enc2 = (chr1 & 3) << 4 | chr2 >> 4;
			enc3 = (chr2 & 15) << 2 | chr3 >> 6;
			enc4 = chr3 & 63;

			if (isNaN(chr2)) {
				enc3 = enc4 = 64;
			} else if (isNaN(chr3)) {
				enc4 = 64;
			}
			aOut[di++] = this.sKeyStr_.charAt(enc1);
			aOut[di++] = this.sKeyStr_.charAt(enc2);
			aOut[di++] = this.sKeyStr_.charAt(enc3);
			aOut[di++] = this.sKeyStr_.charAt(enc4);
		}
		return aOut.join('');
	},

	// public method for decoding
	Base64Decode: function Base64Decode(sInput) {
		// If available, use Mozilla/Safari/Chrome fast native decoder
		if (typeof atob != "undefined") return atob(sInput);

		// ensure hash table is initialized
		this.initialize();
		var aOut = new Array();
		var chr1, chr2, chr3;
		var enc1, enc2, enc3, enc4;
		var i = 0;
		sInput = sInput.replace(/[^A-Za-z0-9\+\/\=]/g, "");

		var len = sInput.length;
		var di = 0;
		while (i < len) {
			enc1 = this.aHashTab_[sInput.charCodeAt(i++)];
			enc2 = this.aHashTab_[sInput.charCodeAt(i++)];
			enc3 = this.aHashTab_[sInput.charCodeAt(i++)];
			enc4 = this.aHashTab_[sInput.charCodeAt(i++)];

			chr1 = enc1 << 2 | enc2 >> 4;
			chr2 = (enc2 & 15) << 4 | enc3 >> 2;
			chr3 = (enc3 & 3) << 6 | enc4;

			aOut[di++] = String.fromCharCode(chr1);
			if (enc3 != 64) {
				aOut[di++] = String.fromCharCode(chr2);
			}
			if (enc4 != 64) {
				aOut[di++] = String.fromCharCode(chr3);
			}
		}
		return aOut.join('');
	},

	UTF8Encode: function UTF8Encode(sString) {
		//sString = sString.replace(/\r\n/g,"\n");
		var len = sString.length;
		if (_bowser2.default.msie) {
			var aBytes = new Array();
			var di = 0;
			for (var n = 0; n < len; ++n) {
				var c = sString.charCodeAt(n);
				if (c < 128) {
					aBytes[di++] = String.fromCharCode(c);
				} else if (c > 127 && c < 2048) {
					aBytes[di++] = String.fromCharCode(c >> 6 | 192);
					aBytes[di++] = String.fromCharCode(c & 63 | 128);
				} else {
					aBytes[di++] += String.fromCharCode(c >> 12 | 224);
					aBytes[di++] += String.fromCharCode(c >> 6 & 63 | 128);
					aBytes[di++] += String.fromCharCode(c & 63 | 128);
				}
			}
			return aBytes.join('');
		} else {
			var sBytes = "";
			for (var n = 0; n < len; ++n) {
				var c = sString.charCodeAt(n);
				if (c < 128) {
					sBytes += String.fromCharCode(c);
				} else if (c > 127 && c < 2048) {
					sBytes += String.fromCharCode(c >> 6 | 192);
					sBytes += String.fromCharCode(c & 63 | 128);
				} else {
					sBytes += String.fromCharCode(c >> 12 | 224);
					sBytes += String.fromCharCode(c >> 6 & 63 | 128);
					sBytes += String.fromCharCode(c & 63 | 128);
				}
			}
			return sBytes;
		}
	},

	UTF8Decode: function UTF8Decode(sBytes) {
		var i = 0;
		var c1 = 0;
		var c2 = 0;
		var c3 = 0;
		var len = sBytes.length;

		if (_bowser2.default.msi) {
			// Use array join technique
			var aOut = new Array();
			var di = 0;
			while (i < len) {
				c1 = sBytes.charCodeAt(i);
				if (c1 < 128) {
					aOut[di++] = String.fromCharCode(c1);
					i++;
				} else if (c1 > 191 && c1 < 224) {
					c2 = sBytes.charCodeAt(i + 1);
					aOut[di++] = String.fromCharCode((c1 & 31) << 6 | c2 & 63);
					i += 2;
				} else {
					c2 = sBytes.charCodeAt(i + 1);
					c3 = sBytes.charCodeAt(i + 2);
					aOut[di++] = String.fromCharCode((c1 & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
					i += 3;
				}
			}
			return aOut.join('');
		} else {
			// Use standard += operator technique
			var sOut = "";
			while (i < len) {
				c1 = sBytes.charCodeAt(i);
				if (c1 < 128) {
					sOut += String.fromCharCode(c1);
					i++;
				} else if (c1 > 191 && c1 < 224) {
					c2 = sBytes.charCodeAt(i + 1);
					sOut += String.fromCharCode((c1 & 31) << 6 | c2 & 63);
					i += 2;
				} else {
					c2 = sBytes.charCodeAt(i + 1);
					c3 = sBytes.charCodeAt(i + 2);
					sOut += String.fromCharCode((c1 & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
					i += 3;
				}
			}
			return sOut;
		}
	}
}; /**
     * Encoder/Decoder class
     * (c) ASoft Ltd, 2009-2011 (http://asoft.ru)
     * Some ideas were taken from Base64 class borrowed from http://webtoolkit.info
     * Adapted for Webpack by Computerica
     *
     * Works fast in Mozilla/Chrome/Safari, and performance under IE is much better
     * than original Base64 encoder/decoder
     **/
exports.default = Encoder;

/***/ }),

/***/ 75:
/***/ (function(module, exports) {

module.exports = function() {
	throw new Error("define cannot be used indirect");
};


/***/ }),

/***/ 76:
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_RESULT__;/**!
 * ajax - v2.3.3
 * Ajax module in Vanilla JS
 * https://github.com/fdaciuk/ajax

 * Tue Sep 18 2018 12:44:02 GMT-0300 (-03)
 * MIT (c) Fernando Daciuk
*/
!function(e,t){"use strict"; true?!(__WEBPACK_AMD_DEFINE_FACTORY__ = (t),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.call(exports, __webpack_require__, exports, module)) :
				__WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)):"object"==typeof exports?exports=module.exports=t():e.ajax=t()}(this,function(){"use strict";function e(e){var r=["get","post","put","delete"];return e=e||{},e.baseUrl=e.baseUrl||"",e.method&&e.url?n(e.method,e.baseUrl+e.url,t(e.data),e):r.reduce(function(r,o){return r[o]=function(r,u){return n(o,e.baseUrl+r,t(u),e)},r},{})}function t(e){return e||null}function n(e,t,n,u){var c=["then","catch","always"],i=c.reduce(function(e,t){return e[t]=function(n){return e[t]=n,e},e},{}),f=new XMLHttpRequest,p=r(t,n,e);return f.open(e,p,!0),f.withCredentials=u.hasOwnProperty("withCredentials"),o(f,u.headers,n),f.addEventListener("readystatechange",a(i,f),!1),f.send(s(n)?JSON.stringify(n):n),i.abort=function(){return f.abort()},i}function r(e,t,n){if("get"!==n.toLowerCase()||!t)return e;var r=i(t),o=e.indexOf("?")>-1?"&":"?";return e+o+r}function o(e,t,n){t=t||{},u(t)||(t["Content-Type"]=s(n)?"application/json":"application/x-www-form-urlencoded"),Object.keys(t).forEach(function(n){t[n]&&e.setRequestHeader(n,t[n])})}function u(e){return Object.keys(e).some(function(e){return"content-type"===e.toLowerCase()})}function a(e,t){return function n(){t.readyState===t.DONE&&(t.removeEventListener("readystatechange",n,!1),e.always.apply(e,c(t)),t.status>=200&&t.status<300?e.then.apply(e,c(t)):e["catch"].apply(e,c(t)))}}function c(e){var t;try{t=JSON.parse(e.responseText)}catch(n){t=e.responseText}return[t,e]}function i(e){return s(e)?f(e):e}function s(e){return"[object Object]"===Object.prototype.toString.call(e)}function f(e,t){return Object.keys(e).map(function(n){if(e.hasOwnProperty(n)&&void 0!==e[n]){var r=e[n];return n=t?t+"["+n+"]":n,null!==r&&"object"==typeof r?f(r,n):p(n)+"="+p(r)}}).filter(Boolean).join("&")}function p(e){return encodeURIComponent(e)}return e});

/***/ })

},[382]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWRtaW4uanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vc3JjL2FkbWluLmpzIiwid2VicGFjazovLy9zcmMvbGliL2VmZmlfcHJvdG9jb2wuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2Jvd3Nlci9zcmMvYm93c2VyLmpzIiwid2VicGFjazovLy9zcmMvbGliL2Jhc2U2NC5qcyIsIndlYnBhY2s6Ly8vKHdlYnBhY2spL2J1aWxkaW4vYW1kLWRlZmluZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvQGZkYWNpdWsvYWpheC9kaXN0L2FqYXgubWluLmpzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEVmZmlQcm90b2NvbCB9IGZyb20gJ2xpYi9lZmZpX3Byb3RvY29sJztcblxuXG53aW5kb3cuZWZmaSA9IG5ldyBFZmZpUHJvdG9jb2woKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBzcmMvYWRtaW4uanMiLCIvLyBpbXBvcnQgalF1ZXJ5IGZyb20gJ2pxdWVyeSdcbmltcG9ydCBFbmNvZGVyIGZyb20gJy4vYmFzZTY0LmpzJ1xuaW1wb3J0IGFqYXggZnJvbSAnQGZkYWNpdWsvYWpheCdcbi8vIGltcG9ydCBBamF4IGZyb20gJ3NpbXBsZS1hamF4JztcblxuaWYgKHR5cGVvZiBTdHJpbmcucHJvdG90eXBlLnN0YXJ0c1dpdGggIT0gJ2Z1bmN0aW9uJykge1xuXHRTdHJpbmcucHJvdG90eXBlLnN0YXJ0c1dpdGggPSBmdW5jdGlvbiAoc3RyKSB7XG5cdFx0cmV0dXJuIHRoaXMuaW5kZXhPZihzdHIpID09PSAwO1xuXHR9O1xufVxuXG5pZiAoIUFycmF5LmlzQXJyYXkpIHtcblx0QXJyYXkuaXNBcnJheSA9IGZ1bmN0aW9uKGFyZykge1xuXHRcdHJldHVybiBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwoYXJnKSA9PT0gJ1tvYmplY3QgQXJyYXldJztcblx0fTtcbn1cbmZ1bmN0aW9uIGlzT2JqZWN0KG9iaikge1xuICAgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmcuY2FsbChvYmopID09PSAnW29iamVjdCBPYmplY3RdJztcbn1cblxuXG5sZXQgcGFyc2VYbWw7XG5cbmlmICh0eXBlb2Ygd2luZG93LkRPTVBhcnNlciAhPSBcInVuZGVmaW5lZFwiKSB7XG4gICAgcGFyc2VYbWwgPSBmdW5jdGlvbih4bWxTdHIpIHtcbiAgICAgICAgcmV0dXJuICggbmV3IHdpbmRvdy5ET01QYXJzZXIoKSApLnBhcnNlRnJvbVN0cmluZyh4bWxTdHIsIFwidGV4dC94bWxcIik7XG4gICAgfTtcbn0gZWxzZSBpZiAodHlwZW9mIHdpbmRvdy5BY3RpdmVYT2JqZWN0ICE9IFwidW5kZWZpbmVkXCIgJiZcbiAgICAgICBuZXcgd2luZG93LkFjdGl2ZVhPYmplY3QoXCJNaWNyb3NvZnQuWE1MRE9NXCIpKSB7XG4gICAgcGFyc2VYbWwgPSBmdW5jdGlvbih4bWxTdHIpIHtcbiAgICAgICAgdmFyIHhtbERvYyA9IG5ldyB3aW5kb3cuQWN0aXZlWE9iamVjdChcIk1pY3Jvc29mdC5YTUxET01cIik7XG4gICAgICAgIHhtbERvYy5hc3luYyA9IFwiZmFsc2VcIjtcbiAgICAgICAgeG1sRG9jLmxvYWRYTUwoeG1sU3RyKTtcbiAgICAgICAgcmV0dXJuIHhtbERvYztcbiAgICB9O1xufSBlbHNlIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoXCJObyBYTUwgcGFyc2VyIGZvdW5kXCIpO1xufVxuXG5cbmZ1bmN0aW9uIEVmZmlQcm90b2NvbChvcHRzKSB7XG5cdG9wdHMgPSBvcHRzIHx8IHt9O1xuXHR0aGlzLmxvZ2luID0gb3B0cy5sb2dpbiB8fCAnYmFybic7XG5cdHRoaXMucGFzc3dvcmQgPSBvcHRzLnBhc3N3b3JkIHx8ICh0aGlzLmxvZ2luID09ICdiYXJuJyA/ICdiYXJuJyA6ICcnKTtcblx0dGhpcy5sYW5ndWFnZSA9ICdydV9SVSc7XG5cdHRoaXMuaG9zdCA9IG9wdHMuaG9zdCB8fCAnJztcblx0dGhpcy5hdXRoZW50aWNhdGVkID0gZmFsc2U7XG5cdHRoaXMuZXZlbnRfc3Vic2NyaWJlcnMgPSBbXTtcblx0dGhpcy5sb2dvdXRfc3Vic2NyaWJlcnMgPSBbXTtcblx0dGhpcy5wb2xsaW5nID0gZmFsc2U7XG5cblx0dmFyIGNvbnRleHQgPSB0aGlzO1xuXG5cdGZ1bmN0aW9uIHBhcnNlU3RydWN0dXJlKG9iaikge1xuXHRcdHZhciByZXN1bHQgPSB7fTtcblx0XHRmb3IgKHZhciBpPTA7IGk8b2JqLmNoaWxkcmVuLmxlbmd0aDsgaSs9Mikge1xuXHRcdFx0dmFyIGtleSA9IG9iai5jaGlsZHJlbltpXS50ZXh0Q29udGVudCxcblx0XHRcdFx0dmFsdWUgPSBwYXJzZVZhbHVlKG9iai5jaGlsZHJlbltpKzFdKTtcblx0XHRcdHJlc3VsdFtrZXldID0gdmFsdWU7XG5cdFx0fVxuXHRcdHJldHVybiByZXN1bHQ7XG5cdH1cblxuXHRmdW5jdGlvbiBwYXJzZUFycmF5KG9iaikge1xuXHRcdHZhciByZXN1bHQgPSBbXTtcblx0XHRmb3IgKHZhciBpPTA7IGk8b2JqLmNoaWxkcmVuLmxlbmd0aDsgaSsrKSB7XG5cdFx0XHRyZXN1bHRbaV0gPSBwYXJzZVZhbHVlKG9iai5jaGlsZHJlbltpXSk7XG5cdFx0fVxuXHRcdHJldHVybiByZXN1bHQ7XG5cdH1cblxuXHRmdW5jdGlvbiBwYXJzZUV4Y2VwdGlvbihvYmopIHtcblx0XHRsZXQgcmVzdWx0ID0ge1xuXHRcdFx0RXhjZXB0aW9uVGV4dDogXCJcIixcblx0XHRcdEVycm9yQ29kZTogdW5kZWZpbmVkXG5cdFx0fTtcblx0XHRpZiAob2JqLmNoaWxkcmVuLmxlbmd0aCA8IDEpIHJldHVybiByZXN1bHQ7XG5cdFx0cmVzdWx0LkV4Y2VwdGlvblRleHQgPSBvYmouY2hpbGRyZW5bMF0udGV4dENvbnRlbnQ7XG5cdFx0aWYgKG9iai5jaGlsZHJlbi5sZW5ndGggPiAxKSByZXN1bHQuRXJyb3JDb2RlID0gcGFyc2VWYWx1ZShvYmouY2hpbGRyZW5bMV0pO1xuXHRcdHJldHVybiByZXN1bHQ7XG5cdH1cblxuXHRmdW5jdGlvbiBwYXJzZVRpbWUodmFsKSB7XG5cdFx0bGV0IGZvdW5kID0gdmFsLm1hdGNoKC8oXFxkXFxkXFxkXFxkKShcXGRcXGQpKFxcZFxcZClUKFxcZFxcZCkoXFxkXFxkKShcXGRcXGQpLyk7XG5cdFx0aWYgKGZvdW5kICE9IG51bGwpIHtcblx0XHRcdHJldHVybiBuZXcgRGF0ZShwYXJzZUludChmb3VuZFsxXSksIHBhcnNlSW50KGZvdW5kWzJdKS0xLCBwYXJzZUludChmb3VuZFszXSksIHBhcnNlSW50KGZvdW5kWzRdKSwgcGFyc2VJbnQoZm91bmRbNV0pLCBwYXJzZUludChmb3VuZFs2XSkpO1xuXHRcdH1cblx0XHRyZXR1cm4gbnVsbDtcblx0fVxuXHRmdW5jdGlvbiBwYXJzZURhdGUodmFsKSB7XG5cdFx0bGV0IGZvdW5kID0gdmFsLm1hdGNoKC8oXFxkXFxkXFxkXFxkKShcXGRcXGQpKFxcZFxcZCkvKTtcblx0XHRpZiAoZm91bmQgIT0gbnVsbCkge1xuXHRcdFx0cmV0dXJuIG5ldyBEYXRlKHBhcnNlSW50KGZvdW5kWzFdKSwgcGFyc2VJbnQoZm91bmRbMl0pLTEsIHBhcnNlSW50KGZvdW5kWzNdKSk7XG5cdFx0fVxuXHRcdHJldHVybiBudWxsO1xuXHR9XG5cblx0ZnVuY3Rpb24gcGFyc2VWYWx1ZShvYmopIHtcblx0XHRpZiAob2JqID09IG51bGwgfHwgbnVsbCA9PSBvYmouY2hpbGRyZW5bMF0pIHJldHVybiBudWxsO1xuXHRcdGxldCBjb250YWluZXIgPSBvYmouY2hpbGRyZW5bMF07XG5cdFx0bGV0IG5vZGVOYW1lID0gY29udGFpbmVyLm5vZGVOYW1lLnRvVXBwZXJDYXNlKCk7XG5cdFx0aWYgKG5vZGVOYW1lID09ICdTVFJVQ1RVUkUnKSByZXR1cm4gcGFyc2VTdHJ1Y3R1cmUoY29udGFpbmVyKTtcblx0XHRlbHNlIGlmIChub2RlTmFtZSA9PSAnQVJSQVknKSByZXR1cm4gcGFyc2VBcnJheShjb250YWluZXIpO1xuXHRcdGVsc2UgaWYgKG5vZGVOYW1lID09ICdVTCcgfHwgbm9kZU5hbWUgPT0gJ09QVElPTkFMJyB8fCBub2RlTmFtZSA9PSAnVScpIHJldHVybiBwYXJzZVZhbHVlKGNvbnRhaW5lcik7XG5cdFx0ZWxzZSBpZiAobm9kZU5hbWUgPT0gJ0lOVDY0X1QnIHx8IG5vZGVOYW1lID09ICdJTlQnKSByZXR1cm4gcGFyc2VJbnQoY29udGFpbmVyLnRleHRDb250ZW50KTtcblx0XHRlbHNlIGlmIChub2RlTmFtZSA9PSAnVElNRScpIHJldHVybiBwYXJzZVRpbWUoY29udGFpbmVyLmNoaWxkcmVuWzBdLnRleHRDb250ZW50KTtcblx0XHRlbHNlIGlmIChub2RlTmFtZSA9PSAnQURBVEUnKSByZXR1cm4gcGFyc2VEYXRlKGNvbnRhaW5lci5jaGlsZHJlblswXS50ZXh0Q29udGVudCk7XG5cdFx0ZWxzZSBpZiAobm9kZU5hbWUgPT0gJ0RFQ0lNQUwnKSByZXR1cm4gcGFyc2VGbG9hdChjb250YWluZXIudGV4dENvbnRlbnQpO1xuXHRcdGVsc2UgaWYgKG5vZGVOYW1lID09ICdET1VCTEUnKSByZXR1cm4gcGFyc2VGbG9hdChjb250YWluZXIudGV4dENvbnRlbnQpO1xuXHRcdGVsc2UgaWYgKG5vZGVOYW1lID09ICdFWENFUFRJT04nKSByZXR1cm4gcGFyc2VFeGNlcHRpb24oY29udGFpbmVyKTtcblx0XHRlbHNlIGlmIChub2RlTmFtZSA9PSAnVkFMVUUnKSByZXR1cm4gcGFyc2VWYWx1ZShjb250YWluZXIpO1xuXHRcdGVsc2UgcmV0dXJuIGNvbnRhaW5lci50ZXh0Q29udGVudDtcblx0XHRyZXR1cm4gbnVsbDtcblx0fVxuXG5cdGZ1bmN0aW9uIHBhcnNlQVhNTChkYXRhKSB7XG5cdFx0dmFyIGFwYWNrZXRfcmUgPSAvQVBhY2tldFxcKFxcZCsgLFwiXCIsXCJcIixcIlwiLFwiXCIsXCJcIixcXHtcIlJlc3BvbnNlVG9cIjp1bFxcKDAgXFwpXFx9LCguKilcXCkvO1xuXHRcdHZhciByZXN1bHQgPSBkYXRhLFxuXHRcdFx0JHhtbCA9IHBhcnNlWG1sKGRhdGEpO1xuXHRcdGlmIChkYXRhLnN0YXJ0c1dpdGgoJ0FQYWNrZXQnKSkge1xuXHRcdFx0cmVzdWx0ID0gcmVzdWx0LnJlcGxhY2UoYXBhY2tldF9yZSwgJyQxJyk7XG5cdFx0fVxuXG5cdFx0Ly8gdmFyIHZhbHVlcyA9ICR4bWwuc2VsZWN0KCdBUGFja2V0ID4gVmFsdWUnKTtcblx0XHRsZXQgdmFsdWVzID0gJHhtbC5ldmFsdWF0ZSggJy8vQVBhY2tldC9WYWx1ZScsICR4bWwsIG51bGwsIFhQYXRoUmVzdWx0Lk9SREVSRURfTk9ERV9TTkFQU0hPVF9UWVBFLCBudWxsKTtcblx0XHRpZiAoIXZhbHVlcyB8fCB2YWx1ZXMuc25hcHNob3RMZW5ndGggPT0gMCkge1xuXHRcdFx0aWYgKCR4bWwuY2hpbGRyZW4ubGVuZ3RoIDwgMSkgcmV0dXJuO1xuXHRcdFx0cmV0dXJuIHBhcnNlVmFsdWUoJHhtbC5jaGlsZHJlblswXSk7XG5cdFx0fVxuXHRcdGxldCBjb250ZW50ID0gdmFsdWVzLnNuYXBzaG90SXRlbSh2YWx1ZXMuc25hcHNob3RMZW5ndGgtMSk7XG5cdFx0cmV0dXJuIHBhcnNlVmFsdWUoY29udGVudCk7XG5cdH1cblxuXHRmdW5jdGlvbiBfcGFyc2UoZGF0YSwgeGhyKSB7XG5cdFx0dmFyIGN0ID0geGhyLmdldFJlc3BvbnNlSGVhZGVyKFwiY29udGVudC10eXBlXCIpIHx8IFwiXCI7XG5cdFx0dmFyIHJlcyA9IGRhdGE7XG5cdFx0aWYgKGN0LmluZGV4T2YoJ3RleHQveG1sJykgPiAtMSkge1xuXHRcdFx0cmVzID0gcGFyc2VBWE1MKGRhdGEpO1xuXHRcdH1cblx0XHRlbHNlIGlmIChjdC5pbmRleE9mKCdhcHBsaWNhdGlvbi9qc29uJykgPiAtMSkge1xuXHRcdFx0cmVzID0gSlNPTi5wYXJzZShkYXRhKTtcblx0XHR9XG5cdFx0cmV0dXJuIHJlcztcblx0fVxuXHRcblx0dGhpcy5hdXRoID0gZnVuY3Rpb24ob3B0cykge1xuXHRcdGxldCBsb2dpbiA9IG9wdHMubG9naW4gfHwgY29udGV4dC5sb2dpbixcblx0XHRcdHBhc3N3b3JkID0gb3B0cy5wYXNzd29yZCB8fCBjb250ZXh0LnBhc3N3b3JkLFxuXHRcdFx0bGFuZyA9IG9wdHMubGFuZyB8fCAncnVfUlUnLFxuXHRcdFx0c2tpbiA9IG9wdHMuc2tpbiB8fCAnbWF0ZXJpbycsXG5cdFx0XHR0aW1lX3pvbmUgPSBvcHRzLnRpbWVfem9uZSB8fCBuZXcgRGF0ZSgpLmdldFRpbWV6b25lT2Zmc2V0KCk7XG5cdFx0cmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcblx0XHRcdGxldCByZXF1ZXN0ID0gYWpheCh7XG5cdFx0XHRcdHVybDogY29udGV4dC5ob3N0ICsgJy9hdXRoL2xvZ2luJywgXG5cdFx0XHRcdG1ldGhvZDogJ1BPU1QnLFxuXHRcdFx0XHRkYXRhVHlwZTogJ3RleHQnLFxuXHRcdFx0XHRkYXRhOiBgTG9naW49czoke2xvZ2lufSZQYXNzd29yZD1zOiR7cGFzc3dvcmR9Jkxhbmc9czoke2xhbmd9JlNraW49czoke3NraW59JlRpbWVab25lPWk6JHt0aW1lX3pvbmV9JmBcblx0XHRcdH0pXG5cdFx0XHQudGhlbigocmVzcCkgPT4ge1xuXHRcdFx0XHR2YXIgcmVzID0gcGFyc2VBWE1MKHJlc3ApO1xuXHRcdFx0XHRjb250ZXh0LmF1dGhlbnRpY2F0ZWQgPSB0cnVlO1xuXHRcdFx0XHRjb250ZXh0LmxvZ2luID0gbG9naW47XG5cdFx0XHRcdGNvbnRleHQucGFzc3dvcmQgPSBwYXNzd29yZDtcblx0XHRcdFx0aWYgKHR5cGVvZiByZXMgPT0gJ3N0cmluZycpIGNvbnRleHQubGFuZ3VhZ2UgPSByZXM7XG5cdFx0XHRcdGlmICh0eXBlb2Ygb3B0cy5zdWNjZXNzICE9ICd1bmRlZmluZWQnKSBvcHRzLnN1Y2Nlc3MoY29udGV4dC5sYW5ndWFnZSwgcmVzKTtcblx0XHRcdFx0cmVzb2x2ZShjb250ZXh0Lmxhbmd1YWdlLCByZXMpO1xuXHRcdFx0fSlcblx0XHRcdC5jYXRjaCgocmVzcCkgPT4ge1xuXHRcdFx0XHR2YXIgcmVzcG9uc2VFcnJvciA9IHBhcnNlQVhNTChyZXNwKTtcblx0XHRcdFx0Y29uc29sZS5lcnJvcihyZXNwb25zZUVycm9yLkVycm9yQ29kZSwgcmVzcG9uc2VFcnJvci5FeGNlcHRpb25UZXh0KTtcblx0XHRcdFx0aWYgKHR5cGVvZiBvcHRzLmVycm9yICE9ICd1bmRlZmluZWQnKSBvcHRzLmVycm9yKHJlc3BvbnNlRXJyb3IpO1xuXHRcdFx0XHRyZWplY3QocmVzcG9uc2VFcnJvcik7XG5cdFx0XHR9KTtcblx0XHR9KTtcblx0fVxuXG5cdHRoaXMucmVxdWVzdCA9IGZ1bmN0aW9uIChvcHRzKSB7XG5cdFx0cmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcblx0XHRcdGxldCBkYXRhID0gb3B0cy5kYXRhIHx8IFwiZHVtbXk9bm9uZTomXCI7XG5cdFx0XHRhamF4KClcblx0XHRcdFx0LnBvc3QoY29udGV4dC5ob3N0ICsgb3B0cy51cmwsIGRhdGEpXG5cdFx0XHRcdC50aGVuKChyZXNwLCB4aHIpID0+IHtcblx0XHRcdFx0XHRsZXQgcmVzID0gX3BhcnNlKHJlc3AsIHhocik7XG5cdFx0XHRcdFx0aWYgKHR5cGVvZiBvcHRzLnN1Y2Nlc3MgIT0gJ3VuZGVmaW5lZCcpIG9wdHMuc3VjY2VzcyhyZXMpO1xuXHRcdFx0XHRcdHJlc29sdmUocmVzKTtcblx0XHRcdFx0fSlcblx0XHRcdFx0LmNhdGNoKChyZXNwLCB4aHIpID0+IHtcblx0XHRcdFx0XHR2YXIgcmVzcG9uc2VFcnJvciA9IHBhcnNlQVhNTChyZXNwKTtcblx0XHRcdFx0XHRjb25zb2xlLmVycm9yKHJlc3BvbnNlRXJyb3IuRXJyb3JDb2RlLCByZXNwb25zZUVycm9yLkV4Y2VwdGlvblRleHQpO1xuXHRcdFx0XHRcdGlmIChyZXNwb25zZUVycm9yLkVycm9yQ29kZSA9PSAxMDEgfHwgcmVzcG9uc2VFcnJvci5FcnJvckNvZGUgPT0gMTAwKSB7XG5cdFx0XHRcdFx0XHRjb250ZXh0LmF1dGgoe1xuXHRcdFx0XHRcdFx0XHRzdWNjZXNzOiAoKSA9PiB7XG5cdFx0XHRcdFx0XHRcdFx0Y29udGV4dC5yZXF1ZXN0KG9wdHMpXG5cdFx0XHRcdFx0XHRcdFx0XHQudGhlbigocmVzKSA9PiB7IHJlc29sdmUocmVzKTsgfSlcblx0XHRcdFx0XHRcdFx0XHRcdC5jYXRjaCgoZXJyKSA9PiB7IHJlamVjdChlcnIpOyB9KVxuXHRcdFx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdFx0XHRlcnJvcjogKCkgPT4ge1xuXHRcdFx0XHRcdFx0XHRcdGNvbnRleHQuZmlyZUxvZ291dCgpO1xuXHRcdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0ZWxzZSB7XG5cdFx0XHRcdFx0XHRpZiAodHlwZW9mIG9wdHMuZXJyb3IgICE9ICd1bmRlZmluZWQnKSBvcHRzLmVycm9yKHJlc3BvbnNlRXJyb3IpO1xuXHRcdFx0XHRcdFx0cmVqZWN0KHJlc3BvbnNlRXJyb3IpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSk7XG5cdFx0fSk7XG5cdH1cblxuXHR0aGlzLm9uTG9nb3V0ID0gZnVuY3Rpb24gKGNhbGxlcikgIHtcblx0XHRpZiAodHlwZW9mIGNhbGxlciAhPSAnZnVuY3Rpb24nKSB0aHJvdyBcIkVmZmlQcm90b2NvbC5vbkxvZ291dDogYXJndW1lbnQgaXMgbm90IGEgZnVuY3Rpb24uIFwiO1xuXHRcdHRoaXMubG9nb3V0X3N1YnNjcmliZXJzLnB1c2goY2FsbGVyKTtcblx0fVxuXHR0aGlzLmZpcmVMb2dvdXQgPSBmdW5jdGlvbiAoKSB7XG5cdFx0Zm9yIChsZXQgaT0wOyBpPD10aGlzLmxvZ291dF9zdWJzY3JpYmVycy5sZW5ndGg7IGkrKykge1xuXHRcdFx0bGV0IGNhbGxlciA9IHRoaXMubG9nb3V0X3N1YnNjcmliZXJzW2ldO1xuXHRcdFx0Y2FsbGVyKHRoaXMpO1xuXHRcdH1cblx0fVxuXG5cdHRoaXMucG9sbEV2ZW50cyA9IGZ1bmN0aW9uIChjbnQpIHtcblx0XHRjbnQgPSBjbnQgfHwgMDtcblx0XHR0aGlzLnBvbGxpbmcgPSB0cnVlO1xuXHRcdGFqYXgoY29udGV4dC5ob3N0ICsgJy9zcnYvV1dXL1dXV1dvcmtlci9HZXRFdmVudCcpXG5cdFx0XHQuZ2V0KClcblx0XHRcdC50aGVuKGZ1bmN0aW9uIChkYXRhKSB7XG5cdFx0XHRcdGNvbnRleHQuZXZlbnRzVGltZW91dCA9IHNldFRpbWVvdXQoKCkgPT4ge2NvbnRleHQucG9sbEV2ZW50cygxKX0sIDEpO1xuXHRcdFx0XHRsZXQgcmVzID0gcGFyc2VBWE1MKGRhdGEpO1xuXHRcdFx0XHQvLyBjb25zb2xlLmxvZygnZXZlbnQgc3VjY2VzczonLCByZXMpO1xuXHRcdFx0XHRmb3IgKGxldCBlPTA7IGU8cmVzLmxlbmd0aDsgZSsrKSB7XG5cdFx0XHRcdFx0bGV0IGV2ZW50ID0gIHJlc1tlXTtcblx0XHRcdFx0XHQvLyBjb25zb2xlLmxvZyhjb250ZXh0LmV2ZW50X3N1YnNjcmliZXJzKVxuXHRcdFx0XHRcdGZvciAobGV0IGk9MDsgaTxjb250ZXh0LmV2ZW50X3N1YnNjcmliZXJzLmxlbmd0aDsgaSsrKSB7XG5cdFx0XHRcdFx0XHRsZXQgc3Vic2NyID0gY29udGV4dC5ldmVudF9zdWJzY3JpYmVyc1tpXTtcblx0XHRcdFx0XHRcdGlmICghc3Vic2NyLnR5cGUgfHwgc3Vic2NyLnR5cGUgPT0gJyonIHx8IHN1YnNjci50eXBlID09IGV2ZW50LlR5cGUpIHtcblx0XHRcdFx0XHRcdFx0c3Vic2NyLmNhbGxiYWNrKGV2ZW50KTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9XG5cdFx0XHRcdH1cblx0XHRcdH0pXG5cdFx0XHQuY2F0Y2goZnVuY3Rpb24gKHJlc3AsIHhocikge1xuXHRcdFx0XHR2YXIgcmVzcG9uc2VFcnJvciA9IHBhcnNlQVhNTChyZXNwKTtcblx0XHRcdFx0aWYgKCFyZXNwb25zZUVycm9yKSB7XG5cdFx0XHRcdFx0aWYgKGNudDw0KSB7XG5cdFx0XHRcdFx0XHRjb250ZXh0LmV2ZW50c1RpbWVvdXQgPSBzZXRUaW1lb3V0KCgpID0+IHtjb250ZXh0LnBvbGxFdmVudHMoY250KzEpfSwgMjAwMCk7XG5cdFx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRcdGNsZWFyVGltZW91dChjb250ZXh0LmV2ZW50c1RpbWVvdXQpO1xuXHRcdFx0XHRcdFx0Y29uc29sZS5lcnJvcihcIkNvbm5lY3Rpb24gdG8gc2VydmVyIGxvc3QuIFwiKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0XHR9XG5cdFx0XHRcdGNvbnNvbGUuZXJyb3IoY250LCByZXNwb25zZUVycm9yLkVycm9yQ29kZSwgcmVzcG9uc2VFcnJvci5FeGNlcHRpb25UZXh0KTtcblx0XHRcdFx0aWYgKGNudDw0ICYmIChyZXNwb25zZUVycm9yLkVycm9yQ29kZSA9PSAxMDEgfHwgcmVzcG9uc2VFcnJvci5FcnJvckNvZGUgPT0gMTAwKSkge1xuXHRcdFx0XHRcdGNvbnRleHQuYXV0aCh7XG5cdFx0XHRcdFx0XHRzdWNjZXNzOiAoKSA9PiB7XG5cdFx0XHRcdFx0XHRcdGNvbnNvbGUubG9nKCdhdXRoIGRvbmUnKVxuXHRcdFx0XHRcdFx0XHRjb250ZXh0LnBvbGxFdmVudHMoMSk7XG5cdFx0XHRcdFx0XHR9XHRcblx0XHRcdFx0XHR9KTtcblx0XHRcdFx0fVxuXHRcdFx0XHRlbHNlIHtcblx0XHRcdFx0XHRjbGVhclRpbWVvdXQoY29udGV4dC5ldmVudHNUaW1lb3V0KTtcblx0XHRcdFx0XHRjb25zb2xlLmVycm9yKFwiQ29ubmVjdGlvbiB0byBzZXJ2ZXIgbG9zdC4gXCIpO1xuXHRcdFx0XHRcdHRocm93IHJlc3BvbnNlRXJyb3I7XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXHR9XG5cblx0dGhpcy5zdWJzY3JpYmUgPSBmdW5jdGlvbiAoY3R4LCBldmVudF9uYW1lLCBjYWxsYmFjaykge1xuXHRcdGlmICh0eXBlb2YgKGV2ZW50X25hbWUpID09ICdmdW5jdGlvbicpIHtcblx0XHRcdGV2ZW50X25hbWUgPSAnKic7XG5cdFx0XHRjYWxsYmFjayA9IGV2ZW50X25hbWU7XG5cdFx0fVxuXHRcdGNvbnRleHQuZXZlbnRfc3Vic2NyaWJlcnMucHVzaCh7Y3R4OiBjdHgsIHR5cGU6IGV2ZW50X25hbWUsIGNhbGxiYWNrOiBjYWxsYmFja30pO1xuXHRcdGlmICghY29udGV4dC5wb2xsaW5nKSB7XG5cdFx0XHRpZiAodHlwZW9mIFN1YnNjcmliZU9uRXZlbnQgIT09ICd1bmRlZmluZWQnKSB7XG5cdFx0XHRcdC8vIGNvbnNvbGUubG9nKCdjdXJyZW50IHdpbmRvdyBTdWJzY3JpYmVPbkV2ZW50Jyk7XG5cdFx0XHRcdFN1YnNjcmliZU9uRXZlbnQoZXZlbnRfbmFtZSwgXCJkb2N1bWVudFwiLCBjb250ZXh0LnByb2Nlc3NQcm90b3R5cGVFdmVudCwgY29udGV4dCk7XG5cdFx0XHRcdGNvbnRleHQucG9sbGluZyA9IHRydWU7XG5cdFx0XHR9XG5cdFx0XHRlbHNlIGlmICh0eXBlb2Ygd2luZG93Lm9wZW5lciAhPT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93Lm9wZW5lciAmJiB0eXBlb2Ygd2luZG93Lm9wZW5lci5TdWJzY3JpYmVPbkV2ZW50ICE9PSAndW5kZWZpbmVkJykge1xuXHRcdFx0XHQvLyBjb25zb2xlLmxvZygncGFyZW50IHdpbmRvdyBTdWJzY3JpYmVPbkV2ZW50Jyk7XG5cdFx0XHRcdHdpbmRvdy5vcGVuZXIuU3Vic2NyaWJlT25FdmVudChldmVudF9uYW1lLCBcImRvY3VtZW50XCIsIGNvbnRleHQucHJvY2Vzc1Byb3RvdHlwZUV2ZW50LCBjb250ZXh0KTtcblx0XHRcdFx0Y29udGV4dC5wb2xsaW5nID0gdHJ1ZTtcblx0XHRcdH1cblx0XHRcdGVsc2Uge1xuXHRcdFx0XHQvLyBjb25zb2xlLmxvZygnc2VwYXJhdGUgd2luZG93Jyk7XG5cdFx0XHRcdGNvbnRleHQuZXZlbnRzVGltZW91dCA9IHNldFRpbWVvdXQoKCkgPT4ge2NvbnRleHQucG9sbEV2ZW50cygxKX0sIDEpO1xuXHRcdFx0fVxuXHRcdH1cblx0fVxuXHR0aGlzLnVuc3Vic2NyaWJlID0gZnVuY3Rpb24gKGN0eCwgZXZlbnRfbmFtZSkge1xuXHRcdC8vIGNvbnNvbGUubG9nKCd1bnN1YnNjcmliZScsIGN0eCwgZXZlbnRfbmFtZSlcblx0XHRmb3IgKGxldCBpPTA7IGk8dGhpcy5ldmVudF9zdWJzY3JpYmVycy5sZW5ndGg7IGkrKykge1xuXHRcdFx0bGV0IHN1YnNjciA9IHRoaXMuZXZlbnRfc3Vic2NyaWJlcnNbaV07XG5cdFx0XHRpZiAoc3Vic2NyLmN0eCA9PSBjdHggJiYgKCFldmVudF9uYW1lIHx8IGV2ZW50X25hbWUgPT0gJyonIHx8IGV2ZW50X25hbWUgPT0gc3Vic2NyLnR5cGUpKSB7XG5cdFx0XHRcdC8vIGNvbnNvbGUubG9nKCcgIGRyb3Agc3Vic2NyaWJlcicsIHN1YnNjcik7XG5cdFx0XHRcdHRoaXMuZXZlbnRfc3Vic2NyaWJlcnMuc3BsaWNlKGksIDEpO1xuXHRcdFx0fVxuXHRcdH1cblx0XHQvLyBpZiAoVW5zdWJzY3JpYmVGcm9tRXZlbnQpIHtcblx0XHQvLyBcdFVuc3Vic2NyaWJlRnJvbUV2ZW50KGV2ZW50X25hbWUsIFwiZG9jdW1lbnRcIiwgdGhpcy5wcm9jZXNzUHJvdG90eXBlRXZlbnQsIHRoaXMpXG5cdFx0Ly8gfVxuXHR9XG5cblx0dGhpcy5ub3JtYWxpemVQcm90b3R5cGVPYmplY3QgPSBmdW5jdGlvbihvYmopIHtcblx0XHRsZXQgcmVzID0ge31cblx0XHRmb3IgKGxldCBrIGluIG9iaikge1xuXHRcdFx0aWYgKHR5cGVvZiBvYmpba10gPT0gJ29iamVjdCcpIHtcblx0XHRcdFx0aWYgKCdvVmFsdWVfJyBpbiBvYmpba10pIHtcblx0XHRcdFx0XHRyZXNba10gPSBvYmpba10ub1ZhbHVlXztcblx0XHRcdFx0XHRpZiAodHlwZW9mIG9ialtrXS5vVmFsdWVfID09ICdvYmplY3QnICYmICd2YWx1ZV8nIGluIG9ialtrXS5vVmFsdWVfKSByZXNba10gPSBvYmpba10ub1ZhbHVlXy52YWx1ZV87XG5cdFx0XHRcdH1cblx0XHRcdFx0Ly8gZWxzZSBpZiAoJ3ZhbHVlXycgaW4gb2JqW2tdKSByZXNba10gPSBvYmpba10udmFsdWVfXG5cdFx0XHRcdGVsc2UgcmVzW2tdID0gbnVsbDtcblx0XHRcdH1cblx0XHRcdGVsc2UgcmVzW2tdID0gb2JqW2tdO1xuXHRcdH1cblx0XHRyZXR1cm4gcmVzO1xuXHR9XG5cdHRoaXMucHJvY2Vzc1Byb3RvdHlwZUV2ZW50ID0gZnVuY3Rpb24oZSkge1xuXHRcdGxldCBldmVudF9kYXRhID0gdGhpcy5ub3JtYWxpemVQcm90b3R5cGVPYmplY3QoZS5vRXZlbnREYXRhXyksXG5cdFx0XHRldmVudCA9IHtUeXBlOiBlLnNFdmVudE5hbWVfLCBEYXRhOiBldmVudF9kYXRhfTtcblx0XHRmb3IgKGxldCBpPTA7IGk8Y29udGV4dC5ldmVudF9zdWJzY3JpYmVycy5sZW5ndGg7IGkrKykge1xuXHRcdFx0bGV0IHN1YnNjciA9IGNvbnRleHQuZXZlbnRfc3Vic2NyaWJlcnNbaV07XG5cdFx0XHRpZiAoIXN1YnNjci50eXBlIHx8IHN1YnNjci50eXBlID09ICcqJyB8fCBzdWJzY3IudHlwZS50b1VwcGVyQ2FzZSgpID09IGUuc0V2ZW50TmFtZV8pIHtcblx0XHRcdFx0c3Vic2NyLmNhbGxiYWNrKGV2ZW50KTtcblx0XHRcdH1cblx0XHR9XG5cdH1cblxuXHRmdW5jdGlvbiBkZWxldGVfY29va2llKG5hbWUpIHtcblx0XHRkb2N1bWVudC5jb29raWUgPSBuYW1lICsnPTsgUGF0aD0vOyBFeHBpcmVzPVRodSwgMDEgSmFuIDE5NzAgMDA6MDA6MDEgR01UOyc7XG5cdH1cblxuXHR0aGlzLmxvZ291dCA9IGZ1bmN0aW9uICgpIHtcblx0XHRjbGVhclRpbWVvdXQoY29udGV4dC5ldmVudHNUaW1lb3V0KTtcblx0XHRkZWxldGVfY29va2llKCdfMTAyNF9zaWQnKTtcblx0XHR0aGlzLmF1dGhlbnRpY2F0ZWQgPSBmYWxzZTtcblx0fVxufVxuXG5mdW5jdGlvbiBwYWRkeShuLCBwLCBjKSB7XG5cdHZhciBwYWRfY2hhciA9IHR5cGVvZiBjICE9PSAndW5kZWZpbmVkJyA/IGMgOiAnMCc7XG5cdHZhciBwYWQgPSBuZXcgQXJyYXkoMSArIHApLmpvaW4ocGFkX2NoYXIpO1xuXHRyZXR1cm4gKHBhZCArIG4pLnNsaWNlKC1wYWQubGVuZ3RoKTtcbn1cbmZ1bmN0aW9uIGZvcm1hdF9lZmZpX2RhdGUoZGF0ZSkge1xuXHRpZiAoZGF0ZSA9PSBudWxsKSByZXR1cm4gJ25vdC1hLWRhdGUnO1xuXHRyZXR1cm4gcGFkZHkoZGF0ZS5nZXRGdWxsWWVhcigpLCA0KSArIHBhZGR5KGRhdGUuZ2V0TW9udGgoKSsxLCAyKSArIHBhZGR5KGRhdGUuZ2V0RGF0ZSgpLCAyKTtcbn1cbmZ1bmN0aW9uIGZvcm1hdF9lZmZpX3RpbWUoZGF0ZSkge1xuXHRpZiAoZGF0ZSA9PSBudWxsKSByZXR1cm4gJ25vdC1hLWRhdGUtdGltZSc7XG5cdHJldHVybiBwYWRkeShkYXRlLmdldEZ1bGxZZWFyKCksIDQpICsgcGFkZHkoZGF0ZS5nZXRNb250aCgpKzEsIDIpICsgcGFkZHkoZGF0ZS5nZXREYXRlKCksIDIpICsgJ1QnICsgXG5cdFx0cGFkZHkoZGF0ZS5nZXRIb3VycygpLCAyKSArIHBhZGR5KGRhdGUuZ2V0TWludXRlcygpLCAyKSArIHBhZGR5KGRhdGUuZ2V0U2Vjb25kcygpLCAyKTtcbn1cblxuZnVuY3Rpb24gZW5jb2RlQVVSTENvbXBvbmVudChvYmopIHtcblx0dmFyIHNlcmlhbGl6ZWQgPSAnJztcblx0aWYgKHR5cGVvZiBvYmogPT0gJ3VuZGVmaW5lZCcgfHwgb2JqID09IG51bGwpIHJldHVybiAnbm9uZTomJ1xuXHRlbHNlIGlmIChvYmoudHlwZSA9PSAnYXJyYXknIHx8IEFycmF5LmlzQXJyYXkob2JqKSkge1xuXHRcdGxldCBzID0gJyc7XG5cdFx0Zm9yIChsZXQgaT0wOyBpPG9iai5sZW5ndGg7IGkrKykge1xuXHRcdFx0bGV0IG8gPSBvYmpbaV07XG5cdFx0XHRzICs9IGVuY29kZUFVUkxDb21wb25lbnQobyk7XG5cdFx0fVxuXHRcdHNlcmlhbGl6ZWQgKz0gJ1ZhbHVlOkFycmF5OicgKyBzICsgJyYmJztcblx0fVxuXHRlbHNlIGlmIChvYmoudHlwZSA9PSAnb3B0aW9uYWxJbnQnKSBzZXJpYWxpemVkID0gJ29wdGlvbmFsOmk6JyArIG9iai52YWx1ZSArICcmJic7XG5cdGVsc2UgaWYgKG9iai50eXBlID09ICdmbG9hdCcgfHwgdHlwZW9mIG9iai52YWx1ZSA9PSAnZmxvYXQnKSBzZXJpYWxpemVkID0gJ2Q6JyArIG9iai52YWx1ZSArICcmJztcblx0ZWxzZSBpZiAob2JqLnR5cGUgPT0gJ2ludCcgfHwgdHlwZW9mIG9iai52YWx1ZSA9PSAnbnVtYmVyJykgc2VyaWFsaXplZCA9ICdpOicgKyBvYmoudmFsdWUgKyAnJic7XG5cdGVsc2UgaWYgKG9iai50eXBlID09ICdkYXRlJyB8fCBvYmoudHlwZSA9PSAnQURhdGUnKSBzZXJpYWxpemVkID0gJ0FEYXRlOnM6JyArIGZvcm1hdF9lZmZpX2RhdGUob2JqLnZhbHVlKSArICcmJic7XG5cdGVsc2UgaWYgKG9iai50eXBlID09ICdkYXRldGltZScgfHwgb2JqLnR5cGUgPT0gJ1RpbWUnIHx8IG9iai52YWx1ZSBpbnN0YW5jZW9mIERhdGUpIHNlcmlhbGl6ZWQgPSAnVGltZTpzOicgKyBmb3JtYXRfZWZmaV90aW1lKG9iai52YWx1ZSkgKyAnJiYnO1xuXHRlbHNlIGlmIChvYmoudHlwZSA9PSAnY2hlY2tib3gnKSBzZXJpYWxpemVkID0gJ3M6JyArIG9iai52YWx1ZSArICcmJztcblx0ZWxzZSBpZiAob2JqLnR5cGUgPT0gJ29wdGlvbmFsU3RyaW5nJykgc2VyaWFsaXplZCA9ICdvcHRpb25hbDpzOicgKyBvYmoudmFsdWUucmVwbGFjZSgvIC9nLCAnXFxcXHcnKSArICcmJic7XG5cdGVsc2UgaWYgKG9iai50eXBlID09ICdiaW5hcnknKSBzZXJpYWxpemVkID0gJ2I6JyArIEVuY29kZXIuQmFzZTY0RW5jb2RlKEVuY29kZXIuVVRGOEVuY29kZShvYmoudmFsdWUpKSArICcmJztcblx0ZWxzZSB7XG5cdFx0bGV0IHYgPSAoKHR5cGVvZiBvYmoudmFsdWUgPT0gJ3VuZGVmaW5lZCcpID8gb2JqIDogb2JqLnZhbHVlKTtcblx0XHRzZXJpYWxpemVkID0gJ3M6JyArIHYucmVwbGFjZSgvIC9nLCAnXFxcXHcnKSArICcmJztcblx0fVxuXHRyZXR1cm4gc2VyaWFsaXplZDtcbn1cblxuZnVuY3Rpb24gZW5jb2RlUGxhaW4ob2JqKSB7XG5cdGxldCBzZXJpYWxpemVkID0gJyc7XG5cdGlmICh0eXBlb2Ygb2JqID09ICd1bmRlZmluZWQnIHx8IG9iaiA9PSBudWxsKSByZXR1cm4gJ25vbmU6Jidcblx0ZWxzZSBpZiAob2JqLnR5cGUgPT0gJ29wdGlvbmFsSW50Jykgc2VyaWFsaXplZCA9ICdvcHRpb25hbDppOicgKyBvYmoudmFsdWUgKyAnJiYnO1xuXHRlbHNlIGlmIChvYmoudHlwZSA9PSAnZmxvYXQnIHx8IHR5cGVvZiBvYmoudmFsdWUgPT0gJ2Zsb2F0Jykgc2VyaWFsaXplZCA9ICdkOicgKyBvYmoudmFsdWUgKyAnJic7XG5cdGVsc2UgaWYgKG9iai50eXBlID09ICdpbnQnIHx8IHR5cGVvZiBvYmoudmFsdWUgPT0gJ251bWJlcicpIHNlcmlhbGl6ZWQgPSAnaTonICsgb2JqLnZhbHVlICsgJyYnO1xuXHRlbHNlIGlmIChvYmoudHlwZSA9PSAnZGF0ZScgfHwgb2JqLnR5cGUgPT0gJ0FEYXRlJykgc2VyaWFsaXplZCA9ICdBRGF0ZTpzOicgKyBmb3JtYXRfZWZmaV9kYXRlKG9iai52YWx1ZSkgKyAnJiYnO1xuXHRlbHNlIGlmIChvYmoudHlwZSA9PSAnZGF0ZXRpbWUnIHx8IG9iai50eXBlID09ICdUaW1lJyB8fCBvYmoudmFsdWUgaW5zdGFuY2VvZiBEYXRlKSBzZXJpYWxpemVkID0gJ1RpbWU6czonICsgZm9ybWF0X2VmZmlfdGltZShvYmoudmFsdWUpICsgJyYmJztcblx0ZWxzZSBpZiAob2JqLnR5cGUgPT0gJ2NoZWNrYm94Jykgc2VyaWFsaXplZCA9ICdzOicgKyBvYmoudmFsdWUgKyAnJic7XG5cdGVsc2UgaWYgKG9iai50eXBlID09ICdvcHRpb25hbFN0cmluZycpIHNlcmlhbGl6ZWQgPSAnb3B0aW9uYWw6czonICsgb2JqLnZhbHVlLnJlcGxhY2UoLyAvZywgJ1xcXFx3JykgKyAnJiYnO1xuXHRlbHNlIGlmIChvYmoudHlwZSA9PSAnYmluYXJ5Jykgc2VyaWFsaXplZCA9ICdiOicgKyBFbmNvZGVyLkJhc2U2NEVuY29kZShvYmoudmFsdWUpICsgJyYnO1xuXHRlbHNlIHtcblx0XHRsZXQgdiA9ICgodHlwZW9mIG9iai52YWx1ZSA9PSAndW5kZWZpbmVkJykgPyBvYmogOiBvYmoudmFsdWUpO1xuXHRcdHNlcmlhbGl6ZWQgPSAnczonICsgdi5yZXBsYWNlKC8gL2csICdcXFxcdycpICsgJyYnO1xuXHR9XG5cdHJldHVybiBzZXJpYWxpemVkO1xufVxuZnVuY3Rpb24gZW5jb2RlQmxvYihmaWxlLCBjYWxsYmFjaykge1xuXHR2YXIgcmVhZGVyID0gbmV3IEZpbGVSZWFkZXIoKTtcblxuXHRyZWFkZXIub25sb2FkID0gZnVuY3Rpb24ocmVhZGVyRXZ0KSB7XG5cdFx0Y29uc29sZS5sb2coJ2xvYWRlZCBmaWxlIHNpemU9JyArIHJlYWRlckV2dC50YXJnZXQucmVzdWx0Lmxlbmd0aCk7XG5cdFx0bGV0IHNlcmlhbGl6ZWQgPSAnYjonICsgRW5jb2Rlci5CYXNlNjRFbmNvZGUocmVhZGVyRXZ0LnRhcmdldC5yZXN1bHQpICsgJyYnO1xuXHRcdGNhbGxiYWNrKHNlcmlhbGl6ZWQpO1xuXHR9O1xuXG5cdHJlYWRlci5yZWFkQXNCaW5hcnlTdHJpbmcoZmlsZSk7XG59XG5mdW5jdGlvbiBlbmNvZGVCbG9iRmlsZShmaWxlLCBjYWxsYmFjaykge1xuXHRlbmNvZGVCbG9iKGZpbGUsIGZ1bmN0aW9uKHNlcmlhbGl6ZWRfYmxvYikge1xuXHRcdGxldCBzZXJpYWxpemVkID0gJ0Jsb2JGaWxlOicgKyBcblx0XHRcdGVuY29kZUFVUkxDb21wb25lbnQoe3ZhbHVlOiBmaWxlLm5hbWUsIHR5cGU6ICdzdHJpbmcnfSkgK1xuXHRcdFx0ZW5jb2RlQVVSTENvbXBvbmVudCh7dmFsdWU6IGZpbGUudHlwZSwgdHlwZTogJ3N0cmluZyd9KSArXG5cdFx0XHRzZXJpYWxpemVkX2Jsb2IgK1xuXHRcdFx0ZW5jb2RlQVVSTENvbXBvbmVudChudWxsKSArXG5cdFx0XHRlbmNvZGVBVVJMQ29tcG9uZW50KHt2YWx1ZTogZmlsZS5sYXN0TW9kaWZpZWREYXRlLCB0eXBlOiAnVGltZSd9KSArIFxuXHRcdFx0JyYnO1xuXHRcdGNhbGxiYWNrKHNlcmlhbGl6ZWQpO1xuXHR9KTtcbn1cblxuZnVuY3Rpb24gcmVkdWNlU2VyaWFsaXphdGlvbkFycmF5KHNlcmlhbGl6ZWQsIGFycmF5LCBpLCBjYWxsYmFjaykge1xuXHRpZiAoYXJyYXkubGVuZ3RoID09IDApIHtcblx0XHRjYWxsYmFjayhzZXJpYWxpemVkKTtcblx0XHRyZXR1cm47XG5cdH1cblx0bGV0IG9iaiA9IGFycmF5LnNwbGljZSgwLCAxKTtcblx0ZW5jb2RlQVVSTENvbXBvbmVudEFzeW5jKHNlcmlhbGl6ZWQsIG9iaiwgZnVuY3Rpb24odikge1xuXHRcdHJlZHVjZVNlcmlhbGl6YXRpb25BcnJheSh2LCBhcnJheSwgKytpLCBjYWxsYmFjayk7XG5cdH0pO1xufVxuZnVuY3Rpb24gcmVkdWNlU2VyaWFsaXphdGlvblN0cnVjdHVyZShzZXJpYWxpemVkLCBhcnJheSwgb2JqZWN0LCBpLCBjYWxsYmFjaykge1xuXHRpZiAoYXJyYXkubGVuZ3RoID09IDApIHtcblx0XHRjYWxsYmFjayhzZXJpYWxpemVkKTtcblx0XHRyZXR1cm47XG5cdH1cblx0bGV0IGtleSA9IGFycmF5LnNwbGljZSgwLCAxKTtcblx0bGV0IG9iaiA9IG9iamVjdFtrZXldO1xuXHRsZXQgcyA9IHNlcmlhbGl6ZWQgKyBrZXkgKyAnPSc7XG5cdGVuY29kZUFVUkxDb21wb25lbnRBc3luYyhzLCBvYmosIGZ1bmN0aW9uICh2KSB7XG5cdFx0cmVkdWNlU2VyaWFsaXphdGlvblN0cnVjdHVyZSh2LCBhcnJheSwgb2JqZWN0LCArK2ksIGNhbGxiYWNrKTtcblx0fSk7XG59XG5cbmZ1bmN0aW9uIGVuY29kZUFVUkxDb21wb25lbnRBc3luYyhzZXJpYWxpemVkLCBvYmosIGNhbGxiYWNrKSB7XG5cdC8vIGNvbnNvbGUubG9nKG9iaik7XG5cdGlmIChvYmoudHlwZSA9PSAnQmxvYkZpbGUnIHx8IG9iai52YWx1ZSBpbnN0YW5jZW9mIEZpbGUpIHtcblx0XHRlbmNvZGVCbG9iRmlsZShvYmoudmFsdWUsIGZ1bmN0aW9uIChzZXJpYWxpemVkX2ZpbGUpIHtcblx0XHRcdGNhbGxiYWNrKHNlcmlhbGl6ZWQgKyBzZXJpYWxpemVkX2ZpbGUpO1xuXHRcdH0pO1xuXHR9XG5cdGVsc2UgaWYgKG9iai50eXBlID09ICdBcnJheScgfHwgT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZy5jYWxsKG9iaikgPT09ICdbb2JqZWN0IEFycmF5XScpIHtcblx0XHRsZXQgbyA9IG9iai52YWx1ZSB8fCBvYmo7XG5cdFx0cmVkdWNlU2VyaWFsaXphdGlvbkFycmF5KHNlcmlhbGl6ZWQgKyAnVmFsdWU6QXJyYXk6JywgbywgMCwgZnVuY3Rpb24gKHYpIHtcblx0XHRcdGNhbGxiYWNrKHYgKyAnJiYnKVxuXHRcdH0pO1xuXHR9XG5cdGVsc2UgaWYgKG9iai50eXBlID09ICdTdHJ1Y3R1cmUnKSB7XG5cdFx0bGV0IGtleXMgPSBbXTtcblx0XHRmb3IgKGxldCBrIGluIG9iai52YWx1ZSkga2V5cy5wdXNoKGspO1xuXHRcdGxldCBzID0gc2VyaWFsaXplZCArICdWYWx1ZTpTdHJ1Y3R1cmU6Jztcblx0XHRyZWR1Y2VTZXJpYWxpemF0aW9uU3RydWN0dXJlKHMsIGtleXMsIG9iai52YWx1ZSwgMCwgZnVuY3Rpb24gKHYpIHtcblx0XHRcdGNhbGxiYWNrKHYgKyAnJiYnKVxuXHRcdH0pO1xuXHR9XG5cdGVsc2Uge1xuXHRcdGxldCBzMyA9IGVuY29kZVBsYWluKG9iaik7XG5cdFx0Y2FsbGJhY2soc2VyaWFsaXplZCArIHMzKTtcblx0fVxufVxuXG5mdW5jdGlvbiBzZXJpYWxpemVBVVJMKGEpIHtcblx0dmFyIHJlc3VsdCA9ICcnO1xuXHRmb3IgKHZhciBrZXkgaW4gYSkge1xuXHRcdHZhciBvID0gYVtrZXldO1xuXHRcdHJlc3VsdCArPSBrZXkgKyBcIj1cIiArIGVuY29kZUFVUkxDb21wb25lbnQobyk7XG5cdH1cblx0cmV0dXJuIHJlc3VsdDtcbn07XG5mdW5jdGlvbiBzZXJpYWxpemVBVVJMQXN5bmMoYSwgY2FsbGJhY2spIHtcblx0bGV0IGtleXMgPSBbXTtcblx0Zm9yIChsZXQgayBpbiBhKSBrZXlzLnB1c2goayk7XG5cdHJlZHVjZVNlcmlhbGl6YXRpb25TdHJ1Y3R1cmUoJycsIGtleXMsIGEsIDAsIGNhbGxiYWNrKTtcbn1cblxuZnVuY3Rpb24gcHJlcGFyZURhdGFMaXN0KGRhdGEpIHtcblx0aWYgKCFkYXRhIHx8IGRhdGEubGVuZ3RoID09IDApIHRocm93IFwiSW52YWxpZCBEYXRhTGlzdCBhcmd1bWVudC4gXCI7XG5cdGNvbnN0IGhlYWRlciA9IGRhdGFbMF07XG5cdGxldCByZXN1bHQgPSB7XG5cdFx0aGVhZGVyOiBoZWFkZXIsXG5cdFx0ZGF0YTogW11cblx0fTtcblx0Zm9yIChsZXQgaT0xOyBpPGRhdGEubGVuZ3RoOyBpKyspIHtcblx0XHRsZXQgciA9IGRhdGFbaV0sXG5cdFx0XHRyb3cgPSB7fTtcblx0XHRmb3IgKGxldCBjPTA7IGM8ci5sZW5ndGg7IGMrKykge1xuXHRcdFx0cm93W2hlYWRlcltjXV0gPSByW2NdO1xuXHRcdH1cblx0XHRyZXN1bHQuZGF0YS5wdXNoKHJvdyk7XG5cdH1cblx0cmV0dXJuIHJlc3VsdDtcbn1cblxuLypcbmpRdWVyeS5mbi5leHRlbmQoe1xuXHRzZXJpYWxpemVBVVJMOiBmdW5jdGlvbigpIHtcblx0XHRyZXR1cm4gc2VyaWFsaXplQVVSTCggdGhpcy5zZXJpYWxpemVUeXBlZEFycmF5KCkgKTtcblx0fSxcblx0c2VyaWFsaXplVHlwZWRBcnJheTogZnVuY3Rpb24oKSB7XG5cdFx0dmFyIHJDUkxGID0gL1xccj9cXG4vZyxcblx0XHRcdHJzdWJtaXR0ZXJUeXBlcyA9IC9eKD86c3VibWl0fGJ1dHRvbnxpbWFnZXxyZXNldHxmaWxlKSQvaSxcblx0XHRcdHJzdWJtaXR0YWJsZSA9IC9eKD86aW5wdXR8c2VsZWN0fHRleHRhcmVhfGtleWdlbikvaSxcblx0XHRcdHJjaGVja2FibGVUeXBlID0gL14oPzpjaGVja2JveHxyYWRpbykkL2k7XG5cdFx0XG5cdFx0ZnVuY3Rpb24gZ2V0dih2YWwsIHR5cGUsIGlzQXJyYXkpIHtcblx0XHRcdHR5cGUgPSB0eXBlIHx8ICdzdHJpbmcnO1xuXHRcdFx0dmFyIHYgPSBcIlwiO1xuXHRcdFx0aWYgKHR5cGUgPT0gJ2RhdGUnKSB7XG5cdFx0XHRcdHZhciBmb3VuZCA9IHZhbC5tYXRjaCgvKFxcZFxcZClcXC4oXFxkXFxkKVxcLihcXGRcXGRcXGRcXGQpKCAoXFxkXFxkKTooXFxkXFxkKSg6KFxcZFxcZCkpPyk/Lyk7XG5cdFx0XHRcdGlmIChmb3VuZCAhPSBudWxsKSB7XG5cdFx0XHRcdFx0Ly8gdmFyIGggPSBmb3VuZFs1XSA/IHBhcnNlSW50KGZvdW5kWzVdKSA6IHVuZGVmaW5lZCwgXG5cdFx0XHRcdFx0Ly8gXHRtID0gZm91bmRbNl0gPyBwYXJzZUludChmb3VuZFs2XSkgOiB1bmRlZmluZWQsIFxuXHRcdFx0XHRcdC8vIFx0cyA9IGZvdW5kWzhdID8gcGFyc2VJbnQoZm91bmRbOF0pIDogdW5kZWZpbmVkO1xuXHRcdFx0XHRcdHYgPSBuZXcgRGF0ZShwYXJzZUludChmb3VuZFszXSksIHBhcnNlSW50KGZvdW5kWzJdKS0xLCBwYXJzZUludChmb3VuZFsxXSkpO1xuXHRcdFx0XHRcdC8vIGNvbnNvbGUubG9nKHBhcnNlSW50KGZvdW5kWzNdKSwgcGFyc2VJbnQoZm91bmRbMl0pLTEsIHBhcnNlSW50KGZvdW5kWzFdKSwgaCwgbSwgcywgJy0+Jywgdik7XG5cdFx0XHRcdH1cblx0XHRcdFx0ZWxzZSB2ID0gbnVsbDtcblx0XHRcdH1cblx0XHRcdC8vIGVsc2UgaWYgKHR5cGUgPT0gJ2NoZWNrYm94JykgXG5cdFx0XHRlbHNlIGlmICh0eXBlID09ICdpbnQnKSB2ID0gcGFyc2VJbnQodmFsKTtcblx0XHRcdGVsc2UgaWYgKHR5cGUgPT0gJ2NoZWNrYm94JyAmJiAhaXNBcnJheSkge1xuXHRcdFx0XHR2ID0gKHZhbCA9PSAnb24nID8gdHJ1ZSA6IGZhbHNlKTtcblx0XHRcdH1cblx0XHRcdGVsc2UgdiA9IHZhbC5yZXBsYWNlKCByQ1JMRiwgXCJcXHJcXG5cIiApO1xuXHRcdFx0cmV0dXJuIHY7XG5cdFx0fVxuXG5cdFx0dmFyIGxpc3QgPSB0aGlzLm1hcChmdW5jdGlvbigpIHtcblx0XHRcdC8vIENhbiBhZGQgcHJvcEhvb2sgZm9yIFwiZWxlbWVudHNcIiB0byBmaWx0ZXIgb3IgYWRkIGZvcm0gZWxlbWVudHNcblx0XHRcdHZhciBlbGVtZW50cyA9IGpRdWVyeS5wcm9wKCB0aGlzLCBcImVsZW1lbnRzXCIgKTtcblx0XHRcdHJldHVybiBlbGVtZW50cyA/IGpRdWVyeS5tYWtlQXJyYXkoIGVsZW1lbnRzICkgOiB0aGlzO1xuXHRcdH0pXG5cdFx0LmZpbHRlcihmdW5jdGlvbigpIHtcblx0XHRcdHZhciB0eXBlID0gdGhpcy50eXBlO1xuXG5cdFx0XHQvLyBVc2UgLmlzKCBcIjpkaXNhYmxlZFwiICkgc28gdGhhdCBmaWVsZHNldFtkaXNhYmxlZF0gd29ya3Ncblx0XHRcdHJldHVybiB0aGlzLm5hbWUgJiYgIWpRdWVyeSggdGhpcyApLmlzKCBcIjpkaXNhYmxlZFwiICkgJiZcblx0XHRcdFx0cnN1Ym1pdHRhYmxlLnRlc3QoIHRoaXMubm9kZU5hbWUgKSAmJiAhcnN1Ym1pdHRlclR5cGVzLnRlc3QoIHR5cGUgKSAmJlxuXHRcdFx0XHQoIHRoaXMuY2hlY2tlZCB8fCAhcmNoZWNrYWJsZVR5cGUudGVzdCggdHlwZSApICk7XG5cdFx0fSlcblx0XHQubWFwKGZ1bmN0aW9uKCBpLCBlbGVtICkge1xuXHRcdFx0dmFyIHZhbCA9IGpRdWVyeSggdGhpcyApLnZhbCgpLFxuXHRcdFx0XHR0eXBlID0galF1ZXJ5KCB0aGlzICkuYXR0cignZGF0YS10eXBlJykgfHwgdGhpcy50eXBlLFxuXHRcdFx0XHRpc0FycmF5ID0galF1ZXJ5KCB0aGlzICkuYXR0cignZGF0YS1hcnJheScpID09ICd0cnVlJyB8fCBmYWxzZTtcblxuXHRcdFx0cmV0dXJuIHZhbCA9PSBudWxsID9cblx0XHRcdFx0bnVsbCA6XG5cdFx0XHRcdGpRdWVyeS5pc0FycmF5KCB2YWwgKSA/XG5cdFx0XHRcdFx0alF1ZXJ5Lm1hcCggdmFsLCBmdW5jdGlvbiggdmFsICkge1xuXHRcdFx0XHRcdFx0cmV0dXJuIHsgbmFtZTogZWxlbS5uYW1lLCB2YWx1ZTogZ2V0dih2YWwsIHR5cGUsIGlzQXJyYXkpLCB0eXBlOiB0eXBlLCBpc0FycmF5OiBpc0FycmF5IH07XG5cdFx0XHRcdFx0fSkgOlxuXHRcdFx0XHRcdHsgbmFtZTogZWxlbS5uYW1lLCB2YWx1ZTogZ2V0dih2YWwsIHR5cGUsIGlzQXJyYXkpLCB0eXBlOiB0eXBlLCBpc0FycmF5OiBpc0FycmF5IH07XG5cdFx0fSkuZ2V0KCk7XG5cblx0XHR2YXIgcyA9IHt9XG5cdFx0Zm9yICh2YXIgaT0wOyBpPGxpc3QubGVuZ3RoOyBpKyspIHtcblx0XHRcdHZhciBvID0gbGlzdFtpXTtcblx0XHRcdGlmIChvLmlzQXJyYXkgfHwgby5uYW1lIGluIHMpIHtcblx0XHRcdFx0aWYgKCEoby5uYW1lIGluIHMpKSBzW28ubmFtZV0gPSBbb107XG5cdFx0XHRcdGVsc2Ugc1tvLm5hbWVdLnB1c2gobyk7XG5cdFx0XHR9XG5cdFx0XHRlbHNlIHNbby5uYW1lXSA9IG87XG5cdFx0fVxuXHRcdHJldHVybiBzO1xuXHR9XG59KTtcbiovXG5cbmV4cG9ydCB7IEVmZmlQcm90b2NvbCwgc2VyaWFsaXplQVVSTCwgc2VyaWFsaXplQVVSTEFzeW5jLCBmb3JtYXRfZWZmaV9kYXRlLCBmb3JtYXRfZWZmaV90aW1lLCBwcmVwYXJlRGF0YUxpc3QgfTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBzcmMvbGliL2VmZmlfcHJvdG9jb2wuanMiLCIvKiFcbiAqIEJvd3NlciAtIGEgYnJvd3NlciBkZXRlY3RvclxuICogaHR0cHM6Ly9naXRodWIuY29tL2RlZC9ib3dzZXJcbiAqIE1JVCBMaWNlbnNlIHwgKGMpIER1c3RpbiBEaWF6IDIwMTVcbiAqL1xuXG4hZnVuY3Rpb24gKHJvb3QsIG5hbWUsIGRlZmluaXRpb24pIHtcbiAgaWYgKHR5cGVvZiBtb2R1bGUgIT0gJ3VuZGVmaW5lZCcgJiYgbW9kdWxlLmV4cG9ydHMpIG1vZHVsZS5leHBvcnRzID0gZGVmaW5pdGlvbigpXG4gIGVsc2UgaWYgKHR5cGVvZiBkZWZpbmUgPT0gJ2Z1bmN0aW9uJyAmJiBkZWZpbmUuYW1kKSBkZWZpbmUobmFtZSwgZGVmaW5pdGlvbilcbiAgZWxzZSByb290W25hbWVdID0gZGVmaW5pdGlvbigpXG59KHRoaXMsICdib3dzZXInLCBmdW5jdGlvbiAoKSB7XG4gIC8qKlxuICAgICogU2VlIHVzZXJhZ2VudHMuanMgZm9yIGV4YW1wbGVzIG9mIG5hdmlnYXRvci51c2VyQWdlbnRcbiAgICAqL1xuXG4gIHZhciB0ID0gdHJ1ZVxuXG4gIGZ1bmN0aW9uIGRldGVjdCh1YSkge1xuXG4gICAgZnVuY3Rpb24gZ2V0Rmlyc3RNYXRjaChyZWdleCkge1xuICAgICAgdmFyIG1hdGNoID0gdWEubWF0Y2gocmVnZXgpO1xuICAgICAgcmV0dXJuIChtYXRjaCAmJiBtYXRjaC5sZW5ndGggPiAxICYmIG1hdGNoWzFdKSB8fCAnJztcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBnZXRTZWNvbmRNYXRjaChyZWdleCkge1xuICAgICAgdmFyIG1hdGNoID0gdWEubWF0Y2gocmVnZXgpO1xuICAgICAgcmV0dXJuIChtYXRjaCAmJiBtYXRjaC5sZW5ndGggPiAxICYmIG1hdGNoWzJdKSB8fCAnJztcbiAgICB9XG5cbiAgICB2YXIgaW9zZGV2aWNlID0gZ2V0Rmlyc3RNYXRjaCgvKGlwb2R8aXBob25lfGlwYWQpL2kpLnRvTG93ZXJDYXNlKClcbiAgICAgICwgbGlrZUFuZHJvaWQgPSAvbGlrZSBhbmRyb2lkL2kudGVzdCh1YSlcbiAgICAgICwgYW5kcm9pZCA9ICFsaWtlQW5kcm9pZCAmJiAvYW5kcm9pZC9pLnRlc3QodWEpXG4gICAgICAsIG5leHVzTW9iaWxlID0gL25leHVzXFxzKlswLTZdXFxzKi9pLnRlc3QodWEpXG4gICAgICAsIG5leHVzVGFibGV0ID0gIW5leHVzTW9iaWxlICYmIC9uZXh1c1xccypbMC05XSsvaS50ZXN0KHVhKVxuICAgICAgLCBjaHJvbWVvcyA9IC9Dck9TLy50ZXN0KHVhKVxuICAgICAgLCBzaWxrID0gL3NpbGsvaS50ZXN0KHVhKVxuICAgICAgLCBzYWlsZmlzaCA9IC9zYWlsZmlzaC9pLnRlc3QodWEpXG4gICAgICAsIHRpemVuID0gL3RpemVuL2kudGVzdCh1YSlcbiAgICAgICwgd2Vib3MgPSAvKHdlYnxocHcpKG98MClzL2kudGVzdCh1YSlcbiAgICAgICwgd2luZG93c3Bob25lID0gL3dpbmRvd3MgcGhvbmUvaS50ZXN0KHVhKVxuICAgICAgLCBzYW1zdW5nQnJvd3NlciA9IC9TYW1zdW5nQnJvd3Nlci9pLnRlc3QodWEpXG4gICAgICAsIHdpbmRvd3MgPSAhd2luZG93c3Bob25lICYmIC93aW5kb3dzL2kudGVzdCh1YSlcbiAgICAgICwgbWFjID0gIWlvc2RldmljZSAmJiAhc2lsayAmJiAvbWFjaW50b3NoL2kudGVzdCh1YSlcbiAgICAgICwgbGludXggPSAhYW5kcm9pZCAmJiAhc2FpbGZpc2ggJiYgIXRpemVuICYmICF3ZWJvcyAmJiAvbGludXgvaS50ZXN0KHVhKVxuICAgICAgLCBlZGdlVmVyc2lvbiA9IGdldFNlY29uZE1hdGNoKC9lZGcoW2VhXXxpb3MpXFwvKFxcZCsoXFwuXFxkKyk/KS9pKVxuICAgICAgLCB2ZXJzaW9uSWRlbnRpZmllciA9IGdldEZpcnN0TWF0Y2goL3ZlcnNpb25cXC8oXFxkKyhcXC5cXGQrKT8pL2kpXG4gICAgICAsIHRhYmxldCA9IC90YWJsZXQvaS50ZXN0KHVhKSAmJiAhL3RhYmxldCBwYy9pLnRlc3QodWEpXG4gICAgICAsIG1vYmlsZSA9ICF0YWJsZXQgJiYgL1teLV1tb2JpL2kudGVzdCh1YSlcbiAgICAgICwgeGJveCA9IC94Ym94L2kudGVzdCh1YSlcbiAgICAgICwgcmVzdWx0XG5cbiAgICBpZiAoL29wZXJhL2kudGVzdCh1YSkpIHtcbiAgICAgIC8vICBhbiBvbGQgT3BlcmFcbiAgICAgIHJlc3VsdCA9IHtcbiAgICAgICAgbmFtZTogJ09wZXJhJ1xuICAgICAgLCBvcGVyYTogdFxuICAgICAgLCB2ZXJzaW9uOiB2ZXJzaW9uSWRlbnRpZmllciB8fCBnZXRGaXJzdE1hdGNoKC8oPzpvcGVyYXxvcHJ8b3Bpb3MpW1xcc1xcL10oXFxkKyhcXC5cXGQrKT8pL2kpXG4gICAgICB9XG4gICAgfSBlbHNlIGlmICgvb3ByXFwvfG9waW9zL2kudGVzdCh1YSkpIHtcbiAgICAgIC8vIGEgbmV3IE9wZXJhXG4gICAgICByZXN1bHQgPSB7XG4gICAgICAgIG5hbWU6ICdPcGVyYSdcbiAgICAgICAgLCBvcGVyYTogdFxuICAgICAgICAsIHZlcnNpb246IGdldEZpcnN0TWF0Y2goLyg/Om9wcnxvcGlvcylbXFxzXFwvXShcXGQrKFxcLlxcZCspPykvaSkgfHwgdmVyc2lvbklkZW50aWZpZXJcbiAgICAgIH1cbiAgICB9XG4gICAgZWxzZSBpZiAoL1NhbXN1bmdCcm93c2VyL2kudGVzdCh1YSkpIHtcbiAgICAgIHJlc3VsdCA9IHtcbiAgICAgICAgbmFtZTogJ1NhbXN1bmcgSW50ZXJuZXQgZm9yIEFuZHJvaWQnXG4gICAgICAgICwgc2Ftc3VuZ0Jyb3dzZXI6IHRcbiAgICAgICAgLCB2ZXJzaW9uOiB2ZXJzaW9uSWRlbnRpZmllciB8fCBnZXRGaXJzdE1hdGNoKC8oPzpTYW1zdW5nQnJvd3NlcilbXFxzXFwvXShcXGQrKFxcLlxcZCspPykvaSlcbiAgICAgIH1cbiAgICB9XG4gICAgZWxzZSBpZiAoL1doYWxlL2kudGVzdCh1YSkpIHtcbiAgICAgIHJlc3VsdCA9IHtcbiAgICAgICAgbmFtZTogJ05BVkVSIFdoYWxlIGJyb3dzZXInXG4gICAgICAgICwgd2hhbGU6IHRcbiAgICAgICAgLCB2ZXJzaW9uOiBnZXRGaXJzdE1hdGNoKC8oPzp3aGFsZSlbXFxzXFwvXShcXGQrKD86XFwuXFxkKykrKS9pKVxuICAgICAgfVxuICAgIH1cbiAgICBlbHNlIGlmICgvTVpCcm93c2VyL2kudGVzdCh1YSkpIHtcbiAgICAgIHJlc3VsdCA9IHtcbiAgICAgICAgbmFtZTogJ01aIEJyb3dzZXInXG4gICAgICAgICwgbXpicm93c2VyOiB0XG4gICAgICAgICwgdmVyc2lvbjogZ2V0Rmlyc3RNYXRjaCgvKD86TVpCcm93c2VyKVtcXHNcXC9dKFxcZCsoPzpcXC5cXGQrKSspL2kpXG4gICAgICB9XG4gICAgfVxuICAgIGVsc2UgaWYgKC9jb2FzdC9pLnRlc3QodWEpKSB7XG4gICAgICByZXN1bHQgPSB7XG4gICAgICAgIG5hbWU6ICdPcGVyYSBDb2FzdCdcbiAgICAgICAgLCBjb2FzdDogdFxuICAgICAgICAsIHZlcnNpb246IHZlcnNpb25JZGVudGlmaWVyIHx8IGdldEZpcnN0TWF0Y2goLyg/OmNvYXN0KVtcXHNcXC9dKFxcZCsoXFwuXFxkKyk/KS9pKVxuICAgICAgfVxuICAgIH1cbiAgICBlbHNlIGlmICgvZm9jdXMvaS50ZXN0KHVhKSkge1xuICAgICAgcmVzdWx0ID0ge1xuICAgICAgICBuYW1lOiAnRm9jdXMnXG4gICAgICAgICwgZm9jdXM6IHRcbiAgICAgICAgLCB2ZXJzaW9uOiBnZXRGaXJzdE1hdGNoKC8oPzpmb2N1cylbXFxzXFwvXShcXGQrKD86XFwuXFxkKykrKS9pKVxuICAgICAgfVxuICAgIH1cbiAgICBlbHNlIGlmICgveWFicm93c2VyL2kudGVzdCh1YSkpIHtcbiAgICAgIHJlc3VsdCA9IHtcbiAgICAgICAgbmFtZTogJ1lhbmRleCBCcm93c2VyJ1xuICAgICAgLCB5YW5kZXhicm93c2VyOiB0XG4gICAgICAsIHZlcnNpb246IHZlcnNpb25JZGVudGlmaWVyIHx8IGdldEZpcnN0TWF0Y2goLyg/OnlhYnJvd3NlcilbXFxzXFwvXShcXGQrKFxcLlxcZCspPykvaSlcbiAgICAgIH1cbiAgICB9XG4gICAgZWxzZSBpZiAoL3VjYnJvd3Nlci9pLnRlc3QodWEpKSB7XG4gICAgICByZXN1bHQgPSB7XG4gICAgICAgICAgbmFtZTogJ1VDIEJyb3dzZXInXG4gICAgICAgICwgdWNicm93c2VyOiB0XG4gICAgICAgICwgdmVyc2lvbjogZ2V0Rmlyc3RNYXRjaCgvKD86dWNicm93c2VyKVtcXHNcXC9dKFxcZCsoPzpcXC5cXGQrKSspL2kpXG4gICAgICB9XG4gICAgfVxuICAgIGVsc2UgaWYgKC9teGlvcy9pLnRlc3QodWEpKSB7XG4gICAgICByZXN1bHQgPSB7XG4gICAgICAgIG5hbWU6ICdNYXh0aG9uJ1xuICAgICAgICAsIG1heHRob246IHRcbiAgICAgICAgLCB2ZXJzaW9uOiBnZXRGaXJzdE1hdGNoKC8oPzpteGlvcylbXFxzXFwvXShcXGQrKD86XFwuXFxkKykrKS9pKVxuICAgICAgfVxuICAgIH1cbiAgICBlbHNlIGlmICgvZXBpcGhhbnkvaS50ZXN0KHVhKSkge1xuICAgICAgcmVzdWx0ID0ge1xuICAgICAgICBuYW1lOiAnRXBpcGhhbnknXG4gICAgICAgICwgZXBpcGhhbnk6IHRcbiAgICAgICAgLCB2ZXJzaW9uOiBnZXRGaXJzdE1hdGNoKC8oPzplcGlwaGFueSlbXFxzXFwvXShcXGQrKD86XFwuXFxkKykrKS9pKVxuICAgICAgfVxuICAgIH1cbiAgICBlbHNlIGlmICgvcHVmZmluL2kudGVzdCh1YSkpIHtcbiAgICAgIHJlc3VsdCA9IHtcbiAgICAgICAgbmFtZTogJ1B1ZmZpbidcbiAgICAgICAgLCBwdWZmaW46IHRcbiAgICAgICAgLCB2ZXJzaW9uOiBnZXRGaXJzdE1hdGNoKC8oPzpwdWZmaW4pW1xcc1xcL10oXFxkKyg/OlxcLlxcZCspPykvaSlcbiAgICAgIH1cbiAgICB9XG4gICAgZWxzZSBpZiAoL3NsZWlwbmlyL2kudGVzdCh1YSkpIHtcbiAgICAgIHJlc3VsdCA9IHtcbiAgICAgICAgbmFtZTogJ1NsZWlwbmlyJ1xuICAgICAgICAsIHNsZWlwbmlyOiB0XG4gICAgICAgICwgdmVyc2lvbjogZ2V0Rmlyc3RNYXRjaCgvKD86c2xlaXBuaXIpW1xcc1xcL10oXFxkKyg/OlxcLlxcZCspKykvaSlcbiAgICAgIH1cbiAgICB9XG4gICAgZWxzZSBpZiAoL2stbWVsZW9uL2kudGVzdCh1YSkpIHtcbiAgICAgIHJlc3VsdCA9IHtcbiAgICAgICAgbmFtZTogJ0stTWVsZW9uJ1xuICAgICAgICAsIGtNZWxlb246IHRcbiAgICAgICAgLCB2ZXJzaW9uOiBnZXRGaXJzdE1hdGNoKC8oPzprLW1lbGVvbilbXFxzXFwvXShcXGQrKD86XFwuXFxkKykrKS9pKVxuICAgICAgfVxuICAgIH1cbiAgICBlbHNlIGlmICh3aW5kb3dzcGhvbmUpIHtcbiAgICAgIHJlc3VsdCA9IHtcbiAgICAgICAgbmFtZTogJ1dpbmRvd3MgUGhvbmUnXG4gICAgICAsIG9zbmFtZTogJ1dpbmRvd3MgUGhvbmUnXG4gICAgICAsIHdpbmRvd3NwaG9uZTogdFxuICAgICAgfVxuICAgICAgaWYgKGVkZ2VWZXJzaW9uKSB7XG4gICAgICAgIHJlc3VsdC5tc2VkZ2UgPSB0XG4gICAgICAgIHJlc3VsdC52ZXJzaW9uID0gZWRnZVZlcnNpb25cbiAgICAgIH1cbiAgICAgIGVsc2Uge1xuICAgICAgICByZXN1bHQubXNpZSA9IHRcbiAgICAgICAgcmVzdWx0LnZlcnNpb24gPSBnZXRGaXJzdE1hdGNoKC9pZW1vYmlsZVxcLyhcXGQrKFxcLlxcZCspPykvaSlcbiAgICAgIH1cbiAgICB9XG4gICAgZWxzZSBpZiAoL21zaWV8dHJpZGVudC9pLnRlc3QodWEpKSB7XG4gICAgICByZXN1bHQgPSB7XG4gICAgICAgIG5hbWU6ICdJbnRlcm5ldCBFeHBsb3JlcidcbiAgICAgICwgbXNpZTogdFxuICAgICAgLCB2ZXJzaW9uOiBnZXRGaXJzdE1hdGNoKC8oPzptc2llIHxydjopKFxcZCsoXFwuXFxkKyk/KS9pKVxuICAgICAgfVxuICAgIH0gZWxzZSBpZiAoY2hyb21lb3MpIHtcbiAgICAgIHJlc3VsdCA9IHtcbiAgICAgICAgbmFtZTogJ0Nocm9tZSdcbiAgICAgICwgb3NuYW1lOiAnQ2hyb21lIE9TJ1xuICAgICAgLCBjaHJvbWVvczogdFxuICAgICAgLCBjaHJvbWVCb29rOiB0XG4gICAgICAsIGNocm9tZTogdFxuICAgICAgLCB2ZXJzaW9uOiBnZXRGaXJzdE1hdGNoKC8oPzpjaHJvbWV8Y3Jpb3N8Y3JtbylcXC8oXFxkKyhcXC5cXGQrKT8pL2kpXG4gICAgICB9XG4gICAgfSBlbHNlIGlmICgvZWRnKFtlYV18aW9zKS9pLnRlc3QodWEpKSB7XG4gICAgICByZXN1bHQgPSB7XG4gICAgICAgIG5hbWU6ICdNaWNyb3NvZnQgRWRnZSdcbiAgICAgICwgbXNlZGdlOiB0XG4gICAgICAsIHZlcnNpb246IGVkZ2VWZXJzaW9uXG4gICAgICB9XG4gICAgfVxuICAgIGVsc2UgaWYgKC92aXZhbGRpL2kudGVzdCh1YSkpIHtcbiAgICAgIHJlc3VsdCA9IHtcbiAgICAgICAgbmFtZTogJ1ZpdmFsZGknXG4gICAgICAgICwgdml2YWxkaTogdFxuICAgICAgICAsIHZlcnNpb246IGdldEZpcnN0TWF0Y2goL3ZpdmFsZGlcXC8oXFxkKyhcXC5cXGQrKT8pL2kpIHx8IHZlcnNpb25JZGVudGlmaWVyXG4gICAgICB9XG4gICAgfVxuICAgIGVsc2UgaWYgKHNhaWxmaXNoKSB7XG4gICAgICByZXN1bHQgPSB7XG4gICAgICAgIG5hbWU6ICdTYWlsZmlzaCdcbiAgICAgICwgb3NuYW1lOiAnU2FpbGZpc2ggT1MnXG4gICAgICAsIHNhaWxmaXNoOiB0XG4gICAgICAsIHZlcnNpb246IGdldEZpcnN0TWF0Y2goL3NhaWxmaXNoXFxzP2Jyb3dzZXJcXC8oXFxkKyhcXC5cXGQrKT8pL2kpXG4gICAgICB9XG4gICAgfVxuICAgIGVsc2UgaWYgKC9zZWFtb25rZXlcXC8vaS50ZXN0KHVhKSkge1xuICAgICAgcmVzdWx0ID0ge1xuICAgICAgICBuYW1lOiAnU2VhTW9ua2V5J1xuICAgICAgLCBzZWFtb25rZXk6IHRcbiAgICAgICwgdmVyc2lvbjogZ2V0Rmlyc3RNYXRjaCgvc2VhbW9ua2V5XFwvKFxcZCsoXFwuXFxkKyk/KS9pKVxuICAgICAgfVxuICAgIH1cbiAgICBlbHNlIGlmICgvZmlyZWZveHxpY2V3ZWFzZWx8Znhpb3MvaS50ZXN0KHVhKSkge1xuICAgICAgcmVzdWx0ID0ge1xuICAgICAgICBuYW1lOiAnRmlyZWZveCdcbiAgICAgICwgZmlyZWZveDogdFxuICAgICAgLCB2ZXJzaW9uOiBnZXRGaXJzdE1hdGNoKC8oPzpmaXJlZm94fGljZXdlYXNlbHxmeGlvcylbIFxcL10oXFxkKyhcXC5cXGQrKT8pL2kpXG4gICAgICB9XG4gICAgICBpZiAoL1xcKChtb2JpbGV8dGFibGV0KTtbXlxcKV0qcnY6W1xcZFxcLl0rXFwpL2kudGVzdCh1YSkpIHtcbiAgICAgICAgcmVzdWx0LmZpcmVmb3hvcyA9IHRcbiAgICAgICAgcmVzdWx0Lm9zbmFtZSA9ICdGaXJlZm94IE9TJ1xuICAgICAgfVxuICAgIH1cbiAgICBlbHNlIGlmIChzaWxrKSB7XG4gICAgICByZXN1bHQgPSAge1xuICAgICAgICBuYW1lOiAnQW1hem9uIFNpbGsnXG4gICAgICAsIHNpbGs6IHRcbiAgICAgICwgdmVyc2lvbiA6IGdldEZpcnN0TWF0Y2goL3NpbGtcXC8oXFxkKyhcXC5cXGQrKT8pL2kpXG4gICAgICB9XG4gICAgfVxuICAgIGVsc2UgaWYgKC9waGFudG9tL2kudGVzdCh1YSkpIHtcbiAgICAgIHJlc3VsdCA9IHtcbiAgICAgICAgbmFtZTogJ1BoYW50b21KUydcbiAgICAgICwgcGhhbnRvbTogdFxuICAgICAgLCB2ZXJzaW9uOiBnZXRGaXJzdE1hdGNoKC9waGFudG9tanNcXC8oXFxkKyhcXC5cXGQrKT8pL2kpXG4gICAgICB9XG4gICAgfVxuICAgIGVsc2UgaWYgKC9zbGltZXJqcy9pLnRlc3QodWEpKSB7XG4gICAgICByZXN1bHQgPSB7XG4gICAgICAgIG5hbWU6ICdTbGltZXJKUydcbiAgICAgICAgLCBzbGltZXI6IHRcbiAgICAgICAgLCB2ZXJzaW9uOiBnZXRGaXJzdE1hdGNoKC9zbGltZXJqc1xcLyhcXGQrKFxcLlxcZCspPykvaSlcbiAgICAgIH1cbiAgICB9XG4gICAgZWxzZSBpZiAoL2JsYWNrYmVycnl8XFxiYmJcXGQrL2kudGVzdCh1YSkgfHwgL3JpbVxcc3RhYmxldC9pLnRlc3QodWEpKSB7XG4gICAgICByZXN1bHQgPSB7XG4gICAgICAgIG5hbWU6ICdCbGFja0JlcnJ5J1xuICAgICAgLCBvc25hbWU6ICdCbGFja0JlcnJ5IE9TJ1xuICAgICAgLCBibGFja2JlcnJ5OiB0XG4gICAgICAsIHZlcnNpb246IHZlcnNpb25JZGVudGlmaWVyIHx8IGdldEZpcnN0TWF0Y2goL2JsYWNrYmVycnlbXFxkXStcXC8oXFxkKyhcXC5cXGQrKT8pL2kpXG4gICAgICB9XG4gICAgfVxuICAgIGVsc2UgaWYgKHdlYm9zKSB7XG4gICAgICByZXN1bHQgPSB7XG4gICAgICAgIG5hbWU6ICdXZWJPUydcbiAgICAgICwgb3NuYW1lOiAnV2ViT1MnXG4gICAgICAsIHdlYm9zOiB0XG4gICAgICAsIHZlcnNpb246IHZlcnNpb25JZGVudGlmaWVyIHx8IGdldEZpcnN0TWF0Y2goL3coPzplYik/b3Nicm93c2VyXFwvKFxcZCsoXFwuXFxkKyk/KS9pKVxuICAgICAgfTtcbiAgICAgIC90b3VjaHBhZFxcLy9pLnRlc3QodWEpICYmIChyZXN1bHQudG91Y2hwYWQgPSB0KVxuICAgIH1cbiAgICBlbHNlIGlmICgvYmFkYS9pLnRlc3QodWEpKSB7XG4gICAgICByZXN1bHQgPSB7XG4gICAgICAgIG5hbWU6ICdCYWRhJ1xuICAgICAgLCBvc25hbWU6ICdCYWRhJ1xuICAgICAgLCBiYWRhOiB0XG4gICAgICAsIHZlcnNpb246IGdldEZpcnN0TWF0Y2goL2RvbGZpblxcLyhcXGQrKFxcLlxcZCspPykvaSlcbiAgICAgIH07XG4gICAgfVxuICAgIGVsc2UgaWYgKHRpemVuKSB7XG4gICAgICByZXN1bHQgPSB7XG4gICAgICAgIG5hbWU6ICdUaXplbidcbiAgICAgICwgb3NuYW1lOiAnVGl6ZW4nXG4gICAgICAsIHRpemVuOiB0XG4gICAgICAsIHZlcnNpb246IGdldEZpcnN0TWF0Y2goLyg/OnRpemVuXFxzPyk/YnJvd3NlclxcLyhcXGQrKFxcLlxcZCspPykvaSkgfHwgdmVyc2lvbklkZW50aWZpZXJcbiAgICAgIH07XG4gICAgfVxuICAgIGVsc2UgaWYgKC9xdXB6aWxsYS9pLnRlc3QodWEpKSB7XG4gICAgICByZXN1bHQgPSB7XG4gICAgICAgIG5hbWU6ICdRdXBaaWxsYSdcbiAgICAgICAgLCBxdXB6aWxsYTogdFxuICAgICAgICAsIHZlcnNpb246IGdldEZpcnN0TWF0Y2goLyg/OnF1cHppbGxhKVtcXHNcXC9dKFxcZCsoPzpcXC5cXGQrKSspL2kpIHx8IHZlcnNpb25JZGVudGlmaWVyXG4gICAgICB9XG4gICAgfVxuICAgIGVsc2UgaWYgKC9jaHJvbWl1bS9pLnRlc3QodWEpKSB7XG4gICAgICByZXN1bHQgPSB7XG4gICAgICAgIG5hbWU6ICdDaHJvbWl1bSdcbiAgICAgICAgLCBjaHJvbWl1bTogdFxuICAgICAgICAsIHZlcnNpb246IGdldEZpcnN0TWF0Y2goLyg/OmNocm9taXVtKVtcXHNcXC9dKFxcZCsoPzpcXC5cXGQrKT8pL2kpIHx8IHZlcnNpb25JZGVudGlmaWVyXG4gICAgICB9XG4gICAgfVxuICAgIGVsc2UgaWYgKC9jaHJvbWV8Y3Jpb3N8Y3Jtby9pLnRlc3QodWEpKSB7XG4gICAgICByZXN1bHQgPSB7XG4gICAgICAgIG5hbWU6ICdDaHJvbWUnXG4gICAgICAgICwgY2hyb21lOiB0XG4gICAgICAgICwgdmVyc2lvbjogZ2V0Rmlyc3RNYXRjaCgvKD86Y2hyb21lfGNyaW9zfGNybW8pXFwvKFxcZCsoXFwuXFxkKyk/KS9pKVxuICAgICAgfVxuICAgIH1cbiAgICBlbHNlIGlmIChhbmRyb2lkKSB7XG4gICAgICByZXN1bHQgPSB7XG4gICAgICAgIG5hbWU6ICdBbmRyb2lkJ1xuICAgICAgICAsIHZlcnNpb246IHZlcnNpb25JZGVudGlmaWVyXG4gICAgICB9XG4gICAgfVxuICAgIGVsc2UgaWYgKC9zYWZhcml8YXBwbGV3ZWJraXQvaS50ZXN0KHVhKSkge1xuICAgICAgcmVzdWx0ID0ge1xuICAgICAgICBuYW1lOiAnU2FmYXJpJ1xuICAgICAgLCBzYWZhcmk6IHRcbiAgICAgIH1cbiAgICAgIGlmICh2ZXJzaW9uSWRlbnRpZmllcikge1xuICAgICAgICByZXN1bHQudmVyc2lvbiA9IHZlcnNpb25JZGVudGlmaWVyXG4gICAgICB9XG4gICAgfVxuICAgIGVsc2UgaWYgKGlvc2RldmljZSkge1xuICAgICAgcmVzdWx0ID0ge1xuICAgICAgICBuYW1lIDogaW9zZGV2aWNlID09ICdpcGhvbmUnID8gJ2lQaG9uZScgOiBpb3NkZXZpY2UgPT0gJ2lwYWQnID8gJ2lQYWQnIDogJ2lQb2QnXG4gICAgICB9XG4gICAgICAvLyBXVEY6IHZlcnNpb24gaXMgbm90IHBhcnQgb2YgdXNlciBhZ2VudCBpbiB3ZWIgYXBwc1xuICAgICAgaWYgKHZlcnNpb25JZGVudGlmaWVyKSB7XG4gICAgICAgIHJlc3VsdC52ZXJzaW9uID0gdmVyc2lvbklkZW50aWZpZXJcbiAgICAgIH1cbiAgICB9XG4gICAgZWxzZSBpZigvZ29vZ2xlYm90L2kudGVzdCh1YSkpIHtcbiAgICAgIHJlc3VsdCA9IHtcbiAgICAgICAgbmFtZTogJ0dvb2dsZWJvdCdcbiAgICAgICwgZ29vZ2xlYm90OiB0XG4gICAgICAsIHZlcnNpb246IGdldEZpcnN0TWF0Y2goL2dvb2dsZWJvdFxcLyhcXGQrKFxcLlxcZCspKS9pKSB8fCB2ZXJzaW9uSWRlbnRpZmllclxuICAgICAgfVxuICAgIH1cbiAgICBlbHNlIHtcbiAgICAgIHJlc3VsdCA9IHtcbiAgICAgICAgbmFtZTogZ2V0Rmlyc3RNYXRjaCgvXiguKilcXC8oLiopIC8pLFxuICAgICAgICB2ZXJzaW9uOiBnZXRTZWNvbmRNYXRjaCgvXiguKilcXC8oLiopIC8pXG4gICAgIH07XG4gICB9XG5cbiAgICAvLyBzZXQgd2Via2l0IG9yIGdlY2tvIGZsYWcgZm9yIGJyb3dzZXJzIGJhc2VkIG9uIHRoZXNlIGVuZ2luZXNcbiAgICBpZiAoIXJlc3VsdC5tc2VkZ2UgJiYgLyhhcHBsZSk/d2Via2l0L2kudGVzdCh1YSkpIHtcbiAgICAgIGlmICgvKGFwcGxlKT93ZWJraXRcXC81MzdcXC4zNi9pLnRlc3QodWEpKSB7XG4gICAgICAgIHJlc3VsdC5uYW1lID0gcmVzdWx0Lm5hbWUgfHwgXCJCbGlua1wiXG4gICAgICAgIHJlc3VsdC5ibGluayA9IHRcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHJlc3VsdC5uYW1lID0gcmVzdWx0Lm5hbWUgfHwgXCJXZWJraXRcIlxuICAgICAgICByZXN1bHQud2Via2l0ID0gdFxuICAgICAgfVxuICAgICAgaWYgKCFyZXN1bHQudmVyc2lvbiAmJiB2ZXJzaW9uSWRlbnRpZmllcikge1xuICAgICAgICByZXN1bHQudmVyc2lvbiA9IHZlcnNpb25JZGVudGlmaWVyXG4gICAgICB9XG4gICAgfSBlbHNlIGlmICghcmVzdWx0Lm9wZXJhICYmIC9nZWNrb1xcLy9pLnRlc3QodWEpKSB7XG4gICAgICByZXN1bHQubmFtZSA9IHJlc3VsdC5uYW1lIHx8IFwiR2Vja29cIlxuICAgICAgcmVzdWx0LmdlY2tvID0gdFxuICAgICAgcmVzdWx0LnZlcnNpb24gPSByZXN1bHQudmVyc2lvbiB8fCBnZXRGaXJzdE1hdGNoKC9nZWNrb1xcLyhcXGQrKFxcLlxcZCspPykvaSlcbiAgICB9XG5cbiAgICAvLyBzZXQgT1MgZmxhZ3MgZm9yIHBsYXRmb3JtcyB0aGF0IGhhdmUgbXVsdGlwbGUgYnJvd3NlcnNcbiAgICBpZiAoIXJlc3VsdC53aW5kb3dzcGhvbmUgJiYgKGFuZHJvaWQgfHwgcmVzdWx0LnNpbGspKSB7XG4gICAgICByZXN1bHQuYW5kcm9pZCA9IHRcbiAgICAgIHJlc3VsdC5vc25hbWUgPSAnQW5kcm9pZCdcbiAgICB9IGVsc2UgaWYgKCFyZXN1bHQud2luZG93c3Bob25lICYmIGlvc2RldmljZSkge1xuICAgICAgcmVzdWx0W2lvc2RldmljZV0gPSB0XG4gICAgICByZXN1bHQuaW9zID0gdFxuICAgICAgcmVzdWx0Lm9zbmFtZSA9ICdpT1MnXG4gICAgfSBlbHNlIGlmIChtYWMpIHtcbiAgICAgIHJlc3VsdC5tYWMgPSB0XG4gICAgICByZXN1bHQub3NuYW1lID0gJ21hY09TJ1xuICAgIH0gZWxzZSBpZiAoeGJveCkge1xuICAgICAgcmVzdWx0Lnhib3ggPSB0XG4gICAgICByZXN1bHQub3NuYW1lID0gJ1hib3gnXG4gICAgfSBlbHNlIGlmICh3aW5kb3dzKSB7XG4gICAgICByZXN1bHQud2luZG93cyA9IHRcbiAgICAgIHJlc3VsdC5vc25hbWUgPSAnV2luZG93cydcbiAgICB9IGVsc2UgaWYgKGxpbnV4KSB7XG4gICAgICByZXN1bHQubGludXggPSB0XG4gICAgICByZXN1bHQub3NuYW1lID0gJ0xpbnV4J1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGdldFdpbmRvd3NWZXJzaW9uIChzKSB7XG4gICAgICBzd2l0Y2ggKHMpIHtcbiAgICAgICAgY2FzZSAnTlQnOiByZXR1cm4gJ05UJ1xuICAgICAgICBjYXNlICdYUCc6IHJldHVybiAnWFAnXG4gICAgICAgIGNhc2UgJ05UIDUuMCc6IHJldHVybiAnMjAwMCdcbiAgICAgICAgY2FzZSAnTlQgNS4xJzogcmV0dXJuICdYUCdcbiAgICAgICAgY2FzZSAnTlQgNS4yJzogcmV0dXJuICcyMDAzJ1xuICAgICAgICBjYXNlICdOVCA2LjAnOiByZXR1cm4gJ1Zpc3RhJ1xuICAgICAgICBjYXNlICdOVCA2LjEnOiByZXR1cm4gJzcnXG4gICAgICAgIGNhc2UgJ05UIDYuMic6IHJldHVybiAnOCdcbiAgICAgICAgY2FzZSAnTlQgNi4zJzogcmV0dXJuICc4LjEnXG4gICAgICAgIGNhc2UgJ05UIDEwLjAnOiByZXR1cm4gJzEwJ1xuICAgICAgICBkZWZhdWx0OiByZXR1cm4gdW5kZWZpbmVkXG4gICAgICB9XG4gICAgfVxuXG4gICAgLy8gT1MgdmVyc2lvbiBleHRyYWN0aW9uXG4gICAgdmFyIG9zVmVyc2lvbiA9ICcnO1xuICAgIGlmIChyZXN1bHQud2luZG93cykge1xuICAgICAgb3NWZXJzaW9uID0gZ2V0V2luZG93c1ZlcnNpb24oZ2V0Rmlyc3RNYXRjaCgvV2luZG93cyAoKE5UfFhQKSggXFxkXFxkPy5cXGQpPykvaSkpXG4gICAgfSBlbHNlIGlmIChyZXN1bHQud2luZG93c3Bob25lKSB7XG4gICAgICBvc1ZlcnNpb24gPSBnZXRGaXJzdE1hdGNoKC93aW5kb3dzIHBob25lICg/Om9zKT9cXHM/KFxcZCsoXFwuXFxkKykqKS9pKTtcbiAgICB9IGVsc2UgaWYgKHJlc3VsdC5tYWMpIHtcbiAgICAgIG9zVmVyc2lvbiA9IGdldEZpcnN0TWF0Y2goL01hYyBPUyBYIChcXGQrKFtfXFwuXFxzXVxcZCspKikvaSk7XG4gICAgICBvc1ZlcnNpb24gPSBvc1ZlcnNpb24ucmVwbGFjZSgvW19cXHNdL2csICcuJyk7XG4gICAgfSBlbHNlIGlmIChpb3NkZXZpY2UpIHtcbiAgICAgIG9zVmVyc2lvbiA9IGdldEZpcnN0TWF0Y2goL29zIChcXGQrKFtfXFxzXVxcZCspKikgbGlrZSBtYWMgb3MgeC9pKTtcbiAgICAgIG9zVmVyc2lvbiA9IG9zVmVyc2lvbi5yZXBsYWNlKC9bX1xcc10vZywgJy4nKTtcbiAgICB9IGVsc2UgaWYgKGFuZHJvaWQpIHtcbiAgICAgIG9zVmVyc2lvbiA9IGdldEZpcnN0TWF0Y2goL2FuZHJvaWRbIFxcLy1dKFxcZCsoXFwuXFxkKykqKS9pKTtcbiAgICB9IGVsc2UgaWYgKHJlc3VsdC53ZWJvcykge1xuICAgICAgb3NWZXJzaW9uID0gZ2V0Rmlyc3RNYXRjaCgvKD86d2VifGhwdylvc1xcLyhcXGQrKFxcLlxcZCspKikvaSk7XG4gICAgfSBlbHNlIGlmIChyZXN1bHQuYmxhY2tiZXJyeSkge1xuICAgICAgb3NWZXJzaW9uID0gZ2V0Rmlyc3RNYXRjaCgvcmltXFxzdGFibGV0XFxzb3NcXHMoXFxkKyhcXC5cXGQrKSopL2kpO1xuICAgIH0gZWxzZSBpZiAocmVzdWx0LmJhZGEpIHtcbiAgICAgIG9zVmVyc2lvbiA9IGdldEZpcnN0TWF0Y2goL2JhZGFcXC8oXFxkKyhcXC5cXGQrKSopL2kpO1xuICAgIH0gZWxzZSBpZiAocmVzdWx0LnRpemVuKSB7XG4gICAgICBvc1ZlcnNpb24gPSBnZXRGaXJzdE1hdGNoKC90aXplbltcXC9cXHNdKFxcZCsoXFwuXFxkKykqKS9pKTtcbiAgICB9XG4gICAgaWYgKG9zVmVyc2lvbikge1xuICAgICAgcmVzdWx0Lm9zdmVyc2lvbiA9IG9zVmVyc2lvbjtcbiAgICB9XG5cbiAgICAvLyBkZXZpY2UgdHlwZSBleHRyYWN0aW9uXG4gICAgdmFyIG9zTWFqb3JWZXJzaW9uID0gIXJlc3VsdC53aW5kb3dzICYmIG9zVmVyc2lvbi5zcGxpdCgnLicpWzBdO1xuICAgIGlmIChcbiAgICAgICAgIHRhYmxldFxuICAgICAgfHwgbmV4dXNUYWJsZXRcbiAgICAgIHx8IGlvc2RldmljZSA9PSAnaXBhZCdcbiAgICAgIHx8IChhbmRyb2lkICYmIChvc01ham9yVmVyc2lvbiA9PSAzIHx8IChvc01ham9yVmVyc2lvbiA+PSA0ICYmICFtb2JpbGUpKSlcbiAgICAgIHx8IHJlc3VsdC5zaWxrXG4gICAgKSB7XG4gICAgICByZXN1bHQudGFibGV0ID0gdFxuICAgIH0gZWxzZSBpZiAoXG4gICAgICAgICBtb2JpbGVcbiAgICAgIHx8IGlvc2RldmljZSA9PSAnaXBob25lJ1xuICAgICAgfHwgaW9zZGV2aWNlID09ICdpcG9kJ1xuICAgICAgfHwgYW5kcm9pZFxuICAgICAgfHwgbmV4dXNNb2JpbGVcbiAgICAgIHx8IHJlc3VsdC5ibGFja2JlcnJ5XG4gICAgICB8fCByZXN1bHQud2Vib3NcbiAgICAgIHx8IHJlc3VsdC5iYWRhXG4gICAgKSB7XG4gICAgICByZXN1bHQubW9iaWxlID0gdFxuICAgIH1cblxuICAgIC8vIEdyYWRlZCBCcm93c2VyIFN1cHBvcnRcbiAgICAvLyBodHRwOi8vZGV2ZWxvcGVyLnlhaG9vLmNvbS95dWkvYXJ0aWNsZXMvZ2JzXG4gICAgaWYgKHJlc3VsdC5tc2VkZ2UgfHxcbiAgICAgICAgKHJlc3VsdC5tc2llICYmIHJlc3VsdC52ZXJzaW9uID49IDEwKSB8fFxuICAgICAgICAocmVzdWx0LnlhbmRleGJyb3dzZXIgJiYgcmVzdWx0LnZlcnNpb24gPj0gMTUpIHx8XG5cdFx0ICAgIChyZXN1bHQudml2YWxkaSAmJiByZXN1bHQudmVyc2lvbiA+PSAxLjApIHx8XG4gICAgICAgIChyZXN1bHQuY2hyb21lICYmIHJlc3VsdC52ZXJzaW9uID49IDIwKSB8fFxuICAgICAgICAocmVzdWx0LnNhbXN1bmdCcm93c2VyICYmIHJlc3VsdC52ZXJzaW9uID49IDQpIHx8XG4gICAgICAgIChyZXN1bHQud2hhbGUgJiYgY29tcGFyZVZlcnNpb25zKFtyZXN1bHQudmVyc2lvbiwgJzEuMCddKSA9PT0gMSkgfHxcbiAgICAgICAgKHJlc3VsdC5temJyb3dzZXIgJiYgY29tcGFyZVZlcnNpb25zKFtyZXN1bHQudmVyc2lvbiwgJzYuMCddKSA9PT0gMSkgfHxcbiAgICAgICAgKHJlc3VsdC5mb2N1cyAmJiBjb21wYXJlVmVyc2lvbnMoW3Jlc3VsdC52ZXJzaW9uLCAnMS4wJ10pID09PSAxKSB8fFxuICAgICAgICAocmVzdWx0LmZpcmVmb3ggJiYgcmVzdWx0LnZlcnNpb24gPj0gMjAuMCkgfHxcbiAgICAgICAgKHJlc3VsdC5zYWZhcmkgJiYgcmVzdWx0LnZlcnNpb24gPj0gNikgfHxcbiAgICAgICAgKHJlc3VsdC5vcGVyYSAmJiByZXN1bHQudmVyc2lvbiA+PSAxMC4wKSB8fFxuICAgICAgICAocmVzdWx0LmlvcyAmJiByZXN1bHQub3N2ZXJzaW9uICYmIHJlc3VsdC5vc3ZlcnNpb24uc3BsaXQoXCIuXCIpWzBdID49IDYpIHx8XG4gICAgICAgIChyZXN1bHQuYmxhY2tiZXJyeSAmJiByZXN1bHQudmVyc2lvbiA+PSAxMC4xKVxuICAgICAgICB8fCAocmVzdWx0LmNocm9taXVtICYmIHJlc3VsdC52ZXJzaW9uID49IDIwKVxuICAgICAgICApIHtcbiAgICAgIHJlc3VsdC5hID0gdDtcbiAgICB9XG4gICAgZWxzZSBpZiAoKHJlc3VsdC5tc2llICYmIHJlc3VsdC52ZXJzaW9uIDwgMTApIHx8XG4gICAgICAgIChyZXN1bHQuY2hyb21lICYmIHJlc3VsdC52ZXJzaW9uIDwgMjApIHx8XG4gICAgICAgIChyZXN1bHQuZmlyZWZveCAmJiByZXN1bHQudmVyc2lvbiA8IDIwLjApIHx8XG4gICAgICAgIChyZXN1bHQuc2FmYXJpICYmIHJlc3VsdC52ZXJzaW9uIDwgNikgfHxcbiAgICAgICAgKHJlc3VsdC5vcGVyYSAmJiByZXN1bHQudmVyc2lvbiA8IDEwLjApIHx8XG4gICAgICAgIChyZXN1bHQuaW9zICYmIHJlc3VsdC5vc3ZlcnNpb24gJiYgcmVzdWx0Lm9zdmVyc2lvbi5zcGxpdChcIi5cIilbMF0gPCA2KVxuICAgICAgICB8fCAocmVzdWx0LmNocm9taXVtICYmIHJlc3VsdC52ZXJzaW9uIDwgMjApXG4gICAgICAgICkge1xuICAgICAgcmVzdWx0LmMgPSB0XG4gICAgfSBlbHNlIHJlc3VsdC54ID0gdFxuXG4gICAgcmV0dXJuIHJlc3VsdFxuICB9XG5cbiAgdmFyIGJvd3NlciA9IGRldGVjdCh0eXBlb2YgbmF2aWdhdG9yICE9PSAndW5kZWZpbmVkJyA/IG5hdmlnYXRvci51c2VyQWdlbnQgfHwgJycgOiAnJylcblxuICBib3dzZXIudGVzdCA9IGZ1bmN0aW9uIChicm93c2VyTGlzdCkge1xuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgYnJvd3Nlckxpc3QubGVuZ3RoOyArK2kpIHtcbiAgICAgIHZhciBicm93c2VySXRlbSA9IGJyb3dzZXJMaXN0W2ldO1xuICAgICAgaWYgKHR5cGVvZiBicm93c2VySXRlbT09PSAnc3RyaW5nJykge1xuICAgICAgICBpZiAoYnJvd3Nlckl0ZW0gaW4gYm93c2VyKSB7XG4gICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG5cbiAgLyoqXG4gICAqIEdldCB2ZXJzaW9uIHByZWNpc2lvbnMgY291bnRcbiAgICpcbiAgICogQGV4YW1wbGVcbiAgICogICBnZXRWZXJzaW9uUHJlY2lzaW9uKFwiMS4xMC4zXCIpIC8vIDNcbiAgICpcbiAgICogQHBhcmFtICB7c3RyaW5nfSB2ZXJzaW9uXG4gICAqIEByZXR1cm4ge251bWJlcn1cbiAgICovXG4gIGZ1bmN0aW9uIGdldFZlcnNpb25QcmVjaXNpb24odmVyc2lvbikge1xuICAgIHJldHVybiB2ZXJzaW9uLnNwbGl0KFwiLlwiKS5sZW5ndGg7XG4gIH1cblxuICAvKipcbiAgICogQXJyYXk6Om1hcCBwb2x5ZmlsbFxuICAgKlxuICAgKiBAcGFyYW0gIHtBcnJheX0gYXJyXG4gICAqIEBwYXJhbSAge0Z1bmN0aW9ufSBpdGVyYXRvclxuICAgKiBAcmV0dXJuIHtBcnJheX1cbiAgICovXG4gIGZ1bmN0aW9uIG1hcChhcnIsIGl0ZXJhdG9yKSB7XG4gICAgdmFyIHJlc3VsdCA9IFtdLCBpO1xuICAgIGlmIChBcnJheS5wcm90b3R5cGUubWFwKSB7XG4gICAgICByZXR1cm4gQXJyYXkucHJvdG90eXBlLm1hcC5jYWxsKGFyciwgaXRlcmF0b3IpO1xuICAgIH1cbiAgICBmb3IgKGkgPSAwOyBpIDwgYXJyLmxlbmd0aDsgaSsrKSB7XG4gICAgICByZXN1bHQucHVzaChpdGVyYXRvcihhcnJbaV0pKTtcbiAgICB9XG4gICAgcmV0dXJuIHJlc3VsdDtcbiAgfVxuXG4gIC8qKlxuICAgKiBDYWxjdWxhdGUgYnJvd3NlciB2ZXJzaW9uIHdlaWdodFxuICAgKlxuICAgKiBAZXhhbXBsZVxuICAgKiAgIGNvbXBhcmVWZXJzaW9ucyhbJzEuMTAuMi4xJywgICcxLjguMi4xLjkwJ10pICAgIC8vIDFcbiAgICogICBjb21wYXJlVmVyc2lvbnMoWycxLjAxMC4yLjEnLCAnMS4wOS4yLjEuOTAnXSk7ICAvLyAxXG4gICAqICAgY29tcGFyZVZlcnNpb25zKFsnMS4xMC4yLjEnLCAgJzEuMTAuMi4xJ10pOyAgICAgLy8gMFxuICAgKiAgIGNvbXBhcmVWZXJzaW9ucyhbJzEuMTAuMi4xJywgICcxLjA4MDAuMiddKTsgICAgIC8vIC0xXG4gICAqXG4gICAqIEBwYXJhbSAge0FycmF5PFN0cmluZz59IHZlcnNpb25zIHZlcnNpb25zIHRvIGNvbXBhcmVcbiAgICogQHJldHVybiB7TnVtYmVyfSBjb21wYXJpc29uIHJlc3VsdFxuICAgKi9cbiAgZnVuY3Rpb24gY29tcGFyZVZlcnNpb25zKHZlcnNpb25zKSB7XG4gICAgLy8gMSkgZ2V0IGNvbW1vbiBwcmVjaXNpb24gZm9yIGJvdGggdmVyc2lvbnMsIGZvciBleGFtcGxlIGZvciBcIjEwLjBcIiBhbmQgXCI5XCIgaXQgc2hvdWxkIGJlIDJcbiAgICB2YXIgcHJlY2lzaW9uID0gTWF0aC5tYXgoZ2V0VmVyc2lvblByZWNpc2lvbih2ZXJzaW9uc1swXSksIGdldFZlcnNpb25QcmVjaXNpb24odmVyc2lvbnNbMV0pKTtcbiAgICB2YXIgY2h1bmtzID0gbWFwKHZlcnNpb25zLCBmdW5jdGlvbiAodmVyc2lvbikge1xuICAgICAgdmFyIGRlbHRhID0gcHJlY2lzaW9uIC0gZ2V0VmVyc2lvblByZWNpc2lvbih2ZXJzaW9uKTtcblxuICAgICAgLy8gMikgXCI5XCIgLT4gXCI5LjBcIiAoZm9yIHByZWNpc2lvbiA9IDIpXG4gICAgICB2ZXJzaW9uID0gdmVyc2lvbiArIG5ldyBBcnJheShkZWx0YSArIDEpLmpvaW4oXCIuMFwiKTtcblxuICAgICAgLy8gMykgXCI5LjBcIiAtPiBbXCIwMDAwMDAwMDBcIlwiLCBcIjAwMDAwMDAwOVwiXVxuICAgICAgcmV0dXJuIG1hcCh2ZXJzaW9uLnNwbGl0KFwiLlwiKSwgZnVuY3Rpb24gKGNodW5rKSB7XG4gICAgICAgIHJldHVybiBuZXcgQXJyYXkoMjAgLSBjaHVuay5sZW5ndGgpLmpvaW4oXCIwXCIpICsgY2h1bms7XG4gICAgICB9KS5yZXZlcnNlKCk7XG4gICAgfSk7XG5cbiAgICAvLyBpdGVyYXRlIGluIHJldmVyc2Ugb3JkZXIgYnkgcmV2ZXJzZWQgY2h1bmtzIGFycmF5XG4gICAgd2hpbGUgKC0tcHJlY2lzaW9uID49IDApIHtcbiAgICAgIC8vIDQpIGNvbXBhcmU6IFwiMDAwMDAwMDA5XCIgPiBcIjAwMDAwMDAxMFwiID0gZmFsc2UgKGJ1dCBcIjlcIiA+IFwiMTBcIiA9IHRydWUpXG4gICAgICBpZiAoY2h1bmtzWzBdW3ByZWNpc2lvbl0gPiBjaHVua3NbMV1bcHJlY2lzaW9uXSkge1xuICAgICAgICByZXR1cm4gMTtcbiAgICAgIH1cbiAgICAgIGVsc2UgaWYgKGNodW5rc1swXVtwcmVjaXNpb25dID09PSBjaHVua3NbMV1bcHJlY2lzaW9uXSkge1xuICAgICAgICBpZiAocHJlY2lzaW9uID09PSAwKSB7XG4gICAgICAgICAgLy8gYWxsIHZlcnNpb24gY2h1bmtzIGFyZSBzYW1lXG4gICAgICAgICAgcmV0dXJuIDA7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIGVsc2Uge1xuICAgICAgICByZXR1cm4gLTE7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIENoZWNrIGlmIGJyb3dzZXIgaXMgdW5zdXBwb3J0ZWRcbiAgICpcbiAgICogQGV4YW1wbGVcbiAgICogICBib3dzZXIuaXNVbnN1cHBvcnRlZEJyb3dzZXIoe1xuICAgKiAgICAgbXNpZTogXCIxMFwiLFxuICAgKiAgICAgZmlyZWZveDogXCIyM1wiLFxuICAgKiAgICAgY2hyb21lOiBcIjI5XCIsXG4gICAqICAgICBzYWZhcmk6IFwiNS4xXCIsXG4gICAqICAgICBvcGVyYTogXCIxNlwiLFxuICAgKiAgICAgcGhhbnRvbTogXCI1MzRcIlxuICAgKiAgIH0pO1xuICAgKlxuICAgKiBAcGFyYW0gIHtPYmplY3R9ICBtaW5WZXJzaW9ucyBtYXAgb2YgbWluaW1hbCB2ZXJzaW9uIHRvIGJyb3dzZXJcbiAgICogQHBhcmFtICB7Qm9vbGVhbn0gW3N0cmljdE1vZGUgPSBmYWxzZV0gZmxhZyB0byByZXR1cm4gZmFsc2UgaWYgYnJvd3NlciB3YXNuJ3QgZm91bmQgaW4gbWFwXG4gICAqIEBwYXJhbSAge1N0cmluZ30gIFt1YV0gdXNlciBhZ2VudCBzdHJpbmdcbiAgICogQHJldHVybiB7Qm9vbGVhbn1cbiAgICovXG4gIGZ1bmN0aW9uIGlzVW5zdXBwb3J0ZWRCcm93c2VyKG1pblZlcnNpb25zLCBzdHJpY3RNb2RlLCB1YSkge1xuICAgIHZhciBfYm93c2VyID0gYm93c2VyO1xuXG4gICAgLy8gbWFrZSBzdHJpY3RNb2RlIHBhcmFtIG9wdGlvbmFsIHdpdGggdWEgcGFyYW0gdXNhZ2VcbiAgICBpZiAodHlwZW9mIHN0cmljdE1vZGUgPT09ICdzdHJpbmcnKSB7XG4gICAgICB1YSA9IHN0cmljdE1vZGU7XG4gICAgICBzdHJpY3RNb2RlID0gdm9pZCgwKTtcbiAgICB9XG5cbiAgICBpZiAoc3RyaWN0TW9kZSA9PT0gdm9pZCgwKSkge1xuICAgICAgc3RyaWN0TW9kZSA9IGZhbHNlO1xuICAgIH1cbiAgICBpZiAodWEpIHtcbiAgICAgIF9ib3dzZXIgPSBkZXRlY3QodWEpO1xuICAgIH1cblxuICAgIHZhciB2ZXJzaW9uID0gXCJcIiArIF9ib3dzZXIudmVyc2lvbjtcbiAgICBmb3IgKHZhciBicm93c2VyIGluIG1pblZlcnNpb25zKSB7XG4gICAgICBpZiAobWluVmVyc2lvbnMuaGFzT3duUHJvcGVydHkoYnJvd3NlcikpIHtcbiAgICAgICAgaWYgKF9ib3dzZXJbYnJvd3Nlcl0pIHtcbiAgICAgICAgICBpZiAodHlwZW9mIG1pblZlcnNpb25zW2Jyb3dzZXJdICE9PSAnc3RyaW5nJykge1xuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdCcm93c2VyIHZlcnNpb24gaW4gdGhlIG1pblZlcnNpb24gbWFwIHNob3VsZCBiZSBhIHN0cmluZzogJyArIGJyb3dzZXIgKyAnOiAnICsgU3RyaW5nKG1pblZlcnNpb25zKSk7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgLy8gYnJvd3NlciB2ZXJzaW9uIGFuZCBtaW4gc3VwcG9ydGVkIHZlcnNpb24uXG4gICAgICAgICAgcmV0dXJuIGNvbXBhcmVWZXJzaW9ucyhbdmVyc2lvbiwgbWluVmVyc2lvbnNbYnJvd3Nlcl1dKSA8IDA7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gc3RyaWN0TW9kZTsgLy8gbm90IGZvdW5kXG4gIH1cblxuICAvKipcbiAgICogQ2hlY2sgaWYgYnJvd3NlciBpcyBzdXBwb3J0ZWRcbiAgICpcbiAgICogQHBhcmFtICB7T2JqZWN0fSBtaW5WZXJzaW9ucyBtYXAgb2YgbWluaW1hbCB2ZXJzaW9uIHRvIGJyb3dzZXJcbiAgICogQHBhcmFtICB7Qm9vbGVhbn0gW3N0cmljdE1vZGUgPSBmYWxzZV0gZmxhZyB0byByZXR1cm4gZmFsc2UgaWYgYnJvd3NlciB3YXNuJ3QgZm91bmQgaW4gbWFwXG4gICAqIEBwYXJhbSAge1N0cmluZ30gIFt1YV0gdXNlciBhZ2VudCBzdHJpbmdcbiAgICogQHJldHVybiB7Qm9vbGVhbn1cbiAgICovXG4gIGZ1bmN0aW9uIGNoZWNrKG1pblZlcnNpb25zLCBzdHJpY3RNb2RlLCB1YSkge1xuICAgIHJldHVybiAhaXNVbnN1cHBvcnRlZEJyb3dzZXIobWluVmVyc2lvbnMsIHN0cmljdE1vZGUsIHVhKTtcbiAgfVxuXG4gIGJvd3Nlci5pc1Vuc3VwcG9ydGVkQnJvd3NlciA9IGlzVW5zdXBwb3J0ZWRCcm93c2VyO1xuICBib3dzZXIuY29tcGFyZVZlcnNpb25zID0gY29tcGFyZVZlcnNpb25zO1xuICBib3dzZXIuY2hlY2sgPSBjaGVjaztcblxuICAvKlxuICAgKiBTZXQgb3VyIGRldGVjdCBtZXRob2QgdG8gdGhlIG1haW4gYm93c2VyIG9iamVjdCBzbyB3ZSBjYW5cbiAgICogcmV1c2UgaXQgdG8gdGVzdCBvdGhlciB1c2VyIGFnZW50cy5cbiAgICogVGhpcyBpcyBuZWVkZWQgdG8gaW1wbGVtZW50IGZ1dHVyZSB0ZXN0cy5cbiAgICovXG4gIGJvd3Nlci5fZGV0ZWN0ID0gZGV0ZWN0O1xuXG4gIC8qXG4gICAqIFNldCBvdXIgZGV0ZWN0IHB1YmxpYyBtZXRob2QgdG8gdGhlIG1haW4gYm93c2VyIG9iamVjdFxuICAgKiBUaGlzIGlzIG5lZWRlZCB0byBpbXBsZW1lbnQgYm93c2VyIGluIHNlcnZlciBzaWRlXG4gICAqL1xuICBib3dzZXIuZGV0ZWN0ID0gZGV0ZWN0O1xuICByZXR1cm4gYm93c2VyXG59KTtcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL2Jvd3Nlci9zcmMvYm93c2VyLmpzXG4vLyBtb2R1bGUgaWQgPSA3M1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDMgNCIsIi8qKlxyXG4gICogRW5jb2Rlci9EZWNvZGVyIGNsYXNzXHJcbiAgKiAoYykgQVNvZnQgTHRkLCAyMDA5LTIwMTEgKGh0dHA6Ly9hc29mdC5ydSlcclxuICAqIFNvbWUgaWRlYXMgd2VyZSB0YWtlbiBmcm9tIEJhc2U2NCBjbGFzcyBib3Jyb3dlZCBmcm9tIGh0dHA6Ly93ZWJ0b29sa2l0LmluZm9cclxuICAqIEFkYXB0ZWQgZm9yIFdlYnBhY2sgYnkgQ29tcHV0ZXJpY2FcclxuICAqXHJcbiAgKiBXb3JrcyBmYXN0IGluIE1vemlsbGEvQ2hyb21lL1NhZmFyaSwgYW5kIHBlcmZvcm1hbmNlIHVuZGVyIElFIGlzIG11Y2ggYmV0dGVyXHJcbiAgKiB0aGFuIG9yaWdpbmFsIEJhc2U2NCBlbmNvZGVyL2RlY29kZXJcclxuICAqKi9cclxuaW1wb3J0IGJvd3NlciBmcm9tICdib3dzZXInXHJcblxyXG52YXIgRW5jb2RlciA9IHtcclxuXHRzS2V5U3RyXzogXCJBQkNERUZHSElKS0xNTk9QUVJTVFVWV1hZWmFiY2RlZmdoaWprbG1ub3BxcnN0dXZ3eHl6MDEyMzQ1Njc4OSsvPVwiLFxyXG5cdGJJbml0aWFsaXplZF86IGZhbHNlLFxyXG5cdFxyXG5cdGluaXRpYWxpemU6IGZ1bmN0aW9uKCkge1xyXG5cdFx0aWYgKCF0aGlzLmJJbml0aWFsaXplZF8pIHtcclxuXHRcdFx0Ly8gcGVyZm9ybSBpbml0aWFsaXphdGlvblxyXG5cdFx0XHR0aGlzLmFIYXNoVGFiXyA9IG5ldyBBcnJheSgpO1xyXG5cdFx0XHRmb3IgKHZhciBpID0gMDsgaSA8IHRoaXMuc0tleVN0cl8ubGVuZ3RoOyArK2kpIHtcclxuXHRcdFx0XHR2YXIgYyA9IHRoaXMuc0tleVN0cl8uY2hhckNvZGVBdChpKTtcclxuXHRcdFx0XHR0aGlzLmFIYXNoVGFiX1tjXSA9IGk7IC8vIHN0b3JlIGNoYXIgaW5kZXhcclxuXHRcdFx0fVxyXG5cdFx0XHR0aGlzLmJJbml0aWFsaXplZF8gPSB0cnVlO1xyXG5cdFx0fVxyXG5cdH0sXHJcblxyXG5cdEJhc2U2NEVuY29kZTogZnVuY3Rpb24gKHNJbnB1dCkge1xyXG5cdFx0Ly8gSWYgYXZhaWxhYmxlLCB1c2UgTW96aWxsYS9TYWZhcmkvQ2hyb21lIGZhc3QgbmF0aXZlIGJhc2U2NCBlbmNvZGVyXHJcblx0XHRpZiAodHlwZW9mKGJ0b2EpICE9IFwidW5kZWZpbmVkXCIpIHJldHVybiBidG9hKHNJbnB1dCk7XHJcblx0XHRcclxuXHRcdC8vIGVuc3VyZSBoYXNoIHRhYmxlIGlzIGluaXRpYWxpemVkXHJcblx0XHR0aGlzLmluaXRpYWxpemUoKTtcclxuXHRcdHZhciBhT3V0ID0gbmV3IEFycmF5KCk7XHJcblx0XHR2YXIgY2hyMSwgY2hyMiwgY2hyMywgZW5jMSwgZW5jMiwgZW5jMywgZW5jNDtcclxuXHRcdHZhciBpID0gMDtcclxuXHRcdHZhciBkaSA9IDA7XHJcblx0XHR2YXIgbGVuID0gc0lucHV0Lmxlbmd0aDtcclxuXHRcdHdoaWxlIChpIDwgbGVuKSB7XHJcblx0XHRcdGNocjEgPSBzSW5wdXQuY2hhckNvZGVBdChpKyspO1xyXG5cdFx0XHRjaHIyID0gc0lucHV0LmNoYXJDb2RlQXQoaSsrKTtcclxuXHRcdFx0Y2hyMyA9IHNJbnB1dC5jaGFyQ29kZUF0KGkrKyk7XHJcblxyXG5cdFx0XHRlbmMxID0gY2hyMSA+PiAyO1xyXG5cdFx0XHRlbmMyID0gKChjaHIxICYgMykgPDwgNCkgfCAoY2hyMiA+PiA0KTtcclxuXHRcdFx0ZW5jMyA9ICgoY2hyMiAmIDE1KSA8PCAyKSB8IChjaHIzID4+IDYpO1xyXG5cdFx0XHRlbmM0ID0gY2hyMyAmIDYzO1xyXG5cclxuXHRcdFx0aWYgKGlzTmFOKGNocjIpKSB7XHJcblx0XHRcdFx0ZW5jMyA9IGVuYzQgPSA2NDtcclxuXHRcdFx0fSBlbHNlIGlmIChpc05hTihjaHIzKSkge1xyXG5cdFx0XHRcdGVuYzQgPSA2NDtcclxuXHRcdFx0fVxyXG5cdFx0XHRhT3V0W2RpKytdID0gdGhpcy5zS2V5U3RyXy5jaGFyQXQoZW5jMSk7XHJcblx0XHRcdGFPdXRbZGkrK10gPSB0aGlzLnNLZXlTdHJfLmNoYXJBdChlbmMyKTtcclxuXHRcdFx0YU91dFtkaSsrXSA9IHRoaXMuc0tleVN0cl8uY2hhckF0KGVuYzMpO1xyXG5cdFx0XHRhT3V0W2RpKytdID0gdGhpcy5zS2V5U3RyXy5jaGFyQXQoZW5jNCk7XHJcblx0XHR9XHJcblx0XHRyZXR1cm4gYU91dC5qb2luKCcnKTtcclxuXHR9LFxyXG5cclxuXHQvLyBwdWJsaWMgbWV0aG9kIGZvciBkZWNvZGluZ1xyXG5cdEJhc2U2NERlY29kZTogZnVuY3Rpb24gKHNJbnB1dCkge1xyXG5cdFx0Ly8gSWYgYXZhaWxhYmxlLCB1c2UgTW96aWxsYS9TYWZhcmkvQ2hyb21lIGZhc3QgbmF0aXZlIGRlY29kZXJcclxuXHRcdGlmICh0eXBlb2YoYXRvYikgIT0gXCJ1bmRlZmluZWRcIikgcmV0dXJuIGF0b2Ioc0lucHV0KTtcclxuXHRcdFxyXG5cdFx0Ly8gZW5zdXJlIGhhc2ggdGFibGUgaXMgaW5pdGlhbGl6ZWRcclxuXHRcdHRoaXMuaW5pdGlhbGl6ZSgpO1xyXG5cdFx0dmFyIGFPdXQgPSBuZXcgQXJyYXkoKTtcclxuXHRcdHZhciBjaHIxLCBjaHIyLCBjaHIzO1xyXG5cdFx0dmFyIGVuYzEsIGVuYzIsIGVuYzMsIGVuYzQ7XHJcblx0XHR2YXIgaSA9IDA7XHJcblx0XHRzSW5wdXQgPSBzSW5wdXQucmVwbGFjZSgvW15BLVphLXowLTlcXCtcXC9cXD1dL2csIFwiXCIpO1xyXG5cdFx0XHJcblx0XHR2YXIgbGVuID0gc0lucHV0Lmxlbmd0aDtcclxuXHRcdHZhciBkaSA9IDA7XHJcblx0XHR3aGlsZSAoaSA8IGxlbikge1xyXG5cdFx0XHRlbmMxID0gdGhpcy5hSGFzaFRhYl9bc0lucHV0LmNoYXJDb2RlQXQoaSsrKV07XHJcblx0XHRcdGVuYzIgPSB0aGlzLmFIYXNoVGFiX1tzSW5wdXQuY2hhckNvZGVBdChpKyspXTtcclxuXHRcdFx0ZW5jMyA9IHRoaXMuYUhhc2hUYWJfW3NJbnB1dC5jaGFyQ29kZUF0KGkrKyldO1xyXG5cdFx0XHRlbmM0ID0gdGhpcy5hSGFzaFRhYl9bc0lucHV0LmNoYXJDb2RlQXQoaSsrKV07XHJcblx0XHRcdFxyXG5cdFx0XHRjaHIxID0gKGVuYzEgPDwgMikgfCAoZW5jMiA+PiA0KTtcclxuXHRcdFx0Y2hyMiA9ICgoZW5jMiAmIDE1KSA8PCA0KSB8IChlbmMzID4+IDIpO1xyXG5cdFx0XHRjaHIzID0gKChlbmMzICYgMykgPDwgNikgfCBlbmM0O1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRhT3V0W2RpKytdID0gU3RyaW5nLmZyb21DaGFyQ29kZShjaHIxKTtcclxuXHRcdFx0aWYgKGVuYzMgIT0gNjQpIHtcclxuXHRcdFx0XHRhT3V0W2RpKytdID0gU3RyaW5nLmZyb21DaGFyQ29kZShjaHIyKTtcclxuXHRcdFx0fVxyXG5cdFx0XHRpZiAoZW5jNCAhPSA2NCkge1xyXG5cdFx0XHRcdGFPdXRbZGkrK10gPSBTdHJpbmcuZnJvbUNoYXJDb2RlKGNocjMpO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHRyZXR1cm4gYU91dC5qb2luKCcnKTtcclxuXHR9LFxyXG5cclxuXHRVVEY4RW5jb2RlOiBmdW5jdGlvbihzU3RyaW5nKSB7XHJcblx0XHQvL3NTdHJpbmcgPSBzU3RyaW5nLnJlcGxhY2UoL1xcclxcbi9nLFwiXFxuXCIpO1xyXG5cdFx0dmFyIGxlbiA9IHNTdHJpbmcubGVuZ3RoO1xyXG5cdFx0aWYgKGJvd3Nlci5tc2llKSB7XHJcblx0XHRcdHZhciBhQnl0ZXMgPSBuZXcgQXJyYXkoKTtcclxuXHRcdFx0dmFyIGRpID0gMDtcclxuXHRcdFx0Zm9yICh2YXIgbiA9IDA7IG4gPCBsZW47ICsrbikge1xyXG5cdFx0XHRcdHZhciBjID0gc1N0cmluZy5jaGFyQ29kZUF0KG4pO1xyXG5cdFx0XHRcdGlmIChjIDwgMTI4KSB7XHJcblx0XHRcdFx0XHRhQnl0ZXNbZGkrK10gPSBTdHJpbmcuZnJvbUNoYXJDb2RlKGMpO1xyXG5cdFx0XHRcdH0gZWxzZSBpZigoYyA+IDEyNykgJiYgKGMgPCAyMDQ4KSkge1xyXG5cdFx0XHRcdFx0YUJ5dGVzW2RpKytdID0gU3RyaW5nLmZyb21DaGFyQ29kZSgoYyA+PiA2KSB8IDE5Mik7XHJcblx0XHRcdFx0XHRhQnl0ZXNbZGkrK10gPSBTdHJpbmcuZnJvbUNoYXJDb2RlKChjICYgNjMpIHwgMTI4KTtcclxuXHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0YUJ5dGVzW2RpKytdICs9IFN0cmluZy5mcm9tQ2hhckNvZGUoKGMgPj4gMTIpIHwgMjI0KTtcclxuXHRcdFx0XHRcdGFCeXRlc1tkaSsrXSArPSBTdHJpbmcuZnJvbUNoYXJDb2RlKCgoYyA+PiA2KSAmIDYzKSB8IDEyOCk7XHJcblx0XHRcdFx0XHRhQnl0ZXNbZGkrK10gKz0gU3RyaW5nLmZyb21DaGFyQ29kZSgoYyAmIDYzKSB8IDEyOCk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHRcdHJldHVybiBhQnl0ZXMuam9pbignJyk7XHJcblx0XHR9IGVsc2Uge1xyXG5cdFx0XHR2YXIgc0J5dGVzID0gXCJcIjtcclxuXHRcdFx0Zm9yICh2YXIgbiA9IDA7IG4gPCBsZW47ICsrbikge1xyXG5cdFx0XHRcdHZhciBjID0gc1N0cmluZy5jaGFyQ29kZUF0KG4pO1xyXG5cdFx0XHRcdGlmIChjIDwgMTI4KSB7XHJcblx0XHRcdFx0XHRzQnl0ZXMgKz0gU3RyaW5nLmZyb21DaGFyQ29kZShjKTtcclxuXHRcdFx0XHR9IGVsc2UgaWYgKChjID4gMTI3KSAmJiAoYyA8IDIwNDgpKSB7XHJcblx0XHRcdFx0XHRzQnl0ZXMgKz0gU3RyaW5nLmZyb21DaGFyQ29kZSgoYyA+PiA2KSB8IDE5Mik7XHJcblx0XHRcdFx0XHRzQnl0ZXMgKz0gU3RyaW5nLmZyb21DaGFyQ29kZSgoYyAmIDYzKSB8IDEyOCk7XHJcblx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdHNCeXRlcyArPSBTdHJpbmcuZnJvbUNoYXJDb2RlKChjID4+IDEyKSB8IDIyNCk7XHJcblx0XHRcdFx0XHRzQnl0ZXMgKz0gU3RyaW5nLmZyb21DaGFyQ29kZSgoKGMgPj4gNikgJiA2MykgfCAxMjgpO1xyXG5cdFx0XHRcdFx0c0J5dGVzICs9IFN0cmluZy5mcm9tQ2hhckNvZGUoKGMgJiA2MykgfCAxMjgpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0XHRyZXR1cm4gc0J5dGVzO1xyXG5cdFx0fVxyXG5cdH0sXHJcblxyXG5cdFVURjhEZWNvZGU6IGZ1bmN0aW9uIChzQnl0ZXMpIHtcclxuXHRcdHZhciBpID0gMDtcclxuXHRcdHZhciBjMSA9IDA7XHJcblx0XHR2YXIgYzIgPSAwO1xyXG5cdFx0dmFyIGMzID0gMDtcclxuXHRcdHZhciBsZW4gPSBzQnl0ZXMubGVuZ3RoO1xyXG5cdFx0XHJcblx0XHRpZiAoYm93c2VyLm1zaSkge1xyXG5cdFx0XHQvLyBVc2UgYXJyYXkgam9pbiB0ZWNobmlxdWVcclxuXHRcdFx0dmFyIGFPdXQgPSBuZXcgQXJyYXkoKTtcclxuXHRcdFx0dmFyIGRpID0gMDtcclxuXHRcdFx0d2hpbGUgKGkgPCBsZW4gKSB7XHJcblx0XHRcdFx0YzEgPSBzQnl0ZXMuY2hhckNvZGVBdChpKTtcclxuXHRcdFx0XHRpZiAoYzEgPCAxMjgpIHtcclxuXHRcdFx0XHRcdGFPdXRbZGkrK10gPSBTdHJpbmcuZnJvbUNoYXJDb2RlKGMxKTtcclxuXHRcdFx0XHRcdGkrKztcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0ZWxzZSBpZiAoKGMxID4gMTkxKSAmJiAoYzEgPCAyMjQpKSB7XHJcblx0XHRcdFx0XHRjMiA9IHNCeXRlcy5jaGFyQ29kZUF0KGkrMSk7XHJcblx0XHRcdFx0XHRhT3V0W2RpKytdID0gU3RyaW5nLmZyb21DaGFyQ29kZSgoKGMxICYgMzEpIDw8IDYpIHwgKGMyICYgNjMpKTtcclxuXHRcdFx0XHRcdGkgKz0gMjtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0ZWxzZSB7XHJcblx0XHRcdFx0XHRjMiA9IHNCeXRlcy5jaGFyQ29kZUF0KGkrMSk7XHJcblx0XHRcdFx0XHRjMyA9IHNCeXRlcy5jaGFyQ29kZUF0KGkrMik7XHJcblx0XHRcdFx0XHRhT3V0W2RpKytdID0gU3RyaW5nLmZyb21DaGFyQ29kZSgoKGMxICYgMTUpIDw8IDEyKSB8ICgoYzIgJiA2MykgPDwgNikgfCAoYzMgJiA2MykpO1xyXG5cdFx0XHRcdFx0aSArPSAzO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0XHRyZXR1cm4gYU91dC5qb2luKCcnKTtcclxuXHRcdH0gZWxzZSB7XHJcblx0XHRcdC8vIFVzZSBzdGFuZGFyZCArPSBvcGVyYXRvciB0ZWNobmlxdWVcclxuXHRcdFx0dmFyIHNPdXQgPSBcIlwiO1xyXG5cdFx0XHR3aGlsZSAoaSA8IGxlbiApIHtcclxuXHRcdFx0XHRjMSA9IHNCeXRlcy5jaGFyQ29kZUF0KGkpO1xyXG5cdFx0XHRcdGlmIChjMSA8IDEyOCkge1xyXG5cdFx0XHRcdFx0c091dCArPSBTdHJpbmcuZnJvbUNoYXJDb2RlKGMxKTtcclxuXHRcdFx0XHRcdGkrKztcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0ZWxzZSBpZiAoKGMxID4gMTkxKSAmJiAoYzEgPCAyMjQpKSB7XHJcblx0XHRcdFx0XHRjMiA9IHNCeXRlcy5jaGFyQ29kZUF0KGkrMSk7XHJcblx0XHRcdFx0XHRzT3V0ICs9IFN0cmluZy5mcm9tQ2hhckNvZGUoKChjMSAmIDMxKSA8PCA2KSB8IChjMiAmIDYzKSk7XHJcblx0XHRcdFx0XHRpICs9IDI7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdGVsc2Uge1xyXG5cdFx0XHRcdFx0YzIgPSBzQnl0ZXMuY2hhckNvZGVBdChpKzEpO1xyXG5cdFx0XHRcdFx0YzMgPSBzQnl0ZXMuY2hhckNvZGVBdChpKzIpO1xyXG5cdFx0XHRcdFx0c091dCArPSBTdHJpbmcuZnJvbUNoYXJDb2RlKCgoYzEgJiAxNSkgPDwgMTIpIHwgKChjMiAmIDYzKSA8PCA2KSB8IChjMyAmIDYzKSk7XHJcblx0XHRcdFx0XHRpICs9IDM7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHRcdHJldHVybiBzT3V0O1xyXG5cdFx0fVxyXG5cdH1cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgRW5jb2RlcjtcclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHNyYy9saWIvYmFzZTY0LmpzIiwibW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbigpIHtcclxuXHR0aHJvdyBuZXcgRXJyb3IoXCJkZWZpbmUgY2Fubm90IGJlIHVzZWQgaW5kaXJlY3RcIik7XHJcbn07XHJcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vICh3ZWJwYWNrKS9idWlsZGluL2FtZC1kZWZpbmUuanNcbi8vIG1vZHVsZSBpZCA9IDc1XG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMyA0IiwiLyoqIVxuICogYWpheCAtIHYyLjMuM1xuICogQWpheCBtb2R1bGUgaW4gVmFuaWxsYSBKU1xuICogaHR0cHM6Ly9naXRodWIuY29tL2ZkYWNpdWsvYWpheFxuXG4gKiBUdWUgU2VwIDE4IDIwMTggMTI6NDQ6MDIgR01ULTAzMDAgKC0wMylcbiAqIE1JVCAoYykgRmVybmFuZG8gRGFjaXVrXG4qL1xuIWZ1bmN0aW9uKGUsdCl7XCJ1c2Ugc3RyaWN0XCI7XCJmdW5jdGlvblwiPT10eXBlb2YgZGVmaW5lJiZkZWZpbmUuYW1kP2RlZmluZShcImFqYXhcIix0KTpcIm9iamVjdFwiPT10eXBlb2YgZXhwb3J0cz9leHBvcnRzPW1vZHVsZS5leHBvcnRzPXQoKTplLmFqYXg9dCgpfSh0aGlzLGZ1bmN0aW9uKCl7XCJ1c2Ugc3RyaWN0XCI7ZnVuY3Rpb24gZShlKXt2YXIgcj1bXCJnZXRcIixcInBvc3RcIixcInB1dFwiLFwiZGVsZXRlXCJdO3JldHVybiBlPWV8fHt9LGUuYmFzZVVybD1lLmJhc2VVcmx8fFwiXCIsZS5tZXRob2QmJmUudXJsP24oZS5tZXRob2QsZS5iYXNlVXJsK2UudXJsLHQoZS5kYXRhKSxlKTpyLnJlZHVjZShmdW5jdGlvbihyLG8pe3JldHVybiByW29dPWZ1bmN0aW9uKHIsdSl7cmV0dXJuIG4obyxlLmJhc2VVcmwrcix0KHUpLGUpfSxyfSx7fSl9ZnVuY3Rpb24gdChlKXtyZXR1cm4gZXx8bnVsbH1mdW5jdGlvbiBuKGUsdCxuLHUpe3ZhciBjPVtcInRoZW5cIixcImNhdGNoXCIsXCJhbHdheXNcIl0saT1jLnJlZHVjZShmdW5jdGlvbihlLHQpe3JldHVybiBlW3RdPWZ1bmN0aW9uKG4pe3JldHVybiBlW3RdPW4sZX0sZX0se30pLGY9bmV3IFhNTEh0dHBSZXF1ZXN0LHA9cih0LG4sZSk7cmV0dXJuIGYub3BlbihlLHAsITApLGYud2l0aENyZWRlbnRpYWxzPXUuaGFzT3duUHJvcGVydHkoXCJ3aXRoQ3JlZGVudGlhbHNcIiksbyhmLHUuaGVhZGVycyxuKSxmLmFkZEV2ZW50TGlzdGVuZXIoXCJyZWFkeXN0YXRlY2hhbmdlXCIsYShpLGYpLCExKSxmLnNlbmQocyhuKT9KU09OLnN0cmluZ2lmeShuKTpuKSxpLmFib3J0PWZ1bmN0aW9uKCl7cmV0dXJuIGYuYWJvcnQoKX0saX1mdW5jdGlvbiByKGUsdCxuKXtpZihcImdldFwiIT09bi50b0xvd2VyQ2FzZSgpfHwhdClyZXR1cm4gZTt2YXIgcj1pKHQpLG89ZS5pbmRleE9mKFwiP1wiKT4tMT9cIiZcIjpcIj9cIjtyZXR1cm4gZStvK3J9ZnVuY3Rpb24gbyhlLHQsbil7dD10fHx7fSx1KHQpfHwodFtcIkNvbnRlbnQtVHlwZVwiXT1zKG4pP1wiYXBwbGljYXRpb24vanNvblwiOlwiYXBwbGljYXRpb24veC13d3ctZm9ybS11cmxlbmNvZGVkXCIpLE9iamVjdC5rZXlzKHQpLmZvckVhY2goZnVuY3Rpb24obil7dFtuXSYmZS5zZXRSZXF1ZXN0SGVhZGVyKG4sdFtuXSl9KX1mdW5jdGlvbiB1KGUpe3JldHVybiBPYmplY3Qua2V5cyhlKS5zb21lKGZ1bmN0aW9uKGUpe3JldHVyblwiY29udGVudC10eXBlXCI9PT1lLnRvTG93ZXJDYXNlKCl9KX1mdW5jdGlvbiBhKGUsdCl7cmV0dXJuIGZ1bmN0aW9uIG4oKXt0LnJlYWR5U3RhdGU9PT10LkRPTkUmJih0LnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJyZWFkeXN0YXRlY2hhbmdlXCIsbiwhMSksZS5hbHdheXMuYXBwbHkoZSxjKHQpKSx0LnN0YXR1cz49MjAwJiZ0LnN0YXR1czwzMDA/ZS50aGVuLmFwcGx5KGUsYyh0KSk6ZVtcImNhdGNoXCJdLmFwcGx5KGUsYyh0KSkpfX1mdW5jdGlvbiBjKGUpe3ZhciB0O3RyeXt0PUpTT04ucGFyc2UoZS5yZXNwb25zZVRleHQpfWNhdGNoKG4pe3Q9ZS5yZXNwb25zZVRleHR9cmV0dXJuW3QsZV19ZnVuY3Rpb24gaShlKXtyZXR1cm4gcyhlKT9mKGUpOmV9ZnVuY3Rpb24gcyhlKXtyZXR1cm5cIltvYmplY3QgT2JqZWN0XVwiPT09T2JqZWN0LnByb3RvdHlwZS50b1N0cmluZy5jYWxsKGUpfWZ1bmN0aW9uIGYoZSx0KXtyZXR1cm4gT2JqZWN0LmtleXMoZSkubWFwKGZ1bmN0aW9uKG4pe2lmKGUuaGFzT3duUHJvcGVydHkobikmJnZvaWQgMCE9PWVbbl0pe3ZhciByPWVbbl07cmV0dXJuIG49dD90K1wiW1wiK24rXCJdXCI6bixudWxsIT09ciYmXCJvYmplY3RcIj09dHlwZW9mIHI/ZihyLG4pOnAobikrXCI9XCIrcChyKX19KS5maWx0ZXIoQm9vbGVhbikuam9pbihcIiZcIil9ZnVuY3Rpb24gcChlKXtyZXR1cm4gZW5jb2RlVVJJQ29tcG9uZW50KGUpfXJldHVybiBlfSk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvQGZkYWNpdWsvYWpheC9kaXN0L2FqYXgubWluLmpzXG4vLyBtb2R1bGUgaWQgPSA3NlxuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDMgNCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQTtBQUNBO0FBRUE7Ozs7Ozs7Ozs7Ozs7OztBQ0hBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7OztBQUFBO0FBQ0E7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBV0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVJBO0FBVUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBMkVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7Ozs7OztBQ3JrQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7O0FDMW5CQTtBQUNBOzs7OztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBbExBOzs7Ozs7Ozs7QUFxTEE7Ozs7Ozs7QUNoTUE7QUFDQTtBQUNBOzs7Ozs7OztBQ0ZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBOzs7O0EiLCJzb3VyY2VSb290IjoiIn0=