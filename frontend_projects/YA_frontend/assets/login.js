webpackJsonp([3],{

/***/ 383:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.registerUser = exports.loginToPanel = undefined;

var _effi_protocol = __webpack_require__(72);

var effi = new _effi_protocol.EffiProtocol();

function gotoAdminPanel(language) {
	// console.log('go to admin')
	window.location.href = "/" + (language || "ru_RU") + "/";
}

function loginToPanel(opts) {
	opts.success = function (language) {
		effi.request({
			url: '/srv/Baloon/Person/GetCurrent',
			success: function success(resp) {
				console.log(resp, resp.id);
				if (resp.id) {
					// console.log('go to user')
					window.location.href = "/user#/tickets";
				} else {
					gotoAdminPanel(language);
				}
			},
			error: function error(resp) {
				gotoAdminPanel(language);
			}
		});
	};

	effi.logout();
	effi.auth(opts);
}

function registerUser(opts) {
	effi.request({
		url: '/nologin/srv/Baloon/Person/Register_API',
		data: (0, _effi_protocol.serializeAURL)(opts.data),
		success: function success(resp) {
			console.log(resp);
			if (opts.success) opts.success(resp);
		},
		error: function error(resp) {
			if (opts.error) opts.error(resp);
		}
	});
}

window.loginToPanel = loginToPanel;
window.registerUser = registerUser;
exports.loginToPanel = loginToPanel;
exports.registerUser = registerUser;

/***/ }),

/***/ 72:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.prepareDataList = exports.format_effi_time = exports.format_effi_date = exports.serializeAURLAsync = exports.serializeAURL = exports.EffiProtocol = undefined;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; // import jQuery from 'jquery'


var _base = __webpack_require__(77);

var _base2 = _interopRequireDefault(_base);

var _ajax = __webpack_require__(79);

var _ajax2 = _interopRequireDefault(_ajax);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// import Ajax from 'simple-ajax';

if (typeof String.prototype.startsWith != 'function') {
	String.prototype.startsWith = function (str) {
		return this.indexOf(str) === 0;
	};
}

if (!Array.isArray) {
	Array.isArray = function (arg) {
		return Object.prototype.toString.call(arg) === '[object Array]';
	};
}
function isObject(obj) {
	return Object.prototype.toString.call(obj) === '[object Object]';
}

var parseXml = void 0;

if (typeof window.DOMParser != "undefined") {
	parseXml = function parseXml(xmlStr) {
		return new window.DOMParser().parseFromString(xmlStr, "text/xml");
	};
} else if (typeof window.ActiveXObject != "undefined" && new window.ActiveXObject("Microsoft.XMLDOM")) {
	parseXml = function parseXml(xmlStr) {
		var xmlDoc = new window.ActiveXObject("Microsoft.XMLDOM");
		xmlDoc.async = "false";
		xmlDoc.loadXML(xmlStr);
		return xmlDoc;
	};
} else {
	throw new Error("No XML parser found");
}

function EffiProtocol(opts) {
	opts = opts || {};
	this.login = opts.login || 'barn';
	this.password = opts.password || (this.login == 'barn' ? 'barn' : '');
	this.language = 'ru_RU';
	this.host = opts.host || '';
	this.authenticated = false;
	this.event_subscribers = [];
	this.logout_subscribers = [];
	this.polling = false;

	var context = this;

	function parseStructure(obj) {
		var result = {};
		for (var i = 0; i < obj.children.length; i += 2) {
			var key = obj.children[i].textContent,
			    value = parseValue(obj.children[i + 1]);
			result[key] = value;
		}
		return result;
	}

	function parseArray(obj) {
		var result = [];
		for (var i = 0; i < obj.children.length; i++) {
			result[i] = parseValue(obj.children[i]);
		}
		return result;
	}

	function parseException(obj) {
		var result = {
			ExceptionText: "",
			ErrorCode: undefined
		};
		if (obj.children.length < 1) return result;
		result.ExceptionText = obj.children[0].textContent;
		if (obj.children.length > 1) result.ErrorCode = parseValue(obj.children[1]);
		return result;
	}

	function parseTime(val) {
		var found = val.match(/(\d\d\d\d)(\d\d)(\d\d)T(\d\d)(\d\d)(\d\d)/);
		if (found != null) {
			return new Date(parseInt(found[1]), parseInt(found[2]) - 1, parseInt(found[3]), parseInt(found[4]), parseInt(found[5]), parseInt(found[6]));
		}
		return null;
	}
	function parseDate(val) {
		var found = val.match(/(\d\d\d\d)(\d\d)(\d\d)/);
		if (found != null) {
			return new Date(parseInt(found[1]), parseInt(found[2]) - 1, parseInt(found[3]));
		}
		return null;
	}

	function parseValue(obj) {
		if (obj == null || null == obj.children[0]) return null;
		var container = obj.children[0];
		var nodeName = container.nodeName.toUpperCase();
		if (nodeName == 'STRUCTURE') return parseStructure(container);else if (nodeName == 'ARRAY') return parseArray(container);else if (nodeName == 'UL' || nodeName == 'OPTIONAL' || nodeName == 'U') return parseValue(container);else if (nodeName == 'INT64_T' || nodeName == 'INT') return parseInt(container.textContent);else if (nodeName == 'TIME') return parseTime(container.children[0].textContent);else if (nodeName == 'ADATE') return parseDate(container.children[0].textContent);else if (nodeName == 'DECIMAL') return parseFloat(container.textContent);else if (nodeName == 'DOUBLE') return parseFloat(container.textContent);else if (nodeName == 'EXCEPTION') return parseException(container);else if (nodeName == 'VALUE') return parseValue(container);else return container.textContent;
		return null;
	}

	function parseAXML(data) {
		var apacket_re = /APacket\(\d+ ,"","","","","",\{"ResponseTo":ul\(0 \)\},(.*)\)/;
		var result = data,
		    $xml = parseXml(data);
		if (data.startsWith('APacket')) {
			result = result.replace(apacket_re, '$1');
		}

		// var values = $xml.select('APacket > Value');
		var values = $xml.evaluate('//APacket/Value', $xml, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
		if (!values || values.snapshotLength == 0) {
			if ($xml.children.length < 1) return;
			return parseValue($xml.children[0]);
		}
		var content = values.snapshotItem(values.snapshotLength - 1);
		return parseValue(content);
	}

	function _parse(data, xhr) {
		var ct = xhr.getResponseHeader("content-type") || "";
		var res = data;
		if (ct.indexOf('text/xml') > -1) {
			res = parseAXML(data);
		} else if (ct.indexOf('application/json') > -1) {
			res = JSON.parse(data);
		}
		return res;
	}

	this.auth = function (opts) {
		var login = opts.login || context.login,
		    password = opts.password || context.password,
		    lang = opts.lang || 'ru_RU',
		    skin = opts.skin || 'materio',
		    time_zone = opts.time_zone || new Date().getTimezoneOffset();
		return new Promise(function (resolve, reject) {
			var request = (0, _ajax2.default)({
				url: context.host + '/auth/login',
				method: 'POST',
				dataType: 'text',
				data: 'Login=s:' + login + '&Password=s:' + password + '&Lang=s:' + lang + '&Skin=s:' + skin + '&TimeZone=i:' + time_zone + '&'
			}).then(function (resp) {
				var res = parseAXML(resp);
				context.authenticated = true;
				context.login = login;
				context.password = password;
				if (typeof res == 'string') context.language = res;
				if (typeof opts.success != 'undefined') opts.success(context.language, res);
				resolve(context.language, res);
			}).catch(function (resp) {
				var responseError = parseAXML(resp);
				console.error(responseError.ErrorCode, responseError.ExceptionText);
				if (typeof opts.error != 'undefined') opts.error(responseError);
				reject(responseError);
			});
		});
	};

	this.request = function (opts) {
		return new Promise(function (resolve, reject) {
			var data = opts.data || "dummy=none:&";
			(0, _ajax2.default)().post(context.host + opts.url, data).then(function (resp, xhr) {
				var res = _parse(resp, xhr);
				if (typeof opts.success != 'undefined') opts.success(res);
				resolve(res);
			}).catch(function (resp, xhr) {
				var responseError = parseAXML(resp);
				console.error(responseError.ErrorCode, responseError.ExceptionText);
				if (responseError.ErrorCode == 101 || responseError.ErrorCode == 100) {
					context.auth({
						success: function success() {
							context.request(opts).then(function (res) {
								resolve(res);
							}).catch(function (err) {
								reject(err);
							});
						},
						error: function error() {
							context.fireLogout();
						}
					});
				} else {
					if (typeof opts.error != 'undefined') opts.error(responseError);
					reject(responseError);
				}
			});
		});
	};

	this.onLogout = function (caller) {
		if (typeof caller != 'function') throw "EffiProtocol.onLogout: argument is not a function. ";
		this.logout_subscribers.push(caller);
	};
	this.fireLogout = function () {
		for (var i = 0; i <= this.logout_subscribers.length; i++) {
			var caller = this.logout_subscribers[i];
			caller(this);
		}
	};

	this.pollEvents = function (cnt) {
		cnt = cnt || 0;
		this.polling = true;
		(0, _ajax2.default)(context.host + '/srv/WWW/WWWWorker/GetEvent').get().then(function (data) {
			context.eventsTimeout = setTimeout(function () {
				context.pollEvents(1);
			}, 1);
			var res = parseAXML(data);
			// console.log('event success:', res);
			for (var e = 0; e < res.length; e++) {
				var event = res[e];
				// console.log(context.event_subscribers)
				for (var i = 0; i < context.event_subscribers.length; i++) {
					var subscr = context.event_subscribers[i];
					if (!subscr.type || subscr.type == '*' || subscr.type == event.Type) {
						subscr.callback(event);
					}
				}
			}
		}).catch(function (resp, xhr) {
			var responseError = parseAXML(resp);
			if (!responseError) {
				if (cnt < 4) {
					context.eventsTimeout = setTimeout(function () {
						context.pollEvents(cnt + 1);
					}, 2000);
				} else {
					clearTimeout(context.eventsTimeout);
					console.error("Connection to server lost. ");
				}
				return;
			}
			console.error(cnt, responseError.ErrorCode, responseError.ExceptionText);
			if (cnt < 4 && (responseError.ErrorCode == 101 || responseError.ErrorCode == 100)) {
				context.auth({
					success: function success() {
						console.log('auth done');
						context.pollEvents(1);
					}
				});
			} else {
				clearTimeout(context.eventsTimeout);
				console.error("Connection to server lost. ");
				throw responseError;
			}
		});
	};

	this.subscribe = function (ctx, event_name, callback) {
		if (typeof event_name == 'function') {
			event_name = '*';
			callback = event_name;
		}
		context.event_subscribers.push({ ctx: ctx, type: event_name, callback: callback });
		if (!context.polling) {
			if (typeof SubscribeOnEvent !== 'undefined') {
				// console.log('current window SubscribeOnEvent');
				SubscribeOnEvent(event_name, "document", context.processPrototypeEvent, context);
				context.polling = true;
			} else if (typeof window.opener !== 'undefined' && window.opener && typeof window.opener.SubscribeOnEvent !== 'undefined') {
				// console.log('parent window SubscribeOnEvent');
				window.opener.SubscribeOnEvent(event_name, "document", context.processPrototypeEvent, context);
				context.polling = true;
			} else {
				// console.log('separate window');
				context.eventsTimeout = setTimeout(function () {
					context.pollEvents(1);
				}, 1);
			}
		}
	};
	this.unsubscribe = function (ctx, event_name) {
		// console.log('unsubscribe', ctx, event_name)
		for (var i = 0; i < this.event_subscribers.length; i++) {
			var subscr = this.event_subscribers[i];
			if (subscr.ctx == ctx && (!event_name || event_name == '*' || event_name == subscr.type)) {
				// console.log('  drop subscriber', subscr);
				this.event_subscribers.splice(i, 1);
			}
		}
		// if (UnsubscribeFromEvent) {
		// 	UnsubscribeFromEvent(event_name, "document", this.processPrototypeEvent, this)
		// }
	};

	this.normalizePrototypeObject = function (obj) {
		var res = {};
		for (var k in obj) {
			if (_typeof(obj[k]) == 'object') {
				if ('oValue_' in obj[k]) {
					res[k] = obj[k].oValue_;
					if (_typeof(obj[k].oValue_) == 'object' && 'value_' in obj[k].oValue_) res[k] = obj[k].oValue_.value_;
				}
				// else if ('value_' in obj[k]) res[k] = obj[k].value_
				else res[k] = null;
			} else res[k] = obj[k];
		}
		return res;
	};
	this.processPrototypeEvent = function (e) {
		var event_data = this.normalizePrototypeObject(e.oEventData_),
		    event = { Type: e.sEventName_, Data: event_data };
		for (var i = 0; i < context.event_subscribers.length; i++) {
			var subscr = context.event_subscribers[i];
			if (!subscr.type || subscr.type == '*' || subscr.type.toUpperCase() == e.sEventName_) {
				subscr.callback(event);
			}
		}
	};

	function delete_cookie(name) {
		document.cookie = name + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
	}

	this.logout = function () {
		clearTimeout(context.eventsTimeout);
		delete_cookie('_1024_sid');
		this.authenticated = false;
	};
}

function paddy(n, p, c) {
	var pad_char = typeof c !== 'undefined' ? c : '0';
	var pad = new Array(1 + p).join(pad_char);
	return (pad + n).slice(-pad.length);
}
function format_effi_date(date) {
	if (date == null) return 'not-a-date';
	return paddy(date.getFullYear(), 4) + paddy(date.getMonth() + 1, 2) + paddy(date.getDate(), 2);
}
function format_effi_time(date) {
	if (date == null) return 'not-a-date-time';
	return paddy(date.getFullYear(), 4) + paddy(date.getMonth() + 1, 2) + paddy(date.getDate(), 2) + 'T' + paddy(date.getHours(), 2) + paddy(date.getMinutes(), 2) + paddy(date.getSeconds(), 2);
}

function encodeAURLComponent(obj) {
	var serialized = '';
	if (typeof obj == 'undefined' || obj == null) return 'none:&';else if (obj.type == 'array' || Array.isArray(obj)) {
		var s = '';
		for (var i = 0; i < obj.length; i++) {
			var o = obj[i];
			s += encodeAURLComponent(o);
		}
		serialized += 'Value:Array:' + s + '&&';
	} else if (obj.type == 'optionalInt') serialized = 'optional:i:' + obj.value + '&&';else if (obj.type == 'float' || typeof obj.value == 'float') serialized = 'd:' + obj.value + '&';else if (obj.type == 'int' || typeof obj.value == 'number') serialized = 'i:' + obj.value + '&';else if (obj.type == 'date' || obj.type == 'ADate') serialized = 'ADate:s:' + format_effi_date(obj.value) + '&&';else if (obj.type == 'datetime' || obj.type == 'Time' || obj.value instanceof Date) serialized = 'Time:s:' + format_effi_time(obj.value) + '&&';else if (obj.type == 'checkbox') serialized = 's:' + obj.value + '&';else if (obj.type == 'optionalString') serialized = 'optional:s:' + obj.value.replace(/ /g, '\\w') + '&&';else if (obj.type == 'binary') serialized = 'b:' + _base2.default.Base64Encode(_base2.default.UTF8Encode(obj.value)) + '&';else {
		var v = typeof obj.value == 'undefined' ? obj : obj.value;
		serialized = 's:' + v.replace(/ /g, '\\w') + '&';
	}
	return serialized;
}

function encodePlain(obj) {
	var serialized = '';
	if (typeof obj == 'undefined' || obj == null) return 'none:&';else if (obj.type == 'optionalInt') serialized = 'optional:i:' + obj.value + '&&';else if (obj.type == 'float' || typeof obj.value == 'float') serialized = 'd:' + obj.value + '&';else if (obj.type == 'int' || typeof obj.value == 'number') serialized = 'i:' + obj.value + '&';else if (obj.type == 'date' || obj.type == 'ADate') serialized = 'ADate:s:' + format_effi_date(obj.value) + '&&';else if (obj.type == 'datetime' || obj.type == 'Time' || obj.value instanceof Date) serialized = 'Time:s:' + format_effi_time(obj.value) + '&&';else if (obj.type == 'checkbox') serialized = 's:' + obj.value + '&';else if (obj.type == 'optionalString') serialized = 'optional:s:' + obj.value.replace(/ /g, '\\w') + '&&';else if (obj.type == 'binary') serialized = 'b:' + _base2.default.Base64Encode(obj.value) + '&';else {
		var v = typeof obj.value == 'undefined' ? obj : obj.value;
		serialized = 's:' + v.replace(/ /g, '\\w') + '&';
	}
	return serialized;
}
function encodeBlob(file, callback) {
	var reader = new FileReader();

	reader.onload = function (readerEvt) {
		console.log('loaded file size=' + readerEvt.target.result.length);
		var serialized = 'b:' + _base2.default.Base64Encode(readerEvt.target.result) + '&';
		callback(serialized);
	};

	reader.readAsBinaryString(file);
}
function encodeBlobFile(file, callback) {
	encodeBlob(file, function (serialized_blob) {
		var serialized = 'BlobFile:' + encodeAURLComponent({ value: file.name, type: 'string' }) + encodeAURLComponent({ value: file.type, type: 'string' }) + serialized_blob + encodeAURLComponent(null) + encodeAURLComponent({ value: file.lastModifiedDate, type: 'Time' }) + '&';
		callback(serialized);
	});
}

function reduceSerializationArray(serialized, array, i, callback) {
	if (array.length == 0) {
		callback(serialized);
		return;
	}
	var obj = array.splice(0, 1);
	encodeAURLComponentAsync(serialized, obj, function (v) {
		reduceSerializationArray(v, array, ++i, callback);
	});
}
function reduceSerializationStructure(serialized, array, object, i, callback) {
	if (array.length == 0) {
		callback(serialized);
		return;
	}
	var key = array.splice(0, 1);
	var obj = object[key];
	var s = serialized + key + '=';
	encodeAURLComponentAsync(s, obj, function (v) {
		reduceSerializationStructure(v, array, object, ++i, callback);
	});
}

function encodeAURLComponentAsync(serialized, obj, callback) {
	// console.log(obj);
	if (obj.type == 'BlobFile' || obj.value instanceof File) {
		encodeBlobFile(obj.value, function (serialized_file) {
			callback(serialized + serialized_file);
		});
	} else if (obj.type == 'Array' || Object.prototype.toString.call(obj) === '[object Array]') {
		var o = obj.value || obj;
		reduceSerializationArray(serialized + 'Value:Array:', o, 0, function (v) {
			callback(v + '&&');
		});
	} else if (obj.type == 'Structure') {
		var keys = [];
		for (var k in obj.value) {
			keys.push(k);
		}var s = serialized + 'Value:Structure:';
		reduceSerializationStructure(s, keys, obj.value, 0, function (v) {
			callback(v + '&&');
		});
	} else {
		var s3 = encodePlain(obj);
		callback(serialized + s3);
	}
}

function serializeAURL(a) {
	var result = '';
	for (var key in a) {
		var o = a[key];
		result += key + "=" + encodeAURLComponent(o);
	}
	return result;
};
function serializeAURLAsync(a, callback) {
	var keys = [];
	for (var k in a) {
		keys.push(k);
	}reduceSerializationStructure('', keys, a, 0, callback);
}

function prepareDataList(data) {
	if (!data || data.length == 0) throw "Invalid DataList argument. ";
	var header = data[0];
	var result = {
		header: header,
		data: []
	};
	for (var i = 1; i < data.length; i++) {
		var r = data[i],
		    row = {};
		for (var c = 0; c < r.length; c++) {
			row[header[c]] = r[c];
		}
		result.data.push(row);
	}
	return result;
}

/*
jQuery.fn.extend({
	serializeAURL: function() {
		return serializeAURL( this.serializeTypedArray() );
	},
	serializeTypedArray: function() {
		var rCRLF = /\r?\n/g,
			rsubmitterTypes = /^(?:submit|button|image|reset|file)$/i,
			rsubmittable = /^(?:input|select|textarea|keygen)/i,
			rcheckableType = /^(?:checkbox|radio)$/i;
		
		function getv(val, type, isArray) {
			type = type || 'string';
			var v = "";
			if (type == 'date') {
				var found = val.match(/(\d\d)\.(\d\d)\.(\d\d\d\d)( (\d\d):(\d\d)(:(\d\d))?)?/);
				if (found != null) {
					// var h = found[5] ? parseInt(found[5]) : undefined, 
					// 	m = found[6] ? parseInt(found[6]) : undefined, 
					// 	s = found[8] ? parseInt(found[8]) : undefined;
					v = new Date(parseInt(found[3]), parseInt(found[2])-1, parseInt(found[1]));
					// console.log(parseInt(found[3]), parseInt(found[2])-1, parseInt(found[1]), h, m, s, '->', v);
				}
				else v = null;
			}
			// else if (type == 'checkbox') 
			else if (type == 'int') v = parseInt(val);
			else if (type == 'checkbox' && !isArray) {
				v = (val == 'on' ? true : false);
			}
			else v = val.replace( rCRLF, "\r\n" );
			return v;
		}

		var list = this.map(function() {
			// Can add propHook for "elements" to filter or add form elements
			var elements = jQuery.prop( this, "elements" );
			return elements ? jQuery.makeArray( elements ) : this;
		})
		.filter(function() {
			var type = this.type;

			// Use .is( ":disabled" ) so that fieldset[disabled] works
			return this.name && !jQuery( this ).is( ":disabled" ) &&
				rsubmittable.test( this.nodeName ) && !rsubmitterTypes.test( type ) &&
				( this.checked || !rcheckableType.test( type ) );
		})
		.map(function( i, elem ) {
			var val = jQuery( this ).val(),
				type = jQuery( this ).attr('data-type') || this.type,
				isArray = jQuery( this ).attr('data-array') == 'true' || false;

			return val == null ?
				null :
				jQuery.isArray( val ) ?
					jQuery.map( val, function( val ) {
						return { name: elem.name, value: getv(val, type, isArray), type: type, isArray: isArray };
					}) :
					{ name: elem.name, value: getv(val, type, isArray), type: type, isArray: isArray };
		}).get();

		var s = {}
		for (var i=0; i<list.length; i++) {
			var o = list[i];
			if (o.isArray || o.name in s) {
				if (!(o.name in s)) s[o.name] = [o];
				else s[o.name].push(o);
			}
			else s[o.name] = o;
		}
		return s;
	}
});
*/

exports.EffiProtocol = EffiProtocol;
exports.serializeAURL = serializeAURL;
exports.serializeAURLAsync = serializeAURLAsync;
exports.format_effi_date = format_effi_date;
exports.format_effi_time = format_effi_time;
exports.prepareDataList = prepareDataList;

/***/ }),

/***/ 73:
/***/ (function(module, exports, __webpack_require__) {

/*!
 * Bowser - a browser detector
 * https://github.com/ded/bowser
 * MIT License | (c) Dustin Diaz 2015
 */

!function (root, name, definition) {
  if (typeof module != 'undefined' && module.exports) module.exports = definition()
  else if (true) __webpack_require__(78)(name, definition)
  else root[name] = definition()
}(this, 'bowser', function () {
  /**
    * See useragents.js for examples of navigator.userAgent
    */

  var t = true

  function detect(ua) {

    function getFirstMatch(regex) {
      var match = ua.match(regex);
      return (match && match.length > 1 && match[1]) || '';
    }

    function getSecondMatch(regex) {
      var match = ua.match(regex);
      return (match && match.length > 1 && match[2]) || '';
    }

    var iosdevice = getFirstMatch(/(ipod|iphone|ipad)/i).toLowerCase()
      , likeAndroid = /like android/i.test(ua)
      , android = !likeAndroid && /android/i.test(ua)
      , nexusMobile = /nexus\s*[0-6]\s*/i.test(ua)
      , nexusTablet = !nexusMobile && /nexus\s*[0-9]+/i.test(ua)
      , chromeos = /CrOS/.test(ua)
      , silk = /silk/i.test(ua)
      , sailfish = /sailfish/i.test(ua)
      , tizen = /tizen/i.test(ua)
      , webos = /(web|hpw)os/i.test(ua)
      , windowsphone = /windows phone/i.test(ua)
      , samsungBrowser = /SamsungBrowser/i.test(ua)
      , windows = !windowsphone && /windows/i.test(ua)
      , mac = !iosdevice && !silk && /macintosh/i.test(ua)
      , linux = !android && !sailfish && !tizen && !webos && /linux/i.test(ua)
      , edgeVersion = getFirstMatch(/edge\/(\d+(\.\d+)?)/i)
      , versionIdentifier = getFirstMatch(/version\/(\d+(\.\d+)?)/i)
      , tablet = /tablet/i.test(ua) && !/tablet pc/i.test(ua)
      , mobile = !tablet && /[^-]mobi/i.test(ua)
      , xbox = /xbox/i.test(ua)
      , result

    if (/opera/i.test(ua)) {
      //  an old Opera
      result = {
        name: 'Opera'
      , opera: t
      , version: versionIdentifier || getFirstMatch(/(?:opera|opr|opios)[\s\/](\d+(\.\d+)?)/i)
      }
    } else if (/opr\/|opios/i.test(ua)) {
      // a new Opera
      result = {
        name: 'Opera'
        , opera: t
        , version: getFirstMatch(/(?:opr|opios)[\s\/](\d+(\.\d+)?)/i) || versionIdentifier
      }
    }
    else if (/SamsungBrowser/i.test(ua)) {
      result = {
        name: 'Samsung Internet for Android'
        , samsungBrowser: t
        , version: versionIdentifier || getFirstMatch(/(?:SamsungBrowser)[\s\/](\d+(\.\d+)?)/i)
      }
    }
    else if (/coast/i.test(ua)) {
      result = {
        name: 'Opera Coast'
        , coast: t
        , version: versionIdentifier || getFirstMatch(/(?:coast)[\s\/](\d+(\.\d+)?)/i)
      }
    }
    else if (/yabrowser/i.test(ua)) {
      result = {
        name: 'Yandex Browser'
      , yandexbrowser: t
      , version: versionIdentifier || getFirstMatch(/(?:yabrowser)[\s\/](\d+(\.\d+)?)/i)
      }
    }
    else if (/ucbrowser/i.test(ua)) {
      result = {
          name: 'UC Browser'
        , ucbrowser: t
        , version: getFirstMatch(/(?:ucbrowser)[\s\/](\d+(?:\.\d+)+)/i)
      }
    }
    else if (/mxios/i.test(ua)) {
      result = {
        name: 'Maxthon'
        , maxthon: t
        , version: getFirstMatch(/(?:mxios)[\s\/](\d+(?:\.\d+)+)/i)
      }
    }
    else if (/epiphany/i.test(ua)) {
      result = {
        name: 'Epiphany'
        , epiphany: t
        , version: getFirstMatch(/(?:epiphany)[\s\/](\d+(?:\.\d+)+)/i)
      }
    }
    else if (/puffin/i.test(ua)) {
      result = {
        name: 'Puffin'
        , puffin: t
        , version: getFirstMatch(/(?:puffin)[\s\/](\d+(?:\.\d+)?)/i)
      }
    }
    else if (/sleipnir/i.test(ua)) {
      result = {
        name: 'Sleipnir'
        , sleipnir: t
        , version: getFirstMatch(/(?:sleipnir)[\s\/](\d+(?:\.\d+)+)/i)
      }
    }
    else if (/k-meleon/i.test(ua)) {
      result = {
        name: 'K-Meleon'
        , kMeleon: t
        , version: getFirstMatch(/(?:k-meleon)[\s\/](\d+(?:\.\d+)+)/i)
      }
    }
    else if (windowsphone) {
      result = {
        name: 'Windows Phone'
      , windowsphone: t
      }
      if (edgeVersion) {
        result.msedge = t
        result.version = edgeVersion
      }
      else {
        result.msie = t
        result.version = getFirstMatch(/iemobile\/(\d+(\.\d+)?)/i)
      }
    }
    else if (/msie|trident/i.test(ua)) {
      result = {
        name: 'Internet Explorer'
      , msie: t
      , version: getFirstMatch(/(?:msie |rv:)(\d+(\.\d+)?)/i)
      }
    } else if (chromeos) {
      result = {
        name: 'Chrome'
      , chromeos: t
      , chromeBook: t
      , chrome: t
      , version: getFirstMatch(/(?:chrome|crios|crmo)\/(\d+(\.\d+)?)/i)
      }
    } else if (/chrome.+? edge/i.test(ua)) {
      result = {
        name: 'Microsoft Edge'
      , msedge: t
      , version: edgeVersion
      }
    }
    else if (/vivaldi/i.test(ua)) {
      result = {
        name: 'Vivaldi'
        , vivaldi: t
        , version: getFirstMatch(/vivaldi\/(\d+(\.\d+)?)/i) || versionIdentifier
      }
    }
    else if (sailfish) {
      result = {
        name: 'Sailfish'
      , sailfish: t
      , version: getFirstMatch(/sailfish\s?browser\/(\d+(\.\d+)?)/i)
      }
    }
    else if (/seamonkey\//i.test(ua)) {
      result = {
        name: 'SeaMonkey'
      , seamonkey: t
      , version: getFirstMatch(/seamonkey\/(\d+(\.\d+)?)/i)
      }
    }
    else if (/firefox|iceweasel|fxios/i.test(ua)) {
      result = {
        name: 'Firefox'
      , firefox: t
      , version: getFirstMatch(/(?:firefox|iceweasel|fxios)[ \/](\d+(\.\d+)?)/i)
      }
      if (/\((mobile|tablet);[^\)]*rv:[\d\.]+\)/i.test(ua)) {
        result.firefoxos = t
      }
    }
    else if (silk) {
      result =  {
        name: 'Amazon Silk'
      , silk: t
      , version : getFirstMatch(/silk\/(\d+(\.\d+)?)/i)
      }
    }
    else if (/phantom/i.test(ua)) {
      result = {
        name: 'PhantomJS'
      , phantom: t
      , version: getFirstMatch(/phantomjs\/(\d+(\.\d+)?)/i)
      }
    }
    else if (/slimerjs/i.test(ua)) {
      result = {
        name: 'SlimerJS'
        , slimer: t
        , version: getFirstMatch(/slimerjs\/(\d+(\.\d+)?)/i)
      }
    }
    else if (/blackberry|\bbb\d+/i.test(ua) || /rim\stablet/i.test(ua)) {
      result = {
        name: 'BlackBerry'
      , blackberry: t
      , version: versionIdentifier || getFirstMatch(/blackberry[\d]+\/(\d+(\.\d+)?)/i)
      }
    }
    else if (webos) {
      result = {
        name: 'WebOS'
      , webos: t
      , version: versionIdentifier || getFirstMatch(/w(?:eb)?osbrowser\/(\d+(\.\d+)?)/i)
      };
      /touchpad\//i.test(ua) && (result.touchpad = t)
    }
    else if (/bada/i.test(ua)) {
      result = {
        name: 'Bada'
      , bada: t
      , version: getFirstMatch(/dolfin\/(\d+(\.\d+)?)/i)
      };
    }
    else if (tizen) {
      result = {
        name: 'Tizen'
      , tizen: t
      , version: getFirstMatch(/(?:tizen\s?)?browser\/(\d+(\.\d+)?)/i) || versionIdentifier
      };
    }
    else if (/qupzilla/i.test(ua)) {
      result = {
        name: 'QupZilla'
        , qupzilla: t
        , version: getFirstMatch(/(?:qupzilla)[\s\/](\d+(?:\.\d+)+)/i) || versionIdentifier
      }
    }
    else if (/chromium/i.test(ua)) {
      result = {
        name: 'Chromium'
        , chromium: t
        , version: getFirstMatch(/(?:chromium)[\s\/](\d+(?:\.\d+)?)/i) || versionIdentifier
      }
    }
    else if (/chrome|crios|crmo/i.test(ua)) {
      result = {
        name: 'Chrome'
        , chrome: t
        , version: getFirstMatch(/(?:chrome|crios|crmo)\/(\d+(\.\d+)?)/i)
      }
    }
    else if (android) {
      result = {
        name: 'Android'
        , version: versionIdentifier
      }
    }
    else if (/safari|applewebkit/i.test(ua)) {
      result = {
        name: 'Safari'
      , safari: t
      }
      if (versionIdentifier) {
        result.version = versionIdentifier
      }
    }
    else if (iosdevice) {
      result = {
        name : iosdevice == 'iphone' ? 'iPhone' : iosdevice == 'ipad' ? 'iPad' : 'iPod'
      }
      // WTF: version is not part of user agent in web apps
      if (versionIdentifier) {
        result.version = versionIdentifier
      }
    }
    else if(/googlebot/i.test(ua)) {
      result = {
        name: 'Googlebot'
      , googlebot: t
      , version: getFirstMatch(/googlebot\/(\d+(\.\d+))/i) || versionIdentifier
      }
    }
    else {
      result = {
        name: getFirstMatch(/^(.*)\/(.*) /),
        version: getSecondMatch(/^(.*)\/(.*) /)
     };
   }

    // set webkit or gecko flag for browsers based on these engines
    if (!result.msedge && /(apple)?webkit/i.test(ua)) {
      if (/(apple)?webkit\/537\.36/i.test(ua)) {
        result.name = result.name || "Blink"
        result.blink = t
      } else {
        result.name = result.name || "Webkit"
        result.webkit = t
      }
      if (!result.version && versionIdentifier) {
        result.version = versionIdentifier
      }
    } else if (!result.opera && /gecko\//i.test(ua)) {
      result.name = result.name || "Gecko"
      result.gecko = t
      result.version = result.version || getFirstMatch(/gecko\/(\d+(\.\d+)?)/i)
    }

    // set OS flags for platforms that have multiple browsers
    if (!result.windowsphone && !result.msedge && (android || result.silk)) {
      result.android = t
    } else if (!result.windowsphone && !result.msedge && iosdevice) {
      result[iosdevice] = t
      result.ios = t
    } else if (mac) {
      result.mac = t
    } else if (xbox) {
      result.xbox = t
    } else if (windows) {
      result.windows = t
    } else if (linux) {
      result.linux = t
    }

    function getWindowsVersion (s) {
      switch (s) {
        case 'NT': return 'NT'
        case 'XP': return 'XP'
        case 'NT 5.0': return '2000'
        case 'NT 5.1': return 'XP'
        case 'NT 5.2': return '2003'
        case 'NT 6.0': return 'Vista'
        case 'NT 6.1': return '7'
        case 'NT 6.2': return '8'
        case 'NT 6.3': return '8.1'
        case 'NT 10.0': return '10'
        default: return undefined
      }
    }

    // OS version extraction
    var osVersion = '';
    if (result.windows) {
      osVersion = getWindowsVersion(getFirstMatch(/Windows ((NT|XP)( \d\d?.\d)?)/i))
    } else if (result.windowsphone) {
      osVersion = getFirstMatch(/windows phone (?:os)?\s?(\d+(\.\d+)*)/i);
    } else if (result.mac) {
      osVersion = getFirstMatch(/Mac OS X (\d+([_\.\s]\d+)*)/i);
      osVersion = osVersion.replace(/[_\s]/g, '.');
    } else if (iosdevice) {
      osVersion = getFirstMatch(/os (\d+([_\s]\d+)*) like mac os x/i);
      osVersion = osVersion.replace(/[_\s]/g, '.');
    } else if (android) {
      osVersion = getFirstMatch(/android[ \/-](\d+(\.\d+)*)/i);
    } else if (result.webos) {
      osVersion = getFirstMatch(/(?:web|hpw)os\/(\d+(\.\d+)*)/i);
    } else if (result.blackberry) {
      osVersion = getFirstMatch(/rim\stablet\sos\s(\d+(\.\d+)*)/i);
    } else if (result.bada) {
      osVersion = getFirstMatch(/bada\/(\d+(\.\d+)*)/i);
    } else if (result.tizen) {
      osVersion = getFirstMatch(/tizen[\/\s](\d+(\.\d+)*)/i);
    }
    if (osVersion) {
      result.osversion = osVersion;
    }

    // device type extraction
    var osMajorVersion = !result.windows && osVersion.split('.')[0];
    if (
         tablet
      || nexusTablet
      || iosdevice == 'ipad'
      || (android && (osMajorVersion == 3 || (osMajorVersion >= 4 && !mobile)))
      || result.silk
    ) {
      result.tablet = t
    } else if (
         mobile
      || iosdevice == 'iphone'
      || iosdevice == 'ipod'
      || android
      || nexusMobile
      || result.blackberry
      || result.webos
      || result.bada
    ) {
      result.mobile = t
    }

    // Graded Browser Support
    // http://developer.yahoo.com/yui/articles/gbs
    if (result.msedge ||
        (result.msie && result.version >= 10) ||
        (result.yandexbrowser && result.version >= 15) ||
		    (result.vivaldi && result.version >= 1.0) ||
        (result.chrome && result.version >= 20) ||
        (result.samsungBrowser && result.version >= 4) ||
        (result.firefox && result.version >= 20.0) ||
        (result.safari && result.version >= 6) ||
        (result.opera && result.version >= 10.0) ||
        (result.ios && result.osversion && result.osversion.split(".")[0] >= 6) ||
        (result.blackberry && result.version >= 10.1)
        || (result.chromium && result.version >= 20)
        ) {
      result.a = t;
    }
    else if ((result.msie && result.version < 10) ||
        (result.chrome && result.version < 20) ||
        (result.firefox && result.version < 20.0) ||
        (result.safari && result.version < 6) ||
        (result.opera && result.version < 10.0) ||
        (result.ios && result.osversion && result.osversion.split(".")[0] < 6)
        || (result.chromium && result.version < 20)
        ) {
      result.c = t
    } else result.x = t

    return result
  }

  var bowser = detect(typeof navigator !== 'undefined' ? navigator.userAgent || '' : '')

  bowser.test = function (browserList) {
    for (var i = 0; i < browserList.length; ++i) {
      var browserItem = browserList[i];
      if (typeof browserItem=== 'string') {
        if (browserItem in bowser) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * Get version precisions count
   *
   * @example
   *   getVersionPrecision("1.10.3") // 3
   *
   * @param  {string} version
   * @return {number}
   */
  function getVersionPrecision(version) {
    return version.split(".").length;
  }

  /**
   * Array::map polyfill
   *
   * @param  {Array} arr
   * @param  {Function} iterator
   * @return {Array}
   */
  function map(arr, iterator) {
    var result = [], i;
    if (Array.prototype.map) {
      return Array.prototype.map.call(arr, iterator);
    }
    for (i = 0; i < arr.length; i++) {
      result.push(iterator(arr[i]));
    }
    return result;
  }

  /**
   * Calculate browser version weight
   *
   * @example
   *   compareVersions(['1.10.2.1',  '1.8.2.1.90'])    // 1
   *   compareVersions(['1.010.2.1', '1.09.2.1.90']);  // 1
   *   compareVersions(['1.10.2.1',  '1.10.2.1']);     // 0
   *   compareVersions(['1.10.2.1',  '1.0800.2']);     // -1
   *
   * @param  {Array<String>} versions versions to compare
   * @return {Number} comparison result
   */
  function compareVersions(versions) {
    // 1) get common precision for both versions, for example for "10.0" and "9" it should be 2
    var precision = Math.max(getVersionPrecision(versions[0]), getVersionPrecision(versions[1]));
    var chunks = map(versions, function (version) {
      var delta = precision - getVersionPrecision(version);

      // 2) "9" -> "9.0" (for precision = 2)
      version = version + new Array(delta + 1).join(".0");

      // 3) "9.0" -> ["000000000"", "000000009"]
      return map(version.split("."), function (chunk) {
        return new Array(20 - chunk.length).join("0") + chunk;
      }).reverse();
    });

    // iterate in reverse order by reversed chunks array
    while (--precision >= 0) {
      // 4) compare: "000000009" > "000000010" = false (but "9" > "10" = true)
      if (chunks[0][precision] > chunks[1][precision]) {
        return 1;
      }
      else if (chunks[0][precision] === chunks[1][precision]) {
        if (precision === 0) {
          // all version chunks are same
          return 0;
        }
      }
      else {
        return -1;
      }
    }
  }

  /**
   * Check if browser is unsupported
   *
   * @example
   *   bowser.isUnsupportedBrowser({
   *     msie: "10",
   *     firefox: "23",
   *     chrome: "29",
   *     safari: "5.1",
   *     opera: "16",
   *     phantom: "534"
   *   });
   *
   * @param  {Object}  minVersions map of minimal version to browser
   * @param  {Boolean} [strictMode = false] flag to return false if browser wasn't found in map
   * @param  {String}  [ua] user agent string
   * @return {Boolean}
   */
  function isUnsupportedBrowser(minVersions, strictMode, ua) {
    var _bowser = bowser;

    // make strictMode param optional with ua param usage
    if (typeof strictMode === 'string') {
      ua = strictMode;
      strictMode = void(0);
    }

    if (strictMode === void(0)) {
      strictMode = false;
    }
    if (ua) {
      _bowser = detect(ua);
    }

    var version = "" + _bowser.version;
    for (var browser in minVersions) {
      if (minVersions.hasOwnProperty(browser)) {
        if (_bowser[browser]) {
          if (typeof minVersions[browser] !== 'string') {
            throw new Error('Browser version in the minVersion map should be a string: ' + browser + ': ' + String(minVersions));
          }

          // browser version and min supported version.
          return compareVersions([version, minVersions[browser]]) < 0;
        }
      }
    }

    return strictMode; // not found
  }

  /**
   * Check if browser is supported
   *
   * @param  {Object} minVersions map of minimal version to browser
   * @param  {Boolean} [strictMode = false] flag to return false if browser wasn't found in map
   * @param  {String}  [ua] user agent string
   * @return {Boolean}
   */
  function check(minVersions, strictMode, ua) {
    return !isUnsupportedBrowser(minVersions, strictMode, ua);
  }

  bowser.isUnsupportedBrowser = isUnsupportedBrowser;
  bowser.compareVersions = compareVersions;
  bowser.check = check;

  /*
   * Set our detect method to the main bowser object so we can
   * reuse it to test other user agents.
   * This is needed to implement future tests.
   */
  bowser._detect = detect;

  return bowser
});


/***/ }),

/***/ 77:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _bowser = __webpack_require__(73);

var _bowser2 = _interopRequireDefault(_bowser);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Encoder = {
	sKeyStr_: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
	bInitialized_: false,

	initialize: function initialize() {
		if (!this.bInitialized_) {
			// perform initialization
			this.aHashTab_ = new Array();
			for (var i = 0; i < this.sKeyStr_.length; ++i) {
				var c = this.sKeyStr_.charCodeAt(i);
				this.aHashTab_[c] = i; // store char index
			}
			this.bInitialized_ = true;
		}
	},

	Base64Encode: function Base64Encode(sInput) {
		// If available, use Mozilla/Safari/Chrome fast native base64 encoder
		if (typeof btoa != "undefined") return btoa(sInput);

		// ensure hash table is initialized
		this.initialize();
		var aOut = new Array();
		var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
		var i = 0;
		var di = 0;
		var len = sInput.length;
		while (i < len) {
			chr1 = sInput.charCodeAt(i++);
			chr2 = sInput.charCodeAt(i++);
			chr3 = sInput.charCodeAt(i++);

			enc1 = chr1 >> 2;
			enc2 = (chr1 & 3) << 4 | chr2 >> 4;
			enc3 = (chr2 & 15) << 2 | chr3 >> 6;
			enc4 = chr3 & 63;

			if (isNaN(chr2)) {
				enc3 = enc4 = 64;
			} else if (isNaN(chr3)) {
				enc4 = 64;
			}
			aOut[di++] = this.sKeyStr_.charAt(enc1);
			aOut[di++] = this.sKeyStr_.charAt(enc2);
			aOut[di++] = this.sKeyStr_.charAt(enc3);
			aOut[di++] = this.sKeyStr_.charAt(enc4);
		}
		return aOut.join('');
	},

	// public method for decoding
	Base64Decode: function Base64Decode(sInput) {
		// If available, use Mozilla/Safari/Chrome fast native decoder
		if (typeof atob != "undefined") return atob(sInput);

		// ensure hash table is initialized
		this.initialize();
		var aOut = new Array();
		var chr1, chr2, chr3;
		var enc1, enc2, enc3, enc4;
		var i = 0;
		sInput = sInput.replace(/[^A-Za-z0-9\+\/\=]/g, "");

		var len = sInput.length;
		var di = 0;
		while (i < len) {
			enc1 = this.aHashTab_[sInput.charCodeAt(i++)];
			enc2 = this.aHashTab_[sInput.charCodeAt(i++)];
			enc3 = this.aHashTab_[sInput.charCodeAt(i++)];
			enc4 = this.aHashTab_[sInput.charCodeAt(i++)];

			chr1 = enc1 << 2 | enc2 >> 4;
			chr2 = (enc2 & 15) << 4 | enc3 >> 2;
			chr3 = (enc3 & 3) << 6 | enc4;

			aOut[di++] = String.fromCharCode(chr1);
			if (enc3 != 64) {
				aOut[di++] = String.fromCharCode(chr2);
			}
			if (enc4 != 64) {
				aOut[di++] = String.fromCharCode(chr3);
			}
		}
		return aOut.join('');
	},

	UTF8Encode: function UTF8Encode(sString) {
		//sString = sString.replace(/\r\n/g,"\n");
		var len = sString.length;
		if (_bowser2.default.msie) {
			var aBytes = new Array();
			var di = 0;
			for (var n = 0; n < len; ++n) {
				var c = sString.charCodeAt(n);
				if (c < 128) {
					aBytes[di++] = String.fromCharCode(c);
				} else if (c > 127 && c < 2048) {
					aBytes[di++] = String.fromCharCode(c >> 6 | 192);
					aBytes[di++] = String.fromCharCode(c & 63 | 128);
				} else {
					aBytes[di++] += String.fromCharCode(c >> 12 | 224);
					aBytes[di++] += String.fromCharCode(c >> 6 & 63 | 128);
					aBytes[di++] += String.fromCharCode(c & 63 | 128);
				}
			}
			return aBytes.join('');
		} else {
			var sBytes = "";
			for (var n = 0; n < len; ++n) {
				var c = sString.charCodeAt(n);
				if (c < 128) {
					sBytes += String.fromCharCode(c);
				} else if (c > 127 && c < 2048) {
					sBytes += String.fromCharCode(c >> 6 | 192);
					sBytes += String.fromCharCode(c & 63 | 128);
				} else {
					sBytes += String.fromCharCode(c >> 12 | 224);
					sBytes += String.fromCharCode(c >> 6 & 63 | 128);
					sBytes += String.fromCharCode(c & 63 | 128);
				}
			}
			return sBytes;
		}
	},

	UTF8Decode: function UTF8Decode(sBytes) {
		var i = 0;
		var c1 = 0;
		var c2 = 0;
		var c3 = 0;
		var len = sBytes.length;

		if (_bowser2.default.msi) {
			// Use array join technique
			var aOut = new Array();
			var di = 0;
			while (i < len) {
				c1 = sBytes.charCodeAt(i);
				if (c1 < 128) {
					aOut[di++] = String.fromCharCode(c1);
					i++;
				} else if (c1 > 191 && c1 < 224) {
					c2 = sBytes.charCodeAt(i + 1);
					aOut[di++] = String.fromCharCode((c1 & 31) << 6 | c2 & 63);
					i += 2;
				} else {
					c2 = sBytes.charCodeAt(i + 1);
					c3 = sBytes.charCodeAt(i + 2);
					aOut[di++] = String.fromCharCode((c1 & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
					i += 3;
				}
			}
			return aOut.join('');
		} else {
			// Use standard += operator technique
			var sOut = "";
			while (i < len) {
				c1 = sBytes.charCodeAt(i);
				if (c1 < 128) {
					sOut += String.fromCharCode(c1);
					i++;
				} else if (c1 > 191 && c1 < 224) {
					c2 = sBytes.charCodeAt(i + 1);
					sOut += String.fromCharCode((c1 & 31) << 6 | c2 & 63);
					i += 2;
				} else {
					c2 = sBytes.charCodeAt(i + 1);
					c3 = sBytes.charCodeAt(i + 2);
					sOut += String.fromCharCode((c1 & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
					i += 3;
				}
			}
			return sOut;
		}
	}
}; /**
     * Encoder/Decoder class
     * (c) ASoft Ltd, 2009-2011 (http://asoft.ru)
     * Some ideas were taken from Base64 class borrowed from http://webtoolkit.info
     * Adapted for Webpack by Computerica
     *
     * Works fast in Mozilla/Chrome/Safari, and performance under IE is much better
     * than original Base64 encoder/decoder
     **/
exports.default = Encoder;

/***/ }),

/***/ 78:
/***/ (function(module, exports) {

module.exports = function() {
	throw new Error("define cannot be used indirect");
};


/***/ }),

/***/ 79:
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_RESULT__;/**!
 * ajax - v2.3.0
 * Ajax module in Vanilla JS
 * https://github.com/fdaciuk/ajax

 * Sun Jul 23 2017 10:55:09 GMT-0300 (BRT)
 * MIT (c) Fernando Daciuk
*/
!function(e,t){"use strict"; true?!(__WEBPACK_AMD_DEFINE_FACTORY__ = (t),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.call(exports, __webpack_require__, exports, module)) :
				__WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)):"object"==typeof exports?exports=module.exports=t():e.ajax=t()}(this,function(){"use strict";function e(e){var r=["get","post","put","delete"];return e=e||{},e.baseUrl=e.baseUrl||"",e.method&&e.url?n(e.method,e.baseUrl+e.url,t(e.data),e):r.reduce(function(r,o){return r[o]=function(r,u){return n(o,e.baseUrl+r,t(u),e)},r},{})}function t(e){return e||null}function n(e,t,n,u){var c=["then","catch","always"],i=c.reduce(function(e,t){return e[t]=function(n){return e[t]=n,e},e},{}),f=new XMLHttpRequest,d=r(t,n,e);return f.open(e,d,!0),f.withCredentials=u.hasOwnProperty("withCredentials"),o(f,u.headers),f.addEventListener("readystatechange",a(i,f),!1),f.send(s(n)),i.abort=function(){return f.abort()},i}function r(e,t,n){if("get"!==n.toLowerCase()||!t)return e;var r=s(t),o=e.indexOf("?")>-1?"&":"?";return e+o+r}function o(e,t){t=t||{},u(t)||(t["Content-Type"]="application/x-www-form-urlencoded"),Object.keys(t).forEach(function(n){t[n]&&e.setRequestHeader(n,t[n])})}function u(e){return Object.keys(e).some(function(e){return"content-type"===e.toLowerCase()})}function a(e,t){return function n(){t.readyState===t.DONE&&(t.removeEventListener("readystatechange",n,!1),e.always.apply(e,c(t)),t.status>=200&&t.status<300?e.then.apply(e,c(t)):e["catch"].apply(e,c(t)))}}function c(e){var t;try{t=JSON.parse(e.responseText)}catch(n){t=e.responseText}return[t,e]}function s(e){return i(e)?f(e):e}function i(e){return"[object Object]"===Object.prototype.toString.call(e)}function f(e){return Object.keys(e).reduce(function(t,n){var r=t?t+"&":"";return r+d(n)+"="+d(e[n])},"")}function d(e){return encodeURIComponent(e)}return e});

/***/ })

},[383]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4uanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vc3JjL2xvZ2luLmpzIiwid2VicGFjazovLy9zcmMvbGliL2VmZmlfcHJvdG9jb2wuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2Jvd3Nlci9zcmMvYm93c2VyLmpzIiwid2VicGFjazovLy9zcmMvbGliL2Jhc2U2NC5qcyIsIndlYnBhY2s6Ly8vKHdlYnBhY2spL2J1aWxkaW4vYW1kLWRlZmluZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvQGZkYWNpdWsvYWpheC9kaXN0L2FqYXgubWluLmpzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEVmZmlQcm90b2NvbCwgc2VyaWFsaXplQVVSTCB9IGZyb20gJ2xpYi9lZmZpX3Byb3RvY29sJztcblxubGV0IGVmZmkgPSBuZXcgRWZmaVByb3RvY29sKCk7XG5cbmZ1bmN0aW9uIGdvdG9BZG1pblBhbmVsKGxhbmd1YWdlKSB7XG5cdC8vIGNvbnNvbGUubG9nKCdnbyB0byBhZG1pbicpXG5cdHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gXCIvXCIgKyAobGFuZ3VhZ2UgfHwgXCJydV9SVVwiKSArIFwiL1wiO1xufVxuXG5mdW5jdGlvbiBsb2dpblRvUGFuZWwob3B0cykge1xuXHRvcHRzLnN1Y2Nlc3MgPSBmdW5jdGlvbihsYW5ndWFnZSkge1xuXHRcdGVmZmkucmVxdWVzdCh7XG5cdFx0XHR1cmw6ICcvc3J2L0JhbG9vbi9QZXJzb24vR2V0Q3VycmVudCcsXG5cdFx0XHRzdWNjZXNzOiAocmVzcCkgPT4ge1xuXHRcdFx0XHRjb25zb2xlLmxvZyhyZXNwLCByZXNwLmlkKTtcblx0XHRcdFx0aWYgKHJlc3AuaWQpIHtcblx0XHRcdFx0XHQvLyBjb25zb2xlLmxvZygnZ28gdG8gdXNlcicpXG5cdFx0XHRcdFx0d2luZG93LmxvY2F0aW9uLmhyZWYgPSBcIi91c2VyIy90aWNrZXRzXCI7XG5cdFx0XHRcdH1cblx0XHRcdFx0ZWxzZSB7XG5cdFx0XHRcdFx0Z290b0FkbWluUGFuZWwobGFuZ3VhZ2UpO1xuXHRcdFx0XHR9XG5cblx0XHRcdH0sXG5cdFx0XHRlcnJvcjogKHJlc3ApID0+IHtcblx0XHRcdFx0Z290b0FkbWluUGFuZWwobGFuZ3VhZ2UpO1xuXHRcdFx0fVxuXHRcdH0pO1xuXHR9XG5cblx0ZWZmaS5sb2dvdXQoKTtcblx0ZWZmaS5hdXRoKG9wdHMpO1xufVxuXG5mdW5jdGlvbiByZWdpc3RlclVzZXIob3B0cykge1xuXHRlZmZpLnJlcXVlc3Qoe1xuXHRcdHVybDogJy9ub2xvZ2luL3Nydi9CYWxvb24vUGVyc29uL1JlZ2lzdGVyX0FQSScsXG5cdFx0ZGF0YTogc2VyaWFsaXplQVVSTChvcHRzLmRhdGEpLFxuXHRcdHN1Y2Nlc3M6IChyZXNwKSA9PiB7XG5cdFx0XHRjb25zb2xlLmxvZyhyZXNwKTtcblx0XHRcdGlmIChvcHRzLnN1Y2Nlc3MpIG9wdHMuc3VjY2VzcyhyZXNwKVxuXHRcdH0sXG5cdFx0ZXJyb3I6IChyZXNwKSA9PiB7XG5cdFx0XHRpZiAob3B0cy5lcnJvcikgb3B0cy5lcnJvcihyZXNwKTtcblx0XHR9XG5cdH0pO1xufVxuXG53aW5kb3cubG9naW5Ub1BhbmVsID0gbG9naW5Ub1BhbmVsO1xud2luZG93LnJlZ2lzdGVyVXNlciA9IHJlZ2lzdGVyVXNlcjtcbmV4cG9ydCB7IGxvZ2luVG9QYW5lbCwgcmVnaXN0ZXJVc2VyIH07XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHNyYy9sb2dpbi5qcyIsIi8vIGltcG9ydCBqUXVlcnkgZnJvbSAnanF1ZXJ5J1xuaW1wb3J0IEVuY29kZXIgZnJvbSAnLi9iYXNlNjQuanMnXG5pbXBvcnQgYWpheCBmcm9tICdAZmRhY2l1ay9hamF4J1xuLy8gaW1wb3J0IEFqYXggZnJvbSAnc2ltcGxlLWFqYXgnO1xuXG5pZiAodHlwZW9mIFN0cmluZy5wcm90b3R5cGUuc3RhcnRzV2l0aCAhPSAnZnVuY3Rpb24nKSB7XG5cdFN0cmluZy5wcm90b3R5cGUuc3RhcnRzV2l0aCA9IGZ1bmN0aW9uIChzdHIpIHtcblx0XHRyZXR1cm4gdGhpcy5pbmRleE9mKHN0cikgPT09IDA7XG5cdH07XG59XG5cbmlmICghQXJyYXkuaXNBcnJheSkge1xuXHRBcnJheS5pc0FycmF5ID0gZnVuY3Rpb24oYXJnKSB7XG5cdFx0cmV0dXJuIE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmcuY2FsbChhcmcpID09PSAnW29iamVjdCBBcnJheV0nO1xuXHR9O1xufVxuZnVuY3Rpb24gaXNPYmplY3Qob2JqKSB7XG4gICByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZy5jYWxsKG9iaikgPT09ICdbb2JqZWN0IE9iamVjdF0nO1xufVxuXG5cbmxldCBwYXJzZVhtbDtcblxuaWYgKHR5cGVvZiB3aW5kb3cuRE9NUGFyc2VyICE9IFwidW5kZWZpbmVkXCIpIHtcbiAgICBwYXJzZVhtbCA9IGZ1bmN0aW9uKHhtbFN0cikge1xuICAgICAgICByZXR1cm4gKCBuZXcgd2luZG93LkRPTVBhcnNlcigpICkucGFyc2VGcm9tU3RyaW5nKHhtbFN0ciwgXCJ0ZXh0L3htbFwiKTtcbiAgICB9O1xufSBlbHNlIGlmICh0eXBlb2Ygd2luZG93LkFjdGl2ZVhPYmplY3QgIT0gXCJ1bmRlZmluZWRcIiAmJlxuICAgICAgIG5ldyB3aW5kb3cuQWN0aXZlWE9iamVjdChcIk1pY3Jvc29mdC5YTUxET01cIikpIHtcbiAgICBwYXJzZVhtbCA9IGZ1bmN0aW9uKHhtbFN0cikge1xuICAgICAgICB2YXIgeG1sRG9jID0gbmV3IHdpbmRvdy5BY3RpdmVYT2JqZWN0KFwiTWljcm9zb2Z0LlhNTERPTVwiKTtcbiAgICAgICAgeG1sRG9jLmFzeW5jID0gXCJmYWxzZVwiO1xuICAgICAgICB4bWxEb2MubG9hZFhNTCh4bWxTdHIpO1xuICAgICAgICByZXR1cm4geG1sRG9jO1xuICAgIH07XG59IGVsc2Uge1xuICAgIHRocm93IG5ldyBFcnJvcihcIk5vIFhNTCBwYXJzZXIgZm91bmRcIik7XG59XG5cblxuZnVuY3Rpb24gRWZmaVByb3RvY29sKG9wdHMpIHtcblx0b3B0cyA9IG9wdHMgfHwge307XG5cdHRoaXMubG9naW4gPSBvcHRzLmxvZ2luIHx8ICdiYXJuJztcblx0dGhpcy5wYXNzd29yZCA9IG9wdHMucGFzc3dvcmQgfHwgKHRoaXMubG9naW4gPT0gJ2Jhcm4nID8gJ2Jhcm4nIDogJycpO1xuXHR0aGlzLmxhbmd1YWdlID0gJ3J1X1JVJztcblx0dGhpcy5ob3N0ID0gb3B0cy5ob3N0IHx8ICcnO1xuXHR0aGlzLmF1dGhlbnRpY2F0ZWQgPSBmYWxzZTtcblx0dGhpcy5ldmVudF9zdWJzY3JpYmVycyA9IFtdO1xuXHR0aGlzLmxvZ291dF9zdWJzY3JpYmVycyA9IFtdO1xuXHR0aGlzLnBvbGxpbmcgPSBmYWxzZTtcblxuXHR2YXIgY29udGV4dCA9IHRoaXM7XG5cblx0ZnVuY3Rpb24gcGFyc2VTdHJ1Y3R1cmUob2JqKSB7XG5cdFx0dmFyIHJlc3VsdCA9IHt9O1xuXHRcdGZvciAodmFyIGk9MDsgaTxvYmouY2hpbGRyZW4ubGVuZ3RoOyBpKz0yKSB7XG5cdFx0XHR2YXIga2V5ID0gb2JqLmNoaWxkcmVuW2ldLnRleHRDb250ZW50LFxuXHRcdFx0XHR2YWx1ZSA9IHBhcnNlVmFsdWUob2JqLmNoaWxkcmVuW2krMV0pO1xuXHRcdFx0cmVzdWx0W2tleV0gPSB2YWx1ZTtcblx0XHR9XG5cdFx0cmV0dXJuIHJlc3VsdDtcblx0fVxuXG5cdGZ1bmN0aW9uIHBhcnNlQXJyYXkob2JqKSB7XG5cdFx0dmFyIHJlc3VsdCA9IFtdO1xuXHRcdGZvciAodmFyIGk9MDsgaTxvYmouY2hpbGRyZW4ubGVuZ3RoOyBpKyspIHtcblx0XHRcdHJlc3VsdFtpXSA9IHBhcnNlVmFsdWUob2JqLmNoaWxkcmVuW2ldKTtcblx0XHR9XG5cdFx0cmV0dXJuIHJlc3VsdDtcblx0fVxuXG5cdGZ1bmN0aW9uIHBhcnNlRXhjZXB0aW9uKG9iaikge1xuXHRcdGxldCByZXN1bHQgPSB7XG5cdFx0XHRFeGNlcHRpb25UZXh0OiBcIlwiLFxuXHRcdFx0RXJyb3JDb2RlOiB1bmRlZmluZWRcblx0XHR9O1xuXHRcdGlmIChvYmouY2hpbGRyZW4ubGVuZ3RoIDwgMSkgcmV0dXJuIHJlc3VsdDtcblx0XHRyZXN1bHQuRXhjZXB0aW9uVGV4dCA9IG9iai5jaGlsZHJlblswXS50ZXh0Q29udGVudDtcblx0XHRpZiAob2JqLmNoaWxkcmVuLmxlbmd0aCA+IDEpIHJlc3VsdC5FcnJvckNvZGUgPSBwYXJzZVZhbHVlKG9iai5jaGlsZHJlblsxXSk7XG5cdFx0cmV0dXJuIHJlc3VsdDtcblx0fVxuXG5cdGZ1bmN0aW9uIHBhcnNlVGltZSh2YWwpIHtcblx0XHRsZXQgZm91bmQgPSB2YWwubWF0Y2goLyhcXGRcXGRcXGRcXGQpKFxcZFxcZCkoXFxkXFxkKVQoXFxkXFxkKShcXGRcXGQpKFxcZFxcZCkvKTtcblx0XHRpZiAoZm91bmQgIT0gbnVsbCkge1xuXHRcdFx0cmV0dXJuIG5ldyBEYXRlKHBhcnNlSW50KGZvdW5kWzFdKSwgcGFyc2VJbnQoZm91bmRbMl0pLTEsIHBhcnNlSW50KGZvdW5kWzNdKSwgcGFyc2VJbnQoZm91bmRbNF0pLCBwYXJzZUludChmb3VuZFs1XSksIHBhcnNlSW50KGZvdW5kWzZdKSk7XG5cdFx0fVxuXHRcdHJldHVybiBudWxsO1xuXHR9XG5cdGZ1bmN0aW9uIHBhcnNlRGF0ZSh2YWwpIHtcblx0XHRsZXQgZm91bmQgPSB2YWwubWF0Y2goLyhcXGRcXGRcXGRcXGQpKFxcZFxcZCkoXFxkXFxkKS8pO1xuXHRcdGlmIChmb3VuZCAhPSBudWxsKSB7XG5cdFx0XHRyZXR1cm4gbmV3IERhdGUocGFyc2VJbnQoZm91bmRbMV0pLCBwYXJzZUludChmb3VuZFsyXSktMSwgcGFyc2VJbnQoZm91bmRbM10pKTtcblx0XHR9XG5cdFx0cmV0dXJuIG51bGw7XG5cdH1cblxuXHRmdW5jdGlvbiBwYXJzZVZhbHVlKG9iaikge1xuXHRcdGlmIChvYmogPT0gbnVsbCB8fCBudWxsID09IG9iai5jaGlsZHJlblswXSkgcmV0dXJuIG51bGw7XG5cdFx0bGV0IGNvbnRhaW5lciA9IG9iai5jaGlsZHJlblswXTtcblx0XHRsZXQgbm9kZU5hbWUgPSBjb250YWluZXIubm9kZU5hbWUudG9VcHBlckNhc2UoKTtcblx0XHRpZiAobm9kZU5hbWUgPT0gJ1NUUlVDVFVSRScpIHJldHVybiBwYXJzZVN0cnVjdHVyZShjb250YWluZXIpO1xuXHRcdGVsc2UgaWYgKG5vZGVOYW1lID09ICdBUlJBWScpIHJldHVybiBwYXJzZUFycmF5KGNvbnRhaW5lcik7XG5cdFx0ZWxzZSBpZiAobm9kZU5hbWUgPT0gJ1VMJyB8fCBub2RlTmFtZSA9PSAnT1BUSU9OQUwnIHx8IG5vZGVOYW1lID09ICdVJykgcmV0dXJuIHBhcnNlVmFsdWUoY29udGFpbmVyKTtcblx0XHRlbHNlIGlmIChub2RlTmFtZSA9PSAnSU5UNjRfVCcgfHwgbm9kZU5hbWUgPT0gJ0lOVCcpIHJldHVybiBwYXJzZUludChjb250YWluZXIudGV4dENvbnRlbnQpO1xuXHRcdGVsc2UgaWYgKG5vZGVOYW1lID09ICdUSU1FJykgcmV0dXJuIHBhcnNlVGltZShjb250YWluZXIuY2hpbGRyZW5bMF0udGV4dENvbnRlbnQpO1xuXHRcdGVsc2UgaWYgKG5vZGVOYW1lID09ICdBREFURScpIHJldHVybiBwYXJzZURhdGUoY29udGFpbmVyLmNoaWxkcmVuWzBdLnRleHRDb250ZW50KTtcblx0XHRlbHNlIGlmIChub2RlTmFtZSA9PSAnREVDSU1BTCcpIHJldHVybiBwYXJzZUZsb2F0KGNvbnRhaW5lci50ZXh0Q29udGVudCk7XG5cdFx0ZWxzZSBpZiAobm9kZU5hbWUgPT0gJ0RPVUJMRScpIHJldHVybiBwYXJzZUZsb2F0KGNvbnRhaW5lci50ZXh0Q29udGVudCk7XG5cdFx0ZWxzZSBpZiAobm9kZU5hbWUgPT0gJ0VYQ0VQVElPTicpIHJldHVybiBwYXJzZUV4Y2VwdGlvbihjb250YWluZXIpO1xuXHRcdGVsc2UgaWYgKG5vZGVOYW1lID09ICdWQUxVRScpIHJldHVybiBwYXJzZVZhbHVlKGNvbnRhaW5lcik7XG5cdFx0ZWxzZSByZXR1cm4gY29udGFpbmVyLnRleHRDb250ZW50O1xuXHRcdHJldHVybiBudWxsO1xuXHR9XG5cblx0ZnVuY3Rpb24gcGFyc2VBWE1MKGRhdGEpIHtcblx0XHR2YXIgYXBhY2tldF9yZSA9IC9BUGFja2V0XFwoXFxkKyAsXCJcIixcIlwiLFwiXCIsXCJcIixcIlwiLFxce1wiUmVzcG9uc2VUb1wiOnVsXFwoMCBcXClcXH0sKC4qKVxcKS87XG5cdFx0dmFyIHJlc3VsdCA9IGRhdGEsXG5cdFx0XHQkeG1sID0gcGFyc2VYbWwoZGF0YSk7XG5cdFx0aWYgKGRhdGEuc3RhcnRzV2l0aCgnQVBhY2tldCcpKSB7XG5cdFx0XHRyZXN1bHQgPSByZXN1bHQucmVwbGFjZShhcGFja2V0X3JlLCAnJDEnKTtcblx0XHR9XG5cblx0XHQvLyB2YXIgdmFsdWVzID0gJHhtbC5zZWxlY3QoJ0FQYWNrZXQgPiBWYWx1ZScpO1xuXHRcdGxldCB2YWx1ZXMgPSAkeG1sLmV2YWx1YXRlKCAnLy9BUGFja2V0L1ZhbHVlJywgJHhtbCwgbnVsbCwgWFBhdGhSZXN1bHQuT1JERVJFRF9OT0RFX1NOQVBTSE9UX1RZUEUsIG51bGwpO1xuXHRcdGlmICghdmFsdWVzIHx8IHZhbHVlcy5zbmFwc2hvdExlbmd0aCA9PSAwKSB7XG5cdFx0XHRpZiAoJHhtbC5jaGlsZHJlbi5sZW5ndGggPCAxKSByZXR1cm47XG5cdFx0XHRyZXR1cm4gcGFyc2VWYWx1ZSgkeG1sLmNoaWxkcmVuWzBdKTtcblx0XHR9XG5cdFx0bGV0IGNvbnRlbnQgPSB2YWx1ZXMuc25hcHNob3RJdGVtKHZhbHVlcy5zbmFwc2hvdExlbmd0aC0xKTtcblx0XHRyZXR1cm4gcGFyc2VWYWx1ZShjb250ZW50KTtcblx0fVxuXG5cdGZ1bmN0aW9uIF9wYXJzZShkYXRhLCB4aHIpIHtcblx0XHR2YXIgY3QgPSB4aHIuZ2V0UmVzcG9uc2VIZWFkZXIoXCJjb250ZW50LXR5cGVcIikgfHwgXCJcIjtcblx0XHR2YXIgcmVzID0gZGF0YTtcblx0XHRpZiAoY3QuaW5kZXhPZigndGV4dC94bWwnKSA+IC0xKSB7XG5cdFx0XHRyZXMgPSBwYXJzZUFYTUwoZGF0YSk7XG5cdFx0fVxuXHRcdGVsc2UgaWYgKGN0LmluZGV4T2YoJ2FwcGxpY2F0aW9uL2pzb24nKSA+IC0xKSB7XG5cdFx0XHRyZXMgPSBKU09OLnBhcnNlKGRhdGEpO1xuXHRcdH1cblx0XHRyZXR1cm4gcmVzO1xuXHR9XG5cdFxuXHR0aGlzLmF1dGggPSBmdW5jdGlvbihvcHRzKSB7XG5cdFx0bGV0IGxvZ2luID0gb3B0cy5sb2dpbiB8fCBjb250ZXh0LmxvZ2luLFxuXHRcdFx0cGFzc3dvcmQgPSBvcHRzLnBhc3N3b3JkIHx8IGNvbnRleHQucGFzc3dvcmQsXG5cdFx0XHRsYW5nID0gb3B0cy5sYW5nIHx8ICdydV9SVScsXG5cdFx0XHRza2luID0gb3B0cy5za2luIHx8ICdtYXRlcmlvJyxcblx0XHRcdHRpbWVfem9uZSA9IG9wdHMudGltZV96b25lIHx8IG5ldyBEYXRlKCkuZ2V0VGltZXpvbmVPZmZzZXQoKTtcblx0XHRyZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuXHRcdFx0bGV0IHJlcXVlc3QgPSBhamF4KHtcblx0XHRcdFx0dXJsOiBjb250ZXh0Lmhvc3QgKyAnL2F1dGgvbG9naW4nLCBcblx0XHRcdFx0bWV0aG9kOiAnUE9TVCcsXG5cdFx0XHRcdGRhdGFUeXBlOiAndGV4dCcsXG5cdFx0XHRcdGRhdGE6IGBMb2dpbj1zOiR7bG9naW59JlBhc3N3b3JkPXM6JHtwYXNzd29yZH0mTGFuZz1zOiR7bGFuZ30mU2tpbj1zOiR7c2tpbn0mVGltZVpvbmU9aToke3RpbWVfem9uZX0mYFxuXHRcdFx0fSlcblx0XHRcdC50aGVuKChyZXNwKSA9PiB7XG5cdFx0XHRcdHZhciByZXMgPSBwYXJzZUFYTUwocmVzcCk7XG5cdFx0XHRcdGNvbnRleHQuYXV0aGVudGljYXRlZCA9IHRydWU7XG5cdFx0XHRcdGNvbnRleHQubG9naW4gPSBsb2dpbjtcblx0XHRcdFx0Y29udGV4dC5wYXNzd29yZCA9IHBhc3N3b3JkO1xuXHRcdFx0XHRpZiAodHlwZW9mIHJlcyA9PSAnc3RyaW5nJykgY29udGV4dC5sYW5ndWFnZSA9IHJlcztcblx0XHRcdFx0aWYgKHR5cGVvZiBvcHRzLnN1Y2Nlc3MgIT0gJ3VuZGVmaW5lZCcpIG9wdHMuc3VjY2Vzcyhjb250ZXh0Lmxhbmd1YWdlLCByZXMpO1xuXHRcdFx0XHRyZXNvbHZlKGNvbnRleHQubGFuZ3VhZ2UsIHJlcyk7XG5cdFx0XHR9KVxuXHRcdFx0LmNhdGNoKChyZXNwKSA9PiB7XG5cdFx0XHRcdHZhciByZXNwb25zZUVycm9yID0gcGFyc2VBWE1MKHJlc3ApO1xuXHRcdFx0XHRjb25zb2xlLmVycm9yKHJlc3BvbnNlRXJyb3IuRXJyb3JDb2RlLCByZXNwb25zZUVycm9yLkV4Y2VwdGlvblRleHQpO1xuXHRcdFx0XHRpZiAodHlwZW9mIG9wdHMuZXJyb3IgIT0gJ3VuZGVmaW5lZCcpIG9wdHMuZXJyb3IocmVzcG9uc2VFcnJvcik7XG5cdFx0XHRcdHJlamVjdChyZXNwb25zZUVycm9yKTtcblx0XHRcdH0pO1xuXHRcdH0pO1xuXHR9XG5cblx0dGhpcy5yZXF1ZXN0ID0gZnVuY3Rpb24gKG9wdHMpIHtcblx0XHRyZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuXHRcdFx0bGV0IGRhdGEgPSBvcHRzLmRhdGEgfHwgXCJkdW1teT1ub25lOiZcIjtcblx0XHRcdGFqYXgoKVxuXHRcdFx0XHQucG9zdChjb250ZXh0Lmhvc3QgKyBvcHRzLnVybCwgZGF0YSlcblx0XHRcdFx0LnRoZW4oKHJlc3AsIHhocikgPT4ge1xuXHRcdFx0XHRcdGxldCByZXMgPSBfcGFyc2UocmVzcCwgeGhyKTtcblx0XHRcdFx0XHRpZiAodHlwZW9mIG9wdHMuc3VjY2VzcyAhPSAndW5kZWZpbmVkJykgb3B0cy5zdWNjZXNzKHJlcyk7XG5cdFx0XHRcdFx0cmVzb2x2ZShyZXMpO1xuXHRcdFx0XHR9KVxuXHRcdFx0XHQuY2F0Y2goKHJlc3AsIHhocikgPT4ge1xuXHRcdFx0XHRcdHZhciByZXNwb25zZUVycm9yID0gcGFyc2VBWE1MKHJlc3ApO1xuXHRcdFx0XHRcdGNvbnNvbGUuZXJyb3IocmVzcG9uc2VFcnJvci5FcnJvckNvZGUsIHJlc3BvbnNlRXJyb3IuRXhjZXB0aW9uVGV4dCk7XG5cdFx0XHRcdFx0aWYgKHJlc3BvbnNlRXJyb3IuRXJyb3JDb2RlID09IDEwMSB8fCByZXNwb25zZUVycm9yLkVycm9yQ29kZSA9PSAxMDApIHtcblx0XHRcdFx0XHRcdGNvbnRleHQuYXV0aCh7XG5cdFx0XHRcdFx0XHRcdHN1Y2Nlc3M6ICgpID0+IHtcblx0XHRcdFx0XHRcdFx0XHRjb250ZXh0LnJlcXVlc3Qob3B0cylcblx0XHRcdFx0XHRcdFx0XHRcdC50aGVuKChyZXMpID0+IHsgcmVzb2x2ZShyZXMpOyB9KVxuXHRcdFx0XHRcdFx0XHRcdFx0LmNhdGNoKChlcnIpID0+IHsgcmVqZWN0KGVycik7IH0pXG5cdFx0XHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0XHRcdGVycm9yOiAoKSA9PiB7XG5cdFx0XHRcdFx0XHRcdFx0Y29udGV4dC5maXJlTG9nb3V0KCk7XG5cdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHRlbHNlIHtcblx0XHRcdFx0XHRcdGlmICh0eXBlb2Ygb3B0cy5lcnJvciAgIT0gJ3VuZGVmaW5lZCcpIG9wdHMuZXJyb3IocmVzcG9uc2VFcnJvcik7XG5cdFx0XHRcdFx0XHRyZWplY3QocmVzcG9uc2VFcnJvcik7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9KTtcblx0XHR9KTtcblx0fVxuXG5cdHRoaXMub25Mb2dvdXQgPSBmdW5jdGlvbiAoY2FsbGVyKSAge1xuXHRcdGlmICh0eXBlb2YgY2FsbGVyICE9ICdmdW5jdGlvbicpIHRocm93IFwiRWZmaVByb3RvY29sLm9uTG9nb3V0OiBhcmd1bWVudCBpcyBub3QgYSBmdW5jdGlvbi4gXCI7XG5cdFx0dGhpcy5sb2dvdXRfc3Vic2NyaWJlcnMucHVzaChjYWxsZXIpO1xuXHR9XG5cdHRoaXMuZmlyZUxvZ291dCA9IGZ1bmN0aW9uICgpIHtcblx0XHRmb3IgKGxldCBpPTA7IGk8PXRoaXMubG9nb3V0X3N1YnNjcmliZXJzLmxlbmd0aDsgaSsrKSB7XG5cdFx0XHRsZXQgY2FsbGVyID0gdGhpcy5sb2dvdXRfc3Vic2NyaWJlcnNbaV07XG5cdFx0XHRjYWxsZXIodGhpcyk7XG5cdFx0fVxuXHR9XG5cblx0dGhpcy5wb2xsRXZlbnRzID0gZnVuY3Rpb24gKGNudCkge1xuXHRcdGNudCA9IGNudCB8fCAwO1xuXHRcdHRoaXMucG9sbGluZyA9IHRydWU7XG5cdFx0YWpheChjb250ZXh0Lmhvc3QgKyAnL3Nydi9XV1cvV1dXV29ya2VyL0dldEV2ZW50Jylcblx0XHRcdC5nZXQoKVxuXHRcdFx0LnRoZW4oZnVuY3Rpb24gKGRhdGEpIHtcblx0XHRcdFx0Y29udGV4dC5ldmVudHNUaW1lb3V0ID0gc2V0VGltZW91dCgoKSA9PiB7Y29udGV4dC5wb2xsRXZlbnRzKDEpfSwgMSk7XG5cdFx0XHRcdGxldCByZXMgPSBwYXJzZUFYTUwoZGF0YSk7XG5cdFx0XHRcdC8vIGNvbnNvbGUubG9nKCdldmVudCBzdWNjZXNzOicsIHJlcyk7XG5cdFx0XHRcdGZvciAobGV0IGU9MDsgZTxyZXMubGVuZ3RoOyBlKyspIHtcblx0XHRcdFx0XHRsZXQgZXZlbnQgPSAgcmVzW2VdO1xuXHRcdFx0XHRcdC8vIGNvbnNvbGUubG9nKGNvbnRleHQuZXZlbnRfc3Vic2NyaWJlcnMpXG5cdFx0XHRcdFx0Zm9yIChsZXQgaT0wOyBpPGNvbnRleHQuZXZlbnRfc3Vic2NyaWJlcnMubGVuZ3RoOyBpKyspIHtcblx0XHRcdFx0XHRcdGxldCBzdWJzY3IgPSBjb250ZXh0LmV2ZW50X3N1YnNjcmliZXJzW2ldO1xuXHRcdFx0XHRcdFx0aWYgKCFzdWJzY3IudHlwZSB8fCBzdWJzY3IudHlwZSA9PSAnKicgfHwgc3Vic2NyLnR5cGUgPT0gZXZlbnQuVHlwZSkge1xuXHRcdFx0XHRcdFx0XHRzdWJzY3IuY2FsbGJhY2soZXZlbnQpO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXHRcdFx0fSlcblx0XHRcdC5jYXRjaChmdW5jdGlvbiAocmVzcCwgeGhyKSB7XG5cdFx0XHRcdHZhciByZXNwb25zZUVycm9yID0gcGFyc2VBWE1MKHJlc3ApO1xuXHRcdFx0XHRpZiAoIXJlc3BvbnNlRXJyb3IpIHtcblx0XHRcdFx0XHRpZiAoY250PDQpIHtcblx0XHRcdFx0XHRcdGNvbnRleHQuZXZlbnRzVGltZW91dCA9IHNldFRpbWVvdXQoKCkgPT4ge2NvbnRleHQucG9sbEV2ZW50cyhjbnQrMSl9LCAyMDAwKTtcblx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0Y2xlYXJUaW1lb3V0KGNvbnRleHQuZXZlbnRzVGltZW91dCk7XG5cdFx0XHRcdFx0XHRjb25zb2xlLmVycm9yKFwiQ29ubmVjdGlvbiB0byBzZXJ2ZXIgbG9zdC4gXCIpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHRyZXR1cm47XG5cdFx0XHRcdH1cblx0XHRcdFx0Y29uc29sZS5lcnJvcihjbnQsIHJlc3BvbnNlRXJyb3IuRXJyb3JDb2RlLCByZXNwb25zZUVycm9yLkV4Y2VwdGlvblRleHQpO1xuXHRcdFx0XHRpZiAoY250PDQgJiYgKHJlc3BvbnNlRXJyb3IuRXJyb3JDb2RlID09IDEwMSB8fCByZXNwb25zZUVycm9yLkVycm9yQ29kZSA9PSAxMDApKSB7XG5cdFx0XHRcdFx0Y29udGV4dC5hdXRoKHtcblx0XHRcdFx0XHRcdHN1Y2Nlc3M6ICgpID0+IHtcblx0XHRcdFx0XHRcdFx0Y29uc29sZS5sb2coJ2F1dGggZG9uZScpXG5cdFx0XHRcdFx0XHRcdGNvbnRleHQucG9sbEV2ZW50cygxKTtcblx0XHRcdFx0XHRcdH1cdFxuXHRcdFx0XHRcdH0pO1xuXHRcdFx0XHR9XG5cdFx0XHRcdGVsc2Uge1xuXHRcdFx0XHRcdGNsZWFyVGltZW91dChjb250ZXh0LmV2ZW50c1RpbWVvdXQpO1xuXHRcdFx0XHRcdGNvbnNvbGUuZXJyb3IoXCJDb25uZWN0aW9uIHRvIHNlcnZlciBsb3N0LiBcIik7XG5cdFx0XHRcdFx0dGhyb3cgcmVzcG9uc2VFcnJvcjtcblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cdH1cblxuXHR0aGlzLnN1YnNjcmliZSA9IGZ1bmN0aW9uIChjdHgsIGV2ZW50X25hbWUsIGNhbGxiYWNrKSB7XG5cdFx0aWYgKHR5cGVvZiAoZXZlbnRfbmFtZSkgPT0gJ2Z1bmN0aW9uJykge1xuXHRcdFx0ZXZlbnRfbmFtZSA9ICcqJztcblx0XHRcdGNhbGxiYWNrID0gZXZlbnRfbmFtZTtcblx0XHR9XG5cdFx0Y29udGV4dC5ldmVudF9zdWJzY3JpYmVycy5wdXNoKHtjdHg6IGN0eCwgdHlwZTogZXZlbnRfbmFtZSwgY2FsbGJhY2s6IGNhbGxiYWNrfSk7XG5cdFx0aWYgKCFjb250ZXh0LnBvbGxpbmcpIHtcblx0XHRcdGlmICh0eXBlb2YgU3Vic2NyaWJlT25FdmVudCAhPT0gJ3VuZGVmaW5lZCcpIHtcblx0XHRcdFx0Ly8gY29uc29sZS5sb2coJ2N1cnJlbnQgd2luZG93IFN1YnNjcmliZU9uRXZlbnQnKTtcblx0XHRcdFx0U3Vic2NyaWJlT25FdmVudChldmVudF9uYW1lLCBcImRvY3VtZW50XCIsIGNvbnRleHQucHJvY2Vzc1Byb3RvdHlwZUV2ZW50LCBjb250ZXh0KTtcblx0XHRcdFx0Y29udGV4dC5wb2xsaW5nID0gdHJ1ZTtcblx0XHRcdH1cblx0XHRcdGVsc2UgaWYgKHR5cGVvZiB3aW5kb3cub3BlbmVyICE9PSAndW5kZWZpbmVkJyAmJiB3aW5kb3cub3BlbmVyICYmIHR5cGVvZiB3aW5kb3cub3BlbmVyLlN1YnNjcmliZU9uRXZlbnQgIT09ICd1bmRlZmluZWQnKSB7XG5cdFx0XHRcdC8vIGNvbnNvbGUubG9nKCdwYXJlbnQgd2luZG93IFN1YnNjcmliZU9uRXZlbnQnKTtcblx0XHRcdFx0d2luZG93Lm9wZW5lci5TdWJzY3JpYmVPbkV2ZW50KGV2ZW50X25hbWUsIFwiZG9jdW1lbnRcIiwgY29udGV4dC5wcm9jZXNzUHJvdG90eXBlRXZlbnQsIGNvbnRleHQpO1xuXHRcdFx0XHRjb250ZXh0LnBvbGxpbmcgPSB0cnVlO1xuXHRcdFx0fVxuXHRcdFx0ZWxzZSB7XG5cdFx0XHRcdC8vIGNvbnNvbGUubG9nKCdzZXBhcmF0ZSB3aW5kb3cnKTtcblx0XHRcdFx0Y29udGV4dC5ldmVudHNUaW1lb3V0ID0gc2V0VGltZW91dCgoKSA9PiB7Y29udGV4dC5wb2xsRXZlbnRzKDEpfSwgMSk7XG5cdFx0XHR9XG5cdFx0fVxuXHR9XG5cdHRoaXMudW5zdWJzY3JpYmUgPSBmdW5jdGlvbiAoY3R4LCBldmVudF9uYW1lKSB7XG5cdFx0Ly8gY29uc29sZS5sb2coJ3Vuc3Vic2NyaWJlJywgY3R4LCBldmVudF9uYW1lKVxuXHRcdGZvciAobGV0IGk9MDsgaTx0aGlzLmV2ZW50X3N1YnNjcmliZXJzLmxlbmd0aDsgaSsrKSB7XG5cdFx0XHRsZXQgc3Vic2NyID0gdGhpcy5ldmVudF9zdWJzY3JpYmVyc1tpXTtcblx0XHRcdGlmIChzdWJzY3IuY3R4ID09IGN0eCAmJiAoIWV2ZW50X25hbWUgfHwgZXZlbnRfbmFtZSA9PSAnKicgfHwgZXZlbnRfbmFtZSA9PSBzdWJzY3IudHlwZSkpIHtcblx0XHRcdFx0Ly8gY29uc29sZS5sb2coJyAgZHJvcCBzdWJzY3JpYmVyJywgc3Vic2NyKTtcblx0XHRcdFx0dGhpcy5ldmVudF9zdWJzY3JpYmVycy5zcGxpY2UoaSwgMSk7XG5cdFx0XHR9XG5cdFx0fVxuXHRcdC8vIGlmIChVbnN1YnNjcmliZUZyb21FdmVudCkge1xuXHRcdC8vIFx0VW5zdWJzY3JpYmVGcm9tRXZlbnQoZXZlbnRfbmFtZSwgXCJkb2N1bWVudFwiLCB0aGlzLnByb2Nlc3NQcm90b3R5cGVFdmVudCwgdGhpcylcblx0XHQvLyB9XG5cdH1cblxuXHR0aGlzLm5vcm1hbGl6ZVByb3RvdHlwZU9iamVjdCA9IGZ1bmN0aW9uKG9iaikge1xuXHRcdGxldCByZXMgPSB7fVxuXHRcdGZvciAobGV0IGsgaW4gb2JqKSB7XG5cdFx0XHRpZiAodHlwZW9mIG9ialtrXSA9PSAnb2JqZWN0Jykge1xuXHRcdFx0XHRpZiAoJ29WYWx1ZV8nIGluIG9ialtrXSkge1xuXHRcdFx0XHRcdHJlc1trXSA9IG9ialtrXS5vVmFsdWVfO1xuXHRcdFx0XHRcdGlmICh0eXBlb2Ygb2JqW2tdLm9WYWx1ZV8gPT0gJ29iamVjdCcgJiYgJ3ZhbHVlXycgaW4gb2JqW2tdLm9WYWx1ZV8pIHJlc1trXSA9IG9ialtrXS5vVmFsdWVfLnZhbHVlXztcblx0XHRcdFx0fVxuXHRcdFx0XHQvLyBlbHNlIGlmICgndmFsdWVfJyBpbiBvYmpba10pIHJlc1trXSA9IG9ialtrXS52YWx1ZV9cblx0XHRcdFx0ZWxzZSByZXNba10gPSBudWxsO1xuXHRcdFx0fVxuXHRcdFx0ZWxzZSByZXNba10gPSBvYmpba107XG5cdFx0fVxuXHRcdHJldHVybiByZXM7XG5cdH1cblx0dGhpcy5wcm9jZXNzUHJvdG90eXBlRXZlbnQgPSBmdW5jdGlvbihlKSB7XG5cdFx0bGV0IGV2ZW50X2RhdGEgPSB0aGlzLm5vcm1hbGl6ZVByb3RvdHlwZU9iamVjdChlLm9FdmVudERhdGFfKSxcblx0XHRcdGV2ZW50ID0ge1R5cGU6IGUuc0V2ZW50TmFtZV8sIERhdGE6IGV2ZW50X2RhdGF9O1xuXHRcdGZvciAobGV0IGk9MDsgaTxjb250ZXh0LmV2ZW50X3N1YnNjcmliZXJzLmxlbmd0aDsgaSsrKSB7XG5cdFx0XHRsZXQgc3Vic2NyID0gY29udGV4dC5ldmVudF9zdWJzY3JpYmVyc1tpXTtcblx0XHRcdGlmICghc3Vic2NyLnR5cGUgfHwgc3Vic2NyLnR5cGUgPT0gJyonIHx8IHN1YnNjci50eXBlLnRvVXBwZXJDYXNlKCkgPT0gZS5zRXZlbnROYW1lXykge1xuXHRcdFx0XHRzdWJzY3IuY2FsbGJhY2soZXZlbnQpO1xuXHRcdFx0fVxuXHRcdH1cblx0fVxuXG5cdGZ1bmN0aW9uIGRlbGV0ZV9jb29raWUobmFtZSkge1xuXHRcdGRvY3VtZW50LmNvb2tpZSA9IG5hbWUgKyc9OyBQYXRoPS87IEV4cGlyZXM9VGh1LCAwMSBKYW4gMTk3MCAwMDowMDowMSBHTVQ7Jztcblx0fVxuXG5cdHRoaXMubG9nb3V0ID0gZnVuY3Rpb24gKCkge1xuXHRcdGNsZWFyVGltZW91dChjb250ZXh0LmV2ZW50c1RpbWVvdXQpO1xuXHRcdGRlbGV0ZV9jb29raWUoJ18xMDI0X3NpZCcpO1xuXHRcdHRoaXMuYXV0aGVudGljYXRlZCA9IGZhbHNlO1xuXHR9XG59XG5cbmZ1bmN0aW9uIHBhZGR5KG4sIHAsIGMpIHtcblx0dmFyIHBhZF9jaGFyID0gdHlwZW9mIGMgIT09ICd1bmRlZmluZWQnID8gYyA6ICcwJztcblx0dmFyIHBhZCA9IG5ldyBBcnJheSgxICsgcCkuam9pbihwYWRfY2hhcik7XG5cdHJldHVybiAocGFkICsgbikuc2xpY2UoLXBhZC5sZW5ndGgpO1xufVxuZnVuY3Rpb24gZm9ybWF0X2VmZmlfZGF0ZShkYXRlKSB7XG5cdGlmIChkYXRlID09IG51bGwpIHJldHVybiAnbm90LWEtZGF0ZSc7XG5cdHJldHVybiBwYWRkeShkYXRlLmdldEZ1bGxZZWFyKCksIDQpICsgcGFkZHkoZGF0ZS5nZXRNb250aCgpKzEsIDIpICsgcGFkZHkoZGF0ZS5nZXREYXRlKCksIDIpO1xufVxuZnVuY3Rpb24gZm9ybWF0X2VmZmlfdGltZShkYXRlKSB7XG5cdGlmIChkYXRlID09IG51bGwpIHJldHVybiAnbm90LWEtZGF0ZS10aW1lJztcblx0cmV0dXJuIHBhZGR5KGRhdGUuZ2V0RnVsbFllYXIoKSwgNCkgKyBwYWRkeShkYXRlLmdldE1vbnRoKCkrMSwgMikgKyBwYWRkeShkYXRlLmdldERhdGUoKSwgMikgKyAnVCcgKyBcblx0XHRwYWRkeShkYXRlLmdldEhvdXJzKCksIDIpICsgcGFkZHkoZGF0ZS5nZXRNaW51dGVzKCksIDIpICsgcGFkZHkoZGF0ZS5nZXRTZWNvbmRzKCksIDIpO1xufVxuXG5mdW5jdGlvbiBlbmNvZGVBVVJMQ29tcG9uZW50KG9iaikge1xuXHR2YXIgc2VyaWFsaXplZCA9ICcnO1xuXHRpZiAodHlwZW9mIG9iaiA9PSAndW5kZWZpbmVkJyB8fCBvYmogPT0gbnVsbCkgcmV0dXJuICdub25lOiYnXG5cdGVsc2UgaWYgKG9iai50eXBlID09ICdhcnJheScgfHwgQXJyYXkuaXNBcnJheShvYmopKSB7XG5cdFx0bGV0IHMgPSAnJztcblx0XHRmb3IgKGxldCBpPTA7IGk8b2JqLmxlbmd0aDsgaSsrKSB7XG5cdFx0XHRsZXQgbyA9IG9ialtpXTtcblx0XHRcdHMgKz0gZW5jb2RlQVVSTENvbXBvbmVudChvKTtcblx0XHR9XG5cdFx0c2VyaWFsaXplZCArPSAnVmFsdWU6QXJyYXk6JyArIHMgKyAnJiYnO1xuXHR9XG5cdGVsc2UgaWYgKG9iai50eXBlID09ICdvcHRpb25hbEludCcpIHNlcmlhbGl6ZWQgPSAnb3B0aW9uYWw6aTonICsgb2JqLnZhbHVlICsgJyYmJztcblx0ZWxzZSBpZiAob2JqLnR5cGUgPT0gJ2Zsb2F0JyB8fCB0eXBlb2Ygb2JqLnZhbHVlID09ICdmbG9hdCcpIHNlcmlhbGl6ZWQgPSAnZDonICsgb2JqLnZhbHVlICsgJyYnO1xuXHRlbHNlIGlmIChvYmoudHlwZSA9PSAnaW50JyB8fCB0eXBlb2Ygb2JqLnZhbHVlID09ICdudW1iZXInKSBzZXJpYWxpemVkID0gJ2k6JyArIG9iai52YWx1ZSArICcmJztcblx0ZWxzZSBpZiAob2JqLnR5cGUgPT0gJ2RhdGUnIHx8IG9iai50eXBlID09ICdBRGF0ZScpIHNlcmlhbGl6ZWQgPSAnQURhdGU6czonICsgZm9ybWF0X2VmZmlfZGF0ZShvYmoudmFsdWUpICsgJyYmJztcblx0ZWxzZSBpZiAob2JqLnR5cGUgPT0gJ2RhdGV0aW1lJyB8fCBvYmoudHlwZSA9PSAnVGltZScgfHwgb2JqLnZhbHVlIGluc3RhbmNlb2YgRGF0ZSkgc2VyaWFsaXplZCA9ICdUaW1lOnM6JyArIGZvcm1hdF9lZmZpX3RpbWUob2JqLnZhbHVlKSArICcmJic7XG5cdGVsc2UgaWYgKG9iai50eXBlID09ICdjaGVja2JveCcpIHNlcmlhbGl6ZWQgPSAnczonICsgb2JqLnZhbHVlICsgJyYnO1xuXHRlbHNlIGlmIChvYmoudHlwZSA9PSAnb3B0aW9uYWxTdHJpbmcnKSBzZXJpYWxpemVkID0gJ29wdGlvbmFsOnM6JyArIG9iai52YWx1ZS5yZXBsYWNlKC8gL2csICdcXFxcdycpICsgJyYmJztcblx0ZWxzZSBpZiAob2JqLnR5cGUgPT0gJ2JpbmFyeScpIHNlcmlhbGl6ZWQgPSAnYjonICsgRW5jb2Rlci5CYXNlNjRFbmNvZGUoRW5jb2Rlci5VVEY4RW5jb2RlKG9iai52YWx1ZSkpICsgJyYnO1xuXHRlbHNlIHtcblx0XHRsZXQgdiA9ICgodHlwZW9mIG9iai52YWx1ZSA9PSAndW5kZWZpbmVkJykgPyBvYmogOiBvYmoudmFsdWUpO1xuXHRcdHNlcmlhbGl6ZWQgPSAnczonICsgdi5yZXBsYWNlKC8gL2csICdcXFxcdycpICsgJyYnO1xuXHR9XG5cdHJldHVybiBzZXJpYWxpemVkO1xufVxuXG5mdW5jdGlvbiBlbmNvZGVQbGFpbihvYmopIHtcblx0bGV0IHNlcmlhbGl6ZWQgPSAnJztcblx0aWYgKHR5cGVvZiBvYmogPT0gJ3VuZGVmaW5lZCcgfHwgb2JqID09IG51bGwpIHJldHVybiAnbm9uZTomJ1xuXHRlbHNlIGlmIChvYmoudHlwZSA9PSAnb3B0aW9uYWxJbnQnKSBzZXJpYWxpemVkID0gJ29wdGlvbmFsOmk6JyArIG9iai52YWx1ZSArICcmJic7XG5cdGVsc2UgaWYgKG9iai50eXBlID09ICdmbG9hdCcgfHwgdHlwZW9mIG9iai52YWx1ZSA9PSAnZmxvYXQnKSBzZXJpYWxpemVkID0gJ2Q6JyArIG9iai52YWx1ZSArICcmJztcblx0ZWxzZSBpZiAob2JqLnR5cGUgPT0gJ2ludCcgfHwgdHlwZW9mIG9iai52YWx1ZSA9PSAnbnVtYmVyJykgc2VyaWFsaXplZCA9ICdpOicgKyBvYmoudmFsdWUgKyAnJic7XG5cdGVsc2UgaWYgKG9iai50eXBlID09ICdkYXRlJyB8fCBvYmoudHlwZSA9PSAnQURhdGUnKSBzZXJpYWxpemVkID0gJ0FEYXRlOnM6JyArIGZvcm1hdF9lZmZpX2RhdGUob2JqLnZhbHVlKSArICcmJic7XG5cdGVsc2UgaWYgKG9iai50eXBlID09ICdkYXRldGltZScgfHwgb2JqLnR5cGUgPT0gJ1RpbWUnIHx8IG9iai52YWx1ZSBpbnN0YW5jZW9mIERhdGUpIHNlcmlhbGl6ZWQgPSAnVGltZTpzOicgKyBmb3JtYXRfZWZmaV90aW1lKG9iai52YWx1ZSkgKyAnJiYnO1xuXHRlbHNlIGlmIChvYmoudHlwZSA9PSAnY2hlY2tib3gnKSBzZXJpYWxpemVkID0gJ3M6JyArIG9iai52YWx1ZSArICcmJztcblx0ZWxzZSBpZiAob2JqLnR5cGUgPT0gJ29wdGlvbmFsU3RyaW5nJykgc2VyaWFsaXplZCA9ICdvcHRpb25hbDpzOicgKyBvYmoudmFsdWUucmVwbGFjZSgvIC9nLCAnXFxcXHcnKSArICcmJic7XG5cdGVsc2UgaWYgKG9iai50eXBlID09ICdiaW5hcnknKSBzZXJpYWxpemVkID0gJ2I6JyArIEVuY29kZXIuQmFzZTY0RW5jb2RlKG9iai52YWx1ZSkgKyAnJic7XG5cdGVsc2Uge1xuXHRcdGxldCB2ID0gKCh0eXBlb2Ygb2JqLnZhbHVlID09ICd1bmRlZmluZWQnKSA/IG9iaiA6IG9iai52YWx1ZSk7XG5cdFx0c2VyaWFsaXplZCA9ICdzOicgKyB2LnJlcGxhY2UoLyAvZywgJ1xcXFx3JykgKyAnJic7XG5cdH1cblx0cmV0dXJuIHNlcmlhbGl6ZWQ7XG59XG5mdW5jdGlvbiBlbmNvZGVCbG9iKGZpbGUsIGNhbGxiYWNrKSB7XG5cdHZhciByZWFkZXIgPSBuZXcgRmlsZVJlYWRlcigpO1xuXG5cdHJlYWRlci5vbmxvYWQgPSBmdW5jdGlvbihyZWFkZXJFdnQpIHtcblx0XHRjb25zb2xlLmxvZygnbG9hZGVkIGZpbGUgc2l6ZT0nICsgcmVhZGVyRXZ0LnRhcmdldC5yZXN1bHQubGVuZ3RoKTtcblx0XHRsZXQgc2VyaWFsaXplZCA9ICdiOicgKyBFbmNvZGVyLkJhc2U2NEVuY29kZShyZWFkZXJFdnQudGFyZ2V0LnJlc3VsdCkgKyAnJic7XG5cdFx0Y2FsbGJhY2soc2VyaWFsaXplZCk7XG5cdH07XG5cblx0cmVhZGVyLnJlYWRBc0JpbmFyeVN0cmluZyhmaWxlKTtcbn1cbmZ1bmN0aW9uIGVuY29kZUJsb2JGaWxlKGZpbGUsIGNhbGxiYWNrKSB7XG5cdGVuY29kZUJsb2IoZmlsZSwgZnVuY3Rpb24oc2VyaWFsaXplZF9ibG9iKSB7XG5cdFx0bGV0IHNlcmlhbGl6ZWQgPSAnQmxvYkZpbGU6JyArIFxuXHRcdFx0ZW5jb2RlQVVSTENvbXBvbmVudCh7dmFsdWU6IGZpbGUubmFtZSwgdHlwZTogJ3N0cmluZyd9KSArXG5cdFx0XHRlbmNvZGVBVVJMQ29tcG9uZW50KHt2YWx1ZTogZmlsZS50eXBlLCB0eXBlOiAnc3RyaW5nJ30pICtcblx0XHRcdHNlcmlhbGl6ZWRfYmxvYiArXG5cdFx0XHRlbmNvZGVBVVJMQ29tcG9uZW50KG51bGwpICtcblx0XHRcdGVuY29kZUFVUkxDb21wb25lbnQoe3ZhbHVlOiBmaWxlLmxhc3RNb2RpZmllZERhdGUsIHR5cGU6ICdUaW1lJ30pICsgXG5cdFx0XHQnJic7XG5cdFx0Y2FsbGJhY2soc2VyaWFsaXplZCk7XG5cdH0pO1xufVxuXG5mdW5jdGlvbiByZWR1Y2VTZXJpYWxpemF0aW9uQXJyYXkoc2VyaWFsaXplZCwgYXJyYXksIGksIGNhbGxiYWNrKSB7XG5cdGlmIChhcnJheS5sZW5ndGggPT0gMCkge1xuXHRcdGNhbGxiYWNrKHNlcmlhbGl6ZWQpO1xuXHRcdHJldHVybjtcblx0fVxuXHRsZXQgb2JqID0gYXJyYXkuc3BsaWNlKDAsIDEpO1xuXHRlbmNvZGVBVVJMQ29tcG9uZW50QXN5bmMoc2VyaWFsaXplZCwgb2JqLCBmdW5jdGlvbih2KSB7XG5cdFx0cmVkdWNlU2VyaWFsaXphdGlvbkFycmF5KHYsIGFycmF5LCArK2ksIGNhbGxiYWNrKTtcblx0fSk7XG59XG5mdW5jdGlvbiByZWR1Y2VTZXJpYWxpemF0aW9uU3RydWN0dXJlKHNlcmlhbGl6ZWQsIGFycmF5LCBvYmplY3QsIGksIGNhbGxiYWNrKSB7XG5cdGlmIChhcnJheS5sZW5ndGggPT0gMCkge1xuXHRcdGNhbGxiYWNrKHNlcmlhbGl6ZWQpO1xuXHRcdHJldHVybjtcblx0fVxuXHRsZXQga2V5ID0gYXJyYXkuc3BsaWNlKDAsIDEpO1xuXHRsZXQgb2JqID0gb2JqZWN0W2tleV07XG5cdGxldCBzID0gc2VyaWFsaXplZCArIGtleSArICc9Jztcblx0ZW5jb2RlQVVSTENvbXBvbmVudEFzeW5jKHMsIG9iaiwgZnVuY3Rpb24gKHYpIHtcblx0XHRyZWR1Y2VTZXJpYWxpemF0aW9uU3RydWN0dXJlKHYsIGFycmF5LCBvYmplY3QsICsraSwgY2FsbGJhY2spO1xuXHR9KTtcbn1cblxuZnVuY3Rpb24gZW5jb2RlQVVSTENvbXBvbmVudEFzeW5jKHNlcmlhbGl6ZWQsIG9iaiwgY2FsbGJhY2spIHtcblx0Ly8gY29uc29sZS5sb2cob2JqKTtcblx0aWYgKG9iai50eXBlID09ICdCbG9iRmlsZScgfHwgb2JqLnZhbHVlIGluc3RhbmNlb2YgRmlsZSkge1xuXHRcdGVuY29kZUJsb2JGaWxlKG9iai52YWx1ZSwgZnVuY3Rpb24gKHNlcmlhbGl6ZWRfZmlsZSkge1xuXHRcdFx0Y2FsbGJhY2soc2VyaWFsaXplZCArIHNlcmlhbGl6ZWRfZmlsZSk7XG5cdFx0fSk7XG5cdH1cblx0ZWxzZSBpZiAob2JqLnR5cGUgPT0gJ0FycmF5JyB8fCBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwob2JqKSA9PT0gJ1tvYmplY3QgQXJyYXldJykge1xuXHRcdGxldCBvID0gb2JqLnZhbHVlIHx8IG9iajtcblx0XHRyZWR1Y2VTZXJpYWxpemF0aW9uQXJyYXkoc2VyaWFsaXplZCArICdWYWx1ZTpBcnJheTonLCBvLCAwLCBmdW5jdGlvbiAodikge1xuXHRcdFx0Y2FsbGJhY2sodiArICcmJicpXG5cdFx0fSk7XG5cdH1cblx0ZWxzZSBpZiAob2JqLnR5cGUgPT0gJ1N0cnVjdHVyZScpIHtcblx0XHRsZXQga2V5cyA9IFtdO1xuXHRcdGZvciAobGV0IGsgaW4gb2JqLnZhbHVlKSBrZXlzLnB1c2goayk7XG5cdFx0bGV0IHMgPSBzZXJpYWxpemVkICsgJ1ZhbHVlOlN0cnVjdHVyZTonO1xuXHRcdHJlZHVjZVNlcmlhbGl6YXRpb25TdHJ1Y3R1cmUocywga2V5cywgb2JqLnZhbHVlLCAwLCBmdW5jdGlvbiAodikge1xuXHRcdFx0Y2FsbGJhY2sodiArICcmJicpXG5cdFx0fSk7XG5cdH1cblx0ZWxzZSB7XG5cdFx0bGV0IHMzID0gZW5jb2RlUGxhaW4ob2JqKTtcblx0XHRjYWxsYmFjayhzZXJpYWxpemVkICsgczMpO1xuXHR9XG59XG5cbmZ1bmN0aW9uIHNlcmlhbGl6ZUFVUkwoYSkge1xuXHR2YXIgcmVzdWx0ID0gJyc7XG5cdGZvciAodmFyIGtleSBpbiBhKSB7XG5cdFx0dmFyIG8gPSBhW2tleV07XG5cdFx0cmVzdWx0ICs9IGtleSArIFwiPVwiICsgZW5jb2RlQVVSTENvbXBvbmVudChvKTtcblx0fVxuXHRyZXR1cm4gcmVzdWx0O1xufTtcbmZ1bmN0aW9uIHNlcmlhbGl6ZUFVUkxBc3luYyhhLCBjYWxsYmFjaykge1xuXHRsZXQga2V5cyA9IFtdO1xuXHRmb3IgKGxldCBrIGluIGEpIGtleXMucHVzaChrKTtcblx0cmVkdWNlU2VyaWFsaXphdGlvblN0cnVjdHVyZSgnJywga2V5cywgYSwgMCwgY2FsbGJhY2spO1xufVxuXG5mdW5jdGlvbiBwcmVwYXJlRGF0YUxpc3QoZGF0YSkge1xuXHRpZiAoIWRhdGEgfHwgZGF0YS5sZW5ndGggPT0gMCkgdGhyb3cgXCJJbnZhbGlkIERhdGFMaXN0IGFyZ3VtZW50LiBcIjtcblx0Y29uc3QgaGVhZGVyID0gZGF0YVswXTtcblx0bGV0IHJlc3VsdCA9IHtcblx0XHRoZWFkZXI6IGhlYWRlcixcblx0XHRkYXRhOiBbXVxuXHR9O1xuXHRmb3IgKGxldCBpPTE7IGk8ZGF0YS5sZW5ndGg7IGkrKykge1xuXHRcdGxldCByID0gZGF0YVtpXSxcblx0XHRcdHJvdyA9IHt9O1xuXHRcdGZvciAobGV0IGM9MDsgYzxyLmxlbmd0aDsgYysrKSB7XG5cdFx0XHRyb3dbaGVhZGVyW2NdXSA9IHJbY107XG5cdFx0fVxuXHRcdHJlc3VsdC5kYXRhLnB1c2gocm93KTtcblx0fVxuXHRyZXR1cm4gcmVzdWx0O1xufVxuXG4vKlxualF1ZXJ5LmZuLmV4dGVuZCh7XG5cdHNlcmlhbGl6ZUFVUkw6IGZ1bmN0aW9uKCkge1xuXHRcdHJldHVybiBzZXJpYWxpemVBVVJMKCB0aGlzLnNlcmlhbGl6ZVR5cGVkQXJyYXkoKSApO1xuXHR9LFxuXHRzZXJpYWxpemVUeXBlZEFycmF5OiBmdW5jdGlvbigpIHtcblx0XHR2YXIgckNSTEYgPSAvXFxyP1xcbi9nLFxuXHRcdFx0cnN1Ym1pdHRlclR5cGVzID0gL14oPzpzdWJtaXR8YnV0dG9ufGltYWdlfHJlc2V0fGZpbGUpJC9pLFxuXHRcdFx0cnN1Ym1pdHRhYmxlID0gL14oPzppbnB1dHxzZWxlY3R8dGV4dGFyZWF8a2V5Z2VuKS9pLFxuXHRcdFx0cmNoZWNrYWJsZVR5cGUgPSAvXig/OmNoZWNrYm94fHJhZGlvKSQvaTtcblx0XHRcblx0XHRmdW5jdGlvbiBnZXR2KHZhbCwgdHlwZSwgaXNBcnJheSkge1xuXHRcdFx0dHlwZSA9IHR5cGUgfHwgJ3N0cmluZyc7XG5cdFx0XHR2YXIgdiA9IFwiXCI7XG5cdFx0XHRpZiAodHlwZSA9PSAnZGF0ZScpIHtcblx0XHRcdFx0dmFyIGZvdW5kID0gdmFsLm1hdGNoKC8oXFxkXFxkKVxcLihcXGRcXGQpXFwuKFxcZFxcZFxcZFxcZCkoIChcXGRcXGQpOihcXGRcXGQpKDooXFxkXFxkKSk/KT8vKTtcblx0XHRcdFx0aWYgKGZvdW5kICE9IG51bGwpIHtcblx0XHRcdFx0XHQvLyB2YXIgaCA9IGZvdW5kWzVdID8gcGFyc2VJbnQoZm91bmRbNV0pIDogdW5kZWZpbmVkLCBcblx0XHRcdFx0XHQvLyBcdG0gPSBmb3VuZFs2XSA/IHBhcnNlSW50KGZvdW5kWzZdKSA6IHVuZGVmaW5lZCwgXG5cdFx0XHRcdFx0Ly8gXHRzID0gZm91bmRbOF0gPyBwYXJzZUludChmb3VuZFs4XSkgOiB1bmRlZmluZWQ7XG5cdFx0XHRcdFx0diA9IG5ldyBEYXRlKHBhcnNlSW50KGZvdW5kWzNdKSwgcGFyc2VJbnQoZm91bmRbMl0pLTEsIHBhcnNlSW50KGZvdW5kWzFdKSk7XG5cdFx0XHRcdFx0Ly8gY29uc29sZS5sb2cocGFyc2VJbnQoZm91bmRbM10pLCBwYXJzZUludChmb3VuZFsyXSktMSwgcGFyc2VJbnQoZm91bmRbMV0pLCBoLCBtLCBzLCAnLT4nLCB2KTtcblx0XHRcdFx0fVxuXHRcdFx0XHRlbHNlIHYgPSBudWxsO1xuXHRcdFx0fVxuXHRcdFx0Ly8gZWxzZSBpZiAodHlwZSA9PSAnY2hlY2tib3gnKSBcblx0XHRcdGVsc2UgaWYgKHR5cGUgPT0gJ2ludCcpIHYgPSBwYXJzZUludCh2YWwpO1xuXHRcdFx0ZWxzZSBpZiAodHlwZSA9PSAnY2hlY2tib3gnICYmICFpc0FycmF5KSB7XG5cdFx0XHRcdHYgPSAodmFsID09ICdvbicgPyB0cnVlIDogZmFsc2UpO1xuXHRcdFx0fVxuXHRcdFx0ZWxzZSB2ID0gdmFsLnJlcGxhY2UoIHJDUkxGLCBcIlxcclxcblwiICk7XG5cdFx0XHRyZXR1cm4gdjtcblx0XHR9XG5cblx0XHR2YXIgbGlzdCA9IHRoaXMubWFwKGZ1bmN0aW9uKCkge1xuXHRcdFx0Ly8gQ2FuIGFkZCBwcm9wSG9vayBmb3IgXCJlbGVtZW50c1wiIHRvIGZpbHRlciBvciBhZGQgZm9ybSBlbGVtZW50c1xuXHRcdFx0dmFyIGVsZW1lbnRzID0galF1ZXJ5LnByb3AoIHRoaXMsIFwiZWxlbWVudHNcIiApO1xuXHRcdFx0cmV0dXJuIGVsZW1lbnRzID8galF1ZXJ5Lm1ha2VBcnJheSggZWxlbWVudHMgKSA6IHRoaXM7XG5cdFx0fSlcblx0XHQuZmlsdGVyKGZ1bmN0aW9uKCkge1xuXHRcdFx0dmFyIHR5cGUgPSB0aGlzLnR5cGU7XG5cblx0XHRcdC8vIFVzZSAuaXMoIFwiOmRpc2FibGVkXCIgKSBzbyB0aGF0IGZpZWxkc2V0W2Rpc2FibGVkXSB3b3Jrc1xuXHRcdFx0cmV0dXJuIHRoaXMubmFtZSAmJiAhalF1ZXJ5KCB0aGlzICkuaXMoIFwiOmRpc2FibGVkXCIgKSAmJlxuXHRcdFx0XHRyc3VibWl0dGFibGUudGVzdCggdGhpcy5ub2RlTmFtZSApICYmICFyc3VibWl0dGVyVHlwZXMudGVzdCggdHlwZSApICYmXG5cdFx0XHRcdCggdGhpcy5jaGVja2VkIHx8ICFyY2hlY2thYmxlVHlwZS50ZXN0KCB0eXBlICkgKTtcblx0XHR9KVxuXHRcdC5tYXAoZnVuY3Rpb24oIGksIGVsZW0gKSB7XG5cdFx0XHR2YXIgdmFsID0galF1ZXJ5KCB0aGlzICkudmFsKCksXG5cdFx0XHRcdHR5cGUgPSBqUXVlcnkoIHRoaXMgKS5hdHRyKCdkYXRhLXR5cGUnKSB8fCB0aGlzLnR5cGUsXG5cdFx0XHRcdGlzQXJyYXkgPSBqUXVlcnkoIHRoaXMgKS5hdHRyKCdkYXRhLWFycmF5JykgPT0gJ3RydWUnIHx8IGZhbHNlO1xuXG5cdFx0XHRyZXR1cm4gdmFsID09IG51bGwgP1xuXHRcdFx0XHRudWxsIDpcblx0XHRcdFx0alF1ZXJ5LmlzQXJyYXkoIHZhbCApID9cblx0XHRcdFx0XHRqUXVlcnkubWFwKCB2YWwsIGZ1bmN0aW9uKCB2YWwgKSB7XG5cdFx0XHRcdFx0XHRyZXR1cm4geyBuYW1lOiBlbGVtLm5hbWUsIHZhbHVlOiBnZXR2KHZhbCwgdHlwZSwgaXNBcnJheSksIHR5cGU6IHR5cGUsIGlzQXJyYXk6IGlzQXJyYXkgfTtcblx0XHRcdFx0XHR9KSA6XG5cdFx0XHRcdFx0eyBuYW1lOiBlbGVtLm5hbWUsIHZhbHVlOiBnZXR2KHZhbCwgdHlwZSwgaXNBcnJheSksIHR5cGU6IHR5cGUsIGlzQXJyYXk6IGlzQXJyYXkgfTtcblx0XHR9KS5nZXQoKTtcblxuXHRcdHZhciBzID0ge31cblx0XHRmb3IgKHZhciBpPTA7IGk8bGlzdC5sZW5ndGg7IGkrKykge1xuXHRcdFx0dmFyIG8gPSBsaXN0W2ldO1xuXHRcdFx0aWYgKG8uaXNBcnJheSB8fCBvLm5hbWUgaW4gcykge1xuXHRcdFx0XHRpZiAoIShvLm5hbWUgaW4gcykpIHNbby5uYW1lXSA9IFtvXTtcblx0XHRcdFx0ZWxzZSBzW28ubmFtZV0ucHVzaChvKTtcblx0XHRcdH1cblx0XHRcdGVsc2Ugc1tvLm5hbWVdID0gbztcblx0XHR9XG5cdFx0cmV0dXJuIHM7XG5cdH1cbn0pO1xuKi9cblxuZXhwb3J0IHsgRWZmaVByb3RvY29sLCBzZXJpYWxpemVBVVJMLCBzZXJpYWxpemVBVVJMQXN5bmMsIGZvcm1hdF9lZmZpX2RhdGUsIGZvcm1hdF9lZmZpX3RpbWUsIHByZXBhcmVEYXRhTGlzdCB9O1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHNyYy9saWIvZWZmaV9wcm90b2NvbC5qcyIsIi8qIVxuICogQm93c2VyIC0gYSBicm93c2VyIGRldGVjdG9yXG4gKiBodHRwczovL2dpdGh1Yi5jb20vZGVkL2Jvd3NlclxuICogTUlUIExpY2Vuc2UgfCAoYykgRHVzdGluIERpYXogMjAxNVxuICovXG5cbiFmdW5jdGlvbiAocm9vdCwgbmFtZSwgZGVmaW5pdGlvbikge1xuICBpZiAodHlwZW9mIG1vZHVsZSAhPSAndW5kZWZpbmVkJyAmJiBtb2R1bGUuZXhwb3J0cykgbW9kdWxlLmV4cG9ydHMgPSBkZWZpbml0aW9uKClcbiAgZWxzZSBpZiAodHlwZW9mIGRlZmluZSA9PSAnZnVuY3Rpb24nICYmIGRlZmluZS5hbWQpIGRlZmluZShuYW1lLCBkZWZpbml0aW9uKVxuICBlbHNlIHJvb3RbbmFtZV0gPSBkZWZpbml0aW9uKClcbn0odGhpcywgJ2Jvd3NlcicsIGZ1bmN0aW9uICgpIHtcbiAgLyoqXG4gICAgKiBTZWUgdXNlcmFnZW50cy5qcyBmb3IgZXhhbXBsZXMgb2YgbmF2aWdhdG9yLnVzZXJBZ2VudFxuICAgICovXG5cbiAgdmFyIHQgPSB0cnVlXG5cbiAgZnVuY3Rpb24gZGV0ZWN0KHVhKSB7XG5cbiAgICBmdW5jdGlvbiBnZXRGaXJzdE1hdGNoKHJlZ2V4KSB7XG4gICAgICB2YXIgbWF0Y2ggPSB1YS5tYXRjaChyZWdleCk7XG4gICAgICByZXR1cm4gKG1hdGNoICYmIG1hdGNoLmxlbmd0aCA+IDEgJiYgbWF0Y2hbMV0pIHx8ICcnO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGdldFNlY29uZE1hdGNoKHJlZ2V4KSB7XG4gICAgICB2YXIgbWF0Y2ggPSB1YS5tYXRjaChyZWdleCk7XG4gICAgICByZXR1cm4gKG1hdGNoICYmIG1hdGNoLmxlbmd0aCA+IDEgJiYgbWF0Y2hbMl0pIHx8ICcnO1xuICAgIH1cblxuICAgIHZhciBpb3NkZXZpY2UgPSBnZXRGaXJzdE1hdGNoKC8oaXBvZHxpcGhvbmV8aXBhZCkvaSkudG9Mb3dlckNhc2UoKVxuICAgICAgLCBsaWtlQW5kcm9pZCA9IC9saWtlIGFuZHJvaWQvaS50ZXN0KHVhKVxuICAgICAgLCBhbmRyb2lkID0gIWxpa2VBbmRyb2lkICYmIC9hbmRyb2lkL2kudGVzdCh1YSlcbiAgICAgICwgbmV4dXNNb2JpbGUgPSAvbmV4dXNcXHMqWzAtNl1cXHMqL2kudGVzdCh1YSlcbiAgICAgICwgbmV4dXNUYWJsZXQgPSAhbmV4dXNNb2JpbGUgJiYgL25leHVzXFxzKlswLTldKy9pLnRlc3QodWEpXG4gICAgICAsIGNocm9tZW9zID0gL0NyT1MvLnRlc3QodWEpXG4gICAgICAsIHNpbGsgPSAvc2lsay9pLnRlc3QodWEpXG4gICAgICAsIHNhaWxmaXNoID0gL3NhaWxmaXNoL2kudGVzdCh1YSlcbiAgICAgICwgdGl6ZW4gPSAvdGl6ZW4vaS50ZXN0KHVhKVxuICAgICAgLCB3ZWJvcyA9IC8od2VifGhwdylvcy9pLnRlc3QodWEpXG4gICAgICAsIHdpbmRvd3NwaG9uZSA9IC93aW5kb3dzIHBob25lL2kudGVzdCh1YSlcbiAgICAgICwgc2Ftc3VuZ0Jyb3dzZXIgPSAvU2Ftc3VuZ0Jyb3dzZXIvaS50ZXN0KHVhKVxuICAgICAgLCB3aW5kb3dzID0gIXdpbmRvd3NwaG9uZSAmJiAvd2luZG93cy9pLnRlc3QodWEpXG4gICAgICAsIG1hYyA9ICFpb3NkZXZpY2UgJiYgIXNpbGsgJiYgL21hY2ludG9zaC9pLnRlc3QodWEpXG4gICAgICAsIGxpbnV4ID0gIWFuZHJvaWQgJiYgIXNhaWxmaXNoICYmICF0aXplbiAmJiAhd2Vib3MgJiYgL2xpbnV4L2kudGVzdCh1YSlcbiAgICAgICwgZWRnZVZlcnNpb24gPSBnZXRGaXJzdE1hdGNoKC9lZGdlXFwvKFxcZCsoXFwuXFxkKyk/KS9pKVxuICAgICAgLCB2ZXJzaW9uSWRlbnRpZmllciA9IGdldEZpcnN0TWF0Y2goL3ZlcnNpb25cXC8oXFxkKyhcXC5cXGQrKT8pL2kpXG4gICAgICAsIHRhYmxldCA9IC90YWJsZXQvaS50ZXN0KHVhKSAmJiAhL3RhYmxldCBwYy9pLnRlc3QodWEpXG4gICAgICAsIG1vYmlsZSA9ICF0YWJsZXQgJiYgL1teLV1tb2JpL2kudGVzdCh1YSlcbiAgICAgICwgeGJveCA9IC94Ym94L2kudGVzdCh1YSlcbiAgICAgICwgcmVzdWx0XG5cbiAgICBpZiAoL29wZXJhL2kudGVzdCh1YSkpIHtcbiAgICAgIC8vICBhbiBvbGQgT3BlcmFcbiAgICAgIHJlc3VsdCA9IHtcbiAgICAgICAgbmFtZTogJ09wZXJhJ1xuICAgICAgLCBvcGVyYTogdFxuICAgICAgLCB2ZXJzaW9uOiB2ZXJzaW9uSWRlbnRpZmllciB8fCBnZXRGaXJzdE1hdGNoKC8oPzpvcGVyYXxvcHJ8b3Bpb3MpW1xcc1xcL10oXFxkKyhcXC5cXGQrKT8pL2kpXG4gICAgICB9XG4gICAgfSBlbHNlIGlmICgvb3ByXFwvfG9waW9zL2kudGVzdCh1YSkpIHtcbiAgICAgIC8vIGEgbmV3IE9wZXJhXG4gICAgICByZXN1bHQgPSB7XG4gICAgICAgIG5hbWU6ICdPcGVyYSdcbiAgICAgICAgLCBvcGVyYTogdFxuICAgICAgICAsIHZlcnNpb246IGdldEZpcnN0TWF0Y2goLyg/Om9wcnxvcGlvcylbXFxzXFwvXShcXGQrKFxcLlxcZCspPykvaSkgfHwgdmVyc2lvbklkZW50aWZpZXJcbiAgICAgIH1cbiAgICB9XG4gICAgZWxzZSBpZiAoL1NhbXN1bmdCcm93c2VyL2kudGVzdCh1YSkpIHtcbiAgICAgIHJlc3VsdCA9IHtcbiAgICAgICAgbmFtZTogJ1NhbXN1bmcgSW50ZXJuZXQgZm9yIEFuZHJvaWQnXG4gICAgICAgICwgc2Ftc3VuZ0Jyb3dzZXI6IHRcbiAgICAgICAgLCB2ZXJzaW9uOiB2ZXJzaW9uSWRlbnRpZmllciB8fCBnZXRGaXJzdE1hdGNoKC8oPzpTYW1zdW5nQnJvd3NlcilbXFxzXFwvXShcXGQrKFxcLlxcZCspPykvaSlcbiAgICAgIH1cbiAgICB9XG4gICAgZWxzZSBpZiAoL2NvYXN0L2kudGVzdCh1YSkpIHtcbiAgICAgIHJlc3VsdCA9IHtcbiAgICAgICAgbmFtZTogJ09wZXJhIENvYXN0J1xuICAgICAgICAsIGNvYXN0OiB0XG4gICAgICAgICwgdmVyc2lvbjogdmVyc2lvbklkZW50aWZpZXIgfHwgZ2V0Rmlyc3RNYXRjaCgvKD86Y29hc3QpW1xcc1xcL10oXFxkKyhcXC5cXGQrKT8pL2kpXG4gICAgICB9XG4gICAgfVxuICAgIGVsc2UgaWYgKC95YWJyb3dzZXIvaS50ZXN0KHVhKSkge1xuICAgICAgcmVzdWx0ID0ge1xuICAgICAgICBuYW1lOiAnWWFuZGV4IEJyb3dzZXInXG4gICAgICAsIHlhbmRleGJyb3dzZXI6IHRcbiAgICAgICwgdmVyc2lvbjogdmVyc2lvbklkZW50aWZpZXIgfHwgZ2V0Rmlyc3RNYXRjaCgvKD86eWFicm93c2VyKVtcXHNcXC9dKFxcZCsoXFwuXFxkKyk/KS9pKVxuICAgICAgfVxuICAgIH1cbiAgICBlbHNlIGlmICgvdWNicm93c2VyL2kudGVzdCh1YSkpIHtcbiAgICAgIHJlc3VsdCA9IHtcbiAgICAgICAgICBuYW1lOiAnVUMgQnJvd3NlcidcbiAgICAgICAgLCB1Y2Jyb3dzZXI6IHRcbiAgICAgICAgLCB2ZXJzaW9uOiBnZXRGaXJzdE1hdGNoKC8oPzp1Y2Jyb3dzZXIpW1xcc1xcL10oXFxkKyg/OlxcLlxcZCspKykvaSlcbiAgICAgIH1cbiAgICB9XG4gICAgZWxzZSBpZiAoL214aW9zL2kudGVzdCh1YSkpIHtcbiAgICAgIHJlc3VsdCA9IHtcbiAgICAgICAgbmFtZTogJ01heHRob24nXG4gICAgICAgICwgbWF4dGhvbjogdFxuICAgICAgICAsIHZlcnNpb246IGdldEZpcnN0TWF0Y2goLyg/Om14aW9zKVtcXHNcXC9dKFxcZCsoPzpcXC5cXGQrKSspL2kpXG4gICAgICB9XG4gICAgfVxuICAgIGVsc2UgaWYgKC9lcGlwaGFueS9pLnRlc3QodWEpKSB7XG4gICAgICByZXN1bHQgPSB7XG4gICAgICAgIG5hbWU6ICdFcGlwaGFueSdcbiAgICAgICAgLCBlcGlwaGFueTogdFxuICAgICAgICAsIHZlcnNpb246IGdldEZpcnN0TWF0Y2goLyg/OmVwaXBoYW55KVtcXHNcXC9dKFxcZCsoPzpcXC5cXGQrKSspL2kpXG4gICAgICB9XG4gICAgfVxuICAgIGVsc2UgaWYgKC9wdWZmaW4vaS50ZXN0KHVhKSkge1xuICAgICAgcmVzdWx0ID0ge1xuICAgICAgICBuYW1lOiAnUHVmZmluJ1xuICAgICAgICAsIHB1ZmZpbjogdFxuICAgICAgICAsIHZlcnNpb246IGdldEZpcnN0TWF0Y2goLyg/OnB1ZmZpbilbXFxzXFwvXShcXGQrKD86XFwuXFxkKyk/KS9pKVxuICAgICAgfVxuICAgIH1cbiAgICBlbHNlIGlmICgvc2xlaXBuaXIvaS50ZXN0KHVhKSkge1xuICAgICAgcmVzdWx0ID0ge1xuICAgICAgICBuYW1lOiAnU2xlaXBuaXInXG4gICAgICAgICwgc2xlaXBuaXI6IHRcbiAgICAgICAgLCB2ZXJzaW9uOiBnZXRGaXJzdE1hdGNoKC8oPzpzbGVpcG5pcilbXFxzXFwvXShcXGQrKD86XFwuXFxkKykrKS9pKVxuICAgICAgfVxuICAgIH1cbiAgICBlbHNlIGlmICgvay1tZWxlb24vaS50ZXN0KHVhKSkge1xuICAgICAgcmVzdWx0ID0ge1xuICAgICAgICBuYW1lOiAnSy1NZWxlb24nXG4gICAgICAgICwga01lbGVvbjogdFxuICAgICAgICAsIHZlcnNpb246IGdldEZpcnN0TWF0Y2goLyg/OmstbWVsZW9uKVtcXHNcXC9dKFxcZCsoPzpcXC5cXGQrKSspL2kpXG4gICAgICB9XG4gICAgfVxuICAgIGVsc2UgaWYgKHdpbmRvd3NwaG9uZSkge1xuICAgICAgcmVzdWx0ID0ge1xuICAgICAgICBuYW1lOiAnV2luZG93cyBQaG9uZSdcbiAgICAgICwgd2luZG93c3Bob25lOiB0XG4gICAgICB9XG4gICAgICBpZiAoZWRnZVZlcnNpb24pIHtcbiAgICAgICAgcmVzdWx0Lm1zZWRnZSA9IHRcbiAgICAgICAgcmVzdWx0LnZlcnNpb24gPSBlZGdlVmVyc2lvblxuICAgICAgfVxuICAgICAgZWxzZSB7XG4gICAgICAgIHJlc3VsdC5tc2llID0gdFxuICAgICAgICByZXN1bHQudmVyc2lvbiA9IGdldEZpcnN0TWF0Y2goL2llbW9iaWxlXFwvKFxcZCsoXFwuXFxkKyk/KS9pKVxuICAgICAgfVxuICAgIH1cbiAgICBlbHNlIGlmICgvbXNpZXx0cmlkZW50L2kudGVzdCh1YSkpIHtcbiAgICAgIHJlc3VsdCA9IHtcbiAgICAgICAgbmFtZTogJ0ludGVybmV0IEV4cGxvcmVyJ1xuICAgICAgLCBtc2llOiB0XG4gICAgICAsIHZlcnNpb246IGdldEZpcnN0TWF0Y2goLyg/Om1zaWUgfHJ2OikoXFxkKyhcXC5cXGQrKT8pL2kpXG4gICAgICB9XG4gICAgfSBlbHNlIGlmIChjaHJvbWVvcykge1xuICAgICAgcmVzdWx0ID0ge1xuICAgICAgICBuYW1lOiAnQ2hyb21lJ1xuICAgICAgLCBjaHJvbWVvczogdFxuICAgICAgLCBjaHJvbWVCb29rOiB0XG4gICAgICAsIGNocm9tZTogdFxuICAgICAgLCB2ZXJzaW9uOiBnZXRGaXJzdE1hdGNoKC8oPzpjaHJvbWV8Y3Jpb3N8Y3JtbylcXC8oXFxkKyhcXC5cXGQrKT8pL2kpXG4gICAgICB9XG4gICAgfSBlbHNlIGlmICgvY2hyb21lLis/IGVkZ2UvaS50ZXN0KHVhKSkge1xuICAgICAgcmVzdWx0ID0ge1xuICAgICAgICBuYW1lOiAnTWljcm9zb2Z0IEVkZ2UnXG4gICAgICAsIG1zZWRnZTogdFxuICAgICAgLCB2ZXJzaW9uOiBlZGdlVmVyc2lvblxuICAgICAgfVxuICAgIH1cbiAgICBlbHNlIGlmICgvdml2YWxkaS9pLnRlc3QodWEpKSB7XG4gICAgICByZXN1bHQgPSB7XG4gICAgICAgIG5hbWU6ICdWaXZhbGRpJ1xuICAgICAgICAsIHZpdmFsZGk6IHRcbiAgICAgICAgLCB2ZXJzaW9uOiBnZXRGaXJzdE1hdGNoKC92aXZhbGRpXFwvKFxcZCsoXFwuXFxkKyk/KS9pKSB8fCB2ZXJzaW9uSWRlbnRpZmllclxuICAgICAgfVxuICAgIH1cbiAgICBlbHNlIGlmIChzYWlsZmlzaCkge1xuICAgICAgcmVzdWx0ID0ge1xuICAgICAgICBuYW1lOiAnU2FpbGZpc2gnXG4gICAgICAsIHNhaWxmaXNoOiB0XG4gICAgICAsIHZlcnNpb246IGdldEZpcnN0TWF0Y2goL3NhaWxmaXNoXFxzP2Jyb3dzZXJcXC8oXFxkKyhcXC5cXGQrKT8pL2kpXG4gICAgICB9XG4gICAgfVxuICAgIGVsc2UgaWYgKC9zZWFtb25rZXlcXC8vaS50ZXN0KHVhKSkge1xuICAgICAgcmVzdWx0ID0ge1xuICAgICAgICBuYW1lOiAnU2VhTW9ua2V5J1xuICAgICAgLCBzZWFtb25rZXk6IHRcbiAgICAgICwgdmVyc2lvbjogZ2V0Rmlyc3RNYXRjaCgvc2VhbW9ua2V5XFwvKFxcZCsoXFwuXFxkKyk/KS9pKVxuICAgICAgfVxuICAgIH1cbiAgICBlbHNlIGlmICgvZmlyZWZveHxpY2V3ZWFzZWx8Znhpb3MvaS50ZXN0KHVhKSkge1xuICAgICAgcmVzdWx0ID0ge1xuICAgICAgICBuYW1lOiAnRmlyZWZveCdcbiAgICAgICwgZmlyZWZveDogdFxuICAgICAgLCB2ZXJzaW9uOiBnZXRGaXJzdE1hdGNoKC8oPzpmaXJlZm94fGljZXdlYXNlbHxmeGlvcylbIFxcL10oXFxkKyhcXC5cXGQrKT8pL2kpXG4gICAgICB9XG4gICAgICBpZiAoL1xcKChtb2JpbGV8dGFibGV0KTtbXlxcKV0qcnY6W1xcZFxcLl0rXFwpL2kudGVzdCh1YSkpIHtcbiAgICAgICAgcmVzdWx0LmZpcmVmb3hvcyA9IHRcbiAgICAgIH1cbiAgICB9XG4gICAgZWxzZSBpZiAoc2lsaykge1xuICAgICAgcmVzdWx0ID0gIHtcbiAgICAgICAgbmFtZTogJ0FtYXpvbiBTaWxrJ1xuICAgICAgLCBzaWxrOiB0XG4gICAgICAsIHZlcnNpb24gOiBnZXRGaXJzdE1hdGNoKC9zaWxrXFwvKFxcZCsoXFwuXFxkKyk/KS9pKVxuICAgICAgfVxuICAgIH1cbiAgICBlbHNlIGlmICgvcGhhbnRvbS9pLnRlc3QodWEpKSB7XG4gICAgICByZXN1bHQgPSB7XG4gICAgICAgIG5hbWU6ICdQaGFudG9tSlMnXG4gICAgICAsIHBoYW50b206IHRcbiAgICAgICwgdmVyc2lvbjogZ2V0Rmlyc3RNYXRjaCgvcGhhbnRvbWpzXFwvKFxcZCsoXFwuXFxkKyk/KS9pKVxuICAgICAgfVxuICAgIH1cbiAgICBlbHNlIGlmICgvc2xpbWVyanMvaS50ZXN0KHVhKSkge1xuICAgICAgcmVzdWx0ID0ge1xuICAgICAgICBuYW1lOiAnU2xpbWVySlMnXG4gICAgICAgICwgc2xpbWVyOiB0XG4gICAgICAgICwgdmVyc2lvbjogZ2V0Rmlyc3RNYXRjaCgvc2xpbWVyanNcXC8oXFxkKyhcXC5cXGQrKT8pL2kpXG4gICAgICB9XG4gICAgfVxuICAgIGVsc2UgaWYgKC9ibGFja2JlcnJ5fFxcYmJiXFxkKy9pLnRlc3QodWEpIHx8IC9yaW1cXHN0YWJsZXQvaS50ZXN0KHVhKSkge1xuICAgICAgcmVzdWx0ID0ge1xuICAgICAgICBuYW1lOiAnQmxhY2tCZXJyeSdcbiAgICAgICwgYmxhY2tiZXJyeTogdFxuICAgICAgLCB2ZXJzaW9uOiB2ZXJzaW9uSWRlbnRpZmllciB8fCBnZXRGaXJzdE1hdGNoKC9ibGFja2JlcnJ5W1xcZF0rXFwvKFxcZCsoXFwuXFxkKyk/KS9pKVxuICAgICAgfVxuICAgIH1cbiAgICBlbHNlIGlmICh3ZWJvcykge1xuICAgICAgcmVzdWx0ID0ge1xuICAgICAgICBuYW1lOiAnV2ViT1MnXG4gICAgICAsIHdlYm9zOiB0XG4gICAgICAsIHZlcnNpb246IHZlcnNpb25JZGVudGlmaWVyIHx8IGdldEZpcnN0TWF0Y2goL3coPzplYik/b3Nicm93c2VyXFwvKFxcZCsoXFwuXFxkKyk/KS9pKVxuICAgICAgfTtcbiAgICAgIC90b3VjaHBhZFxcLy9pLnRlc3QodWEpICYmIChyZXN1bHQudG91Y2hwYWQgPSB0KVxuICAgIH1cbiAgICBlbHNlIGlmICgvYmFkYS9pLnRlc3QodWEpKSB7XG4gICAgICByZXN1bHQgPSB7XG4gICAgICAgIG5hbWU6ICdCYWRhJ1xuICAgICAgLCBiYWRhOiB0XG4gICAgICAsIHZlcnNpb246IGdldEZpcnN0TWF0Y2goL2RvbGZpblxcLyhcXGQrKFxcLlxcZCspPykvaSlcbiAgICAgIH07XG4gICAgfVxuICAgIGVsc2UgaWYgKHRpemVuKSB7XG4gICAgICByZXN1bHQgPSB7XG4gICAgICAgIG5hbWU6ICdUaXplbidcbiAgICAgICwgdGl6ZW46IHRcbiAgICAgICwgdmVyc2lvbjogZ2V0Rmlyc3RNYXRjaCgvKD86dGl6ZW5cXHM/KT9icm93c2VyXFwvKFxcZCsoXFwuXFxkKyk/KS9pKSB8fCB2ZXJzaW9uSWRlbnRpZmllclxuICAgICAgfTtcbiAgICB9XG4gICAgZWxzZSBpZiAoL3F1cHppbGxhL2kudGVzdCh1YSkpIHtcbiAgICAgIHJlc3VsdCA9IHtcbiAgICAgICAgbmFtZTogJ1F1cFppbGxhJ1xuICAgICAgICAsIHF1cHppbGxhOiB0XG4gICAgICAgICwgdmVyc2lvbjogZ2V0Rmlyc3RNYXRjaCgvKD86cXVwemlsbGEpW1xcc1xcL10oXFxkKyg/OlxcLlxcZCspKykvaSkgfHwgdmVyc2lvbklkZW50aWZpZXJcbiAgICAgIH1cbiAgICB9XG4gICAgZWxzZSBpZiAoL2Nocm9taXVtL2kudGVzdCh1YSkpIHtcbiAgICAgIHJlc3VsdCA9IHtcbiAgICAgICAgbmFtZTogJ0Nocm9taXVtJ1xuICAgICAgICAsIGNocm9taXVtOiB0XG4gICAgICAgICwgdmVyc2lvbjogZ2V0Rmlyc3RNYXRjaCgvKD86Y2hyb21pdW0pW1xcc1xcL10oXFxkKyg/OlxcLlxcZCspPykvaSkgfHwgdmVyc2lvbklkZW50aWZpZXJcbiAgICAgIH1cbiAgICB9XG4gICAgZWxzZSBpZiAoL2Nocm9tZXxjcmlvc3xjcm1vL2kudGVzdCh1YSkpIHtcbiAgICAgIHJlc3VsdCA9IHtcbiAgICAgICAgbmFtZTogJ0Nocm9tZSdcbiAgICAgICAgLCBjaHJvbWU6IHRcbiAgICAgICAgLCB2ZXJzaW9uOiBnZXRGaXJzdE1hdGNoKC8oPzpjaHJvbWV8Y3Jpb3N8Y3JtbylcXC8oXFxkKyhcXC5cXGQrKT8pL2kpXG4gICAgICB9XG4gICAgfVxuICAgIGVsc2UgaWYgKGFuZHJvaWQpIHtcbiAgICAgIHJlc3VsdCA9IHtcbiAgICAgICAgbmFtZTogJ0FuZHJvaWQnXG4gICAgICAgICwgdmVyc2lvbjogdmVyc2lvbklkZW50aWZpZXJcbiAgICAgIH1cbiAgICB9XG4gICAgZWxzZSBpZiAoL3NhZmFyaXxhcHBsZXdlYmtpdC9pLnRlc3QodWEpKSB7XG4gICAgICByZXN1bHQgPSB7XG4gICAgICAgIG5hbWU6ICdTYWZhcmknXG4gICAgICAsIHNhZmFyaTogdFxuICAgICAgfVxuICAgICAgaWYgKHZlcnNpb25JZGVudGlmaWVyKSB7XG4gICAgICAgIHJlc3VsdC52ZXJzaW9uID0gdmVyc2lvbklkZW50aWZpZXJcbiAgICAgIH1cbiAgICB9XG4gICAgZWxzZSBpZiAoaW9zZGV2aWNlKSB7XG4gICAgICByZXN1bHQgPSB7XG4gICAgICAgIG5hbWUgOiBpb3NkZXZpY2UgPT0gJ2lwaG9uZScgPyAnaVBob25lJyA6IGlvc2RldmljZSA9PSAnaXBhZCcgPyAnaVBhZCcgOiAnaVBvZCdcbiAgICAgIH1cbiAgICAgIC8vIFdURjogdmVyc2lvbiBpcyBub3QgcGFydCBvZiB1c2VyIGFnZW50IGluIHdlYiBhcHBzXG4gICAgICBpZiAodmVyc2lvbklkZW50aWZpZXIpIHtcbiAgICAgICAgcmVzdWx0LnZlcnNpb24gPSB2ZXJzaW9uSWRlbnRpZmllclxuICAgICAgfVxuICAgIH1cbiAgICBlbHNlIGlmKC9nb29nbGVib3QvaS50ZXN0KHVhKSkge1xuICAgICAgcmVzdWx0ID0ge1xuICAgICAgICBuYW1lOiAnR29vZ2xlYm90J1xuICAgICAgLCBnb29nbGVib3Q6IHRcbiAgICAgICwgdmVyc2lvbjogZ2V0Rmlyc3RNYXRjaCgvZ29vZ2xlYm90XFwvKFxcZCsoXFwuXFxkKykpL2kpIHx8IHZlcnNpb25JZGVudGlmaWVyXG4gICAgICB9XG4gICAgfVxuICAgIGVsc2Uge1xuICAgICAgcmVzdWx0ID0ge1xuICAgICAgICBuYW1lOiBnZXRGaXJzdE1hdGNoKC9eKC4qKVxcLyguKikgLyksXG4gICAgICAgIHZlcnNpb246IGdldFNlY29uZE1hdGNoKC9eKC4qKVxcLyguKikgLylcbiAgICAgfTtcbiAgIH1cblxuICAgIC8vIHNldCB3ZWJraXQgb3IgZ2Vja28gZmxhZyBmb3IgYnJvd3NlcnMgYmFzZWQgb24gdGhlc2UgZW5naW5lc1xuICAgIGlmICghcmVzdWx0Lm1zZWRnZSAmJiAvKGFwcGxlKT93ZWJraXQvaS50ZXN0KHVhKSkge1xuICAgICAgaWYgKC8oYXBwbGUpP3dlYmtpdFxcLzUzN1xcLjM2L2kudGVzdCh1YSkpIHtcbiAgICAgICAgcmVzdWx0Lm5hbWUgPSByZXN1bHQubmFtZSB8fCBcIkJsaW5rXCJcbiAgICAgICAgcmVzdWx0LmJsaW5rID0gdFxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcmVzdWx0Lm5hbWUgPSByZXN1bHQubmFtZSB8fCBcIldlYmtpdFwiXG4gICAgICAgIHJlc3VsdC53ZWJraXQgPSB0XG4gICAgICB9XG4gICAgICBpZiAoIXJlc3VsdC52ZXJzaW9uICYmIHZlcnNpb25JZGVudGlmaWVyKSB7XG4gICAgICAgIHJlc3VsdC52ZXJzaW9uID0gdmVyc2lvbklkZW50aWZpZXJcbiAgICAgIH1cbiAgICB9IGVsc2UgaWYgKCFyZXN1bHQub3BlcmEgJiYgL2dlY2tvXFwvL2kudGVzdCh1YSkpIHtcbiAgICAgIHJlc3VsdC5uYW1lID0gcmVzdWx0Lm5hbWUgfHwgXCJHZWNrb1wiXG4gICAgICByZXN1bHQuZ2Vja28gPSB0XG4gICAgICByZXN1bHQudmVyc2lvbiA9IHJlc3VsdC52ZXJzaW9uIHx8IGdldEZpcnN0TWF0Y2goL2dlY2tvXFwvKFxcZCsoXFwuXFxkKyk/KS9pKVxuICAgIH1cblxuICAgIC8vIHNldCBPUyBmbGFncyBmb3IgcGxhdGZvcm1zIHRoYXQgaGF2ZSBtdWx0aXBsZSBicm93c2Vyc1xuICAgIGlmICghcmVzdWx0LndpbmRvd3NwaG9uZSAmJiAhcmVzdWx0Lm1zZWRnZSAmJiAoYW5kcm9pZCB8fCByZXN1bHQuc2lsaykpIHtcbiAgICAgIHJlc3VsdC5hbmRyb2lkID0gdFxuICAgIH0gZWxzZSBpZiAoIXJlc3VsdC53aW5kb3dzcGhvbmUgJiYgIXJlc3VsdC5tc2VkZ2UgJiYgaW9zZGV2aWNlKSB7XG4gICAgICByZXN1bHRbaW9zZGV2aWNlXSA9IHRcbiAgICAgIHJlc3VsdC5pb3MgPSB0XG4gICAgfSBlbHNlIGlmIChtYWMpIHtcbiAgICAgIHJlc3VsdC5tYWMgPSB0XG4gICAgfSBlbHNlIGlmICh4Ym94KSB7XG4gICAgICByZXN1bHQueGJveCA9IHRcbiAgICB9IGVsc2UgaWYgKHdpbmRvd3MpIHtcbiAgICAgIHJlc3VsdC53aW5kb3dzID0gdFxuICAgIH0gZWxzZSBpZiAobGludXgpIHtcbiAgICAgIHJlc3VsdC5saW51eCA9IHRcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBnZXRXaW5kb3dzVmVyc2lvbiAocykge1xuICAgICAgc3dpdGNoIChzKSB7XG4gICAgICAgIGNhc2UgJ05UJzogcmV0dXJuICdOVCdcbiAgICAgICAgY2FzZSAnWFAnOiByZXR1cm4gJ1hQJ1xuICAgICAgICBjYXNlICdOVCA1LjAnOiByZXR1cm4gJzIwMDAnXG4gICAgICAgIGNhc2UgJ05UIDUuMSc6IHJldHVybiAnWFAnXG4gICAgICAgIGNhc2UgJ05UIDUuMic6IHJldHVybiAnMjAwMydcbiAgICAgICAgY2FzZSAnTlQgNi4wJzogcmV0dXJuICdWaXN0YSdcbiAgICAgICAgY2FzZSAnTlQgNi4xJzogcmV0dXJuICc3J1xuICAgICAgICBjYXNlICdOVCA2LjInOiByZXR1cm4gJzgnXG4gICAgICAgIGNhc2UgJ05UIDYuMyc6IHJldHVybiAnOC4xJ1xuICAgICAgICBjYXNlICdOVCAxMC4wJzogcmV0dXJuICcxMCdcbiAgICAgICAgZGVmYXVsdDogcmV0dXJuIHVuZGVmaW5lZFxuICAgICAgfVxuICAgIH1cblxuICAgIC8vIE9TIHZlcnNpb24gZXh0cmFjdGlvblxuICAgIHZhciBvc1ZlcnNpb24gPSAnJztcbiAgICBpZiAocmVzdWx0LndpbmRvd3MpIHtcbiAgICAgIG9zVmVyc2lvbiA9IGdldFdpbmRvd3NWZXJzaW9uKGdldEZpcnN0TWF0Y2goL1dpbmRvd3MgKChOVHxYUCkoIFxcZFxcZD8uXFxkKT8pL2kpKVxuICAgIH0gZWxzZSBpZiAocmVzdWx0LndpbmRvd3NwaG9uZSkge1xuICAgICAgb3NWZXJzaW9uID0gZ2V0Rmlyc3RNYXRjaCgvd2luZG93cyBwaG9uZSAoPzpvcyk/XFxzPyhcXGQrKFxcLlxcZCspKikvaSk7XG4gICAgfSBlbHNlIGlmIChyZXN1bHQubWFjKSB7XG4gICAgICBvc1ZlcnNpb24gPSBnZXRGaXJzdE1hdGNoKC9NYWMgT1MgWCAoXFxkKyhbX1xcLlxcc11cXGQrKSopL2kpO1xuICAgICAgb3NWZXJzaW9uID0gb3NWZXJzaW9uLnJlcGxhY2UoL1tfXFxzXS9nLCAnLicpO1xuICAgIH0gZWxzZSBpZiAoaW9zZGV2aWNlKSB7XG4gICAgICBvc1ZlcnNpb24gPSBnZXRGaXJzdE1hdGNoKC9vcyAoXFxkKyhbX1xcc11cXGQrKSopIGxpa2UgbWFjIG9zIHgvaSk7XG4gICAgICBvc1ZlcnNpb24gPSBvc1ZlcnNpb24ucmVwbGFjZSgvW19cXHNdL2csICcuJyk7XG4gICAgfSBlbHNlIGlmIChhbmRyb2lkKSB7XG4gICAgICBvc1ZlcnNpb24gPSBnZXRGaXJzdE1hdGNoKC9hbmRyb2lkWyBcXC8tXShcXGQrKFxcLlxcZCspKikvaSk7XG4gICAgfSBlbHNlIGlmIChyZXN1bHQud2Vib3MpIHtcbiAgICAgIG9zVmVyc2lvbiA9IGdldEZpcnN0TWF0Y2goLyg/OndlYnxocHcpb3NcXC8oXFxkKyhcXC5cXGQrKSopL2kpO1xuICAgIH0gZWxzZSBpZiAocmVzdWx0LmJsYWNrYmVycnkpIHtcbiAgICAgIG9zVmVyc2lvbiA9IGdldEZpcnN0TWF0Y2goL3JpbVxcc3RhYmxldFxcc29zXFxzKFxcZCsoXFwuXFxkKykqKS9pKTtcbiAgICB9IGVsc2UgaWYgKHJlc3VsdC5iYWRhKSB7XG4gICAgICBvc1ZlcnNpb24gPSBnZXRGaXJzdE1hdGNoKC9iYWRhXFwvKFxcZCsoXFwuXFxkKykqKS9pKTtcbiAgICB9IGVsc2UgaWYgKHJlc3VsdC50aXplbikge1xuICAgICAgb3NWZXJzaW9uID0gZ2V0Rmlyc3RNYXRjaCgvdGl6ZW5bXFwvXFxzXShcXGQrKFxcLlxcZCspKikvaSk7XG4gICAgfVxuICAgIGlmIChvc1ZlcnNpb24pIHtcbiAgICAgIHJlc3VsdC5vc3ZlcnNpb24gPSBvc1ZlcnNpb247XG4gICAgfVxuXG4gICAgLy8gZGV2aWNlIHR5cGUgZXh0cmFjdGlvblxuICAgIHZhciBvc01ham9yVmVyc2lvbiA9ICFyZXN1bHQud2luZG93cyAmJiBvc1ZlcnNpb24uc3BsaXQoJy4nKVswXTtcbiAgICBpZiAoXG4gICAgICAgICB0YWJsZXRcbiAgICAgIHx8IG5leHVzVGFibGV0XG4gICAgICB8fCBpb3NkZXZpY2UgPT0gJ2lwYWQnXG4gICAgICB8fCAoYW5kcm9pZCAmJiAob3NNYWpvclZlcnNpb24gPT0gMyB8fCAob3NNYWpvclZlcnNpb24gPj0gNCAmJiAhbW9iaWxlKSkpXG4gICAgICB8fCByZXN1bHQuc2lsa1xuICAgICkge1xuICAgICAgcmVzdWx0LnRhYmxldCA9IHRcbiAgICB9IGVsc2UgaWYgKFxuICAgICAgICAgbW9iaWxlXG4gICAgICB8fCBpb3NkZXZpY2UgPT0gJ2lwaG9uZSdcbiAgICAgIHx8IGlvc2RldmljZSA9PSAnaXBvZCdcbiAgICAgIHx8IGFuZHJvaWRcbiAgICAgIHx8IG5leHVzTW9iaWxlXG4gICAgICB8fCByZXN1bHQuYmxhY2tiZXJyeVxuICAgICAgfHwgcmVzdWx0LndlYm9zXG4gICAgICB8fCByZXN1bHQuYmFkYVxuICAgICkge1xuICAgICAgcmVzdWx0Lm1vYmlsZSA9IHRcbiAgICB9XG5cbiAgICAvLyBHcmFkZWQgQnJvd3NlciBTdXBwb3J0XG4gICAgLy8gaHR0cDovL2RldmVsb3Blci55YWhvby5jb20veXVpL2FydGljbGVzL2dic1xuICAgIGlmIChyZXN1bHQubXNlZGdlIHx8XG4gICAgICAgIChyZXN1bHQubXNpZSAmJiByZXN1bHQudmVyc2lvbiA+PSAxMCkgfHxcbiAgICAgICAgKHJlc3VsdC55YW5kZXhicm93c2VyICYmIHJlc3VsdC52ZXJzaW9uID49IDE1KSB8fFxuXHRcdCAgICAocmVzdWx0LnZpdmFsZGkgJiYgcmVzdWx0LnZlcnNpb24gPj0gMS4wKSB8fFxuICAgICAgICAocmVzdWx0LmNocm9tZSAmJiByZXN1bHQudmVyc2lvbiA+PSAyMCkgfHxcbiAgICAgICAgKHJlc3VsdC5zYW1zdW5nQnJvd3NlciAmJiByZXN1bHQudmVyc2lvbiA+PSA0KSB8fFxuICAgICAgICAocmVzdWx0LmZpcmVmb3ggJiYgcmVzdWx0LnZlcnNpb24gPj0gMjAuMCkgfHxcbiAgICAgICAgKHJlc3VsdC5zYWZhcmkgJiYgcmVzdWx0LnZlcnNpb24gPj0gNikgfHxcbiAgICAgICAgKHJlc3VsdC5vcGVyYSAmJiByZXN1bHQudmVyc2lvbiA+PSAxMC4wKSB8fFxuICAgICAgICAocmVzdWx0LmlvcyAmJiByZXN1bHQub3N2ZXJzaW9uICYmIHJlc3VsdC5vc3ZlcnNpb24uc3BsaXQoXCIuXCIpWzBdID49IDYpIHx8XG4gICAgICAgIChyZXN1bHQuYmxhY2tiZXJyeSAmJiByZXN1bHQudmVyc2lvbiA+PSAxMC4xKVxuICAgICAgICB8fCAocmVzdWx0LmNocm9taXVtICYmIHJlc3VsdC52ZXJzaW9uID49IDIwKVxuICAgICAgICApIHtcbiAgICAgIHJlc3VsdC5hID0gdDtcbiAgICB9XG4gICAgZWxzZSBpZiAoKHJlc3VsdC5tc2llICYmIHJlc3VsdC52ZXJzaW9uIDwgMTApIHx8XG4gICAgICAgIChyZXN1bHQuY2hyb21lICYmIHJlc3VsdC52ZXJzaW9uIDwgMjApIHx8XG4gICAgICAgIChyZXN1bHQuZmlyZWZveCAmJiByZXN1bHQudmVyc2lvbiA8IDIwLjApIHx8XG4gICAgICAgIChyZXN1bHQuc2FmYXJpICYmIHJlc3VsdC52ZXJzaW9uIDwgNikgfHxcbiAgICAgICAgKHJlc3VsdC5vcGVyYSAmJiByZXN1bHQudmVyc2lvbiA8IDEwLjApIHx8XG4gICAgICAgIChyZXN1bHQuaW9zICYmIHJlc3VsdC5vc3ZlcnNpb24gJiYgcmVzdWx0Lm9zdmVyc2lvbi5zcGxpdChcIi5cIilbMF0gPCA2KVxuICAgICAgICB8fCAocmVzdWx0LmNocm9taXVtICYmIHJlc3VsdC52ZXJzaW9uIDwgMjApXG4gICAgICAgICkge1xuICAgICAgcmVzdWx0LmMgPSB0XG4gICAgfSBlbHNlIHJlc3VsdC54ID0gdFxuXG4gICAgcmV0dXJuIHJlc3VsdFxuICB9XG5cbiAgdmFyIGJvd3NlciA9IGRldGVjdCh0eXBlb2YgbmF2aWdhdG9yICE9PSAndW5kZWZpbmVkJyA/IG5hdmlnYXRvci51c2VyQWdlbnQgfHwgJycgOiAnJylcblxuICBib3dzZXIudGVzdCA9IGZ1bmN0aW9uIChicm93c2VyTGlzdCkge1xuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgYnJvd3Nlckxpc3QubGVuZ3RoOyArK2kpIHtcbiAgICAgIHZhciBicm93c2VySXRlbSA9IGJyb3dzZXJMaXN0W2ldO1xuICAgICAgaWYgKHR5cGVvZiBicm93c2VySXRlbT09PSAnc3RyaW5nJykge1xuICAgICAgICBpZiAoYnJvd3Nlckl0ZW0gaW4gYm93c2VyKSB7XG4gICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG5cbiAgLyoqXG4gICAqIEdldCB2ZXJzaW9uIHByZWNpc2lvbnMgY291bnRcbiAgICpcbiAgICogQGV4YW1wbGVcbiAgICogICBnZXRWZXJzaW9uUHJlY2lzaW9uKFwiMS4xMC4zXCIpIC8vIDNcbiAgICpcbiAgICogQHBhcmFtICB7c3RyaW5nfSB2ZXJzaW9uXG4gICAqIEByZXR1cm4ge251bWJlcn1cbiAgICovXG4gIGZ1bmN0aW9uIGdldFZlcnNpb25QcmVjaXNpb24odmVyc2lvbikge1xuICAgIHJldHVybiB2ZXJzaW9uLnNwbGl0KFwiLlwiKS5sZW5ndGg7XG4gIH1cblxuICAvKipcbiAgICogQXJyYXk6Om1hcCBwb2x5ZmlsbFxuICAgKlxuICAgKiBAcGFyYW0gIHtBcnJheX0gYXJyXG4gICAqIEBwYXJhbSAge0Z1bmN0aW9ufSBpdGVyYXRvclxuICAgKiBAcmV0dXJuIHtBcnJheX1cbiAgICovXG4gIGZ1bmN0aW9uIG1hcChhcnIsIGl0ZXJhdG9yKSB7XG4gICAgdmFyIHJlc3VsdCA9IFtdLCBpO1xuICAgIGlmIChBcnJheS5wcm90b3R5cGUubWFwKSB7XG4gICAgICByZXR1cm4gQXJyYXkucHJvdG90eXBlLm1hcC5jYWxsKGFyciwgaXRlcmF0b3IpO1xuICAgIH1cbiAgICBmb3IgKGkgPSAwOyBpIDwgYXJyLmxlbmd0aDsgaSsrKSB7XG4gICAgICByZXN1bHQucHVzaChpdGVyYXRvcihhcnJbaV0pKTtcbiAgICB9XG4gICAgcmV0dXJuIHJlc3VsdDtcbiAgfVxuXG4gIC8qKlxuICAgKiBDYWxjdWxhdGUgYnJvd3NlciB2ZXJzaW9uIHdlaWdodFxuICAgKlxuICAgKiBAZXhhbXBsZVxuICAgKiAgIGNvbXBhcmVWZXJzaW9ucyhbJzEuMTAuMi4xJywgICcxLjguMi4xLjkwJ10pICAgIC8vIDFcbiAgICogICBjb21wYXJlVmVyc2lvbnMoWycxLjAxMC4yLjEnLCAnMS4wOS4yLjEuOTAnXSk7ICAvLyAxXG4gICAqICAgY29tcGFyZVZlcnNpb25zKFsnMS4xMC4yLjEnLCAgJzEuMTAuMi4xJ10pOyAgICAgLy8gMFxuICAgKiAgIGNvbXBhcmVWZXJzaW9ucyhbJzEuMTAuMi4xJywgICcxLjA4MDAuMiddKTsgICAgIC8vIC0xXG4gICAqXG4gICAqIEBwYXJhbSAge0FycmF5PFN0cmluZz59IHZlcnNpb25zIHZlcnNpb25zIHRvIGNvbXBhcmVcbiAgICogQHJldHVybiB7TnVtYmVyfSBjb21wYXJpc29uIHJlc3VsdFxuICAgKi9cbiAgZnVuY3Rpb24gY29tcGFyZVZlcnNpb25zKHZlcnNpb25zKSB7XG4gICAgLy8gMSkgZ2V0IGNvbW1vbiBwcmVjaXNpb24gZm9yIGJvdGggdmVyc2lvbnMsIGZvciBleGFtcGxlIGZvciBcIjEwLjBcIiBhbmQgXCI5XCIgaXQgc2hvdWxkIGJlIDJcbiAgICB2YXIgcHJlY2lzaW9uID0gTWF0aC5tYXgoZ2V0VmVyc2lvblByZWNpc2lvbih2ZXJzaW9uc1swXSksIGdldFZlcnNpb25QcmVjaXNpb24odmVyc2lvbnNbMV0pKTtcbiAgICB2YXIgY2h1bmtzID0gbWFwKHZlcnNpb25zLCBmdW5jdGlvbiAodmVyc2lvbikge1xuICAgICAgdmFyIGRlbHRhID0gcHJlY2lzaW9uIC0gZ2V0VmVyc2lvblByZWNpc2lvbih2ZXJzaW9uKTtcblxuICAgICAgLy8gMikgXCI5XCIgLT4gXCI5LjBcIiAoZm9yIHByZWNpc2lvbiA9IDIpXG4gICAgICB2ZXJzaW9uID0gdmVyc2lvbiArIG5ldyBBcnJheShkZWx0YSArIDEpLmpvaW4oXCIuMFwiKTtcblxuICAgICAgLy8gMykgXCI5LjBcIiAtPiBbXCIwMDAwMDAwMDBcIlwiLCBcIjAwMDAwMDAwOVwiXVxuICAgICAgcmV0dXJuIG1hcCh2ZXJzaW9uLnNwbGl0KFwiLlwiKSwgZnVuY3Rpb24gKGNodW5rKSB7XG4gICAgICAgIHJldHVybiBuZXcgQXJyYXkoMjAgLSBjaHVuay5sZW5ndGgpLmpvaW4oXCIwXCIpICsgY2h1bms7XG4gICAgICB9KS5yZXZlcnNlKCk7XG4gICAgfSk7XG5cbiAgICAvLyBpdGVyYXRlIGluIHJldmVyc2Ugb3JkZXIgYnkgcmV2ZXJzZWQgY2h1bmtzIGFycmF5XG4gICAgd2hpbGUgKC0tcHJlY2lzaW9uID49IDApIHtcbiAgICAgIC8vIDQpIGNvbXBhcmU6IFwiMDAwMDAwMDA5XCIgPiBcIjAwMDAwMDAxMFwiID0gZmFsc2UgKGJ1dCBcIjlcIiA+IFwiMTBcIiA9IHRydWUpXG4gICAgICBpZiAoY2h1bmtzWzBdW3ByZWNpc2lvbl0gPiBjaHVua3NbMV1bcHJlY2lzaW9uXSkge1xuICAgICAgICByZXR1cm4gMTtcbiAgICAgIH1cbiAgICAgIGVsc2UgaWYgKGNodW5rc1swXVtwcmVjaXNpb25dID09PSBjaHVua3NbMV1bcHJlY2lzaW9uXSkge1xuICAgICAgICBpZiAocHJlY2lzaW9uID09PSAwKSB7XG4gICAgICAgICAgLy8gYWxsIHZlcnNpb24gY2h1bmtzIGFyZSBzYW1lXG4gICAgICAgICAgcmV0dXJuIDA7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIGVsc2Uge1xuICAgICAgICByZXR1cm4gLTE7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIENoZWNrIGlmIGJyb3dzZXIgaXMgdW5zdXBwb3J0ZWRcbiAgICpcbiAgICogQGV4YW1wbGVcbiAgICogICBib3dzZXIuaXNVbnN1cHBvcnRlZEJyb3dzZXIoe1xuICAgKiAgICAgbXNpZTogXCIxMFwiLFxuICAgKiAgICAgZmlyZWZveDogXCIyM1wiLFxuICAgKiAgICAgY2hyb21lOiBcIjI5XCIsXG4gICAqICAgICBzYWZhcmk6IFwiNS4xXCIsXG4gICAqICAgICBvcGVyYTogXCIxNlwiLFxuICAgKiAgICAgcGhhbnRvbTogXCI1MzRcIlxuICAgKiAgIH0pO1xuICAgKlxuICAgKiBAcGFyYW0gIHtPYmplY3R9ICBtaW5WZXJzaW9ucyBtYXAgb2YgbWluaW1hbCB2ZXJzaW9uIHRvIGJyb3dzZXJcbiAgICogQHBhcmFtICB7Qm9vbGVhbn0gW3N0cmljdE1vZGUgPSBmYWxzZV0gZmxhZyB0byByZXR1cm4gZmFsc2UgaWYgYnJvd3NlciB3YXNuJ3QgZm91bmQgaW4gbWFwXG4gICAqIEBwYXJhbSAge1N0cmluZ30gIFt1YV0gdXNlciBhZ2VudCBzdHJpbmdcbiAgICogQHJldHVybiB7Qm9vbGVhbn1cbiAgICovXG4gIGZ1bmN0aW9uIGlzVW5zdXBwb3J0ZWRCcm93c2VyKG1pblZlcnNpb25zLCBzdHJpY3RNb2RlLCB1YSkge1xuICAgIHZhciBfYm93c2VyID0gYm93c2VyO1xuXG4gICAgLy8gbWFrZSBzdHJpY3RNb2RlIHBhcmFtIG9wdGlvbmFsIHdpdGggdWEgcGFyYW0gdXNhZ2VcbiAgICBpZiAodHlwZW9mIHN0cmljdE1vZGUgPT09ICdzdHJpbmcnKSB7XG4gICAgICB1YSA9IHN0cmljdE1vZGU7XG4gICAgICBzdHJpY3RNb2RlID0gdm9pZCgwKTtcbiAgICB9XG5cbiAgICBpZiAoc3RyaWN0TW9kZSA9PT0gdm9pZCgwKSkge1xuICAgICAgc3RyaWN0TW9kZSA9IGZhbHNlO1xuICAgIH1cbiAgICBpZiAodWEpIHtcbiAgICAgIF9ib3dzZXIgPSBkZXRlY3QodWEpO1xuICAgIH1cblxuICAgIHZhciB2ZXJzaW9uID0gXCJcIiArIF9ib3dzZXIudmVyc2lvbjtcbiAgICBmb3IgKHZhciBicm93c2VyIGluIG1pblZlcnNpb25zKSB7XG4gICAgICBpZiAobWluVmVyc2lvbnMuaGFzT3duUHJvcGVydHkoYnJvd3NlcikpIHtcbiAgICAgICAgaWYgKF9ib3dzZXJbYnJvd3Nlcl0pIHtcbiAgICAgICAgICBpZiAodHlwZW9mIG1pblZlcnNpb25zW2Jyb3dzZXJdICE9PSAnc3RyaW5nJykge1xuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdCcm93c2VyIHZlcnNpb24gaW4gdGhlIG1pblZlcnNpb24gbWFwIHNob3VsZCBiZSBhIHN0cmluZzogJyArIGJyb3dzZXIgKyAnOiAnICsgU3RyaW5nKG1pblZlcnNpb25zKSk7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgLy8gYnJvd3NlciB2ZXJzaW9uIGFuZCBtaW4gc3VwcG9ydGVkIHZlcnNpb24uXG4gICAgICAgICAgcmV0dXJuIGNvbXBhcmVWZXJzaW9ucyhbdmVyc2lvbiwgbWluVmVyc2lvbnNbYnJvd3Nlcl1dKSA8IDA7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gc3RyaWN0TW9kZTsgLy8gbm90IGZvdW5kXG4gIH1cblxuICAvKipcbiAgICogQ2hlY2sgaWYgYnJvd3NlciBpcyBzdXBwb3J0ZWRcbiAgICpcbiAgICogQHBhcmFtICB7T2JqZWN0fSBtaW5WZXJzaW9ucyBtYXAgb2YgbWluaW1hbCB2ZXJzaW9uIHRvIGJyb3dzZXJcbiAgICogQHBhcmFtICB7Qm9vbGVhbn0gW3N0cmljdE1vZGUgPSBmYWxzZV0gZmxhZyB0byByZXR1cm4gZmFsc2UgaWYgYnJvd3NlciB3YXNuJ3QgZm91bmQgaW4gbWFwXG4gICAqIEBwYXJhbSAge1N0cmluZ30gIFt1YV0gdXNlciBhZ2VudCBzdHJpbmdcbiAgICogQHJldHVybiB7Qm9vbGVhbn1cbiAgICovXG4gIGZ1bmN0aW9uIGNoZWNrKG1pblZlcnNpb25zLCBzdHJpY3RNb2RlLCB1YSkge1xuICAgIHJldHVybiAhaXNVbnN1cHBvcnRlZEJyb3dzZXIobWluVmVyc2lvbnMsIHN0cmljdE1vZGUsIHVhKTtcbiAgfVxuXG4gIGJvd3Nlci5pc1Vuc3VwcG9ydGVkQnJvd3NlciA9IGlzVW5zdXBwb3J0ZWRCcm93c2VyO1xuICBib3dzZXIuY29tcGFyZVZlcnNpb25zID0gY29tcGFyZVZlcnNpb25zO1xuICBib3dzZXIuY2hlY2sgPSBjaGVjaztcblxuICAvKlxuICAgKiBTZXQgb3VyIGRldGVjdCBtZXRob2QgdG8gdGhlIG1haW4gYm93c2VyIG9iamVjdCBzbyB3ZSBjYW5cbiAgICogcmV1c2UgaXQgdG8gdGVzdCBvdGhlciB1c2VyIGFnZW50cy5cbiAgICogVGhpcyBpcyBuZWVkZWQgdG8gaW1wbGVtZW50IGZ1dHVyZSB0ZXN0cy5cbiAgICovXG4gIGJvd3Nlci5fZGV0ZWN0ID0gZGV0ZWN0O1xuXG4gIHJldHVybiBib3dzZXJcbn0pO1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvYm93c2VyL3NyYy9ib3dzZXIuanNcbi8vIG1vZHVsZSBpZCA9IDczXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMyA0IiwiLyoqXHJcbiAgKiBFbmNvZGVyL0RlY29kZXIgY2xhc3NcclxuICAqIChjKSBBU29mdCBMdGQsIDIwMDktMjAxMSAoaHR0cDovL2Fzb2Z0LnJ1KVxyXG4gICogU29tZSBpZGVhcyB3ZXJlIHRha2VuIGZyb20gQmFzZTY0IGNsYXNzIGJvcnJvd2VkIGZyb20gaHR0cDovL3dlYnRvb2xraXQuaW5mb1xyXG4gICogQWRhcHRlZCBmb3IgV2VicGFjayBieSBDb21wdXRlcmljYVxyXG4gICpcclxuICAqIFdvcmtzIGZhc3QgaW4gTW96aWxsYS9DaHJvbWUvU2FmYXJpLCBhbmQgcGVyZm9ybWFuY2UgdW5kZXIgSUUgaXMgbXVjaCBiZXR0ZXJcclxuICAqIHRoYW4gb3JpZ2luYWwgQmFzZTY0IGVuY29kZXIvZGVjb2RlclxyXG4gICoqL1xyXG5pbXBvcnQgYm93c2VyIGZyb20gJ2Jvd3NlcidcclxuXHJcbnZhciBFbmNvZGVyID0ge1xyXG5cdHNLZXlTdHJfOiBcIkFCQ0RFRkdISUpLTE1OT1BRUlNUVVZXWFlaYWJjZGVmZ2hpamtsbW5vcHFyc3R1dnd4eXowMTIzNDU2Nzg5Ky89XCIsXHJcblx0YkluaXRpYWxpemVkXzogZmFsc2UsXHJcblx0XHJcblx0aW5pdGlhbGl6ZTogZnVuY3Rpb24oKSB7XHJcblx0XHRpZiAoIXRoaXMuYkluaXRpYWxpemVkXykge1xyXG5cdFx0XHQvLyBwZXJmb3JtIGluaXRpYWxpemF0aW9uXHJcblx0XHRcdHRoaXMuYUhhc2hUYWJfID0gbmV3IEFycmF5KCk7XHJcblx0XHRcdGZvciAodmFyIGkgPSAwOyBpIDwgdGhpcy5zS2V5U3RyXy5sZW5ndGg7ICsraSkge1xyXG5cdFx0XHRcdHZhciBjID0gdGhpcy5zS2V5U3RyXy5jaGFyQ29kZUF0KGkpO1xyXG5cdFx0XHRcdHRoaXMuYUhhc2hUYWJfW2NdID0gaTsgLy8gc3RvcmUgY2hhciBpbmRleFxyXG5cdFx0XHR9XHJcblx0XHRcdHRoaXMuYkluaXRpYWxpemVkXyA9IHRydWU7XHJcblx0XHR9XHJcblx0fSxcclxuXHJcblx0QmFzZTY0RW5jb2RlOiBmdW5jdGlvbiAoc0lucHV0KSB7XHJcblx0XHQvLyBJZiBhdmFpbGFibGUsIHVzZSBNb3ppbGxhL1NhZmFyaS9DaHJvbWUgZmFzdCBuYXRpdmUgYmFzZTY0IGVuY29kZXJcclxuXHRcdGlmICh0eXBlb2YoYnRvYSkgIT0gXCJ1bmRlZmluZWRcIikgcmV0dXJuIGJ0b2Eoc0lucHV0KTtcclxuXHRcdFxyXG5cdFx0Ly8gZW5zdXJlIGhhc2ggdGFibGUgaXMgaW5pdGlhbGl6ZWRcclxuXHRcdHRoaXMuaW5pdGlhbGl6ZSgpO1xyXG5cdFx0dmFyIGFPdXQgPSBuZXcgQXJyYXkoKTtcclxuXHRcdHZhciBjaHIxLCBjaHIyLCBjaHIzLCBlbmMxLCBlbmMyLCBlbmMzLCBlbmM0O1xyXG5cdFx0dmFyIGkgPSAwO1xyXG5cdFx0dmFyIGRpID0gMDtcclxuXHRcdHZhciBsZW4gPSBzSW5wdXQubGVuZ3RoO1xyXG5cdFx0d2hpbGUgKGkgPCBsZW4pIHtcclxuXHRcdFx0Y2hyMSA9IHNJbnB1dC5jaGFyQ29kZUF0KGkrKyk7XHJcblx0XHRcdGNocjIgPSBzSW5wdXQuY2hhckNvZGVBdChpKyspO1xyXG5cdFx0XHRjaHIzID0gc0lucHV0LmNoYXJDb2RlQXQoaSsrKTtcclxuXHJcblx0XHRcdGVuYzEgPSBjaHIxID4+IDI7XHJcblx0XHRcdGVuYzIgPSAoKGNocjEgJiAzKSA8PCA0KSB8IChjaHIyID4+IDQpO1xyXG5cdFx0XHRlbmMzID0gKChjaHIyICYgMTUpIDw8IDIpIHwgKGNocjMgPj4gNik7XHJcblx0XHRcdGVuYzQgPSBjaHIzICYgNjM7XHJcblxyXG5cdFx0XHRpZiAoaXNOYU4oY2hyMikpIHtcclxuXHRcdFx0XHRlbmMzID0gZW5jNCA9IDY0O1xyXG5cdFx0XHR9IGVsc2UgaWYgKGlzTmFOKGNocjMpKSB7XHJcblx0XHRcdFx0ZW5jNCA9IDY0O1xyXG5cdFx0XHR9XHJcblx0XHRcdGFPdXRbZGkrK10gPSB0aGlzLnNLZXlTdHJfLmNoYXJBdChlbmMxKTtcclxuXHRcdFx0YU91dFtkaSsrXSA9IHRoaXMuc0tleVN0cl8uY2hhckF0KGVuYzIpO1xyXG5cdFx0XHRhT3V0W2RpKytdID0gdGhpcy5zS2V5U3RyXy5jaGFyQXQoZW5jMyk7XHJcblx0XHRcdGFPdXRbZGkrK10gPSB0aGlzLnNLZXlTdHJfLmNoYXJBdChlbmM0KTtcclxuXHRcdH1cclxuXHRcdHJldHVybiBhT3V0LmpvaW4oJycpO1xyXG5cdH0sXHJcblxyXG5cdC8vIHB1YmxpYyBtZXRob2QgZm9yIGRlY29kaW5nXHJcblx0QmFzZTY0RGVjb2RlOiBmdW5jdGlvbiAoc0lucHV0KSB7XHJcblx0XHQvLyBJZiBhdmFpbGFibGUsIHVzZSBNb3ppbGxhL1NhZmFyaS9DaHJvbWUgZmFzdCBuYXRpdmUgZGVjb2RlclxyXG5cdFx0aWYgKHR5cGVvZihhdG9iKSAhPSBcInVuZGVmaW5lZFwiKSByZXR1cm4gYXRvYihzSW5wdXQpO1xyXG5cdFx0XHJcblx0XHQvLyBlbnN1cmUgaGFzaCB0YWJsZSBpcyBpbml0aWFsaXplZFxyXG5cdFx0dGhpcy5pbml0aWFsaXplKCk7XHJcblx0XHR2YXIgYU91dCA9IG5ldyBBcnJheSgpO1xyXG5cdFx0dmFyIGNocjEsIGNocjIsIGNocjM7XHJcblx0XHR2YXIgZW5jMSwgZW5jMiwgZW5jMywgZW5jNDtcclxuXHRcdHZhciBpID0gMDtcclxuXHRcdHNJbnB1dCA9IHNJbnB1dC5yZXBsYWNlKC9bXkEtWmEtejAtOVxcK1xcL1xcPV0vZywgXCJcIik7XHJcblx0XHRcclxuXHRcdHZhciBsZW4gPSBzSW5wdXQubGVuZ3RoO1xyXG5cdFx0dmFyIGRpID0gMDtcclxuXHRcdHdoaWxlIChpIDwgbGVuKSB7XHJcblx0XHRcdGVuYzEgPSB0aGlzLmFIYXNoVGFiX1tzSW5wdXQuY2hhckNvZGVBdChpKyspXTtcclxuXHRcdFx0ZW5jMiA9IHRoaXMuYUhhc2hUYWJfW3NJbnB1dC5jaGFyQ29kZUF0KGkrKyldO1xyXG5cdFx0XHRlbmMzID0gdGhpcy5hSGFzaFRhYl9bc0lucHV0LmNoYXJDb2RlQXQoaSsrKV07XHJcblx0XHRcdGVuYzQgPSB0aGlzLmFIYXNoVGFiX1tzSW5wdXQuY2hhckNvZGVBdChpKyspXTtcclxuXHRcdFx0XHJcblx0XHRcdGNocjEgPSAoZW5jMSA8PCAyKSB8IChlbmMyID4+IDQpO1xyXG5cdFx0XHRjaHIyID0gKChlbmMyICYgMTUpIDw8IDQpIHwgKGVuYzMgPj4gMik7XHJcblx0XHRcdGNocjMgPSAoKGVuYzMgJiAzKSA8PCA2KSB8IGVuYzQ7XHJcblx0XHRcdFx0XHJcblx0XHRcdGFPdXRbZGkrK10gPSBTdHJpbmcuZnJvbUNoYXJDb2RlKGNocjEpO1xyXG5cdFx0XHRpZiAoZW5jMyAhPSA2NCkge1xyXG5cdFx0XHRcdGFPdXRbZGkrK10gPSBTdHJpbmcuZnJvbUNoYXJDb2RlKGNocjIpO1xyXG5cdFx0XHR9XHJcblx0XHRcdGlmIChlbmM0ICE9IDY0KSB7XHJcblx0XHRcdFx0YU91dFtkaSsrXSA9IFN0cmluZy5mcm9tQ2hhckNvZGUoY2hyMyk7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHRcdHJldHVybiBhT3V0LmpvaW4oJycpO1xyXG5cdH0sXHJcblxyXG5cdFVURjhFbmNvZGU6IGZ1bmN0aW9uKHNTdHJpbmcpIHtcclxuXHRcdC8vc1N0cmluZyA9IHNTdHJpbmcucmVwbGFjZSgvXFxyXFxuL2csXCJcXG5cIik7XHJcblx0XHR2YXIgbGVuID0gc1N0cmluZy5sZW5ndGg7XHJcblx0XHRpZiAoYm93c2VyLm1zaWUpIHtcclxuXHRcdFx0dmFyIGFCeXRlcyA9IG5ldyBBcnJheSgpO1xyXG5cdFx0XHR2YXIgZGkgPSAwO1xyXG5cdFx0XHRmb3IgKHZhciBuID0gMDsgbiA8IGxlbjsgKytuKSB7XHJcblx0XHRcdFx0dmFyIGMgPSBzU3RyaW5nLmNoYXJDb2RlQXQobik7XHJcblx0XHRcdFx0aWYgKGMgPCAxMjgpIHtcclxuXHRcdFx0XHRcdGFCeXRlc1tkaSsrXSA9IFN0cmluZy5mcm9tQ2hhckNvZGUoYyk7XHJcblx0XHRcdFx0fSBlbHNlIGlmKChjID4gMTI3KSAmJiAoYyA8IDIwNDgpKSB7XHJcblx0XHRcdFx0XHRhQnl0ZXNbZGkrK10gPSBTdHJpbmcuZnJvbUNoYXJDb2RlKChjID4+IDYpIHwgMTkyKTtcclxuXHRcdFx0XHRcdGFCeXRlc1tkaSsrXSA9IFN0cmluZy5mcm9tQ2hhckNvZGUoKGMgJiA2MykgfCAxMjgpO1xyXG5cdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRhQnl0ZXNbZGkrK10gKz0gU3RyaW5nLmZyb21DaGFyQ29kZSgoYyA+PiAxMikgfCAyMjQpO1xyXG5cdFx0XHRcdFx0YUJ5dGVzW2RpKytdICs9IFN0cmluZy5mcm9tQ2hhckNvZGUoKChjID4+IDYpICYgNjMpIHwgMTI4KTtcclxuXHRcdFx0XHRcdGFCeXRlc1tkaSsrXSArPSBTdHJpbmcuZnJvbUNoYXJDb2RlKChjICYgNjMpIHwgMTI4KTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdFx0cmV0dXJuIGFCeXRlcy5qb2luKCcnKTtcclxuXHRcdH0gZWxzZSB7XHJcblx0XHRcdHZhciBzQnl0ZXMgPSBcIlwiO1xyXG5cdFx0XHRmb3IgKHZhciBuID0gMDsgbiA8IGxlbjsgKytuKSB7XHJcblx0XHRcdFx0dmFyIGMgPSBzU3RyaW5nLmNoYXJDb2RlQXQobik7XHJcblx0XHRcdFx0aWYgKGMgPCAxMjgpIHtcclxuXHRcdFx0XHRcdHNCeXRlcyArPSBTdHJpbmcuZnJvbUNoYXJDb2RlKGMpO1xyXG5cdFx0XHRcdH0gZWxzZSBpZiAoKGMgPiAxMjcpICYmIChjIDwgMjA0OCkpIHtcclxuXHRcdFx0XHRcdHNCeXRlcyArPSBTdHJpbmcuZnJvbUNoYXJDb2RlKChjID4+IDYpIHwgMTkyKTtcclxuXHRcdFx0XHRcdHNCeXRlcyArPSBTdHJpbmcuZnJvbUNoYXJDb2RlKChjICYgNjMpIHwgMTI4KTtcclxuXHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0c0J5dGVzICs9IFN0cmluZy5mcm9tQ2hhckNvZGUoKGMgPj4gMTIpIHwgMjI0KTtcclxuXHRcdFx0XHRcdHNCeXRlcyArPSBTdHJpbmcuZnJvbUNoYXJDb2RlKCgoYyA+PiA2KSAmIDYzKSB8IDEyOCk7XHJcblx0XHRcdFx0XHRzQnl0ZXMgKz0gU3RyaW5nLmZyb21DaGFyQ29kZSgoYyAmIDYzKSB8IDEyOCk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHRcdHJldHVybiBzQnl0ZXM7XHJcblx0XHR9XHJcblx0fSxcclxuXHJcblx0VVRGOERlY29kZTogZnVuY3Rpb24gKHNCeXRlcykge1xyXG5cdFx0dmFyIGkgPSAwO1xyXG5cdFx0dmFyIGMxID0gMDtcclxuXHRcdHZhciBjMiA9IDA7XHJcblx0XHR2YXIgYzMgPSAwO1xyXG5cdFx0dmFyIGxlbiA9IHNCeXRlcy5sZW5ndGg7XHJcblx0XHRcclxuXHRcdGlmIChib3dzZXIubXNpKSB7XHJcblx0XHRcdC8vIFVzZSBhcnJheSBqb2luIHRlY2huaXF1ZVxyXG5cdFx0XHR2YXIgYU91dCA9IG5ldyBBcnJheSgpO1xyXG5cdFx0XHR2YXIgZGkgPSAwO1xyXG5cdFx0XHR3aGlsZSAoaSA8IGxlbiApIHtcclxuXHRcdFx0XHRjMSA9IHNCeXRlcy5jaGFyQ29kZUF0KGkpO1xyXG5cdFx0XHRcdGlmIChjMSA8IDEyOCkge1xyXG5cdFx0XHRcdFx0YU91dFtkaSsrXSA9IFN0cmluZy5mcm9tQ2hhckNvZGUoYzEpO1xyXG5cdFx0XHRcdFx0aSsrO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRlbHNlIGlmICgoYzEgPiAxOTEpICYmIChjMSA8IDIyNCkpIHtcclxuXHRcdFx0XHRcdGMyID0gc0J5dGVzLmNoYXJDb2RlQXQoaSsxKTtcclxuXHRcdFx0XHRcdGFPdXRbZGkrK10gPSBTdHJpbmcuZnJvbUNoYXJDb2RlKCgoYzEgJiAzMSkgPDwgNikgfCAoYzIgJiA2MykpO1xyXG5cdFx0XHRcdFx0aSArPSAyO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRlbHNlIHtcclxuXHRcdFx0XHRcdGMyID0gc0J5dGVzLmNoYXJDb2RlQXQoaSsxKTtcclxuXHRcdFx0XHRcdGMzID0gc0J5dGVzLmNoYXJDb2RlQXQoaSsyKTtcclxuXHRcdFx0XHRcdGFPdXRbZGkrK10gPSBTdHJpbmcuZnJvbUNoYXJDb2RlKCgoYzEgJiAxNSkgPDwgMTIpIHwgKChjMiAmIDYzKSA8PCA2KSB8IChjMyAmIDYzKSk7XHJcblx0XHRcdFx0XHRpICs9IDM7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHRcdHJldHVybiBhT3V0LmpvaW4oJycpO1xyXG5cdFx0fSBlbHNlIHtcclxuXHRcdFx0Ly8gVXNlIHN0YW5kYXJkICs9IG9wZXJhdG9yIHRlY2huaXF1ZVxyXG5cdFx0XHR2YXIgc091dCA9IFwiXCI7XHJcblx0XHRcdHdoaWxlIChpIDwgbGVuICkge1xyXG5cdFx0XHRcdGMxID0gc0J5dGVzLmNoYXJDb2RlQXQoaSk7XHJcblx0XHRcdFx0aWYgKGMxIDwgMTI4KSB7XHJcblx0XHRcdFx0XHRzT3V0ICs9IFN0cmluZy5mcm9tQ2hhckNvZGUoYzEpO1xyXG5cdFx0XHRcdFx0aSsrO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRlbHNlIGlmICgoYzEgPiAxOTEpICYmIChjMSA8IDIyNCkpIHtcclxuXHRcdFx0XHRcdGMyID0gc0J5dGVzLmNoYXJDb2RlQXQoaSsxKTtcclxuXHRcdFx0XHRcdHNPdXQgKz0gU3RyaW5nLmZyb21DaGFyQ29kZSgoKGMxICYgMzEpIDw8IDYpIHwgKGMyICYgNjMpKTtcclxuXHRcdFx0XHRcdGkgKz0gMjtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0ZWxzZSB7XHJcblx0XHRcdFx0XHRjMiA9IHNCeXRlcy5jaGFyQ29kZUF0KGkrMSk7XHJcblx0XHRcdFx0XHRjMyA9IHNCeXRlcy5jaGFyQ29kZUF0KGkrMik7XHJcblx0XHRcdFx0XHRzT3V0ICs9IFN0cmluZy5mcm9tQ2hhckNvZGUoKChjMSAmIDE1KSA8PCAxMikgfCAoKGMyICYgNjMpIDw8IDYpIHwgKGMzICYgNjMpKTtcclxuXHRcdFx0XHRcdGkgKz0gMztcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdFx0cmV0dXJuIHNPdXQ7XHJcblx0XHR9XHJcblx0fVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBFbmNvZGVyO1xyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gc3JjL2xpYi9iYXNlNjQuanMiLCJtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKCkge1xyXG5cdHRocm93IG5ldyBFcnJvcihcImRlZmluZSBjYW5ub3QgYmUgdXNlZCBpbmRpcmVjdFwiKTtcclxufTtcclxuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gKHdlYnBhY2spL2J1aWxkaW4vYW1kLWRlZmluZS5qc1xuLy8gbW9kdWxlIGlkID0gNzhcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzIDQiLCIvKiohXG4gKiBhamF4IC0gdjIuMy4wXG4gKiBBamF4IG1vZHVsZSBpbiBWYW5pbGxhIEpTXG4gKiBodHRwczovL2dpdGh1Yi5jb20vZmRhY2l1ay9hamF4XG5cbiAqIFN1biBKdWwgMjMgMjAxNyAxMDo1NTowOSBHTVQtMDMwMCAoQlJUKVxuICogTUlUIChjKSBGZXJuYW5kbyBEYWNpdWtcbiovXG4hZnVuY3Rpb24oZSx0KXtcInVzZSBzdHJpY3RcIjtcImZ1bmN0aW9uXCI9PXR5cGVvZiBkZWZpbmUmJmRlZmluZS5hbWQ/ZGVmaW5lKFwiYWpheFwiLHQpOlwib2JqZWN0XCI9PXR5cGVvZiBleHBvcnRzP2V4cG9ydHM9bW9kdWxlLmV4cG9ydHM9dCgpOmUuYWpheD10KCl9KHRoaXMsZnVuY3Rpb24oKXtcInVzZSBzdHJpY3RcIjtmdW5jdGlvbiBlKGUpe3ZhciByPVtcImdldFwiLFwicG9zdFwiLFwicHV0XCIsXCJkZWxldGVcIl07cmV0dXJuIGU9ZXx8e30sZS5iYXNlVXJsPWUuYmFzZVVybHx8XCJcIixlLm1ldGhvZCYmZS51cmw/bihlLm1ldGhvZCxlLmJhc2VVcmwrZS51cmwsdChlLmRhdGEpLGUpOnIucmVkdWNlKGZ1bmN0aW9uKHIsbyl7cmV0dXJuIHJbb109ZnVuY3Rpb24ocix1KXtyZXR1cm4gbihvLGUuYmFzZVVybCtyLHQodSksZSl9LHJ9LHt9KX1mdW5jdGlvbiB0KGUpe3JldHVybiBlfHxudWxsfWZ1bmN0aW9uIG4oZSx0LG4sdSl7dmFyIGM9W1widGhlblwiLFwiY2F0Y2hcIixcImFsd2F5c1wiXSxpPWMucmVkdWNlKGZ1bmN0aW9uKGUsdCl7cmV0dXJuIGVbdF09ZnVuY3Rpb24obil7cmV0dXJuIGVbdF09bixlfSxlfSx7fSksZj1uZXcgWE1MSHR0cFJlcXVlc3QsZD1yKHQsbixlKTtyZXR1cm4gZi5vcGVuKGUsZCwhMCksZi53aXRoQ3JlZGVudGlhbHM9dS5oYXNPd25Qcm9wZXJ0eShcIndpdGhDcmVkZW50aWFsc1wiKSxvKGYsdS5oZWFkZXJzKSxmLmFkZEV2ZW50TGlzdGVuZXIoXCJyZWFkeXN0YXRlY2hhbmdlXCIsYShpLGYpLCExKSxmLnNlbmQocyhuKSksaS5hYm9ydD1mdW5jdGlvbigpe3JldHVybiBmLmFib3J0KCl9LGl9ZnVuY3Rpb24gcihlLHQsbil7aWYoXCJnZXRcIiE9PW4udG9Mb3dlckNhc2UoKXx8IXQpcmV0dXJuIGU7dmFyIHI9cyh0KSxvPWUuaW5kZXhPZihcIj9cIik+LTE/XCImXCI6XCI/XCI7cmV0dXJuIGUrbytyfWZ1bmN0aW9uIG8oZSx0KXt0PXR8fHt9LHUodCl8fCh0W1wiQ29udGVudC1UeXBlXCJdPVwiYXBwbGljYXRpb24veC13d3ctZm9ybS11cmxlbmNvZGVkXCIpLE9iamVjdC5rZXlzKHQpLmZvckVhY2goZnVuY3Rpb24obil7dFtuXSYmZS5zZXRSZXF1ZXN0SGVhZGVyKG4sdFtuXSl9KX1mdW5jdGlvbiB1KGUpe3JldHVybiBPYmplY3Qua2V5cyhlKS5zb21lKGZ1bmN0aW9uKGUpe3JldHVyblwiY29udGVudC10eXBlXCI9PT1lLnRvTG93ZXJDYXNlKCl9KX1mdW5jdGlvbiBhKGUsdCl7cmV0dXJuIGZ1bmN0aW9uIG4oKXt0LnJlYWR5U3RhdGU9PT10LkRPTkUmJih0LnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJyZWFkeXN0YXRlY2hhbmdlXCIsbiwhMSksZS5hbHdheXMuYXBwbHkoZSxjKHQpKSx0LnN0YXR1cz49MjAwJiZ0LnN0YXR1czwzMDA/ZS50aGVuLmFwcGx5KGUsYyh0KSk6ZVtcImNhdGNoXCJdLmFwcGx5KGUsYyh0KSkpfX1mdW5jdGlvbiBjKGUpe3ZhciB0O3RyeXt0PUpTT04ucGFyc2UoZS5yZXNwb25zZVRleHQpfWNhdGNoKG4pe3Q9ZS5yZXNwb25zZVRleHR9cmV0dXJuW3QsZV19ZnVuY3Rpb24gcyhlKXtyZXR1cm4gaShlKT9mKGUpOmV9ZnVuY3Rpb24gaShlKXtyZXR1cm5cIltvYmplY3QgT2JqZWN0XVwiPT09T2JqZWN0LnByb3RvdHlwZS50b1N0cmluZy5jYWxsKGUpfWZ1bmN0aW9uIGYoZSl7cmV0dXJuIE9iamVjdC5rZXlzKGUpLnJlZHVjZShmdW5jdGlvbih0LG4pe3ZhciByPXQ/dCtcIiZcIjpcIlwiO3JldHVybiByK2QobikrXCI9XCIrZChlW25dKX0sXCJcIil9ZnVuY3Rpb24gZChlKXtyZXR1cm4gZW5jb2RlVVJJQ29tcG9uZW50KGUpfXJldHVybiBlfSk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvQGZkYWNpdWsvYWpheC9kaXN0L2FqYXgubWluLmpzXG4vLyBtb2R1bGUgaWQgPSA3OVxuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDMgNCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBZkE7QUFpQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVRBO0FBV0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBOzs7Ozs7Ozs7Ozs7Ozs7QUNsREE7QUFDQTtBQUNBO0FBREE7QUFDQTs7O0FBQUE7QUFDQTs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUkE7QUFVQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUEyRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOzs7Ozs7O0FDcmtCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7O0FDL2tCQTtBQUNBOzs7OztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBbExBOzs7Ozs7Ozs7QUFxTEE7Ozs7Ozs7QUNoTUE7QUFDQTtBQUNBOzs7Ozs7OztBQ0ZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBOzs7O0EiLCJzb3VyY2VSb290IjoiIn0=