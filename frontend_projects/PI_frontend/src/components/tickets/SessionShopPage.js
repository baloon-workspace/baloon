import React from 'react';
import { observer } from 'mobx-react';
import { appContext } from './SessionTicketsApp';
import { DatePicker } from 'lib/date_picker';
import SelectField from 'lib/select-field';
import Counter from 'lib/counter';
import Checkout from './SessionsCheckout';
//{backgroundImage: "/custom/yaroslavl/human_big_unactive.png"},

@observer
export default class ShopPage extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			page: "shop",
			date: new Date(),
			count: 1,
			sessions: [],
			selectedSession: {},
			promo: "",
			confirmedPromo: "",
			discount: 0,
			babyActive: false,
			calendarActive: false,
			button: false,
			rightsChecked: false,
			showPromoError: false,
            showPromoSuccess: false,
            times: [],
			time: {},
			mail: "",
			bigHumans: [
				"session__human_big-active",
				"session__human_big",
				"session__human_big",
				"session__human_big",
				"session__human_big",
				"session__human_big",
				"session__human_big",
				"session__human_big"
			],
			smallHumans: [
                "session__human_small",
                "session__human_small",
                "session__human_small",
                "session__human_small"
			],
            tariffs: [],
			tariff: {}
		}
        this.parseSmallHumans = this.parseSmallHumans.bind(this);
        this.parseBigHumans = this.parseBigHumans.bind(this);
        this.timeFormat = this.timeFormat.bind(this);
	}

	changeCount = (count) => {
		//this.setState({count: count});
		this.selectHuman(count-1,true) 
	}

	setDate = (date) => {
	/*	this.setState({date: date, selectedSession: {}}, () => {
			this.updateSessions()
		});*/
		this.setState({date: date});
       this.checkTariffs(date);
        this.setState({showPromoError: false,showPromoSuccess: false, discount: 0,promo: "",confirmedPromo: ""});
	}
    setTime = (time) =>{
        this.setState({time: time});
    }
	/*updateSessions = () => {
		appContext.appState.requestSessionsByDate(this.state.date, (sessions) => {
			this.setState({sessions: sessions || []});
		})
	}*/
	checkTariffs = (date) =>{
        appContext.appState.requestServiceRules(date,(data) => {
            if (data.length <= 0) return;
            let times = this.timeFormat(data);
            this.setState({times: times,time: times[0]});
        })
	}
	timeFormat = function(data) {
		let time = [];
        for(let i=0; i< data.length; i++){
            time[i] = {
                id: data[i].id,
				price: data[i].price,
                /*text: data[i]["time_from"].slice(0,2) + ":" + data[i]["time_from"].slice(2,4) + "-" +
                + data[i]["time_upto"].slice(0,2) + ":" +data[i]["time_upto"].slice(2,4)*/
                text: data[i].name
            }
        }
        return time
	}
	submitTicket = () => {
            appContext.appState.order = {
            	promo: this.state.confirmedPromo,
			    discount: this.state.discount,
                date: this.state.date,
                count: this.state.count,
                id: this.state.time.id,
				email: this.state.mail
            }
            appContext.appState.requestPlaceAPI(() => {})
	}

	setShop = () => {
		this.setState({page: "shop"});
	}

	/*selectSession = (index) => {
		this.setState({selectedSession: this.state.sessions[index]});
	}

	checkEnabled = () => {
		if (this.state.selectedSession.id) return true;
		return false
	}*/
	activeBabyHint = () =>{
		this.setState({babyActive: true})

	}
    deactiveBabyHint = () =>{
        this.setState({babyActive: false})
    }
	componentDidMount() {
		/*appContext.appState.requestSessionsByDate(new Date(), (sessions) => {
			this.setState({sessions: sessions});
		})*/
        this.checkTariffs(this.state.date);
        document.addEventListener('mousedown', this.handleClickOutside);
	}
    setWrapperRef = (node) => {
        this.wrapperRef = node;
    }
    handleClickOutside = (event) => {
        if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
            this.setState({calendarActive: false});
        }
    }
	selectHuman = (index,isBig) =>{
		if(isBig){
            let humans = [];
            let unactiveHumans = [];
            for(let i=0;i<=index;i++){
                humans[i]=  "session__human_big-active";
			}
            for(let i=index+1;i<8;i++){
                humans[i]=  "session__human_big";
            }
            for(let i=0;i<8;i++){
                unactiveHumans[i]=  "session__human_small";
            }
            this.setState({tariff: this.state.tariffs[0],count: index+1,bigHumans:humans,smallHumans: unactiveHumans});
        }else{
            let humans = this.state.smallHumans;
            let unactiveHumans = this.state.bigHumans;
            for(let i=0;i<=index;i++){
                humans[i]=  "session__human_small-active";
            }
            for(let i=index+1;i<4;i++){
                humans[i]= "session__human_small";
            }
            for(let i=0;i<4;i++){
                unactiveHumans[i]=  "session__human_big";
            }
            this.setState({tariff: this.state.tariffs[1],count: index+1,smallHumans:humans,bigHumans: unactiveHumans});
		}



	}
	rightsConfirm = () =>{
		this.setState({rightsChecked: true});
        if(this.state.mail !== ""){
            this.setState({button: true})
        }else{
            this.setState({button: false})
		}
	}
	setEmail = e =>{
        this.setState({mail: e.currentTarget.value});
        if(e.currentTarget.value !== ""&& this.state.rightsChecked === true){
            this.setState({button: true})
        }else{
            this.setState({button: false})
        }
	}
	calendarActive = () =>{
		this.setState({calendarActive: !this.state.calendarActive});
	}
	parseBigHumans(){
		let human = [];
		for(let i=0;i<8;i++){
            human.push(<div key={i} className={this.state.bigHumans[i]}/>)
		}//onClick={e =>this.selectHuman(i,true)} 
		return human
	}
    parseSmallHumans(){
        let human = [];
        for(let i=0;i<4;i++){
            human.push(<div key={i}  className={this.state.smallHumans[i]}/>)
        }//onClick={e =>this.selectHuman(i,false)} 
        return human
    }
     promoChange = e =>{
        let value = e.currentTarget.value;
        if(value.length > 15){
        	value = value.slice(0,15);
		}
        this.setState({promo: value,showPromoError: false, showPromoSuccess: false});
	}
	promoCheck = () =>{
        appContext.appState.requestCheckPromo(this.state.promo,this.state.date ,data => {
            //if (data.length <= 0) return;
            if (data['is_active'] === "true"){
                this.setState({discount: data['discount'],showPromoSuccess: true, showPromoError: false, promoDate: this.state.date,confirmedPromo: this.state.promo});
            }
            else{
				this.setState({showPromoError: true,showPromoSuccess: false});
            }
        })
	}


	render() {
		const costs = appContext.appState.costs;
		const sessions = this.state.sessions;

		/*let sessionsHTML = (<div>Сеансов на эту дату нет</div>);
		if (sessions.length) {
			sessionsHTML = (sessions.map((session, i) => {
								const timeMatch = session.name.match(/^(\d+:\d+):\d+-(\d+:\d+):\d+/);
								const time = `${timeMatch[1]}-${timeMatch[2]}`;
								return(
									<li onClick={() => {this.selectSession(i)}} class={this.state.selectedSession.id == session.id ? "selected" : ""}>
										{time}
										<p>{session.price} р.</p>
									</li>
								)
							}))
		}*/
		document.body.style.backgroundColor = "#f2f3f5";
		let disabled = "disabled";
		/*if (this.checkEnabled()) { disabled = ""}*/
		let note;
		if (this.state.showPromoError) {
            note = (<p className="promo-false">Промокод не работает или действителен в другой день</p>)
        }
        if (this.state.showPromoSuccess) {
            note = (<p className="promo-true">Промокод применен</p>)
        }
		let price = this.state.time.price;
		const shop = (
			<div>
				<div className="session-layout">
					<h1>Билеты на каток</h1>
					<div className="session-ticket">
						<h2>Количество человек</h2>
						<div className="session__big">
							{this.parseBigHumans()}
						</div>
						<div className="session__small">

						</div>
						<Counter initValue={1} onChange={this.changeCount} maxValue={8}/>
						<p>До 7 лет бесплатно</p>
					</div>
					<div className="session__right-part">
						<div>
							<div className="session-settings">
								<h1>Билеты на каток</h1>
								<div ref={this.setWrapperRef}  className="session__date">
									<p>
										{this.state.date.toLocaleDateString("ru-Ru")}
									</p>
									{this.state.calendarActive ? <DatePicker handleChange={this.setDate}/>: <div/>}
									<div onClick={this.calendarActive} className="session__calendar"/>
								</div>
                                <div className="ticket-rule">
                                    {
                                        this.state.times.length === 0 ? (<div></div>) : (<SelectField options={this.state.times} selectOption={this.setTime}/>)
                                    }
                                </div>
								<div className="session__email">
									<input onChange={this.setEmail} placeholder="Email"/>
								</div>
								<div className="session__price">
									<p>
										{this.state.time.price*this.state.count-this.state.time.price*this.state.count*this.state.discount/100}
									</p>
									<div>Р</div>
								</div>
								<div className="promo-section">
									<h2>Введите промокод</h2>
									<input value={this.state.promo} type="text" name="promo" onChange={this.promoChange}/>
			                        {note}
									<button className="check-button" onClick={this.promoCheck}>
										Проверить
									</button>
								</div>
								<div className="session__rights">
									<input type="radio" onClick={this.rightsConfirm} checked={this.state.rightsChecked}/>
									<p>
										Согласен с условиями <a href="https://docs.google.com/document/d/1zpB6vg_mmMmcWEmy_J-5to2mBcMzDA4QsQ1spWls1zY/edit?usp=sharing">пользовательского соглашения</a>,
										<a href="http://www.sevcableport.ru/katok-ticket">Правилами покупки билетов</a>, <a href="http://sevcableport.ru/katok-rules">Правилами посещения катка</a>
									</p>
								</div>
								<div className="session__button">
									{this.state.button ? <button onClick={this.submitTicket} className="button-active">Оплатить</button>
										: <button className="button-unactive">Оплатить</button>}
								</div>
							</div>
							<div className="session__info">
								<div className="session__phone-number">+7 931 954 89 04</div>
								<div className="session__text">
									Работаем каждый день с 11:00 до 21:00<br/>*пятница и суббота до 22:30
								</div>
								<div className="session__icons"/>
								<div className="session__text_1" >Организатором и администрацией катка является ООО "K-40".</div><br/>
								<div className="session__text_2" >Местанахождение организатора: Россия, Санкт-Петербург, Гавань В.О.,<br/>
									Кожевенная линия 40 <br/>Тел.: +7 931 954 89 04</div>
							</div>
						</div>
						<div className="session__logos">
							<a href="https://beeline-katok.ru/,">
								<div className="session__logo_1"/>
							</a>
							<a href="https://www.europaplus.ru/">
								<div className="session__logo_2"/>
							</a>
							<a href="http://sevcableport.ru/katok">
								<div className="session__logo_3"/>
							</a>
						</div>
					</div>
				</div>
			</div>
		)

		const checkout = (
			<Checkout goBack={this.setShop} />
		)
		return(
			<div className="shop-layout">
				{this.state.page == "shop" ? shop : (<div></div>)}
				{this.state.page == "checkout" ? checkout : (<div></div>)}
				
			</div>
		)
	}
}
/*<h1>ПОКУПКА БИЛЕТА</h1>
				<div className="ticket-settings">
					<div className="ticket-date">
						<h2>Выберите дату поездки</h2>
						<DatePicker handleChange={this.setDate}/>
					</div>

					<div className="ticket-count">
						<h2>Выберите количество</h2>
						<Counter initValue={1} onChange={this.changeCount}/>
					</div>
				</div>
				<div className="sessions-section">
					<ul>
						{
							sessionsHTML
						}
					</ul>
				</div>
				<div className="submit-section">
					<button disabled={!this.checkEnabled()} className={"purchase-button " + `${disabled}`} onClick={this.submitTicket}>
						Купить
					</button>
				</div>*/
/*<div className="session-settings">
						<div className="session__date">
							<p>
								{this.state.date}
							</p>
							<div/>
						</div>
						<div className="session__email">
							<inputbox/>
						</div>
						<div className="session__price">

						</div>
						<div className="session__rights">

						</div>
						<div className="session__button">
							<button/>
						</div>
					</div>
					<div>



								<div className="session__others">
									<div className="session__bike"/>
									<div className="session__camera"/>
									<div className="session__p"/>
									<div className="session__invalid"/>
								</div>
								<div onMouseEnter={this.activeBabyHint} onMouseLeave={this.deactiveBabyHint} className="session__baby">
									{this.state.babyActive ? <div>
										<div className="session__baby_triangle"/>
										<div className="session__baby_window">Коляску можно оставить в вестибюле</div>
									</div> : <div/>}
								</div>
							</div>
							<div>
								<div className="session__dog"/>
								<div className="session__cola"/>
							</div>




					*/
























