import React from 'react';
import ym from 'react-yandex-metrika';
import { YMInitializer } from 'react-yandex-metrika';
import { observer } from 'mobx-react';
import { appContext } from './TicketApp';
import { DatePicker } from 'lib/date_picker';
import SelectField from 'lib/select-field';
import Counter from 'lib/counter';
import Checkout from './Checkout';

@observer
export default class ShopPage extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			page: "shop",
			date: new Date(),
			count: 1,
			tariff: {
				text: '',
			},
			time: {
                id: 0,
				text: '',
                price: 0,
			},
            tariffs: [],
            times: [],
			specialTariff: undefined
			//rules: []
		}
	}

	changeCount = (count) => {
		this.setState({count: count});
	}
	setSpecialTariff = () =>{
		this.setState({tariff: this.state.specialTariff});
            // appContext.appState.openCheckout();
        console.log(this.state.specialTariff);
        }


	setDate = (date) => {
        /*appContext.appState.requestServiceTimes(date, this.state.tariff.text,(data) => {
            if (data.length <= 0) return;
            let times = [];
            for(let i = 0; i< data.length; i++){
                times[i] = {};
                times[i]['id'] = data[i]['id'];
                times[i]['text'] = this.parseTime(data[i]['time_from'], data[i]['time_upto']);
                times[i]['price'] = data[i]['price'];
            }
            console.log(times);
            console.log(this.state.tariff.text)
            this.setState({times: times, time: times[0]});
        })*/
       appContext.appState.requestServiceRules(date,(data) => {
             if (data.length <= 0) return;
             let tariffs = [];
             console.log(tariffs);
             for(let i = 0; i< data.length; i++){
             	if(data[i]["id"] !== 70){
                    tariffs[i] = {};
                    tariffs[i]['id'] = data[i]['id'];
                    tariffs[i]['text'] = data[i]['name'];
                    tariffs[i]['price'] = data[i]['price'];
				}
				console.log(tariffs);
             }
           this.setState({tariffs: tariffs, tariff: tariffs[0]});
         })
        this.setState({date: date});
	}
	parseTime = (from,to) => from.substr(0,2)+ ":" + from.substr(2,2)+ " - " + to.substr(0,2) + ":" + to.substr(2,2)

	setTariff = (tariff) => {
        /*appContext.appState.requestServiceTimes(this.state.date, tariff.text,(data) => {
            if (data.length <= 0) return;
            let times =[];
            for(let i = 0; i< data.length; i++){
                times[i] = {};
                times[i]['id'] = data[i]['id'];
                times[i]['text'] = this.parseTime(data[i]['time_from'], data[i]['time_upto']);
                times[i]['price'] = data[i]['price'];
            }
            console.log(data);
            this.setState({times: times, time: times[0], tariff: tariff});
        })*/
        this.setState({tariff: tariff});
	}
    setTime = (time) => {
        this.setState({time: time});
    }

	submitTicket = () =>{
        appContext.appState.order = {
            count: this.state.count,
            tariffName: this.state.tariff.text,
            date: this.state.date,
            price: this.state.tariff.price,
            id: this.state.tariff.id
        }
        this.setState({page: "checkout"})
        // appContext.appState.openCheckout();

	}

	setShop = () => {
		this.setState({page: "shop"});
	}
	getSpecial = (date) =>{
        appContext.appState.requestServiceRules(date,(data) => {
            if (data.length <= 0) return;
            let specialTariff = undefined;
            for(let i = 0; i< data.length; i++){
                if(data[i]["id"] === 70){
                    specialTariff = {};
                    specialTariff['id'] = data[i]['id'];
                    specialTariff['text'] = data[i]['name'];
                    specialTariff['price'] = data[i]['price'];
                    console.log(specialTariff);
                }
            }

            this.setState({ specialTariff:specialTariff});
        })
	}
	componentWillMount(){
        let date = new Date()
        date.setMonth(11);
        date.setDate(7);
        console.log(date)
        this.getSpecial(date);
	}
	componentDidMount() {
        /* appContext.appState.requestServiceRules((data) => {
            if (data.length <= 0) return;
             let tariffs = [];
             for(let i = 0; i< data.length; i++){
                 tariffs[i] = {};
                 tariffs[i]['text'] = data[i]['name']
             }
            this.setState({tariffs: tariffs, tariff: tariffs[0]});*/

        appContext.appState.requestServiceRules(this.state.date,(data) => {
            if (data.length <= 0) return;
            let tariffs = [];
            console.log(tariffs);
            for(let i = 0; i< data.length; i++){
                if(data[i]["id"] !== 70){
                    tariffs[i] = {};
                    tariffs[i]['id'] = data[i]['id'];
                    tariffs[i]['text'] = data[i]['name'];
                    tariffs[i]['price'] = data[i]['price'];
                }
            }
            this.setState({tariffs: tariffs, tariff: tariffs[0]});
        })



       // })
		//console.log(this.state.days);*/
       /*	let times = [];
       	let hour = "10", min = "00", i = 0;
       	while(hour !== "19" || min !== "20"){
            if(min ==='60'){
                min="00";
                hour = (parseInt(hour) + 1).toString();
            }
            times[i] = {};
       		times[i].text = hour + ":" + min;
            i++;
            min = (parseInt(min) + 20).toString();
		}*/

		
	}

	parseTariffs = (data) => {
		let tariffs = [];
		for(let i=0;i< data.length; i++) {
            //if (data[i]['name'].indexOf(day['text'].toLowerCase()) !== -1) {
                tariffs[i] = {};
            	tariffs[i]['text'] = data[i]['name'];
                tariffs[i]['id'] = data[i]['id'];
                tariffs[i]['price'] = data[i]['price'];
        //	}
		}
		return tariffs;
	}


	render() {

		const costs = appContext.appState.costs;
		const { tariffs } = this.state;
       // const { times } = this.state;
		//const { days } = this.state;
       // console.log(days);
		/*{costs.map((item, i) => {
							return(<li key={i}>Цена за проезд {item.name} - {item.price} руб. в будни и {item.price} руб. в выходные</li>);
						})}*/
		const shop = (
			<div>

			<h1>ПОКУПКА БИЛЕТА</h1>
				<div className="ticket-settings">
					<div className="ticket-date">
						<h2>Выберите дату посещения</h2>
						<DatePicker handleChange={this.setDate}/>
					</div>
					<div className="ticket-rule">
						<h2>Выберите тариф</h2>
                        {
                            tariffs.length == 0 ? (<div></div>) : (<SelectField options={tariffs} selectOption={this.setTariff}/>)
                        }
					</div>
					<div className="ticket-count">
						<h2>Выберите кол-во билетов</h2>
						<Counter initValue={1} onChange={this.changeCount} maxValue={50}/>
					</div>

					<div className="ticket-cost">
						<h2>Стоимость покупки</h2>
						<p>{this.state.tariff.price*this.state.count} Руб</p>
					</div>
				</div>
				<div className="submit-section">
					<button className="purchase-button" onClick={this.submitTicket}>
						Купить
					</button>
				</div>
				<YMInitializer accounts={[54403639]} options={{  clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    ecommerce:"dataLayer"}}  />
			</div>
		)

		const checkout = (
			<Checkout goBack={this.setShop} />
		)
		return(
			<div className="shop-layout">
				{this.state.page == "shop" ? shop : (<div></div>)}
				{this.state.page == "checkout" ? checkout : (<div></div>)}
			</div>
		)
	}
}

	/*<div style={{zIndex: 2}} className="ticket-rule">
							<h2>Выберите сеанс</h2>
                            {
                                times.length == 0 ? (<div></div>) : (<SelectField options={times} selectOption={this.setTime}/>)
                            }
						</div>
						<div className="ticket-special" onClick={this.setSpecialTariff}>
						{this.state.specialTariff? <div>{this.state.specialTariff.text}</div>: <div/>}
					</div>

						*/

























