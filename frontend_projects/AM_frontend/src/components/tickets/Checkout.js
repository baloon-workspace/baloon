import React from 'react';
import { observer } from 'mobx-react';
import { appContext } from './TicketApp';
import { DatePicker } from 'lib/date_picker';
import ym from 'react-yandex-metrika';
import { YMInitializer } from 'react-yandex-metrika';
import SelectField from 'lib/select-field';
import Counter from 'lib/counter';
import _ from 'lodash';

@observer
export default class Checkout extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			submitEnabled: false,
			email: "",
			emailConfirm: "",
			renderEmailError: false
		}
	}

	emailChange = e => {
		const value = e.currentTarget.value;
		this.setState({email: value, renderEmailError: false}, () => {
			this.checkEnabled();
			appContext.appState.order.email = this.state.email;
		});
		
	}

	emailConfirmChange = e => {
		const value = e.currentTarget.value;
		this.setState({emailConfirm: value, renderEmailError: false}, () => {
			this.checkEnabled();
			appContext.appState.order.email = this.state.email;
		});
		
	}

	checkEnabled = () => {
		if (this.state.email != "" && this.state.emailConfirm != "" && appContext.appState.order.count > 0 && appContext.appState.order.tariff != "") {
			this.setState({submitEnabled: true});
		} else {
			this.setState({submitEnabled: false});
		}
	}
	submitTicket = () => {
		if(this.state.email == this.state.emailConfirm) {
            ym('reachGoal', 'pochta');
			appContext.appState.requestPlaceAPI(() => {})
		} else {
			this.setState({renderEmailError: true});
		}
	}

	goBack = () => {
		// window.location.href = "#/";
		if (this.props.goBack ) {this.props.goBack();}
	}

	render() {
		const { tariffName, time, count, date} = appContext.appState.order;
		let {price} = appContext.appState.order;
		//const ruleDetails = _.find(appContext.appState.rules, function(o) { return o.name == rule.text; });
      	//console.log(appContext.appState.order);
        price = (count || 0)*price.toFixed(2);
        console.log(appContext.appState.order);
		//if (ruleDetails) {
		//}

		let note = null;
		if (this.state.renderEmailError) {
			note = (<p className="email-note">Адреса электронной почты не совпадают.</p>)
		}

		return(

			<div className="shop-layout">
				<h1>ПОКУПКА БИЛЕТА</h1>
				<button className="back-button" onClick={this.goBack}>Назад</button>
				
				<div className="order-positions">			
					<table>
						<tr>
							<th>Дата</th>
							<th>Тариф</th>
							<th>Количество</th>
							<th>Цена</th>
						</tr>
						<tr>
							<td>{date.toLocaleDateString("ru-Ru")}</td>
							<td>{tariffName}</td>
							<td>{count}</td>
							<td>{price} руб</td>
						</tr>
					</table>
				</div>
				<div className="form">
					<label htmlFor="email">Введите адрес почты для получения билетов*</label>
					<input type="text" name="email" onChange={this.emailChange}/>
					<label htmlFor="email-confirm">Повторите адрес электронной почты*</label>
					<input type="text" name="email-confirm" onChange={this.emailConfirmChange}/>
					{note}
				</div>
				<div className="submit-section">
					<div className="total-price">
						<h1>{price} руб </h1>
					</div>
					<button disabled={!this.state.submitEnabled} className={"purchase-button " + (this.state.submitEnabled ? "" : " disbled")} onClick={this.submitTicket}>
						Оплатить
					</button>
					<h3>Уважаемые посетители, для посещении музея Вам необходимо предъявить на кассе электронный билет и получить браслет.</h3>
				</div>
				<YMInitializer accounts={[54403639]} options={{  clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    ecommerce:"dataLayer"}}  />
			</div>
		)
	}
}