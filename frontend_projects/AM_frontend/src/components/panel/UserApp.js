import HistoryAppContext from 'components/HistoryApp';
import UserPanelState from 'stores/UserPanelState';
import UserProfile from 'components/panel/UserProfile';
import UserTransactions from 'components/panel/UserTransactions';
import UserShopPage from 'components/panel/UserShopPage';


import 'stylesheets/user-panel.less'
import 'stylesheets/tickets-shop.less'

const routes = {
	'/': UserProfile,
	'/transactions': UserTransactions,
	'/tickets': UserShopPage

}

const muiTheme = {
	fontFamily: "'WhitneyLight', Arial, Helvetica, sans-serif",
	palette: {
		primary1Color: '#ff0015',
		primary2Color: '#ffbfb6',
		// canvasColor: '#f6f2ec'
	}
}

let appContext = new HistoryAppContext({
	routes: routes,
	appState: UserPanelState,
	scriptUrl: '/assets/user-panel.js'
});

appContext.appState.effi.onLogout(function() {
	window.location.href = "/auth/login.html";
})

export default appContext;
export { appContext, muiTheme };