#include "baloon_auto.h"
Effi::STContainer* GetServiceInstance(Effi::ParamsMapRef params) {
	return new BALOON::Baloon(params);
}

using namespace Effi;
namespace BALOON {
//#include "/usr/local/effi/include/Services/Authorizer/authorizer_auto.h"
using namespace  AUTHORIZER;

//#include "/usr/local/computerica/include/Paygate/paygate_auto.h"
using namespace  PAYGATE;

//#include "/usr/local/computerica/include/Basicsite/basicsite_auto.h"
using namespace  BASICSITE;

//#include "/usr/local/computerica/include/Fiscal/fiscal_auto.h"
using namespace  FISCAL;

//#include "/usr/local/effi/include/Services/Multimedia/multimedia_auto.h"
using namespace  MULTIMEDIA;

	#define LoggingComponent "APPLICATION/BALOON"

	PName< Int > id("id");
	PName< Int64 > userTokenID("userTokenID");
	PName< Str > last_name("last_name");
	PName< Str > first_name("first_name");
	PName< Str > middle_name("middle_name");
	PName< ADate > birthdate("birthdate");
	PName< Str > gender("gender");
	PName< Str > phone("phone");
	PName< Str > email("email");
	PName< Str > regcode("regcode");
	PName< Time > last_sync("last_sync");
	PName< Str > oid("oid");
	PName< ADate > registered_at("registered_at");
	PName< Str > old_password("old_password");
	PName< Str > password("password");
	PName< Str > password2("password2");
	PName< Int > minutes_timeout("minutes_timeout");
	PName< Str > name("name");
	PName< Str > enabled("enabled");
	PName< Int > extid("extid");
	PName< Int > extcode("extcode");
	PName< Str > comment("comment");
	PName< Decimal > package_cost("package_cost");
	PName< Str > identifier_rulename("identifier_rulename");
	PName< Str > token("token");
	PName< Int > token_type("token_type");
	PName< Str > description("description");
	PName< Time > valid_from("valid_from");
	PName< Time > last_register("last_register");
	PName< Str > title("title");
	PName< Str > message("message");
	PName< Str > sent("sent");
	PName< Time > message_date("message_date");
	PName< Int > deviceid("deviceid");
	PName< Int > messageid("messageid");
	PName< Str > sendresult("sendresult");
	PName< Time > delivery_date("delivery_date");
	PName< Str > replenish_enabled("replenish_enabled");
	PName< Str > service_rulename("service_rulename");
	PName< Decimal > weekday_cost("weekday_cost");
	PName< Decimal > weekend_cost("weekend_cost");
	PName< Str > is_default("is_default");
	PName< Int > personid("personid");
	PName< Str > code("code");
	PName< Str > groupname("groupname");
	PName< Time > valid_to("valid_to");
	PName< Str > permanent_rulename("permanent_rulename");
	PName< Str > categoryname("categoryname");
	PName< Str > ratename("ratename");
	PName< Str > clientoid("clientoid");
	PName< Str > status("status");
	PName< Int > spdgateid("spdgateid");
	PName< Str > print_code("print_code");
	PName< Value > categories("categories");
	PName< Decimal > amount("amount");
	PName< Str > link("link");
	PName< Time > filed_at("filed_at");
	PName< Str > promocode("promocode");
	PName< Decimal > final_amount("final_amount");
	PName< Str > sitecode("sitecode");
	PName< Str > success_url("success_url");
	PName< Str > fail_url("fail_url");
	PName< ADate > adate("adate");
	PName< ATime > atime("atime");
	PName< Int > qty("qty");
	PName< Str > identifier_service_rulename("identifier_service_rulename");
	PName< Str > identifier_ratename("identifier_ratename");
	PName< Str > orderdesc("orderdesc");
	PName< Str > timeoffset("timeoffset");
	PName< Int > week_tariffid("week_tariffid");
	PName< Int > skipass_configurationid("skipass_configurationid");
	PName< Str > trxid("trxid");
	PName< Decimal > discount("discount");
	PName< Str > is_active("is_active");
	PName< ADate > first_day("first_day");
	PName< ADate > last_day("last_day");
	PName< Int > use_count("use_count");
	PName< Int > orderline_count("orderline_count");
	PName< Decimal > total_amount("total_amount");
	PName< Int > areaid("areaid");
	PName< Int > enter_timeout("enter_timeout");
	PName< Int > exit_timeout("exit_timeout");
	PName< Int > orderid("orderid");
	PName< Time > end_time("end_time");
	PName< Str > secretkey("secretkey");
	PName< Int > identifierid("identifierid");
	PName< Time > time_from("time_from");
	PName< Time > time_upto("time_upto");
	PName< Decimal > price("price");
	PName< Int > fiscal_docid("fiscal_docid");
	PName< Str > make_transaction("make_transaction");
	PName< Int > paygateid("paygateid");
	PName< Str > eid("eid");
	PName< Int > default_paygateid("default_paygateid");
	PName< Int > default_fiscalgateid("default_fiscalgateid");
	PName< Str > code_conversion_algo("code_conversion_algo");
	PName< Double > agent_commission("agent_commission");
	PName< Decimal > ident_sale_price("ident_sale_price");
	PName< Int > timeout_before("timeout_before");
	PName< Str > sun("sun");
	PName< Str > mon("mon");
	PName< Str > tue("tue");
	PName< Str > wed("wed");
	PName< Str > thu("thu");
	PName< Str > fri("fri");
	PName< Str > sat("sat");
	PName< Str > uri("uri");
	PName< Str > comments("comments");
	PName< Double > commission("commission");
	PName< Int > agentid("agentid");
	PName< Str > AgentOrderType("AgentOrderType");
	PName< Str > ItemRef("ItemRef");
	PName< Str > other_card_code("other_card_code");
	PName< ADate > current_date("current_date");
	PName< Decimal > total_date_amount("total_date_amount");
	PName< Decimal > avarage_date_order_amount("avarage_date_order_amount");
	PName< Decimal > avarage_order_amount("avarage_order_amount");
	PName< Int > orders_date_count("orders_date_count");
	PName< Int > orders_count("orders_count");
	PName< Decimal > date_commission("date_commission");
	PName< Decimal > total_commission("total_commission");
	PName< Int > identifier_count("identifier_count");
	PName< Str > currency("currency");
	PName< Str > valid("valid");
	PName< Decimal > balance("balance");
	PName< Str > in_sync("in_sync");
	PName< Time > valid_until("valid_until");
	PName< Str > meta("meta");
	PName< Str > text_content("text_content");
	PName< Str > html_link("html_link");
	PName< Time > posted_at("posted_at");
	PName< Str > thumbnail("thumbnail");
	PName< Int > items_qty("items_qty");
	PName< BlobFile > image("image");
	PName< Str > widget("widget");
	PName< Int > priority("priority");
	PName< Str > out_of_service("out_of_service");
	PName< Value > holidays("holidays");
	PName< Int > partnerid("partnerid");
	PName< Int > agroupid("agroupid");
	PName< Str > week_day("week_day");
	PName< Str > pricetype("pricetype");
	PName< Int > free("free");
	PName< Int > total("total");
	PName< Str > counttype("counttype");
	PName< Str > external_name("external_name");
	PName< Str > identifier_categoryname("identifier_categoryname");
	PName< Value > week_days("week_days");
	PName< Value > tariffs("tariffs");
	PName< Str > agroupname("agroupname");
	PName< Value > configurations("configurations");
	PName< Str > host("host");
	PName< Str > port("port");
	PName< Value > names("names");
	PName< Int > categoryid("categoryid");
	PName< Int > tariffid("tariffid");
	PName< Int > hourid("hourid");
	PName< Value > services("services");
	PName< BlobFile > pricelist("pricelist");
	PName< Str > legal_address("legal_address");
	PName< Str > post_address("post_address");
	PName< Str > inn("inn");
	PName< Str > kpp("kpp");
	PName< Str > payment_account("payment_account");
	PName< Str > correspondent_account("correspondent_account");
	PName< Str > bank("bank");
	PName< Str > bik("bik");
	PName< Str > okpo("okpo");
	PName< Str > okato("okato");
	PName< Str > okved("okved");
	PName< Str > ogrn("ogrn");
	PName< Str > principal("principal");
	PName< Str > fax("fax");
	PName< Str > notification_email("notification_email");
	PName< Str > contractbarcode("contractbarcode");
	PName< Str > accountname("accountname");
	PName< Int > debitaccountid("debitaccountid");
	PName< Str > debitaccountname("debitaccountname");
	PName< Str > debitaccountatype("debitaccountatype");
	PName< Str > debitaccountstatus("debitaccountstatus");
	PName< Decimal > debitaccountamount("debitaccountamount");
	PName< Str > debitaccountexists("debitaccountexists");
	PName< Decimal > debitrefill_amount("debitrefill_amount");
	PName< Str > debitrefill_reason("debitrefill_reason");
	PName< Int > creditaccountid("creditaccountid");
	PName< Str > creditaccountname("creditaccountname");
	PName< Str > creditaccountatype("creditaccountatype");
	PName< Str > creditaccountstatus("creditaccountstatus");
	PName< Decimal > creditaccountamount("creditaccountamount");
	PName< Str > creditaccountexists("creditaccountexists");
	PName< Str > managerfullname("managerfullname");
	PName< Str > manageremail("manageremail");
	PName< Str > managerphone("managerphone");
	PName< ADate > contractend_date("contractend_date");
	PName< Decimal > contractbenefit_amount("contractbenefit_amount");
	PName< Str > contractbenefit_type("contractbenefit_type");
	PName< Int64 > auserTokenID("auserTokenID");
	PName< Str > fullname("fullname");
	PName< Str > passport_number("passport_number");
	PName< ADate > issue_date("issue_date");
	PName< Str > issued_by("issued_by");
	PName< Str > department_code("department_code");
	PName< Str > barcode("barcode");
	PName< ADate > start_date("start_date");
	PName< ADate > end_date("end_date");
	PName< Str > benefit_type("benefit_type");
	PName< Decimal > benefit_amount("benefit_amount");
	PName< Str > atype("atype");
	PName< Str > reason("reason");
	PName< Decimal > refill_amount("refill_amount");
	PName< Int > accountid("accountid");
	PName< Str > cache_flow("cache_flow");
	PName< Decimal > current_balance("current_balance");
	PName< Int > tariffgroupid("tariffgroupid");
	PName< Decimal > initial_cost("initial_cost");
	PName< Decimal > total_cost("total_cost");
	PName< Int > identifiers_count("identifiers_count");
	PName< Int > tickets_count("tickets_count");
	PName< Str > is_public("is_public");
	PName< Value > identifiers("identifiers");

	const string BOOL_FALSE="false";
	const string BOOL_TRUE="true";
	const string both="both";
	const string none="none";
	const string forward="forward";
	const string back="back";
	const string rectangle="rectangle";
	const string circle="circle";
	const string ellipse="ellipse";
	const string diamond="diamond";
	const string note="note";
	const string tab="tab";
	const string folder="folder";
	const string component="component";
	const string AND="and";
	const string OR="or";
	const string PRESENT_DAY="present_day";
	const string PREVIOUS_DAY="previous_day";
	const string PRESENT_WEEK="present_week";
	const string PREVIOUS_WEEK="previous_week";
	const string PRESENT_MONTH="present_month";
	const string PREVIOUS_MONTH="previous_month";
	const string OTHER="";
	const string AUTH_SIGNUP="signup";
	const string AUTH_RESTOREPWD="restorepwd";
	const string ENABLED="enabled";
	const string DISABLED="disabled";
	const string man="man";
	const string lady="lady";
	const string CODE_CONVERSION_ALGO_REVERSE_HEX="revese-hex";
	const string AGENT_ORDER_STATUS_OFFLINE="offline";
	const string AGENT_ORDER_STATUS_WAITING="waiting";
	const string AGENT_ORDER_STATUS_PAYED="payed";
	const string AGENT_ORDER_STATUS_EXPIRED="expired";
	const string AGENT_ORDER_STATUS_REJECTED="canceled";
	const string AGENT_ORDER_STATUS_CANCELED="canceled";
	const string AGENT_ORDER_STATUS_REFUNDED="refunded";
	const string AGENT_ORDER_STATUS_ERROR="error";
	const string PERSON_ORDER_STATUS_WAITING="waiting";
	const string PERSON_ORDER_STATUS_PAYED="payed";
	const string PERSON_ORDER_STATUS_EXPIRED="expired";
	const string PERSON_ORDER_STATUS_REJECTED="canceled";
	const string PERSON_ORDER_STATUS_CANCELED="canceled";
	const string PERSON_ORDER_STATUS_REFUNDED="refunded";
	const string PERSON_ORDER_STATUS_ERROR="error";
	const string PERSON_ORDER_STATUS_PAYMENT_NOT_REQUIRED="not_required";
	const string WEEKDAY="WEEKDAY";
	const string WEEKEND="WEEKEND";
	const string HOLIDAY="HOLIDAY";
	const string IDENTIFIER_STATUS_OK="ok";
	const string IDENTIFIER_STATUS_CANCELED="canceled";
	const string IDENTIFIER_STATUS_WAITING="waiting";
	const string SUNDAY="DAY_7";
	const string MONDAY="DAY_1";
	const string TUESDAY="DAY_2";
	const string WEDNESDAY="DAY_3";
	const string THURSDAY="DAY_4";
	const string FRIDAY="DAY_5";
	const string SATURDAY="DAY_6";
	const string HIGHDAY="DAY_X";
	const string INFO_WIDGET="INFO_WIDGET";
	const string AD_WIDGET="AD_WIDGET";
	const string DISCOUNT_BENEFIT="DISCOUNT_BENEFIT";
	const string REWARD_BENEFIT="REWARD_BENEFIT";
	const string BLOCKED="BLOCKED";
	const string ACTIVE="ACTIVE";
	const string REFILL="REFILL";
	const string EXPENSE="EXPENSE";
	const string DEBIT_ACCOUNT="DEBIT_ACCOUNT";
	const string CREDIT_ACCOUNT="CREDIT_ACCOUNT";
	const string BALOON_AGENTORDER_AGENTORDER="AgentOrder";
	const string BALOON_AGENTORDER_IDENTIFIERAGENTORDER="IdentifierAgentOrder";
	const string BALOON_AGENTORDER_PACKAGEAGENTORDER="PackageAgentOrder";

const Value BOOLValues() {
	Value en;
	en["Enums"]["false"]="false";
	en["Enums"]["true"]="true";
	return en;
}

const Value ARROWDIRValues() {
	Value en;
	en["Enums"]["both"]="both";
	en["Enums"]["none"]="none";
	en["Enums"]["forward"]="forward";
	en["Enums"]["back"]="back";
	return en;
}

const Value SHAPEValues() {
	Value en;
	en["Enums"]["rectangle"]="rectangle";
	en["Enums"]["circle"]="circle";
	en["Enums"]["ellipse"]="ellipse";
	en["Enums"]["diamond"]="diamond";
	en["Enums"]["note"]="note";
	en["Enums"]["tab"]="tab";
	en["Enums"]["folder"]="folder";
	en["Enums"]["component"]="component";
	return en;
}

const Value AND_ORValues() {
	Value en;
	en["Enums"]["and"]="AND";
	en["Enums"]["or"]="OR";
	return en;
}

const Value PERIOD_TYPEValues() {
	Value en;
	en["Enums"]["present_day"]="Present day";
	en["Enums"]["previous_day"]="Previous day";
	en["Enums"]["present_week"]="Present week";
	en["Enums"]["previous_week"]="Previous week";
	en["Enums"]["present_month"]="Present month";
	en["Enums"]["previous_month"]="Previous month";
	en["Enums"][""]="Set specific interval";
	return en;
}

const Value AUTH_TYPEValues() {
	Value en;
	en["Enums"]["signup"]="Signup";
	en["Enums"]["restorepwd"]="Restore Password";
	return en;
}

const Value ENABLED_STATUSValues() {
	Value en;
	en["Enums"]["enabled"]="Enabled";
	en["Enums"]["disabled"]="Disabled";
	return en;
}

const Value GENDERValues() {
	Value en;
	en["Enums"]["man"]="Man";
	en["Enums"]["lady"]="Lady";
	return en;
}

const Value CODE_CONVERSION_ALGOValues() {
	Value en;
	en["Enums"]["revese-hex"]="Reverse Hex";
	return en;
}

const Value AGENT_ORDER_STATUSValues() {
	Value en;
	en["Enums"]["offline"]="Offlne payment";
	en["Enums"]["waiting"]="Waiting";
	en["Enums"]["payed"]="Payed";
	en["Enums"]["expired"]="Expired";
	en["Enums"]["canceled"]="Rejected";
	en["Enums"]["canceled"]="Canceled";
	en["Enums"]["refunded"]="Refunded";
	en["Enums"]["error"]="Error";
	return en;
}

const Value PERSON_ORDER_STATUSValues() {
	Value en;
	en["Enums"]["waiting"]="Waiting";
	en["Enums"]["payed"]="Payed";
	en["Enums"]["expired"]="Expired";
	en["Enums"]["canceled"]="Rejected";
	en["Enums"]["canceled"]="Canceled";
	en["Enums"]["refunded"]="Refunded";
	en["Enums"]["error"]="Error";
	en["Enums"]["not_required"]="Payment not required";
	return en;
}

const Value DAY_TYPEValues() {
	Value en;
	en["Enums"]["WEEKDAY"]="Weekday";
	en["Enums"]["WEEKEND"]="Weekend";
	en["Enums"]["HOLIDAY"]="Holiday";
	return en;
}

const Value IDENTIFIER_STATUSValues() {
	Value en;
	en["Enums"]["ok"]="Ok";
	en["Enums"]["canceled"]="Canceled";
	en["Enums"]["waiting"]="Waiting";
	return en;
}

const Value WEEK_DAYValues() {
	Value en;
	en["Enums"]["DAY_7"]="Sunday";
	en["Enums"]["DAY_1"]="Monday";
	en["Enums"]["DAY_2"]="Tuesday";
	en["Enums"]["DAY_3"]="Wednesday";
	en["Enums"]["DAY_4"]="Thursday";
	en["Enums"]["DAY_5"]="Friday";
	en["Enums"]["DAY_6"]="Saturday";
	en["Enums"]["DAY_X"]="Holiday";
	return en;
}

const Value WIDGET_TYPEValues() {
	Value en;
	en["Enums"]["INFO_WIDGET"]="Information";
	en["Enums"]["AD_WIDGET"]="Advertisement";
	return en;
}

const Value PARTNER_BENEFITValues() {
	Value en;
	en["Enums"]["DISCOUNT_BENEFIT"]="Public Tariff Discount";
	en["Enums"]["REWARD_BENEFIT"]="Arbitrary Price";
	return en;
}

const Value BLOCK_STATUSValues() {
	Value en;
	en["Enums"]["BLOCKED"]="Blocked";
	en["Enums"]["ACTIVE"]="Active";
	return en;
}

const Value CACHE_FLOWValues() {
	Value en;
	en["Enums"]["REFILL"]="Refill";
	en["Enums"]["EXPENSE"]="Expense";
	return en;
}

const Value ACCOUNT_TYPEValues() {
	Value en;
	en["Enums"]["DEBIT_ACCOUNT"]="Debit";
	en["Enums"]["CREDIT_ACCOUNT"]="Credit";
	return en;
}

const Value AgentOrder_TYPEValues() {
	Value en;
	en["Enums"]["AgentOrder"]="Fillup Order";
	en["Enums"]["IdentifierAgentOrder"]="Identifier Order";
	en["Enums"]["PackageAgentOrder"]="Package Order";
	return en;
}

void BALOONPerson::initialize(ParamsMapRef params) {
	RegisterMethod("Register_API", (&BALOONPerson::__Register_API));
	RegisterMethod("RegisterNewClient_API", (&BALOONPerson::__RegisterNewClient_API));
	RegisterMethod("CreateNewClient_API", (&BALOONPerson::__CreateNewClient_API));
	RegisterMethod("AddToContour", (&BALOONPerson::__AddToContour));
	RegisterMethod("ShowRegistration", (&BALOONPerson::__ShowRegistration));
	RegisterMethod("ShowUserPanel", (&BALOONPerson::__ShowUserPanel));
	RegisterMethod("ShowTicketsShop", (&BALOONPerson::__ShowTicketsShop));
	RegisterMethod("ShowSuccessPayment", (&BALOONPerson::__ShowSuccessPayment));
	RegisterMethod("ShowFailedPayment", (&BALOONPerson::__ShowFailedPayment));
	RegisterMethod("ShowSuccessPaymentIOS", (&BALOONPerson::__ShowSuccessPaymentIOS));
	RegisterMethod("ShowSuccessDownloadPayment", (&BALOONPerson::__ShowSuccessDownloadPayment));
	RegisterMethod("GetCurrent", (&BALOONPerson::__GetCurrent));
	RegisterMethod("UpdateCurrent", (&BALOONPerson::__UpdateCurrent));
	RegisterMethod("ChangeCurrentPassword", (&BALOONPerson::__ChangeCurrentPassword));
	RegisterMethod("GetCurrentAccounts", (&BALOONPerson::__GetCurrentAccounts));
	RegisterMethod("Add", (&BALOONPerson::__Add));
	RegisterMethod("Get", (&BALOONPerson::__Get));
	RegisterMethod("Update", (&BALOONPerson::__Update));
	RegisterMethod("Delete", (&BALOONPerson::__Delete));
	RegisterMethod("PersonListGet", (&BALOONPerson::__PersonListGet));
	InitDatabase(params);
}

 const Value BALOONPerson::__Register_API(const Param& param) {
	return Register_API(param["regcode"], param["email"], param["password"], param["password2"], param["phone"]);
}

 const Value BALOONPerson::__RegisterNewClient_API(const Param& param) {
	return RegisterNewClient_API(param["regcode"], param["email"], param["password"], param["password2"], param["phone"], param["last_name"], param["first_name"], param["middle_name"], param["birthdate"], param["gender"]);
}

 const Value BALOONPerson::__CreateNewClient_API(const Param& param) {
	return CreateNewClient_API(param["email"], param["password"], param["password2"], param["last_name"], param["first_name"], param["middle_name"], param["birthdate"], param["gender"]);
}

 const Value BALOONPerson::__AddToContour(const Param& param) {
	return AddToContour(param["id"]);
}

 const Value BALOONPerson::__ShowRegistration(const Param& param) {
	return ShowRegistration();
}

 const Value BALOONPerson::__ShowUserPanel(const Param& param) {
	return ShowUserPanel();
}

 const Value BALOONPerson::__ShowTicketsShop(const Param& param) {
	return ShowTicketsShop();
}

 const Value BALOONPerson::__ShowSuccessPayment(const Param& param) {
	return ShowSuccessPayment();
}

 const Value BALOONPerson::__ShowFailedPayment(const Param& param) {
	return ShowFailedPayment();
}

 const Value BALOONPerson::__ShowSuccessPaymentIOS(const Param& param) {
	return ShowSuccessPaymentIOS();
}

 const Value BALOONPerson::__ShowSuccessDownloadPayment(const Param& param) {
	return ShowSuccessDownloadPayment();
}

 const Value BALOONPerson::__GetCurrent(const Param& param) {
	return GetCurrent();
}

 const Value BALOONPerson::__UpdateCurrent(const Param& param) {
	return UpdateCurrent(param["last_name"], param["first_name"], param["middle_name"], param["birthdate"], param["gender"], param["phone"]);
}

 const Value BALOONPerson::__ChangeCurrentPassword(const Param& param) {
	return ChangeCurrentPassword(param["old_password"], param["password"], param["password2"]);
}

 const Value BALOONPerson::__GetCurrentAccounts(const Param& param) {
	return GetCurrentAccounts();
}

// Wrappers for automatic methods
const Value BALOONPerson::__Add(const Param& param) {
	return Add(param["userTokenID"], param["last_name"], param["first_name"], param["middle_name"], param["birthdate"], param["gender"], param["phone"], param["email"], param["regcode"], param["last_sync"], param["oid"], param["registered_at"]);
}

const Value BALOONPerson::__Update(const Param& param) {
	return Update(param["id"], param["userTokenID"], param["last_name"], param["first_name"], param["middle_name"], param["birthdate"], param["gender"], param["phone"], param["email"], param["regcode"], param["last_sync"], param["oid"], param["registered_at"]);
}

const Value BALOONPerson::__Delete(const Param& param) {
	return Delete(param["id"]);
}

const Value BALOONPerson::__Get(const Param& param) {
	return Get(param["id"]);
}

const Value BALOONPerson::__PersonListGet(const Param& param) {
	return PersonListGet(param["userTokenID"]);
}

// Implementation of automatic methods
const Value BALOONPerson::Add(const Int64 userTokenID, const Str last_name, const Str first_name, const Str middle_name, const ADate birthdate, const Str gender, const Str phone, const Str email, const Str regcode, const Time last_sync, const Str oid, const ADate registered_at) {
	Exception e((Message("Cannot add ") << Message("Client").What() << ". ").What());
	bool error=false;
	error |= Defined(userTokenID)  && !__CheckIDExists(Users()->TokenID, *userTokenID, "User", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		Person aPerson;
		aPerson.userTokenID=userTokenID;
		aPerson.last_name=last_name;
		aPerson.first_name=first_name;
		aPerson.middle_name=middle_name;
		aPerson.birthdate=birthdate;
		aPerson.gender=gender;
		aPerson.phone=phone;
		aPerson.email=email;
		aPerson.regcode=regcode;
		aPerson.last_sync=last_sync;
		aPerson.oid=oid;
		aPerson.registered_at=registered_at;
		Int sk=aPerson.Insert(rdb_);
		Value res;
		res["id"]=sk;
		AddMessage(Message()<<Message("Client").What()<<" "<<sk<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONPerson::Update(const Int id, const optional<Int64> userTokenID, const optional<Str> last_name, const optional<Str> first_name, const optional<Str> middle_name, const optional<ADate> birthdate, const optional<Str> gender, const optional<Str> phone, const optional<Str> email, const optional<Str> regcode, const optional<Time> last_sync, const optional<Str> oid, const optional<ADate> registered_at) {
	Exception e((Message("Cannot update ") << Message("Client").What() << ". ").What());
	bool error=false;
	error |= Defined(userTokenID) && Defined(*userTokenID)  && !__CheckIDExists(Users()->TokenID, *userTokenID, "User", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		Person aPerson;
		aPerson.id=id;
		aPerson.Select(rdb_);
		if(Defined(userTokenID)) aPerson.userTokenID=*userTokenID;
		if(Defined(last_name)) aPerson.last_name=*last_name;
		if(Defined(first_name)) aPerson.first_name=*first_name;
		if(Defined(middle_name)) aPerson.middle_name=*middle_name;
		if(Defined(birthdate)) aPerson.birthdate=*birthdate;
		if(Defined(gender)) aPerson.gender=*gender;
		if(Defined(phone)) aPerson.phone=*phone;
		if(Defined(email)) aPerson.email=*email;
		if(Defined(regcode)) aPerson.regcode=*regcode;
		if(Defined(last_sync)) aPerson.last_sync=*last_sync;
		if(Defined(oid)) aPerson.oid=*oid;
		if(Defined(registered_at)) aPerson.registered_at=*registered_at;
		aPerson.Update(rdb_);
		AddMessage(Message()<<Message("Client").What()<<" "<<aPerson.id<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONPerson::Delete(const Int id) {
	Person aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Client").What()<<". ").What());
	//check Identifier reference to Person reference 
	error |= !__CheckIDNotExists(Identifier()->personid, id, "Identifier", e, rdb_);
	//check PersonOrder reference to Person reference 
	error |= !__CheckIDNotExists(PersonOrder()->personid, id, "Order", e, rdb_);
	//check Transaction reference to Person reference 
	error |= !__CheckIDNotExists(Transaction()->personid, id, "Payment", e, rdb_);
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.id=id;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Client").What()<<" "<<id<<" deleted. ");
	return Value();
}

const Value BALOONPerson::Get(const Int id) {
	Value res;
	Person aPerson;
	aPerson.id=id;
	aPerson.Select(rdb_);
	res["id"]=aPerson.id;
	res["userTokenID"]=aPerson.userTokenID;
	res["last_name"]=aPerson.last_name;
	res["first_name"]=aPerson.first_name;
	res["middle_name"]=aPerson.middle_name;
	res["birthdate"]=aPerson.birthdate;
	res["gender"]=aPerson.gender;
	res["phone"]=aPerson.phone;
	res["email"]=aPerson.email;
	res["regcode"]=aPerson.regcode;
	res["last_sync"]=aPerson.last_sync;
	res["oid"]=aPerson.oid;
	res["registered_at"]=aPerson.registered_at;
	return res;
}

const Value BALOONPerson::PersonListGet(const optional<Int64> userTokenID) {
	Data::DataList lr;
	lr.AddColumn("id", Data::INTEGER);
	lr.AddColumn("last_name", Data::STRING);
	lr.AddColumn("first_name", Data::STRING);
	lr.AddColumn("middle_name", Data::STRING);
	lr.AddColumn("birthdate", Data::DATETIME);
	lr.AddColumn("gender", Data::STRING, GENDERValues());
	lr.AddColumn("phone", Data::STRING);
	lr.AddColumn("email", Data::STRING);
	lr.AddColumn("registered_at", Data::DATETIME);
	Person aPerson;
	lr.Bind(aPerson.id, "id");
	lr.Bind(aPerson.last_name, "last_name");
	lr.Bind(aPerson.first_name, "first_name");
	lr.Bind(aPerson.middle_name, "middle_name");
	lr.Bind(aPerson.birthdate, "birthdate");
	lr.Bind(aPerson.gender, "gender");
	lr.Bind(aPerson.phone, "phone");
	lr.Bind(aPerson.email, "email");
	lr.Bind(aPerson.registered_at, "registered_at");
	Selector sel;
	sel << aPerson->id << aPerson->last_name << aPerson->first_name << aPerson->middle_name << aPerson->birthdate << aPerson->gender << aPerson->phone << aPerson->email << aPerson->registered_at;
	sel.Where( (Defined(userTokenID) ? aPerson->userTokenID==*userTokenID : Expression()));
	DataSet data=sel.Execute(rdb_);
	while(data.Fetch()) lr.AddRow();
	Value res=lr;
	return res;
}

void BALOONSpdServiceRule::initialize(ParamsMapRef params) {
	RegisterMethod("SpdServiceRuleMenuListGet", (&BALOONSpdServiceRule::__SpdServiceRuleMenuListGet));
	RegisterMethod("SpdServiceRuleEnabledListGet", (&BALOONSpdServiceRule::__SpdServiceRuleEnabledListGet));
	RegisterMethod("SpdServiceRuleListGet_API", (&BALOONSpdServiceRule::__SpdServiceRuleListGet_API));
	RegisterMethod("SyncDictionary", (&BALOONSpdServiceRule::__SyncDictionary));
	RegisterMethod("Add", (&BALOONSpdServiceRule::__Add));
	RegisterMethod("Get", (&BALOONSpdServiceRule::__Get));
	RegisterMethod("Update", (&BALOONSpdServiceRule::__Update));
	RegisterMethod("Delete", (&BALOONSpdServiceRule::__Delete));
	RegisterMethod("SpdServiceRuleListGet", (&BALOONSpdServiceRule::__SpdServiceRuleListGet));
	InitDatabase(params);
}

 const Value BALOONSpdServiceRule::__SpdServiceRuleMenuListGet(const Param& param) {
	return SpdServiceRuleMenuListGet();
}

 const Value BALOONSpdServiceRule::__SpdServiceRuleEnabledListGet(const Param& param) {
	return SpdServiceRuleEnabledListGet();
}

 const Value BALOONSpdServiceRule::__SpdServiceRuleListGet_API(const Param& param) {
	return SpdServiceRuleListGet_API(param["identifier_rulename"]);
}

 const Value BALOONSpdServiceRule::__SyncDictionary(const Param& param) {
	return SyncDictionary();
}

// Wrappers for automatic methods
const Value BALOONSpdServiceRule::__Add(const Param& param) {
	return Add(param["name"], param["enabled"], param["extid"], param["extcode"], param["comment"], param["package_cost"]);
}

const Value BALOONSpdServiceRule::__Update(const Param& param) {
	return Update(param["name"], param["enabled"], param["extid"], param["extcode"], param["comment"], param["package_cost"]);
}

const Value BALOONSpdServiceRule::__Delete(const Param& param) {
	return Delete(param["name"]);
}

const Value BALOONSpdServiceRule::__Get(const Param& param) {
	return Get(param["name"]);
}

const Value BALOONSpdServiceRule::__SpdServiceRuleListGet(const Param& param) {
	return SpdServiceRuleListGet();
}

// Implementation of automatic methods
const Value BALOONSpdServiceRule::Add(const Str name, const Str enabled, const Int extid, const Int extcode, const Str comment, const Decimal package_cost) {
	Exception e((Message("Cannot add ") << Message("Правило оформления").What() << ". ").What());
	try {
		SpdServiceRule aSpdServiceRule;
		aSpdServiceRule.name=name;
		aSpdServiceRule.enabled=enabled;
		aSpdServiceRule.extid=extid;
		aSpdServiceRule.extcode=extcode;
		aSpdServiceRule.comment=comment;
		aSpdServiceRule.package_cost=package_cost;
	aSpdServiceRule.Insert(rdb_);
		Value res;
		AddMessage(Message()<<Message("Правило оформления").What()<<" "<<aSpdServiceRule.name<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONSpdServiceRule::Update(const Str name, const optional<Str> enabled, const optional<Int> extid, const optional<Int> extcode, const optional<Str> comment, const optional<Decimal> package_cost) {
	Exception e((Message("Cannot update ") << Message("Правило оформления").What() << ". ").What());
	try {
		SpdServiceRule aSpdServiceRule;
		aSpdServiceRule.name=name;
		aSpdServiceRule.Select(rdb_);
		if(Defined(enabled)) aSpdServiceRule.enabled=*enabled;
		if(Defined(extid)) aSpdServiceRule.extid=*extid;
		if(Defined(extcode)) aSpdServiceRule.extcode=*extcode;
		if(Defined(comment)) aSpdServiceRule.comment=*comment;
		if(Defined(package_cost)) aSpdServiceRule.package_cost=*package_cost;
		aSpdServiceRule.Update(rdb_);
		AddMessage(Message()<<Message("Правило оформления").What()<<" "<<aSpdServiceRule.name<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONSpdServiceRule::Delete(const Str name) {
	SpdServiceRule aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Правило оформления").What()<<". ").What());
	//check PersonIdentifierOrderline reference to SpdServiceRule reference 
	error |= !__CheckIDNotExists(PersonIdentifierOrderline()->service_rulename, name, "Orderline", e, rdb_);
	//check PackageAgentOrder reference to SpdServiceRule reference 
	error |= !__CheckIDNotExists(PackageAgentOrder()->service_rulename, name, "Package Order", e, rdb_);
	//check PackageOrder reference to SpdServiceRule reference 
	error |= !__CheckIDNotExists(PackageOrder()->service_rulename, name, "Package Order", e, rdb_);
	//check WeekTariff reference to SpdServiceRule reference 
	error |= !__CheckIDNotExists(WeekTariff()->service_rulename, name, "Week Tariff", e, rdb_);
	//check SkipassConfiguration reference to SpdServiceRule reference 
	error |= !__CheckIDNotExists(SkipassConfiguration()->service_rulename, name, "Skipass Configuration", e, rdb_);
	//check PartnerOrderline reference to SpdServiceRule reference 
	error |= !__CheckIDNotExists(PartnerOrderline()->service_rulename, name, "Orderline", e, rdb_);
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.name=name;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Правило оформления").What()<<" "<<name<<" deleted. ");
	return Value();
}

const Value BALOONSpdServiceRule::Get(const Str name) {
	Value res;
	SpdServiceRule aSpdServiceRule;
	aSpdServiceRule.name=name;
	aSpdServiceRule.Select(rdb_);
	res["name"]=aSpdServiceRule.name;
	res["enabled"]=aSpdServiceRule.enabled;
	res["extid"]=aSpdServiceRule.extid;
	res["extcode"]=aSpdServiceRule.extcode;
	res["comment"]=aSpdServiceRule.comment;
	res["package_cost"]=aSpdServiceRule.package_cost;
	return res;
}

const Value BALOONSpdServiceRule::SpdServiceRuleListGet() {
	Data::DataList lr;
	lr.AddColumn("name", Data::STRING);
	lr.AddColumn("enabled", Data::STRING, ENABLED_STATUSValues());
	lr.AddColumn("extid", Data::INTEGER);
	lr.AddColumn("extcode", Data::INTEGER);
	lr.AddColumn("comment", Data::STRING);
	lr.AddColumn("package_cost", Data::DECIMAL);
	SpdServiceRule aSpdServiceRule;
	lr.Bind(aSpdServiceRule.name, "name");
	lr.Bind(aSpdServiceRule.enabled, "enabled");
	lr.Bind(aSpdServiceRule.extid, "extid");
	lr.Bind(aSpdServiceRule.extcode, "extcode");
	lr.Bind(aSpdServiceRule.comment, "comment");
	lr.Bind(aSpdServiceRule.package_cost, "package_cost");
	Selector sel;
	sel << aSpdServiceRule->name << aSpdServiceRule->enabled << aSpdServiceRule->extid << aSpdServiceRule->extcode << aSpdServiceRule->comment << aSpdServiceRule->package_cost;
	sel.Where();
	DataSet data=sel.Execute(rdb_);
	while(data.Fetch()) lr.AddRow();
	Value res=lr;
	return res;
}

void BALOONPushNotification::initialize(ParamsMapRef params) {
	RegisterMethod("Delete", (&BALOONPushNotification::__Delete));
	RegisterMethod("DeviceRegisterApple_FE", (&BALOONPushNotification::__DeviceRegisterApple_FE));
	RegisterMethod("DeviceRegisterAndroid_FE", (&BALOONPushNotification::__DeviceRegisterAndroid_FE));
	RegisterMethod("DeviceRegisterWindows_FE", (&BALOONPushNotification::__DeviceRegisterWindows_FE));
	RegisterMethod("Add", (&BALOONPushNotification::__Add));
	RegisterMethod("Get", (&BALOONPushNotification::__Get));
	RegisterMethod("Update", (&BALOONPushNotification::__Update));
	RegisterMethod("PushNotificationListGet", (&BALOONPushNotification::__PushNotificationListGet));
	InitDatabase(params);
}

 const Value BALOONPushNotification::__Delete(const Param& param) {
	return Delete(param["id"]);
}

 const Value BALOONPushNotification::__DeviceRegisterApple_FE(const Param& param) {
	return DeviceRegisterApple_FE(param["token"], param["first_name"], param["last_name"], param["middle_name"], param["description"]);
}

 const Value BALOONPushNotification::__DeviceRegisterAndroid_FE(const Param& param) {
	return DeviceRegisterAndroid_FE(param["token"], param["first_name"], param["last_name"], param["middle_name"], param["description"]);
}

 const Value BALOONPushNotification::__DeviceRegisterWindows_FE(const Param& param) {
	return DeviceRegisterWindows_FE(param["token"], param["first_name"], param["last_name"], param["middle_name"], param["description"]);
}

// Wrappers for automatic methods
const Value BALOONPushNotification::__Add(const Param& param) {
	return Add(param["token"], param["token_type"], param["first_name"], param["last_name"], param["middle_name"], param["description"], param["enabled"], param["valid_from"], param["last_register"]);
}

const Value BALOONPushNotification::__Update(const Param& param) {
	return Update(param["id"], param["token"], param["token_type"], param["first_name"], param["last_name"], param["middle_name"], param["description"], param["enabled"], param["valid_from"], param["last_register"]);
}

const Value BALOONPushNotification::__Get(const Param& param) {
	return Get(param["id"]);
}

const Value BALOONPushNotification::__PushNotificationListGet(const Param& param) {
	return PushNotificationListGet();
}

// Implementation of automatic methods
const Value BALOONPushNotification::Add(const Str token, const Int token_type, const Str first_name, const Str last_name, const Str middle_name, const Str description, const Str enabled, const Time valid_from, const Time last_register) {
	Exception e((Message("Cannot add ") << Message("Регистрации").What() << ". ").What());
	try {
		PushNotification aPushNotification;
		aPushNotification.token=token;
		aPushNotification.token_type=token_type;
		aPushNotification.first_name=first_name;
		aPushNotification.last_name=last_name;
		aPushNotification.middle_name=middle_name;
		aPushNotification.description=description;
		aPushNotification.enabled=enabled;
		aPushNotification.valid_from=valid_from;
		aPushNotification.last_register=last_register;
		Int sk=aPushNotification.Insert(rdb_);
		Value res;
		res["id"]=sk;
		AddMessage(Message()<<Message("Регистрации").What()<<" "<<sk<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONPushNotification::Update(const Int id, const optional<Str> token, const optional<Int> token_type, const optional<Str> first_name, const optional<Str> last_name, const optional<Str> middle_name, const optional<Str> description, const optional<Str> enabled, const optional<Time> valid_from, const optional<Time> last_register) {
	Exception e((Message("Cannot update ") << Message("Регистрации").What() << ". ").What());
	try {
		PushNotification aPushNotification;
		aPushNotification.id=id;
		aPushNotification.Select(rdb_);
		if(Defined(token)) aPushNotification.token=*token;
		if(Defined(token_type)) aPushNotification.token_type=*token_type;
		if(Defined(first_name)) aPushNotification.first_name=*first_name;
		if(Defined(last_name)) aPushNotification.last_name=*last_name;
		if(Defined(middle_name)) aPushNotification.middle_name=*middle_name;
		if(Defined(description)) aPushNotification.description=*description;
		if(Defined(enabled)) aPushNotification.enabled=*enabled;
		if(Defined(valid_from)) aPushNotification.valid_from=*valid_from;
		if(Defined(last_register)) aPushNotification.last_register=*last_register;
		aPushNotification.Update(rdb_);
		AddMessage(Message()<<Message("Регистрации").What()<<" "<<aPushNotification.id<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

#if 0 //BALOONPushNotification Delete
const Value BALOONPushNotification::Delete(const Int id) {
	PushNotification aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Регистрации").What()<<". ").What());
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.id=id;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Регистрации").What()<<" "<<id<<" deleted. ");
	return Value();
}

#endif //BALOONPushNotification Delete

const Value BALOONPushNotification::Get(const Int id) {
	Value res;
	PushNotification aPushNotification;
	aPushNotification.id=id;
	aPushNotification.Select(rdb_);
	res["id"]=aPushNotification.id;
	res["token"]=aPushNotification.token;
	res["token_type"]=aPushNotification.token_type;
	res["first_name"]=aPushNotification.first_name;
	res["last_name"]=aPushNotification.last_name;
	res["middle_name"]=aPushNotification.middle_name;
	res["description"]=aPushNotification.description;
	res["enabled"]=aPushNotification.enabled;
	res["valid_from"]=aPushNotification.valid_from;
	res["last_register"]=aPushNotification.last_register;
	return res;
}

const Value BALOONPushNotification::PushNotificationListGet() {
	Data::DataList lr;
	lr.AddColumn("id", Data::INTEGER);
	lr.AddColumn("token", Data::STRING);
	lr.AddColumn("token_type", Data::INTEGER);
	lr.AddColumn("first_name", Data::STRING);
	lr.AddColumn("last_name", Data::STRING);
	lr.AddColumn("middle_name", Data::STRING);
	lr.AddColumn("description", Data::STRING);
	lr.AddColumn("enabled", Data::STRING, ENABLED_STATUSValues());
	lr.AddColumn("valid_from", Data::DATETIME);
	lr.AddColumn("last_register", Data::DATETIME);
	PushNotification aPushNotification;
	lr.Bind(aPushNotification.id, "id");
	lr.Bind(aPushNotification.token, "token");
	lr.Bind(aPushNotification.token_type, "token_type");
	lr.Bind(aPushNotification.first_name, "first_name");
	lr.Bind(aPushNotification.last_name, "last_name");
	lr.Bind(aPushNotification.middle_name, "middle_name");
	lr.Bind(aPushNotification.description, "description");
	lr.Bind(aPushNotification.enabled, "enabled");
	lr.Bind(aPushNotification.valid_from, "valid_from");
	lr.Bind(aPushNotification.last_register, "last_register");
	Selector sel;
	sel << aPushNotification->id << aPushNotification->token << aPushNotification->token_type << aPushNotification->first_name << aPushNotification->last_name << aPushNotification->middle_name << aPushNotification->description << aPushNotification->enabled << aPushNotification->valid_from << aPushNotification->last_register;
	sel.Where();
	DataSet data=sel.Execute(rdb_);
	while(data.Fetch()) lr.AddRow();
	Value res=lr;
	return res;
}

void BALOONPushNotificationMessages::initialize(ParamsMapRef params) {
	RegisterMethod("SendAllNotifications", (&BALOONPushNotificationMessages::__SendAllNotifications));
	RegisterMethod("SendNotifications", (&BALOONPushNotificationMessages::__SendNotifications));
	RegisterMethod("PushNotificationMessageAdd_FE", (&BALOONPushNotificationMessages::__PushNotificationMessageAdd_FE));
	RegisterMethod("PushNotificationMessageUpdate_FE", (&BALOONPushNotificationMessages::__PushNotificationMessageUpdate_FE));
	RegisterMethod("Add", (&BALOONPushNotificationMessages::__Add));
	RegisterMethod("Get", (&BALOONPushNotificationMessages::__Get));
	RegisterMethod("Update", (&BALOONPushNotificationMessages::__Update));
	RegisterMethod("Delete", (&BALOONPushNotificationMessages::__Delete));
	RegisterMethod("PushNotificationMessagesListGet", (&BALOONPushNotificationMessages::__PushNotificationMessagesListGet));
	InitDatabase(params);
}

 const Value BALOONPushNotificationMessages::__SendAllNotifications(const Param& param) {
	return SendAllNotifications();
}

 const Value BALOONPushNotificationMessages::__SendNotifications(const Param& param) {
	return SendNotifications(param["id"], param["sent"], param["enabled"]);
}

 const Value BALOONPushNotificationMessages::__PushNotificationMessageAdd_FE(const Param& param) {
	return PushNotificationMessageAdd_FE(param["title"], param["message"], param["enabled"]);
}

 const Value BALOONPushNotificationMessages::__PushNotificationMessageUpdate_FE(const Param& param) {
	return PushNotificationMessageUpdate_FE(param["id"], param["title"], param["message"], param["enabled"]);
}

// Wrappers for automatic methods
const Value BALOONPushNotificationMessages::__Add(const Param& param) {
	return Add(param["title"], param["message"], param["sent"], param["enabled"], param["message_date"]);
}

const Value BALOONPushNotificationMessages::__Update(const Param& param) {
	return Update(param["id"], param["title"], param["message"], param["sent"], param["enabled"], param["message_date"]);
}

const Value BALOONPushNotificationMessages::__Delete(const Param& param) {
	return Delete(param["id"]);
}

const Value BALOONPushNotificationMessages::__Get(const Param& param) {
	return Get(param["id"]);
}

const Value BALOONPushNotificationMessages::__PushNotificationMessagesListGet(const Param& param) {
	return PushNotificationMessagesListGet();
}

// Implementation of automatic methods
const Value BALOONPushNotificationMessages::Add(const Str title, const Str message, const Str sent, const Str enabled, const Time message_date) {
	Exception e((Message("Cannot add ") << Message("Уведомления").What() << ". ").What());
	try {
		PushNotificationMessages aPushNotificationMessages;
		aPushNotificationMessages.title=title;
		aPushNotificationMessages.message=message;
		aPushNotificationMessages.sent=sent;
		aPushNotificationMessages.enabled=enabled;
		aPushNotificationMessages.message_date=message_date;
		Int sk=aPushNotificationMessages.Insert(rdb_);
		Value res;
		res["id"]=sk;
		AddMessage(Message()<<Message("Уведомления").What()<<" "<<sk<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONPushNotificationMessages::Update(const Int id, const optional<Str> title, const optional<Str> message, const optional<Str> sent, const optional<Str> enabled, const optional<Time> message_date) {
	Exception e((Message("Cannot update ") << Message("Уведомления").What() << ". ").What());
	try {
		PushNotificationMessages aPushNotificationMessages;
		aPushNotificationMessages.id=id;
		aPushNotificationMessages.Select(rdb_);
		if(Defined(title)) aPushNotificationMessages.title=*title;
		if(Defined(message)) aPushNotificationMessages.message=*message;
		if(Defined(sent)) aPushNotificationMessages.sent=*sent;
		if(Defined(enabled)) aPushNotificationMessages.enabled=*enabled;
		if(Defined(message_date)) aPushNotificationMessages.message_date=*message_date;
		aPushNotificationMessages.Update(rdb_);
		AddMessage(Message()<<Message("Уведомления").What()<<" "<<aPushNotificationMessages.id<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONPushNotificationMessages::Delete(const Int id) {
	PushNotificationMessages aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Уведомления").What()<<". ").What());
	//check PushNotificationDeliveries reference to PushNotificationMessages reference 
	error |= !__CheckIDNotExists(PushNotificationDeliveries()->messageid, id, "Доставка", e, rdb_);
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.id=id;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Уведомления").What()<<" "<<id<<" deleted. ");
	return Value();
}

const Value BALOONPushNotificationMessages::Get(const Int id) {
	Value res;
	PushNotificationMessages aPushNotificationMessages;
	aPushNotificationMessages.id=id;
	aPushNotificationMessages.Select(rdb_);
	res["id"]=aPushNotificationMessages.id;
	res["title"]=aPushNotificationMessages.title;
	res["message"]=aPushNotificationMessages.message;
	res["sent"]=aPushNotificationMessages.sent;
	res["enabled"]=aPushNotificationMessages.enabled;
	res["message_date"]=aPushNotificationMessages.message_date;
	return res;
}

const Value BALOONPushNotificationMessages::PushNotificationMessagesListGet() {
	Data::DataList lr;
	lr.AddColumn("id", Data::INTEGER);
	lr.AddColumn("title", Data::STRING);
	lr.AddColumn("message", Data::STRING);
	lr.AddColumn("sent", Data::STRING, ENABLED_STATUSValues());
	lr.AddColumn("enabled", Data::STRING, ENABLED_STATUSValues());
	lr.AddColumn("message_date", Data::DATETIME);
	PushNotificationMessages aPushNotificationMessages;
	lr.Bind(aPushNotificationMessages.id, "id");
	lr.Bind(aPushNotificationMessages.title, "title");
	lr.Bind(aPushNotificationMessages.message, "message");
	lr.Bind(aPushNotificationMessages.sent, "sent");
	lr.Bind(aPushNotificationMessages.enabled, "enabled");
	lr.Bind(aPushNotificationMessages.message_date, "message_date");
	Selector sel;
	sel << aPushNotificationMessages->id << aPushNotificationMessages->title << aPushNotificationMessages->message << aPushNotificationMessages->sent << aPushNotificationMessages->enabled << aPushNotificationMessages->message_date;
	sel.Where();
	DataSet data=sel.Execute(rdb_);
	while(data.Fetch()) lr.AddRow();
	Value res=lr;
	return res;
}

void BALOONPushNotificationDeliveries::initialize(ParamsMapRef params) {
	RegisterMethod("ResendNotification", (&BALOONPushNotificationDeliveries::__ResendNotification));
	RegisterMethod("PushNotificationDeliveryAdd_FE", (&BALOONPushNotificationDeliveries::__PushNotificationDeliveryAdd_FE));
	RegisterMethod("Add", (&BALOONPushNotificationDeliveries::__Add));
	RegisterMethod("Get", (&BALOONPushNotificationDeliveries::__Get));
	RegisterMethod("Update", (&BALOONPushNotificationDeliveries::__Update));
	RegisterMethod("Delete", (&BALOONPushNotificationDeliveries::__Delete));
	RegisterMethod("PushNotificationDeliveriesListGet", (&BALOONPushNotificationDeliveries::__PushNotificationDeliveriesListGet));
	InitDatabase(params);
}

 const Value BALOONPushNotificationDeliveries::__ResendNotification(const Param& param) {
	return ResendNotification(param["id"]);
}

 const Value BALOONPushNotificationDeliveries::__PushNotificationDeliveryAdd_FE(const Param& param) {
	return PushNotificationDeliveryAdd_FE(param["messageid"], param["deviceid"], param["sendresult"]);
}

// Wrappers for automatic methods
const Value BALOONPushNotificationDeliveries::__Add(const Param& param) {
	return Add(param["deviceid"], param["messageid"], param["sendresult"], param["delivery_date"]);
}

const Value BALOONPushNotificationDeliveries::__Update(const Param& param) {
	return Update(param["id"], param["deviceid"], param["messageid"], param["sendresult"], param["delivery_date"]);
}

const Value BALOONPushNotificationDeliveries::__Delete(const Param& param) {
	return Delete(param["id"]);
}

const Value BALOONPushNotificationDeliveries::__Get(const Param& param) {
	return Get(param["id"]);
}

const Value BALOONPushNotificationDeliveries::__PushNotificationDeliveriesListGet(const Param& param) {
	return PushNotificationDeliveriesListGet(param["messageid"]);
}

// Implementation of automatic methods
const Value BALOONPushNotificationDeliveries::Add(const Int deviceid, const Int messageid, const Str sendresult, const Time delivery_date) {
	Exception e((Message("Cannot add ") << Message("Доставка").What() << ". ").What());
	bool error=false;
	error |= Defined(messageid)  && !__CheckIDExists(PushNotificationMessages()->id, *messageid, "ID рассылки уведомлений", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		PushNotificationDeliveries aPushNotificationDeliveries;
		aPushNotificationDeliveries.deviceid=deviceid;
		aPushNotificationDeliveries.messageid=messageid;
		aPushNotificationDeliveries.sendresult=sendresult;
		aPushNotificationDeliveries.delivery_date=delivery_date;
		Int sk=aPushNotificationDeliveries.Insert(rdb_);
		Value res;
		res["id"]=sk;
		AddMessage(Message()<<Message("Доставка").What()<<" "<<sk<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONPushNotificationDeliveries::Update(const Int id, const optional<Int> deviceid, const optional<Int> messageid, const optional<Str> sendresult, const optional<Time> delivery_date) {
	Exception e((Message("Cannot update ") << Message("Доставка").What() << ". ").What());
	bool error=false;
	error |= Defined(messageid) && Defined(*messageid)  && !__CheckIDExists(PushNotificationMessages()->id, *messageid, "ID рассылки уведомлений", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		PushNotificationDeliveries aPushNotificationDeliveries;
		aPushNotificationDeliveries.id=id;
		aPushNotificationDeliveries.Select(rdb_);
		if(Defined(deviceid)) aPushNotificationDeliveries.deviceid=*deviceid;
		if(Defined(messageid)) aPushNotificationDeliveries.messageid=*messageid;
		if(Defined(sendresult)) aPushNotificationDeliveries.sendresult=*sendresult;
		if(Defined(delivery_date)) aPushNotificationDeliveries.delivery_date=*delivery_date;
		aPushNotificationDeliveries.Update(rdb_);
		AddMessage(Message()<<Message("Доставка").What()<<" "<<aPushNotificationDeliveries.id<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONPushNotificationDeliveries::Delete(const Int id) {
	PushNotificationDeliveries aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Доставка").What()<<". ").What());
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.id=id;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Доставка").What()<<" "<<id<<" deleted. ");
	return Value();
}

const Value BALOONPushNotificationDeliveries::Get(const Int id) {
	Value res;
	PushNotificationDeliveries aPushNotificationDeliveries;
	aPushNotificationDeliveries.id=id;
	aPushNotificationDeliveries.Select(rdb_);
	res["id"]=aPushNotificationDeliveries.id;
	res["deviceid"]=aPushNotificationDeliveries.deviceid;
	res["messageid"]=aPushNotificationDeliveries.messageid;
	res["sendresult"]=aPushNotificationDeliveries.sendresult;
	res["delivery_date"]=aPushNotificationDeliveries.delivery_date;
	return res;
}

const Value BALOONPushNotificationDeliveries::PushNotificationDeliveriesListGet(const optional<Int> messageid) {
	Data::DataList lr;
	lr.AddColumn("id", Data::INTEGER);
	lr.AddColumn("devicetoken", Data::STRING);
	lr.AddColumn("messagetitle", Data::STRING);
	lr.AddColumn("sendresult", Data::STRING);
	lr.AddColumn("delivery_date", Data::DATETIME);
	PushNotificationDeliveries aPushNotificationDeliveries;
	PushNotification adevice;
	PushNotificationMessages amessage;
	lr.Bind(aPushNotificationDeliveries.id, "id");
	lr.Bind(adevice.token, "devicetoken");
	lr.Bind(amessage.title, "messagetitle");
	lr.Bind(aPushNotificationDeliveries.sendresult, "sendresult");
	lr.Bind(aPushNotificationDeliveries.delivery_date, "delivery_date");
	Selector sel;
	sel << aPushNotificationDeliveries->id << adevice->token << amessage->title << aPushNotificationDeliveries->sendresult << aPushNotificationDeliveries->delivery_date;
	sel.Join(adevice).On(aPushNotificationDeliveries->deviceid==adevice->id).Join(amessage).On(aPushNotificationDeliveries->messageid==amessage->id);
	sel.Where( (Defined(messageid) ? aPushNotificationDeliveries->messageid==*messageid : Expression()));
	DataSet data=sel.Execute(rdb_);
	while(data.Fetch()) lr.AddRow();
	Value res=lr;
	return res;
}

void BALOONIdentifierRule::initialize(ParamsMapRef params) {
	RegisterMethod("IdentifierRuleMenuListGet", (&BALOONIdentifierRule::__IdentifierRuleMenuListGet));
	RegisterMethod("IdentifierRuleEnabledListGet", (&BALOONIdentifierRule::__IdentifierRuleEnabledListGet));
	RegisterMethod("IdentifierRuleListGet_API", (&BALOONIdentifierRule::__IdentifierRuleListGet_API));
	RegisterMethod("SyncDictionary", (&BALOONIdentifierRule::__SyncDictionary));
	RegisterMethod("Add", (&BALOONIdentifierRule::__Add));
	RegisterMethod("Get", (&BALOONIdentifierRule::__Get));
	RegisterMethod("Update", (&BALOONIdentifierRule::__Update));
	RegisterMethod("Delete", (&BALOONIdentifierRule::__Delete));
	RegisterMethod("IdentifierRuleListGet", (&BALOONIdentifierRule::__IdentifierRuleListGet));
	InitDatabase(params);
}

 const Value BALOONIdentifierRule::__IdentifierRuleMenuListGet(const Param& param) {
	return IdentifierRuleMenuListGet();
}

 const Value BALOONIdentifierRule::__IdentifierRuleEnabledListGet(const Param& param) {
	return IdentifierRuleEnabledListGet();
}

 const Value BALOONIdentifierRule::__IdentifierRuleListGet_API(const Param& param) {
	return IdentifierRuleListGet_API();
}

 const Value BALOONIdentifierRule::__SyncDictionary(const Param& param) {
	return SyncDictionary();
}

// Wrappers for automatic methods
const Value BALOONIdentifierRule::__Add(const Param& param) {
	return Add(param["name"], param["enabled"], param["replenish_enabled"], param["extid"], param["extcode"], param["comment"]);
}

const Value BALOONIdentifierRule::__Update(const Param& param) {
	return Update(param["name"], param["enabled"], param["replenish_enabled"], param["extid"], param["extcode"], param["comment"]);
}

const Value BALOONIdentifierRule::__Delete(const Param& param) {
	return Delete(param["name"]);
}

const Value BALOONIdentifierRule::__Get(const Param& param) {
	return Get(param["name"]);
}

const Value BALOONIdentifierRule::__IdentifierRuleListGet(const Param& param) {
	return IdentifierRuleListGet();
}

// Implementation of automatic methods
const Value BALOONIdentifierRule::Add(const Str name, const Str enabled, const Str replenish_enabled, const Int extid, const Int extcode, const Str comment) {
	Exception e((Message("Cannot add ") << Message("Правило пользования").What() << ". ").What());
	try {
		IdentifierRule aIdentifierRule;
		aIdentifierRule.name=name;
		aIdentifierRule.enabled=enabled;
		aIdentifierRule.replenish_enabled=replenish_enabled;
		aIdentifierRule.extid=extid;
		aIdentifierRule.extcode=extcode;
		aIdentifierRule.comment=comment;
	aIdentifierRule.Insert(rdb_);
		Value res;
		AddMessage(Message()<<Message("Правило пользования").What()<<" "<<aIdentifierRule.name<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONIdentifierRule::Update(const Str name, const optional<Str> enabled, const optional<Str> replenish_enabled, const optional<Int> extid, const optional<Int> extcode, const optional<Str> comment) {
	Exception e((Message("Cannot update ") << Message("Правило пользования").What() << ". ").What());
	try {
		IdentifierRule aIdentifierRule;
		aIdentifierRule.name=name;
		aIdentifierRule.Select(rdb_);
		if(Defined(enabled)) aIdentifierRule.enabled=*enabled;
		if(Defined(replenish_enabled)) aIdentifierRule.replenish_enabled=*replenish_enabled;
		if(Defined(extid)) aIdentifierRule.extid=*extid;
		if(Defined(extcode)) aIdentifierRule.extcode=*extcode;
		if(Defined(comment)) aIdentifierRule.comment=*comment;
		aIdentifierRule.Update(rdb_);
		AddMessage(Message()<<Message("Правило пользования").What()<<" "<<aIdentifierRule.name<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONIdentifierRule::Delete(const Str name) {
	IdentifierRule aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Правило пользования").What()<<". ").What());
	//check Identifier reference to IdentifierRule reference 
	error |= !__CheckIDNotExists(Identifier()->permanent_rulename, name, "Identifier", e, rdb_);
	//check IdentifierAgentOrder reference to IdentifierRule reference 
	error |= !__CheckIDNotExists(IdentifierAgentOrder()->permanent_rulename, name, "Identifier Order", e, rdb_);
	//check WeekTariff reference to IdentifierRule reference 
	error |= !__CheckIDNotExists(WeekTariff()->identifier_rulename, name, "Week Tariff", e, rdb_);
	//check SkipassConfiguration reference to IdentifierRule reference 
	error |= !__CheckIDNotExists(SkipassConfiguration()->identifier_rulename, name, "Skipass Configuration", e, rdb_);
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.name=name;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Правило пользования").What()<<" "<<name<<" deleted. ");
	return Value();
}

const Value BALOONIdentifierRule::Get(const Str name) {
	Value res;
	IdentifierRule aIdentifierRule;
	aIdentifierRule.name=name;
	aIdentifierRule.Select(rdb_);
	res["name"]=aIdentifierRule.name;
	res["enabled"]=aIdentifierRule.enabled;
	res["replenish_enabled"]=aIdentifierRule.replenish_enabled;
	res["extid"]=aIdentifierRule.extid;
	res["extcode"]=aIdentifierRule.extcode;
	res["comment"]=aIdentifierRule.comment;
	return res;
}

const Value BALOONIdentifierRule::IdentifierRuleListGet() {
	Data::DataList lr;
	lr.AddColumn("name", Data::STRING);
	lr.AddColumn("enabled", Data::STRING, ENABLED_STATUSValues());
	lr.AddColumn("replenish_enabled", Data::STRING, ENABLED_STATUSValues());
	lr.AddColumn("extid", Data::INTEGER);
	lr.AddColumn("extcode", Data::INTEGER);
	lr.AddColumn("comment", Data::STRING);
	IdentifierRule aIdentifierRule;
	lr.Bind(aIdentifierRule.name, "name");
	lr.Bind(aIdentifierRule.enabled, "enabled");
	lr.Bind(aIdentifierRule.replenish_enabled, "replenish_enabled");
	lr.Bind(aIdentifierRule.extid, "extid");
	lr.Bind(aIdentifierRule.extcode, "extcode");
	lr.Bind(aIdentifierRule.comment, "comment");
	Selector sel;
	sel << aIdentifierRule->name << aIdentifierRule->enabled << aIdentifierRule->replenish_enabled << aIdentifierRule->extid << aIdentifierRule->extcode << aIdentifierRule->comment;
	sel.Where();
	DataSet data=sel.Execute(rdb_);
	while(data.Fetch()) lr.AddRow();
	Value res=lr;
	return res;
}

void BALOONIdentifierServiceRule::initialize(ParamsMapRef params) {
	RegisterMethod("IdentifierServiceRuleListGet_API", (&BALOONIdentifierServiceRule::__IdentifierServiceRuleListGet_API));
	RegisterMethod("Delete", (&BALOONIdentifierServiceRule::__Delete));
	RegisterMethod("Add", (&BALOONIdentifierServiceRule::__Add));
	RegisterMethod("Get", (&BALOONIdentifierServiceRule::__Get));
	RegisterMethod("Update", (&BALOONIdentifierServiceRule::__Update));
	RegisterMethod("IdentifierServiceRuleListGet", (&BALOONIdentifierServiceRule::__IdentifierServiceRuleListGet));
	InitDatabase(params);
}

 const Value BALOONIdentifierServiceRule::__IdentifierServiceRuleListGet_API(const Param& param) {
	return IdentifierServiceRuleListGet_API();
}

 const Value BALOONIdentifierServiceRule::__Delete(const Param& param) {
	return Delete(param["identifier_rulename"], param["service_rulename"]);
}

// Wrappers for automatic methods
const Value BALOONIdentifierServiceRule::__Add(const Param& param) {
	return Add(param["identifier_rulename"], param["service_rulename"], param["name"], param["enabled"], param["weekday_cost"], param["weekend_cost"]);
}

const Value BALOONIdentifierServiceRule::__Update(const Param& param) {
	return Update(param["identifier_rulename"], param["service_rulename"], param["name"], param["enabled"], param["weekday_cost"], param["weekend_cost"]);
}

const Value BALOONIdentifierServiceRule::__Get(const Param& param) {
	return Get(param["identifier_rulename"], param["service_rulename"]);
}

const Value BALOONIdentifierServiceRule::__IdentifierServiceRuleListGet(const Param& param) {
	return IdentifierServiceRuleListGet(param["identifier_rulename"], param["service_rulename"]);
}

// Implementation of automatic methods
const Value BALOONIdentifierServiceRule::Add(const Str identifier_rulename, const Str service_rulename, const Str name, const Str enabled, const Decimal weekday_cost, const Decimal weekend_cost) {
	Exception e((Message("Cannot add ") << Message("Service Rule Accessibility Settings").What() << ". ").What());
	bool error=false;
	error |= Defined(identifier_rulename)  && !__CheckIDExists(IdentifierRule()->name, *identifier_rulename, "Правило пользования", e, rdb_);
	error |= Defined(service_rulename)  && !__CheckIDExists(SpdServiceRule()->name, *service_rulename, "Правило оформления", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		IdentifierServiceRule aIdentifierServiceRule;
		aIdentifierServiceRule.identifier_rulename=identifier_rulename;
		aIdentifierServiceRule.service_rulename=service_rulename;
		aIdentifierServiceRule.name=name;
		aIdentifierServiceRule.enabled=enabled;
		aIdentifierServiceRule.weekday_cost=weekday_cost;
		aIdentifierServiceRule.weekend_cost=weekend_cost;
	aIdentifierServiceRule.Insert(rdb_);
		Value res;
		AddMessage(Message()<<Message("Service Rule Accessibility Settings").What()<<" "<<aIdentifierServiceRule.identifier_rulename<<" "<<aIdentifierServiceRule.service_rulename<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONIdentifierServiceRule::Update(const Str identifier_rulename, const Str service_rulename, const optional<Str> name, const optional<Str> enabled, const optional<Decimal> weekday_cost, const optional<Decimal> weekend_cost) {
	Exception e((Message("Cannot update ") << Message("Service Rule Accessibility Settings").What() << ". ").What());
	bool error=false;
	error |= Defined(identifier_rulename)  && !__CheckIDExists(IdentifierRule()->name, *identifier_rulename, "Правило пользования", e, rdb_);
	error |= Defined(service_rulename)  && !__CheckIDExists(SpdServiceRule()->name, *service_rulename, "Правило оформления", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		IdentifierServiceRule aIdentifierServiceRule;
		aIdentifierServiceRule.identifier_rulename=identifier_rulename;
		aIdentifierServiceRule.service_rulename=service_rulename;
		aIdentifierServiceRule.Select(rdb_);
		if(Defined(name)) aIdentifierServiceRule.name=*name;
		if(Defined(enabled)) aIdentifierServiceRule.enabled=*enabled;
		if(Defined(weekday_cost)) aIdentifierServiceRule.weekday_cost=*weekday_cost;
		if(Defined(weekend_cost)) aIdentifierServiceRule.weekend_cost=*weekend_cost;
		aIdentifierServiceRule.Update(rdb_);
		AddMessage(Message()<<Message("Service Rule Accessibility Settings").What()<<" "<<aIdentifierServiceRule.identifier_rulename<<" "<<aIdentifierServiceRule.service_rulename<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

#if 0 //BALOONIdentifierServiceRule Delete
const Value BALOONIdentifierServiceRule::Delete(const Str identifier_rulename, const Str service_rulename) {
	IdentifierServiceRule aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Service Rule Accessibility Settings").What()<<". ").What());
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.identifier_rulename=identifier_rulename;
	aRecord.service_rulename=service_rulename;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Service Rule Accessibility Settings").What()<<" "<<identifier_rulename<<" "<<service_rulename<<" deleted. ");
	return Value();
}

#endif //BALOONIdentifierServiceRule Delete

const Value BALOONIdentifierServiceRule::Get(const Str identifier_rulename, const Str service_rulename) {
	Value res;
	IdentifierServiceRule aIdentifierServiceRule;
	aIdentifierServiceRule.identifier_rulename=identifier_rulename;
	aIdentifierServiceRule.service_rulename=service_rulename;
	aIdentifierServiceRule.Select(rdb_);
	res["identifier_rulename"]=aIdentifierServiceRule.identifier_rulename;
	res["service_rulename"]=aIdentifierServiceRule.service_rulename;
	res["name"]=aIdentifierServiceRule.name;
	res["enabled"]=aIdentifierServiceRule.enabled;
	res["weekday_cost"]=aIdentifierServiceRule.weekday_cost;
	res["weekend_cost"]=aIdentifierServiceRule.weekend_cost;
	return res;
}

const Value BALOONIdentifierServiceRule::IdentifierServiceRuleListGet(const optional<Str> identifier_rulename, const optional<Str> service_rulename) {
	Data::DataList lr;
	lr.AddColumn("name", Data::STRING);
	lr.AddColumn("identifier_rulename", Data::STRING);
	lr.AddColumn("service_rulename", Data::STRING);
	lr.AddColumn("enabled", Data::STRING, ENABLED_STATUSValues());
	lr.AddColumn("weekday_cost", Data::DECIMAL);
	lr.AddColumn("weekend_cost", Data::DECIMAL);
	IdentifierServiceRule aIdentifierServiceRule;
	IdentifierRule aidentifier_rule;
	SpdServiceRule aservice_rule;
	lr.Bind(aIdentifierServiceRule.name, "name");
	lr.Bind(aidentifier_rule.name, "identifier_rulename");
	lr.Bind(aservice_rule.name, "service_rulename");
	lr.Bind(aIdentifierServiceRule.enabled, "enabled");
	lr.Bind(aIdentifierServiceRule.weekday_cost, "weekday_cost");
	lr.Bind(aIdentifierServiceRule.weekend_cost, "weekend_cost");
	Selector sel;
	sel << aIdentifierServiceRule->name << aidentifier_rule->name << aservice_rule->name << aIdentifierServiceRule->enabled << aIdentifierServiceRule->weekday_cost << aIdentifierServiceRule->weekend_cost;
	sel.Join(aidentifier_rule).On(aIdentifierServiceRule->identifier_rulename==aidentifier_rule->name).Join(aservice_rule).On(aIdentifierServiceRule->service_rulename==aservice_rule->name);
	sel.Where( (Defined(identifier_rulename) ? aIdentifierServiceRule->identifier_rulename==*identifier_rulename : Expression()) &&  (Defined(service_rulename) ? aIdentifierServiceRule->service_rulename==*service_rulename : Expression()));
	DataSet data=sel.Execute(rdb_);
	while(data.Fetch()) lr.AddRow();
	Value res=lr;
	return res;
}

void BALOONIdentifierCategory::initialize(ParamsMapRef params) {
	RegisterMethod("IdentifierCategoryMenuListGet", (&BALOONIdentifierCategory::__IdentifierCategoryMenuListGet));
	RegisterMethod("IdentifierCategoryEnabledListGet", (&BALOONIdentifierCategory::__IdentifierCategoryEnabledListGet));
	RegisterMethod("IdentifierCategoryListGet_API", (&BALOONIdentifierCategory::__IdentifierCategoryListGet_API));
	RegisterMethod("SyncDictionary", (&BALOONIdentifierCategory::__SyncDictionary));
	RegisterMethod("Update", (&BALOONIdentifierCategory::__Update));
	RegisterMethod("Add", (&BALOONIdentifierCategory::__Add));
	RegisterMethod("Get", (&BALOONIdentifierCategory::__Get));
	RegisterMethod("Delete", (&BALOONIdentifierCategory::__Delete));
	RegisterMethod("IdentifierCategoryListGet", (&BALOONIdentifierCategory::__IdentifierCategoryListGet));
	InitDatabase(params);
}

 const Value BALOONIdentifierCategory::__IdentifierCategoryMenuListGet(const Param& param) {
	return IdentifierCategoryMenuListGet();
}

 const Value BALOONIdentifierCategory::__IdentifierCategoryEnabledListGet(const Param& param) {
	return IdentifierCategoryEnabledListGet();
}

 const Value BALOONIdentifierCategory::__IdentifierCategoryListGet_API(const Param& param) {
	return IdentifierCategoryListGet_API();
}

 const Value BALOONIdentifierCategory::__SyncDictionary(const Param& param) {
	return SyncDictionary();
}

 const Value BALOONIdentifierCategory::__Update(const Param& param) {
	return Update(param["name"], param["enabled"], param["comment"], param["is_default"]);
}

// Wrappers for automatic methods
const Value BALOONIdentifierCategory::__Add(const Param& param) {
	return Add(param["name"], param["enabled"], param["extid"], param["extcode"], param["comment"], param["is_default"]);
}

const Value BALOONIdentifierCategory::__Delete(const Param& param) {
	return Delete(param["name"]);
}

const Value BALOONIdentifierCategory::__Get(const Param& param) {
	return Get(param["name"]);
}

const Value BALOONIdentifierCategory::__IdentifierCategoryListGet(const Param& param) {
	return IdentifierCategoryListGet();
}

// Implementation of automatic methods
const Value BALOONIdentifierCategory::Add(const Str name, const Str enabled, const Int extid, const Int extcode, const Str comment, const Str is_default) {
	Exception e((Message("Cannot add ") << Message("Категория счета").What() << ". ").What());
	try {
		IdentifierCategory aIdentifierCategory;
		aIdentifierCategory.name=name;
		aIdentifierCategory.enabled=enabled;
		aIdentifierCategory.extid=extid;
		aIdentifierCategory.extcode=extcode;
		aIdentifierCategory.comment=comment;
		aIdentifierCategory.is_default=is_default;
	aIdentifierCategory.Insert(rdb_);
		Value res;
		AddMessage(Message()<<Message("Категория счета").What()<<" "<<aIdentifierCategory.name<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

#if 0 //BALOONIdentifierCategory Update
const Value BALOONIdentifierCategory::Update(const Str name, const optional<Str> enabled, const optional<Int> extid, const optional<Int> extcode, const optional<Str> comment, const optional<Str> is_default) {
	Exception e((Message("Cannot update ") << Message("Категория счета").What() << ". ").What());
	try {
		IdentifierCategory aIdentifierCategory;
		aIdentifierCategory.name=name;
		aIdentifierCategory.Select(rdb_);
		if(Defined(enabled)) aIdentifierCategory.enabled=*enabled;
		if(Defined(extid)) aIdentifierCategory.extid=*extid;
		if(Defined(extcode)) aIdentifierCategory.extcode=*extcode;
		if(Defined(comment)) aIdentifierCategory.comment=*comment;
		if(Defined(is_default)) aIdentifierCategory.is_default=*is_default;
		aIdentifierCategory.Update(rdb_);
		AddMessage(Message()<<Message("Категория счета").What()<<" "<<aIdentifierCategory.name<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

#endif //BALOONIdentifierCategory Update

const Value BALOONIdentifierCategory::Delete(const Str name) {
	IdentifierCategory aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Категория счета").What()<<". ").What());
	//check Identifier reference to IdentifierCategory reference 
	error |= !__CheckIDNotExists(Identifier()->categoryname, name, "Identifier", e, rdb_);
	//check IdentifierAgentOrder reference to IdentifierCategory reference 
	error |= !__CheckIDNotExists(IdentifierAgentOrder()->categoryname, name, "Identifier Order", e, rdb_);
	//check WeekTariff reference to IdentifierCategory reference 
	error |= !__CheckIDNotExists(WeekTariff()->identifier_categoryname, name, "Week Tariff", e, rdb_);
	//check SkipassConfiguration reference to IdentifierCategory reference 
	error |= !__CheckIDNotExists(SkipassConfiguration()->identifier_categoryname, name, "Skipass Configuration", e, rdb_);
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.name=name;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Категория счета").What()<<" "<<name<<" deleted. ");
	return Value();
}

const Value BALOONIdentifierCategory::Get(const Str name) {
	Value res;
	IdentifierCategory aIdentifierCategory;
	aIdentifierCategory.name=name;
	aIdentifierCategory.Select(rdb_);
	res["name"]=aIdentifierCategory.name;
	res["enabled"]=aIdentifierCategory.enabled;
	res["extid"]=aIdentifierCategory.extid;
	res["extcode"]=aIdentifierCategory.extcode;
	res["comment"]=aIdentifierCategory.comment;
	res["is_default"]=aIdentifierCategory.is_default;
	return res;
}

const Value BALOONIdentifierCategory::IdentifierCategoryListGet() {
	Data::DataList lr;
	lr.AddColumn("name", Data::STRING);
	lr.AddColumn("enabled", Data::STRING, ENABLED_STATUSValues());
	lr.AddColumn("extid", Data::INTEGER);
	lr.AddColumn("extcode", Data::INTEGER);
	lr.AddColumn("comment", Data::STRING);
	lr.AddColumn("is_default", Data::STRING, BOOLValues());
	IdentifierCategory aIdentifierCategory;
	lr.Bind(aIdentifierCategory.name, "name");
	lr.Bind(aIdentifierCategory.enabled, "enabled");
	lr.Bind(aIdentifierCategory.extid, "extid");
	lr.Bind(aIdentifierCategory.extcode, "extcode");
	lr.Bind(aIdentifierCategory.comment, "comment");
	lr.Bind(aIdentifierCategory.is_default, "is_default");
	Selector sel;
	sel << aIdentifierCategory->name << aIdentifierCategory->enabled << aIdentifierCategory->extid << aIdentifierCategory->extcode << aIdentifierCategory->comment << aIdentifierCategory->is_default;
	sel.Where();
	DataSet data=sel.Execute(rdb_);
	while(data.Fetch()) lr.AddRow();
	Value res=lr;
	return res;
}

void BALOONIdentifierRate::initialize(ParamsMapRef params) {
	RegisterMethod("IdentifierRateMenuListGet", (&BALOONIdentifierRate::__IdentifierRateMenuListGet));
	RegisterMethod("IdentifierRateEnabledListGet", (&BALOONIdentifierRate::__IdentifierRateEnabledListGet));
	RegisterMethod("IdentifierRateListGet_API", (&BALOONIdentifierRate::__IdentifierRateListGet_API));
	RegisterMethod("SyncDictionary", (&BALOONIdentifierRate::__SyncDictionary));
	RegisterMethod("Add", (&BALOONIdentifierRate::__Add));
	RegisterMethod("Get", (&BALOONIdentifierRate::__Get));
	RegisterMethod("Update", (&BALOONIdentifierRate::__Update));
	RegisterMethod("Delete", (&BALOONIdentifierRate::__Delete));
	RegisterMethod("IdentifierRateListGet", (&BALOONIdentifierRate::__IdentifierRateListGet));
	InitDatabase(params);
}

 const Value BALOONIdentifierRate::__IdentifierRateMenuListGet(const Param& param) {
	return IdentifierRateMenuListGet();
}

 const Value BALOONIdentifierRate::__IdentifierRateEnabledListGet(const Param& param) {
	return IdentifierRateEnabledListGet();
}

 const Value BALOONIdentifierRate::__IdentifierRateListGet_API(const Param& param) {
	return IdentifierRateListGet_API();
}

 const Value BALOONIdentifierRate::__SyncDictionary(const Param& param) {
	return SyncDictionary();
}

// Wrappers for automatic methods
const Value BALOONIdentifierRate::__Add(const Param& param) {
	return Add(param["name"], param["enabled"], param["extid"], param["extcode"], param["comment"]);
}

const Value BALOONIdentifierRate::__Update(const Param& param) {
	return Update(param["name"], param["enabled"], param["extid"], param["extcode"], param["comment"]);
}

const Value BALOONIdentifierRate::__Delete(const Param& param) {
	return Delete(param["name"]);
}

const Value BALOONIdentifierRate::__Get(const Param& param) {
	return Get(param["name"]);
}

const Value BALOONIdentifierRate::__IdentifierRateListGet(const Param& param) {
	return IdentifierRateListGet();
}

// Implementation of automatic methods
const Value BALOONIdentifierRate::Add(const Str name, const Str enabled, const Int extid, const Int extcode, const Str comment) {
	Exception e((Message("Cannot add ") << Message("Тариф").What() << ". ").What());
	try {
		IdentifierRate aIdentifierRate;
		aIdentifierRate.name=name;
		aIdentifierRate.enabled=enabled;
		aIdentifierRate.extid=extid;
		aIdentifierRate.extcode=extcode;
		aIdentifierRate.comment=comment;
	aIdentifierRate.Insert(rdb_);
		Value res;
		AddMessage(Message()<<Message("Тариф").What()<<" "<<aIdentifierRate.name<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONIdentifierRate::Update(const Str name, const optional<Str> enabled, const optional<Int> extid, const optional<Int> extcode, const optional<Str> comment) {
	Exception e((Message("Cannot update ") << Message("Тариф").What() << ". ").What());
	try {
		IdentifierRate aIdentifierRate;
		aIdentifierRate.name=name;
		aIdentifierRate.Select(rdb_);
		if(Defined(enabled)) aIdentifierRate.enabled=*enabled;
		if(Defined(extid)) aIdentifierRate.extid=*extid;
		if(Defined(extcode)) aIdentifierRate.extcode=*extcode;
		if(Defined(comment)) aIdentifierRate.comment=*comment;
		aIdentifierRate.Update(rdb_);
		AddMessage(Message()<<Message("Тариф").What()<<" "<<aIdentifierRate.name<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONIdentifierRate::Delete(const Str name) {
	IdentifierRate aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Тариф").What()<<". ").What());
	//check Identifier reference to IdentifierRate reference 
	error |= !__CheckIDNotExists(Identifier()->ratename, name, "Identifier", e, rdb_);
	//check IdentifierAgentOrder reference to IdentifierRate reference 
	error |= !__CheckIDNotExists(IdentifierAgentOrder()->ratename, name, "Identifier Order", e, rdb_);
	//check WeekTariff reference to IdentifierRate reference 
	error |= !__CheckIDNotExists(WeekTariff()->ratename, name, "Week Tariff", e, rdb_);
	//check SkipassConfiguration reference to IdentifierRate reference 
	error |= !__CheckIDNotExists(SkipassConfiguration()->ratename, name, "Skipass Configuration", e, rdb_);
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.name=name;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Тариф").What()<<" "<<name<<" deleted. ");
	return Value();
}

const Value BALOONIdentifierRate::Get(const Str name) {
	Value res;
	IdentifierRate aIdentifierRate;
	aIdentifierRate.name=name;
	aIdentifierRate.Select(rdb_);
	res["name"]=aIdentifierRate.name;
	res["enabled"]=aIdentifierRate.enabled;
	res["extid"]=aIdentifierRate.extid;
	res["extcode"]=aIdentifierRate.extcode;
	res["comment"]=aIdentifierRate.comment;
	return res;
}

const Value BALOONIdentifierRate::IdentifierRateListGet() {
	Data::DataList lr;
	lr.AddColumn("name", Data::STRING);
	lr.AddColumn("enabled", Data::STRING, ENABLED_STATUSValues());
	lr.AddColumn("extid", Data::INTEGER);
	lr.AddColumn("extcode", Data::INTEGER);
	lr.AddColumn("comment", Data::STRING);
	IdentifierRate aIdentifierRate;
	lr.Bind(aIdentifierRate.name, "name");
	lr.Bind(aIdentifierRate.enabled, "enabled");
	lr.Bind(aIdentifierRate.extid, "extid");
	lr.Bind(aIdentifierRate.extcode, "extcode");
	lr.Bind(aIdentifierRate.comment, "comment");
	Selector sel;
	sel << aIdentifierRate->name << aIdentifierRate->enabled << aIdentifierRate->extid << aIdentifierRate->extcode << aIdentifierRate->comment;
	sel.Where();
	DataSet data=sel.Execute(rdb_);
	while(data.Fetch()) lr.AddRow();
	Value res=lr;
	return res;
}

void BALOONIdentifier::initialize(ParamsMapRef params) {
	RegisterMethod("OurIdentifierListGet", (&BALOONIdentifier::__OurIdentifierListGet));
	RegisterMethod("OurPushToContour", (&BALOONIdentifier::__OurPushToContour));
	RegisterMethod("Add", (&BALOONIdentifier::__Add));
	RegisterMethod("BindIdentifierToCurrentPerson_FE", (&BALOONIdentifier::__BindIdentifierToCurrentPerson_FE));
	RegisterMethod("CurrentIdentifierListGet_FE", (&BALOONIdentifier::__CurrentIdentifierListGet_FE));
	RegisterMethod("IdentifierListDistributedByAccountsGet_FE", (&BALOONIdentifier::__IdentifierListDistributedByAccountsGet_FE));
	RegisterMethod("BalancedIdentifierListGet_FE", (&BALOONIdentifier::__BalancedIdentifierListGet_FE));
	RegisterMethod("IdentifierNameUpdate_FE", (&BALOONIdentifier::__IdentifierNameUpdate_FE));
	RegisterMethod("SetDateTimeFields", (&BALOONIdentifier::__SetDateTimeFields));
	RegisterMethod("GetDetails_API", (&BALOONIdentifier::__GetDetails_API));
	RegisterMethod("GetBalance_API", (&BALOONIdentifier::__GetBalance_API));
	RegisterMethod("GetBalance_FE", (&BALOONIdentifier::__GetBalance_FE));
	RegisterMethod("PackagesListGet_API", (&BALOONIdentifier::__PackagesListGet_API));
	RegisterMethod("ConvertPrintCode", (&BALOONIdentifier::__ConvertPrintCode));
	RegisterMethod("ShowConvertPrintCode", (&BALOONIdentifier::__ShowConvertPrintCode));
	RegisterMethod("CreateUnpersonalizedIdentifierInSpd", (&BALOONIdentifier::__CreateUnpersonalizedIdentifierInSpd));
	RegisterMethod("MyPayedIdentifierListGet_FE", (&BALOONIdentifier::__MyPayedIdentifierListGet_FE));
	RegisterMethod("DownloadQRCode", (&BALOONIdentifier::__DownloadQRCode));
	RegisterMethod("GenerateQRCode", (&BALOONIdentifier::__GenerateQRCode));
	RegisterMethod("DownloadBarcode", (&BALOONIdentifier::__DownloadBarcode));
	RegisterMethod("GenerateBarcode", (&BALOONIdentifier::__GenerateBarcode));
	RegisterMethod("Cancel_FE", (&BALOONIdentifier::__Cancel_FE));
	RegisterMethod("Enable", (&BALOONIdentifier::__Enable));
	RegisterMethod("Get", (&BALOONIdentifier::__Get));
	RegisterMethod("Delete", (&BALOONIdentifier::__Delete));
	RegisterMethod("IdentifierListGet", (&BALOONIdentifier::__IdentifierListGet));
	InitDatabase(params);
}

 const Value BALOONIdentifier::__OurIdentifierListGet(const Param& param) {
	return OurIdentifierListGet();
}

 const Value BALOONIdentifier::__OurPushToContour(const Param& param) {
	return OurPushToContour(param["id"]);
}

 const Value BALOONIdentifier::__Add(const Param& param) {
	return Add(param["personid"], param["code"], param["valid_from"], param["valid_to"], param["permanent_rulename"], param["categoryname"], param["ratename"]);
}

 const Value BALOONIdentifier::__BindIdentifierToCurrentPerson_FE(const Param& param) {
	return BindIdentifierToCurrentPerson_FE(param["code"]);
}

 const Value BALOONIdentifier::__CurrentIdentifierListGet_FE(const Param& param) {
	return CurrentIdentifierListGet_FE();
}

 const Value BALOONIdentifier::__IdentifierListDistributedByAccountsGet_FE(const Param& param) {
	return IdentifierListDistributedByAccountsGet_FE();
}

 const Value BALOONIdentifier::__BalancedIdentifierListGet_FE(const Param& param) {
	return BalancedIdentifierListGet_FE();
}

 const Value BALOONIdentifier::__IdentifierNameUpdate_FE(const Param& param) {
	return IdentifierNameUpdate_FE(param["code"], param["name"]);
}

 const Value BALOONIdentifier::__SetDateTimeFields(const Param& param) {
	return SetDateTimeFields();
}

 const Value BALOONIdentifier::__GetDetails_API(const Param& param) {
	return GetDetails_API(param["code"]);
}

 const Value BALOONIdentifier::__GetBalance_API(const Param& param) {
	return GetBalance_API(param["code"], param["print_code"]);
}

 const Value BALOONIdentifier::__GetBalance_FE(const Param& param) {
	return GetBalance_FE(param["code"], param["print_code"]);
}

 const Value BALOONIdentifier::__PackagesListGet_API(const Param& param) {
	return PackagesListGet_API(param["code"], param["print_code"]);
}

 const Value BALOONIdentifier::__ConvertPrintCode(const Param& param) {
	return ConvertPrintCode(param["print_code"]);
}

 const Value BALOONIdentifier::__ShowConvertPrintCode(const Param& param) {
	return ShowConvertPrintCode(param["print_code"]);
}

 const Value BALOONIdentifier::__CreateUnpersonalizedIdentifierInSpd(const Param& param) {
	return CreateUnpersonalizedIdentifierInSpd(param["email"], param["code"], param["valid_from"], param["valid_to"], param["permanent_rulename"], param["categoryname"], param["ratename"], param["amount"]);
}

 const Value BALOONIdentifier::__MyPayedIdentifierListGet_FE(const Param& param) {
	return MyPayedIdentifierListGet_FE();
}

 const Value BALOONIdentifier::__DownloadQRCode(const Param& param) {
	return DownloadQRCode(param["code"]);
}

 const Value BALOONIdentifier::__GenerateQRCode(const Param& param) {
	return GenerateQRCode(param["code"]);
}

 const Value BALOONIdentifier::__DownloadBarcode(const Param& param) {
	return DownloadBarcode(param["code"]);
}

 const Value BALOONIdentifier::__GenerateBarcode(const Param& param) {
	return GenerateBarcode(param["code"]);
}

 const Value BALOONIdentifier::__Cancel_FE(const Param& param) {
	return Cancel_FE(param["code"]);
}

 const Value BALOONIdentifier::__Enable(const Param& param) {
	return Enable(param["id"]);
}

// Wrappers for automatic methods
const Value BALOONIdentifier::__Delete(const Param& param) {
	return Delete(param["id"]);
}

const Value BALOONIdentifier::__Get(const Param& param) {
	return Get(param["id"]);
}

const Value BALOONIdentifier::__IdentifierListGet(const Param& param) {
	return IdentifierListGet(param["personid"], param["permanent_rulename"], param["categoryname"], param["ratename"], param["spdgateid"]);
}

// Implementation of automatic methods
const Value BALOONIdentifier::Delete(const Int id) {
	Identifier aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Identifier").What()<<". ").What());
	//check ParkingOrderline reference to Identifier reference 
	error |= !__CheckIDNotExists(ParkingOrderline()->identifierid, id, "Parking Ticket", e, rdb_);
	//check IdentifierInvoice reference to Identifier reference 
	error |= !__CheckIDNotExists(IdentifierInvoice()->identifierid, id, "Invoice", e, rdb_);
	//check PackageOrder reference to Identifier reference 
	error |= !__CheckIDNotExists(PackageOrder()->identifierid, id, "Package Order", e, rdb_);
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.id=id;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Identifier").What()<<" "<<id<<" deleted. ");
	return Value();
}

const Value BALOONIdentifier::Get(const Int id) {
	Value res;
	Identifier aIdentifier;
	aIdentifier.id=id;
	aIdentifier.Select(rdb_);
	res["id"]=aIdentifier.id;
	res["personid"]=aIdentifier.personid;
	res["code"]=aIdentifier.code;
	res["name"]=aIdentifier.name;
	res["groupname"]=aIdentifier.groupname;
	res["valid_from"]=aIdentifier.valid_from;
	res["valid_to"]=aIdentifier.valid_to;
	res["permanent_rulename"]=aIdentifier.permanent_rulename;
	res["categoryname"]=aIdentifier.categoryname;
	res["ratename"]=aIdentifier.ratename;
	res["clientoid"]=aIdentifier.clientoid;
	res["status"]=aIdentifier.status;
	res["comment"]=aIdentifier.comment;
	res["spdgateid"]=aIdentifier.spdgateid;
	return res;
}

const Value BALOONIdentifier::IdentifierListGet(const optional<Int> personid, const optional<Str> permanent_rulename, const optional<Str> categoryname, const optional<Str> ratename, const optional<Int> spdgateid) {
	Data::DataList lr;
	lr.AddColumn("id", Data::INTEGER);
	lr.AddColumn("code", Data::STRING);
	lr.AddColumn("name", Data::STRING);
	lr.AddColumn("valid_from", Data::DATETIME);
	lr.AddColumn("valid_to", Data::DATETIME);
	lr.AddColumn("permanent_rulename", Data::STRING);
	lr.AddColumn("categoryname", Data::STRING);
	lr.AddColumn("ratename", Data::STRING);
	lr.AddColumn("status", Data::STRING, IDENTIFIER_STATUSValues());
	lr.AddColumn("comment", Data::STRING);
	Identifier aIdentifier;
	IdentifierRule apermanent_rule;
	IdentifierCategory acategory;
	IdentifierRate arate;
	lr.Bind(aIdentifier.id, "id");
	lr.Bind(aIdentifier.code, "code");
	lr.Bind(aIdentifier.name, "name");
	lr.Bind(aIdentifier.valid_from, "valid_from");
	lr.Bind(aIdentifier.valid_to, "valid_to");
	lr.Bind(apermanent_rule.name, "permanent_rulename");
	lr.Bind(acategory.name, "categoryname");
	lr.Bind(arate.name, "ratename");
	lr.Bind(aIdentifier.status, "status");
	lr.Bind(aIdentifier.comment, "comment");
	Selector sel;
	sel << aIdentifier->id << aIdentifier->code << aIdentifier->name << aIdentifier->valid_from << aIdentifier->valid_to << apermanent_rule->name << acategory->name << arate->name << aIdentifier->status << aIdentifier->comment;
	sel.Join(apermanent_rule).On(aIdentifier->permanent_rulename==apermanent_rule->name).Join(acategory).On(aIdentifier->categoryname==acategory->name).Join(arate).On(aIdentifier->ratename==arate->name);
	sel.Where( (Defined(personid) ? aIdentifier->personid==*personid : Expression()) &&  (Defined(permanent_rulename) ? aIdentifier->permanent_rulename==*permanent_rulename : Expression()) &&  (Defined(categoryname) ? aIdentifier->categoryname==*categoryname : Expression()) &&  (Defined(ratename) ? aIdentifier->ratename==*ratename : Expression()) &&  (Defined(spdgateid) ? aIdentifier->spdgateid==*spdgateid : Expression()));
	DataSet data=sel.Execute(rdb_);
	while(data.Fetch()) lr.AddRow();
	Value res=lr;
	return res;
}

void BALOONOrderSite::initialize(ParamsMapRef params) {
	RegisterMethod("Add", (&BALOONOrderSite::__Add));
	RegisterMethod("Get", (&BALOONOrderSite::__Get));
	RegisterMethod("Update", (&BALOONOrderSite::__Update));
	RegisterMethod("Delete", (&BALOONOrderSite::__Delete));
	RegisterMethod("OrderSiteListGet", (&BALOONOrderSite::__OrderSiteListGet));
	InitDatabase(params);
}

// Wrappers for automatic methods
const Value BALOONOrderSite::__Add(const Param& param) {
	return Add(param["code"], param["name"], param["link"]);
}

const Value BALOONOrderSite::__Update(const Param& param) {
	return Update(param["code"], param["name"], param["link"]);
}

const Value BALOONOrderSite::__Delete(const Param& param) {
	return Delete(param["code"]);
}

const Value BALOONOrderSite::__Get(const Param& param) {
	return Get(param["code"]);
}

const Value BALOONOrderSite::__OrderSiteListGet(const Param& param) {
	return OrderSiteListGet();
}

// Implementation of automatic methods
const Value BALOONOrderSite::Add(const Str code, const Str name, const Str link) {
	Exception e((Message("Cannot add ") << Message("Order Site").What() << ". ").What());
	try {
		OrderSite aOrderSite;
		aOrderSite.code=code;
		aOrderSite.name=name;
		aOrderSite.link=link;
	aOrderSite.Insert(rdb_);
		Value res;
		AddMessage(Message()<<Message("Order Site").What()<<" "<<aOrderSite.code<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONOrderSite::Update(const Str code, const optional<Str> name, const optional<Str> link) {
	Exception e((Message("Cannot update ") << Message("Order Site").What() << ". ").What());
	try {
		OrderSite aOrderSite;
		aOrderSite.code=code;
		aOrderSite.Select(rdb_);
		if(Defined(name)) aOrderSite.name=*name;
		if(Defined(link)) aOrderSite.link=*link;
		aOrderSite.Update(rdb_);
		AddMessage(Message()<<Message("Order Site").What()<<" "<<aOrderSite.code<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONOrderSite::Delete(const Str code) {
	OrderSite aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Order Site").What()<<". ").What());
	//check PersonOrder reference to OrderSite reference 
	error |= !__CheckIDNotExists(PersonOrder()->sitecode, code, "Order", e, rdb_);
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.code=code;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Order Site").What()<<" "<<code<<" deleted. ");
	return Value();
}

const Value BALOONOrderSite::Get(const Str code) {
	Value res;
	OrderSite aOrderSite;
	aOrderSite.code=code;
	aOrderSite.Select(rdb_);
	res["code"]=aOrderSite.code;
	res["name"]=aOrderSite.name;
	res["link"]=aOrderSite.link;
	return res;
}

const Value BALOONOrderSite::OrderSiteListGet() {
	Data::DataList lr;
	lr.AddColumn("code", Data::STRING);
	lr.AddColumn("name", Data::STRING);
	lr.AddColumn("link", Data::STRING);
	OrderSite aOrderSite;
	lr.Bind(aOrderSite.code, "code");
	lr.Bind(aOrderSite.name, "name");
	lr.Bind(aOrderSite.link, "link");
	Selector sel;
	sel << aOrderSite->code << aOrderSite->name << aOrderSite->link;
	sel.Where();
	DataSet data=sel.Execute(rdb_);
	while(data.Fetch()) lr.AddRow();
	Value res=lr;
	return res;
}

void BALOONPersonOrder::initialize(ParamsMapRef params) {
	RegisterMethod("Place_API", (&BALOONPersonOrder::__Place_API));
	RegisterMethod("PlaceFree", (&BALOONPersonOrder::__PlaceFree));
	RegisterMethod("MyPlace_API", (&BALOONPersonOrder::__MyPlace_API));
	RegisterMethod("Place_FE", (&BALOONPersonOrder::__Place_FE));
	RegisterMethod("PlaceAny_FE", (&BALOONPersonOrder::__PlaceAny_FE));
	RegisterMethod("PlaceAmount_FE", (&BALOONPersonOrder::__PlaceAmount_FE));
	RegisterMethod("Place_WEB", (&BALOONPersonOrder::__Place_WEB));
	RegisterMethod("CheckPromocode_API", (&BALOONPersonOrder::__CheckPromocode_API));
	RegisterMethod("BuySkipass_API", (&BALOONPersonOrder::__BuySkipass_API));
	RegisterMethod("MyBuySkipass_API", (&BALOONPersonOrder::__MyBuySkipass_API));
	RegisterMethod("Add", (&BALOONPersonOrder::__Add));
	RegisterMethod("RequestPaymentStatus", (&BALOONPersonOrder::__RequestPaymentStatus));
	RegisterMethod("GetPreliminaryOrderAmount_FE", (&BALOONPersonOrder::__GetPreliminaryOrderAmount_FE));
	RegisterMethod("FindByIdentifier", (&BALOONPersonOrder::__FindByIdentifier));
	RegisterMethod("MyGetStatus_FE", (&BALOONPersonOrder::__MyGetStatus_FE));
	RegisterMethod("MySyncStatus_FE", (&BALOONPersonOrder::__MySyncStatus_FE));
	RegisterMethod("Download_FE", (&BALOONPersonOrder::__Download_FE));
	RegisterMethod("MyOrderListGet_FE", (&BALOONPersonOrder::__MyOrderListGet_FE));
	RegisterMethod("MyDownload_FE", (&BALOONPersonOrder::__MyDownload_FE));
	RegisterMethod("PromotionSaleListGet", (&BALOONPersonOrder::__PromotionSaleListGet));
	RegisterMethod("ParkingOrderPlace_API", (&BALOONPersonOrder::__ParkingOrderPlace_API));
	RegisterMethod("ParkingIdentifierInfoGet_API", (&BALOONPersonOrder::__ParkingIdentifierInfoGet_API));
	RegisterMethod("Get", (&BALOONPersonOrder::__Get));
	RegisterMethod("Update", (&BALOONPersonOrder::__Update));
	RegisterMethod("Delete", (&BALOONPersonOrder::__Delete));
	RegisterMethod("PersonOrderListGet", (&BALOONPersonOrder::__PersonOrderListGet));
	InitDatabase(params);
}

 const Value BALOONPersonOrder::__Place_API(const Param& param) {
	return Place_API(param["email"], param["last_name"], param["first_name"], param["adate"], param["identifier_service_rulename"], param["qty"]);
}

 const Value BALOONPersonOrder::__PlaceFree(const Param& param) {
	return PlaceFree(param["email"], param["last_name"], param["first_name"], param["adate"], param["week_tariffid"], param["qty"]);
}

 const Value BALOONPersonOrder::__MyPlace_API(const Param& param) {
	return MyPlace_API(param["adate"], param["identifier_service_rulename"], param["qty"]);
}

 const Value BALOONPersonOrder::__Place_FE(const Param& param) {
	return Place_FE(param["email"], param["last_name"], param["first_name"], param["adate"], param["week_tariffid"], param["qty"]);
}

 const Value BALOONPersonOrder::__PlaceAny_FE(const Param& param) {
	return PlaceAny_FE(param["email"], param["adate"], param["orderdesc"]);
}

 const Value BALOONPersonOrder::__PlaceAmount_FE(const Param& param) {
	return PlaceAmount_FE(param["email"], param["adate"], param["timeoffset"], param["orderdesc"]);
}

 const Value BALOONPersonOrder::__Place_WEB(const Param& param) {
	return Place_WEB(param["email"], param["last_name"], param["first_name"], param["adate"], param["week_tariffid"], param["qty"], param["promocode"], param["sitecode"]);
}

 const Value BALOONPersonOrder::__CheckPromocode_API(const Param& param) {
	return CheckPromocode_API(param["promocode"], param["adate"]);
}

 const Value BALOONPersonOrder::__BuySkipass_API(const Param& param) {
	return BuySkipass_API(param["email"], param["last_name"], param["first_name"], param["skipass_configurationid"], param["qty"]);
}

 const Value BALOONPersonOrder::__MyBuySkipass_API(const Param& param) {
	return MyBuySkipass_API(param["skipass_configurationid"], param["qty"]);
}

 const Value BALOONPersonOrder::__Add(const Param& param) {
	return Add(param["personid"], param["adate"], param["identifier_service_rulename"], param["identifier_ratename"], param["qty"]);
}

 const Value BALOONPersonOrder::__RequestPaymentStatus(const Param& param) {
	return RequestPaymentStatus();
}

 const Value BALOONPersonOrder::__GetPreliminaryOrderAmount_FE(const Param& param) {
	return GetPreliminaryOrderAmount_FE(param["adate"], param["identifier_rulename"], param["service_rulename"], param["qty"]);
}

 const Value BALOONPersonOrder::__FindByIdentifier(const Param& param) {
	return FindByIdentifier(param["code"]);
}

 const Value BALOONPersonOrder::__MyGetStatus_FE(const Param& param) {
	return MyGetStatus_FE(param["id"]);
}

 const Value BALOONPersonOrder::__MySyncStatus_FE(const Param& param) {
	return MySyncStatus_FE(param["id"]);
}

 const Value BALOONPersonOrder::__Download_FE(const Param& param) {
	return Download_FE(param["trxid"]);
}

 const Value BALOONPersonOrder::__MyOrderListGet_FE(const Param& param) {
	return MyOrderListGet_FE();
}

 const Value BALOONPersonOrder::__MyDownload_FE(const Param& param) {
	return MyDownload_FE(param["id"]);
}

 const Value BALOONPersonOrder::__PromotionSaleListGet(const Param& param) {
	return PromotionSaleListGet(param["first_day"], param["last_day"]);
}

 const Value BALOONPersonOrder::__ParkingOrderPlace_API(const Param& param) {
	return ParkingOrderPlace_API(param["areaid"], param["email"], param["code"]);
}

 const Value BALOONPersonOrder::__ParkingIdentifierInfoGet_API(const Param& param) {
	return ParkingIdentifierInfoGet_API(param["areaid"], param["code"]);
}

// Wrappers for automatic methods
const Value BALOONPersonOrder::__Update(const Param& param) {
	return Update(param["id"], param["personid"], param["filed_at"], param["amount"], param["status"], param["promocode"], param["final_amount"], param["sitecode"]);
}

const Value BALOONPersonOrder::__Delete(const Param& param) {
	return Delete(param["id"]);
}

const Value BALOONPersonOrder::__Get(const Param& param) {
	return Get(param["id"]);
}

const Value BALOONPersonOrder::__PersonOrderListGet(const Param& param) {
	return PersonOrderListGet(param["personid"], param["sitecode"]);
}

// Implementation of automatic methods
#if 0 //BALOONPersonOrder Add
const Value BALOONPersonOrder::Add(const Int personid, const Time filed_at, const Decimal amount, const Str status, const Str promocode, const Decimal final_amount, const Str sitecode) {
	Exception e((Message("Cannot add ") << Message("Order").What() << ". ").What());
	bool error=false;
	error |= Defined(personid)  && !__CheckIDExists(Person()->id, *personid, "Person", e, rdb_);
	error |= Defined(sitecode)  && !__CheckIDExists(OrderSite()->code, *sitecode, "Site", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		PersonOrder aPersonOrder;
		aPersonOrder.personid=personid;
		aPersonOrder.filed_at=filed_at;
		aPersonOrder.amount=amount;
		aPersonOrder.status=status;
		aPersonOrder.promocode=promocode;
		aPersonOrder.final_amount=final_amount;
		aPersonOrder.sitecode=sitecode;
		Int sk=aPersonOrder.Insert(rdb_);
		Value res;
		res["id"]=sk;
		AddMessage(Message()<<Message("Order").What()<<" "<<sk<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

#endif //BALOONPersonOrder Add

const Value BALOONPersonOrder::Update(const Int id, const optional<Int> personid, const optional<Time> filed_at, const optional<Decimal> amount, const optional<Str> status, const optional<Str> promocode, const optional<Decimal> final_amount, const optional<Str> sitecode) {
	Exception e((Message("Cannot update ") << Message("Order").What() << ". ").What());
	bool error=false;
	error |= Defined(personid) && Defined(*personid)  && !__CheckIDExists(Person()->id, *personid, "Person", e, rdb_);
	error |= Defined(sitecode) && Defined(*sitecode)  && !__CheckIDExists(OrderSite()->code, *sitecode, "Site", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		PersonOrder aPersonOrder;
		aPersonOrder.id=id;
		aPersonOrder.Select(rdb_);
		if(Defined(personid)) aPersonOrder.personid=*personid;
		if(Defined(filed_at)) aPersonOrder.filed_at=*filed_at;
		if(Defined(amount)) aPersonOrder.amount=*amount;
		if(Defined(status)) aPersonOrder.status=*status;
		if(Defined(promocode)) aPersonOrder.promocode=*promocode;
		if(Defined(final_amount)) aPersonOrder.final_amount=*final_amount;
		if(Defined(sitecode)) aPersonOrder.sitecode=*sitecode;
		aPersonOrder.Update(rdb_);
		AddMessage(Message()<<Message("Order").What()<<" "<<aPersonOrder.id<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONPersonOrder::Delete(const Int id) {
	PersonOrder aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Order").What()<<". ").What());
	//check ParkingOrderline reference to PersonOrder reference 
	error |= !__CheckIDNotExists(ParkingOrderline()->orderid, id, "Parking Ticket", e, rdb_);
	//check PersonInvoice reference to PersonOrder reference 
	error |= !__CheckIDNotExists(PersonInvoice()->orderid, id, "Invoice", e, rdb_);
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.id=id;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Order").What()<<" "<<id<<" deleted. ");
	return Value();
}

const Value BALOONPersonOrder::Get(const Int id) {
	Value res;
	PersonOrder aPersonOrder;
	aPersonOrder.id=id;
	aPersonOrder.Select(rdb_);
	res["id"]=aPersonOrder.id;
	res["personid"]=aPersonOrder.personid;
	res["filed_at"]=aPersonOrder.filed_at;
	res["amount"]=aPersonOrder.amount;
	res["status"]=aPersonOrder.status;
	res["promocode"]=aPersonOrder.promocode;
	res["final_amount"]=aPersonOrder.final_amount;
	res["sitecode"]=aPersonOrder.sitecode;
	return res;
}

const Value BALOONPersonOrder::PersonOrderListGet(const optional<Int> personid, const optional<Str> sitecode) {
	Data::DataList lr;
	lr.AddColumn("id", Data::INTEGER);
	lr.AddColumn("personemail", Data::STRING);
	lr.AddColumn("filed_at", Data::DATETIME);
	lr.AddColumn("amount", Data::DECIMAL);
	lr.AddColumn("status", Data::STRING, PERSON_ORDER_STATUSValues());
	lr.AddColumn("sitename", Data::STRING);
	PersonOrder aPersonOrder;
	Person aperson;
	OrderSite asite;
	lr.Bind(aPersonOrder.id, "id");
	lr.Bind(aperson.email, "personemail");
	lr.Bind(aPersonOrder.filed_at, "filed_at");
	lr.Bind(aPersonOrder.amount, "amount");
	lr.Bind(aPersonOrder.status, "status");
	lr.Bind(asite.name, "sitename");
	Selector sel;
	sel << aPersonOrder->id << aperson->email << aPersonOrder->filed_at << aPersonOrder->amount << aPersonOrder->status << asite->name;
	sel.Join(aperson).On(aPersonOrder->personid==aperson->id).LeftJoin(asite).On(aPersonOrder->sitecode==asite->code);
	sel.Where( (Defined(personid) ? aPersonOrder->personid==*personid : Expression()) &&  (Defined(sitecode) ? aPersonOrder->sitecode==*sitecode : Expression()));
	DataSet data=sel.Execute(rdb_);
	while(data.Fetch()) lr.AddRow();
	Value res=lr;
	return res;
}

void BALOONParkingArea::initialize(ParamsMapRef params) {
	RegisterMethod("ParkingAreaListGet_API", (&BALOONParkingArea::__ParkingAreaListGet_API));
	RegisterMethod("Add", (&BALOONParkingArea::__Add));
	RegisterMethod("Get", (&BALOONParkingArea::__Get));
	RegisterMethod("Update", (&BALOONParkingArea::__Update));
	RegisterMethod("Delete", (&BALOONParkingArea::__Delete));
	RegisterMethod("ParkingAreaListGet", (&BALOONParkingArea::__ParkingAreaListGet));
	InitDatabase(params);
}

 const Value BALOONParkingArea::__ParkingAreaListGet_API(const Param& param) {
	return ParkingAreaListGet_API();
}

// Wrappers for automatic methods
const Value BALOONParkingArea::__Add(const Param& param) {
	return Add(param["name"], param["spdgateid"], param["enter_timeout"], param["exit_timeout"]);
}

const Value BALOONParkingArea::__Update(const Param& param) {
	return Update(param["id"], param["name"], param["spdgateid"], param["enter_timeout"], param["exit_timeout"]);
}

const Value BALOONParkingArea::__Delete(const Param& param) {
	return Delete(param["id"]);
}

const Value BALOONParkingArea::__Get(const Param& param) {
	return Get(param["id"]);
}

const Value BALOONParkingArea::__ParkingAreaListGet(const Param& param) {
	return ParkingAreaListGet(param["spdgateid"]);
}

// Implementation of automatic methods
const Value BALOONParkingArea::Add(const Str name, const Int spdgateid, const Int enter_timeout, const Int exit_timeout) {
	Exception e((Message("Cannot add ") << Message("Parking Area").What() << ". ").What());
	bool error=false;
	error |= Defined(spdgateid)  && !__CheckIDExists(SpdGate()->id, *spdgateid, "SPD Gate", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		ParkingArea aParkingArea;
		aParkingArea.name=name;
		aParkingArea.spdgateid=spdgateid;
		aParkingArea.enter_timeout=enter_timeout;
		aParkingArea.exit_timeout=exit_timeout;
		Int sk=aParkingArea.Insert(rdb_);
		Value res;
		res["id"]=sk;
		AddMessage(Message()<<Message("Parking Area").What()<<" "<<sk<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONParkingArea::Update(const Int id, const optional<Str> name, const optional<Int> spdgateid, const optional<Int> enter_timeout, const optional<Int> exit_timeout) {
	Exception e((Message("Cannot update ") << Message("Parking Area").What() << ". ").What());
	bool error=false;
	error |= Defined(spdgateid) && Defined(*spdgateid)  && !__CheckIDExists(SpdGate()->id, *spdgateid, "SPD Gate", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		ParkingArea aParkingArea;
		aParkingArea.id=id;
		aParkingArea.Select(rdb_);
		if(Defined(name)) aParkingArea.name=*name;
		if(Defined(spdgateid)) aParkingArea.spdgateid=*spdgateid;
		if(Defined(enter_timeout)) aParkingArea.enter_timeout=*enter_timeout;
		if(Defined(exit_timeout)) aParkingArea.exit_timeout=*exit_timeout;
		aParkingArea.Update(rdb_);
		AddMessage(Message()<<Message("Parking Area").What()<<" "<<aParkingArea.id<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONParkingArea::Delete(const Int id) {
	ParkingArea aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Parking Area").What()<<". ").What());
	//check ParkingOrderline reference to ParkingArea reference 
	error |= !__CheckIDNotExists(ParkingOrderline()->areaid, id, "Parking Ticket", e, rdb_);
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.id=id;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Parking Area").What()<<" "<<id<<" deleted. ");
	return Value();
}

const Value BALOONParkingArea::Get(const Int id) {
	Value res;
	ParkingArea aParkingArea;
	aParkingArea.id=id;
	aParkingArea.Select(rdb_);
	res["id"]=aParkingArea.id;
	res["name"]=aParkingArea.name;
	res["spdgateid"]=aParkingArea.spdgateid;
	res["enter_timeout"]=aParkingArea.enter_timeout;
	res["exit_timeout"]=aParkingArea.exit_timeout;
	return res;
}

const Value BALOONParkingArea::ParkingAreaListGet(const optional<Int> spdgateid) {
	Data::DataList lr;
	lr.AddColumn("id", Data::INTEGER);
	lr.AddColumn("name", Data::STRING);
	lr.AddColumn("spdgatename", Data::STRING);
	lr.AddColumn("enter_timeout", Data::INTEGER);
	lr.AddColumn("exit_timeout", Data::INTEGER);
	ParkingArea aParkingArea;
	SpdGate aspdgate;
	lr.Bind(aParkingArea.id, "id");
	lr.Bind(aParkingArea.name, "name");
	lr.Bind(aspdgate.name, "spdgatename");
	lr.Bind(aParkingArea.enter_timeout, "enter_timeout");
	lr.Bind(aParkingArea.exit_timeout, "exit_timeout");
	Selector sel;
	sel << aParkingArea->id << aParkingArea->name << aspdgate->name << aParkingArea->enter_timeout << aParkingArea->exit_timeout;
	sel.Join(aspdgate).On(aParkingArea->spdgateid==aspdgate->id);
	sel.Where( (Defined(spdgateid) ? aParkingArea->spdgateid==*spdgateid : Expression()));
	DataSet data=sel.Execute(rdb_);
	while(data.Fetch()) lr.AddRow();
	Value res=lr;
	return res;
}

void BALOONParkingTimer::initialize(ParamsMapRef params) {
	RegisterMethod("TimeLeftGet_API", (&BALOONParkingTimer::__TimeLeftGet_API));
	RegisterMethod("Start_API", (&BALOONParkingTimer::__Start_API));
	RegisterMethod("Get", (&BALOONParkingTimer::__Get));
	RegisterMethod("Delete", (&BALOONParkingTimer::__Delete));
	InitDatabase(params);
}

 const Value BALOONParkingTimer::__TimeLeftGet_API(const Param& param) {
	return TimeLeftGet_API(param["code"]);
}

 const Value BALOONParkingTimer::__Start_API(const Param& param) {
	return Start_API(param["code"], param["secretkey"]);
}

// Wrappers for automatic methods
const Value BALOONParkingTimer::__Delete(const Param& param) {
	return Delete(param["orderid"]);
}

const Value BALOONParkingTimer::__Get(const Param& param) {
	return Get(param["orderid"]);
}

// Implementation of automatic methods
const Value BALOONParkingTimer::Delete(const Int orderid) {
	ParkingTimer aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Parking Timer").What()<<". ").What());
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.orderid=orderid;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Parking Timer").What()<<" "<<orderid<<" deleted. ");
	return Value();
}

const Value BALOONParkingTimer::Get(const Int orderid) {
	Value res;
	ParkingTimer aParkingTimer;
	aParkingTimer.orderid=orderid;
	aParkingTimer.Select(rdb_);
	res["orderid"]=aParkingTimer.orderid;
	res["end_time"]=aParkingTimer.end_time;
	return res;
}

void BALOONParkingOrderline::initialize(ParamsMapRef params) {
	RegisterMethod("Add", (&BALOONParkingOrderline::__Add));
	RegisterMethod("Get", (&BALOONParkingOrderline::__Get));
	RegisterMethod("Update", (&BALOONParkingOrderline::__Update));
	RegisterMethod("Delete", (&BALOONParkingOrderline::__Delete));
	RegisterMethod("ParkingOrderlineListGet", (&BALOONParkingOrderline::__ParkingOrderlineListGet));
	InitDatabase(params);
}

// Wrappers for automatic methods
const Value BALOONParkingOrderline::__Add(const Param& param) {
	return Add(param["orderid"], param["identifierid"], param["time_from"], param["time_upto"], param["price"], param["areaid"]);
}

const Value BALOONParkingOrderline::__Update(const Param& param) {
	return Update(param["id"], param["orderid"], param["identifierid"], param["time_from"], param["time_upto"], param["price"], param["areaid"]);
}

const Value BALOONParkingOrderline::__Delete(const Param& param) {
	return Delete(param["id"]);
}

const Value BALOONParkingOrderline::__Get(const Param& param) {
	return Get(param["id"]);
}

const Value BALOONParkingOrderline::__ParkingOrderlineListGet(const Param& param) {
	return ParkingOrderlineListGet(param["orderid"], param["identifierid"], param["areaid"]);
}

// Implementation of automatic methods
const Value BALOONParkingOrderline::Add(const Int orderid, const Int identifierid, const Time time_from, const Time time_upto, const Decimal price, const Int areaid) {
	Exception e((Message("Cannot add ") << Message("Parking Ticket").What() << ". ").What());
	bool error=false;
	error |= Defined(orderid)  && !__CheckIDExists(PersonOrder()->id, *orderid, "Person Order", e, rdb_);
	error |= Defined(identifierid)  && !__CheckIDExists(Identifier()->id, *identifierid, "Identifier", e, rdb_);
	error |= Defined(areaid)  && !__CheckIDExists(ParkingArea()->id, *areaid, "Parking Area", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		ParkingOrderline aParkingOrderline;
		aParkingOrderline.orderid=orderid;
		aParkingOrderline.identifierid=identifierid;
		aParkingOrderline.time_from=time_from;
		aParkingOrderline.time_upto=time_upto;
		aParkingOrderline.price=price;
		aParkingOrderline.areaid=areaid;
		Int sk=aParkingOrderline.Insert(rdb_);
		Value res;
		res["id"]=sk;
		AddMessage(Message()<<Message("Parking Ticket").What()<<" "<<sk<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONParkingOrderline::Update(const Int id, const optional<Int> orderid, const optional<Int> identifierid, const optional<Time> time_from, const optional<Time> time_upto, const optional<Decimal> price, const optional<Int> areaid) {
	Exception e((Message("Cannot update ") << Message("Parking Ticket").What() << ". ").What());
	bool error=false;
	error |= Defined(orderid) && Defined(*orderid)  && !__CheckIDExists(PersonOrder()->id, *orderid, "Person Order", e, rdb_);
	error |= Defined(identifierid) && Defined(*identifierid)  && !__CheckIDExists(Identifier()->id, *identifierid, "Identifier", e, rdb_);
	error |= Defined(areaid) && Defined(*areaid)  && !__CheckIDExists(ParkingArea()->id, *areaid, "Parking Area", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		ParkingOrderline aParkingOrderline;
		aParkingOrderline.id=id;
		aParkingOrderline.Select(rdb_);
		if(Defined(orderid)) aParkingOrderline.orderid=*orderid;
		if(Defined(identifierid)) aParkingOrderline.identifierid=*identifierid;
		if(Defined(time_from)) aParkingOrderline.time_from=*time_from;
		if(Defined(time_upto)) aParkingOrderline.time_upto=*time_upto;
		if(Defined(price)) aParkingOrderline.price=*price;
		if(Defined(areaid)) aParkingOrderline.areaid=*areaid;
		aParkingOrderline.Update(rdb_);
		AddMessage(Message()<<Message("Parking Ticket").What()<<" "<<aParkingOrderline.id<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONParkingOrderline::Delete(const Int id) {
	ParkingOrderline aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Parking Ticket").What()<<". ").What());
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.id=id;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Parking Ticket").What()<<" "<<id<<" deleted. ");
	return Value();
}

const Value BALOONParkingOrderline::Get(const Int id) {
	Value res;
	ParkingOrderline aParkingOrderline;
	aParkingOrderline.id=id;
	aParkingOrderline.Select(rdb_);
	res["id"]=aParkingOrderline.id;
	res["orderid"]=aParkingOrderline.orderid;
	res["identifierid"]=aParkingOrderline.identifierid;
	res["time_from"]=aParkingOrderline.time_from;
	res["time_upto"]=aParkingOrderline.time_upto;
	res["price"]=aParkingOrderline.price;
	res["areaid"]=aParkingOrderline.areaid;
	return res;
}

const Value BALOONParkingOrderline::ParkingOrderlineListGet(const optional<Int> orderid, const optional<Int> identifierid, const optional<Int> areaid) {
	Data::DataList lr;
	lr.AddColumn("id", Data::INTEGER);
	lr.AddColumn("orderid", Data::INTEGER);
	lr.AddColumn("identifiercode", Data::STRING);
	lr.AddColumn("time_from", Data::DATETIME);
	lr.AddColumn("time_upto", Data::DATETIME);
	lr.AddColumn("price", Data::DECIMAL);
	lr.AddColumn("orderstatus", Data::STRING, PERSON_ORDER_STATUSValues());
	ParkingOrderline aParkingOrderline;
	PersonOrder aorder;
	Identifier aidentifier;
	lr.Bind(aParkingOrderline.id, "id");
	lr.Bind(aorder.id, "orderid");
	lr.Bind(aidentifier.code, "identifiercode");
	lr.Bind(aParkingOrderline.time_from, "time_from");
	lr.Bind(aParkingOrderline.time_upto, "time_upto");
	lr.Bind(aParkingOrderline.price, "price");
	lr.Bind(aorder.status, "orderstatus");
	Selector sel;
	sel << aParkingOrderline->id << aorder->id << aidentifier->code << aParkingOrderline->time_from << aParkingOrderline->time_upto << aParkingOrderline->price << aorder->status;
	sel.Join(aorder).On(aParkingOrderline->orderid==aorder->id).Join(aidentifier).On(aParkingOrderline->identifierid==aidentifier->id);
	sel.Where( (Defined(orderid) ? aParkingOrderline->orderid==*orderid : Expression()) &&  (Defined(identifierid) ? aParkingOrderline->identifierid==*identifierid : Expression()) &&  (Defined(areaid) ? aParkingOrderline->areaid==*areaid : Expression()));
	DataSet data=sel.Execute(rdb_);
	while(data.Fetch()) lr.AddRow();
	Value res=lr;
	return res;
}

void BALOONPersonOrderFiscalDoc::initialize(ParamsMapRef params) {
	RegisterMethod("Add", (&BALOONPersonOrderFiscalDoc::__Add));
	RegisterMethod("Get", (&BALOONPersonOrderFiscalDoc::__Get));
	RegisterMethod("Update", (&BALOONPersonOrderFiscalDoc::__Update));
	RegisterMethod("Delete", (&BALOONPersonOrderFiscalDoc::__Delete));
	RegisterMethod("PersonOrderFiscalDocListGet", (&BALOONPersonOrderFiscalDoc::__PersonOrderFiscalDocListGet));
	InitDatabase(params);
}

// Wrappers for automatic methods
const Value BALOONPersonOrderFiscalDoc::__Add(const Param& param) {
	return Add(param["orderid"], param["fiscal_docid"]);
}

const Value BALOONPersonOrderFiscalDoc::__Update(const Param& param) {
	return Update(param["orderid"], param["fiscal_docid"]);
}

const Value BALOONPersonOrderFiscalDoc::__Delete(const Param& param) {
	return Delete(param["orderid"], param["fiscal_docid"]);
}

const Value BALOONPersonOrderFiscalDoc::__Get(const Param& param) {
	return Get(param["orderid"], param["fiscal_docid"]);
}

const Value BALOONPersonOrderFiscalDoc::__PersonOrderFiscalDocListGet(const Param& param) {
	return PersonOrderFiscalDocListGet(param["orderid"], param["fiscal_docid"]);
}

// Implementation of automatic methods
const Value BALOONPersonOrderFiscalDoc::Add(const Int orderid, const Int fiscal_docid) {
	Exception e((Message("Cannot add ") << Message("Fiscal Document").What() << ". ").What());
	bool error=false;
	error |= Defined(orderid)  && !__CheckIDExists(PersonOrder()->id, *orderid, "Person Order", e, rdb_);
	error |= Defined(fiscal_docid)  && !__CheckIDExists(FiscalDoc()->id, *fiscal_docid, "Fiscal Document", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		PersonOrderFiscalDoc aPersonOrderFiscalDoc;
		aPersonOrderFiscalDoc.orderid=orderid;
		aPersonOrderFiscalDoc.fiscal_docid=fiscal_docid;
	aPersonOrderFiscalDoc.Insert(rdb_);
		Value res;
		AddMessage(Message()<<Message("Fiscal Document").What()<<" "<<aPersonOrderFiscalDoc.orderid<<" "<<aPersonOrderFiscalDoc.fiscal_docid<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONPersonOrderFiscalDoc::Update(const Int orderid, const Int fiscal_docid) {
	Exception e((Message("Cannot update ") << Message("Fiscal Document").What() << ". ").What());
	bool error=false;
	error |= Defined(orderid)  && !__CheckIDExists(PersonOrder()->id, *orderid, "Person Order", e, rdb_);
	error |= Defined(fiscal_docid)  && !__CheckIDExists(FiscalDoc()->id, *fiscal_docid, "Fiscal Document", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		PersonOrderFiscalDoc aPersonOrderFiscalDoc;
		aPersonOrderFiscalDoc.orderid=orderid;
		aPersonOrderFiscalDoc.fiscal_docid=fiscal_docid;
		aPersonOrderFiscalDoc.Select(rdb_);
		aPersonOrderFiscalDoc.Update(rdb_);
		AddMessage(Message()<<Message("Fiscal Document").What()<<" "<<aPersonOrderFiscalDoc.orderid<<" "<<aPersonOrderFiscalDoc.fiscal_docid<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONPersonOrderFiscalDoc::Delete(const Int orderid, const Int fiscal_docid) {
	PersonOrderFiscalDoc aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Fiscal Document").What()<<". ").What());
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.orderid=orderid;
	aRecord.fiscal_docid=fiscal_docid;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Fiscal Document").What()<<" "<<orderid<<" "<<fiscal_docid<<" deleted. ");
	return Value();
}

const Value BALOONPersonOrderFiscalDoc::Get(const Int orderid, const Int fiscal_docid) {
	Value res;
	PersonOrderFiscalDoc aPersonOrderFiscalDoc;
	aPersonOrderFiscalDoc.orderid=orderid;
	aPersonOrderFiscalDoc.fiscal_docid=fiscal_docid;
	aPersonOrderFiscalDoc.Select(rdb_);
	res["orderid"]=aPersonOrderFiscalDoc.orderid;
	res["fiscal_docid"]=aPersonOrderFiscalDoc.fiscal_docid;
	return res;
}

const Value BALOONPersonOrderFiscalDoc::PersonOrderFiscalDocListGet(const optional<Int> orderid, const optional<Int> fiscal_docid) {
	Data::DataList lr;
	lr.AddColumn("orderid", Data::INTEGER);
	lr.AddColumn("fiscal_docid", Data::INTEGER);
	lr.AddColumn("fiscal_docdoc_type", Data::STRING);
	lr.AddColumn("fiscal_docbarcode", Data::STRING);
	lr.AddColumn("fiscal_docstatus", Data::STRING);
	lr.AddColumn("fiscal_docerror_message", Data::STRING);
	PersonOrder aorder;
	FiscalDoc afiscal_doc;
	PersonOrderFiscalDoc aPersonOrderFiscalDoc;
	lr.Bind(aorder.id, "orderid");
	lr.Bind(afiscal_doc.id, "fiscal_docid");
	lr.Bind(afiscal_doc.doc_type, "fiscal_docdoc_type");
	lr.Bind(afiscal_doc.barcode, "fiscal_docbarcode");
	lr.Bind(afiscal_doc.status, "fiscal_docstatus");
	lr.Bind(afiscal_doc.error_message, "fiscal_docerror_message");
	Selector sel;
	sel << aorder->id << afiscal_doc->id << afiscal_doc->doc_type << afiscal_doc->barcode << afiscal_doc->status << afiscal_doc->error_message;
	sel.Join(aorder).On(aPersonOrderFiscalDoc->orderid==aorder->id).Join(afiscal_doc).On(aPersonOrderFiscalDoc->fiscal_docid==afiscal_doc->id);
	sel.Where( (Defined(orderid) ? aPersonOrderFiscalDoc->orderid==*orderid : Expression()) &&  (Defined(fiscal_docid) ? aPersonOrderFiscalDoc->fiscal_docid==*fiscal_docid : Expression()));
	DataSet data=sel.Execute(rdb_);
	while(data.Fetch()) lr.AddRow();
	Value res=lr;
	return res;
}

void BALOONPersonIdentifierOrderline::initialize(ParamsMapRef params) {
	RegisterMethod("PersonIdentifierOrderlineListGet", (&BALOONPersonIdentifierOrderline::__PersonIdentifierOrderlineListGet));
	RegisterMethod("PersonIdentifierOrderlineListGet_FE", (&BALOONPersonIdentifierOrderline::__PersonIdentifierOrderlineListGet_FE));
	RegisterMethod("MyOrderlineListGet_FE", (&BALOONPersonIdentifierOrderline::__MyOrderlineListGet_FE));
	RegisterMethod("Add", (&BALOONPersonIdentifierOrderline::__Add));
	RegisterMethod("Get", (&BALOONPersonIdentifierOrderline::__Get));
	RegisterMethod("Update", (&BALOONPersonIdentifierOrderline::__Update));
	RegisterMethod("Delete", (&BALOONPersonIdentifierOrderline::__Delete));
	InitDatabase(params);
}

 const Value BALOONPersonIdentifierOrderline::__PersonIdentifierOrderlineListGet(const Param& param) {
	return PersonIdentifierOrderlineListGet(param["orderid"], param["identifierid"]);
}

 const Value BALOONPersonIdentifierOrderline::__PersonIdentifierOrderlineListGet_FE(const Param& param) {
	return PersonIdentifierOrderlineListGet_FE(param["orderid"]);
}

 const Value BALOONPersonIdentifierOrderline::__MyOrderlineListGet_FE(const Param& param) {
	return MyOrderlineListGet_FE(param["orderid"]);
}

// Wrappers for automatic methods
const Value BALOONPersonIdentifierOrderline::__Add(const Param& param) {
	return Add(param["orderid"], param["identifierid"], param["price"], param["service_rulename"], param["make_transaction"]);
}

const Value BALOONPersonIdentifierOrderline::__Update(const Param& param) {
	return Update(param["orderid"], param["identifierid"], param["price"], param["service_rulename"], param["make_transaction"]);
}

const Value BALOONPersonIdentifierOrderline::__Delete(const Param& param) {
	return Delete(param["orderid"], param["identifierid"]);
}

const Value BALOONPersonIdentifierOrderline::__Get(const Param& param) {
	return Get(param["orderid"], param["identifierid"]);
}

// Implementation of automatic methods
const Value BALOONPersonIdentifierOrderline::Add(const Int orderid, const Int identifierid, const Decimal price, const Str service_rulename, const Str make_transaction) {
	Exception e((Message("Cannot add ") << Message("Orderline").What() << ". ").What());
	bool error=false;
	error |= Defined(orderid)  && !__CheckIDExists(PersonOrder()->id, *orderid, "Order", e, rdb_);
	error |= Defined(identifierid)  && !__CheckIDExists(Identifier()->id, *identifierid, "Identifier", e, rdb_);
	error |= Defined(service_rulename)  && !__CheckIDExists(SpdServiceRule()->name, *service_rulename, "Package", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		PersonIdentifierOrderline aPersonIdentifierOrderline;
		aPersonIdentifierOrderline.orderid=orderid;
		aPersonIdentifierOrderline.identifierid=identifierid;
		aPersonIdentifierOrderline.price=price;
		aPersonIdentifierOrderline.service_rulename=service_rulename;
		aPersonIdentifierOrderline.make_transaction=make_transaction;
	aPersonIdentifierOrderline.Insert(rdb_);
		Value res;
		AddMessage(Message()<<Message("Orderline").What()<<" "<<aPersonIdentifierOrderline.orderid<<" "<<aPersonIdentifierOrderline.identifierid<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONPersonIdentifierOrderline::Update(const Int orderid, const Int identifierid, const optional<Decimal> price, const optional<Str> service_rulename, const optional<Str> make_transaction) {
	Exception e((Message("Cannot update ") << Message("Orderline").What() << ". ").What());
	bool error=false;
	error |= Defined(orderid)  && !__CheckIDExists(PersonOrder()->id, *orderid, "Order", e, rdb_);
	error |= Defined(identifierid)  && !__CheckIDExists(Identifier()->id, *identifierid, "Identifier", e, rdb_);
	error |= Defined(service_rulename) && Defined(*service_rulename)  && !__CheckIDExists(SpdServiceRule()->name, *service_rulename, "Package", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		PersonIdentifierOrderline aPersonIdentifierOrderline;
		aPersonIdentifierOrderline.orderid=orderid;
		aPersonIdentifierOrderline.identifierid=identifierid;
		aPersonIdentifierOrderline.Select(rdb_);
		if(Defined(price)) aPersonIdentifierOrderline.price=*price;
		if(Defined(service_rulename)) aPersonIdentifierOrderline.service_rulename=*service_rulename;
		if(Defined(make_transaction)) aPersonIdentifierOrderline.make_transaction=*make_transaction;
		aPersonIdentifierOrderline.Update(rdb_);
		AddMessage(Message()<<Message("Orderline").What()<<" "<<aPersonIdentifierOrderline.orderid<<" "<<aPersonIdentifierOrderline.identifierid<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONPersonIdentifierOrderline::Delete(const Int orderid, const Int identifierid) {
	PersonIdentifierOrderline aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Orderline").What()<<". ").What());
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.orderid=orderid;
	aRecord.identifierid=identifierid;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Orderline").What()<<" "<<orderid<<" "<<identifierid<<" deleted. ");
	return Value();
}

const Value BALOONPersonIdentifierOrderline::Get(const Int orderid, const Int identifierid) {
	Value res;
	PersonIdentifierOrderline aPersonIdentifierOrderline;
	aPersonIdentifierOrderline.orderid=orderid;
	aPersonIdentifierOrderline.identifierid=identifierid;
	aPersonIdentifierOrderline.Select(rdb_);
	res["orderid"]=aPersonIdentifierOrderline.orderid;
	res["identifierid"]=aPersonIdentifierOrderline.identifierid;
	res["price"]=aPersonIdentifierOrderline.price;
	res["service_rulename"]=aPersonIdentifierOrderline.service_rulename;
	res["make_transaction"]=aPersonIdentifierOrderline.make_transaction;
	return res;
}

void BALOONAgentIdentifierOrderline::initialize(ParamsMapRef params) {
	RegisterMethod("Add", (&BALOONAgentIdentifierOrderline::__Add));
	RegisterMethod("Get", (&BALOONAgentIdentifierOrderline::__Get));
	RegisterMethod("Update", (&BALOONAgentIdentifierOrderline::__Update));
	RegisterMethod("Delete", (&BALOONAgentIdentifierOrderline::__Delete));
	RegisterMethod("AgentIdentifierOrderlineListGet", (&BALOONAgentIdentifierOrderline::__AgentIdentifierOrderlineListGet));
	InitDatabase(params);
}

// Wrappers for automatic methods
const Value BALOONAgentIdentifierOrderline::__Add(const Param& param) {
	return Add(param["orderid"], param["identifierid"], param["price"]);
}

const Value BALOONAgentIdentifierOrderline::__Update(const Param& param) {
	return Update(param["orderid"], param["identifierid"], param["price"]);
}

const Value BALOONAgentIdentifierOrderline::__Delete(const Param& param) {
	return Delete(param["orderid"], param["identifierid"]);
}

const Value BALOONAgentIdentifierOrderline::__Get(const Param& param) {
	return Get(param["orderid"], param["identifierid"]);
}

const Value BALOONAgentIdentifierOrderline::__AgentIdentifierOrderlineListGet(const Param& param) {
	return AgentIdentifierOrderlineListGet(param["orderid"], param["identifierid"]);
}

// Implementation of automatic methods
const Value BALOONAgentIdentifierOrderline::Add(const Int orderid, const Int identifierid, const Decimal price) {
	Exception e((Message("Cannot add ") << Message("Orderline").What() << ". ").What());
	bool error=false;
	error |= Defined(orderid)  && !__CheckIDExists(AgentOrder()->id, *orderid, "Agent Order", e, rdb_);
	error |= Defined(identifierid)  && !__CheckIDExists(Identifier()->id, *identifierid, "Identifier", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		AgentIdentifierOrderline aAgentIdentifierOrderline;
		aAgentIdentifierOrderline.orderid=orderid;
		aAgentIdentifierOrderline.identifierid=identifierid;
		aAgentIdentifierOrderline.price=price;
	aAgentIdentifierOrderline.Insert(rdb_);
		Value res;
		AddMessage(Message()<<Message("Orderline").What()<<" "<<aAgentIdentifierOrderline.orderid<<" "<<aAgentIdentifierOrderline.identifierid<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONAgentIdentifierOrderline::Update(const Int orderid, const Int identifierid, const optional<Decimal> price) {
	Exception e((Message("Cannot update ") << Message("Orderline").What() << ". ").What());
	bool error=false;
	error |= Defined(orderid)  && !__CheckIDExists(AgentOrder()->id, *orderid, "Agent Order", e, rdb_);
	error |= Defined(identifierid)  && !__CheckIDExists(Identifier()->id, *identifierid, "Identifier", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		AgentIdentifierOrderline aAgentIdentifierOrderline;
		aAgentIdentifierOrderline.orderid=orderid;
		aAgentIdentifierOrderline.identifierid=identifierid;
		aAgentIdentifierOrderline.Select(rdb_);
		if(Defined(price)) aAgentIdentifierOrderline.price=*price;
		aAgentIdentifierOrderline.Update(rdb_);
		AddMessage(Message()<<Message("Orderline").What()<<" "<<aAgentIdentifierOrderline.orderid<<" "<<aAgentIdentifierOrderline.identifierid<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONAgentIdentifierOrderline::Delete(const Int orderid, const Int identifierid) {
	AgentIdentifierOrderline aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Orderline").What()<<". ").What());
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.orderid=orderid;
	aRecord.identifierid=identifierid;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Orderline").What()<<" "<<orderid<<" "<<identifierid<<" deleted. ");
	return Value();
}

const Value BALOONAgentIdentifierOrderline::Get(const Int orderid, const Int identifierid) {
	Value res;
	AgentIdentifierOrderline aAgentIdentifierOrderline;
	aAgentIdentifierOrderline.orderid=orderid;
	aAgentIdentifierOrderline.identifierid=identifierid;
	aAgentIdentifierOrderline.Select(rdb_);
	res["orderid"]=aAgentIdentifierOrderline.orderid;
	res["identifierid"]=aAgentIdentifierOrderline.identifierid;
	res["price"]=aAgentIdentifierOrderline.price;
	return res;
}

const Value BALOONAgentIdentifierOrderline::AgentIdentifierOrderlineListGet(const optional<Int> orderid, const optional<Int> identifierid) {
	Data::DataList lr;
	lr.AddColumn("orderid", Data::INTEGER);
	lr.AddColumn("identifiercode", Data::STRING);
	lr.AddColumn("identifierpermanent_rulename", Data::STRING);
	lr.AddColumn("identifiervalid_from", Data::DATETIME);
	lr.AddColumn("identifiervalid_to", Data::DATETIME);
	lr.AddColumn("identifierratename", Data::STRING);
	lr.AddColumn("identifiername", Data::STRING);
	lr.AddColumn("price", Data::DECIMAL);
	lr.AddColumn("identifierstatus", Data::STRING, IDENTIFIER_STATUSValues());
	lr.AddColumn("identifiercomment", Data::STRING);
	lr.AddColumn("service_rulename", Data::STRING);
	AgentIdentifierOrderline aAgentIdentifierOrderline;
	AgentOrder aorder;
	Identifier aidentifier;
	SpdServiceRule aservice_rule;
	lr.Bind(aorder.id, "orderid");
	lr.Bind(aidentifier.code, "identifiercode");
	lr.Bind(aidentifier.permanent_rulename, "identifierpermanent_rulename");
	lr.Bind(aidentifier.valid_from, "identifiervalid_from");
	lr.Bind(aidentifier.valid_to, "identifiervalid_to");
	lr.Bind(aidentifier.ratename, "identifierratename");
	lr.Bind(aidentifier.name, "identifiername");
	lr.Bind(aAgentIdentifierOrderline.price, "price");
	lr.Bind(aidentifier.status, "identifierstatus");
	lr.Bind(aidentifier.comment, "identifiercomment");
	lr.Bind(aservice_rule.name, "service_rulename");
	Selector sel;
	sel << aorder->id << aidentifier->code << aidentifier->permanent_rulename << aidentifier->valid_from << aidentifier->valid_to << aidentifier->ratename << aidentifier->name << aAgentIdentifierOrderline->price << aidentifier->status << aidentifier->comment << aservice_rule->name;
	sel.Join(aorder).On(aAgentIdentifierOrderline->orderid==aorder->id).Join(aidentifier).On(aAgentIdentifierOrderline->identifierid==aidentifier->id);
	sel.Where( (Defined(orderid) ? aAgentIdentifierOrderline->orderid==*orderid : Expression()) &&  (Defined(identifierid) ? aAgentIdentifierOrderline->identifierid==*identifierid : Expression()));
	DataSet data=sel.Execute(rdb_);
	while(data.Fetch()) lr.AddRow();
	Value res=lr;
	return res;
}

void BALOONPersonInvoice::initialize(ParamsMapRef params) {
	RegisterMethod("RaiseInvoice_FE", (&BALOONPersonInvoice::__RaiseInvoice_FE));
	RegisterMethod("StatusUpdated", (&BALOONPersonInvoice::__StatusUpdated));
	RegisterMethod("Add", (&BALOONPersonInvoice::__Add));
	RegisterMethod("Get", (&BALOONPersonInvoice::__Get));
	RegisterMethod("Update", (&BALOONPersonInvoice::__Update));
	RegisterMethod("Delete", (&BALOONPersonInvoice::__Delete));
	RegisterMethod("PersonInvoiceListGet", (&BALOONPersonInvoice::__PersonInvoiceListGet));
	InitDatabase(params);
}

 const Value BALOONPersonInvoice::__RaiseInvoice_FE(const Param& param) {
	return RaiseInvoice_FE(param["orderid"], param["amount"], param["success_url"], param["fail_url"]);
}

 const Value BALOONPersonInvoice::__StatusUpdated(const Param& param) {
	return StatusUpdated(param["id"], param["barcode"], param["status"], param["amount"]);
}

// Wrappers for automatic methods
const Value BALOONPersonInvoice::__Add(const Param& param) {
	return Add(param["id"], param["orderid"]);
}

const Value BALOONPersonInvoice::__Update(const Param& param) {
	return Update(param["id"], param["orderid"]);
}

const Value BALOONPersonInvoice::__Delete(const Param& param) {
	return Delete(param["id"]);
}

const Value BALOONPersonInvoice::__Get(const Param& param) {
	return Get(param["id"]);
}

const Value BALOONPersonInvoice::__PersonInvoiceListGet(const Param& param) {
	return PersonInvoiceListGet(param["id"], param["orderid"]);
}

// Implementation of automatic methods
const Value BALOONPersonInvoice::Add(const Int id, const Int orderid) {
	Exception e((Message("Cannot add ") << Message("Invoice").What() << ". ").What());
	bool error=false;
	error |= Defined(id)  && !__CheckIDExists(Invoice()->id, *id, "Invoice ID", e, rdb_);
	error |= Defined(orderid)  && !__CheckIDExists(PersonOrder()->id, *orderid, "Order", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		PersonInvoice aPersonInvoice;
		aPersonInvoice.id=id;
		aPersonInvoice.orderid=orderid;
	aPersonInvoice.Insert(rdb_);
		Value res;
		AddMessage(Message()<<Message("Invoice").What()<<" "<<aPersonInvoice.id<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONPersonInvoice::Update(const Int id, const optional<Int> orderid) {
	Exception e((Message("Cannot update ") << Message("Invoice").What() << ". ").What());
	bool error=false;
	error |= Defined(id)  && !__CheckIDExists(Invoice()->id, *id, "Invoice ID", e, rdb_);
	error |= Defined(orderid) && Defined(*orderid)  && !__CheckIDExists(PersonOrder()->id, *orderid, "Order", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		PersonInvoice aPersonInvoice;
		aPersonInvoice.id=id;
		aPersonInvoice.Select(rdb_);
		if(Defined(orderid)) aPersonInvoice.orderid=*orderid;
		aPersonInvoice.Update(rdb_);
		AddMessage(Message()<<Message("Invoice").What()<<" "<<aPersonInvoice.id<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONPersonInvoice::Delete(const Int id) {
	PersonInvoice aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Invoice").What()<<". ").What());
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.id=id;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Invoice").What()<<" "<<id<<" deleted. ");
	return Value();
}

const Value BALOONPersonInvoice::Get(const Int id) {
	Value res;
	PersonInvoice aPersonInvoice;
	aPersonInvoice.id=id;
	aPersonInvoice.Select(rdb_);
	res["id"]=aPersonInvoice.id;
	res["orderid"]=aPersonInvoice.orderid;
	return res;
}

const Value BALOONPersonInvoice::PersonInvoiceListGet(const optional<Int> id, const optional<Int> orderid) {
	Data::DataList lr;
	lr.AddColumn("invoicebarcode", Data::STRING);
	lr.AddColumn("paygatename", Data::STRING);
	lr.AddColumn("invoicecurrency", Data::STRING);
	lr.AddColumn("invoiceamount", Data::DECIMAL);
	lr.AddColumn("invoicestatus", Data::STRING);
	Invoice ainvoice;
	PayGate apaygate;
	PersonInvoice aPersonInvoice;
	PersonOrder aorder;
	lr.Bind(ainvoice.barcode, "invoicebarcode");
	lr.Bind(apaygate.name, "paygatename");
	lr.Bind(ainvoice.currency, "invoicecurrency");
	lr.Bind(ainvoice.amount, "invoiceamount");
	lr.Bind(ainvoice.status, "invoicestatus");
	Selector sel;
	sel << ainvoice->barcode << apaygate->name << ainvoice->currency << ainvoice->amount << ainvoice->status;
	sel.Join(ainvoice).On(aPersonInvoice->id==ainvoice->id).Join(apaygate).On(ainvoice->paygateid==apaygate->id).Join(aorder).On(aPersonInvoice->orderid==aorder->id);
	sel.Where( (Defined(id) ? aPersonInvoice->id==*id : Expression()) &&  (Defined(orderid) ? aPersonInvoice->orderid==*orderid : Expression()));
	DataSet data=sel.Execute(rdb_);
	while(data.Fetch()) lr.AddRow();
	Value res=lr;
	return res;
}

void BALOONIdentifierInvoice::initialize(ParamsMapRef params) {
	RegisterMethod("SyncInvoice", (&BALOONIdentifierInvoice::__SyncInvoice));
	RegisterMethod("StatusUpdated", (&BALOONIdentifierInvoice::__StatusUpdated));
	RegisterMethod("RaiseInvoice", (&BALOONIdentifierInvoice::__RaiseInvoice));
	RegisterMethod("RaiseInvoiceForCurrentUser", (&BALOONIdentifierInvoice::__RaiseInvoiceForCurrentUser));
	RegisterMethod("RaiseInvoice_FE", (&BALOONIdentifierInvoice::__RaiseInvoice_FE));
	RegisterMethod("CurrentInvoicesGetList_FE", (&BALOONIdentifierInvoice::__CurrentInvoicesGetList_FE));
	RegisterMethod("Add", (&BALOONIdentifierInvoice::__Add));
	RegisterMethod("Get", (&BALOONIdentifierInvoice::__Get));
	RegisterMethod("Update", (&BALOONIdentifierInvoice::__Update));
	RegisterMethod("Delete", (&BALOONIdentifierInvoice::__Delete));
	RegisterMethod("IdentifierInvoiceListGet", (&BALOONIdentifierInvoice::__IdentifierInvoiceListGet));
	InitDatabase(params);
}

 const Value BALOONIdentifierInvoice::__SyncInvoice(const Param& param) {
	return SyncInvoice(param["id"]);
}

 const Value BALOONIdentifierInvoice::__StatusUpdated(const Param& param) {
	return StatusUpdated(param["id"], param["barcode"], param["status"], param["amount"]);
}

 const Value BALOONIdentifierInvoice::__RaiseInvoice(const Param& param) {
	return RaiseInvoice(param["paygateid"], param["identifierid"], param["amount"], param["success_url"], param["fail_url"]);
}

 const Value BALOONIdentifierInvoice::__RaiseInvoiceForCurrentUser(const Param& param) {
	return RaiseInvoiceForCurrentUser(param["amount"], param["success_url"], param["fail_url"]);
}

 const Value BALOONIdentifierInvoice::__RaiseInvoice_FE(const Param& param) {
	return RaiseInvoice_FE(param["code"], param["amount"], param["success_url"], param["fail_url"]);
}

 const Value BALOONIdentifierInvoice::__CurrentInvoicesGetList_FE(const Param& param) {
	return CurrentInvoicesGetList_FE();
}

// Wrappers for automatic methods
const Value BALOONIdentifierInvoice::__Add(const Param& param) {
	return Add(param["id"], param["identifierid"], param["fiscal_docid"], param["eid"]);
}

const Value BALOONIdentifierInvoice::__Update(const Param& param) {
	return Update(param["id"], param["identifierid"], param["fiscal_docid"], param["eid"]);
}

const Value BALOONIdentifierInvoice::__Delete(const Param& param) {
	return Delete(param["id"]);
}

const Value BALOONIdentifierInvoice::__Get(const Param& param) {
	return Get(param["id"]);
}

const Value BALOONIdentifierInvoice::__IdentifierInvoiceListGet(const Param& param) {
	return IdentifierInvoiceListGet(param["id"], param["identifierid"], param["fiscal_docid"]);
}

// Implementation of automatic methods
const Value BALOONIdentifierInvoice::Add(const Int id, const Int identifierid, const Int fiscal_docid, const Str eid) {
	Exception e((Message("Cannot add ") << Message("Invoice").What() << ". ").What());
	bool error=false;
	error |= Defined(id)  && !__CheckIDExists(Invoice()->id, *id, "Invoice ID", e, rdb_);
	error |= Defined(identifierid)  && !__CheckIDExists(Identifier()->id, *identifierid, "Identifier", e, rdb_);
	error |= Defined(fiscal_docid)  && !__CheckIDExists(FiscalDoc()->id, *fiscal_docid, "Fiscal Document", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		IdentifierInvoice aIdentifierInvoice;
		aIdentifierInvoice.id=id;
		aIdentifierInvoice.identifierid=identifierid;
		aIdentifierInvoice.fiscal_docid=fiscal_docid;
		aIdentifierInvoice.eid=eid;
	aIdentifierInvoice.Insert(rdb_);
		Value res;
		AddMessage(Message()<<Message("Invoice").What()<<" "<<aIdentifierInvoice.id<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONIdentifierInvoice::Update(const Int id, const optional<Int> identifierid, const optional<Int> fiscal_docid, const optional<Str> eid) {
	Exception e((Message("Cannot update ") << Message("Invoice").What() << ". ").What());
	bool error=false;
	error |= Defined(id)  && !__CheckIDExists(Invoice()->id, *id, "Invoice ID", e, rdb_);
	error |= Defined(identifierid) && Defined(*identifierid)  && !__CheckIDExists(Identifier()->id, *identifierid, "Identifier", e, rdb_);
	error |= Defined(fiscal_docid) && Defined(*fiscal_docid)  && !__CheckIDExists(FiscalDoc()->id, *fiscal_docid, "Fiscal Document", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		IdentifierInvoice aIdentifierInvoice;
		aIdentifierInvoice.id=id;
		aIdentifierInvoice.Select(rdb_);
		if(Defined(identifierid)) aIdentifierInvoice.identifierid=*identifierid;
		if(Defined(fiscal_docid)) aIdentifierInvoice.fiscal_docid=*fiscal_docid;
		if(Defined(eid)) aIdentifierInvoice.eid=*eid;
		aIdentifierInvoice.Update(rdb_);
		AddMessage(Message()<<Message("Invoice").What()<<" "<<aIdentifierInvoice.id<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONIdentifierInvoice::Delete(const Int id) {
	IdentifierInvoice aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Invoice").What()<<". ").What());
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.id=id;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Invoice").What()<<" "<<id<<" deleted. ");
	return Value();
}

const Value BALOONIdentifierInvoice::Get(const Int id) {
	Value res;
	IdentifierInvoice aIdentifierInvoice;
	aIdentifierInvoice.id=id;
	aIdentifierInvoice.Select(rdb_);
	res["id"]=aIdentifierInvoice.id;
	res["identifierid"]=aIdentifierInvoice.identifierid;
	res["fiscal_docid"]=aIdentifierInvoice.fiscal_docid;
	res["eid"]=aIdentifierInvoice.eid;
	return res;
}

const Value BALOONIdentifierInvoice::IdentifierInvoiceListGet(const optional<Int> id, const optional<Int> identifierid, const optional<Int> fiscal_docid) {
	Data::DataList lr;
	lr.AddColumn("id", Data::INTEGER);
	lr.AddColumn("barcode", Data::STRING);
	lr.AddColumn("paygateid", Data::INTEGER);
	lr.AddColumn("gatename", Data::STRING);
	lr.AddColumn("currency", Data::STRING);
	lr.AddColumn("amount", Data::DECIMAL);
	lr.AddColumn("status", Data::STRING);
	lr.AddColumn("comment", Data::STRING);
	lr.AddColumn("ext_orderid", Data::STRING);
	lr.AddColumn("identifiercode", Data::STRING);
	IdentifierInvoice aIdentifierInvoice;
	Invoice aInvoice;
	PayGate agate;
	Identifier aidentifier;
	PayGate apaygate;
	lr.Bind(aIdentifierInvoice.id, "id");
	lr.Bind(aInvoice.barcode, "barcode");
	lr.Bind(aInvoice.paygateid, "paygateid");
	lr.Bind(agate.name, "gatename");
	lr.Bind(aInvoice.currency, "currency");
	lr.Bind(aInvoice.amount, "amount");
	lr.Bind(aInvoice.status, "status");
	lr.Bind(aInvoice.comment, "comment");
	lr.Bind(aInvoice.ext_orderid, "ext_orderid");
	lr.Bind(aidentifier.code, "identifiercode");
	Selector sel;
	sel << aIdentifierInvoice->id << aInvoice->barcode << aInvoice->paygateid << agate->name << aInvoice->currency << aInvoice->amount << aInvoice->status << aInvoice->comment << aInvoice->ext_orderid << aidentifier->code;
	sel.Join(aInvoice).On(aIdentifierInvoice->id==aInvoice->id).Join(aidentifier).On(aIdentifierInvoice->identifierid==aidentifier->id).LeftJoin(apaygate).On(aInvoice->paygateid==apaygate->id);
	sel.Where( (Defined(id) ? aIdentifierInvoice->id==*id : Expression()) &&  (Defined(identifierid) ? aIdentifierInvoice->identifierid==*identifierid : Expression()) &&  (Defined(fiscal_docid) ? aIdentifierInvoice->fiscal_docid==*fiscal_docid : Expression()));
	DataSet data=sel.Execute(rdb_);
	while(data.Fetch()) lr.AddRow();
	Value res=lr;
	return res;
}

void BALOONBaloonProvider::initialize(ParamsMapRef params) {
	RegisterMethod("IdentifierPriceGet", (&BALOONBaloonProvider::__IdentifierPriceGet));
	RegisterMethod("Add", (&BALOONBaloonProvider::__Add));
	RegisterMethod("Get", (&BALOONBaloonProvider::__Get));
	RegisterMethod("Update", (&BALOONBaloonProvider::__Update));
	RegisterMethod("Delete", (&BALOONBaloonProvider::__Delete));
	InitDatabase(params);
}

 const Value BALOONBaloonProvider::__IdentifierPriceGet(const Param& param) {
	return IdentifierPriceGet();
}

// Wrappers for automatic methods
const Value BALOONBaloonProvider::__Add(const Param& param) {
	return Add(param["default_paygateid"], param["default_fiscalgateid"], param["code_conversion_algo"], param["agent_commission"], param["ident_sale_price"], param["timeout_before"], param["sun"], param["mon"], param["tue"], param["wed"], param["thu"], param["fri"], param["sat"]);
}

const Value BALOONBaloonProvider::__Update(const Param& param) {
	return Update(param["default_paygateid"], param["default_fiscalgateid"], param["code_conversion_algo"], param["agent_commission"], param["ident_sale_price"], param["timeout_before"], param["sun"], param["mon"], param["tue"], param["wed"], param["thu"], param["fri"], param["sat"]);
}

const Value BALOONBaloonProvider::__Delete(const Param& param) {
	return Delete();
}

const Value BALOONBaloonProvider::__Get(const Param& param) {
	return Get();
}

// Implementation of automatic methods
const Value BALOONBaloonProvider::Add(const Int default_paygateid, const Int default_fiscalgateid, const Str code_conversion_algo, const Double agent_commission, const Decimal ident_sale_price, const Int timeout_before, const Str sun, const Str mon, const Str tue, const Str wed, const Str thu, const Str fri, const Str sat) {
	Exception e((Message("Cannot add ") << Message("Provider Settings").What() << ". ").What());
	bool error=false;
	error |= Defined(default_paygateid)  && !__CheckIDExists(PayGate()->id, *default_paygateid, "Default Payment Gate", e, rdb_);
	error |= Defined(default_fiscalgateid)  && !__CheckIDExists(FiscalGate()->id, *default_fiscalgateid, "Default Fiscal Gate", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		throw(Exception("Attempt to call Add method for singular(without key fields) class BaloonProvider. "));
		Value res;
		AddMessage(Message()<<Message("Provider Settings").What()<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONBaloonProvider::Update(const optional<Int> default_paygateid, const optional<Int> default_fiscalgateid, const optional<Str> code_conversion_algo, const optional<Double> agent_commission, const optional<Decimal> ident_sale_price, const optional<Int> timeout_before, const optional<Str> sun, const optional<Str> mon, const optional<Str> tue, const optional<Str> wed, const optional<Str> thu, const optional<Str> fri, const optional<Str> sat) {
	Exception e((Message("Cannot update ") << Message("Provider Settings").What() << ". ").What());
	bool error=false;
	error |= Defined(default_paygateid) && Defined(*default_paygateid)  && !__CheckIDExists(PayGate()->id, *default_paygateid, "Default Payment Gate", e, rdb_);
	error |= Defined(default_fiscalgateid) && Defined(*default_fiscalgateid)  && !__CheckIDExists(FiscalGate()->id, *default_fiscalgateid, "Default Fiscal Gate", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		BaloonProvider aBaloonProvider;
		aBaloonProvider.Select(rdb_);
		if(Defined(default_paygateid)) aBaloonProvider.default_paygateid=*default_paygateid;
		if(Defined(default_fiscalgateid)) aBaloonProvider.default_fiscalgateid=*default_fiscalgateid;
		if(Defined(code_conversion_algo)) aBaloonProvider.code_conversion_algo=*code_conversion_algo;
		if(Defined(agent_commission)) aBaloonProvider.agent_commission=*agent_commission;
		if(Defined(ident_sale_price)) aBaloonProvider.ident_sale_price=*ident_sale_price;
		if(Defined(timeout_before)) aBaloonProvider.timeout_before=*timeout_before;
		if(Defined(sun)) aBaloonProvider.sun=*sun;
		if(Defined(mon)) aBaloonProvider.mon=*mon;
		if(Defined(tue)) aBaloonProvider.tue=*tue;
		if(Defined(wed)) aBaloonProvider.wed=*wed;
		if(Defined(thu)) aBaloonProvider.thu=*thu;
		if(Defined(fri)) aBaloonProvider.fri=*fri;
		if(Defined(sat)) aBaloonProvider.sat=*sat;
		aBaloonProvider.Update(rdb_);
		AddMessage(Message()<<Message("Provider Settings").What()<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONBaloonProvider::Delete() {
	BaloonProvider aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Provider Settings").What()<<". ").What());
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Provider Settings").What()<<" deleted. ");
	return Value();
}

const Value BALOONBaloonProvider::Get() {
	Value res;
	BaloonProvider aBaloonProvider;
	aBaloonProvider.Select(rdb_);
	res["default_paygateid"]=aBaloonProvider.default_paygateid;
	res["default_fiscalgateid"]=aBaloonProvider.default_fiscalgateid;
	res["code_conversion_algo"]=aBaloonProvider.code_conversion_algo;
	res["agent_commission"]=aBaloonProvider.agent_commission;
	res["ident_sale_price"]=aBaloonProvider.ident_sale_price;
	res["timeout_before"]=aBaloonProvider.timeout_before;
	res["sun"]=aBaloonProvider.sun;
	res["mon"]=aBaloonProvider.mon;
	res["tue"]=aBaloonProvider.tue;
	res["wed"]=aBaloonProvider.wed;
	res["thu"]=aBaloonProvider.thu;
	res["fri"]=aBaloonProvider.fri;
	res["sat"]=aBaloonProvider.sat;
	return res;
}

void BALOONExternalResource::initialize(ParamsMapRef params) {
	RegisterMethod("ExternalResourceListGet_API", (&BALOONExternalResource::__ExternalResourceListGet_API));
	RegisterMethod("Add", (&BALOONExternalResource::__Add));
	RegisterMethod("Get", (&BALOONExternalResource::__Get));
	RegisterMethod("Update", (&BALOONExternalResource::__Update));
	RegisterMethod("Delete", (&BALOONExternalResource::__Delete));
	RegisterMethod("ExternalResourceListGet", (&BALOONExternalResource::__ExternalResourceListGet));
	InitDatabase(params);
}

 const Value BALOONExternalResource::__ExternalResourceListGet_API(const Param& param) {
	return ExternalResourceListGet_API();
}

// Wrappers for automatic methods
const Value BALOONExternalResource::__Add(const Param& param) {
	return Add(param["name"], param["uri"], param["description"], param["enabled"]);
}

const Value BALOONExternalResource::__Update(const Param& param) {
	return Update(param["id"], param["name"], param["uri"], param["description"], param["enabled"]);
}

const Value BALOONExternalResource::__Delete(const Param& param) {
	return Delete(param["id"]);
}

const Value BALOONExternalResource::__Get(const Param& param) {
	return Get(param["id"]);
}

const Value BALOONExternalResource::__ExternalResourceListGet(const Param& param) {
	return ExternalResourceListGet();
}

// Implementation of automatic methods
const Value BALOONExternalResource::Add(const Str name, const Str uri, const Str description, const Str enabled) {
	Exception e((Message("Cannot add ") << Message("External Resource").What() << ". ").What());
	try {
		ExternalResource aExternalResource;
		aExternalResource.name=name;
		aExternalResource.uri=uri;
		aExternalResource.description=description;
		aExternalResource.enabled=enabled;
		Int sk=aExternalResource.Insert(rdb_);
		Value res;
		res["id"]=sk;
		AddMessage(Message()<<Message("External Resource").What()<<" "<<sk<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONExternalResource::Update(const Int id, const optional<Str> name, const optional<Str> uri, const optional<Str> description, const optional<Str> enabled) {
	Exception e((Message("Cannot update ") << Message("External Resource").What() << ". ").What());
	try {
		ExternalResource aExternalResource;
		aExternalResource.id=id;
		aExternalResource.Select(rdb_);
		if(Defined(name)) aExternalResource.name=*name;
		if(Defined(uri)) aExternalResource.uri=*uri;
		if(Defined(description)) aExternalResource.description=*description;
		if(Defined(enabled)) aExternalResource.enabled=*enabled;
		aExternalResource.Update(rdb_);
		AddMessage(Message()<<Message("External Resource").What()<<" "<<aExternalResource.id<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONExternalResource::Delete(const Int id) {
	ExternalResource aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("External Resource").What()<<". ").What());
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.id=id;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("External Resource").What()<<" "<<id<<" deleted. ");
	return Value();
}

const Value BALOONExternalResource::Get(const Int id) {
	Value res;
	ExternalResource aExternalResource;
	aExternalResource.id=id;
	aExternalResource.Select(rdb_);
	res["id"]=aExternalResource.id;
	res["name"]=aExternalResource.name;
	res["uri"]=aExternalResource.uri;
	res["description"]=aExternalResource.description;
	res["enabled"]=aExternalResource.enabled;
	return res;
}

const Value BALOONExternalResource::ExternalResourceListGet() {
	Data::DataList lr;
	lr.AddColumn("id", Data::INTEGER);
	lr.AddColumn("name", Data::STRING);
	lr.AddColumn("uri", Data::STRING);
	lr.AddColumn("description", Data::STRING);
	ExternalResource aExternalResource;
	lr.Bind(aExternalResource.id, "id");
	lr.Bind(aExternalResource.name, "name");
	lr.Bind(aExternalResource.uri, "uri");
	lr.Bind(aExternalResource.description, "description");
	Selector sel;
	sel << aExternalResource->id << aExternalResource->name << aExternalResource->uri << aExternalResource->description;
	sel.Where();
	DataSet data=sel.Execute(rdb_);
	while(data.Fetch()) lr.AddRow();
	Value res=lr;
	return res;
}

void BALOONAgent::initialize(ParamsMapRef params) {
	RegisterMethod("Register_API", (&BALOONAgent::__Register_API));
	RegisterMethod("Update", (&BALOONAgent::__Update));
	RegisterMethod("GetCurrent", (&BALOONAgent::__GetCurrent));
	RegisterMethod("Add", (&BALOONAgent::__Add));
	RegisterMethod("Get", (&BALOONAgent::__Get));
	RegisterMethod("Delete", (&BALOONAgent::__Delete));
	RegisterMethod("AgentListGet", (&BALOONAgent::__AgentListGet));
	InitDatabase(params);
}

 const Value BALOONAgent::__Register_API(const Param& param) {
	return Register_API(param["name"], param["phone"], param["email"], param["password"], param["password2"]);
}

 const Value BALOONAgent::__Update(const Param& param) {
	return Update(param["id"], param["name"], param["phone"], param["email"], param["comments"], param["commission"], param["is_default"]);
}

 const Value BALOONAgent::__GetCurrent(const Param& param) {
	return GetCurrent();
}

// Wrappers for automatic methods
const Value BALOONAgent::__Add(const Param& param) {
	return Add(param["userTokenID"], param["name"], param["phone"], param["email"], param["comments"], param["commission"], param["is_default"]);
}

const Value BALOONAgent::__Delete(const Param& param) {
	return Delete(param["id"]);
}

const Value BALOONAgent::__Get(const Param& param) {
	return Get(param["id"]);
}

const Value BALOONAgent::__AgentListGet(const Param& param) {
	return AgentListGet(param["userTokenID"]);
}

// Implementation of automatic methods
const Value BALOONAgent::Add(const Int64 userTokenID, const Str name, const Str phone, const Str email, const Str comments, const Double commission, const Str is_default) {
	Exception e((Message("Cannot add ") << Message("Agent").What() << ". ").What());
	bool error=false;
	error |= Defined(userTokenID)  && !__CheckIDExists(Users()->TokenID, *userTokenID, "User", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		Agent aAgent;
		aAgent.userTokenID=userTokenID;
		aAgent.name=name;
		aAgent.phone=phone;
		aAgent.email=email;
		aAgent.comments=comments;
		aAgent.commission=commission;
		aAgent.is_default=is_default;
		Int sk=aAgent.Insert(rdb_);
		Value res;
		res["id"]=sk;
		AddMessage(Message()<<Message("Agent").What()<<" "<<sk<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

#if 0 //BALOONAgent Update
const Value BALOONAgent::Update(const Int id, const optional<Int64> userTokenID, const optional<Str> name, const optional<Str> phone, const optional<Str> email, const optional<Str> comments, const optional<Double> commission, const optional<Str> is_default) {
	Exception e((Message("Cannot update ") << Message("Agent").What() << ". ").What());
	bool error=false;
	error |= Defined(userTokenID) && Defined(*userTokenID)  && !__CheckIDExists(Users()->TokenID, *userTokenID, "User", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		Agent aAgent;
		aAgent.id=id;
		aAgent.Select(rdb_);
		if(Defined(userTokenID)) aAgent.userTokenID=*userTokenID;
		if(Defined(name)) aAgent.name=*name;
		if(Defined(phone)) aAgent.phone=*phone;
		if(Defined(email)) aAgent.email=*email;
		if(Defined(comments)) aAgent.comments=*comments;
		if(Defined(commission)) aAgent.commission=*commission;
		if(Defined(is_default)) aAgent.is_default=*is_default;
		aAgent.Update(rdb_);
		AddMessage(Message()<<Message("Agent").What()<<" "<<aAgent.id<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

#endif //BALOONAgent Update

const Value BALOONAgent::Delete(const Int id) {
	Agent aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Agent").What()<<". ").What());
	//check AgentOrder reference to Agent reference 
	error |= !__CheckIDNotExists(AgentOrder()->agentid, id, "Fillup Order", e, rdb_);
	//check Transaction reference to Agent reference 
	error |= !__CheckIDNotExists(Transaction()->agentid, id, "Payment", e, rdb_);
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.id=id;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Agent").What()<<" "<<id<<" deleted. ");
	return Value();
}

const Value BALOONAgent::Get(const Int id) {
	Value res;
	Agent aAgent;
	aAgent.id=id;
	aAgent.Select(rdb_);
	res["id"]=aAgent.id;
	res["userTokenID"]=aAgent.userTokenID;
	res["name"]=aAgent.name;
	res["phone"]=aAgent.phone;
	res["email"]=aAgent.email;
	res["comments"]=aAgent.comments;
	res["commission"]=aAgent.commission;
	res["is_default"]=aAgent.is_default;
	return res;
}

const Value BALOONAgent::AgentListGet(const optional<Int64> userTokenID) {
	Data::DataList lr;
	lr.AddColumn("id", Data::INTEGER);
	lr.AddColumn("name", Data::STRING);
	lr.AddColumn("phone", Data::STRING);
	lr.AddColumn("email", Data::STRING);
	lr.AddColumn("commission", Data::DOUBLE);
	lr.AddColumn("comments", Data::STRING);
	lr.AddColumn("is_default", Data::STRING, BOOLValues());
	Agent aAgent;
	lr.Bind(aAgent.id, "id");
	lr.Bind(aAgent.name, "name");
	lr.Bind(aAgent.phone, "phone");
	lr.Bind(aAgent.email, "email");
	lr.Bind(aAgent.commission, "commission");
	lr.Bind(aAgent.comments, "comments");
	lr.Bind(aAgent.is_default, "is_default");
	Selector sel;
	sel << aAgent->id << aAgent->name << aAgent->phone << aAgent->email << aAgent->commission << aAgent->comments << aAgent->is_default;
	sel.Where( (Defined(userTokenID) ? aAgent->userTokenID==*userTokenID : Expression()));
	DataSet data=sel.Execute(rdb_);
	while(data.Fetch()) lr.AddRow();
	Value res=lr;
	return res;
}

void BALOONAgentOrder::initialize(ParamsMapRef params) {
	RegisterMethod("AgentOrderListGet", (&BALOONAgentOrder::__AgentOrderListGet));
	RegisterMethod("Add", (&BALOONAgentOrder::__Add));
	RegisterMethod("Update", (&BALOONAgentOrder::__Update));
	RegisterMethod("CurrentAgentOrderListGet", (&BALOONAgentOrder::__CurrentAgentOrderListGet));
	RegisterMethod("CurrentAgentOrderListGet_API", (&BALOONAgentOrder::__CurrentAgentOrderListGet_API));
	RegisterMethod("CurrentGet", (&BALOONAgentOrder::__CurrentGet));
	RegisterMethod("CurrentGet_API", (&BALOONAgentOrder::__CurrentGet_API));
	RegisterMethod("CurrentSyncAndGet_API", (&BALOONAgentOrder::__CurrentSyncAndGet_API));
	RegisterMethod("CurrentAdd", (&BALOONAgentOrder::__CurrentAdd));
	RegisterMethod("CurrentAdd_API", (&BALOONAgentOrder::__CurrentAdd_API));
	RegisterMethod("OnlineCurrentAdd_API", (&BALOONAgentOrder::__OnlineCurrentAdd_API));
	RegisterMethod("CurrentUpdate", (&BALOONAgentOrder::__CurrentUpdate));
	RegisterMethod("GetType", (&BALOONAgentOrder::__GetType));
	RegisterMethod("Get", (&BALOONAgentOrder::__Get));
	RegisterMethod("Delete", (&BALOONAgentOrder::__Delete));
	InitDatabase(params);
}

 const Value BALOONAgentOrder::__AgentOrderListGet(const Param& param) {
	return AgentOrderListGet(param["agentid"], param["first_day"], param["last_day"]);
}

 const Value BALOONAgentOrder::__Add(const Param& param) {
	return Add(param["agentid"], param["code"], param["amount"], param["comment"]);
}

 const Value BALOONAgentOrder::__Update(const Param& param) {
	return Update(param["id"], param["comment"]);
}

 const Value BALOONAgentOrder::__CurrentAgentOrderListGet(const Param& param) {
	return CurrentAgentOrderListGet();
}

 const Value BALOONAgentOrder::__CurrentAgentOrderListGet_API(const Param& param) {
	return CurrentAgentOrderListGet_API(param["id"]);
}

 const Value BALOONAgentOrder::__CurrentGet(const Param& param) {
	return CurrentGet(param["id"]);
}

 const Value BALOONAgentOrder::__CurrentGet_API(const Param& param) {
	return CurrentGet_API(param["id"]);
}

 const Value BALOONAgentOrder::__CurrentSyncAndGet_API(const Param& param) {
	return CurrentSyncAndGet_API(param["id"]);
}

 const Value BALOONAgentOrder::__CurrentAdd(const Param& param) {
	return CurrentAdd(param["code"], param["amount"], param["comment"]);
}

 const Value BALOONAgentOrder::__CurrentAdd_API(const Param& param) {
	return CurrentAdd_API(param["code"], param["amount"], param["comment"]);
}

 const Value BALOONAgentOrder::__OnlineCurrentAdd_API(const Param& param) {
	return OnlineCurrentAdd_API(param["code"], param["amount"], param["comment"], param["success_url"], param["fail_url"]);
}

 const Value BALOONAgentOrder::__CurrentUpdate(const Param& param) {
	return CurrentUpdate(param["id"], param["comment"]);
}

 const Value BALOONAgentOrder::__GetType(const Param& param) {
	return GetType(param["id"]);
}

// Wrappers for automatic methods
const Value BALOONAgentOrder::__Delete(const Param& param) {
	return Delete(param["id"]);
}

const Value BALOONAgentOrder::__Get(const Param& param) {
	return Get(param["id"]);
}

// Implementation of automatic methods
const Value BALOONAgentOrder::GetType(const Int id) {
	AgentOrder rowSelector;
	rowSelector.id=id;
	rowSelector.Select(rdb_);
	Value res;
	if(Undefined(rowSelector.AgentOrderType)) throw Exception("Unknown type. ");	res["ItemRef"]=*rowSelector.AgentOrderType;
	return res;
}

#if 0 //BALOONAgentOrder Add
const Value BALOONAgentOrder::Add(const Int agentid, const Str code, const Decimal amount, const Decimal commission, const Time filed_at, const Str comment, const Str status, const Str AgentOrderType) {
	Exception e((Message("Cannot add ") << Message("Fillup Order").What() << ". ").What());
	bool error=false;
	error |= Defined(agentid)  && !__CheckIDExists(Agent()->id, *agentid, "Agent", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		AgentOrder aAgentOrder;
		aAgentOrder.agentid=agentid;
		aAgentOrder.code=code;
		aAgentOrder.amount=amount;
		aAgentOrder.commission=commission;
		aAgentOrder.filed_at=filed_at;
		aAgentOrder.comment=comment;
		aAgentOrder.status=status;
		aAgentOrder.AgentOrderType="AgentOrder";
		Int sk=aAgentOrder.Insert(rdb_);
		Value res;
		res["id"]=sk;
		AddMessage(Message()<<Message("Fillup Order").What()<<" "<<sk<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

#endif //BALOONAgentOrder Add

#if 0 //BALOONAgentOrder Update
const Value BALOONAgentOrder::Update(const Int id, const optional<Int> agentid, const optional<Str> code, const optional<Decimal> amount, const optional<Decimal> commission, const optional<Time> filed_at, const optional<Str> comment, const optional<Str> status, const optional<Str> AgentOrderType) {
	Exception e((Message("Cannot update ") << Message("Fillup Order").What() << ". ").What());
	bool error=false;
	error |= Defined(agentid) && Defined(*agentid)  && !__CheckIDExists(Agent()->id, *agentid, "Agent", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		AgentOrder aAgentOrder;
		aAgentOrder.id=id;
		aAgentOrder.Select(rdb_);
		if(Defined(agentid)) aAgentOrder.agentid=*agentid;
		if(Defined(code)) aAgentOrder.code=*code;
		if(Defined(amount)) aAgentOrder.amount=*amount;
		if(Defined(commission)) aAgentOrder.commission=*commission;
		if(Defined(filed_at)) aAgentOrder.filed_at=*filed_at;
		if(Defined(comment)) aAgentOrder.comment=*comment;
		if(Defined(status)) aAgentOrder.status=*status;
		if(Defined(AgentOrderType)) aAgentOrder.AgentOrderType=*AgentOrderType;
		aAgentOrder.Update(rdb_);
		AddMessage(Message()<<Message("Fillup Order").What()<<" "<<aAgentOrder.id<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

#endif //BALOONAgentOrder Update

const Value BALOONAgentOrder::Delete(const Int id) {
	AgentOrder aRecord;
	bool error=false;
	Exception e;
	Str recordType;
	Message successMessage=Message();
	Selector sel;
	sel.Where( aRecord->id==id) << aRecord->AgentOrderType.Bind(recordType);
	if(!(sel.Execute(rdb_).Fetch()) || IsNull(recordType)) {
		recordType="AgentOrder";
	};
	if(IsNull(recordType) || (*recordType=="AgentOrder")){
		e << (Message("Cannot delete ") << Message("Fillup Order").What() << ". ").What();
		successMessage << Message("Fillup Order").What();
	//check AgentInvoice reference to AgentOrder reference 
	error |= !__CheckIDNotExists(AgentInvoice()->orderid, id, "Invoice", e, rdb_);
	}
	else if(*recordType =="IdentifierAgentOrder") {
		e << (Message("Cannot delete ") << Message("Identifier Order").What() << ". ").What();
		successMessage << Message("Identifier Order").What();
	//check AgentInvoice reference to AgentOrder reference 
	error |= !__CheckIDNotExists(AgentInvoice()->orderid, id, "Invoice", e, rdb_);
	}
	else if(*recordType =="PackageAgentOrder") {
		e << (Message("Cannot delete ") << Message("Package Order").What() << ". ").What();
		successMessage << Message("Package Order").What();
	//check AgentInvoice reference to AgentOrder reference 
	error |= !__CheckIDNotExists(AgentInvoice()->orderid, id, "Invoice", e, rdb_);
	}
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.id=id;
	aRecord.Delete(rdb_);
	AddMessage( successMessage<<" "<<id<<" deleted. ");
	return Value();
}

const Value BALOONAgentOrder::Get(const Int id) {
	Value res;
	AgentOrder aAgentOrder;
	aAgentOrder.id=id;
	aAgentOrder.Select(rdb_);
	res["id"]=aAgentOrder.id;
	res["agentid"]=aAgentOrder.agentid;
	res["code"]=aAgentOrder.code;
	res["amount"]=aAgentOrder.amount;
	res["commission"]=aAgentOrder.commission;
	res["filed_at"]=aAgentOrder.filed_at;
	res["comment"]=aAgentOrder.comment;
	res["status"]=aAgentOrder.status;
	res["AgentOrderType"]=aAgentOrder.AgentOrderType;
	return res;
}

void BALOONIdentifierAgentOrder::initialize(ParamsMapRef params) {
	RegisterMethod("Add", (&BALOONIdentifierAgentOrder::__Add));
	RegisterMethod("Update", (&BALOONIdentifierAgentOrder::__Update));
	RegisterMethod("CurrentGet", (&BALOONIdentifierAgentOrder::__CurrentGet));
	RegisterMethod("CurrentAdd", (&BALOONIdentifierAgentOrder::__CurrentAdd));
	RegisterMethod("CurrentAdd_API", (&BALOONIdentifierAgentOrder::__CurrentAdd_API));
	RegisterMethod("CurrentUpdate", (&BALOONIdentifierAgentOrder::__CurrentUpdate));
	RegisterMethod("Get", (&BALOONIdentifierAgentOrder::__Get));
	RegisterMethod("Delete", (&BALOONIdentifierAgentOrder::__Delete));
	RegisterMethod("IdentifierAgentOrderListGet", (&BALOONIdentifierAgentOrder::__IdentifierAgentOrderListGet));
	InitDatabase(params);
}

 const Value BALOONIdentifierAgentOrder::__Add(const Param& param) {
	return Add(param["agentid"], param["code"], param["amount"], param["comment"], param["valid_from"], param["valid_to"], param["permanent_rulename"], param["categoryname"], param["ratename"], param["other_card_code"]);
}

 const Value BALOONIdentifierAgentOrder::__Update(const Param& param) {
	return Update(param["id"], param["comment"]);
}

 const Value BALOONIdentifierAgentOrder::__CurrentGet(const Param& param) {
	return CurrentGet(param["id"]);
}

 const Value BALOONIdentifierAgentOrder::__CurrentAdd(const Param& param) {
	return CurrentAdd(param["code"], param["amount"], param["comment"], param["valid_from"], param["valid_to"], param["permanent_rulename"], param["categoryname"], param["ratename"], param["other_card_code"]);
}

 const Value BALOONIdentifierAgentOrder::__CurrentAdd_API(const Param& param) {
	return CurrentAdd_API(param["code"], param["amount"], param["comment"], param["valid_from"], param["valid_to"], param["permanent_rulename"], param["categoryname"], param["ratename"], param["other_card_code"]);
}

 const Value BALOONIdentifierAgentOrder::__CurrentUpdate(const Param& param) {
	return CurrentUpdate(param["id"], param["comment"]);
}

// Wrappers for automatic methods
const Value BALOONIdentifierAgentOrder::__Delete(const Param& param) {
	return Delete(param["id"]);
}

const Value BALOONIdentifierAgentOrder::__Get(const Param& param) {
	return Get(param["id"]);
}

const Value BALOONIdentifierAgentOrder::__IdentifierAgentOrderListGet(const Param& param) {
	return IdentifierAgentOrderListGet(param["permanent_rulename"], param["categoryname"], param["ratename"]);
}

// Implementation of automatic methods
#if 0 //BALOONIdentifierAgentOrder Add
const Value BALOONIdentifierAgentOrder::Add(const Int agentid, const Str code, const Decimal amount, const Decimal commission, const Time filed_at, const Str comment, const Str status, const Str AgentOrderType, const Time valid_from, const Time valid_to, const Str permanent_rulename, const Str categoryname, const Str ratename, const Str other_card_code) {
	Exception e((Message("Cannot add ") << Message("Identifier Order").What() << ". ").What());
	bool error=false;
	error |= Defined(agentid)  && !__CheckIDExists(Agent()->id, *agentid, "Agent", e, rdb_);
	error |= Defined(permanent_rulename)  && !__CheckIDExists(IdentifierRule()->name, *permanent_rulename, "Rule to use", e, rdb_);
	error |= Defined(categoryname)  && !__CheckIDExists(IdentifierCategory()->name, *categoryname, "Category", e, rdb_);
	error |= Defined(ratename)  && !__CheckIDExists(IdentifierRate()->name, *ratename, "Rate", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		AgentOrder aAgentOrder;
		aAgentOrder.agentid=agentid;
		aAgentOrder.code=code;
		aAgentOrder.amount=amount;
		aAgentOrder.commission=commission;
		aAgentOrder.filed_at=filed_at;
		aAgentOrder.comment=comment;
		aAgentOrder.status=status;
		aAgentOrder.AgentOrderType="IdentifierAgentOrder";
		Int sk=aAgentOrder.Insert(rdb_);
		IdentifierAgentOrder aIdentifierAgentOrder;
		aIdentifierAgentOrder.valid_from=valid_from;
		aIdentifierAgentOrder.valid_to=valid_to;
		aIdentifierAgentOrder.permanent_rulename=permanent_rulename;
		aIdentifierAgentOrder.categoryname=categoryname;
		aIdentifierAgentOrder.ratename=ratename;
		aIdentifierAgentOrder.other_card_code=other_card_code;
		aIdentifierAgentOrder.id=sk;
	aIdentifierAgentOrder.Insert(rdb_);
		Value res;
		res["id"]=sk;
		AddMessage(Message()<<Message("Identifier Order").What()<<" "<<sk<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

#endif //BALOONIdentifierAgentOrder Add

#if 0 //BALOONIdentifierAgentOrder Update
const Value BALOONIdentifierAgentOrder::Update(const Int id, const optional<Int> agentid, const optional<Str> code, const optional<Decimal> amount, const optional<Decimal> commission, const optional<Time> filed_at, const optional<Str> comment, const optional<Str> status, const optional<Str> AgentOrderType, const optional<Time> valid_from, const optional<Time> valid_to, const optional<Str> permanent_rulename, const optional<Str> categoryname, const optional<Str> ratename, const optional<Str> other_card_code) {
	Exception e((Message("Cannot update ") << Message("Identifier Order").What() << ". ").What());
	bool error=false;
	error |= Defined(agentid) && Defined(*agentid)  && !__CheckIDExists(Agent()->id, *agentid, "Agent", e, rdb_);
	error |= Defined(permanent_rulename) && Defined(*permanent_rulename)  && !__CheckIDExists(IdentifierRule()->name, *permanent_rulename, "Rule to use", e, rdb_);
	error |= Defined(categoryname) && Defined(*categoryname)  && !__CheckIDExists(IdentifierCategory()->name, *categoryname, "Category", e, rdb_);
	error |= Defined(ratename) && Defined(*ratename)  && !__CheckIDExists(IdentifierRate()->name, *ratename, "Rate", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		AgentOrder aAgentOrder;
		aAgentOrder.id=id;
		aAgentOrder.Select(rdb_);
		if(Defined(agentid)) aAgentOrder.agentid=*agentid;
		if(Defined(code)) aAgentOrder.code=*code;
		if(Defined(amount)) aAgentOrder.amount=*amount;
		if(Defined(commission)) aAgentOrder.commission=*commission;
		if(Defined(filed_at)) aAgentOrder.filed_at=*filed_at;
		if(Defined(comment)) aAgentOrder.comment=*comment;
		if(Defined(status)) aAgentOrder.status=*status;
		if(Defined(AgentOrderType)) aAgentOrder.AgentOrderType=*AgentOrderType;
		aAgentOrder.Update(rdb_);
		IdentifierAgentOrder aIdentifierAgentOrder;
		aIdentifierAgentOrder.id=id;
		aIdentifierAgentOrder.Select(rdb_);
		if(Defined(valid_from)) aIdentifierAgentOrder.valid_from=*valid_from;
		if(Defined(valid_to)) aIdentifierAgentOrder.valid_to=*valid_to;
		if(Defined(permanent_rulename)) aIdentifierAgentOrder.permanent_rulename=*permanent_rulename;
		if(Defined(categoryname)) aIdentifierAgentOrder.categoryname=*categoryname;
		if(Defined(ratename)) aIdentifierAgentOrder.ratename=*ratename;
		if(Defined(other_card_code)) aIdentifierAgentOrder.other_card_code=*other_card_code;
		aIdentifierAgentOrder.Update(rdb_);
		AddMessage(Message()<<Message("Identifier Order").What()<<" "<<aIdentifierAgentOrder.id<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

#endif //BALOONIdentifierAgentOrder Update

const Value BALOONIdentifierAgentOrder::Delete(const Int id) {
	BALOONAgentOrder::Delete( id);
	return Value();
}

const Value BALOONIdentifierAgentOrder::Get(const Int id) {
	Value res;
	AgentOrder aAgentOrder;
	aAgentOrder.id=id;
	aAgentOrder.Select(rdb_);
	res["id"]=aAgentOrder.id;
	res["agentid"]=aAgentOrder.agentid;
	res["code"]=aAgentOrder.code;
	res["amount"]=aAgentOrder.amount;
	res["commission"]=aAgentOrder.commission;
	res["filed_at"]=aAgentOrder.filed_at;
	res["comment"]=aAgentOrder.comment;
	res["status"]=aAgentOrder.status;
	res["AgentOrderType"]=aAgentOrder.AgentOrderType;
	IdentifierAgentOrder aIdentifierAgentOrder;
	aIdentifierAgentOrder.id=id;
	aIdentifierAgentOrder.Select(rdb_);
	res["valid_from"]=aIdentifierAgentOrder.valid_from;
	res["valid_to"]=aIdentifierAgentOrder.valid_to;
	res["permanent_rulename"]=aIdentifierAgentOrder.permanent_rulename;
	res["categoryname"]=aIdentifierAgentOrder.categoryname;
	res["ratename"]=aIdentifierAgentOrder.ratename;
	res["other_card_code"]=aIdentifierAgentOrder.other_card_code;
	return res;
}

const Value BALOONIdentifierAgentOrder::IdentifierAgentOrderListGet(const optional<Str> permanent_rulename, const optional<Str> categoryname, const optional<Str> ratename) {
	Data::DataList lr;
	lr.AddColumn("id", Data::INTEGER);
	lr.AddColumn("agentid", Data::INTEGER);
	lr.AddColumn("agentname", Data::STRING);
	lr.AddColumn("code", Data::STRING);
	lr.AddColumn("amount", Data::DECIMAL);
	lr.AddColumn("filed_at", Data::DATETIME);
	lr.AddColumn("valid_from", Data::DATETIME);
	lr.AddColumn("valid_to", Data::DATETIME);
	lr.AddColumn("permanent_rulename", Data::STRING);
	lr.AddColumn("categoryname", Data::STRING);
	lr.AddColumn("ratename", Data::STRING);
	lr.AddColumn("other_card_code", Data::STRING);
	lr.AddColumn("comment", Data::STRING);
	IdentifierAgentOrder aIdentifierAgentOrder;
	AgentOrder aAgentOrder;
	Agent aagent;
	lr.Bind(aIdentifierAgentOrder.id, "id");
	lr.Bind(aAgentOrder.agentid, "agentid");
	lr.Bind(aagent.name, "agentname");
	lr.Bind(aAgentOrder.code, "code");
	lr.Bind(aAgentOrder.amount, "amount");
	lr.Bind(aAgentOrder.filed_at, "filed_at");
	lr.Bind(aIdentifierAgentOrder.valid_from, "valid_from");
	lr.Bind(aIdentifierAgentOrder.valid_to, "valid_to");
	lr.Bind(aIdentifierAgentOrder.permanent_rulename, "permanent_rulename");
	lr.Bind(aIdentifierAgentOrder.categoryname, "categoryname");
	lr.Bind(aIdentifierAgentOrder.ratename, "ratename");
	lr.Bind(aIdentifierAgentOrder.other_card_code, "other_card_code");
	lr.Bind(aAgentOrder.comment, "comment");
	Selector sel;
	sel << aIdentifierAgentOrder->id << aAgentOrder->agentid << aagent->name << aAgentOrder->code << aAgentOrder->amount << aAgentOrder->filed_at << aIdentifierAgentOrder->valid_from << aIdentifierAgentOrder->valid_to << aIdentifierAgentOrder->permanent_rulename << aIdentifierAgentOrder->categoryname << aIdentifierAgentOrder->ratename << aIdentifierAgentOrder->other_card_code << aAgentOrder->comment;
	sel.Join(aAgentOrder).On(aIdentifierAgentOrder->id==aAgentOrder->id).Join(aagent).On(aAgentOrder->agentid==aagent->id);
	sel.Where( (Defined(permanent_rulename) ? aIdentifierAgentOrder->permanent_rulename==*permanent_rulename : Expression()) &&  (Defined(categoryname) ? aIdentifierAgentOrder->categoryname==*categoryname : Expression()) &&  (Defined(ratename) ? aIdentifierAgentOrder->ratename==*ratename : Expression()));
	DataSet data=sel.Execute(rdb_);
	while(data.Fetch()) lr.AddRow();
	Value res=lr;
	return res;
}

void BALOONPackageAgentOrder::initialize(ParamsMapRef params) {
	RegisterMethod("Add", (&BALOONPackageAgentOrder::__Add));
	RegisterMethod("Update", (&BALOONPackageAgentOrder::__Update));
	RegisterMethod("CurrentGet", (&BALOONPackageAgentOrder::__CurrentGet));
	RegisterMethod("CurrentAdd", (&BALOONPackageAgentOrder::__CurrentAdd));
	RegisterMethod("CurrentAdd_API", (&BALOONPackageAgentOrder::__CurrentAdd_API));
	RegisterMethod("OnlineCurrentAdd_API", (&BALOONPackageAgentOrder::__OnlineCurrentAdd_API));
	RegisterMethod("CurrentUpdate", (&BALOONPackageAgentOrder::__CurrentUpdate));
	RegisterMethod("Get", (&BALOONPackageAgentOrder::__Get));
	RegisterMethod("Delete", (&BALOONPackageAgentOrder::__Delete));
	RegisterMethod("PackageAgentOrderListGet", (&BALOONPackageAgentOrder::__PackageAgentOrderListGet));
	InitDatabase(params);
}

 const Value BALOONPackageAgentOrder::__Add(const Param& param) {
	return Add(param["agentid"], param["code"], param["service_rulename"], param["comment"]);
}

 const Value BALOONPackageAgentOrder::__Update(const Param& param) {
	return Update(param["id"], param["comment"]);
}

 const Value BALOONPackageAgentOrder::__CurrentGet(const Param& param) {
	return CurrentGet(param["id"]);
}

 const Value BALOONPackageAgentOrder::__CurrentAdd(const Param& param) {
	return CurrentAdd(param["code"], param["service_rulename"], param["comment"]);
}

 const Value BALOONPackageAgentOrder::__CurrentAdd_API(const Param& param) {
	return CurrentAdd_API(param["code"], param["service_rulename"], param["comment"]);
}

 const Value BALOONPackageAgentOrder::__OnlineCurrentAdd_API(const Param& param) {
	return OnlineCurrentAdd_API(param["code"], param["service_rulename"], param["comment"], param["success_url"], param["fail_url"]);
}

 const Value BALOONPackageAgentOrder::__CurrentUpdate(const Param& param) {
	return CurrentUpdate(param["id"], param["comment"]);
}

// Wrappers for automatic methods
const Value BALOONPackageAgentOrder::__Delete(const Param& param) {
	return Delete(param["id"]);
}

const Value BALOONPackageAgentOrder::__Get(const Param& param) {
	return Get(param["id"]);
}

const Value BALOONPackageAgentOrder::__PackageAgentOrderListGet(const Param& param) {
	return PackageAgentOrderListGet(param["service_rulename"]);
}

// Implementation of automatic methods
#if 0 //BALOONPackageAgentOrder Add
const Value BALOONPackageAgentOrder::Add(const Int agentid, const Str code, const Decimal amount, const Decimal commission, const Time filed_at, const Str comment, const Str status, const Str AgentOrderType, const Str service_rulename) {
	Exception e((Message("Cannot add ") << Message("Package Order").What() << ". ").What());
	bool error=false;
	error |= Defined(agentid)  && !__CheckIDExists(Agent()->id, *agentid, "Agent", e, rdb_);
	error |= Defined(service_rulename)  && !__CheckIDExists(SpdServiceRule()->name, *service_rulename, "Правило оформления", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		AgentOrder aAgentOrder;
		aAgentOrder.agentid=agentid;
		aAgentOrder.code=code;
		aAgentOrder.amount=amount;
		aAgentOrder.commission=commission;
		aAgentOrder.filed_at=filed_at;
		aAgentOrder.comment=comment;
		aAgentOrder.status=status;
		aAgentOrder.AgentOrderType="PackageAgentOrder";
		Int sk=aAgentOrder.Insert(rdb_);
		PackageAgentOrder aPackageAgentOrder;
		aPackageAgentOrder.service_rulename=service_rulename;
		aPackageAgentOrder.id=sk;
	aPackageAgentOrder.Insert(rdb_);
		Value res;
		res["id"]=sk;
		AddMessage(Message()<<Message("Package Order").What()<<" "<<sk<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

#endif //BALOONPackageAgentOrder Add

#if 0 //BALOONPackageAgentOrder Update
const Value BALOONPackageAgentOrder::Update(const Int id, const optional<Int> agentid, const optional<Str> code, const optional<Decimal> amount, const optional<Decimal> commission, const optional<Time> filed_at, const optional<Str> comment, const optional<Str> status, const optional<Str> AgentOrderType, const optional<Str> service_rulename) {
	Exception e((Message("Cannot update ") << Message("Package Order").What() << ". ").What());
	bool error=false;
	error |= Defined(agentid) && Defined(*agentid)  && !__CheckIDExists(Agent()->id, *agentid, "Agent", e, rdb_);
	error |= Defined(service_rulename) && Defined(*service_rulename)  && !__CheckIDExists(SpdServiceRule()->name, *service_rulename, "Правило оформления", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		AgentOrder aAgentOrder;
		aAgentOrder.id=id;
		aAgentOrder.Select(rdb_);
		if(Defined(agentid)) aAgentOrder.agentid=*agentid;
		if(Defined(code)) aAgentOrder.code=*code;
		if(Defined(amount)) aAgentOrder.amount=*amount;
		if(Defined(commission)) aAgentOrder.commission=*commission;
		if(Defined(filed_at)) aAgentOrder.filed_at=*filed_at;
		if(Defined(comment)) aAgentOrder.comment=*comment;
		if(Defined(status)) aAgentOrder.status=*status;
		if(Defined(AgentOrderType)) aAgentOrder.AgentOrderType=*AgentOrderType;
		aAgentOrder.Update(rdb_);
		PackageAgentOrder aPackageAgentOrder;
		aPackageAgentOrder.id=id;
		aPackageAgentOrder.Select(rdb_);
		if(Defined(service_rulename)) aPackageAgentOrder.service_rulename=*service_rulename;
		aPackageAgentOrder.Update(rdb_);
		AddMessage(Message()<<Message("Package Order").What()<<" "<<aPackageAgentOrder.id<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

#endif //BALOONPackageAgentOrder Update

const Value BALOONPackageAgentOrder::Delete(const Int id) {
	BALOONAgentOrder::Delete( id);
	return Value();
}

const Value BALOONPackageAgentOrder::Get(const Int id) {
	Value res;
	AgentOrder aAgentOrder;
	aAgentOrder.id=id;
	aAgentOrder.Select(rdb_);
	res["id"]=aAgentOrder.id;
	res["agentid"]=aAgentOrder.agentid;
	res["code"]=aAgentOrder.code;
	res["amount"]=aAgentOrder.amount;
	res["commission"]=aAgentOrder.commission;
	res["filed_at"]=aAgentOrder.filed_at;
	res["comment"]=aAgentOrder.comment;
	res["status"]=aAgentOrder.status;
	res["AgentOrderType"]=aAgentOrder.AgentOrderType;
	PackageAgentOrder aPackageAgentOrder;
	aPackageAgentOrder.id=id;
	aPackageAgentOrder.Select(rdb_);
	res["service_rulename"]=aPackageAgentOrder.service_rulename;
	return res;
}

const Value BALOONPackageAgentOrder::PackageAgentOrderListGet(const optional<Str> service_rulename) {
	Data::DataList lr;
	lr.AddColumn("id", Data::INTEGER);
	lr.AddColumn("agentid", Data::INTEGER);
	lr.AddColumn("agentname", Data::STRING);
	lr.AddColumn("code", Data::STRING);
	lr.AddColumn("amount", Data::DECIMAL);
	lr.AddColumn("filed_at", Data::DATETIME);
	lr.AddColumn("service_rulename", Data::STRING);
	lr.AddColumn("comment", Data::STRING);
	PackageAgentOrder aPackageAgentOrder;
	AgentOrder aAgentOrder;
	Agent aagent;
	lr.Bind(aPackageAgentOrder.id, "id");
	lr.Bind(aAgentOrder.agentid, "agentid");
	lr.Bind(aagent.name, "agentname");
	lr.Bind(aAgentOrder.code, "code");
	lr.Bind(aAgentOrder.amount, "amount");
	lr.Bind(aAgentOrder.filed_at, "filed_at");
	lr.Bind(aPackageAgentOrder.service_rulename, "service_rulename");
	lr.Bind(aAgentOrder.comment, "comment");
	Selector sel;
	sel << aPackageAgentOrder->id << aAgentOrder->agentid << aagent->name << aAgentOrder->code << aAgentOrder->amount << aAgentOrder->filed_at << aPackageAgentOrder->service_rulename << aAgentOrder->comment;
	sel.Join(aAgentOrder).On(aPackageAgentOrder->id==aAgentOrder->id).Join(aagent).On(aAgentOrder->agentid==aagent->id);
	sel.Where( (Defined(service_rulename) ? aPackageAgentOrder->service_rulename==*service_rulename : Expression()));
	DataSet data=sel.Execute(rdb_);
	while(data.Fetch()) lr.AddRow();
	Value res=lr;
	return res;
}

void BALOONAgentInvoice::initialize(ParamsMapRef params) {
	RegisterMethod("SyncSpd", (&BALOONAgentInvoice::__SyncSpd));
	RegisterMethod("StatusUpdated", (&BALOONAgentInvoice::__StatusUpdated));
	RegisterMethod("RaiseInvoice", (&BALOONAgentInvoice::__RaiseInvoice));
	RegisterMethod("Add", (&BALOONAgentInvoice::__Add));
	RegisterMethod("Get", (&BALOONAgentInvoice::__Get));
	RegisterMethod("Update", (&BALOONAgentInvoice::__Update));
	RegisterMethod("Delete", (&BALOONAgentInvoice::__Delete));
	RegisterMethod("AgentInvoiceListGet", (&BALOONAgentInvoice::__AgentInvoiceListGet));
	InitDatabase(params);
}

 const Value BALOONAgentInvoice::__SyncSpd(const Param& param) {
	return SyncSpd(param["id"]);
}

 const Value BALOONAgentInvoice::__StatusUpdated(const Param& param) {
	return StatusUpdated(param["id"], param["barcode"], param["status"], param["amount"]);
}

 const Value BALOONAgentInvoice::__RaiseInvoice(const Param& param) {
	return RaiseInvoice(param["paygateid"], param["orderid"], param["amount"], param["success_url"], param["fail_url"]);
}

// Wrappers for automatic methods
const Value BALOONAgentInvoice::__Add(const Param& param) {
	return Add(param["id"], param["orderid"], param["eid"]);
}

const Value BALOONAgentInvoice::__Update(const Param& param) {
	return Update(param["id"], param["orderid"], param["eid"]);
}

const Value BALOONAgentInvoice::__Delete(const Param& param) {
	return Delete(param["id"]);
}

const Value BALOONAgentInvoice::__Get(const Param& param) {
	return Get(param["id"]);
}

const Value BALOONAgentInvoice::__AgentInvoiceListGet(const Param& param) {
	return AgentInvoiceListGet(param["id"], param["orderid"]);
}

// Implementation of automatic methods
const Value BALOONAgentInvoice::Add(const Int id, const Int orderid, const Str eid) {
	Exception e((Message("Cannot add ") << Message("Invoice").What() << ". ").What());
	bool error=false;
	error |= Defined(id)  && !__CheckIDExists(Invoice()->id, *id, "Invoice ID", e, rdb_);
	error |= Defined(orderid)  && !__CheckIDExists(AgentOrder()->id, *orderid, "Agent Order", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		AgentInvoice aAgentInvoice;
		aAgentInvoice.id=id;
		aAgentInvoice.orderid=orderid;
		aAgentInvoice.eid=eid;
	aAgentInvoice.Insert(rdb_);
		Value res;
		AddMessage(Message()<<Message("Invoice").What()<<" "<<aAgentInvoice.id<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONAgentInvoice::Update(const Int id, const optional<Int> orderid, const optional<Str> eid) {
	Exception e((Message("Cannot update ") << Message("Invoice").What() << ". ").What());
	bool error=false;
	error |= Defined(id)  && !__CheckIDExists(Invoice()->id, *id, "Invoice ID", e, rdb_);
	error |= Defined(orderid) && Defined(*orderid)  && !__CheckIDExists(AgentOrder()->id, *orderid, "Agent Order", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		AgentInvoice aAgentInvoice;
		aAgentInvoice.id=id;
		aAgentInvoice.Select(rdb_);
		if(Defined(orderid)) aAgentInvoice.orderid=*orderid;
		if(Defined(eid)) aAgentInvoice.eid=*eid;
		aAgentInvoice.Update(rdb_);
		AddMessage(Message()<<Message("Invoice").What()<<" "<<aAgentInvoice.id<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONAgentInvoice::Delete(const Int id) {
	AgentInvoice aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Invoice").What()<<". ").What());
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.id=id;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Invoice").What()<<" "<<id<<" deleted. ");
	return Value();
}

const Value BALOONAgentInvoice::Get(const Int id) {
	Value res;
	AgentInvoice aAgentInvoice;
	aAgentInvoice.id=id;
	aAgentInvoice.Select(rdb_);
	res["id"]=aAgentInvoice.id;
	res["orderid"]=aAgentInvoice.orderid;
	res["eid"]=aAgentInvoice.eid;
	return res;
}

const Value BALOONAgentInvoice::AgentInvoiceListGet(const optional<Int> id, const optional<Int> orderid) {
	Data::DataList lr;
	lr.AddColumn("id", Data::INTEGER);
	lr.AddColumn("barcode", Data::STRING);
	lr.AddColumn("paygateid", Data::INTEGER);
	lr.AddColumn("gatename", Data::STRING);
	lr.AddColumn("currency", Data::STRING);
	lr.AddColumn("amount", Data::DECIMAL);
	lr.AddColumn("status", Data::STRING);
	lr.AddColumn("comment", Data::STRING);
	lr.AddColumn("ext_orderid", Data::STRING);
	lr.AddColumn("orderid", Data::INTEGER);
	lr.AddColumn("code", Data::STRING);
	AgentInvoice aAgentInvoice;
	Invoice aInvoice;
	PayGate agate;
	AgentOrder aAgentOrder;
	PayGate apaygate;
	lr.Bind(aAgentInvoice.id, "id");
	lr.Bind(aInvoice.barcode, "barcode");
	lr.Bind(aInvoice.paygateid, "paygateid");
	lr.Bind(agate.name, "gatename");
	lr.Bind(aInvoice.currency, "currency");
	lr.Bind(aInvoice.amount, "amount");
	lr.Bind(aInvoice.status, "status");
	lr.Bind(aInvoice.comment, "comment");
	lr.Bind(aInvoice.ext_orderid, "ext_orderid");
	lr.Bind(aAgentInvoice.orderid, "orderid");
	lr.Bind(aAgentOrder.code, "code");
	Selector sel;
	sel << aAgentInvoice->id << aInvoice->barcode << aInvoice->paygateid << agate->name << aInvoice->currency << aInvoice->amount << aInvoice->status << aInvoice->comment << aInvoice->ext_orderid << aAgentInvoice->orderid << aAgentOrder->code;
	sel.Join(aInvoice).On(aAgentInvoice->id==aInvoice->id).Join(aAgentOrder).On(aAgentInvoice->orderid==aAgentOrder->id).LeftJoin(apaygate).On(aInvoice->paygateid==apaygate->id);
	sel.Where( (Defined(id) ? aAgentInvoice->id==*id : Expression()) &&  (Defined(orderid) ? aAgentInvoice->orderid==*orderid : Expression()));
	DataSet data=sel.Execute(rdb_);
	while(data.Fetch()) lr.AddRow();
	Value res=lr;
	return res;
}

void BALOONReports::initialize(ParamsMapRef params) {
	RegisterMethod("AgentSalesReportListGet", (&BALOONReports::__AgentSalesReportListGet));
	RegisterMethod("CurrentMonth", (&BALOONReports::__CurrentMonth));
	RegisterMethod("PreviousMonth", (&BALOONReports::__PreviousMonth));
	RegisterMethod("CurrentDate", (&BALOONReports::__CurrentDate));
	RegisterMethod("CurrentAgentSalesReportListGet", (&BALOONReports::__CurrentAgentSalesReportListGet));
	RegisterMethod("PersonSalesReportListGet", (&BALOONReports::__PersonSalesReportListGet));
	RegisterMethod("IdentifierReportListGet", (&BALOONReports::__IdentifierReportListGet));
	InitDatabase(params);
}

 const Value BALOONReports::__AgentSalesReportListGet(const Param& param) {
	return AgentSalesReportListGet(param["first_day"], param["last_day"], param["agentid"]);
}

 const Value BALOONReports::__CurrentMonth(const Param& param) {
	return CurrentMonth();
}

 const Value BALOONReports::__PreviousMonth(const Param& param) {
	return PreviousMonth();
}

 const Value BALOONReports::__CurrentDate(const Param& param) {
	return CurrentDate();
}

 const Value BALOONReports::__CurrentAgentSalesReportListGet(const Param& param) {
	return CurrentAgentSalesReportListGet(param["first_day"], param["last_day"]);
}

 const Value BALOONReports::__PersonSalesReportListGet(const Param& param) {
	return PersonSalesReportListGet(param["first_day"], param["last_day"]);
}

 const Value BALOONReports::__IdentifierReportListGet(const Param& param) {
	return IdentifierReportListGet(param["current_date"]);
}

// Wrappers for automatic methods
// Implementation of automatic methods
void BALOONAccount::initialize(ParamsMapRef params) {
	RegisterMethod("AccountListGet", (&BALOONAccount::__AccountListGet));
	RegisterMethod("Add", (&BALOONAccount::__Add));
	RegisterMethod("Get", (&BALOONAccount::__Get));
	RegisterMethod("Update", (&BALOONAccount::__Update));
	RegisterMethod("Delete", (&BALOONAccount::__Delete));
	InitDatabase(params);
}

 const Value BALOONAccount::__AccountListGet(const Param& param) {
	return AccountListGet(param["personid"]);
}

// Wrappers for automatic methods
const Value BALOONAccount::__Add(const Param& param) {
	return Add(param["currency"], param["personid"], param["valid"], param["balance"]);
}

const Value BALOONAccount::__Update(const Param& param) {
	return Update(param["currency"], param["personid"], param["valid"], param["balance"]);
}

const Value BALOONAccount::__Delete(const Param& param) {
	return Delete(param["currency"], param["personid"]);
}

const Value BALOONAccount::__Get(const Param& param) {
	return Get(param["currency"], param["personid"]);
}

// Implementation of automatic methods
const Value BALOONAccount::Add(const Str currency, const Int personid, const Str valid, const Decimal balance) {
	Exception e((Message("Cannot add ") << Message("Account").What() << ". ").What());
	bool error=false;
	error |= Defined(personid)  && !__CheckIDExists(Person()->id, *personid, "Client", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		Account aAccount;
		aAccount.currency=currency;
		aAccount.personid=personid;
		aAccount.valid=valid;
		aAccount.balance=balance;
	aAccount.Insert(rdb_);
		Value res;
		AddMessage(Message()<<Message("Account").What()<<" "<<aAccount.currency<<" "<<aAccount.personid<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONAccount::Update(const Str currency, const Int personid, const optional<Str> valid, const optional<Decimal> balance) {
	Exception e((Message("Cannot update ") << Message("Account").What() << ". ").What());
	bool error=false;
	error |= Defined(personid)  && !__CheckIDExists(Person()->id, *personid, "Client", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		Account aAccount;
		aAccount.currency=currency;
		aAccount.personid=personid;
		aAccount.Select(rdb_);
		if(Defined(valid)) aAccount.valid=*valid;
		if(Defined(balance)) aAccount.balance=*balance;
		aAccount.Update(rdb_);
		AddMessage(Message()<<Message("Account").What()<<" "<<aAccount.currency<<" "<<aAccount.personid<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONAccount::Delete(const Str currency, const Int personid) {
	Account aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Account").What()<<". ").What());
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.currency=currency;
	aRecord.personid=personid;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Account").What()<<" "<<currency<<" "<<personid<<" deleted. ");
	return Value();
}

const Value BALOONAccount::Get(const Str currency, const Int personid) {
	Value res;
	Account aAccount;
	aAccount.currency=currency;
	aAccount.personid=personid;
	aAccount.Select(rdb_);
	res["currency"]=aAccount.currency;
	res["personid"]=aAccount.personid;
	res["valid"]=aAccount.valid;
	res["balance"]=aAccount.balance;
	return res;
}

void BALOONPackageOrder::initialize(ParamsMapRef params) {
	RegisterMethod("MyPackageListGet_API", (&BALOONPackageOrder::__MyPackageListGet_API));
	RegisterMethod("Add", (&BALOONPackageOrder::__Add));
	RegisterMethod("Get", (&BALOONPackageOrder::__Get));
	RegisterMethod("Update", (&BALOONPackageOrder::__Update));
	RegisterMethod("Delete", (&BALOONPackageOrder::__Delete));
	RegisterMethod("PackageOrderListGet", (&BALOONPackageOrder::__PackageOrderListGet));
	InitDatabase(params);
}

 const Value BALOONPackageOrder::__MyPackageListGet_API(const Param& param) {
	return MyPackageListGet_API(param["code"]);
}

 const Value BALOONPackageOrder::__Add(const Param& param) {
	return Add(param["identifierid"], param["service_rulename"]);
}

// Wrappers for automatic methods
const Value BALOONPackageOrder::__Update(const Param& param) {
	return Update(param["id"], param["identifierid"], param["service_rulename"], param["filed_at"]);
}

const Value BALOONPackageOrder::__Delete(const Param& param) {
	return Delete(param["id"]);
}

const Value BALOONPackageOrder::__Get(const Param& param) {
	return Get(param["id"]);
}

const Value BALOONPackageOrder::__PackageOrderListGet(const Param& param) {
	return PackageOrderListGet(param["identifierid"], param["service_rulename"]);
}

// Implementation of automatic methods
#if 0 //BALOONPackageOrder Add
const Value BALOONPackageOrder::Add(const Int identifierid, const Str service_rulename, const Time filed_at) {
	Exception e((Message("Cannot add ") << Message("Package Order").What() << ". ").What());
	bool error=false;
	error |= Defined(identifierid)  && !__CheckIDExists(Identifier()->id, *identifierid, "Identifier", e, rdb_);
	error |= Defined(service_rulename)  && !__CheckIDExists(SpdServiceRule()->name, *service_rulename, "Service Rule", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		PackageOrder aPackageOrder;
		aPackageOrder.identifierid=identifierid;
		aPackageOrder.service_rulename=service_rulename;
		aPackageOrder.filed_at=filed_at;
		Int sk=aPackageOrder.Insert(rdb_);
		Value res;
		res["id"]=sk;
		AddMessage(Message()<<Message("Package Order").What()<<" "<<sk<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

#endif //BALOONPackageOrder Add

const Value BALOONPackageOrder::Update(const Int id, const optional<Int> identifierid, const optional<Str> service_rulename, const optional<Time> filed_at) {
	Exception e((Message("Cannot update ") << Message("Package Order").What() << ". ").What());
	bool error=false;
	error |= Defined(identifierid) && Defined(*identifierid)  && !__CheckIDExists(Identifier()->id, *identifierid, "Identifier", e, rdb_);
	error |= Defined(service_rulename) && Defined(*service_rulename)  && !__CheckIDExists(SpdServiceRule()->name, *service_rulename, "Service Rule", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		PackageOrder aPackageOrder;
		aPackageOrder.id=id;
		aPackageOrder.Select(rdb_);
		if(Defined(identifierid)) aPackageOrder.identifierid=*identifierid;
		if(Defined(service_rulename)) aPackageOrder.service_rulename=*service_rulename;
		if(Defined(filed_at)) aPackageOrder.filed_at=*filed_at;
		aPackageOrder.Update(rdb_);
		AddMessage(Message()<<Message("Package Order").What()<<" "<<aPackageOrder.id<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONPackageOrder::Delete(const Int id) {
	PackageOrder aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Package Order").What()<<". ").What());
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.id=id;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Package Order").What()<<" "<<id<<" deleted. ");
	return Value();
}

const Value BALOONPackageOrder::Get(const Int id) {
	Value res;
	PackageOrder aPackageOrder;
	aPackageOrder.id=id;
	aPackageOrder.Select(rdb_);
	res["id"]=aPackageOrder.id;
	res["identifierid"]=aPackageOrder.identifierid;
	res["service_rulename"]=aPackageOrder.service_rulename;
	res["filed_at"]=aPackageOrder.filed_at;
	return res;
}

const Value BALOONPackageOrder::PackageOrderListGet(const optional<Int> identifierid, const optional<Str> service_rulename) {
	Data::DataList lr;
	lr.AddColumn("id", Data::INTEGER);
	lr.AddColumn("identifiercode", Data::STRING);
	lr.AddColumn("service_rulename", Data::STRING);
	lr.AddColumn("service_rulepackage_cost", Data::DECIMAL);
	lr.AddColumn("filed_at", Data::DATETIME);
	PackageOrder aPackageOrder;
	Identifier aidentifier;
	SpdServiceRule aservice_rule;
	lr.Bind(aPackageOrder.id, "id");
	lr.Bind(aidentifier.code, "identifiercode");
	lr.Bind(aservice_rule.name, "service_rulename");
	lr.Bind(aservice_rule.package_cost, "service_rulepackage_cost");
	lr.Bind(aPackageOrder.filed_at, "filed_at");
	Selector sel;
	sel << aPackageOrder->id << aidentifier->code << aservice_rule->name << aservice_rule->package_cost << aPackageOrder->filed_at;
	sel.Join(aidentifier).On(aPackageOrder->identifierid==aidentifier->id).Join(aservice_rule).On(aPackageOrder->service_rulename==aservice_rule->name);
	sel.Where( (Defined(identifierid) ? aPackageOrder->identifierid==*identifierid : Expression()) &&  (Defined(service_rulename) ? aPackageOrder->service_rulename==*service_rulename : Expression()));
	DataSet data=sel.Execute(rdb_);
	while(data.Fetch()) lr.AddRow();
	Value res=lr;
	return res;
}

void BALOONTransaction::initialize(ParamsMapRef params) {
	RegisterMethod("TransactionListGet", (&BALOONTransaction::__TransactionListGet));
	RegisterMethod("CurrentTransactionListGet_FE", (&BALOONTransaction::__CurrentTransactionListGet_FE));
	RegisterMethod("IdentifierTransactionListGet_FE", (&BALOONTransaction::__IdentifierTransactionListGet_FE));
	RegisterMethod("SaleIdentifier_API", (&BALOONTransaction::__SaleIdentifier_API));
	RegisterMethod("SalePackage_API", (&BALOONTransaction::__SalePackage_API));
	RegisterMethod("MassSalePackages_FE", (&BALOONTransaction::__MassSalePackages_FE));
	RegisterMethod("FillupAccount_API", (&BALOONTransaction::__FillupAccount_API));
	RegisterMethod("Get", (&BALOONTransaction::__Get));
	RegisterMethod("Delete", (&BALOONTransaction::__Delete));
	InitDatabase(params);
}

 const Value BALOONTransaction::__TransactionListGet(const Param& param) {
	return TransactionListGet(param["identifierid"]);
}

 const Value BALOONTransaction::__CurrentTransactionListGet_FE(const Param& param) {
	return CurrentTransactionListGet_FE();
}

 const Value BALOONTransaction::__IdentifierTransactionListGet_FE(const Param& param) {
	return IdentifierTransactionListGet_FE(param["code"]);
}

 const Value BALOONTransaction::__SaleIdentifier_API(const Param& param) {
	return SaleIdentifier_API(param["code"], param["print_code"], param["valid_from"], param["valid_to"], param["permanent_rulename"], param["categoryname"], param["ratename"], param["currency"], param["amount"]);
}

 const Value BALOONTransaction::__SalePackage_API(const Param& param) {
	return SalePackage_API(param["code"], param["print_code"], param["categoryname"]);
}

 const Value BALOONTransaction::__MassSalePackages_FE(const Param& param) {
	return MassSalePackages_FE(param["code"], param["print_code"], param["categories"]);
}

 const Value BALOONTransaction::__FillupAccount_API(const Param& param) {
	return FillupAccount_API(param["code"], param["print_code"], param["currency"], param["amount"]);
}

// Wrappers for automatic methods
const Value BALOONTransaction::__Delete(const Param& param) {
	return Delete(param["id"]);
}

const Value BALOONTransaction::__Get(const Param& param) {
	return Get(param["id"]);
}

// Implementation of automatic methods
const Value BALOONTransaction::Delete(const Int id) {
	Transaction aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Payment").What()<<". ").What());
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.id=id;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Payment").What()<<" "<<id<<" deleted. ");
	return Value();
}

const Value BALOONTransaction::Get(const Int id) {
	Value res;
	Transaction aTransaction;
	aTransaction.id=id;
	aTransaction.Select(rdb_);
	res["id"]=aTransaction.id;
	res["personid"]=aTransaction.personid;
	res["agentid"]=aTransaction.agentid;
	res["currency"]=aTransaction.currency;
	res["eid"]=aTransaction.eid;
	res["amount"]=aTransaction.amount;
	res["status"]=aTransaction.status;
	res["comment"]=aTransaction.comment;
	res["in_sync"]=aTransaction.in_sync;
	res["valid_until"]=aTransaction.valid_until;
	res["meta"]=aTransaction.meta;
	return res;
}

void BALOONAdvertisement::initialize(ParamsMapRef params) {
	RegisterMethod("NewsArticleListGet", (&BALOONAdvertisement::__NewsArticleListGet));
	RegisterMethod("LatestArticleListGet_FE", (&BALOONAdvertisement::__LatestArticleListGet_FE));
	RegisterMethod("VisualItemListGet_FE", (&BALOONAdvertisement::__VisualItemListGet_FE));
	RegisterMethod("VisualItemListGet_FE_1_13", (&BALOONAdvertisement::__VisualItemListGet_FE_1_13));
	RegisterMethod("NewsArticleAdd", (&BALOONAdvertisement::__NewsArticleAdd));
	RegisterMethod("NewsArticleGet", (&BALOONAdvertisement::__NewsArticleGet));
	RegisterMethod("NewsArticleUpdate", (&BALOONAdvertisement::__NewsArticleUpdate));
	RegisterMethod("Delete", (&BALOONAdvertisement::__Delete));
	RegisterMethod("VisualItemListGet", (&BALOONAdvertisement::__VisualItemListGet));
	RegisterMethod("VisualItemGet", (&BALOONAdvertisement::__VisualItemGet));
	RegisterMethod("VisualItemAdd", (&BALOONAdvertisement::__VisualItemAdd));
	RegisterMethod("VisualItemUpdate", (&BALOONAdvertisement::__VisualItemUpdate));
	InitDatabase(params);
}

 const Value BALOONAdvertisement::__NewsArticleListGet(const Param& param) {
	return NewsArticleListGet();
}

 const Value BALOONAdvertisement::__LatestArticleListGet_FE(const Param& param) {
	return LatestArticleListGet_FE(param["items_qty"]);
}

 const Value BALOONAdvertisement::__VisualItemListGet_FE(const Param& param) {
	return VisualItemListGet_FE(param["items_qty"]);
}

 const Value BALOONAdvertisement::__VisualItemListGet_FE_1_13(const Param& param) {
	return VisualItemListGet_FE_1_13(param["items_qty"]);
}

 const Value BALOONAdvertisement::__NewsArticleAdd(const Param& param) {
	return NewsArticleAdd(param["title"], param["text_content"], param["priority"]);
}

 const Value BALOONAdvertisement::__NewsArticleGet(const Param& param) {
	return NewsArticleGet(param["id"]);
}

 const Value BALOONAdvertisement::__NewsArticleUpdate(const Param& param) {
	return NewsArticleUpdate(param["id"], param["title"], param["text_content"], param["priority"]);
}

 const Value BALOONAdvertisement::__Delete(const Param& param) {
	return Delete(param["id"]);
}

 const Value BALOONAdvertisement::__VisualItemListGet(const Param& param) {
	return VisualItemListGet();
}

 const Value BALOONAdvertisement::__VisualItemGet(const Param& param) {
	return VisualItemGet(param["id"]);
}

 const Value BALOONAdvertisement::__VisualItemAdd(const Param& param) {
	return VisualItemAdd(param["title"], param["html_link"], param["image"], param["widget"], param["priority"]);
}

 const Value BALOONAdvertisement::__VisualItemUpdate(const Param& param) {
	return VisualItemUpdate(param["id"], param["title"], param["html_link"], param["image"], param["widget"], param["priority"]);
}

// Wrappers for automatic methods
// Implementation of automatic methods
void BALOONInfoPlacement::initialize(ParamsMapRef params) {
	RegisterMethod("Add", (&BALOONInfoPlacement::__Add));
	RegisterMethod("Get", (&BALOONInfoPlacement::__Get));
	RegisterMethod("Update", (&BALOONInfoPlacement::__Update));
	RegisterMethod("Delete", (&BALOONInfoPlacement::__Delete));
	RegisterMethod("InfoPlacementListGet", (&BALOONInfoPlacement::__InfoPlacementListGet));
	InitDatabase(params);
}

// Wrappers for automatic methods
const Value BALOONInfoPlacement::__Add(const Param& param) {
	return Add(param["id"], param["widget"]);
}

const Value BALOONInfoPlacement::__Update(const Param& param) {
	return Update(param["id"], param["widget"]);
}

const Value BALOONInfoPlacement::__Delete(const Param& param) {
	return Delete(param["id"]);
}

const Value BALOONInfoPlacement::__Get(const Param& param) {
	return Get(param["id"]);
}

const Value BALOONInfoPlacement::__InfoPlacementListGet(const Param& param) {
	return InfoPlacementListGet(param["id"]);
}

// Implementation of automatic methods
const Value BALOONInfoPlacement::Add(const Int id, const Str widget) {
	Exception e((Message("Cannot add ") << Message("Info Placement").What() << ". ").What());
	bool error=false;
	error |= Defined(id)  && !__CheckIDExists(NewsArticle()->id, *id, "News Article ID", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		InfoPlacement aInfoPlacement;
		aInfoPlacement.id=id;
		aInfoPlacement.widget=widget;
	aInfoPlacement.Insert(rdb_);
		Value res;
		AddMessage(Message()<<Message("Info Placement").What()<<" "<<aInfoPlacement.id<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONInfoPlacement::Update(const Int id, const optional<Str> widget) {
	Exception e((Message("Cannot update ") << Message("Info Placement").What() << ". ").What());
	bool error=false;
	error |= Defined(id)  && !__CheckIDExists(NewsArticle()->id, *id, "News Article ID", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		InfoPlacement aInfoPlacement;
		aInfoPlacement.id=id;
		aInfoPlacement.Select(rdb_);
		if(Defined(widget)) aInfoPlacement.widget=*widget;
		aInfoPlacement.Update(rdb_);
		AddMessage(Message()<<Message("Info Placement").What()<<" "<<aInfoPlacement.id<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONInfoPlacement::Delete(const Int id) {
	InfoPlacement aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Info Placement").What()<<". ").What());
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.id=id;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Info Placement").What()<<" "<<id<<" deleted. ");
	return Value();
}

const Value BALOONInfoPlacement::Get(const Int id) {
	Value res;
	InfoPlacement aInfoPlacement;
	aInfoPlacement.id=id;
	aInfoPlacement.Select(rdb_);
	res["id"]=aInfoPlacement.id;
	res["widget"]=aInfoPlacement.widget;
	return res;
}

const Value BALOONInfoPlacement::InfoPlacementListGet(const optional<Int> id) {
	Data::DataList lr;
	lr.AddColumn("id", Data::INTEGER);
	lr.AddColumn("title", Data::STRING);
	lr.AddColumn("widget", Data::STRING, WIDGET_TYPEValues());
	InfoPlacement aInfoPlacement;
	NewsArticle aNewsArticle;
	lr.Bind(aInfoPlacement.id, "id");
	lr.Bind(aNewsArticle.title, "title");
	lr.Bind(aInfoPlacement.widget, "widget");
	Selector sel;
	sel << aInfoPlacement->id << aNewsArticle->title << aInfoPlacement->widget;
	sel.Join(aNewsArticle).On(aInfoPlacement->id==aNewsArticle->id);
	sel.Where( (Defined(id) ? aInfoPlacement->id==*id : Expression()));
	DataSet data=sel.Execute(rdb_);
	while(data.Fetch()) lr.AddRow();
	Value res=lr;
	return res;
}

void BALOONHoliday::initialize(ParamsMapRef params) {
	RegisterMethod("MassAdd", (&BALOONHoliday::__MassAdd));
	RegisterMethod("HolidaysFromDateListGet_FE", (&BALOONHoliday::__HolidaysFromDateListGet_FE));
	RegisterMethod("Add", (&BALOONHoliday::__Add));
	RegisterMethod("Get", (&BALOONHoliday::__Get));
	RegisterMethod("Update", (&BALOONHoliday::__Update));
	RegisterMethod("Delete", (&BALOONHoliday::__Delete));
	RegisterMethod("HolidayListGet", (&BALOONHoliday::__HolidayListGet));
	InitDatabase(params);
}

 const Value BALOONHoliday::__MassAdd(const Param& param) {
	return MassAdd(param["holidays"]);
}

 const Value BALOONHoliday::__HolidaysFromDateListGet_FE(const Param& param) {
	return HolidaysFromDateListGet_FE(param["adate"]);
}

// Wrappers for automatic methods
const Value BALOONHoliday::__Add(const Param& param) {
	return Add(param["adate"], param["out_of_service"], param["comment"]);
}

const Value BALOONHoliday::__Update(const Param& param) {
	return Update(param["adate"], param["out_of_service"], param["comment"]);
}

const Value BALOONHoliday::__Delete(const Param& param) {
	return Delete(param["adate"]);
}

const Value BALOONHoliday::__Get(const Param& param) {
	return Get(param["adate"]);
}

const Value BALOONHoliday::__HolidayListGet(const Param& param) {
	return HolidayListGet();
}

// Implementation of automatic methods
const Value BALOONHoliday::Add(const ADate adate, const Str out_of_service, const Str comment) {
	Exception e((Message("Cannot add ") << Message("Holiday").What() << ". ").What());
	try {
		Holiday aHoliday;
		aHoliday.adate=adate;
		aHoliday.out_of_service=out_of_service;
		aHoliday.comment=comment;
	aHoliday.Insert(rdb_);
		Value res;
		AddMessage(Message()<<Message("Holiday").What()<<" "<<aHoliday.adate<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONHoliday::Update(const ADate adate, const optional<Str> out_of_service, const optional<Str> comment) {
	Exception e((Message("Cannot update ") << Message("Holiday").What() << ". ").What());
	try {
		Holiday aHoliday;
		aHoliday.adate=adate;
		aHoliday.Select(rdb_);
		if(Defined(out_of_service)) aHoliday.out_of_service=*out_of_service;
		if(Defined(comment)) aHoliday.comment=*comment;
		aHoliday.Update(rdb_);
		AddMessage(Message()<<Message("Holiday").What()<<" "<<aHoliday.adate<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONHoliday::Delete(const ADate adate) {
	Holiday aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Holiday").What()<<". ").What());
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.adate=adate;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Holiday").What()<<" "<<adate<<" deleted. ");
	return Value();
}

const Value BALOONHoliday::Get(const ADate adate) {
	Value res;
	Holiday aHoliday;
	aHoliday.adate=adate;
	aHoliday.Select(rdb_);
	res["adate"]=aHoliday.adate;
	res["out_of_service"]=aHoliday.out_of_service;
	res["comment"]=aHoliday.comment;
	return res;
}

const Value BALOONHoliday::HolidayListGet() {
	Data::DataList lr;
	lr.AddColumn("adate", Data::DATETIME);
	lr.AddColumn("out_of_service", Data::STRING, BOOLValues());
	lr.AddColumn("comment", Data::STRING);
	Holiday aHoliday;
	lr.Bind(aHoliday.adate, "adate");
	lr.Bind(aHoliday.out_of_service, "out_of_service");
	lr.Bind(aHoliday.comment, "comment");
	Selector sel;
	sel << aHoliday->adate << aHoliday->out_of_service << aHoliday->comment;
	sel.Where();
	DataSet data=sel.Execute(rdb_);
	while(data.Fetch()) lr.AddRow();
	Value res=lr;
	return res;
}

void BALOONWeekTariffGroup::initialize(ParamsMapRef params) {
	RegisterMethod("PartnerWeekTariffGroupListGet", (&BALOONWeekTariffGroup::__PartnerWeekTariffGroupListGet));
	RegisterMethod("Add", (&BALOONWeekTariffGroup::__Add));
	RegisterMethod("Update", (&BALOONWeekTariffGroup::__Update));
	RegisterMethod("ForPartnerAdd", (&BALOONWeekTariffGroup::__ForPartnerAdd));
	RegisterMethod("ForPartnerUpdate", (&BALOONWeekTariffGroup::__ForPartnerUpdate));
	RegisterMethod("ForPartnerGet", (&BALOONWeekTariffGroup::__ForPartnerGet));
	RegisterMethod("ForPartnerDelete", (&BALOONWeekTariffGroup::__ForPartnerDelete));
	RegisterMethod("Get", (&BALOONWeekTariffGroup::__Get));
	RegisterMethod("Delete", (&BALOONWeekTariffGroup::__Delete));
	RegisterMethod("WeekTariffGroupListGet", (&BALOONWeekTariffGroup::__WeekTariffGroupListGet));
	InitDatabase(params);
}

 const Value BALOONWeekTariffGroup::__PartnerWeekTariffGroupListGet(const Param& param) {
	return PartnerWeekTariffGroupListGet();
}

 const Value BALOONWeekTariffGroup::__Add(const Param& param) {
	return Add(param["name"], param["description"], param["is_default"]);
}

 const Value BALOONWeekTariffGroup::__Update(const Param& param) {
	return Update(param["id"], param["name"], param["description"], param["is_default"]);
}

 const Value BALOONWeekTariffGroup::__ForPartnerAdd(const Param& param) {
	return ForPartnerAdd(param["partnerid"], param["name"], param["description"]);
}

 const Value BALOONWeekTariffGroup::__ForPartnerUpdate(const Param& param) {
	return ForPartnerUpdate(param["id"], param["name"], param["description"]);
}

 const Value BALOONWeekTariffGroup::__ForPartnerGet(const Param& param) {
	return ForPartnerGet(param["id"]);
}

 const Value BALOONWeekTariffGroup::__ForPartnerDelete(const Param& param) {
	return ForPartnerDelete(param["id"]);
}

// Wrappers for automatic methods
const Value BALOONWeekTariffGroup::__Delete(const Param& param) {
	return Delete(param["id"]);
}

const Value BALOONWeekTariffGroup::__Get(const Param& param) {
	return Get(param["id"]);
}

const Value BALOONWeekTariffGroup::__WeekTariffGroupListGet(const Param& param) {
	return WeekTariffGroupListGet();
}

// Implementation of automatic methods
#if 0 //BALOONWeekTariffGroup Add
const Value BALOONWeekTariffGroup::Add(const Str name, const Str description, const Str is_default) {
	Exception e((Message("Cannot add ") << Message("Tariff Group").What() << ". ").What());
	try {
		WeekTariffGroup aWeekTariffGroup;
		aWeekTariffGroup.name=name;
		aWeekTariffGroup.description=description;
		aWeekTariffGroup.is_default=is_default;
		Int sk=aWeekTariffGroup.Insert(rdb_);
		Value res;
		res["id"]=sk;
		AddMessage(Message()<<Message("Tariff Group").What()<<" "<<sk<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

#endif //BALOONWeekTariffGroup Add

#if 0 //BALOONWeekTariffGroup Update
const Value BALOONWeekTariffGroup::Update(const Int id, const optional<Str> name, const optional<Str> description, const optional<Str> is_default) {
	Exception e((Message("Cannot update ") << Message("Tariff Group").What() << ". ").What());
	try {
		WeekTariffGroup aWeekTariffGroup;
		aWeekTariffGroup.id=id;
		aWeekTariffGroup.Select(rdb_);
		if(Defined(name)) aWeekTariffGroup.name=*name;
		if(Defined(description)) aWeekTariffGroup.description=*description;
		if(Defined(is_default)) aWeekTariffGroup.is_default=*is_default;
		aWeekTariffGroup.Update(rdb_);
		AddMessage(Message()<<Message("Tariff Group").What()<<" "<<aWeekTariffGroup.id<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

#endif //BALOONWeekTariffGroup Update

const Value BALOONWeekTariffGroup::Delete(const Int id) {
	WeekTariffGroup aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Tariff Group").What()<<". ").What());
	//check WeekTariff reference to WeekTariffGroup reference 
	error |= !__CheckIDNotExists(WeekTariff()->agroupid, id, "Week Tariff", e, rdb_);
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.id=id;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Tariff Group").What()<<" "<<id<<" deleted. ");
	return Value();
}

const Value BALOONWeekTariffGroup::Get(const Int id) {
	Value res;
	WeekTariffGroup aWeekTariffGroup;
	aWeekTariffGroup.id=id;
	aWeekTariffGroup.Select(rdb_);
	res["id"]=aWeekTariffGroup.id;
	res["name"]=aWeekTariffGroup.name;
	res["description"]=aWeekTariffGroup.description;
	res["is_default"]=aWeekTariffGroup.is_default;
	return res;
}

const Value BALOONWeekTariffGroup::WeekTariffGroupListGet() {
	Data::DataList lr;
	lr.AddColumn("id", Data::INTEGER);
	lr.AddColumn("name", Data::STRING);
	lr.AddColumn("is_default", Data::STRING, BOOLValues());
	lr.AddColumn("description", Data::STRING);
	WeekTariffGroup aWeekTariffGroup;
	lr.Bind(aWeekTariffGroup.id, "id");
	lr.Bind(aWeekTariffGroup.name, "name");
	lr.Bind(aWeekTariffGroup.is_default, "is_default");
	lr.Bind(aWeekTariffGroup.description, "description");
	Selector sel;
	sel << aWeekTariffGroup->id << aWeekTariffGroup->name << aWeekTariffGroup->is_default << aWeekTariffGroup->description;
	sel.Where();
	DataSet data=sel.Execute(rdb_);
	while(data.Fetch()) lr.AddRow();
	Value res=lr;
	return res;
}

void BALOONWeekTariff::initialize(ParamsMapRef params) {
	RegisterMethod("WeekTariffListGet", (&BALOONWeekTariff::__WeekTariffListGet));
	RegisterMethod("PartnerWeekTariffListGet", (&BALOONWeekTariff::__PartnerWeekTariffListGet));
	RegisterMethod("MassAdd", (&BALOONWeekTariff::__MassAdd));
	RegisterMethod("AsyncRestoreCounters", (&BALOONWeekTariff::__AsyncRestoreCounters));
	RegisterMethod("SeansesAllByDateListGet_FE", (&BALOONWeekTariff::__SeansesAllByDateListGet_FE));
	RegisterMethod("SeansesListGet_FE", (&BALOONWeekTariff::__SeansesListGet_FE));
	RegisterMethod("SeansesByDateListGet_FE", (&BALOONWeekTariff::__SeansesByDateListGet_FE));
	RegisterMethod("SeansesByDateListGet", (&BALOONWeekTariff::__SeansesByDateListGet));
	RegisterMethod("ForPartnerDelete", (&BALOONWeekTariff::__ForPartnerDelete));
	RegisterMethod("OurSeansesByDateListGet", (&BALOONWeekTariff::__OurSeansesByDateListGet));
	RegisterMethod("OurSeansesByDateListGet_API", (&BALOONWeekTariff::__OurSeansesByDateListGet_API));
	RegisterMethod("Add", (&BALOONWeekTariff::__Add));
	RegisterMethod("Get", (&BALOONWeekTariff::__Get));
	RegisterMethod("Update", (&BALOONWeekTariff::__Update));
	RegisterMethod("Delete", (&BALOONWeekTariff::__Delete));
	InitDatabase(params);
}

 const Value BALOONWeekTariff::__WeekTariffListGet(const Param& param) {
	return WeekTariffListGet(param["agroupid"]);
}

 const Value BALOONWeekTariff::__PartnerWeekTariffListGet(const Param& param) {
	return PartnerWeekTariffListGet(param["agroupid"]);
}

 const Value BALOONWeekTariff::__MassAdd(const Param& param) {
	return MassAdd(param["agroupid"], param["week_days"], param["tariffs"]);
}

 const Value BALOONWeekTariff::__AsyncRestoreCounters(const Param& param) {
	return AsyncRestoreCounters();
}

 const Value BALOONWeekTariff::__SeansesAllByDateListGet_FE(const Param& param) {
	return SeansesAllByDateListGet_FE(param["adate"], param["name"], param["is_default"]);
}

 const Value BALOONWeekTariff::__SeansesListGet_FE(const Param& param) {
	return SeansesListGet_FE(param["adate"], param["identifier_rulename"], param["identifier_categoryname"], param["ratename"]);
}

 const Value BALOONWeekTariff::__SeansesByDateListGet_FE(const Param& param) {
	return SeansesByDateListGet_FE(param["adate"]);
}

 const Value BALOONWeekTariff::__SeansesByDateListGet(const Param& param) {
	return SeansesByDateListGet(param["adate"]);
}

 const Value BALOONWeekTariff::__ForPartnerDelete(const Param& param) {
	return ForPartnerDelete(param["id"]);
}

 const Value BALOONWeekTariff::__OurSeansesByDateListGet(const Param& param) {
	return OurSeansesByDateListGet(param["adate"]);
}

 const Value BALOONWeekTariff::__OurSeansesByDateListGet_API(const Param& param) {
	return OurSeansesByDateListGet_API(param["adate"], param["token"]);
}

// Wrappers for automatic methods
const Value BALOONWeekTariff::__Add(const Param& param) {
	return Add(param["agroupid"], param["week_day"], param["time_from"], param["time_upto"], param["price"], param["pricetype"], param["free"], param["total"], param["counttype"], param["external_name"], param["identifier_rulename"], param["service_rulename"], param["identifier_categoryname"], param["ratename"], param["enabled"]);
}

const Value BALOONWeekTariff::__Update(const Param& param) {
	return Update(param["id"], param["agroupid"], param["week_day"], param["time_from"], param["time_upto"], param["price"], param["pricetype"], param["free"], param["total"], param["counttype"], param["external_name"], param["identifier_rulename"], param["service_rulename"], param["identifier_categoryname"], param["ratename"], param["enabled"]);
}

const Value BALOONWeekTariff::__Delete(const Param& param) {
	return Delete(param["id"]);
}

const Value BALOONWeekTariff::__Get(const Param& param) {
	return Get(param["id"]);
}

// Implementation of automatic methods
const Value BALOONWeekTariff::Add(const Int agroupid, const Str week_day, const ATime time_from, const ATime time_upto, const Decimal price, const Str pricetype, const Int free, const Int total, const Str counttype, const Str external_name, const Str identifier_rulename, const Str service_rulename, const Str identifier_categoryname, const Str ratename, const Str enabled) {
	Exception e((Message("Cannot add ") << Message("Week Tariff").What() << ". ").What());
	bool error=false;
	error |= Defined(agroupid)  && !__CheckIDExists(WeekTariffGroup()->id, *agroupid, "Tariff Group", e, rdb_);
	error |= Defined(identifier_rulename)  && !__CheckIDExists(IdentifierRule()->name, *identifier_rulename, "Identifier Rule", e, rdb_);
	error |= Defined(service_rulename)  && !__CheckIDExists(SpdServiceRule()->name, *service_rulename, "Service Rule", e, rdb_);
	error |= Defined(identifier_categoryname)  && !__CheckIDExists(IdentifierCategory()->name, *identifier_categoryname, "Category", e, rdb_);
	error |= Defined(ratename)  && !__CheckIDExists(IdentifierRate()->name, *ratename, "Tariff", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		WeekTariff aWeekTariff;
		aWeekTariff.agroupid=agroupid;
		aWeekTariff.week_day=week_day;
		aWeekTariff.time_from=time_from;
		aWeekTariff.time_upto=time_upto;
		aWeekTariff.price=price;
		aWeekTariff.pricetype=pricetype;
		aWeekTariff.free=free;
		aWeekTariff.total=total;
		aWeekTariff.counttype=counttype;
		aWeekTariff.external_name=external_name;
		aWeekTariff.identifier_rulename=identifier_rulename;
		aWeekTariff.service_rulename=service_rulename;
		aWeekTariff.identifier_categoryname=identifier_categoryname;
		aWeekTariff.ratename=ratename;
		aWeekTariff.enabled=enabled;
		Int sk=aWeekTariff.Insert(rdb_);
		Value res;
		res["id"]=sk;
		AddMessage(Message()<<Message("Week Tariff").What()<<" "<<sk<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONWeekTariff::Update(const Int id, const optional<Int> agroupid, const optional<Str> week_day, const optional<ATime> time_from, const optional<ATime> time_upto, const optional<Decimal> price, const optional<Str> pricetype, const optional<Int> free, const optional<Int> total, const optional<Str> counttype, const optional<Str> external_name, const optional<Str> identifier_rulename, const optional<Str> service_rulename, const optional<Str> identifier_categoryname, const optional<Str> ratename, const optional<Str> enabled) {
	Exception e((Message("Cannot update ") << Message("Week Tariff").What() << ". ").What());
	bool error=false;
	error |= Defined(agroupid) && Defined(*agroupid)  && !__CheckIDExists(WeekTariffGroup()->id, *agroupid, "Tariff Group", e, rdb_);
	error |= Defined(identifier_rulename) && Defined(*identifier_rulename)  && !__CheckIDExists(IdentifierRule()->name, *identifier_rulename, "Identifier Rule", e, rdb_);
	error |= Defined(service_rulename) && Defined(*service_rulename)  && !__CheckIDExists(SpdServiceRule()->name, *service_rulename, "Service Rule", e, rdb_);
	error |= Defined(identifier_categoryname) && Defined(*identifier_categoryname)  && !__CheckIDExists(IdentifierCategory()->name, *identifier_categoryname, "Category", e, rdb_);
	error |= Defined(ratename) && Defined(*ratename)  && !__CheckIDExists(IdentifierRate()->name, *ratename, "Tariff", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		WeekTariff aWeekTariff;
		aWeekTariff.id=id;
		aWeekTariff.Select(rdb_);
		if(Defined(agroupid)) aWeekTariff.agroupid=*agroupid;
		if(Defined(week_day)) aWeekTariff.week_day=*week_day;
		if(Defined(time_from)) aWeekTariff.time_from=*time_from;
		if(Defined(time_upto)) aWeekTariff.time_upto=*time_upto;
		if(Defined(price)) aWeekTariff.price=*price;
		if(Defined(pricetype)) aWeekTariff.pricetype=*pricetype;
		if(Defined(free)) aWeekTariff.free=*free;
		if(Defined(total)) aWeekTariff.total=*total;
		if(Defined(counttype)) aWeekTariff.counttype=*counttype;
		if(Defined(external_name)) aWeekTariff.external_name=*external_name;
		if(Defined(identifier_rulename)) aWeekTariff.identifier_rulename=*identifier_rulename;
		if(Defined(service_rulename)) aWeekTariff.service_rulename=*service_rulename;
		if(Defined(identifier_categoryname)) aWeekTariff.identifier_categoryname=*identifier_categoryname;
		if(Defined(ratename)) aWeekTariff.ratename=*ratename;
		if(Defined(enabled)) aWeekTariff.enabled=*enabled;
		aWeekTariff.Update(rdb_);
		AddMessage(Message()<<Message("Week Tariff").What()<<" "<<aWeekTariff.id<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONWeekTariff::Delete(const Int id) {
	WeekTariff aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Week Tariff").What()<<". ").What());
	//check PartnerOrder reference to WeekTariff reference 
	error |= !__CheckIDNotExists(PartnerOrder()->week_tariffid, id, "Order", e, rdb_);
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.id=id;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Week Tariff").What()<<" "<<id<<" deleted. ");
	return Value();
}

const Value BALOONWeekTariff::Get(const Int id) {
	Value res;
	WeekTariff aWeekTariff;
	aWeekTariff.id=id;
	aWeekTariff.Select(rdb_);
	res["id"]=aWeekTariff.id;
	res["agroupid"]=aWeekTariff.agroupid;
	res["week_day"]=aWeekTariff.week_day;
	res["time_from"]=aWeekTariff.time_from;
	res["time_upto"]=aWeekTariff.time_upto;
	res["price"]=aWeekTariff.price;
	res["pricetype"]=aWeekTariff.pricetype;
	res["free"]=aWeekTariff.free;
	res["total"]=aWeekTariff.total;
	res["counttype"]=aWeekTariff.counttype;
	res["external_name"]=aWeekTariff.external_name;
	res["identifier_rulename"]=aWeekTariff.identifier_rulename;
	res["service_rulename"]=aWeekTariff.service_rulename;
	res["identifier_categoryname"]=aWeekTariff.identifier_categoryname;
	res["ratename"]=aWeekTariff.ratename;
	res["enabled"]=aWeekTariff.enabled;
	return res;
}

void BALOONSkipassConfiguration::initialize(ParamsMapRef params) {
	RegisterMethod("MassAdd", (&BALOONSkipassConfiguration::__MassAdd));
	RegisterMethod("SkipassConfigurationListGet_API", (&BALOONSkipassConfiguration::__SkipassConfigurationListGet_API));
	RegisterMethod("SkipassConfigurationGroupNameListGet_API", (&BALOONSkipassConfiguration::__SkipassConfigurationGroupNameListGet_API));
	RegisterMethod("Add", (&BALOONSkipassConfiguration::__Add));
	RegisterMethod("Get", (&BALOONSkipassConfiguration::__Get));
	RegisterMethod("Update", (&BALOONSkipassConfiguration::__Update));
	RegisterMethod("Delete", (&BALOONSkipassConfiguration::__Delete));
	RegisterMethod("SkipassConfigurationListGet", (&BALOONSkipassConfiguration::__SkipassConfigurationListGet));
	InitDatabase(params);
}

 const Value BALOONSkipassConfiguration::__MassAdd(const Param& param) {
	return MassAdd(param["groupname"], param["configurations"]);
}

 const Value BALOONSkipassConfiguration::__SkipassConfigurationListGet_API(const Param& param) {
	return SkipassConfigurationListGet_API(param["groupname"]);
}

 const Value BALOONSkipassConfiguration::__SkipassConfigurationGroupNameListGet_API(const Param& param) {
	return SkipassConfigurationGroupNameListGet_API();
}

// Wrappers for automatic methods
const Value BALOONSkipassConfiguration::__Add(const Param& param) {
	return Add(param["groupname"], param["name"], param["identifier_rulename"], param["service_rulename"], param["identifier_categoryname"], param["ratename"], param["valid_from"], param["valid_to"], param["price"], param["make_transaction"], param["enabled"]);
}

const Value BALOONSkipassConfiguration::__Update(const Param& param) {
	return Update(param["id"], param["groupname"], param["name"], param["identifier_rulename"], param["service_rulename"], param["identifier_categoryname"], param["ratename"], param["valid_from"], param["valid_to"], param["price"], param["make_transaction"], param["enabled"]);
}

const Value BALOONSkipassConfiguration::__Delete(const Param& param) {
	return Delete(param["id"]);
}

const Value BALOONSkipassConfiguration::__Get(const Param& param) {
	return Get(param["id"]);
}

const Value BALOONSkipassConfiguration::__SkipassConfigurationListGet(const Param& param) {
	return SkipassConfigurationListGet(param["identifier_rulename"], param["service_rulename"], param["identifier_categoryname"], param["ratename"]);
}

// Implementation of automatic methods
const Value BALOONSkipassConfiguration::Add(const Str groupname, const Str name, const Str identifier_rulename, const Str service_rulename, const Str identifier_categoryname, const Str ratename, const Time valid_from, const Time valid_to, const Decimal price, const Str make_transaction, const Str enabled) {
	Exception e((Message("Cannot add ") << Message("Skipass Configuration").What() << ". ").What());
	bool error=false;
	error |= Defined(identifier_rulename)  && !__CheckIDExists(IdentifierRule()->name, *identifier_rulename, "Identifier Rule", e, rdb_);
	error |= Defined(service_rulename)  && !__CheckIDExists(SpdServiceRule()->name, *service_rulename, "Service Rule", e, rdb_);
	error |= Defined(identifier_categoryname)  && !__CheckIDExists(IdentifierCategory()->name, *identifier_categoryname, "Category", e, rdb_);
	error |= Defined(ratename)  && !__CheckIDExists(IdentifierRate()->name, *ratename, "Tariff", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		SkipassConfiguration aSkipassConfiguration;
		aSkipassConfiguration.groupname=groupname;
		aSkipassConfiguration.name=name;
		aSkipassConfiguration.identifier_rulename=identifier_rulename;
		aSkipassConfiguration.service_rulename=service_rulename;
		aSkipassConfiguration.identifier_categoryname=identifier_categoryname;
		aSkipassConfiguration.ratename=ratename;
		aSkipassConfiguration.valid_from=valid_from;
		aSkipassConfiguration.valid_to=valid_to;
		aSkipassConfiguration.price=price;
		aSkipassConfiguration.make_transaction=make_transaction;
		aSkipassConfiguration.enabled=enabled;
		Int sk=aSkipassConfiguration.Insert(rdb_);
		Value res;
		res["id"]=sk;
		AddMessage(Message()<<Message("Skipass Configuration").What()<<" "<<sk<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONSkipassConfiguration::Update(const Int id, const optional<Str> groupname, const optional<Str> name, const optional<Str> identifier_rulename, const optional<Str> service_rulename, const optional<Str> identifier_categoryname, const optional<Str> ratename, const optional<Time> valid_from, const optional<Time> valid_to, const optional<Decimal> price, const optional<Str> make_transaction, const optional<Str> enabled) {
	Exception e((Message("Cannot update ") << Message("Skipass Configuration").What() << ". ").What());
	bool error=false;
	error |= Defined(identifier_rulename) && Defined(*identifier_rulename)  && !__CheckIDExists(IdentifierRule()->name, *identifier_rulename, "Identifier Rule", e, rdb_);
	error |= Defined(service_rulename) && Defined(*service_rulename)  && !__CheckIDExists(SpdServiceRule()->name, *service_rulename, "Service Rule", e, rdb_);
	error |= Defined(identifier_categoryname) && Defined(*identifier_categoryname)  && !__CheckIDExists(IdentifierCategory()->name, *identifier_categoryname, "Category", e, rdb_);
	error |= Defined(ratename) && Defined(*ratename)  && !__CheckIDExists(IdentifierRate()->name, *ratename, "Tariff", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		SkipassConfiguration aSkipassConfiguration;
		aSkipassConfiguration.id=id;
		aSkipassConfiguration.Select(rdb_);
		if(Defined(groupname)) aSkipassConfiguration.groupname=*groupname;
		if(Defined(name)) aSkipassConfiguration.name=*name;
		if(Defined(identifier_rulename)) aSkipassConfiguration.identifier_rulename=*identifier_rulename;
		if(Defined(service_rulename)) aSkipassConfiguration.service_rulename=*service_rulename;
		if(Defined(identifier_categoryname)) aSkipassConfiguration.identifier_categoryname=*identifier_categoryname;
		if(Defined(ratename)) aSkipassConfiguration.ratename=*ratename;
		if(Defined(valid_from)) aSkipassConfiguration.valid_from=*valid_from;
		if(Defined(valid_to)) aSkipassConfiguration.valid_to=*valid_to;
		if(Defined(price)) aSkipassConfiguration.price=*price;
		if(Defined(make_transaction)) aSkipassConfiguration.make_transaction=*make_transaction;
		if(Defined(enabled)) aSkipassConfiguration.enabled=*enabled;
		aSkipassConfiguration.Update(rdb_);
		AddMessage(Message()<<Message("Skipass Configuration").What()<<" "<<aSkipassConfiguration.id<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONSkipassConfiguration::Delete(const Int id) {
	SkipassConfiguration aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Skipass Configuration").What()<<". ").What());
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.id=id;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Skipass Configuration").What()<<" "<<id<<" deleted. ");
	return Value();
}

const Value BALOONSkipassConfiguration::Get(const Int id) {
	Value res;
	SkipassConfiguration aSkipassConfiguration;
	aSkipassConfiguration.id=id;
	aSkipassConfiguration.Select(rdb_);
	res["id"]=aSkipassConfiguration.id;
	res["groupname"]=aSkipassConfiguration.groupname;
	res["name"]=aSkipassConfiguration.name;
	res["identifier_rulename"]=aSkipassConfiguration.identifier_rulename;
	res["service_rulename"]=aSkipassConfiguration.service_rulename;
	res["identifier_categoryname"]=aSkipassConfiguration.identifier_categoryname;
	res["ratename"]=aSkipassConfiguration.ratename;
	res["valid_from"]=aSkipassConfiguration.valid_from;
	res["valid_to"]=aSkipassConfiguration.valid_to;
	res["price"]=aSkipassConfiguration.price;
	res["make_transaction"]=aSkipassConfiguration.make_transaction;
	res["enabled"]=aSkipassConfiguration.enabled;
	return res;
}

const Value BALOONSkipassConfiguration::SkipassConfigurationListGet(const optional<Str> identifier_rulename, const optional<Str> service_rulename, const optional<Str> identifier_categoryname, const optional<Str> ratename) {
	Data::DataList lr;
	lr.AddColumn("id", Data::INTEGER);
	lr.AddColumn("groupname", Data::STRING);
	lr.AddColumn("name", Data::STRING);
	lr.AddColumn("identifier_rulename", Data::STRING);
	lr.AddColumn("service_rulename", Data::STRING);
	lr.AddColumn("identifier_categoryname", Data::STRING);
	lr.AddColumn("ratename", Data::STRING);
	lr.AddColumn("valid_from", Data::DATETIME);
	lr.AddColumn("valid_to", Data::DATETIME);
	lr.AddColumn("price", Data::DECIMAL);
	lr.AddColumn("make_transaction", Data::STRING, BOOLValues());
	lr.AddColumn("enabled", Data::STRING, ENABLED_STATUSValues());
	SkipassConfiguration aSkipassConfiguration;
	IdentifierRule aidentifier_rule;
	SpdServiceRule aservice_rule;
	IdentifierCategory aidentifier_category;
	IdentifierRate arate;
	lr.Bind(aSkipassConfiguration.id, "id");
	lr.Bind(aSkipassConfiguration.groupname, "groupname");
	lr.Bind(aSkipassConfiguration.name, "name");
	lr.Bind(aidentifier_rule.name, "identifier_rulename");
	lr.Bind(aservice_rule.name, "service_rulename");
	lr.Bind(aidentifier_category.name, "identifier_categoryname");
	lr.Bind(arate.name, "ratename");
	lr.Bind(aSkipassConfiguration.valid_from, "valid_from");
	lr.Bind(aSkipassConfiguration.valid_to, "valid_to");
	lr.Bind(aSkipassConfiguration.price, "price");
	lr.Bind(aSkipassConfiguration.make_transaction, "make_transaction");
	lr.Bind(aSkipassConfiguration.enabled, "enabled");
	Selector sel;
	sel << aSkipassConfiguration->id << aSkipassConfiguration->groupname << aSkipassConfiguration->name << aidentifier_rule->name << aservice_rule->name << aidentifier_category->name << arate->name << aSkipassConfiguration->valid_from << aSkipassConfiguration->valid_to << aSkipassConfiguration->price << aSkipassConfiguration->make_transaction << aSkipassConfiguration->enabled;
	sel.Join(aidentifier_rule).On(aSkipassConfiguration->identifier_rulename==aidentifier_rule->name).LeftJoin(aservice_rule).On(aSkipassConfiguration->service_rulename==aservice_rule->name).Join(aidentifier_category).On(aSkipassConfiguration->identifier_categoryname==aidentifier_category->name).Join(arate).On(aSkipassConfiguration->ratename==arate->name);
	sel.Where( (Defined(identifier_rulename) ? aSkipassConfiguration->identifier_rulename==*identifier_rulename : Expression()) &&  (Defined(service_rulename) ? aSkipassConfiguration->service_rulename==*service_rulename : Expression()) &&  (Defined(identifier_categoryname) ? aSkipassConfiguration->identifier_categoryname==*identifier_categoryname : Expression()) &&  (Defined(ratename) ? aSkipassConfiguration->ratename==*ratename : Expression()));
	DataSet data=sel.Execute(rdb_);
	while(data.Fetch()) lr.AddRow();
	Value res=lr;
	return res;
}

void BALOONSpdGate::initialize(ParamsMapRef params) {
	RegisterMethod("Add", (&BALOONSpdGate::__Add));
	RegisterMethod("Get", (&BALOONSpdGate::__Get));
	RegisterMethod("Update", (&BALOONSpdGate::__Update));
	RegisterMethod("Delete", (&BALOONSpdGate::__Delete));
	RegisterMethod("SpdGateListGet", (&BALOONSpdGate::__SpdGateListGet));
	InitDatabase(params);
}

// Wrappers for automatic methods
const Value BALOONSpdGate::__Add(const Param& param) {
	return Add(param["name"], param["host"], param["port"], param["enabled"]);
}

const Value BALOONSpdGate::__Update(const Param& param) {
	return Update(param["id"], param["name"], param["host"], param["port"], param["enabled"]);
}

const Value BALOONSpdGate::__Delete(const Param& param) {
	return Delete(param["id"]);
}

const Value BALOONSpdGate::__Get(const Param& param) {
	return Get(param["id"]);
}

const Value BALOONSpdGate::__SpdGateListGet(const Param& param) {
	return SpdGateListGet();
}

// Implementation of automatic methods
const Value BALOONSpdGate::Add(const Str name, const Str host, const Str port, const Str enabled) {
	Exception e((Message("Cannot add ") << Message("Spd Gate").What() << ". ").What());
	try {
		SpdGate aSpdGate;
		aSpdGate.name=name;
		aSpdGate.host=host;
		aSpdGate.port=port;
		aSpdGate.enabled=enabled;
		Int sk=aSpdGate.Insert(rdb_);
		Value res;
		res["id"]=sk;
		AddMessage(Message()<<Message("Spd Gate").What()<<" "<<sk<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONSpdGate::Update(const Int id, const optional<Str> name, const optional<Str> host, const optional<Str> port, const optional<Str> enabled) {
	Exception e((Message("Cannot update ") << Message("Spd Gate").What() << ". ").What());
	try {
		SpdGate aSpdGate;
		aSpdGate.id=id;
		aSpdGate.Select(rdb_);
		if(Defined(name)) aSpdGate.name=*name;
		if(Defined(host)) aSpdGate.host=*host;
		if(Defined(port)) aSpdGate.port=*port;
		if(Defined(enabled)) aSpdGate.enabled=*enabled;
		aSpdGate.Update(rdb_);
		AddMessage(Message()<<Message("Spd Gate").What()<<" "<<aSpdGate.id<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONSpdGate::Delete(const Int id) {
	SpdGate aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Spd Gate").What()<<". ").What());
	//check Identifier reference to SpdGate reference 
	error |= !__CheckIDNotExists(Identifier()->spdgateid, id, "Identifier", e, rdb_);
	//check ParkingArea reference to SpdGate reference 
	error |= !__CheckIDNotExists(ParkingArea()->spdgateid, id, "Parking Area", e, rdb_);
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.id=id;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Spd Gate").What()<<" "<<id<<" deleted. ");
	return Value();
}

const Value BALOONSpdGate::Get(const Int id) {
	Value res;
	SpdGate aSpdGate;
	aSpdGate.id=id;
	aSpdGate.Select(rdb_);
	res["id"]=aSpdGate.id;
	res["name"]=aSpdGate.name;
	res["host"]=aSpdGate.host;
	res["port"]=aSpdGate.port;
	res["enabled"]=aSpdGate.enabled;
	return res;
}

const Value BALOONSpdGate::SpdGateListGet() {
	Data::DataList lr;
	lr.AddColumn("id", Data::INTEGER);
	lr.AddColumn("name", Data::STRING);
	lr.AddColumn("host", Data::STRING);
	lr.AddColumn("port", Data::STRING);
	lr.AddColumn("enabled", Data::STRING, ENABLED_STATUSValues());
	SpdGate aSpdGate;
	lr.Bind(aSpdGate.id, "id");
	lr.Bind(aSpdGate.name, "name");
	lr.Bind(aSpdGate.host, "host");
	lr.Bind(aSpdGate.port, "port");
	lr.Bind(aSpdGate.enabled, "enabled");
	Selector sel;
	sel << aSpdGate->id << aSpdGate->name << aSpdGate->host << aSpdGate->port << aSpdGate->enabled;
	sel.Where();
	DataSet data=sel.Execute(rdb_);
	while(data.Fetch()) lr.AddRow();
	Value res=lr;
	return res;
}

void BALOONPaidServiceCategory::initialize(ParamsMapRef params) {
	RegisterMethod("MassAdd", (&BALOONPaidServiceCategory::__MassAdd));
	RegisterMethod("PaidServiceCategoryListGet_FE", (&BALOONPaidServiceCategory::__PaidServiceCategoryListGet_FE));
	RegisterMethod("Add", (&BALOONPaidServiceCategory::__Add));
	RegisterMethod("Get", (&BALOONPaidServiceCategory::__Get));
	RegisterMethod("Update", (&BALOONPaidServiceCategory::__Update));
	RegisterMethod("Delete", (&BALOONPaidServiceCategory::__Delete));
	RegisterMethod("PaidServiceCategoryListGet", (&BALOONPaidServiceCategory::__PaidServiceCategoryListGet));
	InitDatabase(params);
}

 const Value BALOONPaidServiceCategory::__MassAdd(const Param& param) {
	return MassAdd(param["names"]);
}

 const Value BALOONPaidServiceCategory::__PaidServiceCategoryListGet_FE(const Param& param) {
	return PaidServiceCategoryListGet_FE();
}

// Wrappers for automatic methods
const Value BALOONPaidServiceCategory::__Add(const Param& param) {
	return Add(param["name"], param["status"]);
}

const Value BALOONPaidServiceCategory::__Update(const Param& param) {
	return Update(param["id"], param["name"], param["status"]);
}

const Value BALOONPaidServiceCategory::__Delete(const Param& param) {
	return Delete(param["id"]);
}

const Value BALOONPaidServiceCategory::__Get(const Param& param) {
	return Get(param["id"]);
}

const Value BALOONPaidServiceCategory::__PaidServiceCategoryListGet(const Param& param) {
	return PaidServiceCategoryListGet();
}

// Implementation of automatic methods
const Value BALOONPaidServiceCategory::Add(const Str name, const Str status) {
	Exception e((Message("Cannot add ") << Message("Paid Service Category").What() << ". ").What());
	try {
		PaidServiceCategory aPaidServiceCategory;
		aPaidServiceCategory.name=name;
		aPaidServiceCategory.status=status;
		Int sk=aPaidServiceCategory.Insert(rdb_);
		Value res;
		res["id"]=sk;
		AddMessage(Message()<<Message("Paid Service Category").What()<<" "<<sk<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONPaidServiceCategory::Update(const Int id, const optional<Str> name, const optional<Str> status) {
	Exception e((Message("Cannot update ") << Message("Paid Service Category").What() << ". ").What());
	try {
		PaidServiceCategory aPaidServiceCategory;
		aPaidServiceCategory.id=id;
		aPaidServiceCategory.Select(rdb_);
		if(Defined(name)) aPaidServiceCategory.name=*name;
		if(Defined(status)) aPaidServiceCategory.status=*status;
		aPaidServiceCategory.Update(rdb_);
		AddMessage(Message()<<Message("Paid Service Category").What()<<" "<<aPaidServiceCategory.id<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONPaidServiceCategory::Delete(const Int id) {
	PaidServiceCategory aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Paid Service Category").What()<<". ").What());
	//check PaidService reference to PaidServiceCategory reference 
	error |= !__CheckIDNotExists(PaidService()->categoryid, id, "Paid Service", e, rdb_);
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.id=id;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Paid Service Category").What()<<" "<<id<<" deleted. ");
	return Value();
}

const Value BALOONPaidServiceCategory::Get(const Int id) {
	Value res;
	PaidServiceCategory aPaidServiceCategory;
	aPaidServiceCategory.id=id;
	aPaidServiceCategory.Select(rdb_);
	res["id"]=aPaidServiceCategory.id;
	res["name"]=aPaidServiceCategory.name;
	res["status"]=aPaidServiceCategory.status;
	return res;
}

const Value BALOONPaidServiceCategory::PaidServiceCategoryListGet() {
	Data::DataList lr;
	lr.AddColumn("id", Data::INTEGER);
	lr.AddColumn("name", Data::STRING);
	lr.AddColumn("status", Data::STRING, ENABLED_STATUSValues());
	PaidServiceCategory aPaidServiceCategory;
	lr.Bind(aPaidServiceCategory.id, "id");
	lr.Bind(aPaidServiceCategory.name, "name");
	lr.Bind(aPaidServiceCategory.status, "status");
	Selector sel;
	sel << aPaidServiceCategory->id << aPaidServiceCategory->name << aPaidServiceCategory->status;
	sel.Where();
	DataSet data=sel.Execute(rdb_);
	while(data.Fetch()) lr.AddRow();
	Value res=lr;
	return res;
}

void BALOONPaidServiceTariff::initialize(ParamsMapRef params) {
	RegisterMethod("MassAdd", (&BALOONPaidServiceTariff::__MassAdd));
	RegisterMethod("Add", (&BALOONPaidServiceTariff::__Add));
	RegisterMethod("Get", (&BALOONPaidServiceTariff::__Get));
	RegisterMethod("Update", (&BALOONPaidServiceTariff::__Update));
	RegisterMethod("Delete", (&BALOONPaidServiceTariff::__Delete));
	RegisterMethod("PaidServiceTariffListGet", (&BALOONPaidServiceTariff::__PaidServiceTariffListGet));
	InitDatabase(params);
}

 const Value BALOONPaidServiceTariff::__MassAdd(const Param& param) {
	return MassAdd(param["names"]);
}

// Wrappers for automatic methods
const Value BALOONPaidServiceTariff::__Add(const Param& param) {
	return Add(param["name"], param["status"]);
}

const Value BALOONPaidServiceTariff::__Update(const Param& param) {
	return Update(param["id"], param["name"], param["status"]);
}

const Value BALOONPaidServiceTariff::__Delete(const Param& param) {
	return Delete(param["id"]);
}

const Value BALOONPaidServiceTariff::__Get(const Param& param) {
	return Get(param["id"]);
}

const Value BALOONPaidServiceTariff::__PaidServiceTariffListGet(const Param& param) {
	return PaidServiceTariffListGet();
}

// Implementation of automatic methods
const Value BALOONPaidServiceTariff::Add(const Str name, const Str status) {
	Exception e((Message("Cannot add ") << Message("Paid Service Tariff").What() << ". ").What());
	try {
		PaidServiceTariff aPaidServiceTariff;
		aPaidServiceTariff.name=name;
		aPaidServiceTariff.status=status;
		Int sk=aPaidServiceTariff.Insert(rdb_);
		Value res;
		res["id"]=sk;
		AddMessage(Message()<<Message("Paid Service Tariff").What()<<" "<<sk<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONPaidServiceTariff::Update(const Int id, const optional<Str> name, const optional<Str> status) {
	Exception e((Message("Cannot update ") << Message("Paid Service Tariff").What() << ". ").What());
	try {
		PaidServiceTariff aPaidServiceTariff;
		aPaidServiceTariff.id=id;
		aPaidServiceTariff.Select(rdb_);
		if(Defined(name)) aPaidServiceTariff.name=*name;
		if(Defined(status)) aPaidServiceTariff.status=*status;
		aPaidServiceTariff.Update(rdb_);
		AddMessage(Message()<<Message("Paid Service Tariff").What()<<" "<<aPaidServiceTariff.id<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONPaidServiceTariff::Delete(const Int id) {
	PaidServiceTariff aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Paid Service Tariff").What()<<". ").What());
	//check PaidService reference to PaidServiceTariff reference 
	error |= !__CheckIDNotExists(PaidService()->tariffid, id, "Paid Service", e, rdb_);
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.id=id;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Paid Service Tariff").What()<<" "<<id<<" deleted. ");
	return Value();
}

const Value BALOONPaidServiceTariff::Get(const Int id) {
	Value res;
	PaidServiceTariff aPaidServiceTariff;
	aPaidServiceTariff.id=id;
	aPaidServiceTariff.Select(rdb_);
	res["id"]=aPaidServiceTariff.id;
	res["name"]=aPaidServiceTariff.name;
	res["status"]=aPaidServiceTariff.status;
	return res;
}

const Value BALOONPaidServiceTariff::PaidServiceTariffListGet() {
	Data::DataList lr;
	lr.AddColumn("id", Data::INTEGER);
	lr.AddColumn("name", Data::STRING);
	lr.AddColumn("status", Data::STRING, ENABLED_STATUSValues());
	PaidServiceTariff aPaidServiceTariff;
	lr.Bind(aPaidServiceTariff.id, "id");
	lr.Bind(aPaidServiceTariff.name, "name");
	lr.Bind(aPaidServiceTariff.status, "status");
	Selector sel;
	sel << aPaidServiceTariff->id << aPaidServiceTariff->name << aPaidServiceTariff->status;
	sel.Where();
	DataSet data=sel.Execute(rdb_);
	while(data.Fetch()) lr.AddRow();
	Value res=lr;
	return res;
}

void BALOONPaidServiceHour::initialize(ParamsMapRef params) {
	RegisterMethod("MassAdd", (&BALOONPaidServiceHour::__MassAdd));
	RegisterMethod("Add", (&BALOONPaidServiceHour::__Add));
	RegisterMethod("Get", (&BALOONPaidServiceHour::__Get));
	RegisterMethod("Update", (&BALOONPaidServiceHour::__Update));
	RegisterMethod("Delete", (&BALOONPaidServiceHour::__Delete));
	RegisterMethod("PaidServiceHourListGet", (&BALOONPaidServiceHour::__PaidServiceHourListGet));
	InitDatabase(params);
}

 const Value BALOONPaidServiceHour::__MassAdd(const Param& param) {
	return MassAdd(param["names"]);
}

// Wrappers for automatic methods
const Value BALOONPaidServiceHour::__Add(const Param& param) {
	return Add(param["name"], param["status"]);
}

const Value BALOONPaidServiceHour::__Update(const Param& param) {
	return Update(param["id"], param["name"], param["status"]);
}

const Value BALOONPaidServiceHour::__Delete(const Param& param) {
	return Delete(param["id"]);
}

const Value BALOONPaidServiceHour::__Get(const Param& param) {
	return Get(param["id"]);
}

const Value BALOONPaidServiceHour::__PaidServiceHourListGet(const Param& param) {
	return PaidServiceHourListGet();
}

// Implementation of automatic methods
const Value BALOONPaidServiceHour::Add(const Str name, const Str status) {
	Exception e((Message("Cannot add ") << Message("Paid Service Hour").What() << ". ").What());
	try {
		PaidServiceHour aPaidServiceHour;
		aPaidServiceHour.name=name;
		aPaidServiceHour.status=status;
		Int sk=aPaidServiceHour.Insert(rdb_);
		Value res;
		res["id"]=sk;
		AddMessage(Message()<<Message("Paid Service Hour").What()<<" "<<sk<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONPaidServiceHour::Update(const Int id, const optional<Str> name, const optional<Str> status) {
	Exception e((Message("Cannot update ") << Message("Paid Service Hour").What() << ". ").What());
	try {
		PaidServiceHour aPaidServiceHour;
		aPaidServiceHour.id=id;
		aPaidServiceHour.Select(rdb_);
		if(Defined(name)) aPaidServiceHour.name=*name;
		if(Defined(status)) aPaidServiceHour.status=*status;
		aPaidServiceHour.Update(rdb_);
		AddMessage(Message()<<Message("Paid Service Hour").What()<<" "<<aPaidServiceHour.id<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONPaidServiceHour::Delete(const Int id) {
	PaidServiceHour aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Paid Service Hour").What()<<". ").What());
	//check PaidService reference to PaidServiceHour reference 
	error |= !__CheckIDNotExists(PaidService()->hourid, id, "Paid Service", e, rdb_);
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.id=id;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Paid Service Hour").What()<<" "<<id<<" deleted. ");
	return Value();
}

const Value BALOONPaidServiceHour::Get(const Int id) {
	Value res;
	PaidServiceHour aPaidServiceHour;
	aPaidServiceHour.id=id;
	aPaidServiceHour.Select(rdb_);
	res["id"]=aPaidServiceHour.id;
	res["name"]=aPaidServiceHour.name;
	res["status"]=aPaidServiceHour.status;
	return res;
}

const Value BALOONPaidServiceHour::PaidServiceHourListGet() {
	Data::DataList lr;
	lr.AddColumn("id", Data::INTEGER);
	lr.AddColumn("name", Data::STRING);
	lr.AddColumn("status", Data::STRING, ENABLED_STATUSValues());
	PaidServiceHour aPaidServiceHour;
	lr.Bind(aPaidServiceHour.id, "id");
	lr.Bind(aPaidServiceHour.name, "name");
	lr.Bind(aPaidServiceHour.status, "status");
	Selector sel;
	sel << aPaidServiceHour->id << aPaidServiceHour->name << aPaidServiceHour->status;
	sel.Where();
	DataSet data=sel.Execute(rdb_);
	while(data.Fetch()) lr.AddRow();
	Value res=lr;
	return res;
}

void BALOONPaidService::initialize(ParamsMapRef params) {
	RegisterMethod("MassAdd", (&BALOONPaidService::__MassAdd));
	RegisterMethod("ImportPricelist", (&BALOONPaidService::__ImportPricelist));
	RegisterMethod("PaidServiceListGet_FE", (&BALOONPaidService::__PaidServiceListGet_FE));
	RegisterMethod("PaidServiceListGet_API", (&BALOONPaidService::__PaidServiceListGet_API));
	RegisterMethod("Add", (&BALOONPaidService::__Add));
	RegisterMethod("Get", (&BALOONPaidService::__Get));
	RegisterMethod("Update", (&BALOONPaidService::__Update));
	RegisterMethod("Delete", (&BALOONPaidService::__Delete));
	RegisterMethod("PaidServiceListGet", (&BALOONPaidService::__PaidServiceListGet));
	InitDatabase(params);
}

 const Value BALOONPaidService::__MassAdd(const Param& param) {
	return MassAdd(param["services"]);
}

 const Value BALOONPaidService::__ImportPricelist(const Param& param) {
	return ImportPricelist(param["pricelist"]);
}

 const Value BALOONPaidService::__PaidServiceListGet_FE(const Param& param) {
	return PaidServiceListGet_FE(param["categoryid"]);
}

 const Value BALOONPaidService::__PaidServiceListGet_API(const Param& param) {
	return PaidServiceListGet_API(param["categoryid"]);
}

// Wrappers for automatic methods
const Value BALOONPaidService::__Add(const Param& param) {
	return Add(param["categoryid"], param["name"], param["tariffid"], param["hourid"], param["description"], param["price"], param["status"]);
}

const Value BALOONPaidService::__Update(const Param& param) {
	return Update(param["id"], param["categoryid"], param["name"], param["tariffid"], param["hourid"], param["description"], param["price"], param["status"]);
}

const Value BALOONPaidService::__Delete(const Param& param) {
	return Delete(param["id"]);
}

const Value BALOONPaidService::__Get(const Param& param) {
	return Get(param["id"]);
}

const Value BALOONPaidService::__PaidServiceListGet(const Param& param) {
	return PaidServiceListGet(param["categoryid"], param["tariffid"], param["hourid"]);
}

// Implementation of automatic methods
const Value BALOONPaidService::Add(const Int categoryid, const Str name, const Int tariffid, const Int hourid, const Str description, const Decimal price, const Str status) {
	Exception e((Message("Cannot add ") << Message("Paid Service").What() << ". ").What());
	bool error=false;
	error |= Defined(categoryid)  && !__CheckIDExists(PaidServiceCategory()->id, *categoryid, "Category", e, rdb_);
	error |= Defined(tariffid)  && !__CheckIDExists(PaidServiceTariff()->id, *tariffid, "Tariff", e, rdb_);
	error |= Defined(hourid)  && !__CheckIDExists(PaidServiceHour()->id, *hourid, "Hours", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		PaidService aPaidService;
		aPaidService.categoryid=categoryid;
		aPaidService.name=name;
		aPaidService.tariffid=tariffid;
		aPaidService.hourid=hourid;
		aPaidService.description=description;
		aPaidService.price=price;
		aPaidService.status=status;
		Int sk=aPaidService.Insert(rdb_);
		Value res;
		res["id"]=sk;
		AddMessage(Message()<<Message("Paid Service").What()<<" "<<sk<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONPaidService::Update(const Int id, const optional<Int> categoryid, const optional<Str> name, const optional<Int> tariffid, const optional<Int> hourid, const optional<Str> description, const optional<Decimal> price, const optional<Str> status) {
	Exception e((Message("Cannot update ") << Message("Paid Service").What() << ". ").What());
	bool error=false;
	error |= Defined(categoryid) && Defined(*categoryid)  && !__CheckIDExists(PaidServiceCategory()->id, *categoryid, "Category", e, rdb_);
	error |= Defined(tariffid) && Defined(*tariffid)  && !__CheckIDExists(PaidServiceTariff()->id, *tariffid, "Tariff", e, rdb_);
	error |= Defined(hourid) && Defined(*hourid)  && !__CheckIDExists(PaidServiceHour()->id, *hourid, "Hours", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		PaidService aPaidService;
		aPaidService.id=id;
		aPaidService.Select(rdb_);
		if(Defined(categoryid)) aPaidService.categoryid=*categoryid;
		if(Defined(name)) aPaidService.name=*name;
		if(Defined(tariffid)) aPaidService.tariffid=*tariffid;
		if(Defined(hourid)) aPaidService.hourid=*hourid;
		if(Defined(description)) aPaidService.description=*description;
		if(Defined(price)) aPaidService.price=*price;
		if(Defined(status)) aPaidService.status=*status;
		aPaidService.Update(rdb_);
		AddMessage(Message()<<Message("Paid Service").What()<<" "<<aPaidService.id<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONPaidService::Delete(const Int id) {
	PaidService aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Paid Service").What()<<". ").What());
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.id=id;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Paid Service").What()<<" "<<id<<" deleted. ");
	return Value();
}

const Value BALOONPaidService::Get(const Int id) {
	Value res;
	PaidService aPaidService;
	aPaidService.id=id;
	aPaidService.Select(rdb_);
	res["id"]=aPaidService.id;
	res["categoryid"]=aPaidService.categoryid;
	res["name"]=aPaidService.name;
	res["tariffid"]=aPaidService.tariffid;
	res["hourid"]=aPaidService.hourid;
	res["description"]=aPaidService.description;
	res["price"]=aPaidService.price;
	res["status"]=aPaidService.status;
	return res;
}

const Value BALOONPaidService::PaidServiceListGet(const optional<Int> categoryid, const optional<Int> tariffid, const optional<Int> hourid) {
	Data::DataList lr;
	lr.AddColumn("id", Data::INTEGER);
	lr.AddColumn("categoryname", Data::STRING);
	lr.AddColumn("name", Data::STRING);
	lr.AddColumn("hourname", Data::STRING);
	lr.AddColumn("tariffname", Data::STRING);
	lr.AddColumn("description", Data::STRING);
	lr.AddColumn("price", Data::DECIMAL);
	PaidService aPaidService;
	PaidServiceCategory acategory;
	PaidServiceHour ahour;
	PaidServiceTariff atariff;
	lr.Bind(aPaidService.id, "id");
	lr.Bind(acategory.name, "categoryname");
	lr.Bind(aPaidService.name, "name");
	lr.Bind(ahour.name, "hourname");
	lr.Bind(atariff.name, "tariffname");
	lr.Bind(aPaidService.description, "description");
	lr.Bind(aPaidService.price, "price");
	Selector sel;
	sel << aPaidService->id << acategory->name << aPaidService->name << ahour->name << atariff->name << aPaidService->description << aPaidService->price;
	sel.LeftJoin(acategory).On(aPaidService->categoryid==acategory->id).LeftJoin(ahour).On(aPaidService->hourid==ahour->id).LeftJoin(atariff).On(aPaidService->tariffid==atariff->id);
	sel.Where( (Defined(categoryid) ? aPaidService->categoryid==*categoryid : Expression()) &&  (Defined(tariffid) ? aPaidService->tariffid==*tariffid : Expression()) &&  (Defined(hourid) ? aPaidService->hourid==*hourid : Expression()));
	DataSet data=sel.Execute(rdb_);
	while(data.Fetch()) lr.AddRow();
	Value res=lr;
	return res;
}

void BALOONPartner::initialize(ParamsMapRef params) {
	RegisterMethod("PartnerListGet", (&BALOONPartner::__PartnerListGet));
	RegisterMethod("Add", (&BALOONPartner::__Add));
	RegisterMethod("OurGet", (&BALOONPartner::__OurGet));
	RegisterMethod("Get", (&BALOONPartner::__Get));
	RegisterMethod("Update", (&BALOONPartner::__Update));
	RegisterMethod("Delete", (&BALOONPartner::__Delete));
	InitDatabase(params);
}

 const Value BALOONPartner::__PartnerListGet(const Param& param) {
	return PartnerListGet();
}

 const Value BALOONPartner::__Add(const Param& param) {
	return Add(param["name"], param["legal_address"], param["post_address"], param["inn"], param["kpp"], param["payment_account"], param["correspondent_account"], param["bank"], param["bik"], param["okpo"], param["okato"], param["okved"], param["ogrn"], param["principal"], param["phone"], param["fax"], param["email"], param["notification_email"], param["contractbarcode"], param["start_date"], param["end_date"], param["benefit_type"], param["benefit_amount"], param["accountname"], param["amount"], param["reason"], param["managerfullname"], param["passport_number"], param["issue_date"], param["issued_by"], param["department_code"], param["managerphone"], param["manageremail"], param["password"], param["password2"]);
}

 const Value BALOONPartner::__OurGet(const Param& param) {
	return OurGet();
}

// Wrappers for automatic methods
const Value BALOONPartner::__Update(const Param& param) {
	return Update(param["id"], param["name"], param["status"], param["legal_address"], param["post_address"], param["inn"], param["kpp"], param["payment_account"], param["correspondent_account"], param["bank"], param["bik"], param["okpo"], param["okato"], param["okved"], param["ogrn"], param["principal"], param["phone"], param["fax"], param["email"], param["token"], param["notification_email"]);
}

const Value BALOONPartner::__Delete(const Param& param) {
	return Delete(param["id"]);
}

const Value BALOONPartner::__Get(const Param& param) {
	return Get(param["id"]);
}

// Implementation of automatic methods
#if 0 //BALOONPartner Add
const Value BALOONPartner::Add(const Str name, const Str status, const Str legal_address, const Str post_address, const Str inn, const Str kpp, const Str payment_account, const Str correspondent_account, const Str bank, const Str bik, const Str okpo, const Str okato, const Str okved, const Str ogrn, const Str principal, const Str phone, const Str fax, const Str email, const Str token, const Str notification_email) {
	Exception e((Message("Cannot add ") << Message("Partner").What() << ". ").What());
	try {
		Partner aPartner;
		aPartner.name=name;
		aPartner.status=status;
		aPartner.legal_address=legal_address;
		aPartner.post_address=post_address;
		aPartner.inn=inn;
		aPartner.kpp=kpp;
		aPartner.payment_account=payment_account;
		aPartner.correspondent_account=correspondent_account;
		aPartner.bank=bank;
		aPartner.bik=bik;
		aPartner.okpo=okpo;
		aPartner.okato=okato;
		aPartner.okved=okved;
		aPartner.ogrn=ogrn;
		aPartner.principal=principal;
		aPartner.phone=phone;
		aPartner.fax=fax;
		aPartner.email=email;
		aPartner.token=token;
		aPartner.notification_email=notification_email;
		Int sk=aPartner.Insert(rdb_);
		Value res;
		res["id"]=sk;
		AddMessage(Message()<<Message("Partner").What()<<" "<<sk<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

#endif //BALOONPartner Add

const Value BALOONPartner::Update(const Int id, const optional<Str> name, const optional<Str> status, const optional<Str> legal_address, const optional<Str> post_address, const optional<Str> inn, const optional<Str> kpp, const optional<Str> payment_account, const optional<Str> correspondent_account, const optional<Str> bank, const optional<Str> bik, const optional<Str> okpo, const optional<Str> okato, const optional<Str> okved, const optional<Str> ogrn, const optional<Str> principal, const optional<Str> phone, const optional<Str> fax, const optional<Str> email, const optional<Str> token, const optional<Str> notification_email) {
	Exception e((Message("Cannot update ") << Message("Partner").What() << ". ").What());
	try {
		Partner aPartner;
		aPartner.id=id;
		aPartner.Select(rdb_);
		if(Defined(name)) aPartner.name=*name;
		if(Defined(status)) aPartner.status=*status;
		if(Defined(legal_address)) aPartner.legal_address=*legal_address;
		if(Defined(post_address)) aPartner.post_address=*post_address;
		if(Defined(inn)) aPartner.inn=*inn;
		if(Defined(kpp)) aPartner.kpp=*kpp;
		if(Defined(payment_account)) aPartner.payment_account=*payment_account;
		if(Defined(correspondent_account)) aPartner.correspondent_account=*correspondent_account;
		if(Defined(bank)) aPartner.bank=*bank;
		if(Defined(bik)) aPartner.bik=*bik;
		if(Defined(okpo)) aPartner.okpo=*okpo;
		if(Defined(okato)) aPartner.okato=*okato;
		if(Defined(okved)) aPartner.okved=*okved;
		if(Defined(ogrn)) aPartner.ogrn=*ogrn;
		if(Defined(principal)) aPartner.principal=*principal;
		if(Defined(phone)) aPartner.phone=*phone;
		if(Defined(fax)) aPartner.fax=*fax;
		if(Defined(email)) aPartner.email=*email;
		if(Defined(token)) aPartner.token=*token;
		if(Defined(notification_email)) aPartner.notification_email=*notification_email;
		aPartner.Update(rdb_);
		AddMessage(Message()<<Message("Partner").What()<<" "<<aPartner.id<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONPartner::Delete(const Int id) {
	Partner aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Partner").What()<<". ").What());
	//check PartnerManager reference to Partner reference 
	error |= !__CheckIDNotExists(PartnerManager()->partnerid, id, "Person In Charge", e, rdb_);
	//check PartnerContract reference to Partner reference 
	error |= !__CheckIDNotExists(PartnerContract()->partnerid, id, "Partner Contract", e, rdb_);
	//check PartnerAccount reference to Partner reference 
	error |= !__CheckIDNotExists(PartnerAccount()->partnerid, id, "Account", e, rdb_);
	//check PartnerOrder reference to Partner reference 
	error |= !__CheckIDNotExists(PartnerOrder()->partnerid, id, "Order", e, rdb_);
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.id=id;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Partner").What()<<" "<<id<<" deleted. ");
	return Value();
}

const Value BALOONPartner::Get(const Int id) {
	Value res;
	Partner aPartner;
	aPartner.id=id;
	aPartner.Select(rdb_);
	res["id"]=aPartner.id;
	res["name"]=aPartner.name;
	res["status"]=aPartner.status;
	res["legal_address"]=aPartner.legal_address;
	res["post_address"]=aPartner.post_address;
	res["inn"]=aPartner.inn;
	res["kpp"]=aPartner.kpp;
	res["payment_account"]=aPartner.payment_account;
	res["correspondent_account"]=aPartner.correspondent_account;
	res["bank"]=aPartner.bank;
	res["bik"]=aPartner.bik;
	res["okpo"]=aPartner.okpo;
	res["okato"]=aPartner.okato;
	res["okved"]=aPartner.okved;
	res["ogrn"]=aPartner.ogrn;
	res["principal"]=aPartner.principal;
	res["phone"]=aPartner.phone;
	res["fax"]=aPartner.fax;
	res["email"]=aPartner.email;
	res["token"]=aPartner.token;
	res["notification_email"]=aPartner.notification_email;
	return res;
}

void BALOONPartnerAdministrator::initialize(ParamsMapRef params) {
	RegisterMethod("Add", (&BALOONPartnerAdministrator::__Add));
	RegisterMethod("GetCurrent", (&BALOONPartnerAdministrator::__GetCurrent));
	RegisterMethod("Get", (&BALOONPartnerAdministrator::__Get));
	RegisterMethod("Update", (&BALOONPartnerAdministrator::__Update));
	RegisterMethod("Delete", (&BALOONPartnerAdministrator::__Delete));
	RegisterMethod("PartnerAdministratorListGet", (&BALOONPartnerAdministrator::__PartnerAdministratorListGet));
	InitDatabase(params);
}

 const Value BALOONPartnerAdministrator::__Add(const Param& param) {
	return Add(param["fullname"], param["email"], param["password"], param["password2"]);
}

 const Value BALOONPartnerAdministrator::__GetCurrent(const Param& param) {
	return GetCurrent();
}

// Wrappers for automatic methods
const Value BALOONPartnerAdministrator::__Update(const Param& param) {
	return Update(param["auserTokenID"], param["fullname"]);
}

const Value BALOONPartnerAdministrator::__Delete(const Param& param) {
	return Delete(param["auserTokenID"]);
}

const Value BALOONPartnerAdministrator::__Get(const Param& param) {
	return Get(param["auserTokenID"]);
}

const Value BALOONPartnerAdministrator::__PartnerAdministratorListGet(const Param& param) {
	return PartnerAdministratorListGet(param["auserTokenID"]);
}

// Implementation of automatic methods
#if 0 //BALOONPartnerAdministrator Add
const Value BALOONPartnerAdministrator::Add(const Int64 auserTokenID, const Str fullname) {
	Exception e((Message("Cannot add ") << Message("Partner Adminstrator").What() << ". ").What());
	bool error=false;
	error |= Defined(auserTokenID)  && !__CheckIDExists(Users()->TokenID, *auserTokenID, "ID", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		PartnerAdministrator aPartnerAdministrator;
		aPartnerAdministrator.auserTokenID=auserTokenID;
		aPartnerAdministrator.fullname=fullname;
	aPartnerAdministrator.Insert(rdb_);
		Value res;
		AddMessage(Message()<<Message("Partner Adminstrator").What()<<" "<<aPartnerAdministrator.auserTokenID<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

#endif //BALOONPartnerAdministrator Add

const Value BALOONPartnerAdministrator::Update(const Int64 auserTokenID, const optional<Str> fullname) {
	Exception e((Message("Cannot update ") << Message("Partner Adminstrator").What() << ". ").What());
	bool error=false;
	error |= Defined(auserTokenID)  && !__CheckIDExists(Users()->TokenID, *auserTokenID, "ID", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		PartnerAdministrator aPartnerAdministrator;
		aPartnerAdministrator.auserTokenID=auserTokenID;
		aPartnerAdministrator.Select(rdb_);
		if(Defined(fullname)) aPartnerAdministrator.fullname=*fullname;
		aPartnerAdministrator.Update(rdb_);
		AddMessage(Message()<<Message("Partner Adminstrator").What()<<" "<<aPartnerAdministrator.auserTokenID<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONPartnerAdministrator::Delete(const Int64 auserTokenID) {
	PartnerAdministrator aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Partner Adminstrator").What()<<". ").What());
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.auserTokenID=auserTokenID;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Partner Adminstrator").What()<<" "<<auserTokenID<<" deleted. ");
	return Value();
}

const Value BALOONPartnerAdministrator::Get(const Int64 auserTokenID) {
	Value res;
	PartnerAdministrator aPartnerAdministrator;
	aPartnerAdministrator.auserTokenID=auserTokenID;
	aPartnerAdministrator.Select(rdb_);
	res["auserTokenID"]=aPartnerAdministrator.auserTokenID;
	res["fullname"]=aPartnerAdministrator.fullname;
	return res;
}

const Value BALOONPartnerAdministrator::PartnerAdministratorListGet(const optional<Int64> auserTokenID) {
	Data::DataList lr;
	lr.AddColumn("auserTokenID", Data::INT64);
	lr.AddColumn("fullname", Data::STRING);
	lr.AddColumn("auserLogin", Data::STRING);
	PartnerAdministrator aPartnerAdministrator;
	Users aauser;
	lr.Bind(aauser.TokenID, "auserTokenID");
	lr.Bind(aPartnerAdministrator.fullname, "fullname");
	lr.Bind(aauser.Login, "auserLogin");
	Selector sel;
	sel << aauser->TokenID << aPartnerAdministrator->fullname << aauser->Login;
	sel.Join(aauser).On(aPartnerAdministrator->auserTokenID==aauser->TokenID);
	sel.Where( (Defined(auserTokenID) ? aPartnerAdministrator->auserTokenID==*auserTokenID : Expression()));
	DataSet data=sel.Execute(rdb_);
	while(data.Fetch()) lr.AddRow();
	Value res=lr;
	return res;
}

void BALOONPartnerManager::initialize(ParamsMapRef params) {
	RegisterMethod("Add", (&BALOONPartnerManager::__Add));
	RegisterMethod("GetCurrent", (&BALOONPartnerManager::__GetCurrent));
	RegisterMethod("MyGet", (&BALOONPartnerManager::__MyGet));
	RegisterMethod("MyUpdate", (&BALOONPartnerManager::__MyUpdate));
	RegisterMethod("Get", (&BALOONPartnerManager::__Get));
	RegisterMethod("Update", (&BALOONPartnerManager::__Update));
	RegisterMethod("Delete", (&BALOONPartnerManager::__Delete));
	RegisterMethod("PartnerManagerListGet", (&BALOONPartnerManager::__PartnerManagerListGet));
	InitDatabase(params);
}

 const Value BALOONPartnerManager::__Add(const Param& param) {
	return Add(param["partnerid"], param["fullname"], param["passport_number"], param["issue_date"], param["issued_by"], param["department_code"], param["phone"], param["email"], param["password"], param["password2"]);
}

 const Value BALOONPartnerManager::__GetCurrent(const Param& param) {
	return GetCurrent();
}

 const Value BALOONPartnerManager::__MyGet(const Param& param) {
	return MyGet();
}

 const Value BALOONPartnerManager::__MyUpdate(const Param& param) {
	return MyUpdate(param["passport_number"], param["issue_date"], param["issued_by"], param["department_code"], param["phone"]);
}

// Wrappers for automatic methods
const Value BALOONPartnerManager::__Update(const Param& param) {
	return Update(param["auserTokenID"], param["partnerid"], param["fullname"], param["passport_number"], param["issue_date"], param["issued_by"], param["department_code"], param["phone"]);
}

const Value BALOONPartnerManager::__Delete(const Param& param) {
	return Delete(param["auserTokenID"]);
}

const Value BALOONPartnerManager::__Get(const Param& param) {
	return Get(param["auserTokenID"]);
}

const Value BALOONPartnerManager::__PartnerManagerListGet(const Param& param) {
	return PartnerManagerListGet(param["auserTokenID"], param["partnerid"]);
}

// Implementation of automatic methods
#if 0 //BALOONPartnerManager Add
const Value BALOONPartnerManager::Add(const Int64 auserTokenID, const Int partnerid, const Str fullname, const Str passport_number, const ADate issue_date, const Str issued_by, const Str department_code, const Str phone) {
	Exception e((Message("Cannot add ") << Message("Person In Charge").What() << ". ").What());
	bool error=false;
	error |= Defined(auserTokenID)  && !__CheckIDExists(Users()->TokenID, *auserTokenID, "ID", e, rdb_);
	error |= Defined(partnerid)  && !__CheckIDExists(Partner()->id, *partnerid, "Partner", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		PartnerManager aPartnerManager;
		aPartnerManager.auserTokenID=auserTokenID;
		aPartnerManager.partnerid=partnerid;
		aPartnerManager.fullname=fullname;
		aPartnerManager.passport_number=passport_number;
		aPartnerManager.issue_date=issue_date;
		aPartnerManager.issued_by=issued_by;
		aPartnerManager.department_code=department_code;
		aPartnerManager.phone=phone;
	aPartnerManager.Insert(rdb_);
		Value res;
		AddMessage(Message()<<Message("Person In Charge").What()<<" "<<aPartnerManager.auserTokenID<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

#endif //BALOONPartnerManager Add

const Value BALOONPartnerManager::Update(const Int64 auserTokenID, const optional<Int> partnerid, const optional<Str> fullname, const optional<Str> passport_number, const optional<ADate> issue_date, const optional<Str> issued_by, const optional<Str> department_code, const optional<Str> phone) {
	Exception e((Message("Cannot update ") << Message("Person In Charge").What() << ". ").What());
	bool error=false;
	error |= Defined(auserTokenID)  && !__CheckIDExists(Users()->TokenID, *auserTokenID, "ID", e, rdb_);
	error |= Defined(partnerid) && Defined(*partnerid)  && !__CheckIDExists(Partner()->id, *partnerid, "Partner", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		PartnerManager aPartnerManager;
		aPartnerManager.auserTokenID=auserTokenID;
		aPartnerManager.Select(rdb_);
		if(Defined(partnerid)) aPartnerManager.partnerid=*partnerid;
		if(Defined(fullname)) aPartnerManager.fullname=*fullname;
		if(Defined(passport_number)) aPartnerManager.passport_number=*passport_number;
		if(Defined(issue_date)) aPartnerManager.issue_date=*issue_date;
		if(Defined(issued_by)) aPartnerManager.issued_by=*issued_by;
		if(Defined(department_code)) aPartnerManager.department_code=*department_code;
		if(Defined(phone)) aPartnerManager.phone=*phone;
		aPartnerManager.Update(rdb_);
		AddMessage(Message()<<Message("Person In Charge").What()<<" "<<aPartnerManager.auserTokenID<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONPartnerManager::Delete(const Int64 auserTokenID) {
	PartnerManager aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Person In Charge").What()<<". ").What());
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.auserTokenID=auserTokenID;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Person In Charge").What()<<" "<<auserTokenID<<" deleted. ");
	return Value();
}

const Value BALOONPartnerManager::Get(const Int64 auserTokenID) {
	Value res;
	PartnerManager aPartnerManager;
	aPartnerManager.auserTokenID=auserTokenID;
	aPartnerManager.Select(rdb_);
	res["auserTokenID"]=aPartnerManager.auserTokenID;
	res["partnerid"]=aPartnerManager.partnerid;
	res["fullname"]=aPartnerManager.fullname;
	res["passport_number"]=aPartnerManager.passport_number;
	res["issue_date"]=aPartnerManager.issue_date;
	res["issued_by"]=aPartnerManager.issued_by;
	res["department_code"]=aPartnerManager.department_code;
	res["phone"]=aPartnerManager.phone;
	return res;
}

const Value BALOONPartnerManager::PartnerManagerListGet(const optional<Int64> auserTokenID, const optional<Int> partnerid) {
	Data::DataList lr;
	lr.AddColumn("auserTokenID", Data::INT64);
	lr.AddColumn("fullname", Data::STRING);
	lr.AddColumn("auserLogin", Data::STRING);
	lr.AddColumn("phone", Data::STRING);
	lr.AddColumn("passport_number", Data::STRING);
	lr.AddColumn("issue_date", Data::DATETIME);
	lr.AddColumn("issued_by", Data::STRING);
	lr.AddColumn("department_code", Data::STRING);
	PartnerManager aPartnerManager;
	Users aauser;
	Partner apartner;
	lr.Bind(aauser.TokenID, "auserTokenID");
	lr.Bind(aPartnerManager.fullname, "fullname");
	lr.Bind(aauser.Login, "auserLogin");
	lr.Bind(aPartnerManager.phone, "phone");
	lr.Bind(aPartnerManager.passport_number, "passport_number");
	lr.Bind(aPartnerManager.issue_date, "issue_date");
	lr.Bind(aPartnerManager.issued_by, "issued_by");
	lr.Bind(aPartnerManager.department_code, "department_code");
	Selector sel;
	sel << aauser->TokenID << aPartnerManager->fullname << aauser->Login << aPartnerManager->phone << aPartnerManager->passport_number << aPartnerManager->issue_date << aPartnerManager->issued_by << aPartnerManager->department_code;
	sel.Join(aauser).On(aPartnerManager->auserTokenID==aauser->TokenID).Join(apartner).On(aPartnerManager->partnerid==apartner->id);
	sel.Where( (Defined(auserTokenID) ? aPartnerManager->auserTokenID==*auserTokenID : Expression()) &&  (Defined(partnerid) ? aPartnerManager->partnerid==*partnerid : Expression()));
	DataSet data=sel.Execute(rdb_);
	while(data.Fetch()) lr.AddRow();
	Value res=lr;
	return res;
}

void BALOONPartnerContract::initialize(ParamsMapRef params) {
	RegisterMethod("OurPartnerContractListGet", (&BALOONPartnerContract::__OurPartnerContractListGet));
	RegisterMethod("OurGet", (&BALOONPartnerContract::__OurGet));
	RegisterMethod("Add", (&BALOONPartnerContract::__Add));
	RegisterMethod("Get", (&BALOONPartnerContract::__Get));
	RegisterMethod("Update", (&BALOONPartnerContract::__Update));
	RegisterMethod("Delete", (&BALOONPartnerContract::__Delete));
	RegisterMethod("PartnerContractListGet", (&BALOONPartnerContract::__PartnerContractListGet));
	InitDatabase(params);
}

 const Value BALOONPartnerContract::__OurPartnerContractListGet(const Param& param) {
	return OurPartnerContractListGet();
}

 const Value BALOONPartnerContract::__OurGet(const Param& param) {
	return OurGet(param["id"]);
}

// Wrappers for automatic methods
const Value BALOONPartnerContract::__Add(const Param& param) {
	return Add(param["partnerid"], param["barcode"], param["start_date"], param["end_date"], param["benefit_type"], param["benefit_amount"]);
}

const Value BALOONPartnerContract::__Update(const Param& param) {
	return Update(param["id"], param["partnerid"], param["barcode"], param["start_date"], param["end_date"], param["benefit_type"], param["benefit_amount"]);
}

const Value BALOONPartnerContract::__Delete(const Param& param) {
	return Delete(param["id"]);
}

const Value BALOONPartnerContract::__Get(const Param& param) {
	return Get(param["id"]);
}

const Value BALOONPartnerContract::__PartnerContractListGet(const Param& param) {
	return PartnerContractListGet(param["partnerid"]);
}

// Implementation of automatic methods
const Value BALOONPartnerContract::Add(const Int partnerid, const Str barcode, const ADate start_date, const ADate end_date, const Str benefit_type, const Decimal benefit_amount) {
	Exception e((Message("Cannot add ") << Message("Partner Contract").What() << ". ").What());
	bool error=false;
	error |= Defined(partnerid)  && !__CheckIDExists(Partner()->id, *partnerid, "Partner", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		PartnerContract aPartnerContract;
		aPartnerContract.partnerid=partnerid;
		aPartnerContract.barcode=barcode;
		aPartnerContract.start_date=start_date;
		aPartnerContract.end_date=end_date;
		aPartnerContract.benefit_type=benefit_type;
		aPartnerContract.benefit_amount=benefit_amount;
		Int sk=aPartnerContract.Insert(rdb_);
		Value res;
		res["id"]=sk;
		AddMessage(Message()<<Message("Partner Contract").What()<<" "<<sk<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONPartnerContract::Update(const Int id, const optional<Int> partnerid, const optional<Str> barcode, const optional<ADate> start_date, const optional<ADate> end_date, const optional<Str> benefit_type, const optional<Decimal> benefit_amount) {
	Exception e((Message("Cannot update ") << Message("Partner Contract").What() << ". ").What());
	bool error=false;
	error |= Defined(partnerid) && Defined(*partnerid)  && !__CheckIDExists(Partner()->id, *partnerid, "Partner", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		PartnerContract aPartnerContract;
		aPartnerContract.id=id;
		aPartnerContract.Select(rdb_);
		if(Defined(partnerid)) aPartnerContract.partnerid=*partnerid;
		if(Defined(barcode)) aPartnerContract.barcode=*barcode;
		if(Defined(start_date)) aPartnerContract.start_date=*start_date;
		if(Defined(end_date)) aPartnerContract.end_date=*end_date;
		if(Defined(benefit_type)) aPartnerContract.benefit_type=*benefit_type;
		if(Defined(benefit_amount)) aPartnerContract.benefit_amount=*benefit_amount;
		aPartnerContract.Update(rdb_);
		AddMessage(Message()<<Message("Partner Contract").What()<<" "<<aPartnerContract.id<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONPartnerContract::Delete(const Int id) {
	PartnerContract aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Partner Contract").What()<<". ").What());
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.id=id;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Partner Contract").What()<<" "<<id<<" deleted. ");
	return Value();
}

const Value BALOONPartnerContract::Get(const Int id) {
	Value res;
	PartnerContract aPartnerContract;
	aPartnerContract.id=id;
	aPartnerContract.Select(rdb_);
	res["id"]=aPartnerContract.id;
	res["partnerid"]=aPartnerContract.partnerid;
	res["barcode"]=aPartnerContract.barcode;
	res["start_date"]=aPartnerContract.start_date;
	res["end_date"]=aPartnerContract.end_date;
	res["benefit_type"]=aPartnerContract.benefit_type;
	res["benefit_amount"]=aPartnerContract.benefit_amount;
	return res;
}

const Value BALOONPartnerContract::PartnerContractListGet(const optional<Int> partnerid) {
	Data::DataList lr;
	lr.AddColumn("id", Data::INTEGER);
	lr.AddColumn("partnername", Data::STRING);
	lr.AddColumn("barcode", Data::STRING);
	lr.AddColumn("start_date", Data::DATETIME);
	lr.AddColumn("end_date", Data::DATETIME);
	lr.AddColumn("benefit_type", Data::STRING, PARTNER_BENEFITValues());
	lr.AddColumn("benefit_amount", Data::DECIMAL);
	PartnerContract aPartnerContract;
	Partner apartner;
	lr.Bind(aPartnerContract.id, "id");
	lr.Bind(apartner.name, "partnername");
	lr.Bind(aPartnerContract.barcode, "barcode");
	lr.Bind(aPartnerContract.start_date, "start_date");
	lr.Bind(aPartnerContract.end_date, "end_date");
	lr.Bind(aPartnerContract.benefit_type, "benefit_type");
	lr.Bind(aPartnerContract.benefit_amount, "benefit_amount");
	Selector sel;
	sel << aPartnerContract->id << apartner->name << aPartnerContract->barcode << aPartnerContract->start_date << aPartnerContract->end_date << aPartnerContract->benefit_type << aPartnerContract->benefit_amount;
	sel.Join(apartner).On(aPartnerContract->partnerid==apartner->id);
	sel.Where( (Defined(partnerid) ? aPartnerContract->partnerid==*partnerid : Expression()));
	DataSet data=sel.Execute(rdb_);
	while(data.Fetch()) lr.AddRow();
	Value res=lr;
	return res;
}

void BALOONPartnerAccount::initialize(ParamsMapRef params) {
	RegisterMethod("Refill", (&BALOONPartnerAccount::__Refill));
	RegisterMethod("Block", (&BALOONPartnerAccount::__Block));
	RegisterMethod("DebitGet", (&BALOONPartnerAccount::__DebitGet));
	RegisterMethod("CreditGet", (&BALOONPartnerAccount::__CreditGet));
	RegisterMethod("OurDebitGet", (&BALOONPartnerAccount::__OurDebitGet));
	RegisterMethod("OurCreditGet", (&BALOONPartnerAccount::__OurCreditGet));
	RegisterMethod("OurPartnerAccountListGet", (&BALOONPartnerAccount::__OurPartnerAccountListGet));
	RegisterMethod("Add", (&BALOONPartnerAccount::__Add));
	RegisterMethod("Get", (&BALOONPartnerAccount::__Get));
	RegisterMethod("Update", (&BALOONPartnerAccount::__Update));
	RegisterMethod("Delete", (&BALOONPartnerAccount::__Delete));
	RegisterMethod("PartnerAccountListGet", (&BALOONPartnerAccount::__PartnerAccountListGet));
	InitDatabase(params);
}

 const Value BALOONPartnerAccount::__Refill(const Param& param) {
	return Refill(param["id"], param["refill_amount"], param["reason"]);
}

 const Value BALOONPartnerAccount::__Block(const Param& param) {
	return Block(param["id"]);
}

 const Value BALOONPartnerAccount::__DebitGet(const Param& param) {
	return DebitGet(param["partnerid"]);
}

 const Value BALOONPartnerAccount::__CreditGet(const Param& param) {
	return CreditGet(param["partnerid"]);
}

 const Value BALOONPartnerAccount::__OurDebitGet(const Param& param) {
	return OurDebitGet();
}

 const Value BALOONPartnerAccount::__OurCreditGet(const Param& param) {
	return OurCreditGet();
}

 const Value BALOONPartnerAccount::__OurPartnerAccountListGet(const Param& param) {
	return OurPartnerAccountListGet();
}

// Wrappers for automatic methods
const Value BALOONPartnerAccount::__Add(const Param& param) {
	return Add(param["partnerid"], param["name"], param["atype"], param["amount"], param["status"]);
}

const Value BALOONPartnerAccount::__Update(const Param& param) {
	return Update(param["id"], param["partnerid"], param["name"], param["atype"], param["amount"], param["status"]);
}

const Value BALOONPartnerAccount::__Delete(const Param& param) {
	return Delete(param["id"]);
}

const Value BALOONPartnerAccount::__Get(const Param& param) {
	return Get(param["id"]);
}

const Value BALOONPartnerAccount::__PartnerAccountListGet(const Param& param) {
	return PartnerAccountListGet(param["partnerid"]);
}

// Implementation of automatic methods
const Value BALOONPartnerAccount::Add(const Int partnerid, const Str name, const Str atype, const Decimal amount, const Str status) {
	Exception e((Message("Cannot add ") << Message("Account").What() << ". ").What());
	bool error=false;
	error |= Defined(partnerid)  && !__CheckIDExists(Partner()->id, *partnerid, "Partner", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		PartnerAccount aPartnerAccount;
		aPartnerAccount.partnerid=partnerid;
		aPartnerAccount.name=name;
		aPartnerAccount.atype=atype;
		aPartnerAccount.amount=amount;
		aPartnerAccount.status=status;
		Int sk=aPartnerAccount.Insert(rdb_);
		Value res;
		res["id"]=sk;
		AddMessage(Message()<<Message("Account").What()<<" "<<sk<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONPartnerAccount::Update(const Int id, const optional<Int> partnerid, const optional<Str> name, const optional<Str> atype, const optional<Decimal> amount, const optional<Str> status) {
	Exception e((Message("Cannot update ") << Message("Account").What() << ". ").What());
	bool error=false;
	error |= Defined(partnerid) && Defined(*partnerid)  && !__CheckIDExists(Partner()->id, *partnerid, "Partner", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		PartnerAccount aPartnerAccount;
		aPartnerAccount.id=id;
		aPartnerAccount.Select(rdb_);
		if(Defined(partnerid)) aPartnerAccount.partnerid=*partnerid;
		if(Defined(name)) aPartnerAccount.name=*name;
		if(Defined(atype)) aPartnerAccount.atype=*atype;
		if(Defined(amount)) aPartnerAccount.amount=*amount;
		if(Defined(status)) aPartnerAccount.status=*status;
		aPartnerAccount.Update(rdb_);
		AddMessage(Message()<<Message("Account").What()<<" "<<aPartnerAccount.id<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONPartnerAccount::Delete(const Int id) {
	PartnerAccount aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Account").What()<<". ").What());
	//check AccountTransaction reference to PartnerAccount reference 
	error |= !__CheckIDNotExists(AccountTransaction()->accountid, id, "Transaction", e, rdb_);
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.id=id;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Account").What()<<" "<<id<<" deleted. ");
	return Value();
}

const Value BALOONPartnerAccount::Get(const Int id) {
	Value res;
	PartnerAccount aPartnerAccount;
	aPartnerAccount.id=id;
	aPartnerAccount.Select(rdb_);
	res["id"]=aPartnerAccount.id;
	res["partnerid"]=aPartnerAccount.partnerid;
	res["name"]=aPartnerAccount.name;
	res["atype"]=aPartnerAccount.atype;
	res["amount"]=aPartnerAccount.amount;
	res["status"]=aPartnerAccount.status;
	return res;
}

const Value BALOONPartnerAccount::PartnerAccountListGet(const optional<Int> partnerid) {
	Data::DataList lr;
	lr.AddColumn("id", Data::INTEGER);
	lr.AddColumn("partnername", Data::STRING);
	lr.AddColumn("name", Data::STRING);
	lr.AddColumn("atype", Data::STRING, ACCOUNT_TYPEValues());
	lr.AddColumn("amount", Data::DECIMAL);
	lr.AddColumn("status", Data::STRING, BLOCK_STATUSValues());
	PartnerAccount aPartnerAccount;
	Partner apartner;
	lr.Bind(aPartnerAccount.id, "id");
	lr.Bind(apartner.name, "partnername");
	lr.Bind(aPartnerAccount.name, "name");
	lr.Bind(aPartnerAccount.atype, "atype");
	lr.Bind(aPartnerAccount.amount, "amount");
	lr.Bind(aPartnerAccount.status, "status");
	Selector sel;
	sel << aPartnerAccount->id << apartner->name << aPartnerAccount->name << aPartnerAccount->atype << aPartnerAccount->amount << aPartnerAccount->status;
	sel.Join(apartner).On(aPartnerAccount->partnerid==apartner->id);
	sel.Where( (Defined(partnerid) ? aPartnerAccount->partnerid==*partnerid : Expression()));
	DataSet data=sel.Execute(rdb_);
	while(data.Fetch()) lr.AddRow();
	Value res=lr;
	return res;
}

void BALOONAccountTransaction::initialize(ParamsMapRef params) {
	RegisterMethod("OurDebitAccountTransactionListGet", (&BALOONAccountTransaction::__OurDebitAccountTransactionListGet));
	RegisterMethod("DebitAccountTransactionListGet", (&BALOONAccountTransaction::__DebitAccountTransactionListGet));
	RegisterMethod("OurCreditAccountTransactionListGet", (&BALOONAccountTransaction::__OurCreditAccountTransactionListGet));
	RegisterMethod("CreditAccountTransactionListGet", (&BALOONAccountTransaction::__CreditAccountTransactionListGet));
	RegisterMethod("Add", (&BALOONAccountTransaction::__Add));
	RegisterMethod("Get", (&BALOONAccountTransaction::__Get));
	RegisterMethod("Update", (&BALOONAccountTransaction::__Update));
	RegisterMethod("Delete", (&BALOONAccountTransaction::__Delete));
	RegisterMethod("AccountTransactionListGet", (&BALOONAccountTransaction::__AccountTransactionListGet));
	InitDatabase(params);
}

 const Value BALOONAccountTransaction::__OurDebitAccountTransactionListGet(const Param& param) {
	return OurDebitAccountTransactionListGet();
}

 const Value BALOONAccountTransaction::__DebitAccountTransactionListGet(const Param& param) {
	return DebitAccountTransactionListGet(param["partnerid"]);
}

 const Value BALOONAccountTransaction::__OurCreditAccountTransactionListGet(const Param& param) {
	return OurCreditAccountTransactionListGet();
}

 const Value BALOONAccountTransaction::__CreditAccountTransactionListGet(const Param& param) {
	return CreditAccountTransactionListGet(param["partnerid"]);
}

// Wrappers for automatic methods
const Value BALOONAccountTransaction::__Add(const Param& param) {
	return Add(param["accountid"], param["amount"], param["cache_flow"], param["reason"]);
}

const Value BALOONAccountTransaction::__Update(const Param& param) {
	return Update(param["id"], param["accountid"], param["amount"], param["cache_flow"], param["reason"]);
}

const Value BALOONAccountTransaction::__Delete(const Param& param) {
	return Delete(param["id"]);
}

const Value BALOONAccountTransaction::__Get(const Param& param) {
	return Get(param["id"]);
}

const Value BALOONAccountTransaction::__AccountTransactionListGet(const Param& param) {
	return AccountTransactionListGet(param["accountid"]);
}

// Implementation of automatic methods
const Value BALOONAccountTransaction::Add(const Int accountid, const Decimal amount, const Str cache_flow, const Str reason) {
	Exception e((Message("Cannot add ") << Message("Transaction").What() << ". ").What());
	bool error=false;
	error |= Defined(accountid)  && !__CheckIDExists(PartnerAccount()->id, *accountid, "Account", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		AccountTransaction aAccountTransaction;
		aAccountTransaction.accountid=accountid;
		aAccountTransaction.amount=amount;
		aAccountTransaction.cache_flow=cache_flow;
		aAccountTransaction.reason=reason;
		Int sk=aAccountTransaction.Insert(rdb_);
		Value res;
		res["id"]=sk;
		AddMessage(Message()<<Message("Transaction").What()<<" "<<sk<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONAccountTransaction::Update(const Int id, const optional<Int> accountid, const optional<Decimal> amount, const optional<Str> cache_flow, const optional<Str> reason) {
	Exception e((Message("Cannot update ") << Message("Transaction").What() << ". ").What());
	bool error=false;
	error |= Defined(accountid) && Defined(*accountid)  && !__CheckIDExists(PartnerAccount()->id, *accountid, "Account", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		AccountTransaction aAccountTransaction;
		aAccountTransaction.id=id;
		aAccountTransaction.Select(rdb_);
		if(Defined(accountid)) aAccountTransaction.accountid=*accountid;
		if(Defined(amount)) aAccountTransaction.amount=*amount;
		if(Defined(cache_flow)) aAccountTransaction.cache_flow=*cache_flow;
		if(Defined(reason)) aAccountTransaction.reason=*reason;
		aAccountTransaction.Update(rdb_);
		AddMessage(Message()<<Message("Transaction").What()<<" "<<aAccountTransaction.id<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONAccountTransaction::Delete(const Int id) {
	AccountTransaction aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Transaction").What()<<". ").What());
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.id=id;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Transaction").What()<<" "<<id<<" deleted. ");
	return Value();
}

const Value BALOONAccountTransaction::Get(const Int id) {
	Value res;
	AccountTransaction aAccountTransaction;
	aAccountTransaction.id=id;
	aAccountTransaction.Select(rdb_);
	res["id"]=aAccountTransaction.id;
	res["accountid"]=aAccountTransaction.accountid;
	res["amount"]=aAccountTransaction.amount;
	res["cache_flow"]=aAccountTransaction.cache_flow;
	res["reason"]=aAccountTransaction.reason;
	return res;
}

const Value BALOONAccountTransaction::AccountTransactionListGet(const optional<Int> accountid) {
	Data::DataList lr;
	lr.AddColumn("id", Data::INTEGER);
	lr.AddColumn("amount", Data::DECIMAL);
	lr.AddColumn("cache_flow", Data::STRING, CACHE_FLOWValues());
	lr.AddColumn("reason", Data::STRING);
	AccountTransaction aAccountTransaction;
	lr.Bind(aAccountTransaction.id, "id");
	lr.Bind(aAccountTransaction.amount, "amount");
	lr.Bind(aAccountTransaction.cache_flow, "cache_flow");
	lr.Bind(aAccountTransaction.reason, "reason");
	Selector sel;
	sel << aAccountTransaction->id << aAccountTransaction->amount << aAccountTransaction->cache_flow << aAccountTransaction->reason;
	sel.Where( (Defined(accountid) ? aAccountTransaction->accountid==*accountid : Expression()));
	DataSet data=sel.Execute(rdb_);
	while(data.Fetch()) lr.AddRow();
	Value res=lr;
	return res;
}

void BALOONPartnerWeekTariffGroup::initialize(ParamsMapRef params) {
	RegisterMethod("Get", (&BALOONPartnerWeekTariffGroup::__Get));
	RegisterMethod("Delete", (&BALOONPartnerWeekTariffGroup::__Delete));
	InitDatabase(params);
}

// Wrappers for automatic methods
const Value BALOONPartnerWeekTariffGroup::__Delete(const Param& param) {
	return Delete(param["partnerid"], param["tariffgroupid"]);
}

const Value BALOONPartnerWeekTariffGroup::__Get(const Param& param) {
	return Get(param["partnerid"], param["tariffgroupid"]);
}

// Implementation of automatic methods
const Value BALOONPartnerWeekTariffGroup::Delete(const Int partnerid, const Int tariffgroupid) {
	PartnerWeekTariffGroup aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Week Tariff Group").What()<<". ").What());
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.partnerid=partnerid;
	aRecord.tariffgroupid=tariffgroupid;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Week Tariff Group").What()<<" "<<partnerid<<" "<<tariffgroupid<<" deleted. ");
	return Value();
}

const Value BALOONPartnerWeekTariffGroup::Get(const Int partnerid, const Int tariffgroupid) {
	Value res;
	PartnerWeekTariffGroup aPartnerWeekTariffGroup;
	aPartnerWeekTariffGroup.partnerid=partnerid;
	aPartnerWeekTariffGroup.tariffgroupid=tariffgroupid;
	aPartnerWeekTariffGroup.Select(rdb_);
	res["partnerid"]=aPartnerWeekTariffGroup.partnerid;
	res["tariffgroupid"]=aPartnerWeekTariffGroup.tariffgroupid;
	return res;
}

void BALOONPartnerOrder::initialize(ParamsMapRef params) {
	RegisterMethod("Get", (&BALOONPartnerOrder::__Get));
	RegisterMethod("OurPartnerOrderListGet", (&BALOONPartnerOrder::__OurPartnerOrderListGet));
	RegisterMethod("OurCreate", (&BALOONPartnerOrder::__OurCreate));
	RegisterMethod("OurTicketsDownload", (&BALOONPartnerOrder::__OurTicketsDownload));
	RegisterMethod("TicketsDownload", (&BALOONPartnerOrder::__TicketsDownload));
	RegisterMethod("Place_API", (&BALOONPartnerOrder::__Place_API));
	RegisterMethod("DetailsGet", (&BALOONPartnerOrder::__DetailsGet));
	RegisterMethod("Add", (&BALOONPartnerOrder::__Add));
	RegisterMethod("Update", (&BALOONPartnerOrder::__Update));
	RegisterMethod("Delete", (&BALOONPartnerOrder::__Delete));
	RegisterMethod("PartnerOrderListGet", (&BALOONPartnerOrder::__PartnerOrderListGet));
	InitDatabase(params);
}

 const Value BALOONPartnerOrder::__Get(const Param& param) {
	return Get(param["id"]);
}

 const Value BALOONPartnerOrder::__OurPartnerOrderListGet(const Param& param) {
	return OurPartnerOrderListGet();
}

 const Value BALOONPartnerOrder::__OurCreate(const Param& param) {
	return OurCreate(param["adate"], param["week_tariffid"], param["identifiers_count"], param["accountid"], param["comment"]);
}

 const Value BALOONPartnerOrder::__OurTicketsDownload(const Param& param) {
	return OurTicketsDownload(param["id"]);
}

 const Value BALOONPartnerOrder::__TicketsDownload(const Param& param) {
	return TicketsDownload(param["id"]);
}

 const Value BALOONPartnerOrder::__Place_API(const Param& param) {
	return Place_API(param["adate"], param["identifiers"], param["week_tariffid"], param["token"]);
}

 const Value BALOONPartnerOrder::__DetailsGet(const Param& param) {
	return DetailsGet(param["id"]);
}

// Wrappers for automatic methods
const Value BALOONPartnerOrder::__Add(const Param& param) {
	return Add(param["partnerid"], param["initial_cost"], param["discount"], param["total_cost"], param["week_tariffid"]);
}

const Value BALOONPartnerOrder::__Update(const Param& param) {
	return Update(param["id"], param["partnerid"], param["initial_cost"], param["discount"], param["total_cost"], param["week_tariffid"]);
}

const Value BALOONPartnerOrder::__Delete(const Param& param) {
	return Delete(param["id"]);
}

const Value BALOONPartnerOrder::__PartnerOrderListGet(const Param& param) {
	return PartnerOrderListGet(param["partnerid"], param["week_tariffid"]);
}

// Implementation of automatic methods
const Value BALOONPartnerOrder::Add(const Int partnerid, const Decimal initial_cost, const Decimal discount, const Decimal total_cost, const Int week_tariffid) {
	Exception e((Message("Cannot add ") << Message("Order").What() << ". ").What());
	bool error=false;
	error |= Defined(partnerid)  && !__CheckIDExists(Partner()->id, *partnerid, "Partner", e, rdb_);
	error |= Defined(week_tariffid)  && !__CheckIDExists(WeekTariff()->id, *week_tariffid, "Week Tariff", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		PartnerOrder aPartnerOrder;
		aPartnerOrder.partnerid=partnerid;
		aPartnerOrder.initial_cost=initial_cost;
		aPartnerOrder.discount=discount;
		aPartnerOrder.total_cost=total_cost;
		aPartnerOrder.week_tariffid=week_tariffid;
		Int sk=aPartnerOrder.Insert(rdb_);
		Value res;
		res["id"]=sk;
		AddMessage(Message()<<Message("Order").What()<<" "<<sk<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONPartnerOrder::Update(const Int id, const optional<Int> partnerid, const optional<Decimal> initial_cost, const optional<Decimal> discount, const optional<Decimal> total_cost, const optional<Int> week_tariffid) {
	Exception e((Message("Cannot update ") << Message("Order").What() << ". ").What());
	bool error=false;
	error |= Defined(partnerid) && Defined(*partnerid)  && !__CheckIDExists(Partner()->id, *partnerid, "Partner", e, rdb_);
	error |= Defined(week_tariffid) && Defined(*week_tariffid)  && !__CheckIDExists(WeekTariff()->id, *week_tariffid, "Week Tariff", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		PartnerOrder aPartnerOrder;
		aPartnerOrder.id=id;
		aPartnerOrder.Select(rdb_);
		if(Defined(partnerid)) aPartnerOrder.partnerid=*partnerid;
		if(Defined(initial_cost)) aPartnerOrder.initial_cost=*initial_cost;
		if(Defined(discount)) aPartnerOrder.discount=*discount;
		if(Defined(total_cost)) aPartnerOrder.total_cost=*total_cost;
		if(Defined(week_tariffid)) aPartnerOrder.week_tariffid=*week_tariffid;
		aPartnerOrder.Update(rdb_);
		AddMessage(Message()<<Message("Order").What()<<" "<<aPartnerOrder.id<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONPartnerOrder::Delete(const Int id) {
	PartnerOrder aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Order").What()<<". ").What());
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.id=id;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Order").What()<<" "<<id<<" deleted. ");
	return Value();
}

#if 0 //BALOONPartnerOrder Get
const Value BALOONPartnerOrder::Get(const Int id) {
	Value res;
	PartnerOrder aPartnerOrder;
	aPartnerOrder.id=id;
	aPartnerOrder.Select(rdb_);
	res["id"]=aPartnerOrder.id;
	res["partnerid"]=aPartnerOrder.partnerid;
	res["initial_cost"]=aPartnerOrder.initial_cost;
	res["discount"]=aPartnerOrder.discount;
	res["total_cost"]=aPartnerOrder.total_cost;
	res["week_tariffid"]=aPartnerOrder.week_tariffid;
	return res;
}

#endif //BALOONPartnerOrder Get

const Value BALOONPartnerOrder::PartnerOrderListGet(const optional<Int> partnerid, const optional<Int> week_tariffid) {
	Data::DataList lr;
	lr.AddColumn("id", Data::INTEGER);
	lr.AddColumn("DateArc", Data::DATETIME);
	lr.AddColumn("partnername", Data::STRING);
	lr.AddColumn("total_cost", Data::DECIMAL);
	PartnerOrder aPartnerOrder;
	Partner apartner;
	lr.Bind(aPartnerOrder.id, "id");
	lr.Bind(aPartnerOrder.DateArc, "DateArc");
	lr.Bind(apartner.name, "partnername");
	lr.Bind(aPartnerOrder.total_cost, "total_cost");
	Selector sel;
	sel << aPartnerOrder->id << aPartnerOrder->DateArc << apartner->name << aPartnerOrder->total_cost;
	sel.Join(apartner).On(aPartnerOrder->partnerid==apartner->id);
	sel.Where( (Defined(partnerid) ? aPartnerOrder->partnerid==*partnerid : Expression()) &&  (Defined(week_tariffid) ? aPartnerOrder->week_tariffid==*week_tariffid : Expression()));
	DataSet data=sel.Execute(rdb_);
	while(data.Fetch()) lr.AddRow();
	Value res=lr;
	return res;
}

void BALOONPartnerOrderline::initialize(ParamsMapRef params) {
	RegisterMethod("OurPartnerOrderlineListGet", (&BALOONPartnerOrderline::__OurPartnerOrderlineListGet));
	RegisterMethod("OurTicketListGet", (&BALOONPartnerOrderline::__OurTicketListGet));
	RegisterMethod("AsyncRegisterInContour", (&BALOONPartnerOrderline::__AsyncRegisterInContour));
	RegisterMethod("Add", (&BALOONPartnerOrderline::__Add));
	RegisterMethod("Get", (&BALOONPartnerOrderline::__Get));
	RegisterMethod("Update", (&BALOONPartnerOrderline::__Update));
	RegisterMethod("Delete", (&BALOONPartnerOrderline::__Delete));
	RegisterMethod("PartnerOrderlineListGet", (&BALOONPartnerOrderline::__PartnerOrderlineListGet));
	InitDatabase(params);
}

 const Value BALOONPartnerOrderline::__OurPartnerOrderlineListGet(const Param& param) {
	return OurPartnerOrderlineListGet(param["orderid"]);
}

 const Value BALOONPartnerOrderline::__OurTicketListGet(const Param& param) {
	return OurTicketListGet(param["orderid"]);
}

 const Value BALOONPartnerOrderline::__AsyncRegisterInContour(const Param& param) {
	return AsyncRegisterInContour();
}

// Wrappers for automatic methods
const Value BALOONPartnerOrderline::__Add(const Param& param) {
	return Add(param["orderid"], param["identifierid"], param["price"], param["service_rulename"]);
}

const Value BALOONPartnerOrderline::__Update(const Param& param) {
	return Update(param["orderid"], param["identifierid"], param["price"], param["service_rulename"]);
}

const Value BALOONPartnerOrderline::__Delete(const Param& param) {
	return Delete(param["orderid"], param["identifierid"]);
}

const Value BALOONPartnerOrderline::__Get(const Param& param) {
	return Get(param["orderid"], param["identifierid"]);
}

const Value BALOONPartnerOrderline::__PartnerOrderlineListGet(const Param& param) {
	return PartnerOrderlineListGet(param["orderid"], param["identifierid"], param["service_rulename"]);
}

// Implementation of automatic methods
const Value BALOONPartnerOrderline::Add(const Int orderid, const Int identifierid, const Decimal price, const Str service_rulename) {
	Exception e((Message("Cannot add ") << Message("Orderline").What() << ". ").What());
	bool error=false;
	error |= Defined(orderid)  && !__CheckIDExists(PartnerOrder()->id, *orderid, "Order", e, rdb_);
	error |= Defined(identifierid)  && !__CheckIDExists(Identifier()->id, *identifierid, "Identifier", e, rdb_);
	error |= Defined(service_rulename)  && !__CheckIDExists(SpdServiceRule()->name, *service_rulename, "Package", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		PartnerOrderline aPartnerOrderline;
		aPartnerOrderline.orderid=orderid;
		aPartnerOrderline.identifierid=identifierid;
		aPartnerOrderline.price=price;
		aPartnerOrderline.service_rulename=service_rulename;
	aPartnerOrderline.Insert(rdb_);
		Value res;
		AddMessage(Message()<<Message("Orderline").What()<<" "<<aPartnerOrderline.orderid<<" "<<aPartnerOrderline.identifierid<<" added. ");
		return res;
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONPartnerOrderline::Update(const Int orderid, const Int identifierid, const optional<Decimal> price, const optional<Str> service_rulename) {
	Exception e((Message("Cannot update ") << Message("Orderline").What() << ". ").What());
	bool error=false;
	error |= Defined(orderid)  && !__CheckIDExists(PartnerOrder()->id, *orderid, "Order", e, rdb_);
	error |= Defined(identifierid)  && !__CheckIDExists(Identifier()->id, *identifierid, "Identifier", e, rdb_);
	error |= Defined(service_rulename) && Defined(*service_rulename)  && !__CheckIDExists(SpdServiceRule()->name, *service_rulename, "Package", e, rdb_);
	if(error) {
		throw(e);
	}
	try {
		PartnerOrderline aPartnerOrderline;
		aPartnerOrderline.orderid=orderid;
		aPartnerOrderline.identifierid=identifierid;
		aPartnerOrderline.Select(rdb_);
		if(Defined(price)) aPartnerOrderline.price=*price;
		if(Defined(service_rulename)) aPartnerOrderline.service_rulename=*service_rulename;
		aPartnerOrderline.Update(rdb_);
		AddMessage(Message()<<Message("Orderline").What()<<" "<<aPartnerOrderline.orderid<<" "<<aPartnerOrderline.identifierid<<" updated. ");
		return Value();
	}
	catch (Exception& exc) {
		if(exc.ErrorCode() == RDBMS_ERR_DUPLICATE_ENTRY) {
			e << Message("Duplicate entry. ").What();
		}
		else {
			e << Message(exc.what()).What();
		}
		throw e;
	}
}

const Value BALOONPartnerOrderline::Delete(const Int orderid, const Int identifierid) {
	PartnerOrderline aRecord;
	bool error=false;
	Exception e((Message("Cannot delete ")<<Message("Orderline").What()<<". ").What());
	//throw exception in case of error
	if(error) {throw(e);};
	aRecord.orderid=orderid;
	aRecord.identifierid=identifierid;
	aRecord.Delete(rdb_);
	AddMessage( Message() << Message("Orderline").What()<<" "<<orderid<<" "<<identifierid<<" deleted. ");
	return Value();
}

const Value BALOONPartnerOrderline::Get(const Int orderid, const Int identifierid) {
	Value res;
	PartnerOrderline aPartnerOrderline;
	aPartnerOrderline.orderid=orderid;
	aPartnerOrderline.identifierid=identifierid;
	aPartnerOrderline.Select(rdb_);
	res["orderid"]=aPartnerOrderline.orderid;
	res["identifierid"]=aPartnerOrderline.identifierid;
	res["price"]=aPartnerOrderline.price;
	res["service_rulename"]=aPartnerOrderline.service_rulename;
	return res;
}

const Value BALOONPartnerOrderline::PartnerOrderlineListGet(const optional<Int> orderid, const optional<Int> identifierid, const optional<Str> service_rulename) {
	Data::DataList lr;
	lr.AddColumn("identifierid", Data::INTEGER);
	lr.AddColumn("identifiercode", Data::STRING);
	lr.AddColumn("price", Data::DECIMAL);
	lr.AddColumn("identifiervalid_from", Data::DATETIME);
	lr.AddColumn("identifiervalid_to", Data::DATETIME);
	lr.AddColumn("identifierpermanent_rulename", Data::STRING);
	lr.AddColumn("identifiercategoryname", Data::STRING);
	lr.AddColumn("identifierratename", Data::STRING);
	lr.AddColumn("identifierstatus", Data::STRING, IDENTIFIER_STATUSValues());
	PartnerOrderline aPartnerOrderline;
	Identifier aidentifier;
	PartnerOrder aorder;
	lr.Bind(aidentifier.id, "identifierid");
	lr.Bind(aidentifier.code, "identifiercode");
	lr.Bind(aPartnerOrderline.price, "price");
	lr.Bind(aidentifier.valid_from, "identifiervalid_from");
	lr.Bind(aidentifier.valid_to, "identifiervalid_to");
	lr.Bind(aidentifier.permanent_rulename, "identifierpermanent_rulename");
	lr.Bind(aidentifier.categoryname, "identifiercategoryname");
	lr.Bind(aidentifier.ratename, "identifierratename");
	lr.Bind(aidentifier.status, "identifierstatus");
	Selector sel;
	sel << aidentifier->id << aidentifier->code << aPartnerOrderline->price << aidentifier->valid_from << aidentifier->valid_to << aidentifier->permanent_rulename << aidentifier->categoryname << aidentifier->ratename << aidentifier->status;
	sel.Join(aidentifier).On(aPartnerOrderline->identifierid==aidentifier->id).Join(aorder).On(aPartnerOrderline->orderid==aorder->id);
	sel.Where( (Defined(orderid) ? aPartnerOrderline->orderid==*orderid : Expression()) &&  (Defined(identifierid) ? aPartnerOrderline->identifierid==*identifierid : Expression()) &&  (Defined(service_rulename) ? aPartnerOrderline->service_rulename==*service_rulename : Expression()));
	DataSet data=sel.Execute(rdb_);
	while(data.Fetch()) lr.AddRow();
	Value res=lr;
	return res;
}

void BALOONAuxiliary::initialize(ParamsMapRef params) {
	RegisterMethod("CurrentDate", (&BALOONAuxiliary::__CurrentDate));
	InitDatabase(params);
}

 const Value BALOONAuxiliary::__CurrentDate(const Param& param) {
	return CurrentDate();
}

// Wrappers for automatic methods
// Implementation of automatic methods
void Baloon::initialize(ParamsMapRef params) {
	ShPtr<AObject> object;
	{
		ShPtr<AObject> object=new BALOONPerson(params);
		Register("Person", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONSpdServiceRule(params);
		Register("SpdServiceRule", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONPushNotification(params);
		Register("PushNotification", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONPushNotificationMessages(params);
		Register("PushNotificationMessages", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONPushNotificationDeliveries(params);
		Register("PushNotificationDeliveries", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONIdentifierRule(params);
		Register("IdentifierRule", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONIdentifierServiceRule(params);
		Register("IdentifierServiceRule", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONIdentifierCategory(params);
		Register("IdentifierCategory", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONIdentifierRate(params);
		Register("IdentifierRate", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONIdentifier(params);
		Register("Identifier", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONOrderSite(params);
		Register("OrderSite", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONPersonOrder(params);
		Register("PersonOrder", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONParkingArea(params);
		Register("ParkingArea", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONParkingTimer(params);
		Register("ParkingTimer", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONParkingOrderline(params);
		Register("ParkingOrderline", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONPersonOrderFiscalDoc(params);
		Register("PersonOrderFiscalDoc", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONPersonIdentifierOrderline(params);
		Register("PersonIdentifierOrderline", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONAgentIdentifierOrderline(params);
		Register("AgentIdentifierOrderline", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONPersonInvoice(params);
		Register("PersonInvoice", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONIdentifierInvoice(params);
		Register("IdentifierInvoice", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONBaloonProvider(params);
		Register("BaloonProvider", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONExternalResource(params);
		Register("ExternalResource", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONAgent(params);
		Register("Agent", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONAgentOrder(params);
		Register("AgentOrder", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONIdentifierAgentOrder(params);
		Register("IdentifierAgentOrder", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONPackageAgentOrder(params);
		Register("PackageAgentOrder", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONAgentInvoice(params);
		Register("AgentInvoice", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONReports(params);
		Register("Reports", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONAccount(params);
		Register("Account", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONPackageOrder(params);
		Register("PackageOrder", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONTransaction(params);
		Register("Transaction", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONAdvertisement(params);
		Register("Advertisement", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONInfoPlacement(params);
		Register("InfoPlacement", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONHoliday(params);
		Register("Holiday", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONWeekTariffGroup(params);
		Register("WeekTariffGroup", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONWeekTariff(params);
		Register("WeekTariff", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONSkipassConfiguration(params);
		Register("SkipassConfiguration", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONSpdGate(params);
		Register("SpdGate", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONPaidServiceCategory(params);
		Register("PaidServiceCategory", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONPaidServiceTariff(params);
		Register("PaidServiceTariff", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONPaidServiceHour(params);
		Register("PaidServiceHour", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONPaidService(params);
		Register("PaidService", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONPartner(params);
		Register("Partner", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONPartnerAdministrator(params);
		Register("PartnerAdministrator", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONPartnerManager(params);
		Register("PartnerManager", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONPartnerContract(params);
		Register("PartnerContract", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONPartnerAccount(params);
		Register("PartnerAccount", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONAccountTransaction(params);
		Register("AccountTransaction", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONPartnerWeekTariffGroup(params);
		Register("PartnerWeekTariffGroup", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONPartnerOrder(params);
		Register("PartnerOrder", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONPartnerOrderline(params);
		Register("PartnerOrderline", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

	{
		ShPtr<AObject> object=new BALOONAuxiliary(params);
		Register("Auxiliary", object);
  	((ApplicationDBService*)object.GetBuffer())->AttachToBalancer(&objectBalancer_);
	}

}

void cloneBaloonTenant(Connection* rdb, int64_t baseTenantID, int64_t newTenantID){
	string dbType = rdb->GetDBType();
	if(dbType == DBTYPE_MYSQL) {
		Selector sel0("SET FOREIGN_KEY_CHECKS = 0;");
		sel0.Execute(rdb);
		Selector sel_zero("SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO';");
		sel_zero.Execute(rdb);
	}
	else if (dbType == DBTYPE_ORACLE) {
		Selector sel0("ALTER SESSION SET constraints=DEFERRED");
		sel0.Execute(rdb);
	}
	else {
		throw("Tenant cloning for DBType " + dbType + " is not supported. ");
	}
	
	if(Effi::GetCurrentContext()->TenantID() != TENANT_ADMIN) {
		throw("You can call cloning tenants only from Administative tenant. ");
	}
	
	try {
		{// clone Person records 
		Person aPerson;
		Inserter ins(aPerson, Int64(newTenantID));
		ins << aPerson->id(aPerson->id)<< aPerson->userTokenID(aPerson->userTokenID)<< aPerson->last_name(aPerson->last_name)<< aPerson->first_name(aPerson->first_name)<< aPerson->middle_name(aPerson->middle_name)<< aPerson->birthdate(aPerson->birthdate)<< aPerson->gender(aPerson->gender)<< aPerson->phone(aPerson->phone)<< aPerson->email(aPerson->email)<< aPerson->regcode(aPerson->regcode)<< aPerson->last_sync(aPerson->last_sync)<< aPerson->oid(aPerson->oid)<< aPerson->registered_at(aPerson->registered_at);
		ins.Where(aPerson->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone SpdServiceRule records 
		SpdServiceRule aSpdServiceRule;
		Inserter ins(aSpdServiceRule, Int64(newTenantID));
		ins << aSpdServiceRule->name(aSpdServiceRule->name)<< aSpdServiceRule->enabled(aSpdServiceRule->enabled)<< aSpdServiceRule->extid(aSpdServiceRule->extid)<< aSpdServiceRule->extcode(aSpdServiceRule->extcode)<< aSpdServiceRule->comment(aSpdServiceRule->comment)<< aSpdServiceRule->package_cost(aSpdServiceRule->package_cost);
		ins.Where(aSpdServiceRule->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone PushNotification records 
		PushNotification aPushNotification;
		Inserter ins(aPushNotification, Int64(newTenantID));
		ins << aPushNotification->id(aPushNotification->id)<< aPushNotification->token(aPushNotification->token)<< aPushNotification->token_type(aPushNotification->token_type)<< aPushNotification->first_name(aPushNotification->first_name)<< aPushNotification->last_name(aPushNotification->last_name)<< aPushNotification->middle_name(aPushNotification->middle_name)<< aPushNotification->description(aPushNotification->description)<< aPushNotification->enabled(aPushNotification->enabled)<< aPushNotification->valid_from(aPushNotification->valid_from)<< aPushNotification->last_register(aPushNotification->last_register);
		ins.Where(aPushNotification->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone PushNotificationMessages records 
		PushNotificationMessages aPushNotificationMessages;
		Inserter ins(aPushNotificationMessages, Int64(newTenantID));
		ins << aPushNotificationMessages->id(aPushNotificationMessages->id)<< aPushNotificationMessages->title(aPushNotificationMessages->title)<< aPushNotificationMessages->message(aPushNotificationMessages->message)<< aPushNotificationMessages->sent(aPushNotificationMessages->sent)<< aPushNotificationMessages->enabled(aPushNotificationMessages->enabled)<< aPushNotificationMessages->message_date(aPushNotificationMessages->message_date);
		ins.Where(aPushNotificationMessages->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone PushNotificationDeliveries records 
		PushNotificationDeliveries aPushNotificationDeliveries;
		Inserter ins(aPushNotificationDeliveries, Int64(newTenantID));
		ins << aPushNotificationDeliveries->id(aPushNotificationDeliveries->id)<< aPushNotificationDeliveries->deviceid(aPushNotificationDeliveries->deviceid)<< aPushNotificationDeliveries->messageid(aPushNotificationDeliveries->messageid)<< aPushNotificationDeliveries->sendresult(aPushNotificationDeliveries->sendresult)<< aPushNotificationDeliveries->delivery_date(aPushNotificationDeliveries->delivery_date);
		ins.Where(aPushNotificationDeliveries->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone IdentifierRule records 
		IdentifierRule aIdentifierRule;
		Inserter ins(aIdentifierRule, Int64(newTenantID));
		ins << aIdentifierRule->name(aIdentifierRule->name)<< aIdentifierRule->enabled(aIdentifierRule->enabled)<< aIdentifierRule->replenish_enabled(aIdentifierRule->replenish_enabled)<< aIdentifierRule->extid(aIdentifierRule->extid)<< aIdentifierRule->extcode(aIdentifierRule->extcode)<< aIdentifierRule->comment(aIdentifierRule->comment);
		ins.Where(aIdentifierRule->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone IdentifierServiceRule records 
		IdentifierServiceRule aIdentifierServiceRule;
		Inserter ins(aIdentifierServiceRule, Int64(newTenantID));
		ins << aIdentifierServiceRule->identifier_rulename(aIdentifierServiceRule->identifier_rulename)<< aIdentifierServiceRule->service_rulename(aIdentifierServiceRule->service_rulename)<< aIdentifierServiceRule->name(aIdentifierServiceRule->name)<< aIdentifierServiceRule->enabled(aIdentifierServiceRule->enabled)<< aIdentifierServiceRule->weekday_cost(aIdentifierServiceRule->weekday_cost)<< aIdentifierServiceRule->weekend_cost(aIdentifierServiceRule->weekend_cost);
		ins.Where(aIdentifierServiceRule->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone IdentifierCategory records 
		IdentifierCategory aIdentifierCategory;
		Inserter ins(aIdentifierCategory, Int64(newTenantID));
		ins << aIdentifierCategory->name(aIdentifierCategory->name)<< aIdentifierCategory->enabled(aIdentifierCategory->enabled)<< aIdentifierCategory->extid(aIdentifierCategory->extid)<< aIdentifierCategory->extcode(aIdentifierCategory->extcode)<< aIdentifierCategory->comment(aIdentifierCategory->comment)<< aIdentifierCategory->is_default(aIdentifierCategory->is_default);
		ins.Where(aIdentifierCategory->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone IdentifierRate records 
		IdentifierRate aIdentifierRate;
		Inserter ins(aIdentifierRate, Int64(newTenantID));
		ins << aIdentifierRate->name(aIdentifierRate->name)<< aIdentifierRate->enabled(aIdentifierRate->enabled)<< aIdentifierRate->extid(aIdentifierRate->extid)<< aIdentifierRate->extcode(aIdentifierRate->extcode)<< aIdentifierRate->comment(aIdentifierRate->comment);
		ins.Where(aIdentifierRate->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone Identifier records 
		Identifier aIdentifier;
		Inserter ins(aIdentifier, Int64(newTenantID));
		ins << aIdentifier->id(aIdentifier->id)<< aIdentifier->personid(aIdentifier->personid)<< aIdentifier->code(aIdentifier->code)<< aIdentifier->name(aIdentifier->name)<< aIdentifier->groupname(aIdentifier->groupname)<< aIdentifier->valid_from(aIdentifier->valid_from)<< aIdentifier->valid_to(aIdentifier->valid_to)<< aIdentifier->permanent_rulename(aIdentifier->permanent_rulename)<< aIdentifier->categoryname(aIdentifier->categoryname)<< aIdentifier->ratename(aIdentifier->ratename)<< aIdentifier->clientoid(aIdentifier->clientoid)<< aIdentifier->status(aIdentifier->status)<< aIdentifier->comment(aIdentifier->comment)<< aIdentifier->spdgateid(aIdentifier->spdgateid);
		ins.Where(aIdentifier->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone OrderSite records 
		OrderSite aOrderSite;
		Inserter ins(aOrderSite, Int64(newTenantID));
		ins << aOrderSite->code(aOrderSite->code)<< aOrderSite->name(aOrderSite->name)<< aOrderSite->link(aOrderSite->link);
		ins.Where(aOrderSite->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone PersonOrder records 
		PersonOrder aPersonOrder;
		Inserter ins(aPersonOrder, Int64(newTenantID));
		ins << aPersonOrder->id(aPersonOrder->id)<< aPersonOrder->personid(aPersonOrder->personid)<< aPersonOrder->filed_at(aPersonOrder->filed_at)<< aPersonOrder->amount(aPersonOrder->amount)<< aPersonOrder->status(aPersonOrder->status)<< aPersonOrder->promocode(aPersonOrder->promocode)<< aPersonOrder->final_amount(aPersonOrder->final_amount)<< aPersonOrder->sitecode(aPersonOrder->sitecode);
		ins.Where(aPersonOrder->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone ParkingArea records 
		ParkingArea aParkingArea;
		Inserter ins(aParkingArea, Int64(newTenantID));
		ins << aParkingArea->id(aParkingArea->id)<< aParkingArea->name(aParkingArea->name)<< aParkingArea->spdgateid(aParkingArea->spdgateid)<< aParkingArea->enter_timeout(aParkingArea->enter_timeout)<< aParkingArea->exit_timeout(aParkingArea->exit_timeout);
		ins.Where(aParkingArea->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone ParkingTimer records 
		ParkingTimer aParkingTimer;
		Inserter ins(aParkingTimer, Int64(newTenantID));
		ins << aParkingTimer->orderid(aParkingTimer->orderid)<< aParkingTimer->end_time(aParkingTimer->end_time);
		ins.Where(aParkingTimer->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone ParkingOrderline records 
		ParkingOrderline aParkingOrderline;
		Inserter ins(aParkingOrderline, Int64(newTenantID));
		ins << aParkingOrderline->id(aParkingOrderline->id)<< aParkingOrderline->orderid(aParkingOrderline->orderid)<< aParkingOrderline->identifierid(aParkingOrderline->identifierid)<< aParkingOrderline->time_from(aParkingOrderline->time_from)<< aParkingOrderline->time_upto(aParkingOrderline->time_upto)<< aParkingOrderline->price(aParkingOrderline->price)<< aParkingOrderline->areaid(aParkingOrderline->areaid);
		ins.Where(aParkingOrderline->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone PersonOrderFiscalDoc records 
		PersonOrderFiscalDoc aPersonOrderFiscalDoc;
		Inserter ins(aPersonOrderFiscalDoc, Int64(newTenantID));
		ins << aPersonOrderFiscalDoc->orderid(aPersonOrderFiscalDoc->orderid)<< aPersonOrderFiscalDoc->fiscal_docid(aPersonOrderFiscalDoc->fiscal_docid);
		ins.Where(aPersonOrderFiscalDoc->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone PersonIdentifierOrderline records 
		PersonIdentifierOrderline aPersonIdentifierOrderline;
		Inserter ins(aPersonIdentifierOrderline, Int64(newTenantID));
		ins << aPersonIdentifierOrderline->orderid(aPersonIdentifierOrderline->orderid)<< aPersonIdentifierOrderline->identifierid(aPersonIdentifierOrderline->identifierid)<< aPersonIdentifierOrderline->price(aPersonIdentifierOrderline->price)<< aPersonIdentifierOrderline->service_rulename(aPersonIdentifierOrderline->service_rulename)<< aPersonIdentifierOrderline->make_transaction(aPersonIdentifierOrderline->make_transaction);
		ins.Where(aPersonIdentifierOrderline->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone AgentIdentifierOrderline records 
		AgentIdentifierOrderline aAgentIdentifierOrderline;
		Inserter ins(aAgentIdentifierOrderline, Int64(newTenantID));
		ins << aAgentIdentifierOrderline->orderid(aAgentIdentifierOrderline->orderid)<< aAgentIdentifierOrderline->identifierid(aAgentIdentifierOrderline->identifierid)<< aAgentIdentifierOrderline->price(aAgentIdentifierOrderline->price);
		ins.Where(aAgentIdentifierOrderline->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone PersonInvoice records 
		PersonInvoice aPersonInvoice;
		Inserter ins(aPersonInvoice, Int64(newTenantID));
		ins << aPersonInvoice->id(aPersonInvoice->id)<< aPersonInvoice->orderid(aPersonInvoice->orderid);
		ins.Where(aPersonInvoice->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone IdentifierInvoice records 
		IdentifierInvoice aIdentifierInvoice;
		Inserter ins(aIdentifierInvoice, Int64(newTenantID));
		ins << aIdentifierInvoice->id(aIdentifierInvoice->id)<< aIdentifierInvoice->identifierid(aIdentifierInvoice->identifierid)<< aIdentifierInvoice->fiscal_docid(aIdentifierInvoice->fiscal_docid)<< aIdentifierInvoice->eid(aIdentifierInvoice->eid);
		ins.Where(aIdentifierInvoice->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone BaloonProvider records 
		BaloonProvider aBaloonProvider;
		Inserter ins(aBaloonProvider, Int64(newTenantID));
		ins << aBaloonProvider->default_paygateid(aBaloonProvider->default_paygateid)<< aBaloonProvider->default_fiscalgateid(aBaloonProvider->default_fiscalgateid)<< aBaloonProvider->code_conversion_algo(aBaloonProvider->code_conversion_algo)<< aBaloonProvider->agent_commission(aBaloonProvider->agent_commission)<< aBaloonProvider->ident_sale_price(aBaloonProvider->ident_sale_price)<< aBaloonProvider->timeout_before(aBaloonProvider->timeout_before)<< aBaloonProvider->sun(aBaloonProvider->sun)<< aBaloonProvider->mon(aBaloonProvider->mon)<< aBaloonProvider->tue(aBaloonProvider->tue)<< aBaloonProvider->wed(aBaloonProvider->wed)<< aBaloonProvider->thu(aBaloonProvider->thu)<< aBaloonProvider->fri(aBaloonProvider->fri)<< aBaloonProvider->sat(aBaloonProvider->sat);
		ins.Where(aBaloonProvider->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone ExternalResource records 
		ExternalResource aExternalResource;
		Inserter ins(aExternalResource, Int64(newTenantID));
		ins << aExternalResource->id(aExternalResource->id)<< aExternalResource->name(aExternalResource->name)<< aExternalResource->uri(aExternalResource->uri)<< aExternalResource->description(aExternalResource->description)<< aExternalResource->enabled(aExternalResource->enabled);
		ins.Where(aExternalResource->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone Agent records 
		Agent aAgent;
		Inserter ins(aAgent, Int64(newTenantID));
		ins << aAgent->id(aAgent->id)<< aAgent->userTokenID(aAgent->userTokenID)<< aAgent->name(aAgent->name)<< aAgent->phone(aAgent->phone)<< aAgent->email(aAgent->email)<< aAgent->comments(aAgent->comments)<< aAgent->commission(aAgent->commission)<< aAgent->is_default(aAgent->is_default);
		ins.Where(aAgent->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone AgentOrder records 
		AgentOrder aAgentOrder;
		Inserter ins(aAgentOrder, Int64(newTenantID));
		ins << aAgentOrder->id(aAgentOrder->id)<< aAgentOrder->agentid(aAgentOrder->agentid)<< aAgentOrder->code(aAgentOrder->code)<< aAgentOrder->amount(aAgentOrder->amount)<< aAgentOrder->commission(aAgentOrder->commission)<< aAgentOrder->filed_at(aAgentOrder->filed_at)<< aAgentOrder->comment(aAgentOrder->comment)<< aAgentOrder->status(aAgentOrder->status)<< aAgentOrder->AgentOrderType(aAgentOrder->AgentOrderType);
		ins.Where(aAgentOrder->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone IdentifierAgentOrder records 
		IdentifierAgentOrder aIdentifierAgentOrder;
		Inserter ins(aIdentifierAgentOrder, Int64(newTenantID));
		ins << aIdentifierAgentOrder->id(aIdentifierAgentOrder->id)<< aIdentifierAgentOrder->valid_from(aIdentifierAgentOrder->valid_from)<< aIdentifierAgentOrder->valid_to(aIdentifierAgentOrder->valid_to)<< aIdentifierAgentOrder->permanent_rulename(aIdentifierAgentOrder->permanent_rulename)<< aIdentifierAgentOrder->categoryname(aIdentifierAgentOrder->categoryname)<< aIdentifierAgentOrder->ratename(aIdentifierAgentOrder->ratename)<< aIdentifierAgentOrder->other_card_code(aIdentifierAgentOrder->other_card_code);
		ins.Where(aIdentifierAgentOrder->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone PackageAgentOrder records 
		PackageAgentOrder aPackageAgentOrder;
		Inserter ins(aPackageAgentOrder, Int64(newTenantID));
		ins << aPackageAgentOrder->id(aPackageAgentOrder->id)<< aPackageAgentOrder->service_rulename(aPackageAgentOrder->service_rulename);
		ins.Where(aPackageAgentOrder->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone AgentInvoice records 
		AgentInvoice aAgentInvoice;
		Inserter ins(aAgentInvoice, Int64(newTenantID));
		ins << aAgentInvoice->id(aAgentInvoice->id)<< aAgentInvoice->orderid(aAgentInvoice->orderid)<< aAgentInvoice->eid(aAgentInvoice->eid);
		ins.Where(aAgentInvoice->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone Account records 
		Account aAccount;
		Inserter ins(aAccount, Int64(newTenantID));
		ins << aAccount->currency(aAccount->currency)<< aAccount->personid(aAccount->personid)<< aAccount->valid(aAccount->valid)<< aAccount->balance(aAccount->balance);
		ins.Where(aAccount->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone PackageOrder records 
		PackageOrder aPackageOrder;
		Inserter ins(aPackageOrder, Int64(newTenantID));
		ins << aPackageOrder->id(aPackageOrder->id)<< aPackageOrder->identifierid(aPackageOrder->identifierid)<< aPackageOrder->service_rulename(aPackageOrder->service_rulename)<< aPackageOrder->filed_at(aPackageOrder->filed_at);
		ins.Where(aPackageOrder->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone Transaction records 
		Transaction aTransaction;
		Inserter ins(aTransaction, Int64(newTenantID));
		ins << aTransaction->id(aTransaction->id)<< aTransaction->personid(aTransaction->personid)<< aTransaction->agentid(aTransaction->agentid)<< aTransaction->currency(aTransaction->currency)<< aTransaction->eid(aTransaction->eid)<< aTransaction->amount(aTransaction->amount)<< aTransaction->status(aTransaction->status)<< aTransaction->comment(aTransaction->comment)<< aTransaction->in_sync(aTransaction->in_sync)<< aTransaction->valid_until(aTransaction->valid_until)<< aTransaction->meta(aTransaction->meta);
		ins.Where(aTransaction->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone InfoPlacement records 
		InfoPlacement aInfoPlacement;
		Inserter ins(aInfoPlacement, Int64(newTenantID));
		ins << aInfoPlacement->id(aInfoPlacement->id)<< aInfoPlacement->widget(aInfoPlacement->widget);
		ins.Where(aInfoPlacement->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone Holiday records 
		Holiday aHoliday;
		Inserter ins(aHoliday, Int64(newTenantID));
		ins << aHoliday->adate(aHoliday->adate)<< aHoliday->out_of_service(aHoliday->out_of_service)<< aHoliday->comment(aHoliday->comment);
		ins.Where(aHoliday->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone WeekTariffGroup records 
		WeekTariffGroup aWeekTariffGroup;
		Inserter ins(aWeekTariffGroup, Int64(newTenantID));
		ins << aWeekTariffGroup->id(aWeekTariffGroup->id)<< aWeekTariffGroup->name(aWeekTariffGroup->name)<< aWeekTariffGroup->description(aWeekTariffGroup->description)<< aWeekTariffGroup->is_default(aWeekTariffGroup->is_default);
		ins.Where(aWeekTariffGroup->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone WeekTariff records 
		WeekTariff aWeekTariff;
		Inserter ins(aWeekTariff, Int64(newTenantID));
		ins << aWeekTariff->id(aWeekTariff->id)<< aWeekTariff->agroupid(aWeekTariff->agroupid)<< aWeekTariff->week_day(aWeekTariff->week_day)<< aWeekTariff->time_from(aWeekTariff->time_from)<< aWeekTariff->time_upto(aWeekTariff->time_upto)<< aWeekTariff->price(aWeekTariff->price)<< aWeekTariff->pricetype(aWeekTariff->pricetype)<< aWeekTariff->free(aWeekTariff->free)<< aWeekTariff->total(aWeekTariff->total)<< aWeekTariff->counttype(aWeekTariff->counttype)<< aWeekTariff->external_name(aWeekTariff->external_name)<< aWeekTariff->identifier_rulename(aWeekTariff->identifier_rulename)<< aWeekTariff->service_rulename(aWeekTariff->service_rulename)<< aWeekTariff->identifier_categoryname(aWeekTariff->identifier_categoryname)<< aWeekTariff->ratename(aWeekTariff->ratename)<< aWeekTariff->enabled(aWeekTariff->enabled);
		ins.Where(aWeekTariff->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone SkipassConfiguration records 
		SkipassConfiguration aSkipassConfiguration;
		Inserter ins(aSkipassConfiguration, Int64(newTenantID));
		ins << aSkipassConfiguration->id(aSkipassConfiguration->id)<< aSkipassConfiguration->groupname(aSkipassConfiguration->groupname)<< aSkipassConfiguration->name(aSkipassConfiguration->name)<< aSkipassConfiguration->identifier_rulename(aSkipassConfiguration->identifier_rulename)<< aSkipassConfiguration->service_rulename(aSkipassConfiguration->service_rulename)<< aSkipassConfiguration->identifier_categoryname(aSkipassConfiguration->identifier_categoryname)<< aSkipassConfiguration->ratename(aSkipassConfiguration->ratename)<< aSkipassConfiguration->valid_from(aSkipassConfiguration->valid_from)<< aSkipassConfiguration->valid_to(aSkipassConfiguration->valid_to)<< aSkipassConfiguration->price(aSkipassConfiguration->price)<< aSkipassConfiguration->make_transaction(aSkipassConfiguration->make_transaction)<< aSkipassConfiguration->enabled(aSkipassConfiguration->enabled);
		ins.Where(aSkipassConfiguration->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone SpdGate records 
		SpdGate aSpdGate;
		Inserter ins(aSpdGate, Int64(newTenantID));
		ins << aSpdGate->id(aSpdGate->id)<< aSpdGate->name(aSpdGate->name)<< aSpdGate->host(aSpdGate->host)<< aSpdGate->port(aSpdGate->port)<< aSpdGate->enabled(aSpdGate->enabled);
		ins.Where(aSpdGate->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone PaidServiceCategory records 
		PaidServiceCategory aPaidServiceCategory;
		Inserter ins(aPaidServiceCategory, Int64(newTenantID));
		ins << aPaidServiceCategory->id(aPaidServiceCategory->id)<< aPaidServiceCategory->name(aPaidServiceCategory->name)<< aPaidServiceCategory->status(aPaidServiceCategory->status);
		ins.Where(aPaidServiceCategory->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone PaidServiceTariff records 
		PaidServiceTariff aPaidServiceTariff;
		Inserter ins(aPaidServiceTariff, Int64(newTenantID));
		ins << aPaidServiceTariff->id(aPaidServiceTariff->id)<< aPaidServiceTariff->name(aPaidServiceTariff->name)<< aPaidServiceTariff->status(aPaidServiceTariff->status);
		ins.Where(aPaidServiceTariff->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone PaidServiceHour records 
		PaidServiceHour aPaidServiceHour;
		Inserter ins(aPaidServiceHour, Int64(newTenantID));
		ins << aPaidServiceHour->id(aPaidServiceHour->id)<< aPaidServiceHour->name(aPaidServiceHour->name)<< aPaidServiceHour->status(aPaidServiceHour->status);
		ins.Where(aPaidServiceHour->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone PaidService records 
		PaidService aPaidService;
		Inserter ins(aPaidService, Int64(newTenantID));
		ins << aPaidService->id(aPaidService->id)<< aPaidService->categoryid(aPaidService->categoryid)<< aPaidService->name(aPaidService->name)<< aPaidService->tariffid(aPaidService->tariffid)<< aPaidService->hourid(aPaidService->hourid)<< aPaidService->description(aPaidService->description)<< aPaidService->price(aPaidService->price)<< aPaidService->status(aPaidService->status);
		ins.Where(aPaidService->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone Partner records 
		Partner aPartner;
		Inserter ins(aPartner, Int64(newTenantID));
		ins << aPartner->id(aPartner->id)<< aPartner->name(aPartner->name)<< aPartner->status(aPartner->status)<< aPartner->legal_address(aPartner->legal_address)<< aPartner->post_address(aPartner->post_address)<< aPartner->inn(aPartner->inn)<< aPartner->kpp(aPartner->kpp)<< aPartner->payment_account(aPartner->payment_account)<< aPartner->correspondent_account(aPartner->correspondent_account)<< aPartner->bank(aPartner->bank)<< aPartner->bik(aPartner->bik)<< aPartner->okpo(aPartner->okpo)<< aPartner->okato(aPartner->okato)<< aPartner->okved(aPartner->okved)<< aPartner->ogrn(aPartner->ogrn)<< aPartner->principal(aPartner->principal)<< aPartner->phone(aPartner->phone)<< aPartner->fax(aPartner->fax)<< aPartner->email(aPartner->email)<< aPartner->token(aPartner->token)<< aPartner->notification_email(aPartner->notification_email);
		ins.Where(aPartner->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone PartnerAdministrator records 
		PartnerAdministrator aPartnerAdministrator;
		Inserter ins(aPartnerAdministrator, Int64(newTenantID));
		ins << aPartnerAdministrator->auserTokenID(aPartnerAdministrator->auserTokenID)<< aPartnerAdministrator->fullname(aPartnerAdministrator->fullname);
		ins.Where(aPartnerAdministrator->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone PartnerManager records 
		PartnerManager aPartnerManager;
		Inserter ins(aPartnerManager, Int64(newTenantID));
		ins << aPartnerManager->auserTokenID(aPartnerManager->auserTokenID)<< aPartnerManager->partnerid(aPartnerManager->partnerid)<< aPartnerManager->fullname(aPartnerManager->fullname)<< aPartnerManager->passport_number(aPartnerManager->passport_number)<< aPartnerManager->issue_date(aPartnerManager->issue_date)<< aPartnerManager->issued_by(aPartnerManager->issued_by)<< aPartnerManager->department_code(aPartnerManager->department_code)<< aPartnerManager->phone(aPartnerManager->phone);
		ins.Where(aPartnerManager->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone PartnerContract records 
		PartnerContract aPartnerContract;
		Inserter ins(aPartnerContract, Int64(newTenantID));
		ins << aPartnerContract->id(aPartnerContract->id)<< aPartnerContract->partnerid(aPartnerContract->partnerid)<< aPartnerContract->barcode(aPartnerContract->barcode)<< aPartnerContract->start_date(aPartnerContract->start_date)<< aPartnerContract->end_date(aPartnerContract->end_date)<< aPartnerContract->benefit_type(aPartnerContract->benefit_type)<< aPartnerContract->benefit_amount(aPartnerContract->benefit_amount);
		ins.Where(aPartnerContract->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone PartnerAccount records 
		PartnerAccount aPartnerAccount;
		Inserter ins(aPartnerAccount, Int64(newTenantID));
		ins << aPartnerAccount->id(aPartnerAccount->id)<< aPartnerAccount->partnerid(aPartnerAccount->partnerid)<< aPartnerAccount->name(aPartnerAccount->name)<< aPartnerAccount->atype(aPartnerAccount->atype)<< aPartnerAccount->amount(aPartnerAccount->amount)<< aPartnerAccount->status(aPartnerAccount->status);
		ins.Where(aPartnerAccount->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone AccountTransaction records 
		AccountTransaction aAccountTransaction;
		Inserter ins(aAccountTransaction, Int64(newTenantID));
		ins << aAccountTransaction->id(aAccountTransaction->id)<< aAccountTransaction->accountid(aAccountTransaction->accountid)<< aAccountTransaction->amount(aAccountTransaction->amount)<< aAccountTransaction->cache_flow(aAccountTransaction->cache_flow)<< aAccountTransaction->reason(aAccountTransaction->reason);
		ins.Where(aAccountTransaction->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone PartnerWeekTariffGroup records 
		PartnerWeekTariffGroup aPartnerWeekTariffGroup;
		Inserter ins(aPartnerWeekTariffGroup, Int64(newTenantID));
		ins << aPartnerWeekTariffGroup->partnerid(aPartnerWeekTariffGroup->partnerid)<< aPartnerWeekTariffGroup->tariffgroupid(aPartnerWeekTariffGroup->tariffgroupid);
		ins.Where(aPartnerWeekTariffGroup->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone PartnerOrder records 
		PartnerOrder aPartnerOrder;
		Inserter ins(aPartnerOrder, Int64(newTenantID));
		ins << aPartnerOrder->id(aPartnerOrder->id)<< aPartnerOrder->partnerid(aPartnerOrder->partnerid)<< aPartnerOrder->initial_cost(aPartnerOrder->initial_cost)<< aPartnerOrder->discount(aPartnerOrder->discount)<< aPartnerOrder->total_cost(aPartnerOrder->total_cost)<< aPartnerOrder->week_tariffid(aPartnerOrder->week_tariffid);
		ins.Where(aPartnerOrder->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
		{// clone PartnerOrderline records 
		PartnerOrderline aPartnerOrderline;
		Inserter ins(aPartnerOrderline, Int64(newTenantID));
		ins << aPartnerOrderline->orderid(aPartnerOrderline->orderid)<< aPartnerOrderline->identifierid(aPartnerOrderline->identifierid)<< aPartnerOrderline->price(aPartnerOrderline->price)<< aPartnerOrderline->service_rulename(aPartnerOrderline->service_rulename);
		ins.Where(aPartnerOrderline->TenantID==ValueOf(baseTenantID));
		ins.Execute(rdb);
		}
	}
	catch (Exception &e) {
		Log(0) << "Exception received:" << e << endl;		if(dbType == DBTYPE_MYSQL) {
			Selector sel1("SET FOREIGN_KEY_CHECKS = 1;");
			sel1.Execute(rdb);
		}
		else if (dbType == DBTYPE_ORACLE) {
			Selector sel1("ALTER SESSION SET constraints=IMMEDIATE");
			sel1.Execute(rdb);
		}
		throw;
	}
	catch (...) {
		Log(0) << "Error executing clone" <<  endl;
		if(dbType == DBTYPE_MYSQL) {
			Selector sel1("SET FOREIGN_KEY_CHECKS = 1;");
			sel1.Execute(rdb);
		}
		else if (dbType == DBTYPE_ORACLE) {
			Selector sel1("ALTER SESSION SET constraints=IMMEDIATE");
			sel1.Execute(rdb);
		}
		throw ("Cloning of tenant failed. ");
	}
	
	if(dbType == DBTYPE_MYSQL) {
		Log(0) << "Finalize clone" <<  endl;
		Selector sel1("SET FOREIGN_KEY_CHECKS = 1;");
		sel1.Execute(rdb);
		Selector sel_non_zero("SET SQL_MODE=@OLD_SQL_MODE;");
		sel_non_zero.Execute(rdb);
		Log(0) << "Finalized successfully" <<  endl;
	}
	else if (dbType == DBTYPE_ORACLE) {
		Selector sel1("ALTER SESSION SET constraints=IMMEDIATE");
		sel1.Execute(rdb);
	}
}
void deleteBaloonTenant(Connection* rdb, int64_t TenantID){
	string dbType = rdb->GetDBType();
	if(dbType == DBTYPE_MYSQL) {
		Selector sel0("SET FOREIGN_KEY_CHECKS = 0;");
		sel0.Execute(rdb);
	}
	else if (dbType == DBTYPE_ORACLE) {
		Selector sel0("ALTER SESSION SET constraints=DEFERRED");
		sel0.Execute(rdb);
	}
	else {
		throw("Tenant cloning for DBType " + dbType + " is not supported. ");
	}
	
	if(Effi::GetCurrentContext()->TenantID() != TENANT_ADMIN) {
		throw("You can call cloning tenants only from Administative tenant. ");
	}
	
	try {
		{// delete Person records 
		Person aPerson;
		Deleter del(aPerson);
		del.Where(aPerson->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete SpdServiceRule records 
		SpdServiceRule aSpdServiceRule;
		Deleter del(aSpdServiceRule);
		del.Where(aSpdServiceRule->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete PushNotification records 
		PushNotification aPushNotification;
		Deleter del(aPushNotification);
		del.Where(aPushNotification->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete PushNotificationMessages records 
		PushNotificationMessages aPushNotificationMessages;
		Deleter del(aPushNotificationMessages);
		del.Where(aPushNotificationMessages->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete PushNotificationDeliveries records 
		PushNotificationDeliveries aPushNotificationDeliveries;
		Deleter del(aPushNotificationDeliveries);
		del.Where(aPushNotificationDeliveries->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete IdentifierRule records 
		IdentifierRule aIdentifierRule;
		Deleter del(aIdentifierRule);
		del.Where(aIdentifierRule->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete IdentifierServiceRule records 
		IdentifierServiceRule aIdentifierServiceRule;
		Deleter del(aIdentifierServiceRule);
		del.Where(aIdentifierServiceRule->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete IdentifierCategory records 
		IdentifierCategory aIdentifierCategory;
		Deleter del(aIdentifierCategory);
		del.Where(aIdentifierCategory->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete IdentifierRate records 
		IdentifierRate aIdentifierRate;
		Deleter del(aIdentifierRate);
		del.Where(aIdentifierRate->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete Identifier records 
		Identifier aIdentifier;
		Deleter del(aIdentifier);
		del.Where(aIdentifier->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete OrderSite records 
		OrderSite aOrderSite;
		Deleter del(aOrderSite);
		del.Where(aOrderSite->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete PersonOrder records 
		PersonOrder aPersonOrder;
		Deleter del(aPersonOrder);
		del.Where(aPersonOrder->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete ParkingArea records 
		ParkingArea aParkingArea;
		Deleter del(aParkingArea);
		del.Where(aParkingArea->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete ParkingTimer records 
		ParkingTimer aParkingTimer;
		Deleter del(aParkingTimer);
		del.Where(aParkingTimer->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete ParkingOrderline records 
		ParkingOrderline aParkingOrderline;
		Deleter del(aParkingOrderline);
		del.Where(aParkingOrderline->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete PersonOrderFiscalDoc records 
		PersonOrderFiscalDoc aPersonOrderFiscalDoc;
		Deleter del(aPersonOrderFiscalDoc);
		del.Where(aPersonOrderFiscalDoc->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete PersonIdentifierOrderline records 
		PersonIdentifierOrderline aPersonIdentifierOrderline;
		Deleter del(aPersonIdentifierOrderline);
		del.Where(aPersonIdentifierOrderline->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete AgentIdentifierOrderline records 
		AgentIdentifierOrderline aAgentIdentifierOrderline;
		Deleter del(aAgentIdentifierOrderline);
		del.Where(aAgentIdentifierOrderline->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete PersonInvoice records 
		PersonInvoice aPersonInvoice;
		Deleter del(aPersonInvoice);
		del.Where(aPersonInvoice->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete IdentifierInvoice records 
		IdentifierInvoice aIdentifierInvoice;
		Deleter del(aIdentifierInvoice);
		del.Where(aIdentifierInvoice->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete BaloonProvider records 
		BaloonProvider aBaloonProvider;
		Deleter del(aBaloonProvider);
		del.Where(aBaloonProvider->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete ExternalResource records 
		ExternalResource aExternalResource;
		Deleter del(aExternalResource);
		del.Where(aExternalResource->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete Agent records 
		Agent aAgent;
		Deleter del(aAgent);
		del.Where(aAgent->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete AgentOrder records 
		AgentOrder aAgentOrder;
		Deleter del(aAgentOrder);
		del.Where(aAgentOrder->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete IdentifierAgentOrder records 
		IdentifierAgentOrder aIdentifierAgentOrder;
		Deleter del(aIdentifierAgentOrder);
		del.Where(aIdentifierAgentOrder->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete PackageAgentOrder records 
		PackageAgentOrder aPackageAgentOrder;
		Deleter del(aPackageAgentOrder);
		del.Where(aPackageAgentOrder->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete AgentInvoice records 
		AgentInvoice aAgentInvoice;
		Deleter del(aAgentInvoice);
		del.Where(aAgentInvoice->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete Account records 
		Account aAccount;
		Deleter del(aAccount);
		del.Where(aAccount->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete PackageOrder records 
		PackageOrder aPackageOrder;
		Deleter del(aPackageOrder);
		del.Where(aPackageOrder->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete Transaction records 
		Transaction aTransaction;
		Deleter del(aTransaction);
		del.Where(aTransaction->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete InfoPlacement records 
		InfoPlacement aInfoPlacement;
		Deleter del(aInfoPlacement);
		del.Where(aInfoPlacement->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete Holiday records 
		Holiday aHoliday;
		Deleter del(aHoliday);
		del.Where(aHoliday->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete WeekTariffGroup records 
		WeekTariffGroup aWeekTariffGroup;
		Deleter del(aWeekTariffGroup);
		del.Where(aWeekTariffGroup->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete WeekTariff records 
		WeekTariff aWeekTariff;
		Deleter del(aWeekTariff);
		del.Where(aWeekTariff->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete SkipassConfiguration records 
		SkipassConfiguration aSkipassConfiguration;
		Deleter del(aSkipassConfiguration);
		del.Where(aSkipassConfiguration->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete SpdGate records 
		SpdGate aSpdGate;
		Deleter del(aSpdGate);
		del.Where(aSpdGate->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete PaidServiceCategory records 
		PaidServiceCategory aPaidServiceCategory;
		Deleter del(aPaidServiceCategory);
		del.Where(aPaidServiceCategory->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete PaidServiceTariff records 
		PaidServiceTariff aPaidServiceTariff;
		Deleter del(aPaidServiceTariff);
		del.Where(aPaidServiceTariff->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete PaidServiceHour records 
		PaidServiceHour aPaidServiceHour;
		Deleter del(aPaidServiceHour);
		del.Where(aPaidServiceHour->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete PaidService records 
		PaidService aPaidService;
		Deleter del(aPaidService);
		del.Where(aPaidService->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete Partner records 
		Partner aPartner;
		Deleter del(aPartner);
		del.Where(aPartner->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete PartnerAdministrator records 
		PartnerAdministrator aPartnerAdministrator;
		Deleter del(aPartnerAdministrator);
		del.Where(aPartnerAdministrator->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete PartnerManager records 
		PartnerManager aPartnerManager;
		Deleter del(aPartnerManager);
		del.Where(aPartnerManager->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete PartnerContract records 
		PartnerContract aPartnerContract;
		Deleter del(aPartnerContract);
		del.Where(aPartnerContract->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete PartnerAccount records 
		PartnerAccount aPartnerAccount;
		Deleter del(aPartnerAccount);
		del.Where(aPartnerAccount->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete AccountTransaction records 
		AccountTransaction aAccountTransaction;
		Deleter del(aAccountTransaction);
		del.Where(aAccountTransaction->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete PartnerWeekTariffGroup records 
		PartnerWeekTariffGroup aPartnerWeekTariffGroup;
		Deleter del(aPartnerWeekTariffGroup);
		del.Where(aPartnerWeekTariffGroup->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete PartnerOrder records 
		PartnerOrder aPartnerOrder;
		Deleter del(aPartnerOrder);
		del.Where(aPartnerOrder->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
		{// delete PartnerOrderline records 
		PartnerOrderline aPartnerOrderline;
		Deleter del(aPartnerOrderline);
		del.Where(aPartnerOrderline->TenantID==ValueOf(TenantID));
		del.Execute(rdb);
		}
	}
	catch (Exception &e) {
		Log(0) << "Exception received:" << e << endl;		if(dbType == DBTYPE_MYSQL) {
			Selector sel1("SET FOREIGN_KEY_CHECKS = 1;");
			sel1.Execute(rdb);
		}
		else if (dbType == DBTYPE_ORACLE) {
			Selector sel1("ALTER SESSION SET constraints=IMMEDIATE");
			sel1.Execute(rdb);
		}
		throw;
	}
	catch (...) {
		Log(0) << "Error executing clone" <<  endl;
		if(dbType == DBTYPE_MYSQL) {
			Selector sel1("SET FOREIGN_KEY_CHECKS = 1;");
			sel1.Execute(rdb);
		}
		else if (dbType == DBTYPE_ORACLE) {
			Selector sel1("ALTER SESSION SET constraints=IMMEDIATE");
			sel1.Execute(rdb);
		}
		throw ("Cloning of tenant failed. ");
	}
	
	if(dbType == DBTYPE_MYSQL) {
		Log(0) << "Finalize clone" <<  endl;
		Selector sel1("SET FOREIGN_KEY_CHECKS = 1;");
		sel1.Execute(rdb);
		Log(0) << "Finalized successfully" <<  endl;
	}
	else if (dbType == DBTYPE_ORACLE) {
		Selector sel1("ALTER SESSION SET constraints=IMMEDIATE");
		sel1.Execute(rdb);
	}
}
} // namespace BALOON
