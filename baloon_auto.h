#ifndef _BALOON_AUTO_H_
#define _BALOON_AUTO_H_

#include "ardbms/convertor.h"
#include "ardbms/domain.h"
#include "acommon/blobfile.h"
#include "datalist/remotelist.h"

#include "dbservice/appdbservice.h"

#include "/usr/local/effi/include/Services/Authorizer/authorizer_auto.h"
//using namespace  AUTHORIZER;

#include "/usr/local/computerica/include/Paygate/paygate_auto.h"
//using namespace  PAYGATE;

#include "/usr/local/computerica/include/Basicsite/basicsite_auto.h"
//using namespace  BASICSITE;

#include "/usr/local/computerica/include/Fiscal/fiscal_auto.h"
//using namespace  FISCAL;

#include "/usr/local/effi/include/Services/Multimedia/multimedia_auto.h"
//using namespace  MULTIMEDIA;

#include "./scheme.h"
namespace BALOON {

extern const string Baloon_DECL BOOL_FALSE;
extern const string Baloon_DECL BOOL_TRUE;
extern const string Baloon_DECL both;
extern const string Baloon_DECL none;
extern const string Baloon_DECL forward;
extern const string Baloon_DECL back;
extern const string Baloon_DECL rectangle;
extern const string Baloon_DECL circle;
extern const string Baloon_DECL ellipse;
extern const string Baloon_DECL diamond;
extern const string Baloon_DECL note;
extern const string Baloon_DECL tab;
extern const string Baloon_DECL folder;
extern const string Baloon_DECL component;
extern const string Baloon_DECL AND;
extern const string Baloon_DECL OR;
extern const string Baloon_DECL PRESENT_DAY;
extern const string Baloon_DECL PREVIOUS_DAY;
extern const string Baloon_DECL PRESENT_WEEK;
extern const string Baloon_DECL PREVIOUS_WEEK;
extern const string Baloon_DECL PRESENT_MONTH;
extern const string Baloon_DECL PREVIOUS_MONTH;
extern const string Baloon_DECL OTHER;
extern const string Baloon_DECL AUTH_SIGNUP;
extern const string Baloon_DECL AUTH_RESTOREPWD;
extern const string Baloon_DECL ENABLED;
extern const string Baloon_DECL DISABLED;
extern const string Baloon_DECL man;
extern const string Baloon_DECL lady;
extern const string Baloon_DECL CODE_CONVERSION_ALGO_REVERSE_HEX;
extern const string Baloon_DECL AGENT_ORDER_STATUS_OFFLINE;
extern const string Baloon_DECL AGENT_ORDER_STATUS_WAITING;
extern const string Baloon_DECL AGENT_ORDER_STATUS_PAYED;
extern const string Baloon_DECL AGENT_ORDER_STATUS_EXPIRED;
extern const string Baloon_DECL AGENT_ORDER_STATUS_REJECTED;
extern const string Baloon_DECL AGENT_ORDER_STATUS_CANCELED;
extern const string Baloon_DECL AGENT_ORDER_STATUS_REFUNDED;
extern const string Baloon_DECL AGENT_ORDER_STATUS_ERROR;
extern const string Baloon_DECL PERSON_ORDER_STATUS_WAITING;
extern const string Baloon_DECL PERSON_ORDER_STATUS_PAYED;
extern const string Baloon_DECL PERSON_ORDER_STATUS_EXPIRED;
extern const string Baloon_DECL PERSON_ORDER_STATUS_REJECTED;
extern const string Baloon_DECL PERSON_ORDER_STATUS_CANCELED;
extern const string Baloon_DECL PERSON_ORDER_STATUS_REFUNDED;
extern const string Baloon_DECL PERSON_ORDER_STATUS_ERROR;
extern const string Baloon_DECL PERSON_ORDER_STATUS_PAYMENT_NOT_REQUIRED;
extern const string Baloon_DECL WEEKDAY;
extern const string Baloon_DECL WEEKEND;
extern const string Baloon_DECL HOLIDAY;
extern const string Baloon_DECL IDENTIFIER_STATUS_OK;
extern const string Baloon_DECL IDENTIFIER_STATUS_CANCELED;
extern const string Baloon_DECL IDENTIFIER_STATUS_WAITING;
extern const string Baloon_DECL SUNDAY;
extern const string Baloon_DECL MONDAY;
extern const string Baloon_DECL TUESDAY;
extern const string Baloon_DECL WEDNESDAY;
extern const string Baloon_DECL THURSDAY;
extern const string Baloon_DECL FRIDAY;
extern const string Baloon_DECL SATURDAY;
extern const string Baloon_DECL HIGHDAY;
extern const string Baloon_DECL INFO_WIDGET;
extern const string Baloon_DECL AD_WIDGET;
extern const string Baloon_DECL DISCOUNT_BENEFIT;
extern const string Baloon_DECL REWARD_BENEFIT;
extern const string Baloon_DECL BLOCKED;
extern const string Baloon_DECL ACTIVE;
extern const string Baloon_DECL REFILL;
extern const string Baloon_DECL EXPENSE;
extern const string Baloon_DECL DEBIT_ACCOUNT;
extern const string Baloon_DECL CREDIT_ACCOUNT;
extern const string Baloon_DECL BALOON_AGENTORDER_AGENTORDER;
extern const string Baloon_DECL BALOON_AGENTORDER_IDENTIFIERAGENTORDER;
extern const string Baloon_DECL BALOON_AGENTORDER_PACKAGEAGENTORDER;

Baloon_DECL const Value BOOLValues();
Baloon_DECL const Value ARROWDIRValues();
Baloon_DECL const Value SHAPEValues();
Baloon_DECL const Value AND_ORValues();
Baloon_DECL const Value PERIOD_TYPEValues();
Baloon_DECL const Value AUTH_TYPEValues();
Baloon_DECL const Value ENABLED_STATUSValues();
Baloon_DECL const Value GENDERValues();
Baloon_DECL const Value CODE_CONVERSION_ALGOValues();
Baloon_DECL const Value AGENT_ORDER_STATUSValues();
Baloon_DECL const Value PERSON_ORDER_STATUSValues();
Baloon_DECL const Value DAY_TYPEValues();
Baloon_DECL const Value IDENTIFIER_STATUSValues();
Baloon_DECL const Value WEEK_DAYValues();
Baloon_DECL const Value WIDGET_TYPEValues();
Baloon_DECL const Value PARTNER_BENEFITValues();
Baloon_DECL const Value BLOCK_STATUSValues();
Baloon_DECL const Value CACHE_FLOWValues();
Baloon_DECL const Value ACCOUNT_TYPEValues();
Baloon_DECL const Value AgentOrder_TYPEValues();

class Baloon_DECL BALOONPerson : public ApplicationDBService {
public:
	BALOONPerson(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONPerson(){}
public:
	// Wrappers for manual methods
	static const Value __Register_API(const Param& param);
	static const Value __RegisterNewClient_API(const Param& param);
	static const Value __CreateNewClient_API(const Param& param);
	static const Value __AddToContour(const Param& param);
	static const Value __ShowRegistration(const Param& param);
	static const Value __ShowUserPanel(const Param& param);
	static const Value __ShowTicketsShop(const Param& param);
	static const Value __ShowSuccessPayment(const Param& param);
	static const Value __ShowFailedPayment(const Param& param);
	static const Value __ShowSuccessPaymentIOS(const Param& param);
	static const Value __ShowSuccessDownloadPayment(const Param& param);
	static const Value __GetCurrent(const Param& param);
	static const Value __UpdateCurrent(const Param& param);
	static const Value __ChangeCurrentPassword(const Param& param);
	static const Value __GetCurrentAccounts(const Param& param);

	// Wrappers for automatic methods declaration
	static const Value __Add(const Param& param);
	static const Value __Update(const Param& param);
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);
	static const Value __PersonListGet(const Param& param);

	// Manual methods declaration
	static const Value Register_API(const Str regcode, const Str email, const Str password, const Str password2, const Str phone);
	static const Value RegisterNewClient_API(const Str regcode, const Str email, const Str password, const Str password2, const Str phone, const Str last_name, const Str first_name, const Str middle_name, const ADate birthdate, const Str gender);
	static const Value CreateNewClient_API(const Str email, const Str password, const Str password2, const Str last_name, const Str first_name, const Str middle_name, const ADate birthdate, const Str gender);
	static const Value AddToContour(const Int id);
	static const Value ShowRegistration();
	static const Value ShowUserPanel();
	static const Value ShowTicketsShop();
	static const Value ShowSuccessPayment();
	static const Value ShowFailedPayment();
	static const Value ShowSuccessPaymentIOS();
	static const Value ShowSuccessDownloadPayment();
	static const Value GetCurrent();
	static const Value UpdateCurrent(const optional<Str> last_name, const optional<Str> first_name, const optional<Str> middle_name, const optional<ADate> birthdate, const optional<Str> gender, const optional<Str> phone);
	static const Value ChangeCurrentPassword(const Str old_password, const Str password, const Str password2);
	static const Value GetCurrentAccounts();

	// Automatic methods declaration
	static const Value Add(const Int64 userTokenID, const Str last_name, const Str first_name, const Str middle_name, const ADate birthdate, const Str gender, const Str phone, const Str email, const Str regcode, const Time last_sync, const Str oid, const ADate registered_at);
	static const Value Update(const Int id, const optional<Int64> userTokenID, const optional<Str> last_name, const optional<Str> first_name, const optional<Str> middle_name, const optional<ADate> birthdate, const optional<Str> gender, const optional<Str> phone, const optional<Str> email, const optional<Str> regcode, const optional<Time> last_sync, const optional<Str> oid, const optional<ADate> registered_at);
	static const Value Delete(const Int id);
	static const Value Get(const Int id);
	static const Value PersonListGet(const optional<Int64> userTokenID);
private: //prohibited operations
	BALOONPerson(const BALOONPerson&);
	BALOONPerson& operator=(const BALOONPerson&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONSpdServiceRule : public ApplicationDBService {
public:
	BALOONSpdServiceRule(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONSpdServiceRule(){}
public:
	// Wrappers for manual methods
	static const Value __SpdServiceRuleMenuListGet(const Param& param);
	static const Value __SpdServiceRuleEnabledListGet(const Param& param);
	static const Value __SpdServiceRuleListGet_API(const Param& param);
	static const Value __SyncDictionary(const Param& param);

	// Wrappers for automatic methods declaration
	static const Value __Add(const Param& param);
	static const Value __Update(const Param& param);
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);
	static const Value __SpdServiceRuleListGet(const Param& param);

	// Manual methods declaration
	static const Value SpdServiceRuleMenuListGet();
	static const Value SpdServiceRuleEnabledListGet();
	static const Value SpdServiceRuleListGet_API(const Str identifier_rulename);
	static const Value SyncDictionary();

	// Automatic methods declaration
	static const Value Add(const Str name, const Str enabled, const Int extid, const Int extcode, const Str comment, const Decimal package_cost);
	static const Value Update(const Str name, const optional<Str> enabled, const optional<Int> extid, const optional<Int> extcode, const optional<Str> comment, const optional<Decimal> package_cost);
	static const Value Delete(const Str name);
	static const Value Get(const Str name);
	static const Value SpdServiceRuleListGet();
private: //prohibited operations
	BALOONSpdServiceRule(const BALOONSpdServiceRule&);
	BALOONSpdServiceRule& operator=(const BALOONSpdServiceRule&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONPushNotification : public ApplicationDBService {
public:
	BALOONPushNotification(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONPushNotification(){}
public:
	// Wrappers for manual methods
	static const Value __Delete(const Param& param);
	static const Value __DeviceRegisterApple_FE(const Param& param);
	static const Value __DeviceRegisterAndroid_FE(const Param& param);
	static const Value __DeviceRegisterWindows_FE(const Param& param);

	// Wrappers for automatic methods declaration
	static const Value __Add(const Param& param);
	static const Value __Update(const Param& param);
	static const Value __Get(const Param& param);
	static const Value __PushNotificationListGet(const Param& param);

	// Manual methods declaration
	static const Value Delete(const Int id);
	static const Value DeviceRegisterApple_FE(const Str token, const Str first_name, const Str last_name, const Str middle_name, const Str description);
	static const Value DeviceRegisterAndroid_FE(const Str token, const Str first_name, const Str last_name, const Str middle_name, const Str description);
	static const Value DeviceRegisterWindows_FE(const Str token, const Str first_name, const Str last_name, const Str middle_name, const Str description);

	// Automatic methods declaration
	static const Value Add(const Str token, const Int token_type, const Str first_name, const Str last_name, const Str middle_name, const Str description, const Str enabled, const Time valid_from, const Time last_register);
	static const Value Update(const Int id, const optional<Str> token, const optional<Int> token_type, const optional<Str> first_name, const optional<Str> last_name, const optional<Str> middle_name, const optional<Str> description, const optional<Str> enabled, const optional<Time> valid_from, const optional<Time> last_register);
	static const Value Get(const Int id);
	static const Value PushNotificationListGet();
private: //prohibited operations
	BALOONPushNotification(const BALOONPushNotification&);
	BALOONPushNotification& operator=(const BALOONPushNotification&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONPushNotificationMessages : public ApplicationDBService {
public:
	BALOONPushNotificationMessages(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONPushNotificationMessages(){}
public:
	// Wrappers for manual methods
	static const Value __SendAllNotifications(const Param& param);
	static const Value __SendNotifications(const Param& param);
	static const Value __PushNotificationMessageAdd_FE(const Param& param);
	static const Value __PushNotificationMessageUpdate_FE(const Param& param);

	// Wrappers for automatic methods declaration
	static const Value __Add(const Param& param);
	static const Value __Update(const Param& param);
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);
	static const Value __PushNotificationMessagesListGet(const Param& param);

	// Manual methods declaration
	static const Value SendAllNotifications();
	static const Value SendNotifications(const Int id, const Str sent, const Str enabled);
	static const Value PushNotificationMessageAdd_FE(const Str title, const Str message, const Str enabled);
	static const Value PushNotificationMessageUpdate_FE(const Int id, const Str title, const Str message, const Str enabled);

	// Automatic methods declaration
	static const Value Add(const Str title, const Str message, const Str sent, const Str enabled, const Time message_date);
	static const Value Update(const Int id, const optional<Str> title, const optional<Str> message, const optional<Str> sent, const optional<Str> enabled, const optional<Time> message_date);
	static const Value Delete(const Int id);
	static const Value Get(const Int id);
	static const Value PushNotificationMessagesListGet();
private: //prohibited operations
	BALOONPushNotificationMessages(const BALOONPushNotificationMessages&);
	BALOONPushNotificationMessages& operator=(const BALOONPushNotificationMessages&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONPushNotificationDeliveries : public ApplicationDBService {
public:
	BALOONPushNotificationDeliveries(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONPushNotificationDeliveries(){}
public:
	// Wrappers for manual methods
	static const Value __ResendNotification(const Param& param);
	static const Value __PushNotificationDeliveryAdd_FE(const Param& param);

	// Wrappers for automatic methods declaration
	static const Value __Add(const Param& param);
	static const Value __Update(const Param& param);
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);
	static const Value __PushNotificationDeliveriesListGet(const Param& param);

	// Manual methods declaration
	static const Value ResendNotification(const Int id);
	static const Value PushNotificationDeliveryAdd_FE(const Int messageid, const Int deviceid, const Str sendresult);

	// Automatic methods declaration
	static const Value Add(const Int deviceid, const Int messageid, const Str sendresult, const Time delivery_date);
	static const Value Update(const Int id, const optional<Int> deviceid, const optional<Int> messageid, const optional<Str> sendresult, const optional<Time> delivery_date);
	static const Value Delete(const Int id);
	static const Value Get(const Int id);
	static const Value PushNotificationDeliveriesListGet(const optional<Int> messageid);
private: //prohibited operations
	BALOONPushNotificationDeliveries(const BALOONPushNotificationDeliveries&);
	BALOONPushNotificationDeliveries& operator=(const BALOONPushNotificationDeliveries&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONIdentifierRule : public ApplicationDBService {
public:
	BALOONIdentifierRule(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONIdentifierRule(){}
public:
	// Wrappers for manual methods
	static const Value __IdentifierRuleMenuListGet(const Param& param);
	static const Value __IdentifierRuleEnabledListGet(const Param& param);
	static const Value __IdentifierRuleListGet_API(const Param& param);
	static const Value __SyncDictionary(const Param& param);

	// Wrappers for automatic methods declaration
	static const Value __Add(const Param& param);
	static const Value __Update(const Param& param);
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);
	static const Value __IdentifierRuleListGet(const Param& param);

	// Manual methods declaration
	static const Value IdentifierRuleMenuListGet();
	static const Value IdentifierRuleEnabledListGet();
	static const Value IdentifierRuleListGet_API();
	static const Value SyncDictionary();

	// Automatic methods declaration
	static const Value Add(const Str name, const Str enabled, const Str replenish_enabled, const Int extid, const Int extcode, const Str comment);
	static const Value Update(const Str name, const optional<Str> enabled, const optional<Str> replenish_enabled, const optional<Int> extid, const optional<Int> extcode, const optional<Str> comment);
	static const Value Delete(const Str name);
	static const Value Get(const Str name);
	static const Value IdentifierRuleListGet();
private: //prohibited operations
	BALOONIdentifierRule(const BALOONIdentifierRule&);
	BALOONIdentifierRule& operator=(const BALOONIdentifierRule&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONIdentifierServiceRule : public ApplicationDBService {
public:
	BALOONIdentifierServiceRule(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONIdentifierServiceRule(){}
public:
	// Wrappers for manual methods
	static const Value __IdentifierServiceRuleListGet_API(const Param& param);
	static const Value __Delete(const Param& param);

	// Wrappers for automatic methods declaration
	static const Value __Add(const Param& param);
	static const Value __Update(const Param& param);
	static const Value __Get(const Param& param);
	static const Value __IdentifierServiceRuleListGet(const Param& param);

	// Manual methods declaration
	static const Value IdentifierServiceRuleListGet_API();
	static const Value Delete(const Str identifier_rulename, const Str service_rulename);

	// Automatic methods declaration
	static const Value Add(const Str identifier_rulename, const Str service_rulename, const Str name, const Str enabled, const Decimal weekday_cost, const Decimal weekend_cost);
	static const Value Update(const Str identifier_rulename, const Str service_rulename, const optional<Str> name, const optional<Str> enabled, const optional<Decimal> weekday_cost, const optional<Decimal> weekend_cost);
	static const Value Get(const Str identifier_rulename, const Str service_rulename);
	static const Value IdentifierServiceRuleListGet(const optional<Str> identifier_rulename, const optional<Str> service_rulename);
private: //prohibited operations
	BALOONIdentifierServiceRule(const BALOONIdentifierServiceRule&);
	BALOONIdentifierServiceRule& operator=(const BALOONIdentifierServiceRule&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONIdentifierCategory : public ApplicationDBService {
public:
	BALOONIdentifierCategory(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONIdentifierCategory(){}
public:
	// Wrappers for manual methods
	static const Value __IdentifierCategoryMenuListGet(const Param& param);
	static const Value __IdentifierCategoryEnabledListGet(const Param& param);
	static const Value __IdentifierCategoryListGet_API(const Param& param);
	static const Value __SyncDictionary(const Param& param);
	static const Value __Update(const Param& param);

	// Wrappers for automatic methods declaration
	static const Value __Add(const Param& param);
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);
	static const Value __IdentifierCategoryListGet(const Param& param);

	// Manual methods declaration
	static const Value IdentifierCategoryMenuListGet();
	static const Value IdentifierCategoryEnabledListGet();
	static const Value IdentifierCategoryListGet_API();
	static const Value SyncDictionary();
	static const Value Update(const Str name, const optional<Str> enabled, const optional<Str> comment, const optional<Str> is_default);

	// Automatic methods declaration
	static const Value Add(const Str name, const Str enabled, const Int extid, const Int extcode, const Str comment, const Str is_default);
	static const Value Delete(const Str name);
	static const Value Get(const Str name);
	static const Value IdentifierCategoryListGet();
private: //prohibited operations
	BALOONIdentifierCategory(const BALOONIdentifierCategory&);
	BALOONIdentifierCategory& operator=(const BALOONIdentifierCategory&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONIdentifierRate : public ApplicationDBService {
public:
	BALOONIdentifierRate(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONIdentifierRate(){}
public:
	// Wrappers for manual methods
	static const Value __IdentifierRateMenuListGet(const Param& param);
	static const Value __IdentifierRateEnabledListGet(const Param& param);
	static const Value __IdentifierRateListGet_API(const Param& param);
	static const Value __SyncDictionary(const Param& param);

	// Wrappers for automatic methods declaration
	static const Value __Add(const Param& param);
	static const Value __Update(const Param& param);
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);
	static const Value __IdentifierRateListGet(const Param& param);

	// Manual methods declaration
	static const Value IdentifierRateMenuListGet();
	static const Value IdentifierRateEnabledListGet();
	static const Value IdentifierRateListGet_API();
	static const Value SyncDictionary();

	// Automatic methods declaration
	static const Value Add(const Str name, const Str enabled, const Int extid, const Int extcode, const Str comment);
	static const Value Update(const Str name, const optional<Str> enabled, const optional<Int> extid, const optional<Int> extcode, const optional<Str> comment);
	static const Value Delete(const Str name);
	static const Value Get(const Str name);
	static const Value IdentifierRateListGet();
private: //prohibited operations
	BALOONIdentifierRate(const BALOONIdentifierRate&);
	BALOONIdentifierRate& operator=(const BALOONIdentifierRate&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONIdentifier : public ApplicationDBService {
public:
	BALOONIdentifier(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONIdentifier(){}
public:
	// Wrappers for manual methods
	static const Value __OurIdentifierListGet(const Param& param);
	static const Value __OurPushToContour(const Param& param);
	static const Value __Add(const Param& param);
	static const Value __BindIdentifierToCurrentPerson_FE(const Param& param);
	static const Value __CurrentIdentifierListGet_FE(const Param& param);
	static const Value __IdentifierListDistributedByAccountsGet_FE(const Param& param);
	static const Value __BalancedIdentifierListGet_FE(const Param& param);
	static const Value __IdentifierNameUpdate_FE(const Param& param);
	static const Value __SetDateTimeFields(const Param& param);
	static const Value __GetDetails_API(const Param& param);
	static const Value __GetBalance_API(const Param& param);
	static const Value __GetBalance_FE(const Param& param);
	static const Value __PackagesListGet_API(const Param& param);
	static const Value __ConvertPrintCode(const Param& param);
	static const Value __ShowConvertPrintCode(const Param& param);
	static const Value __CreateUnpersonalizedIdentifierInSpd(const Param& param);
	static const Value __MyPayedIdentifierListGet_FE(const Param& param);
	static const Value __DownloadQRCode(const Param& param);
	static const Value __GenerateQRCode(const Param& param);
	static const Value __DownloadBarcode(const Param& param);
	static const Value __GenerateBarcode(const Param& param);
	static const Value __Cancel_FE(const Param& param);
	static const Value __Enable(const Param& param);

	// Wrappers for automatic methods declaration
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);
	static const Value __IdentifierListGet(const Param& param);

	// Manual methods declaration
	static const Value OurIdentifierListGet();
	static const Value OurPushToContour(const Int id);
	static const Value Add(const Int personid, const Str code, const Time valid_from, const Time valid_to, const Str permanent_rulename, const Str categoryname, const Str ratename);
	static const Value BindIdentifierToCurrentPerson_FE(const Str code);
	static const Value CurrentIdentifierListGet_FE();
	static const Value IdentifierListDistributedByAccountsGet_FE();
	static const Value BalancedIdentifierListGet_FE();
	static const Value IdentifierNameUpdate_FE(const Str code, const Str name);
	static const Value SetDateTimeFields();
	static const Value GetDetails_API(const Str code);
	static const Value GetBalance_API(const Str code, const Str print_code);
	static const Value GetBalance_FE(const Str code, const Str print_code);
	static const Value PackagesListGet_API(const Str code, const Str print_code);
	static const Value ConvertPrintCode(const Str print_code);
	static const Value ShowConvertPrintCode(const Str print_code);
	static const Value CreateUnpersonalizedIdentifierInSpd(const Str email, const Str code, const Time valid_from, const Time valid_to, const Str permanent_rulename, const Str categoryname, const Str ratename, const Decimal amount);
	static const Value MyPayedIdentifierListGet_FE();
	static const Value DownloadQRCode(const Str code);
	static const Value GenerateQRCode(const Str code);
	static const Value DownloadBarcode(const Str code);
	static const Value GenerateBarcode(const Str code);
	static const Value Cancel_FE(const Str code);
	static const Value Enable(const Int id);

	// Automatic methods declaration
	static const Value Delete(const Int id);
	static const Value Get(const Int id);
	static const Value IdentifierListGet(const optional<Int> personid, const optional<Str> permanent_rulename, const optional<Str> categoryname, const optional<Str> ratename, const optional<Int> spdgateid);
private: //prohibited operations
	BALOONIdentifier(const BALOONIdentifier&);
	BALOONIdentifier& operator=(const BALOONIdentifier&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONOrderSite : public ApplicationDBService {
public:
	BALOONOrderSite(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONOrderSite(){}
public:
	// Wrappers for manual methods

	// Wrappers for automatic methods declaration
	static const Value __Add(const Param& param);
	static const Value __Update(const Param& param);
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);
	static const Value __OrderSiteListGet(const Param& param);

	// Manual methods declaration

	// Automatic methods declaration
	static const Value Add(const Str code, const Str name, const Str link);
	static const Value Update(const Str code, const optional<Str> name, const optional<Str> link);
	static const Value Delete(const Str code);
	static const Value Get(const Str code);
	static const Value OrderSiteListGet();
private: //prohibited operations
	BALOONOrderSite(const BALOONOrderSite&);
	BALOONOrderSite& operator=(const BALOONOrderSite&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONPersonOrder : public ApplicationDBService {
public:
	BALOONPersonOrder(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONPersonOrder(){}
public:
	// Wrappers for manual methods
	static const Value __Place_API(const Param& param);
	static const Value __PlaceFree(const Param& param);
	static const Value __MyPlace_API(const Param& param);
	static const Value __Place_FE(const Param& param);
	static const Value __PlaceAny_FE(const Param& param);
	static const Value __PlaceAmount_FE(const Param& param);
	static const Value __Place_WEB(const Param& param);
	static const Value __CheckPromocode_API(const Param& param);
	static const Value __BuySkipass_API(const Param& param);
	static const Value __MyBuySkipass_API(const Param& param);
	static const Value __Add(const Param& param);
	static const Value __RequestPaymentStatus(const Param& param);
	static const Value __GetPreliminaryOrderAmount_FE(const Param& param);
	static const Value __FindByIdentifier(const Param& param);
	static const Value __MyGetStatus_FE(const Param& param);
	static const Value __MySyncStatus_FE(const Param& param);
	static const Value __Download_FE(const Param& param);
	static const Value __MyOrderListGet_FE(const Param& param);
	static const Value __MyDownload_FE(const Param& param);
	static const Value __PromotionSaleListGet(const Param& param);
	static const Value __ParkingOrderPlace_API(const Param& param);
	static const Value __ParkingIdentifierInfoGet_API(const Param& param);

	// Wrappers for automatic methods declaration
	static const Value __Update(const Param& param);
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);
	static const Value __PersonOrderListGet(const Param& param);

	// Manual methods declaration
	static const Value Place_API(const Str email, const Str last_name, const Str first_name, const ADate adate, const Str identifier_service_rulename, const Int qty);
	static const Value PlaceFree(const Str email, const Str last_name, const Str first_name, const ADate adate, const Int week_tariffid, const Int qty);
	static const Value MyPlace_API(const ADate adate, const Str identifier_service_rulename, const Int qty);
	static const Value Place_FE(const Str email, const Str last_name, const Str first_name, const ADate adate, const Int week_tariffid, const Int qty);
	static const Value PlaceAny_FE(const Str email, const ADate adate, const Str orderdesc);
	static const Value PlaceAmount_FE(const Str email, const ADate adate, const Str timeoffset, const Str orderdesc);
	static const Value Place_WEB(const Str email, const Str last_name, const Str first_name, const ADate adate, const Int week_tariffid, const Int qty, const Str promocode, const Str sitecode);
	static const Value CheckPromocode_API(const Str promocode, const ADate adate);
	static const Value BuySkipass_API(const Str email, const Str last_name, const Str first_name, const Int skipass_configurationid, const Int qty);
	static const Value MyBuySkipass_API(const Int skipass_configurationid, const Int qty);
	static const Value Add(const Int personid, const ADate adate, const Str identifier_service_rulename, const Str identifier_ratename, const Int qty);
	static const Value RequestPaymentStatus();
	static const Value GetPreliminaryOrderAmount_FE(const ADate adate, const Str identifier_rulename, const Str service_rulename, const Int qty);
	static const Value FindByIdentifier(const Str code);
	static const Value MyGetStatus_FE(const Int id);
	static const Value MySyncStatus_FE(const Int id);
	static const Value Download_FE(const Str trxid);
	static const Value MyOrderListGet_FE();
	static const Value MyDownload_FE(const Int id);
	static const Value PromotionSaleListGet(const ADate first_day, const ADate last_day);
	static const Value ParkingOrderPlace_API(const Int areaid, const Str email, const Str code);
	static const Value ParkingIdentifierInfoGet_API(const Int areaid, const Str code);

	// Automatic methods declaration
	static const Value Update(const Int id, const optional<Int> personid, const optional<Time> filed_at, const optional<Decimal> amount, const optional<Str> status, const optional<Str> promocode, const optional<Decimal> final_amount, const optional<Str> sitecode);
	static const Value Delete(const Int id);
	static const Value Get(const Int id);
	static const Value PersonOrderListGet(const optional<Int> personid, const optional<Str> sitecode);
private: //prohibited operations
	BALOONPersonOrder(const BALOONPersonOrder&);
	BALOONPersonOrder& operator=(const BALOONPersonOrder&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONParkingArea : public ApplicationDBService {
public:
	BALOONParkingArea(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONParkingArea(){}
public:
	// Wrappers for manual methods
	static const Value __ParkingAreaListGet_API(const Param& param);

	// Wrappers for automatic methods declaration
	static const Value __Add(const Param& param);
	static const Value __Update(const Param& param);
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);
	static const Value __ParkingAreaListGet(const Param& param);

	// Manual methods declaration
	static const Value ParkingAreaListGet_API();

	// Automatic methods declaration
	static const Value Add(const Str name, const Int spdgateid, const Int enter_timeout, const Int exit_timeout);
	static const Value Update(const Int id, const optional<Str> name, const optional<Int> spdgateid, const optional<Int> enter_timeout, const optional<Int> exit_timeout);
	static const Value Delete(const Int id);
	static const Value Get(const Int id);
	static const Value ParkingAreaListGet(const optional<Int> spdgateid);
private: //prohibited operations
	BALOONParkingArea(const BALOONParkingArea&);
	BALOONParkingArea& operator=(const BALOONParkingArea&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONParkingTimer : public ApplicationDBService {
public:
	BALOONParkingTimer(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONParkingTimer(){}
public:
	// Wrappers for manual methods
	static const Value __TimeLeftGet_API(const Param& param);
	static const Value __Start_API(const Param& param);

	// Wrappers for automatic methods declaration
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);

	// Manual methods declaration
	static const Value TimeLeftGet_API(const Str code);
	static const Value Start_API(const Str code, const Str secretkey);

	// Automatic methods declaration
	static const Value Delete(const Int orderid);
	static const Value Get(const Int orderid);
private: //prohibited operations
	BALOONParkingTimer(const BALOONParkingTimer&);
	BALOONParkingTimer& operator=(const BALOONParkingTimer&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONParkingOrderline : public ApplicationDBService {
public:
	BALOONParkingOrderline(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONParkingOrderline(){}
public:
	// Wrappers for manual methods

	// Wrappers for automatic methods declaration
	static const Value __Add(const Param& param);
	static const Value __Update(const Param& param);
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);
	static const Value __ParkingOrderlineListGet(const Param& param);

	// Manual methods declaration

	// Automatic methods declaration
	static const Value Add(const Int orderid, const Int identifierid, const Time time_from, const Time time_upto, const Decimal price, const Int areaid);
	static const Value Update(const Int id, const optional<Int> orderid, const optional<Int> identifierid, const optional<Time> time_from, const optional<Time> time_upto, const optional<Decimal> price, const optional<Int> areaid);
	static const Value Delete(const Int id);
	static const Value Get(const Int id);
	static const Value ParkingOrderlineListGet(const optional<Int> orderid, const optional<Int> identifierid, const optional<Int> areaid);
private: //prohibited operations
	BALOONParkingOrderline(const BALOONParkingOrderline&);
	BALOONParkingOrderline& operator=(const BALOONParkingOrderline&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONPersonOrderFiscalDoc : public ApplicationDBService {
public:
	BALOONPersonOrderFiscalDoc(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONPersonOrderFiscalDoc(){}
public:
	// Wrappers for manual methods

	// Wrappers for automatic methods declaration
	static const Value __Add(const Param& param);
	static const Value __Update(const Param& param);
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);
	static const Value __PersonOrderFiscalDocListGet(const Param& param);

	// Manual methods declaration

	// Automatic methods declaration
	static const Value Add(const Int orderid, const Int fiscal_docid);
	static const Value Update(const Int orderid, const Int fiscal_docid);
	static const Value Delete(const Int orderid, const Int fiscal_docid);
	static const Value Get(const Int orderid, const Int fiscal_docid);
	static const Value PersonOrderFiscalDocListGet(const optional<Int> orderid, const optional<Int> fiscal_docid);
private: //prohibited operations
	BALOONPersonOrderFiscalDoc(const BALOONPersonOrderFiscalDoc&);
	BALOONPersonOrderFiscalDoc& operator=(const BALOONPersonOrderFiscalDoc&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONPersonIdentifierOrderline : public ApplicationDBService {
public:
	BALOONPersonIdentifierOrderline(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONPersonIdentifierOrderline(){}
public:
	// Wrappers for manual methods
	static const Value __PersonIdentifierOrderlineListGet(const Param& param);
	static const Value __PersonIdentifierOrderlineListGet_FE(const Param& param);
	static const Value __MyOrderlineListGet_FE(const Param& param);

	// Wrappers for automatic methods declaration
	static const Value __Add(const Param& param);
	static const Value __Update(const Param& param);
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);

	// Manual methods declaration
	static const Value PersonIdentifierOrderlineListGet(const optional<Int> orderid, const optional<Int> identifierid);
	static const Value PersonIdentifierOrderlineListGet_FE(const Int orderid);
	static const Value MyOrderlineListGet_FE(const Int orderid);

	// Automatic methods declaration
	static const Value Add(const Int orderid, const Int identifierid, const Decimal price, const Str service_rulename, const Str make_transaction);
	static const Value Update(const Int orderid, const Int identifierid, const optional<Decimal> price, const optional<Str> service_rulename, const optional<Str> make_transaction);
	static const Value Delete(const Int orderid, const Int identifierid);
	static const Value Get(const Int orderid, const Int identifierid);
private: //prohibited operations
	BALOONPersonIdentifierOrderline(const BALOONPersonIdentifierOrderline&);
	BALOONPersonIdentifierOrderline& operator=(const BALOONPersonIdentifierOrderline&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONAgentIdentifierOrderline : public ApplicationDBService {
public:
	BALOONAgentIdentifierOrderline(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONAgentIdentifierOrderline(){}
public:
	// Wrappers for manual methods

	// Wrappers for automatic methods declaration
	static const Value __Add(const Param& param);
	static const Value __Update(const Param& param);
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);
	static const Value __AgentIdentifierOrderlineListGet(const Param& param);

	// Manual methods declaration

	// Automatic methods declaration
	static const Value Add(const Int orderid, const Int identifierid, const Decimal price);
	static const Value Update(const Int orderid, const Int identifierid, const optional<Decimal> price);
	static const Value Delete(const Int orderid, const Int identifierid);
	static const Value Get(const Int orderid, const Int identifierid);
	static const Value AgentIdentifierOrderlineListGet(const optional<Int> orderid, const optional<Int> identifierid);
private: //prohibited operations
	BALOONAgentIdentifierOrderline(const BALOONAgentIdentifierOrderline&);
	BALOONAgentIdentifierOrderline& operator=(const BALOONAgentIdentifierOrderline&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONPersonInvoice : public ApplicationDBService {
public:
	BALOONPersonInvoice(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONPersonInvoice(){}
public:
	// Wrappers for manual methods
	static const Value __RaiseInvoice_FE(const Param& param);
	static const Value __StatusUpdated(const Param& param);

	// Wrappers for automatic methods declaration
	static const Value __Add(const Param& param);
	static const Value __Update(const Param& param);
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);
	static const Value __PersonInvoiceListGet(const Param& param);

	// Manual methods declaration
	static const Value RaiseInvoice_FE(const Int orderid, const Decimal amount, const Str success_url, const Str fail_url);
	static const Value StatusUpdated(const Int id, const Str barcode, const Str status, const Decimal amount);

	// Automatic methods declaration
	static const Value Add(const Int id, const Int orderid);
	static const Value Update(const Int id, const optional<Int> orderid);
	static const Value Delete(const Int id);
	static const Value Get(const Int id);
	static const Value PersonInvoiceListGet(const optional<Int> id, const optional<Int> orderid);
private: //prohibited operations
	BALOONPersonInvoice(const BALOONPersonInvoice&);
	BALOONPersonInvoice& operator=(const BALOONPersonInvoice&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONIdentifierInvoice : public ApplicationDBService {
public:
	BALOONIdentifierInvoice(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONIdentifierInvoice(){}
public:
	// Wrappers for manual methods
	static const Value __SyncInvoice(const Param& param);
	static const Value __StatusUpdated(const Param& param);
	static const Value __RaiseInvoice(const Param& param);
	static const Value __RaiseInvoiceForCurrentUser(const Param& param);
	static const Value __RaiseInvoice_FE(const Param& param);
	static const Value __CurrentInvoicesGetList_FE(const Param& param);

	// Wrappers for automatic methods declaration
	static const Value __Add(const Param& param);
	static const Value __Update(const Param& param);
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);
	static const Value __IdentifierInvoiceListGet(const Param& param);

	// Manual methods declaration
	static const Value SyncInvoice(const Int id);
	static const Value StatusUpdated(const Int id, const Str barcode, const Str status, const Decimal amount);
	static const Value RaiseInvoice(const Int paygateid, const Int identifierid, const Decimal amount, const Str success_url, const Str fail_url);
	static const Value RaiseInvoiceForCurrentUser(const Decimal amount, const Str success_url, const Str fail_url);
	static const Value RaiseInvoice_FE(const Str code, const Decimal amount, const Str success_url, const Str fail_url);
	static const Value CurrentInvoicesGetList_FE();

	// Automatic methods declaration
	static const Value Add(const Int id, const Int identifierid, const Int fiscal_docid, const Str eid);
	static const Value Update(const Int id, const optional<Int> identifierid, const optional<Int> fiscal_docid, const optional<Str> eid);
	static const Value Delete(const Int id);
	static const Value Get(const Int id);
	static const Value IdentifierInvoiceListGet(const optional<Int> id, const optional<Int> identifierid, const optional<Int> fiscal_docid);
private: //prohibited operations
	BALOONIdentifierInvoice(const BALOONIdentifierInvoice&);
	BALOONIdentifierInvoice& operator=(const BALOONIdentifierInvoice&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONBaloonProvider : public ApplicationDBService {
public:
	BALOONBaloonProvider(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONBaloonProvider(){}
public:
	// Wrappers for manual methods
	static const Value __IdentifierPriceGet(const Param& param);

	// Wrappers for automatic methods declaration
	static const Value __Add(const Param& param);
	static const Value __Update(const Param& param);
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);

	// Manual methods declaration
	static const Value IdentifierPriceGet();

	// Automatic methods declaration
	static const Value Add(const Int default_paygateid, const Int default_fiscalgateid, const Str code_conversion_algo, const Double agent_commission, const Decimal ident_sale_price, const Int timeout_before, const Str sun, const Str mon, const Str tue, const Str wed, const Str thu, const Str fri, const Str sat);
	static const Value Update(const optional<Int> default_paygateid, const optional<Int> default_fiscalgateid, const optional<Str> code_conversion_algo, const optional<Double> agent_commission, const optional<Decimal> ident_sale_price, const optional<Int> timeout_before, const optional<Str> sun, const optional<Str> mon, const optional<Str> tue, const optional<Str> wed, const optional<Str> thu, const optional<Str> fri, const optional<Str> sat);
	static const Value Delete();
	static const Value Get();
private: //prohibited operations
	BALOONBaloonProvider(const BALOONBaloonProvider&);
	BALOONBaloonProvider& operator=(const BALOONBaloonProvider&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONExternalResource : public ApplicationDBService {
public:
	BALOONExternalResource(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONExternalResource(){}
public:
	// Wrappers for manual methods
	static const Value __ExternalResourceListGet_API(const Param& param);

	// Wrappers for automatic methods declaration
	static const Value __Add(const Param& param);
	static const Value __Update(const Param& param);
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);
	static const Value __ExternalResourceListGet(const Param& param);

	// Manual methods declaration
	static const Value ExternalResourceListGet_API();

	// Automatic methods declaration
	static const Value Add(const Str name, const Str uri, const Str description, const Str enabled);
	static const Value Update(const Int id, const optional<Str> name, const optional<Str> uri, const optional<Str> description, const optional<Str> enabled);
	static const Value Delete(const Int id);
	static const Value Get(const Int id);
	static const Value ExternalResourceListGet();
private: //prohibited operations
	BALOONExternalResource(const BALOONExternalResource&);
	BALOONExternalResource& operator=(const BALOONExternalResource&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONAgent : public ApplicationDBService {
public:
	BALOONAgent(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONAgent(){}
public:
	// Wrappers for manual methods
	static const Value __Register_API(const Param& param);
	static const Value __Update(const Param& param);
	static const Value __GetCurrent(const Param& param);

	// Wrappers for automatic methods declaration
	static const Value __Add(const Param& param);
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);
	static const Value __AgentListGet(const Param& param);

	// Manual methods declaration
	static const Value Register_API(const Str name, const Str phone, const Str email, const Str password, const Str password2);
	static const Value Update(const Int id, const optional<Str> name, const optional<Str> phone, const optional<Str> email, const optional<Str> comments, const optional<Double> commission, const optional<Str> is_default);
	static const Value GetCurrent();

	// Automatic methods declaration
	static const Value Add(const Int64 userTokenID, const Str name, const Str phone, const Str email, const Str comments, const Double commission, const Str is_default);
	static const Value Delete(const Int id);
	static const Value Get(const Int id);
	static const Value AgentListGet(const optional<Int64> userTokenID);
private: //prohibited operations
	BALOONAgent(const BALOONAgent&);
	BALOONAgent& operator=(const BALOONAgent&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONAgentOrder : public ApplicationDBService {
public:
	BALOONAgentOrder(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONAgentOrder(){}
public:
	// Wrappers for manual methods
	static const Value __AgentOrderListGet(const Param& param);
	static const Value __Add(const Param& param);
	static const Value __Update(const Param& param);
	static const Value __CurrentAgentOrderListGet(const Param& param);
	static const Value __CurrentAgentOrderListGet_API(const Param& param);
	static const Value __CurrentGet(const Param& param);
	static const Value __CurrentGet_API(const Param& param);
	static const Value __CurrentSyncAndGet_API(const Param& param);
	static const Value __CurrentAdd(const Param& param);
	static const Value __CurrentAdd_API(const Param& param);
	static const Value __OnlineCurrentAdd_API(const Param& param);
	static const Value __CurrentUpdate(const Param& param);
	static const Value __GetType(const Param& param);

	// Wrappers for automatic methods declaration
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);

	// Manual methods declaration
	static const Value AgentOrderListGet(const optional<Int> agentid, const optional<ADate> first_day, const optional<ADate> last_day);
	static const Value Add(const Int agentid, const Str code, const Decimal amount, const Str comment);
	static const Value Update(const Int id, const optional<Str> comment);
	static const Value CurrentAgentOrderListGet();
	static const Value CurrentAgentOrderListGet_API(const optional<Int> id);
	static const Value CurrentGet(const Int id);
	static const Value CurrentGet_API(const Int id);
	static const Value CurrentSyncAndGet_API(const Int id);
	static const Value CurrentAdd(const Str code, const Decimal amount, const Str comment);
	static const Value CurrentAdd_API(const Str code, const Decimal amount, const Str comment);
	static const Value OnlineCurrentAdd_API(const Str code, const Decimal amount, const Str comment, const Str success_url, const Str fail_url);
	static const Value CurrentUpdate(const Int id, const optional<Str> comment);
	static const Value GetType(const Int id);

	// Automatic methods declaration
	static const Value Delete(const Int id);
	static const Value Get(const Int id);
private: //prohibited operations
	BALOONAgentOrder(const BALOONAgentOrder&);
	BALOONAgentOrder& operator=(const BALOONAgentOrder&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONIdentifierAgentOrder : public ApplicationDBService {
public:
	BALOONIdentifierAgentOrder(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONIdentifierAgentOrder(){}
public:
	// Wrappers for manual methods
	static const Value __Add(const Param& param);
	static const Value __Update(const Param& param);
	static const Value __CurrentGet(const Param& param);
	static const Value __CurrentAdd(const Param& param);
	static const Value __CurrentAdd_API(const Param& param);
	static const Value __CurrentUpdate(const Param& param);

	// Wrappers for automatic methods declaration
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);
	static const Value __IdentifierAgentOrderListGet(const Param& param);

	// Manual methods declaration
	static const Value Add(const Int agentid, const Str code, const Decimal amount, const Str comment, const Time valid_from, const Time valid_to, const Str permanent_rulename, const Str categoryname, const Str ratename, const Str other_card_code);
	static const Value Update(const Int id, const optional<Str> comment);
	static const Value CurrentGet(const Int id);
	static const Value CurrentAdd(const Str code, const Decimal amount, const Str comment, const Time valid_from, const Time valid_to, const Str permanent_rulename, const Str categoryname, const Str ratename, const Str other_card_code);
	static const Value CurrentAdd_API(const Str code, const Decimal amount, const Str comment, const Time valid_from, const Time valid_to, const Str permanent_rulename, const Str categoryname, const Str ratename, const Str other_card_code);
	static const Value CurrentUpdate(const Int id, const optional<Str> comment);

	// Automatic methods declaration
	static const Value Delete(const Int id);
	static const Value Get(const Int id);
	static const Value IdentifierAgentOrderListGet(const optional<Str> permanent_rulename, const optional<Str> categoryname, const optional<Str> ratename);
private: //prohibited operations
	BALOONIdentifierAgentOrder(const BALOONIdentifierAgentOrder&);
	BALOONIdentifierAgentOrder& operator=(const BALOONIdentifierAgentOrder&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONPackageAgentOrder : public ApplicationDBService {
public:
	BALOONPackageAgentOrder(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONPackageAgentOrder(){}
public:
	// Wrappers for manual methods
	static const Value __Add(const Param& param);
	static const Value __Update(const Param& param);
	static const Value __CurrentGet(const Param& param);
	static const Value __CurrentAdd(const Param& param);
	static const Value __CurrentAdd_API(const Param& param);
	static const Value __OnlineCurrentAdd_API(const Param& param);
	static const Value __CurrentUpdate(const Param& param);

	// Wrappers for automatic methods declaration
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);
	static const Value __PackageAgentOrderListGet(const Param& param);

	// Manual methods declaration
	static const Value Add(const Int agentid, const Str code, const Str service_rulename, const Str comment);
	static const Value Update(const Int id, const optional<Str> comment);
	static const Value CurrentGet(const Int id);
	static const Value CurrentAdd(const Str code, const Str service_rulename, const Str comment);
	static const Value CurrentAdd_API(const Str code, const Str service_rulename, const Str comment);
	static const Value OnlineCurrentAdd_API(const Str code, const Str service_rulename, const Str comment, const Str success_url, const Str fail_url);
	static const Value CurrentUpdate(const Int id, const optional<Str> comment);

	// Automatic methods declaration
	static const Value Delete(const Int id);
	static const Value Get(const Int id);
	static const Value PackageAgentOrderListGet(const optional<Str> service_rulename);
private: //prohibited operations
	BALOONPackageAgentOrder(const BALOONPackageAgentOrder&);
	BALOONPackageAgentOrder& operator=(const BALOONPackageAgentOrder&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONAgentInvoice : public ApplicationDBService {
public:
	BALOONAgentInvoice(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONAgentInvoice(){}
public:
	// Wrappers for manual methods
	static const Value __SyncSpd(const Param& param);
	static const Value __StatusUpdated(const Param& param);
	static const Value __RaiseInvoice(const Param& param);

	// Wrappers for automatic methods declaration
	static const Value __Add(const Param& param);
	static const Value __Update(const Param& param);
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);
	static const Value __AgentInvoiceListGet(const Param& param);

	// Manual methods declaration
	static const Value SyncSpd(const Int id);
	static const Value StatusUpdated(const Int id, const Str barcode, const Str status, const Decimal amount);
	static const Value RaiseInvoice(const Int paygateid, const Int orderid, const Decimal amount, const Str success_url, const Str fail_url);

	// Automatic methods declaration
	static const Value Add(const Int id, const Int orderid, const Str eid);
	static const Value Update(const Int id, const optional<Int> orderid, const optional<Str> eid);
	static const Value Delete(const Int id);
	static const Value Get(const Int id);
	static const Value AgentInvoiceListGet(const optional<Int> id, const optional<Int> orderid);
private: //prohibited operations
	BALOONAgentInvoice(const BALOONAgentInvoice&);
	BALOONAgentInvoice& operator=(const BALOONAgentInvoice&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONReports : public ApplicationDBService {
public:
	BALOONReports(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONReports(){}
public:
	// Wrappers for manual methods
	static const Value __AgentSalesReportListGet(const Param& param);
	static const Value __CurrentMonth(const Param& param);
	static const Value __PreviousMonth(const Param& param);
	static const Value __CurrentDate(const Param& param);
	static const Value __CurrentAgentSalesReportListGet(const Param& param);
	static const Value __PersonSalesReportListGet(const Param& param);
	static const Value __IdentifierReportListGet(const Param& param);

	// Wrappers for automatic methods declaration

	// Manual methods declaration
	static const Value AgentSalesReportListGet(const ADate first_day, const ADate last_day, const Int agentid);
	static const Value CurrentMonth();
	static const Value PreviousMonth();
	static const Value CurrentDate();
	static const Value CurrentAgentSalesReportListGet(const ADate first_day, const ADate last_day);
	static const Value PersonSalesReportListGet(const ADate first_day, const ADate last_day);
	static const Value IdentifierReportListGet(const ADate current_date);

	// Automatic methods declaration
private: //prohibited operations
	BALOONReports(const BALOONReports&);
	BALOONReports& operator=(const BALOONReports&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONAccount : public ApplicationDBService {
public:
	BALOONAccount(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONAccount(){}
public:
	// Wrappers for manual methods
	static const Value __AccountListGet(const Param& param);

	// Wrappers for automatic methods declaration
	static const Value __Add(const Param& param);
	static const Value __Update(const Param& param);
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);

	// Manual methods declaration
	static const Value AccountListGet(const Int personid);

	// Automatic methods declaration
	static const Value Add(const Str currency, const Int personid, const Str valid, const Decimal balance);
	static const Value Update(const Str currency, const Int personid, const optional<Str> valid, const optional<Decimal> balance);
	static const Value Delete(const Str currency, const Int personid);
	static const Value Get(const Str currency, const Int personid);
private: //prohibited operations
	BALOONAccount(const BALOONAccount&);
	BALOONAccount& operator=(const BALOONAccount&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONPackageOrder : public ApplicationDBService {
public:
	BALOONPackageOrder(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONPackageOrder(){}
public:
	// Wrappers for manual methods
	static const Value __MyPackageListGet_API(const Param& param);
	static const Value __Add(const Param& param);

	// Wrappers for automatic methods declaration
	static const Value __Update(const Param& param);
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);
	static const Value __PackageOrderListGet(const Param& param);

	// Manual methods declaration
	static const Value MyPackageListGet_API(const Str code);
	static const Value Add(const Int identifierid, const Str service_rulename);

	// Automatic methods declaration
	static const Value Update(const Int id, const optional<Int> identifierid, const optional<Str> service_rulename, const optional<Time> filed_at);
	static const Value Delete(const Int id);
	static const Value Get(const Int id);
	static const Value PackageOrderListGet(const optional<Int> identifierid, const optional<Str> service_rulename);
private: //prohibited operations
	BALOONPackageOrder(const BALOONPackageOrder&);
	BALOONPackageOrder& operator=(const BALOONPackageOrder&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONTransaction : public ApplicationDBService {
public:
	BALOONTransaction(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONTransaction(){}
public:
	// Wrappers for manual methods
	static const Value __TransactionListGet(const Param& param);
	static const Value __CurrentTransactionListGet_FE(const Param& param);
	static const Value __IdentifierTransactionListGet_FE(const Param& param);
	static const Value __SaleIdentifier_API(const Param& param);
	static const Value __SalePackage_API(const Param& param);
	static const Value __MassSalePackages_FE(const Param& param);
	static const Value __FillupAccount_API(const Param& param);

	// Wrappers for automatic methods declaration
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);

	// Manual methods declaration
	static const Value TransactionListGet(const Int identifierid);
	static const Value CurrentTransactionListGet_FE();
	static const Value IdentifierTransactionListGet_FE(const Str code);
	static const Value SaleIdentifier_API(const Str code, const Str print_code, const Time valid_from, const Time valid_to, const Str permanent_rulename, const Str categoryname, const Str ratename, const Str currency, const Decimal amount);
	static const Value SalePackage_API(const Str code, const Str print_code, const Str categoryname);
	static const Value MassSalePackages_FE(const Str code, const Str print_code, const Value categories);
	static const Value FillupAccount_API(const Str code, const Str print_code, const Str currency, const Decimal amount);

	// Automatic methods declaration
	static const Value Delete(const Int id);
	static const Value Get(const Int id);
private: //prohibited operations
	BALOONTransaction(const BALOONTransaction&);
	BALOONTransaction& operator=(const BALOONTransaction&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONAdvertisement : public ApplicationDBService {
public:
	BALOONAdvertisement(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONAdvertisement(){}
public:
	// Wrappers for manual methods
	static const Value __NewsArticleListGet(const Param& param);
	static const Value __LatestArticleListGet_FE(const Param& param);
	static const Value __VisualItemListGet_FE(const Param& param);
	static const Value __VisualItemListGet_FE_1_13(const Param& param);
	static const Value __NewsArticleAdd(const Param& param);
	static const Value __NewsArticleGet(const Param& param);
	static const Value __NewsArticleUpdate(const Param& param);
	static const Value __Delete(const Param& param);
	static const Value __VisualItemListGet(const Param& param);
	static const Value __VisualItemGet(const Param& param);
	static const Value __VisualItemAdd(const Param& param);
	static const Value __VisualItemUpdate(const Param& param);

	// Wrappers for automatic methods declaration

	// Manual methods declaration
	static const Value NewsArticleListGet();
	static const Value LatestArticleListGet_FE(const Int items_qty);
	static const Value VisualItemListGet_FE(const Int items_qty);
	static const Value VisualItemListGet_FE_1_13(const Int items_qty);
	static const Value NewsArticleAdd(const Str title, const Str text_content, const Int priority);
	static const Value NewsArticleGet(const Int id);
	static const Value NewsArticleUpdate(const Int id, const optional<Str> title, const optional<Str> text_content, const optional<Int> priority);
	static const Value Delete(const Int id);
	static const Value VisualItemListGet();
	static const Value VisualItemGet(const Int id);
	static const Value VisualItemAdd(const Str title, const Str html_link, const optional<BlobFile> image, const Str widget, const Int priority);
	static const Value VisualItemUpdate(const Int id, const optional<Str> title, const optional<Str> html_link, const optional<BlobFile> image, const optional<Str> widget, const optional<Int> priority);

	// Automatic methods declaration
private: //prohibited operations
	BALOONAdvertisement(const BALOONAdvertisement&);
	BALOONAdvertisement& operator=(const BALOONAdvertisement&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONInfoPlacement : public ApplicationDBService {
public:
	BALOONInfoPlacement(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONInfoPlacement(){}
public:
	// Wrappers for manual methods

	// Wrappers for automatic methods declaration
	static const Value __Add(const Param& param);
	static const Value __Update(const Param& param);
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);
	static const Value __InfoPlacementListGet(const Param& param);

	// Manual methods declaration

	// Automatic methods declaration
	static const Value Add(const Int id, const Str widget);
	static const Value Update(const Int id, const optional<Str> widget);
	static const Value Delete(const Int id);
	static const Value Get(const Int id);
	static const Value InfoPlacementListGet(const optional<Int> id);
private: //prohibited operations
	BALOONInfoPlacement(const BALOONInfoPlacement&);
	BALOONInfoPlacement& operator=(const BALOONInfoPlacement&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONHoliday : public ApplicationDBService {
public:
	BALOONHoliday(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONHoliday(){}
public:
	// Wrappers for manual methods
	static const Value __MassAdd(const Param& param);
	static const Value __HolidaysFromDateListGet_FE(const Param& param);

	// Wrappers for automatic methods declaration
	static const Value __Add(const Param& param);
	static const Value __Update(const Param& param);
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);
	static const Value __HolidayListGet(const Param& param);

	// Manual methods declaration
	static const Value MassAdd(const Value holidays);
	static const Value HolidaysFromDateListGet_FE(const ADate adate);

	// Automatic methods declaration
	static const Value Add(const ADate adate, const Str out_of_service, const Str comment);
	static const Value Update(const ADate adate, const optional<Str> out_of_service, const optional<Str> comment);
	static const Value Delete(const ADate adate);
	static const Value Get(const ADate adate);
	static const Value HolidayListGet();
private: //prohibited operations
	BALOONHoliday(const BALOONHoliday&);
	BALOONHoliday& operator=(const BALOONHoliday&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONWeekTariffGroup : public ApplicationDBService {
public:
	BALOONWeekTariffGroup(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONWeekTariffGroup(){}
public:
	// Wrappers for manual methods
	static const Value __PartnerWeekTariffGroupListGet(const Param& param);
	static const Value __Add(const Param& param);
	static const Value __Update(const Param& param);
	static const Value __ForPartnerAdd(const Param& param);
	static const Value __ForPartnerUpdate(const Param& param);
	static const Value __ForPartnerGet(const Param& param);
	static const Value __ForPartnerDelete(const Param& param);

	// Wrappers for automatic methods declaration
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);
	static const Value __WeekTariffGroupListGet(const Param& param);

	// Manual methods declaration
	static const Value PartnerWeekTariffGroupListGet();
	static const Value Add(const Str name, const Str description, const Str is_default);
	static const Value Update(const Int id, const optional<Str> name, const optional<Str> description, const optional<Str> is_default);
	static const Value ForPartnerAdd(const Int partnerid, const Str name, const Str description);
	static const Value ForPartnerUpdate(const Int id, const optional<Str> name, const optional<Str> description);
	static const Value ForPartnerGet(const Int id);
	static const Value ForPartnerDelete(const Int id);

	// Automatic methods declaration
	static const Value Delete(const Int id);
	static const Value Get(const Int id);
	static const Value WeekTariffGroupListGet();
private: //prohibited operations
	BALOONWeekTariffGroup(const BALOONWeekTariffGroup&);
	BALOONWeekTariffGroup& operator=(const BALOONWeekTariffGroup&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONWeekTariff : public ApplicationDBService {
public:
	BALOONWeekTariff(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONWeekTariff(){}
public:
	// Wrappers for manual methods
	static const Value __WeekTariffListGet(const Param& param);
	static const Value __PartnerWeekTariffListGet(const Param& param);
	static const Value __MassAdd(const Param& param);
	static const Value __AsyncRestoreCounters(const Param& param);
	static const Value __SeansesAllByDateListGet_FE(const Param& param);
	static const Value __SeansesListGet_FE(const Param& param);
	static const Value __SeansesByDateListGet_FE(const Param& param);
	static const Value __SeansesByDateListGet(const Param& param);
	static const Value __ForPartnerDelete(const Param& param);
	static const Value __OurSeansesByDateListGet(const Param& param);
	static const Value __OurSeansesByDateListGet_API(const Param& param);

	// Wrappers for automatic methods declaration
	static const Value __Add(const Param& param);
	static const Value __Update(const Param& param);
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);

	// Manual methods declaration
	static const Value WeekTariffListGet(const optional<Int> agroupid);
	static const Value PartnerWeekTariffListGet(const optional<Int> agroupid);
	static const Value MassAdd(const Int agroupid, const Value week_days, const Value tariffs);
	static const Value AsyncRestoreCounters();
	static const Value SeansesAllByDateListGet_FE(const ADate adate, const Str name, const Str is_default);
	static const Value SeansesListGet_FE(const ADate adate, const Str identifier_rulename, const Str identifier_categoryname, const Str ratename);
	static const Value SeansesByDateListGet_FE(const ADate adate);
	static const Value SeansesByDateListGet(const ADate adate);
	static const Value ForPartnerDelete(const Int id);
	static const Value OurSeansesByDateListGet(const ADate adate);
	static const Value OurSeansesByDateListGet_API(const ADate adate, const Str token);

	// Automatic methods declaration
	static const Value Add(const Int agroupid, const Str week_day, const ATime time_from, const ATime time_upto, const Decimal price, const Str pricetype, const Int free, const Int total, const Str counttype, const Str external_name, const Str identifier_rulename, const Str service_rulename, const Str identifier_categoryname, const Str ratename, const Str enabled);
	static const Value Update(const Int id, const optional<Int> agroupid, const optional<Str> week_day, const optional<ATime> time_from, const optional<ATime> time_upto, const optional<Decimal> price, const optional<Str> pricetype, const optional<Int> free, const optional<Int> total, const optional<Str> counttype, const optional<Str> external_name, const optional<Str> identifier_rulename, const optional<Str> service_rulename, const optional<Str> identifier_categoryname, const optional<Str> ratename, const optional<Str> enabled);
	static const Value Delete(const Int id);
	static const Value Get(const Int id);
private: //prohibited operations
	BALOONWeekTariff(const BALOONWeekTariff&);
	BALOONWeekTariff& operator=(const BALOONWeekTariff&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONSkipassConfiguration : public ApplicationDBService {
public:
	BALOONSkipassConfiguration(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONSkipassConfiguration(){}
public:
	// Wrappers for manual methods
	static const Value __MassAdd(const Param& param);
	static const Value __SkipassConfigurationListGet_API(const Param& param);
	static const Value __SkipassConfigurationGroupNameListGet_API(const Param& param);

	// Wrappers for automatic methods declaration
	static const Value __Add(const Param& param);
	static const Value __Update(const Param& param);
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);
	static const Value __SkipassConfigurationListGet(const Param& param);

	// Manual methods declaration
	static const Value MassAdd(const Str groupname, const Value configurations);
	static const Value SkipassConfigurationListGet_API(const Str groupname);
	static const Value SkipassConfigurationGroupNameListGet_API();

	// Automatic methods declaration
	static const Value Add(const Str groupname, const Str name, const Str identifier_rulename, const Str service_rulename, const Str identifier_categoryname, const Str ratename, const Time valid_from, const Time valid_to, const Decimal price, const Str make_transaction, const Str enabled);
	static const Value Update(const Int id, const optional<Str> groupname, const optional<Str> name, const optional<Str> identifier_rulename, const optional<Str> service_rulename, const optional<Str> identifier_categoryname, const optional<Str> ratename, const optional<Time> valid_from, const optional<Time> valid_to, const optional<Decimal> price, const optional<Str> make_transaction, const optional<Str> enabled);
	static const Value Delete(const Int id);
	static const Value Get(const Int id);
	static const Value SkipassConfigurationListGet(const optional<Str> identifier_rulename, const optional<Str> service_rulename, const optional<Str> identifier_categoryname, const optional<Str> ratename);
private: //prohibited operations
	BALOONSkipassConfiguration(const BALOONSkipassConfiguration&);
	BALOONSkipassConfiguration& operator=(const BALOONSkipassConfiguration&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONSpdGate : public ApplicationDBService {
public:
	BALOONSpdGate(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONSpdGate(){}
public:
	// Wrappers for manual methods

	// Wrappers for automatic methods declaration
	static const Value __Add(const Param& param);
	static const Value __Update(const Param& param);
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);
	static const Value __SpdGateListGet(const Param& param);

	// Manual methods declaration

	// Automatic methods declaration
	static const Value Add(const Str name, const Str host, const Str port, const Str enabled);
	static const Value Update(const Int id, const optional<Str> name, const optional<Str> host, const optional<Str> port, const optional<Str> enabled);
	static const Value Delete(const Int id);
	static const Value Get(const Int id);
	static const Value SpdGateListGet();
private: //prohibited operations
	BALOONSpdGate(const BALOONSpdGate&);
	BALOONSpdGate& operator=(const BALOONSpdGate&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONPaidServiceCategory : public ApplicationDBService {
public:
	BALOONPaidServiceCategory(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONPaidServiceCategory(){}
public:
	// Wrappers for manual methods
	static const Value __MassAdd(const Param& param);
	static const Value __PaidServiceCategoryListGet_FE(const Param& param);

	// Wrappers for automatic methods declaration
	static const Value __Add(const Param& param);
	static const Value __Update(const Param& param);
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);
	static const Value __PaidServiceCategoryListGet(const Param& param);

	// Manual methods declaration
	static const Value MassAdd(const Value names);
	static const Value PaidServiceCategoryListGet_FE();

	// Automatic methods declaration
	static const Value Add(const Str name, const Str status);
	static const Value Update(const Int id, const optional<Str> name, const optional<Str> status);
	static const Value Delete(const Int id);
	static const Value Get(const Int id);
	static const Value PaidServiceCategoryListGet();
private: //prohibited operations
	BALOONPaidServiceCategory(const BALOONPaidServiceCategory&);
	BALOONPaidServiceCategory& operator=(const BALOONPaidServiceCategory&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONPaidServiceTariff : public ApplicationDBService {
public:
	BALOONPaidServiceTariff(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONPaidServiceTariff(){}
public:
	// Wrappers for manual methods
	static const Value __MassAdd(const Param& param);

	// Wrappers for automatic methods declaration
	static const Value __Add(const Param& param);
	static const Value __Update(const Param& param);
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);
	static const Value __PaidServiceTariffListGet(const Param& param);

	// Manual methods declaration
	static const Value MassAdd(const Value names);

	// Automatic methods declaration
	static const Value Add(const Str name, const Str status);
	static const Value Update(const Int id, const optional<Str> name, const optional<Str> status);
	static const Value Delete(const Int id);
	static const Value Get(const Int id);
	static const Value PaidServiceTariffListGet();
private: //prohibited operations
	BALOONPaidServiceTariff(const BALOONPaidServiceTariff&);
	BALOONPaidServiceTariff& operator=(const BALOONPaidServiceTariff&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONPaidServiceHour : public ApplicationDBService {
public:
	BALOONPaidServiceHour(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONPaidServiceHour(){}
public:
	// Wrappers for manual methods
	static const Value __MassAdd(const Param& param);

	// Wrappers for automatic methods declaration
	static const Value __Add(const Param& param);
	static const Value __Update(const Param& param);
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);
	static const Value __PaidServiceHourListGet(const Param& param);

	// Manual methods declaration
	static const Value MassAdd(const Value names);

	// Automatic methods declaration
	static const Value Add(const Str name, const Str status);
	static const Value Update(const Int id, const optional<Str> name, const optional<Str> status);
	static const Value Delete(const Int id);
	static const Value Get(const Int id);
	static const Value PaidServiceHourListGet();
private: //prohibited operations
	BALOONPaidServiceHour(const BALOONPaidServiceHour&);
	BALOONPaidServiceHour& operator=(const BALOONPaidServiceHour&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONPaidService : public ApplicationDBService {
public:
	BALOONPaidService(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONPaidService(){}
public:
	// Wrappers for manual methods
	static const Value __MassAdd(const Param& param);
	static const Value __ImportPricelist(const Param& param);
	static const Value __PaidServiceListGet_FE(const Param& param);
	static const Value __PaidServiceListGet_API(const Param& param);

	// Wrappers for automatic methods declaration
	static const Value __Add(const Param& param);
	static const Value __Update(const Param& param);
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);
	static const Value __PaidServiceListGet(const Param& param);

	// Manual methods declaration
	static const Value MassAdd(const Value services);
	static const Value ImportPricelist(const BlobFile pricelist);
	static const Value PaidServiceListGet_FE(const Int categoryid);
	static const Value PaidServiceListGet_API(const Int categoryid);

	// Automatic methods declaration
	static const Value Add(const Int categoryid, const Str name, const Int tariffid, const Int hourid, const Str description, const Decimal price, const Str status);
	static const Value Update(const Int id, const optional<Int> categoryid, const optional<Str> name, const optional<Int> tariffid, const optional<Int> hourid, const optional<Str> description, const optional<Decimal> price, const optional<Str> status);
	static const Value Delete(const Int id);
	static const Value Get(const Int id);
	static const Value PaidServiceListGet(const optional<Int> categoryid, const optional<Int> tariffid, const optional<Int> hourid);
private: //prohibited operations
	BALOONPaidService(const BALOONPaidService&);
	BALOONPaidService& operator=(const BALOONPaidService&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONPartner : public ApplicationDBService {
public:
	BALOONPartner(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONPartner(){}
public:
	// Wrappers for manual methods
	static const Value __PartnerListGet(const Param& param);
	static const Value __Add(const Param& param);
	static const Value __OurGet(const Param& param);

	// Wrappers for automatic methods declaration
	static const Value __Update(const Param& param);
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);

	// Manual methods declaration
	static const Value PartnerListGet();
	static const Value Add(const Str name, const Str legal_address, const Str post_address, const Str inn, const Str kpp, const Str payment_account, const Str correspondent_account, const Str bank, const Str bik, const Str okpo, const Str okato, const Str okved, const Str ogrn, const Str principal, const Str phone, const Str fax, const Str email, const Str notification_email, const Str contractbarcode, const ADate start_date, const ADate end_date, const Str benefit_type, const Decimal benefit_amount, const Str accountname, const Decimal amount, const Str reason, const Str managerfullname, const Str passport_number, const ADate issue_date, const Str issued_by, const Str department_code, const Str managerphone, const Str manageremail, const Str password, const Str password2);
	static const Value OurGet();

	// Automatic methods declaration
	static const Value Update(const Int id, const optional<Str> name, const optional<Str> status, const optional<Str> legal_address, const optional<Str> post_address, const optional<Str> inn, const optional<Str> kpp, const optional<Str> payment_account, const optional<Str> correspondent_account, const optional<Str> bank, const optional<Str> bik, const optional<Str> okpo, const optional<Str> okato, const optional<Str> okved, const optional<Str> ogrn, const optional<Str> principal, const optional<Str> phone, const optional<Str> fax, const optional<Str> email, const optional<Str> token, const optional<Str> notification_email);
	static const Value Delete(const Int id);
	static const Value Get(const Int id);
private: //prohibited operations
	BALOONPartner(const BALOONPartner&);
	BALOONPartner& operator=(const BALOONPartner&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONPartnerAdministrator : public ApplicationDBService {
public:
	BALOONPartnerAdministrator(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONPartnerAdministrator(){}
public:
	// Wrappers for manual methods
	static const Value __Add(const Param& param);
	static const Value __GetCurrent(const Param& param);

	// Wrappers for automatic methods declaration
	static const Value __Update(const Param& param);
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);
	static const Value __PartnerAdministratorListGet(const Param& param);

	// Manual methods declaration
	static const Value Add(const Str fullname, const Str email, const Str password, const Str password2);
	static const Value GetCurrent();

	// Automatic methods declaration
	static const Value Update(const Int64 auserTokenID, const optional<Str> fullname);
	static const Value Delete(const Int64 auserTokenID);
	static const Value Get(const Int64 auserTokenID);
	static const Value PartnerAdministratorListGet(const optional<Int64> auserTokenID);
private: //prohibited operations
	BALOONPartnerAdministrator(const BALOONPartnerAdministrator&);
	BALOONPartnerAdministrator& operator=(const BALOONPartnerAdministrator&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONPartnerManager : public ApplicationDBService {
public:
	BALOONPartnerManager(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONPartnerManager(){}
public:
	// Wrappers for manual methods
	static const Value __Add(const Param& param);
	static const Value __GetCurrent(const Param& param);
	static const Value __MyGet(const Param& param);
	static const Value __MyUpdate(const Param& param);

	// Wrappers for automatic methods declaration
	static const Value __Update(const Param& param);
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);
	static const Value __PartnerManagerListGet(const Param& param);

	// Manual methods declaration
	static const Value Add(const Int partnerid, const Str fullname, const Str passport_number, const ADate issue_date, const Str issued_by, const Str department_code, const Str phone, const Str email, const Str password, const Str password2);
	static const Value GetCurrent();
	static const Value MyGet();
	static const Value MyUpdate(const optional<Str> passport_number, const optional<ADate> issue_date, const optional<Str> issued_by, const optional<Str> department_code, const optional<Str> phone);

	// Automatic methods declaration
	static const Value Update(const Int64 auserTokenID, const optional<Int> partnerid, const optional<Str> fullname, const optional<Str> passport_number, const optional<ADate> issue_date, const optional<Str> issued_by, const optional<Str> department_code, const optional<Str> phone);
	static const Value Delete(const Int64 auserTokenID);
	static const Value Get(const Int64 auserTokenID);
	static const Value PartnerManagerListGet(const optional<Int64> auserTokenID, const optional<Int> partnerid);
private: //prohibited operations
	BALOONPartnerManager(const BALOONPartnerManager&);
	BALOONPartnerManager& operator=(const BALOONPartnerManager&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONPartnerContract : public ApplicationDBService {
public:
	BALOONPartnerContract(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONPartnerContract(){}
public:
	// Wrappers for manual methods
	static const Value __OurPartnerContractListGet(const Param& param);
	static const Value __OurGet(const Param& param);

	// Wrappers for automatic methods declaration
	static const Value __Add(const Param& param);
	static const Value __Update(const Param& param);
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);
	static const Value __PartnerContractListGet(const Param& param);

	// Manual methods declaration
	static const Value OurPartnerContractListGet();
	static const Value OurGet(const Int id);

	// Automatic methods declaration
	static const Value Add(const Int partnerid, const Str barcode, const ADate start_date, const ADate end_date, const Str benefit_type, const Decimal benefit_amount);
	static const Value Update(const Int id, const optional<Int> partnerid, const optional<Str> barcode, const optional<ADate> start_date, const optional<ADate> end_date, const optional<Str> benefit_type, const optional<Decimal> benefit_amount);
	static const Value Delete(const Int id);
	static const Value Get(const Int id);
	static const Value PartnerContractListGet(const optional<Int> partnerid);
private: //prohibited operations
	BALOONPartnerContract(const BALOONPartnerContract&);
	BALOONPartnerContract& operator=(const BALOONPartnerContract&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONPartnerAccount : public ApplicationDBService {
public:
	BALOONPartnerAccount(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONPartnerAccount(){}
public:
	// Wrappers for manual methods
	static const Value __Refill(const Param& param);
	static const Value __Block(const Param& param);
	static const Value __DebitGet(const Param& param);
	static const Value __CreditGet(const Param& param);
	static const Value __OurDebitGet(const Param& param);
	static const Value __OurCreditGet(const Param& param);
	static const Value __OurPartnerAccountListGet(const Param& param);

	// Wrappers for automatic methods declaration
	static const Value __Add(const Param& param);
	static const Value __Update(const Param& param);
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);
	static const Value __PartnerAccountListGet(const Param& param);

	// Manual methods declaration
	static const Value Refill(const Int id, const Decimal refill_amount, const Str reason);
	static const Value Block(const Int id);
	static const Value DebitGet(const Int partnerid);
	static const Value CreditGet(const Int partnerid);
	static const Value OurDebitGet();
	static const Value OurCreditGet();
	static const Value OurPartnerAccountListGet();

	// Automatic methods declaration
	static const Value Add(const Int partnerid, const Str name, const Str atype, const Decimal amount, const Str status);
	static const Value Update(const Int id, const optional<Int> partnerid, const optional<Str> name, const optional<Str> atype, const optional<Decimal> amount, const optional<Str> status);
	static const Value Delete(const Int id);
	static const Value Get(const Int id);
	static const Value PartnerAccountListGet(const optional<Int> partnerid);
private: //prohibited operations
	BALOONPartnerAccount(const BALOONPartnerAccount&);
	BALOONPartnerAccount& operator=(const BALOONPartnerAccount&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONAccountTransaction : public ApplicationDBService {
public:
	BALOONAccountTransaction(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONAccountTransaction(){}
public:
	// Wrappers for manual methods
	static const Value __OurDebitAccountTransactionListGet(const Param& param);
	static const Value __DebitAccountTransactionListGet(const Param& param);
	static const Value __OurCreditAccountTransactionListGet(const Param& param);
	static const Value __CreditAccountTransactionListGet(const Param& param);

	// Wrappers for automatic methods declaration
	static const Value __Add(const Param& param);
	static const Value __Update(const Param& param);
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);
	static const Value __AccountTransactionListGet(const Param& param);

	// Manual methods declaration
	static const Value OurDebitAccountTransactionListGet();
	static const Value DebitAccountTransactionListGet(const Int partnerid);
	static const Value OurCreditAccountTransactionListGet();
	static const Value CreditAccountTransactionListGet(const Int partnerid);

	// Automatic methods declaration
	static const Value Add(const Int accountid, const Decimal amount, const Str cache_flow, const Str reason);
	static const Value Update(const Int id, const optional<Int> accountid, const optional<Decimal> amount, const optional<Str> cache_flow, const optional<Str> reason);
	static const Value Delete(const Int id);
	static const Value Get(const Int id);
	static const Value AccountTransactionListGet(const optional<Int> accountid);
private: //prohibited operations
	BALOONAccountTransaction(const BALOONAccountTransaction&);
	BALOONAccountTransaction& operator=(const BALOONAccountTransaction&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONPartnerWeekTariffGroup : public ApplicationDBService {
public:
	BALOONPartnerWeekTariffGroup(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONPartnerWeekTariffGroup(){}
public:
	// Wrappers for manual methods

	// Wrappers for automatic methods declaration
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);

	// Manual methods declaration

	// Automatic methods declaration
	static const Value Delete(const Int partnerid, const Int tariffgroupid);
	static const Value Get(const Int partnerid, const Int tariffgroupid);
private: //prohibited operations
	BALOONPartnerWeekTariffGroup(const BALOONPartnerWeekTariffGroup&);
	BALOONPartnerWeekTariffGroup& operator=(const BALOONPartnerWeekTariffGroup&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONPartnerOrder : public ApplicationDBService {
public:
	BALOONPartnerOrder(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONPartnerOrder(){}
public:
	// Wrappers for manual methods
	static const Value __Get(const Param& param);
	static const Value __OurPartnerOrderListGet(const Param& param);
	static const Value __OurCreate(const Param& param);
	static const Value __OurTicketsDownload(const Param& param);
	static const Value __TicketsDownload(const Param& param);
	static const Value __Place_API(const Param& param);
	static const Value __DetailsGet(const Param& param);

	// Wrappers for automatic methods declaration
	static const Value __Add(const Param& param);
	static const Value __Update(const Param& param);
	static const Value __Delete(const Param& param);
	static const Value __PartnerOrderListGet(const Param& param);

	// Manual methods declaration
	static const Value Get(const Int id);
	static const Value OurPartnerOrderListGet();
	static const Value OurCreate(const ADate adate, const Int week_tariffid, const Int identifiers_count, const Int accountid, const Str comment);
	static const Value OurTicketsDownload(const Int id);
	static const Value TicketsDownload(const Int id);
	static const Value Place_API(const ADate adate, const Value identifiers, const Int week_tariffid, const Str token);
	static const Value DetailsGet(const Int id);

	// Automatic methods declaration
	static const Value Add(const Int partnerid, const Decimal initial_cost, const Decimal discount, const Decimal total_cost, const Int week_tariffid);
	static const Value Update(const Int id, const optional<Int> partnerid, const optional<Decimal> initial_cost, const optional<Decimal> discount, const optional<Decimal> total_cost, const optional<Int> week_tariffid);
	static const Value Delete(const Int id);
	static const Value PartnerOrderListGet(const optional<Int> partnerid, const optional<Int> week_tariffid);
private: //prohibited operations
	BALOONPartnerOrder(const BALOONPartnerOrder&);
	BALOONPartnerOrder& operator=(const BALOONPartnerOrder&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONPartnerOrderline : public ApplicationDBService {
public:
	BALOONPartnerOrderline(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONPartnerOrderline(){}
public:
	// Wrappers for manual methods
	static const Value __OurPartnerOrderlineListGet(const Param& param);
	static const Value __OurTicketListGet(const Param& param);
	static const Value __AsyncRegisterInContour(const Param& param);

	// Wrappers for automatic methods declaration
	static const Value __Add(const Param& param);
	static const Value __Update(const Param& param);
	static const Value __Delete(const Param& param);
	static const Value __Get(const Param& param);
	static const Value __PartnerOrderlineListGet(const Param& param);

	// Manual methods declaration
	static const Value OurPartnerOrderlineListGet(const Int orderid);
	static const Value OurTicketListGet(const Int orderid);
	static const Value AsyncRegisterInContour();

	// Automatic methods declaration
	static const Value Add(const Int orderid, const Int identifierid, const Decimal price, const Str service_rulename);
	static const Value Update(const Int orderid, const Int identifierid, const optional<Decimal> price, const optional<Str> service_rulename);
	static const Value Delete(const Int orderid, const Int identifierid);
	static const Value Get(const Int orderid, const Int identifierid);
	static const Value PartnerOrderlineListGet(const optional<Int> orderid, const optional<Int> identifierid, const optional<Str> service_rulename);
private: //prohibited operations
	BALOONPartnerOrderline(const BALOONPartnerOrderline&);
	BALOONPartnerOrderline& operator=(const BALOONPartnerOrderline&);
private:
	void initialize(ParamsMapRef params);
};

class Baloon_DECL BALOONAuxiliary : public ApplicationDBService {
public:
	BALOONAuxiliary(ParamsMapRef params) {
		initialize(params);
	}
	virtual ~BALOONAuxiliary(){}
public:
	// Wrappers for manual methods
	static const Value __CurrentDate(const Param& param);

	// Wrappers for automatic methods declaration

	// Manual methods declaration
	static const Value CurrentDate();

	// Automatic methods declaration
private: //prohibited operations
	BALOONAuxiliary(const BALOONAuxiliary&);
	BALOONAuxiliary& operator=(const BALOONAuxiliary&);
private:
	void initialize(ParamsMapRef params);
};

void cloneBaloonTenant(Connection* rdb, int64_t baseTenantID, int64_t newTenantID);
void deleteBaloonTenant(Connection* rdb, int64_t tenantID);
class Baloon_DECL Baloon : public STContainer {
	typedef ThreadBalancer<ASPtr, DBServiceFactory, ServiceWorker> ObjectBalancerType;
public:
	Baloon(ParamsMapRef params) : objectBalancer_(new DBServiceFactory, 10) {
		initialize(params);
		Start();
    objectBalancer_.Start();
	}
	virtual ~Baloon(){}
private:
	void initialize(ParamsMapRef params);
	ObjectBalancerType objectBalancer_;
}; // class Baloon

}//namespace BALOON
extern "C" {
	Baloon_DECL Effi::STContainer* GetServiceInstance(Effi::ParamsMapRef params);
}
#endif
