import React from 'react';
import { observer } from 'mobx-react';
import { appContext } from './TicketApp';
import { DatePicker } from 'lib/date_picker';
import SelectField from 'lib/select-field';
import Counter from 'lib/counter';
import Checkout from './Checkout';

@observer
export default class ShopPage extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			page: "shop",
			date: new Date(),
			count: 1,
			rule: {
				index: 0,
				text: ""
			},
			rules: []
		}
	}

	changeCount = (count) => {
		this.setState({count: count});
	}

	setDate = (date) => {
		this.setState({date: date});
	}

	setRule = (rule) => {
		console.log(rule);
		this.setState({rule: rule});
	}

	submitTicket = () =>{
        appContext.appState.order = {
            date: this.state.date,
            count: this.state.count,
            rule: this.state.rule,
            price: 0,
            id: 0
        }
        this.setState({page: "checkout"})
        // appContext.appState.openCheckout();

	}

	setShop = () => {
		this.setState({page: "shop"});
	}

	componentDidMount() {
		appContext.appState.requestServiceRules((data) => {
			if (data.length <= 0) return;
			let rules = this.parseRules(data);
			this.setState({rules: rules, rule: rules[0]});
		})
	} 

	parseRules = (data) => {
		let rules = [];
		data.map((item, i) => {
			rules.push({
				index: i,
				text: item.name,
			})
		})
		return rules;
	}


	render() {

		const costs = appContext.appState.costs;
		const { rules } = this.state;
		/*{costs.map((item, i) => {
							return(<li key={i}>Цена за проезд {item.name} - {item.price} руб. в будни и {item.price} руб. в выходные</li>);
						})}*/
		const shop = (
			<div>

			<h1>ПОКУПКА БИЛЕТА</h1>
				<div className="ticket-settings">			
					<div className="ticket-date">
						<h2>Выберите дату поездки</h2>
						<DatePicker handleChange={this.setDate}/>
					</div>
					<div className="ticket-rule">
						<h2>Выберите тариф</h2>
						{
							rules.length == 0 ? (<div></div>) : (<SelectField options={rules} selectOption={this.setRule}/>)
						}
					</div>
					<div className="ticket-count">
						<h2>Выберите количество </h2>
						<h4>(не более 50 билетов за заказ)</h4>
						<Counter initValue={1} onChange={this.changeCount} maxValue={50}/>
					</div>
				</div>
				<div className="costs-label">
					<p>
					Цена за проезд в <span>одну сторону</span>
					</p>
					<ul>
						<li>
						 понедельник - 100 руб 
						</li>
						<li>
						 вторник - 150 руб 
						</li>	
						<li>
						 среда-пятница - 250 руб 
						</li>		
						<li>
						 выходные и праздничные дни - 400 руб 
						</li>	
					</ul>
					<p>
					Цена за проезд <span>туда-обратно</span>
					</p>
					<ul>
						<li>
						 понедельник - 150 руб 
						</li>
						<li>
						 вторник - 250 руб 
						</li>	
						<li>
						 среда-пятница - 450 руб 
						</li>		
						<li>
						 выходные и праздничные дни - 600 руб 
						</li>	
					</ul>
				</div>
				<div className="submit-section">
					<button className="purchase-button" onClick={this.submitTicket}>
						Купить
					</button>
				</div>
				</div>
		)

		const checkout = (
			<Checkout goBack={this.setShop} />
		)
		return(
			<div className="shop-layout">
				{this.state.page == "shop" ? shop : (<div></div>)}
				{this.state.page == "checkout" ? checkout : (<div></div>)}
				
			</div>
		)
	}
}


























