if (typeof String.prototype.startsWith != 'function') {
	String.prototype.startsWith = function (str) {
		return this.indexOf(str) === 0;
	};
}

function EffiProtocol(opts) {
	opts = opts || {};
	this.login = opts.login || 'guest';
	this.password = opts.password || (this.login == 'guest' ? '1q2w3e' : '');
	this.host = opts.host || '';
	this.authenticated = false;

	var context = this;
	var error_axml_re = /<xmlstream><Exception><string>(.*)<\/string><u><int>(\d+)<\/int><\/u><\/Exception><\/xmlstream>/;

	function parseStructure(obj) {
		var result = {};
		for (var i=0; i<obj.children.length; i+=2) {
			var key = obj.children[i].textContent,
				value = parseValue(obj.children[i+1]);
			result[key] = value;
		}
		return result;
	}

	function parseArray(obj) {
		var result = [];
		for (var i=0; i<obj.children.length; i++) {
			result[i] = parseValue(obj.children[i]);
		}
		return result;
	}

	function parseException(obj) {
		result = {
			ExceptionText: "",
			ErrorCode: undefined
		};
		if (obj.children.length < 1) return result;
		result.ExceptionText = obj.children[0].textContent;
		if (obj.children.length > 1) result.ErrorCode = parseValue(obj.children[1]);
		return result;
	}

	function parseTime(val) {
		found = val.match(/(\d\d\d\d)(\d\d)(\d\d)T(\d\d)(\d\d)(\d\d)/);
		if (found != null) {
			return new Date(parseInt(found[1]), parseInt(found[2])-1, parseInt(found[3]), parseInt(found[4]), parseInt(found[5]), parseInt(found[6]));
		}
		return null;
	}
	function parseDate(val) {
		found = val.match(/(\d\d\d\d)(\d\d)(\d\d)/);
		if (found != null) {
			return new Date(parseInt(found[1]), parseInt(found[2])-1, parseInt(found[3]));
		}
		return null;
	}

	function parseValue(obj) {
		if (obj == null || null == obj.children[0]) return null;
		var container = obj.children[0];
		if (container.nodeName == 'STRUCTURE') return parseStructure(container);
		else if (container.nodeName == 'ARRAY') return parseArray(container);
		else if (container.nodeName == 'UL' || container.nodeName == 'OPTIONAL' || container.nodeName == 'U') return parseValue(container);
		else if (container.nodeName == 'INT64_T' || container.nodeName == 'INT') return parseInt(container.textContent);
		else if (container.nodeName == 'TIME') return parseTime(container.children[0].textContent);
		else if (container.nodeName == 'ADATE') return parseDate(container.children[0].textContent);
		else if (container.nodeName == 'DECIMAL') return parseFloat(container.textContent);
		else if (container.nodeName == 'EXCEPTION') return parseException(container);
		else return container.textContent;
		return null;
	}

	function parseAXML(data) {
		var apacket_re = /APacket\(\d+ ,"","","","","",\{"ResponseTo":ul\(0 \)\},(.*)\)/;
		var result = data,
			$xml = jQuery(data);
		if (data.startsWith('APacket')) {
			result = result.replace(apacket_re, '$1');
		}

		var values = $xml.find('APacket > Value');
		if (!values || values.length == 0) {
			if ($xml.length < 2) return;
			return parseValue($xml[1])
		}
		return parseValue(values[values.length-1]);
	}
	
	this.auth = function(fn, error_callback) {
		jQuery.ajax({
			url: context.host + '/auth/login',
			type: 'POST',
			data: "Login=s:"+context.login+"&Password=s:"+context.password+"&Lang=s:ru_RU&Skin=s:aquarium&TimeZone=i:-180&",
			dataType: "text",
			success: function (data) {
				this.authenticated = true;
				fn();
			},
			error: function (x, status, error) {
				var responseError = parseAXML(x.responseText);
				console.error(responseError.ErrorCode, responseError.ExceptionText);
				if (typeof error_callback != 'undefined') error_callback(responseError);
				else throw responseError;
			}
		});
	}

	this.request = function (opts) {
		jQuery.ajax({
			url: context.host + opts.url,
			type: 'GET',
			data: opts.data || "",
			dataType: "text",
			success: function (data) {
				var res = parseAXML(data);
				if (typeof opts.callback != 'undefined') opts.callback(res);
			},
			error: function (x, status, error) {
				var responseError = parseAXML(x.responseText);
				console.error(responseError.ErrorCode, responseError.ExceptionText);
				if (responseError.ErrorCode == 101 || responseError.ErrorCode == 100) {
					context.auth(function () {
						context.request(opts);
					});
				}
				else if (typeof opts.error  != 'undefined') opts.error(responseError);
				else throw responseError;
			}
		});
	}
}

function paddy(n, p, c) {
	var pad_char = typeof c !== 'undefined' ? c : '0';
	var pad = new Array(1 + p).join(pad_char);
	return (pad + n).slice(-pad.length);
}
function format_effi_date(date) {
	if (date == null) return 'not-a-date-time';
	return paddy(date.getFullYear(), 4) + paddy(date.getMonth()+1, 2) + paddy(date.getDate(), 2);
}

function encodeAURLComponent(obj) {
	var serialized = '';
	if (jQuery.isArray(obj)) {
		s = '';
		for (var i=0; i<obj.length; i++) {
			var o = obj[i];
			s += encodeAURLComponent(o);
		}
		serialized += 'Array:' + s + '&';
	}
	else if (obj.type == 'int' || typeof obj.value == 'number') serialized = 'i:' + obj.value + '&';
	else if (typeof obj.value == 'float') serialized = 'd:' + obj.value + '&';
	// else if (null != (m = value.match(/^(\d{2})\.(\d{2})\.(\d{4})/))) serialized = 'ADate:s:' + m[3]+m[2]+m[1] + '&&';
	else if (obj.type == 'date' || obj.value instanceof Date) serialized = 'ADate:s:' + format_effi_date(obj.value) + '&&';
	else if (obj.type == 'checkbox') serialized = 's:' + obj.value + '&';
	else serialized = 's:' + obj.value.replace(/ /g, '\\w') + '&';
	return serialized;
}

function buildParams( prefix, obj, add ) {
	var name;

	if ( jQuery.isArray( obj ) ) {
		// Serialize array item.
		jQuery.each( obj, function( i, v ) {
			if (rbracket.test( prefix )) {
				// Treat each array item as a scalar.
				add( prefix, v );
			} else {
				// Item is non-scalar (array or object), encode its numeric index.
				buildParams( prefix + "[" + ( typeof v === "object" ? i : "" ) + "]", v, add );
			}
		});

	} else if (jQuery.type( obj ) === "object") {
		// Serialize object item.
		for ( name in obj ) {
			buildParams( prefix + "[" + name + "]", obj[ name ], add );
		}

	} else {
		// Serialize scalar item.
		add( prefix, obj );
	}
}

function serializeAURL(a) {
	var result = '';
	for (var key in a) {
		var o = a[key];
		result += key + "=" + encodeAURLComponent(o);
	}
	return result;
};


jQuery.fn.extend({
	serializeAURL: function() {
		return serializeAURL( this.serializeTypedArray() );
	},
	serializeTypedArray: function() {
		var rCRLF = /\r?\n/g,
			rsubmitterTypes = /^(?:submit|button|image|reset|file)$/i,
			rsubmittable = /^(?:input|select|textarea|keygen)/i,
			rcheckableType = /^(?:checkbox|radio)$/i;
		
		function getv(val, type, isArray) {
			type = type || 'string';
			var v = "";
			if (type == 'date') {
				var found = val.match(/(\d\d)\.(\d\d)\.(\d\d\d\d)( (\d\d):(\d\d)(:(\d\d))?)?/);
				if (found != null) {
					// var h = found[5] ? parseInt(found[5]) : undefined, 
					// 	m = found[6] ? parseInt(found[6]) : undefined, 
					// 	s = found[8] ? parseInt(found[8]) : undefined;
					v = new Date(parseInt(found[3]), parseInt(found[2])-1, parseInt(found[1]));
					// console.log(parseInt(found[3]), parseInt(found[2])-1, parseInt(found[1]), h, m, s, '->', v);
				}
				else v = null;
			}
			// else if (type == 'checkbox') 
			else if (type == 'int') v = parseInt(val);
			else if (type == 'checkbox' && !isArray) {
				v = (val == 'on' ? true : false);
			}
			else v = val.replace( rCRLF, "\r\n" );
			return v;
		}

		var list = this.map(function() {
			// Can add propHook for "elements" to filter or add form elements
			var elements = jQuery.prop( this, "elements" );
			return elements ? jQuery.makeArray( elements ) : this;
		})
		.filter(function() {
			var type = this.type;

			// Use .is( ":disabled" ) so that fieldset[disabled] works
			return this.name && !jQuery( this ).is( ":disabled" ) &&
				rsubmittable.test( this.nodeName ) && !rsubmitterTypes.test( type ) &&
				( this.checked || !rcheckableType.test( type ) );
		})
		.map(function( i, elem ) {
			var val = jQuery( this ).val(),
				type = jQuery( this ).attr('data-type') || this.type,
				isArray = jQuery( this ).attr('data-array') == 'true' || false;

			return val == null ?
				null :
				jQuery.isArray( val ) ?
					jQuery.map( val, function( val ) {
						return { name: elem.name, value: getv(val, type, isArray), type: type, isArray: isArray };
					}) :
					{ name: elem.name, value: getv(val, type, isArray), type: type, isArray: isArray };
		}).get();

		var s = {}
		for (var i=0; i<list.length; i++) {
			var o = list[i];
			if (o.isArray || o.name in s) {
				if (!(o.name in s)) s[o.name] = [o];
				else s[o.name].push(o);
			}
			else s[o.name] = o;
		}
		return s;
	}
});
