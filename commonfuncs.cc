#include "commonfuncs.h"

namespace BALOON {
using namespace Effi;

Locker qrcodeLocker_;

ShPtr<Blob> generateQRCode(string data, double scale) {
    Log(5) << "generateQRCode data=" << data << ", scale=" << scale << endl;
    SafeLock lock(qrcodeLocker_); // hangs up w/o lock

    QRcode* qrc;
    if (!(qrc = QRcode_encodeString(data.c_str(), 0, QR_ECLEVEL_H, QR_MODE_8, 1))) {
        throw Exception("Error while encoding QR code");
    }

    MagickWandGenesis();
    MagickWand *mwand = NewMagickWand();
    PixelWand  *pwand = NewPixelWand();
    PixelSetColor(pwand, "white");
    MagickNewImage(mwand, qrc->width, qrc->width, pwand);

    DrawingWand *dwand = NewDrawingWand();
    PixelSetColor(pwand, "black");
    DrawSetStrokeColor(dwand, pwand);

    unsigned char* data_point = qrc->data;
    for (int y=0; y<qrc->width; y++) {
        for (int x=0; x<qrc->width; x++) {
            if (*data_point & 1) DrawPoint(dwand, (double)x, (double)y);
            // if (*data_point & 1) DrawRectangle(dwand, (double)x*scale, (double)y*scale, x*scale+scale, y*scale+scale);
            data_point++;
        }
    }

    MagickDrawImage(mwand, dwand);

    MagickResizeImage(mwand, qrc->width*scale, qrc->width*scale, PointFilter, 1);
    MagickSetImageFormat(mwand, "png");
    size_t image_size;
    unsigned char *image = MagickGetImageBlob(mwand, &image_size);
    // MagickWriteImage(mwand,"qrcode.png");

    if (pwand) DestroyPixelWand(pwand);
    if (mwand) mwand = DestroyMagickWand(mwand);
    MagickWandTerminus();

    QRcode_free(qrc);

    ShPtr<Blob> blob = new Blob(image_size, reinterpret_cast<const char*>(image));
    return blob;
}

static const char* B64chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

static const int B64index[ 256 ] =
{
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  62, 63, 62, 62, 63,
    52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 0,  0,  0,  0,  0,  0,
    0,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10, 11, 12, 13, 14,
    15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 0,  0,  0,  0,  63,
    0,  26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
    41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51
};

const std::string b64encode(const void* data, const size_t &len)
{
    std::string result((len + 2) / 3 * 4, '=');
    char *p = (char*) data, *str = &result[0];
    size_t j = 0, pad = len % 3;
    const size_t last = len - pad;

    for (size_t i = 0; i < last; i += 3)
    {
        int n = int(p[i]) << 16 | int(p[i + 1]) << 8 | p[i + 2];
        str[j++] = B64chars[n >> 18];
        str[j++] = B64chars[n >> 12 & 0x3F];
        str[j++] = B64chars[n >> 6 & 0x3F];
        str[j++] = B64chars[n & 0x3F];
    }
    if (pad)  /// set padding
    {
        int n = --pad ? int(p[last]) << 8 | p[last + 1] : p[last];
        str[j++] = B64chars[pad ? n >> 10 & 0x3F : n >> 2];
        str[j++] = B64chars[pad ? n >> 4 & 0x03F : n << 4 & 0x3F];
        str[j++] = pad ? B64chars[n << 2 & 0x3F] : '=';
    }
    return result;
}

const std::string b64decode(const void* data, const size_t &len)
{
    if (len == 0) return "";

    unsigned char *p = (unsigned char*) data;
    size_t j = 0,
        pad1 = len % 4 || p[len - 1] == '=',
        pad2 = pad1 && (len % 4 > 2 || p[len - 2] != '=');
    const size_t last = (len - pad1) / 4 << 2;
    std::string result(last / 4 * 3 + pad1 + pad2, '\0');
    unsigned char *str = (unsigned char*) &result[0];

    for (size_t i = 0; i < last; i += 4)
    {
        int n = B64index[p[i]] << 18 | B64index[p[i + 1]] << 12 | B64index[p[i + 2]] << 6 | B64index[p[i + 3]];
        str[j++] = n >> 16;
        str[j++] = n >> 8 & 0xFF;
        str[j++] = n & 0xFF;
    }
    if (pad1)
    {
        int n = B64index[p[last]] << 18 | B64index[p[last + 1]] << 12;
        str[j++] = n >> 16;
        if (pad2)
        {
            n |= B64index[p[last + 2]] << 6;
            str[j++] = n >> 8 & 0xFF;
        }
    }
    return result;
}

std::string b64encode(const std::string& str)
{
    return b64encode(str.c_str(), str.size());
}

std::string b64decode(const std::string& str64)
{
    return b64decode(str64.c_str(), str64.size());
}
//------------------------------------------------------------------------------
/// \brief Функция разбиения строки через разделитель
/// \param source исходная строка
/// \param delim символ разделителя
std::vector<std::string> split(const std::string &source, char delim)
{
    std::vector<std::string> items;
    std::string item;
    std::stringstream ss(source);
    
    while (std::getline(ss, item, delim))
        items.push_back(item);
    
    return items;
}

}//namespace BALOON
