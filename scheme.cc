#ifndef _BALOON_SCHEME_C
#define _BALOON_SCHEME_C

#include "ardbms/convertor.h"
#include "adecimal/convertor.h"
#include "ardbms/domain.h"
#include "adecimal/adecimal.h"
using namespace Effi;
#include "scheme.h"
namespace BALOON {
	void PersonArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.userTokenID.Bind(userTokenID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.last_name.Bind(last_name), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.first_name.Bind(first_name), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.middle_name.Bind(middle_name), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.birthdate.Bind(birthdate), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.gender.Bind(gender), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.phone.Bind(phone), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.email.Bind(email), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.regcode.Bind(regcode), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.last_sync.Bind(last_sync), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.oid.Bind(oid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.registered_at.Bind(registered_at), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	PersonArc::PersonArc() : Domain("PersonArc", true), Domain_(*this) {
		pushColumns();
	};
	PersonArc::PersonArc(const PersonArc& copy) : Domain("PersonArc", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, userTokenID(copy.userTokenID)
		, last_name(copy.last_name)
		, first_name(copy.first_name)
		, middle_name(copy.middle_name)
		, birthdate(copy.birthdate)
		, gender(copy.gender)
		, phone(copy.phone)
		, email(copy.email)
		, regcode(copy.regcode)
		, last_sync(copy.last_sync)
		, oid(copy.oid)
		, registered_at(copy.registered_at)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	PersonArc& PersonArc::operator=(const PersonArc& copy) {
		id=copy.id;
		userTokenID=copy.userTokenID;
		last_name=copy.last_name;
		first_name=copy.first_name;
		middle_name=copy.middle_name;
		birthdate=copy.birthdate;
		gender=copy.gender;
		phone=copy.phone;
		email=copy.email;
		regcode=copy.regcode;
		last_sync=copy.last_sync;
		oid=copy.oid;
		registered_at=copy.registered_at;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool PersonArc::operator==(const PersonArc& other) const {
		if( id!=other.id ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool PersonArc::operator<(const PersonArc& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void Person::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.userTokenID.Bind(userTokenID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.last_name.Bind(last_name), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.first_name.Bind(first_name), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.middle_name.Bind(middle_name), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.birthdate.Bind(birthdate), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.gender.Bind(gender), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.phone.Bind(phone), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.email.Bind(email), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.regcode.Bind(regcode), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.last_sync.Bind(last_sync), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.oid.Bind(oid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.registered_at.Bind(registered_at), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	Person::Person() : Domain("Person", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new PersonArc());
	};
	Person::Person(Int __id) : Domain("Person", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new PersonArc());
		id=__id;
	}
	Person::Person(const Person& copy) : Domain("Person", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, userTokenID(copy.userTokenID)
		, last_name(copy.last_name)
		, first_name(copy.first_name)
		, middle_name(copy.middle_name)
		, birthdate(copy.birthdate)
		, gender(copy.gender)
		, phone(copy.phone)
		, email(copy.email)
		, regcode(copy.regcode)
		, last_sync(copy.last_sync)
		, oid(copy.oid)
		, registered_at(copy.registered_at)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	Person& Person::operator=(const Person& copy) {
		id=copy.id;
		userTokenID=copy.userTokenID;
		last_name=copy.last_name;
		first_name=copy.first_name;
		middle_name=copy.middle_name;
		birthdate=copy.birthdate;
		gender=copy.gender;
		phone=copy.phone;
		email=copy.email;
		regcode=copy.regcode;
		last_sync=copy.last_sync;
		oid=copy.oid;
		registered_at=copy.registered_at;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool Person::operator==(const Person& other) const {
		if( id!=other.id ) return false;
		return true;
	};
	bool Person::operator<(const Person& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		return false;
	};
	void SpdServiceRuleArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.name.Bind(name), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.enabled.Bind(enabled), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.extid.Bind(extid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.extcode.Bind(extcode), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.comment.Bind(comment), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.package_cost.Bind(package_cost), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	SpdServiceRuleArc::SpdServiceRuleArc() : Domain("SpdServiceRuleArc", true), Domain_(*this) {
		pushColumns();
	};
	SpdServiceRuleArc::SpdServiceRuleArc(const SpdServiceRuleArc& copy) : Domain("SpdServiceRuleArc", copy.isTenant_), Domain_(*this) 
		, name(copy.name)
		, enabled(copy.enabled)
		, extid(copy.extid)
		, extcode(copy.extcode)
		, comment(copy.comment)
		, package_cost(copy.package_cost)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	SpdServiceRuleArc& SpdServiceRuleArc::operator=(const SpdServiceRuleArc& copy) {
		name=copy.name;
		enabled=copy.enabled;
		extid=copy.extid;
		extcode=copy.extcode;
		comment=copy.comment;
		package_cost=copy.package_cost;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool SpdServiceRuleArc::operator==(const SpdServiceRuleArc& other) const {
		if( name!=other.name ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool SpdServiceRuleArc::operator<(const SpdServiceRuleArc& other) const {
		if( name<other.name ) return true;
		if( name>other.name ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void SpdServiceRule::pushColumns() {
		columns_.push_back(ColSpec(Domain_.name.Bind(name), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.enabled.Bind(enabled), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.extid.Bind(extid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.extcode.Bind(extcode), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.comment.Bind(comment), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.package_cost.Bind(package_cost), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	SpdServiceRule::SpdServiceRule() : Domain("SpdServiceRule", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new SpdServiceRuleArc());
	};
	SpdServiceRule::SpdServiceRule(Str __name) : Domain("SpdServiceRule", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new SpdServiceRuleArc());
		name=__name;
	}
	SpdServiceRule::SpdServiceRule(const SpdServiceRule& copy) : Domain("SpdServiceRule", copy.isTenant_), Domain_(*this) 
		, name(copy.name)
		, enabled(copy.enabled)
		, extid(copy.extid)
		, extcode(copy.extcode)
		, comment(copy.comment)
		, package_cost(copy.package_cost)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	SpdServiceRule& SpdServiceRule::operator=(const SpdServiceRule& copy) {
		name=copy.name;
		enabled=copy.enabled;
		extid=copy.extid;
		extcode=copy.extcode;
		comment=copy.comment;
		package_cost=copy.package_cost;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool SpdServiceRule::operator==(const SpdServiceRule& other) const {
		if( name!=other.name ) return false;
		return true;
	};
	bool SpdServiceRule::operator<(const SpdServiceRule& other) const {
		if( name<other.name ) return true;
		if( name>other.name ) return false;
		return false;
	};
	void PushNotificationArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.token.Bind(token), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.token_type.Bind(token_type), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.first_name.Bind(first_name), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.last_name.Bind(last_name), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.middle_name.Bind(middle_name), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.description.Bind(description), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.enabled.Bind(enabled), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.valid_from.Bind(valid_from), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.last_register.Bind(last_register), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	PushNotificationArc::PushNotificationArc() : Domain("PushNotificationArc", true), Domain_(*this) {
		pushColumns();
	};
	PushNotificationArc::PushNotificationArc(const PushNotificationArc& copy) : Domain("PushNotificationArc", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, token(copy.token)
		, token_type(copy.token_type)
		, first_name(copy.first_name)
		, last_name(copy.last_name)
		, middle_name(copy.middle_name)
		, description(copy.description)
		, enabled(copy.enabled)
		, valid_from(copy.valid_from)
		, last_register(copy.last_register)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	PushNotificationArc& PushNotificationArc::operator=(const PushNotificationArc& copy) {
		id=copy.id;
		token=copy.token;
		token_type=copy.token_type;
		first_name=copy.first_name;
		last_name=copy.last_name;
		middle_name=copy.middle_name;
		description=copy.description;
		enabled=copy.enabled;
		valid_from=copy.valid_from;
		last_register=copy.last_register;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool PushNotificationArc::operator==(const PushNotificationArc& other) const {
		if( id!=other.id ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool PushNotificationArc::operator<(const PushNotificationArc& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void PushNotification::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.token.Bind(token), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.token_type.Bind(token_type), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.first_name.Bind(first_name), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.last_name.Bind(last_name), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.middle_name.Bind(middle_name), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.description.Bind(description), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.enabled.Bind(enabled), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.valid_from.Bind(valid_from), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.last_register.Bind(last_register), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	PushNotification::PushNotification() : Domain("PushNotification", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new PushNotificationArc());
	};
	PushNotification::PushNotification(Int __id) : Domain("PushNotification", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new PushNotificationArc());
		id=__id;
	}
	PushNotification::PushNotification(const PushNotification& copy) : Domain("PushNotification", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, token(copy.token)
		, token_type(copy.token_type)
		, first_name(copy.first_name)
		, last_name(copy.last_name)
		, middle_name(copy.middle_name)
		, description(copy.description)
		, enabled(copy.enabled)
		, valid_from(copy.valid_from)
		, last_register(copy.last_register)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	PushNotification& PushNotification::operator=(const PushNotification& copy) {
		id=copy.id;
		token=copy.token;
		token_type=copy.token_type;
		first_name=copy.first_name;
		last_name=copy.last_name;
		middle_name=copy.middle_name;
		description=copy.description;
		enabled=copy.enabled;
		valid_from=copy.valid_from;
		last_register=copy.last_register;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool PushNotification::operator==(const PushNotification& other) const {
		if( id!=other.id ) return false;
		return true;
	};
	bool PushNotification::operator<(const PushNotification& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		return false;
	};
	void PushNotificationMessagesArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.title.Bind(title), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.message.Bind(message), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.sent.Bind(sent), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.enabled.Bind(enabled), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.message_date.Bind(message_date), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	PushNotificationMessagesArc::PushNotificationMessagesArc() : Domain("PushNotificationMessagesArc", true), Domain_(*this) {
		pushColumns();
	};
	PushNotificationMessagesArc::PushNotificationMessagesArc(const PushNotificationMessagesArc& copy) : Domain("PushNotificationMessagesArc", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, title(copy.title)
		, message(copy.message)
		, sent(copy.sent)
		, enabled(copy.enabled)
		, message_date(copy.message_date)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	PushNotificationMessagesArc& PushNotificationMessagesArc::operator=(const PushNotificationMessagesArc& copy) {
		id=copy.id;
		title=copy.title;
		message=copy.message;
		sent=copy.sent;
		enabled=copy.enabled;
		message_date=copy.message_date;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool PushNotificationMessagesArc::operator==(const PushNotificationMessagesArc& other) const {
		if( id!=other.id ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool PushNotificationMessagesArc::operator<(const PushNotificationMessagesArc& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void PushNotificationMessages::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.title.Bind(title), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.message.Bind(message), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.sent.Bind(sent), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.enabled.Bind(enabled), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.message_date.Bind(message_date), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	PushNotificationMessages::PushNotificationMessages() : Domain("PushNotificationMessages", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new PushNotificationMessagesArc());
	};
	PushNotificationMessages::PushNotificationMessages(Int __id) : Domain("PushNotificationMessages", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new PushNotificationMessagesArc());
		id=__id;
	}
	PushNotificationMessages::PushNotificationMessages(const PushNotificationMessages& copy) : Domain("PushNotificationMessages", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, title(copy.title)
		, message(copy.message)
		, sent(copy.sent)
		, enabled(copy.enabled)
		, message_date(copy.message_date)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	PushNotificationMessages& PushNotificationMessages::operator=(const PushNotificationMessages& copy) {
		id=copy.id;
		title=copy.title;
		message=copy.message;
		sent=copy.sent;
		enabled=copy.enabled;
		message_date=copy.message_date;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool PushNotificationMessages::operator==(const PushNotificationMessages& other) const {
		if( id!=other.id ) return false;
		return true;
	};
	bool PushNotificationMessages::operator<(const PushNotificationMessages& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		return false;
	};
	void PushNotificationDeliveriesArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.deviceid.Bind(deviceid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.messageid.Bind(messageid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.sendresult.Bind(sendresult), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.delivery_date.Bind(delivery_date), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	PushNotificationDeliveriesArc::PushNotificationDeliveriesArc() : Domain("PushNotificationDeliveriesArc", true), Domain_(*this) {
		pushColumns();
	};
	PushNotificationDeliveriesArc::PushNotificationDeliveriesArc(const PushNotificationDeliveriesArc& copy) : Domain("PushNotificationDeliveriesArc", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, deviceid(copy.deviceid)
		, messageid(copy.messageid)
		, sendresult(copy.sendresult)
		, delivery_date(copy.delivery_date)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	PushNotificationDeliveriesArc& PushNotificationDeliveriesArc::operator=(const PushNotificationDeliveriesArc& copy) {
		id=copy.id;
		deviceid=copy.deviceid;
		messageid=copy.messageid;
		sendresult=copy.sendresult;
		delivery_date=copy.delivery_date;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool PushNotificationDeliveriesArc::operator==(const PushNotificationDeliveriesArc& other) const {
		if( id!=other.id ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool PushNotificationDeliveriesArc::operator<(const PushNotificationDeliveriesArc& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void PushNotificationDeliveries::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.deviceid.Bind(deviceid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.messageid.Bind(messageid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.sendresult.Bind(sendresult), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.delivery_date.Bind(delivery_date), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	PushNotificationDeliveries::PushNotificationDeliveries() : Domain("PushNotificationDeliveries", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new PushNotificationDeliveriesArc());
	};
	PushNotificationDeliveries::PushNotificationDeliveries(Int __id) : Domain("PushNotificationDeliveries", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new PushNotificationDeliveriesArc());
		id=__id;
	}
	PushNotificationDeliveries::PushNotificationDeliveries(const PushNotificationDeliveries& copy) : Domain("PushNotificationDeliveries", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, deviceid(copy.deviceid)
		, messageid(copy.messageid)
		, sendresult(copy.sendresult)
		, delivery_date(copy.delivery_date)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	PushNotificationDeliveries& PushNotificationDeliveries::operator=(const PushNotificationDeliveries& copy) {
		id=copy.id;
		deviceid=copy.deviceid;
		messageid=copy.messageid;
		sendresult=copy.sendresult;
		delivery_date=copy.delivery_date;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool PushNotificationDeliveries::operator==(const PushNotificationDeliveries& other) const {
		if( id!=other.id ) return false;
		return true;
	};
	bool PushNotificationDeliveries::operator<(const PushNotificationDeliveries& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		return false;
	};
	void IdentifierRuleArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.name.Bind(name), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.enabled.Bind(enabled), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.replenish_enabled.Bind(replenish_enabled), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.extid.Bind(extid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.extcode.Bind(extcode), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.comment.Bind(comment), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	IdentifierRuleArc::IdentifierRuleArc() : Domain("IdentifierRuleArc", true), Domain_(*this) {
		pushColumns();
	};
	IdentifierRuleArc::IdentifierRuleArc(const IdentifierRuleArc& copy) : Domain("IdentifierRuleArc", copy.isTenant_), Domain_(*this) 
		, name(copy.name)
		, enabled(copy.enabled)
		, replenish_enabled(copy.replenish_enabled)
		, extid(copy.extid)
		, extcode(copy.extcode)
		, comment(copy.comment)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	IdentifierRuleArc& IdentifierRuleArc::operator=(const IdentifierRuleArc& copy) {
		name=copy.name;
		enabled=copy.enabled;
		replenish_enabled=copy.replenish_enabled;
		extid=copy.extid;
		extcode=copy.extcode;
		comment=copy.comment;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool IdentifierRuleArc::operator==(const IdentifierRuleArc& other) const {
		if( name!=other.name ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool IdentifierRuleArc::operator<(const IdentifierRuleArc& other) const {
		if( name<other.name ) return true;
		if( name>other.name ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void IdentifierRule::pushColumns() {
		columns_.push_back(ColSpec(Domain_.name.Bind(name), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.enabled.Bind(enabled), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.replenish_enabled.Bind(replenish_enabled), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.extid.Bind(extid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.extcode.Bind(extcode), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.comment.Bind(comment), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	IdentifierRule::IdentifierRule() : Domain("IdentifierRule", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new IdentifierRuleArc());
	};
	IdentifierRule::IdentifierRule(Str __name) : Domain("IdentifierRule", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new IdentifierRuleArc());
		name=__name;
	}
	IdentifierRule::IdentifierRule(const IdentifierRule& copy) : Domain("IdentifierRule", copy.isTenant_), Domain_(*this) 
		, name(copy.name)
		, enabled(copy.enabled)
		, replenish_enabled(copy.replenish_enabled)
		, extid(copy.extid)
		, extcode(copy.extcode)
		, comment(copy.comment)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	IdentifierRule& IdentifierRule::operator=(const IdentifierRule& copy) {
		name=copy.name;
		enabled=copy.enabled;
		replenish_enabled=copy.replenish_enabled;
		extid=copy.extid;
		extcode=copy.extcode;
		comment=copy.comment;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool IdentifierRule::operator==(const IdentifierRule& other) const {
		if( name!=other.name ) return false;
		return true;
	};
	bool IdentifierRule::operator<(const IdentifierRule& other) const {
		if( name<other.name ) return true;
		if( name>other.name ) return false;
		return false;
	};
	void IdentifierServiceRuleArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.identifier_rulename.Bind(identifier_rulename), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.service_rulename.Bind(service_rulename), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.name.Bind(name), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.enabled.Bind(enabled), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.weekday_cost.Bind(weekday_cost), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.weekend_cost.Bind(weekend_cost), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	IdentifierServiceRuleArc::IdentifierServiceRuleArc() : Domain("IdentifierServiceRuleArc", true), Domain_(*this) {
		pushColumns();
	};
	IdentifierServiceRuleArc::IdentifierServiceRuleArc(const IdentifierServiceRuleArc& copy) : Domain("IdentifierServiceRuleArc", copy.isTenant_), Domain_(*this) 
		, identifier_rulename(copy.identifier_rulename)
		, service_rulename(copy.service_rulename)
		, name(copy.name)
		, enabled(copy.enabled)
		, weekday_cost(copy.weekday_cost)
		, weekend_cost(copy.weekend_cost)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	IdentifierServiceRuleArc& IdentifierServiceRuleArc::operator=(const IdentifierServiceRuleArc& copy) {
		identifier_rulename=copy.identifier_rulename;
		service_rulename=copy.service_rulename;
		name=copy.name;
		enabled=copy.enabled;
		weekday_cost=copy.weekday_cost;
		weekend_cost=copy.weekend_cost;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool IdentifierServiceRuleArc::operator==(const IdentifierServiceRuleArc& other) const {
		if( identifier_rulename!=other.identifier_rulename ) return false;
		if( service_rulename!=other.service_rulename ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool IdentifierServiceRuleArc::operator<(const IdentifierServiceRuleArc& other) const {
		if( identifier_rulename<other.identifier_rulename ) return true;
		if( identifier_rulename>other.identifier_rulename ) return false;
		if( service_rulename<other.service_rulename ) return true;
		if( service_rulename>other.service_rulename ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void IdentifierServiceRule::pushColumns() {
		columns_.push_back(ColSpec(Domain_.identifier_rulename.Bind(identifier_rulename), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.service_rulename.Bind(service_rulename), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.name.Bind(name), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.enabled.Bind(enabled), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.weekday_cost.Bind(weekday_cost), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.weekend_cost.Bind(weekend_cost), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	IdentifierServiceRule::IdentifierServiceRule() : Domain("IdentifierServiceRule", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new IdentifierServiceRuleArc());
	};
	IdentifierServiceRule::IdentifierServiceRule(Str __identifier_rulename, Str __service_rulename) : Domain("IdentifierServiceRule", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new IdentifierServiceRuleArc());
		identifier_rulename=__identifier_rulename;
		service_rulename=__service_rulename;
	}
	IdentifierServiceRule::IdentifierServiceRule(const IdentifierServiceRule& copy) : Domain("IdentifierServiceRule", copy.isTenant_), Domain_(*this) 
		, identifier_rulename(copy.identifier_rulename)
		, service_rulename(copy.service_rulename)
		, name(copy.name)
		, enabled(copy.enabled)
		, weekday_cost(copy.weekday_cost)
		, weekend_cost(copy.weekend_cost)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	IdentifierServiceRule& IdentifierServiceRule::operator=(const IdentifierServiceRule& copy) {
		identifier_rulename=copy.identifier_rulename;
		service_rulename=copy.service_rulename;
		name=copy.name;
		enabled=copy.enabled;
		weekday_cost=copy.weekday_cost;
		weekend_cost=copy.weekend_cost;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool IdentifierServiceRule::operator==(const IdentifierServiceRule& other) const {
		if( identifier_rulename!=other.identifier_rulename ) return false;
		if( service_rulename!=other.service_rulename ) return false;
		return true;
	};
	bool IdentifierServiceRule::operator<(const IdentifierServiceRule& other) const {
		if( identifier_rulename<other.identifier_rulename ) return true;
		if( identifier_rulename>other.identifier_rulename ) return false;
		if( service_rulename<other.service_rulename ) return true;
		if( service_rulename>other.service_rulename ) return false;
		return false;
	};
	void IdentifierCategoryArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.name.Bind(name), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.enabled.Bind(enabled), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.extid.Bind(extid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.extcode.Bind(extcode), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.comment.Bind(comment), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.is_default.Bind(is_default), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	IdentifierCategoryArc::IdentifierCategoryArc() : Domain("IdentifierCategoryArc", true), Domain_(*this) {
		pushColumns();
	};
	IdentifierCategoryArc::IdentifierCategoryArc(const IdentifierCategoryArc& copy) : Domain("IdentifierCategoryArc", copy.isTenant_), Domain_(*this) 
		, name(copy.name)
		, enabled(copy.enabled)
		, extid(copy.extid)
		, extcode(copy.extcode)
		, comment(copy.comment)
		, is_default(copy.is_default)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	IdentifierCategoryArc& IdentifierCategoryArc::operator=(const IdentifierCategoryArc& copy) {
		name=copy.name;
		enabled=copy.enabled;
		extid=copy.extid;
		extcode=copy.extcode;
		comment=copy.comment;
		is_default=copy.is_default;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool IdentifierCategoryArc::operator==(const IdentifierCategoryArc& other) const {
		if( name!=other.name ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool IdentifierCategoryArc::operator<(const IdentifierCategoryArc& other) const {
		if( name<other.name ) return true;
		if( name>other.name ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void IdentifierCategory::pushColumns() {
		columns_.push_back(ColSpec(Domain_.name.Bind(name), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.enabled.Bind(enabled), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.extid.Bind(extid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.extcode.Bind(extcode), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.comment.Bind(comment), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.is_default.Bind(is_default), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	IdentifierCategory::IdentifierCategory() : Domain("IdentifierCategory", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new IdentifierCategoryArc());
	};
	IdentifierCategory::IdentifierCategory(Str __name) : Domain("IdentifierCategory", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new IdentifierCategoryArc());
		name=__name;
	}
	IdentifierCategory::IdentifierCategory(const IdentifierCategory& copy) : Domain("IdentifierCategory", copy.isTenant_), Domain_(*this) 
		, name(copy.name)
		, enabled(copy.enabled)
		, extid(copy.extid)
		, extcode(copy.extcode)
		, comment(copy.comment)
		, is_default(copy.is_default)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	IdentifierCategory& IdentifierCategory::operator=(const IdentifierCategory& copy) {
		name=copy.name;
		enabled=copy.enabled;
		extid=copy.extid;
		extcode=copy.extcode;
		comment=copy.comment;
		is_default=copy.is_default;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool IdentifierCategory::operator==(const IdentifierCategory& other) const {
		if( name!=other.name ) return false;
		return true;
	};
	bool IdentifierCategory::operator<(const IdentifierCategory& other) const {
		if( name<other.name ) return true;
		if( name>other.name ) return false;
		return false;
	};
	void IdentifierRateArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.name.Bind(name), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.enabled.Bind(enabled), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.extid.Bind(extid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.extcode.Bind(extcode), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.comment.Bind(comment), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	IdentifierRateArc::IdentifierRateArc() : Domain("IdentifierRateArc", true), Domain_(*this) {
		pushColumns();
	};
	IdentifierRateArc::IdentifierRateArc(const IdentifierRateArc& copy) : Domain("IdentifierRateArc", copy.isTenant_), Domain_(*this) 
		, name(copy.name)
		, enabled(copy.enabled)
		, extid(copy.extid)
		, extcode(copy.extcode)
		, comment(copy.comment)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	IdentifierRateArc& IdentifierRateArc::operator=(const IdentifierRateArc& copy) {
		name=copy.name;
		enabled=copy.enabled;
		extid=copy.extid;
		extcode=copy.extcode;
		comment=copy.comment;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool IdentifierRateArc::operator==(const IdentifierRateArc& other) const {
		if( name!=other.name ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool IdentifierRateArc::operator<(const IdentifierRateArc& other) const {
		if( name<other.name ) return true;
		if( name>other.name ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void IdentifierRate::pushColumns() {
		columns_.push_back(ColSpec(Domain_.name.Bind(name), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.enabled.Bind(enabled), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.extid.Bind(extid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.extcode.Bind(extcode), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.comment.Bind(comment), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	IdentifierRate::IdentifierRate() : Domain("IdentifierRate", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new IdentifierRateArc());
	};
	IdentifierRate::IdentifierRate(Str __name) : Domain("IdentifierRate", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new IdentifierRateArc());
		name=__name;
	}
	IdentifierRate::IdentifierRate(const IdentifierRate& copy) : Domain("IdentifierRate", copy.isTenant_), Domain_(*this) 
		, name(copy.name)
		, enabled(copy.enabled)
		, extid(copy.extid)
		, extcode(copy.extcode)
		, comment(copy.comment)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	IdentifierRate& IdentifierRate::operator=(const IdentifierRate& copy) {
		name=copy.name;
		enabled=copy.enabled;
		extid=copy.extid;
		extcode=copy.extcode;
		comment=copy.comment;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool IdentifierRate::operator==(const IdentifierRate& other) const {
		if( name!=other.name ) return false;
		return true;
	};
	bool IdentifierRate::operator<(const IdentifierRate& other) const {
		if( name<other.name ) return true;
		if( name>other.name ) return false;
		return false;
	};
	void IdentifierArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.personid.Bind(personid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.code.Bind(code), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.name.Bind(name), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.groupname.Bind(groupname), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.valid_from.Bind(valid_from), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.valid_to.Bind(valid_to), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.permanent_rulename.Bind(permanent_rulename), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.categoryname.Bind(categoryname), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ratename.Bind(ratename), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.clientoid.Bind(clientoid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.status.Bind(status), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.comment.Bind(comment), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.spdgateid.Bind(spdgateid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	IdentifierArc::IdentifierArc() : Domain("IdentifierArc", true), Domain_(*this) {
		pushColumns();
	};
	IdentifierArc::IdentifierArc(const IdentifierArc& copy) : Domain("IdentifierArc", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, personid(copy.personid)
		, code(copy.code)
		, name(copy.name)
		, groupname(copy.groupname)
		, valid_from(copy.valid_from)
		, valid_to(copy.valid_to)
		, permanent_rulename(copy.permanent_rulename)
		, categoryname(copy.categoryname)
		, ratename(copy.ratename)
		, clientoid(copy.clientoid)
		, status(copy.status)
		, comment(copy.comment)
		, spdgateid(copy.spdgateid)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	IdentifierArc& IdentifierArc::operator=(const IdentifierArc& copy) {
		id=copy.id;
		personid=copy.personid;
		code=copy.code;
		name=copy.name;
		groupname=copy.groupname;
		valid_from=copy.valid_from;
		valid_to=copy.valid_to;
		permanent_rulename=copy.permanent_rulename;
		categoryname=copy.categoryname;
		ratename=copy.ratename;
		clientoid=copy.clientoid;
		status=copy.status;
		comment=copy.comment;
		spdgateid=copy.spdgateid;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool IdentifierArc::operator==(const IdentifierArc& other) const {
		if( id!=other.id ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool IdentifierArc::operator<(const IdentifierArc& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void Identifier::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.personid.Bind(personid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.code.Bind(code), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.name.Bind(name), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.groupname.Bind(groupname), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.valid_from.Bind(valid_from), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.valid_to.Bind(valid_to), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.permanent_rulename.Bind(permanent_rulename), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.categoryname.Bind(categoryname), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ratename.Bind(ratename), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.clientoid.Bind(clientoid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.status.Bind(status), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.comment.Bind(comment), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.spdgateid.Bind(spdgateid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	Identifier::Identifier() : Domain("Identifier", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new IdentifierArc());
	};
	Identifier::Identifier(Int __id) : Domain("Identifier", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new IdentifierArc());
		id=__id;
	}
	Identifier::Identifier(const Identifier& copy) : Domain("Identifier", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, personid(copy.personid)
		, code(copy.code)
		, name(copy.name)
		, groupname(copy.groupname)
		, valid_from(copy.valid_from)
		, valid_to(copy.valid_to)
		, permanent_rulename(copy.permanent_rulename)
		, categoryname(copy.categoryname)
		, ratename(copy.ratename)
		, clientoid(copy.clientoid)
		, status(copy.status)
		, comment(copy.comment)
		, spdgateid(copy.spdgateid)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	Identifier& Identifier::operator=(const Identifier& copy) {
		id=copy.id;
		personid=copy.personid;
		code=copy.code;
		name=copy.name;
		groupname=copy.groupname;
		valid_from=copy.valid_from;
		valid_to=copy.valid_to;
		permanent_rulename=copy.permanent_rulename;
		categoryname=copy.categoryname;
		ratename=copy.ratename;
		clientoid=copy.clientoid;
		status=copy.status;
		comment=copy.comment;
		spdgateid=copy.spdgateid;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool Identifier::operator==(const Identifier& other) const {
		if( id!=other.id ) return false;
		return true;
	};
	bool Identifier::operator<(const Identifier& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		return false;
	};
	void OrderSiteArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.code.Bind(code), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.name.Bind(name), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.link.Bind(link), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	OrderSiteArc::OrderSiteArc() : Domain("OrderSiteArc", true), Domain_(*this) {
		pushColumns();
	};
	OrderSiteArc::OrderSiteArc(const OrderSiteArc& copy) : Domain("OrderSiteArc", copy.isTenant_), Domain_(*this) 
		, code(copy.code)
		, name(copy.name)
		, link(copy.link)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	OrderSiteArc& OrderSiteArc::operator=(const OrderSiteArc& copy) {
		code=copy.code;
		name=copy.name;
		link=copy.link;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool OrderSiteArc::operator==(const OrderSiteArc& other) const {
		if( code!=other.code ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool OrderSiteArc::operator<(const OrderSiteArc& other) const {
		if( code<other.code ) return true;
		if( code>other.code ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void OrderSite::pushColumns() {
		columns_.push_back(ColSpec(Domain_.code.Bind(code), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.name.Bind(name), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.link.Bind(link), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	OrderSite::OrderSite() : Domain("OrderSite", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new OrderSiteArc());
	};
	OrderSite::OrderSite(Str __code) : Domain("OrderSite", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new OrderSiteArc());
		code=__code;
	}
	OrderSite::OrderSite(const OrderSite& copy) : Domain("OrderSite", copy.isTenant_), Domain_(*this) 
		, code(copy.code)
		, name(copy.name)
		, link(copy.link)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	OrderSite& OrderSite::operator=(const OrderSite& copy) {
		code=copy.code;
		name=copy.name;
		link=copy.link;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool OrderSite::operator==(const OrderSite& other) const {
		if( code!=other.code ) return false;
		return true;
	};
	bool OrderSite::operator<(const OrderSite& other) const {
		if( code<other.code ) return true;
		if( code>other.code ) return false;
		return false;
	};
	void PersonOrderArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.personid.Bind(personid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.filed_at.Bind(filed_at), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.amount.Bind(amount), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.status.Bind(status), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.promocode.Bind(promocode), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.final_amount.Bind(final_amount), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.sitecode.Bind(sitecode), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	PersonOrderArc::PersonOrderArc() : Domain("PersonOrderArc", true), Domain_(*this) {
		pushColumns();
	};
	PersonOrderArc::PersonOrderArc(const PersonOrderArc& copy) : Domain("PersonOrderArc", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, personid(copy.personid)
		, filed_at(copy.filed_at)
		, amount(copy.amount)
		, status(copy.status)
		, promocode(copy.promocode)
		, final_amount(copy.final_amount)
		, sitecode(copy.sitecode)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	PersonOrderArc& PersonOrderArc::operator=(const PersonOrderArc& copy) {
		id=copy.id;
		personid=copy.personid;
		filed_at=copy.filed_at;
		amount=copy.amount;
		status=copy.status;
		promocode=copy.promocode;
		final_amount=copy.final_amount;
		sitecode=copy.sitecode;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool PersonOrderArc::operator==(const PersonOrderArc& other) const {
		if( id!=other.id ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool PersonOrderArc::operator<(const PersonOrderArc& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void PersonOrder::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.personid.Bind(personid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.filed_at.Bind(filed_at), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.amount.Bind(amount), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.status.Bind(status), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.promocode.Bind(promocode), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.final_amount.Bind(final_amount), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.sitecode.Bind(sitecode), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	PersonOrder::PersonOrder() : Domain("PersonOrder", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new PersonOrderArc());
	};
	PersonOrder::PersonOrder(Int __id) : Domain("PersonOrder", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new PersonOrderArc());
		id=__id;
	}
	PersonOrder::PersonOrder(const PersonOrder& copy) : Domain("PersonOrder", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, personid(copy.personid)
		, filed_at(copy.filed_at)
		, amount(copy.amount)
		, status(copy.status)
		, promocode(copy.promocode)
		, final_amount(copy.final_amount)
		, sitecode(copy.sitecode)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	PersonOrder& PersonOrder::operator=(const PersonOrder& copy) {
		id=copy.id;
		personid=copy.personid;
		filed_at=copy.filed_at;
		amount=copy.amount;
		status=copy.status;
		promocode=copy.promocode;
		final_amount=copy.final_amount;
		sitecode=copy.sitecode;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool PersonOrder::operator==(const PersonOrder& other) const {
		if( id!=other.id ) return false;
		return true;
	};
	bool PersonOrder::operator<(const PersonOrder& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		return false;
	};
	void ParkingAreaArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.name.Bind(name), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.spdgateid.Bind(spdgateid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.enter_timeout.Bind(enter_timeout), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.exit_timeout.Bind(exit_timeout), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	ParkingAreaArc::ParkingAreaArc() : Domain("ParkingAreaArc", true), Domain_(*this) {
		pushColumns();
	};
	ParkingAreaArc::ParkingAreaArc(const ParkingAreaArc& copy) : Domain("ParkingAreaArc", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, name(copy.name)
		, spdgateid(copy.spdgateid)
		, enter_timeout(copy.enter_timeout)
		, exit_timeout(copy.exit_timeout)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	ParkingAreaArc& ParkingAreaArc::operator=(const ParkingAreaArc& copy) {
		id=copy.id;
		name=copy.name;
		spdgateid=copy.spdgateid;
		enter_timeout=copy.enter_timeout;
		exit_timeout=copy.exit_timeout;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool ParkingAreaArc::operator==(const ParkingAreaArc& other) const {
		if( id!=other.id ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool ParkingAreaArc::operator<(const ParkingAreaArc& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void ParkingArea::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.name.Bind(name), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.spdgateid.Bind(spdgateid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.enter_timeout.Bind(enter_timeout), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.exit_timeout.Bind(exit_timeout), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	ParkingArea::ParkingArea() : Domain("ParkingArea", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new ParkingAreaArc());
	};
	ParkingArea::ParkingArea(Int __id) : Domain("ParkingArea", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new ParkingAreaArc());
		id=__id;
	}
	ParkingArea::ParkingArea(const ParkingArea& copy) : Domain("ParkingArea", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, name(copy.name)
		, spdgateid(copy.spdgateid)
		, enter_timeout(copy.enter_timeout)
		, exit_timeout(copy.exit_timeout)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	ParkingArea& ParkingArea::operator=(const ParkingArea& copy) {
		id=copy.id;
		name=copy.name;
		spdgateid=copy.spdgateid;
		enter_timeout=copy.enter_timeout;
		exit_timeout=copy.exit_timeout;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool ParkingArea::operator==(const ParkingArea& other) const {
		if( id!=other.id ) return false;
		return true;
	};
	bool ParkingArea::operator<(const ParkingArea& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		return false;
	};
	void ParkingTimerArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.orderid.Bind(orderid), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.end_time.Bind(end_time), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	ParkingTimerArc::ParkingTimerArc() : Domain("ParkingTimerArc", true), Domain_(*this) {
		pushColumns();
	};
	ParkingTimerArc::ParkingTimerArc(const ParkingTimerArc& copy) : Domain("ParkingTimerArc", copy.isTenant_), Domain_(*this) 
		, orderid(copy.orderid)
		, end_time(copy.end_time)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	ParkingTimerArc& ParkingTimerArc::operator=(const ParkingTimerArc& copy) {
		orderid=copy.orderid;
		end_time=copy.end_time;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool ParkingTimerArc::operator==(const ParkingTimerArc& other) const {
		if( orderid!=other.orderid ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool ParkingTimerArc::operator<(const ParkingTimerArc& other) const {
		if( orderid<other.orderid ) return true;
		if( orderid>other.orderid ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void ParkingTimer::pushColumns() {
		columns_.push_back(ColSpec(Domain_.orderid.Bind(orderid), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.end_time.Bind(end_time), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	ParkingTimer::ParkingTimer() : Domain("ParkingTimer", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new ParkingTimerArc());
	};
	ParkingTimer::ParkingTimer(Int __orderid) : Domain("ParkingTimer", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new ParkingTimerArc());
		orderid=__orderid;
	}
	ParkingTimer::ParkingTimer(const ParkingTimer& copy) : Domain("ParkingTimer", copy.isTenant_), Domain_(*this) 
		, orderid(copy.orderid)
		, end_time(copy.end_time)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	ParkingTimer& ParkingTimer::operator=(const ParkingTimer& copy) {
		orderid=copy.orderid;
		end_time=copy.end_time;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool ParkingTimer::operator==(const ParkingTimer& other) const {
		if( orderid!=other.orderid ) return false;
		return true;
	};
	bool ParkingTimer::operator<(const ParkingTimer& other) const {
		if( orderid<other.orderid ) return true;
		if( orderid>other.orderid ) return false;
		return false;
	};
	void ParkingOrderlineArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.orderid.Bind(orderid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.identifierid.Bind(identifierid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.time_from.Bind(time_from), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.time_upto.Bind(time_upto), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.price.Bind(price), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.areaid.Bind(areaid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	ParkingOrderlineArc::ParkingOrderlineArc() : Domain("ParkingOrderlineArc", true), Domain_(*this) {
		pushColumns();
	};
	ParkingOrderlineArc::ParkingOrderlineArc(const ParkingOrderlineArc& copy) : Domain("ParkingOrderlineArc", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, orderid(copy.orderid)
		, identifierid(copy.identifierid)
		, time_from(copy.time_from)
		, time_upto(copy.time_upto)
		, price(copy.price)
		, areaid(copy.areaid)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	ParkingOrderlineArc& ParkingOrderlineArc::operator=(const ParkingOrderlineArc& copy) {
		id=copy.id;
		orderid=copy.orderid;
		identifierid=copy.identifierid;
		time_from=copy.time_from;
		time_upto=copy.time_upto;
		price=copy.price;
		areaid=copy.areaid;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool ParkingOrderlineArc::operator==(const ParkingOrderlineArc& other) const {
		if( id!=other.id ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool ParkingOrderlineArc::operator<(const ParkingOrderlineArc& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void ParkingOrderline::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.orderid.Bind(orderid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.identifierid.Bind(identifierid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.time_from.Bind(time_from), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.time_upto.Bind(time_upto), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.price.Bind(price), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.areaid.Bind(areaid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	ParkingOrderline::ParkingOrderline() : Domain("ParkingOrderline", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new ParkingOrderlineArc());
	};
	ParkingOrderline::ParkingOrderline(Int __id) : Domain("ParkingOrderline", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new ParkingOrderlineArc());
		id=__id;
	}
	ParkingOrderline::ParkingOrderline(const ParkingOrderline& copy) : Domain("ParkingOrderline", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, orderid(copy.orderid)
		, identifierid(copy.identifierid)
		, time_from(copy.time_from)
		, time_upto(copy.time_upto)
		, price(copy.price)
		, areaid(copy.areaid)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	ParkingOrderline& ParkingOrderline::operator=(const ParkingOrderline& copy) {
		id=copy.id;
		orderid=copy.orderid;
		identifierid=copy.identifierid;
		time_from=copy.time_from;
		time_upto=copy.time_upto;
		price=copy.price;
		areaid=copy.areaid;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool ParkingOrderline::operator==(const ParkingOrderline& other) const {
		if( id!=other.id ) return false;
		return true;
	};
	bool ParkingOrderline::operator<(const ParkingOrderline& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		return false;
	};
	void PersonOrderFiscalDocArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.orderid.Bind(orderid), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.fiscal_docid.Bind(fiscal_docid), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	PersonOrderFiscalDocArc::PersonOrderFiscalDocArc() : Domain("PersonOrderFiscalDocArc", true), Domain_(*this) {
		pushColumns();
	};
	PersonOrderFiscalDocArc::PersonOrderFiscalDocArc(const PersonOrderFiscalDocArc& copy) : Domain("PersonOrderFiscalDocArc", copy.isTenant_), Domain_(*this) 
		, orderid(copy.orderid)
		, fiscal_docid(copy.fiscal_docid)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	PersonOrderFiscalDocArc& PersonOrderFiscalDocArc::operator=(const PersonOrderFiscalDocArc& copy) {
		orderid=copy.orderid;
		fiscal_docid=copy.fiscal_docid;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool PersonOrderFiscalDocArc::operator==(const PersonOrderFiscalDocArc& other) const {
		if( orderid!=other.orderid ) return false;
		if( fiscal_docid!=other.fiscal_docid ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool PersonOrderFiscalDocArc::operator<(const PersonOrderFiscalDocArc& other) const {
		if( orderid<other.orderid ) return true;
		if( orderid>other.orderid ) return false;
		if( fiscal_docid<other.fiscal_docid ) return true;
		if( fiscal_docid>other.fiscal_docid ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void PersonOrderFiscalDoc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.orderid.Bind(orderid), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.fiscal_docid.Bind(fiscal_docid), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	PersonOrderFiscalDoc::PersonOrderFiscalDoc() : Domain("PersonOrderFiscalDoc", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new PersonOrderFiscalDocArc());
	};
	PersonOrderFiscalDoc::PersonOrderFiscalDoc(Int __orderid, Int __fiscal_docid) : Domain("PersonOrderFiscalDoc", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new PersonOrderFiscalDocArc());
		orderid=__orderid;
		fiscal_docid=__fiscal_docid;
	}
	PersonOrderFiscalDoc::PersonOrderFiscalDoc(const PersonOrderFiscalDoc& copy) : Domain("PersonOrderFiscalDoc", copy.isTenant_), Domain_(*this) 
		, orderid(copy.orderid)
		, fiscal_docid(copy.fiscal_docid)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	PersonOrderFiscalDoc& PersonOrderFiscalDoc::operator=(const PersonOrderFiscalDoc& copy) {
		orderid=copy.orderid;
		fiscal_docid=copy.fiscal_docid;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool PersonOrderFiscalDoc::operator==(const PersonOrderFiscalDoc& other) const {
		if( orderid!=other.orderid ) return false;
		if( fiscal_docid!=other.fiscal_docid ) return false;
		return true;
	};
	bool PersonOrderFiscalDoc::operator<(const PersonOrderFiscalDoc& other) const {
		if( orderid<other.orderid ) return true;
		if( orderid>other.orderid ) return false;
		if( fiscal_docid<other.fiscal_docid ) return true;
		if( fiscal_docid>other.fiscal_docid ) return false;
		return false;
	};
	void PersonIdentifierOrderlineArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.orderid.Bind(orderid), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.identifierid.Bind(identifierid), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.price.Bind(price), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.service_rulename.Bind(service_rulename), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.make_transaction.Bind(make_transaction), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	PersonIdentifierOrderlineArc::PersonIdentifierOrderlineArc() : Domain("PersonIdentifierOrderlineArc", true), Domain_(*this) {
		pushColumns();
	};
	PersonIdentifierOrderlineArc::PersonIdentifierOrderlineArc(const PersonIdentifierOrderlineArc& copy) : Domain("PersonIdentifierOrderlineArc", copy.isTenant_), Domain_(*this) 
		, orderid(copy.orderid)
		, identifierid(copy.identifierid)
		, price(copy.price)
		, service_rulename(copy.service_rulename)
		, make_transaction(copy.make_transaction)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	PersonIdentifierOrderlineArc& PersonIdentifierOrderlineArc::operator=(const PersonIdentifierOrderlineArc& copy) {
		orderid=copy.orderid;
		identifierid=copy.identifierid;
		price=copy.price;
		service_rulename=copy.service_rulename;
		make_transaction=copy.make_transaction;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool PersonIdentifierOrderlineArc::operator==(const PersonIdentifierOrderlineArc& other) const {
		if( orderid!=other.orderid ) return false;
		if( identifierid!=other.identifierid ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool PersonIdentifierOrderlineArc::operator<(const PersonIdentifierOrderlineArc& other) const {
		if( orderid<other.orderid ) return true;
		if( orderid>other.orderid ) return false;
		if( identifierid<other.identifierid ) return true;
		if( identifierid>other.identifierid ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void PersonIdentifierOrderline::pushColumns() {
		columns_.push_back(ColSpec(Domain_.orderid.Bind(orderid), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.identifierid.Bind(identifierid), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.price.Bind(price), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.service_rulename.Bind(service_rulename), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.make_transaction.Bind(make_transaction), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	PersonIdentifierOrderline::PersonIdentifierOrderline() : Domain("PersonIdentifierOrderline", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new PersonIdentifierOrderlineArc());
	};
	PersonIdentifierOrderline::PersonIdentifierOrderline(Int __orderid, Int __identifierid) : Domain("PersonIdentifierOrderline", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new PersonIdentifierOrderlineArc());
		orderid=__orderid;
		identifierid=__identifierid;
	}
	PersonIdentifierOrderline::PersonIdentifierOrderline(const PersonIdentifierOrderline& copy) : Domain("PersonIdentifierOrderline", copy.isTenant_), Domain_(*this) 
		, orderid(copy.orderid)
		, identifierid(copy.identifierid)
		, price(copy.price)
		, service_rulename(copy.service_rulename)
		, make_transaction(copy.make_transaction)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	PersonIdentifierOrderline& PersonIdentifierOrderline::operator=(const PersonIdentifierOrderline& copy) {
		orderid=copy.orderid;
		identifierid=copy.identifierid;
		price=copy.price;
		service_rulename=copy.service_rulename;
		make_transaction=copy.make_transaction;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool PersonIdentifierOrderline::operator==(const PersonIdentifierOrderline& other) const {
		if( orderid!=other.orderid ) return false;
		if( identifierid!=other.identifierid ) return false;
		return true;
	};
	bool PersonIdentifierOrderline::operator<(const PersonIdentifierOrderline& other) const {
		if( orderid<other.orderid ) return true;
		if( orderid>other.orderid ) return false;
		if( identifierid<other.identifierid ) return true;
		if( identifierid>other.identifierid ) return false;
		return false;
	};
	void AgentIdentifierOrderlineArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.orderid.Bind(orderid), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.identifierid.Bind(identifierid), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.price.Bind(price), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	AgentIdentifierOrderlineArc::AgentIdentifierOrderlineArc() : Domain("AgentIdentifierOrderlineArc", true), Domain_(*this) {
		pushColumns();
	};
	AgentIdentifierOrderlineArc::AgentIdentifierOrderlineArc(const AgentIdentifierOrderlineArc& copy) : Domain("AgentIdentifierOrderlineArc", copy.isTenant_), Domain_(*this) 
		, orderid(copy.orderid)
		, identifierid(copy.identifierid)
		, price(copy.price)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	AgentIdentifierOrderlineArc& AgentIdentifierOrderlineArc::operator=(const AgentIdentifierOrderlineArc& copy) {
		orderid=copy.orderid;
		identifierid=copy.identifierid;
		price=copy.price;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool AgentIdentifierOrderlineArc::operator==(const AgentIdentifierOrderlineArc& other) const {
		if( orderid!=other.orderid ) return false;
		if( identifierid!=other.identifierid ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool AgentIdentifierOrderlineArc::operator<(const AgentIdentifierOrderlineArc& other) const {
		if( orderid<other.orderid ) return true;
		if( orderid>other.orderid ) return false;
		if( identifierid<other.identifierid ) return true;
		if( identifierid>other.identifierid ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void AgentIdentifierOrderline::pushColumns() {
		columns_.push_back(ColSpec(Domain_.orderid.Bind(orderid), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.identifierid.Bind(identifierid), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.price.Bind(price), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	AgentIdentifierOrderline::AgentIdentifierOrderline() : Domain("AgentIdentifierOrderline", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new AgentIdentifierOrderlineArc());
	};
	AgentIdentifierOrderline::AgentIdentifierOrderline(Int __orderid, Int __identifierid) : Domain("AgentIdentifierOrderline", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new AgentIdentifierOrderlineArc());
		orderid=__orderid;
		identifierid=__identifierid;
	}
	AgentIdentifierOrderline::AgentIdentifierOrderline(const AgentIdentifierOrderline& copy) : Domain("AgentIdentifierOrderline", copy.isTenant_), Domain_(*this) 
		, orderid(copy.orderid)
		, identifierid(copy.identifierid)
		, price(copy.price)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	AgentIdentifierOrderline& AgentIdentifierOrderline::operator=(const AgentIdentifierOrderline& copy) {
		orderid=copy.orderid;
		identifierid=copy.identifierid;
		price=copy.price;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool AgentIdentifierOrderline::operator==(const AgentIdentifierOrderline& other) const {
		if( orderid!=other.orderid ) return false;
		if( identifierid!=other.identifierid ) return false;
		return true;
	};
	bool AgentIdentifierOrderline::operator<(const AgentIdentifierOrderline& other) const {
		if( orderid<other.orderid ) return true;
		if( orderid>other.orderid ) return false;
		if( identifierid<other.identifierid ) return true;
		if( identifierid>other.identifierid ) return false;
		return false;
	};
	void PersonInvoiceArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.orderid.Bind(orderid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	PersonInvoiceArc::PersonInvoiceArc() : Domain("PersonInvoiceArc", true), Domain_(*this) {
		pushColumns();
	};
	PersonInvoiceArc::PersonInvoiceArc(const PersonInvoiceArc& copy) : Domain("PersonInvoiceArc", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, orderid(copy.orderid)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	PersonInvoiceArc& PersonInvoiceArc::operator=(const PersonInvoiceArc& copy) {
		id=copy.id;
		orderid=copy.orderid;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool PersonInvoiceArc::operator==(const PersonInvoiceArc& other) const {
		if( id!=other.id ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool PersonInvoiceArc::operator<(const PersonInvoiceArc& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void PersonInvoice::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.orderid.Bind(orderid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	PersonInvoice::PersonInvoice() : Domain("PersonInvoice", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new PersonInvoiceArc());
	};
	PersonInvoice::PersonInvoice(Int __id) : Domain("PersonInvoice", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new PersonInvoiceArc());
		id=__id;
	}
	PersonInvoice::PersonInvoice(const PersonInvoice& copy) : Domain("PersonInvoice", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, orderid(copy.orderid)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	PersonInvoice& PersonInvoice::operator=(const PersonInvoice& copy) {
		id=copy.id;
		orderid=copy.orderid;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool PersonInvoice::operator==(const PersonInvoice& other) const {
		if( id!=other.id ) return false;
		return true;
	};
	bool PersonInvoice::operator<(const PersonInvoice& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		return false;
	};
	void IdentifierInvoiceArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.identifierid.Bind(identifierid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.fiscal_docid.Bind(fiscal_docid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.eid.Bind(eid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	IdentifierInvoiceArc::IdentifierInvoiceArc() : Domain("IdentifierInvoiceArc", true), Domain_(*this) {
		pushColumns();
	};
	IdentifierInvoiceArc::IdentifierInvoiceArc(const IdentifierInvoiceArc& copy) : Domain("IdentifierInvoiceArc", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, identifierid(copy.identifierid)
		, fiscal_docid(copy.fiscal_docid)
		, eid(copy.eid)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	IdentifierInvoiceArc& IdentifierInvoiceArc::operator=(const IdentifierInvoiceArc& copy) {
		id=copy.id;
		identifierid=copy.identifierid;
		fiscal_docid=copy.fiscal_docid;
		eid=copy.eid;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool IdentifierInvoiceArc::operator==(const IdentifierInvoiceArc& other) const {
		if( id!=other.id ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool IdentifierInvoiceArc::operator<(const IdentifierInvoiceArc& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void IdentifierInvoice::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.identifierid.Bind(identifierid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.fiscal_docid.Bind(fiscal_docid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.eid.Bind(eid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	IdentifierInvoice::IdentifierInvoice() : Domain("IdentifierInvoice", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new IdentifierInvoiceArc());
	};
	IdentifierInvoice::IdentifierInvoice(Int __id) : Domain("IdentifierInvoice", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new IdentifierInvoiceArc());
		id=__id;
	}
	IdentifierInvoice::IdentifierInvoice(const IdentifierInvoice& copy) : Domain("IdentifierInvoice", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, identifierid(copy.identifierid)
		, fiscal_docid(copy.fiscal_docid)
		, eid(copy.eid)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	IdentifierInvoice& IdentifierInvoice::operator=(const IdentifierInvoice& copy) {
		id=copy.id;
		identifierid=copy.identifierid;
		fiscal_docid=copy.fiscal_docid;
		eid=copy.eid;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool IdentifierInvoice::operator==(const IdentifierInvoice& other) const {
		if( id!=other.id ) return false;
		return true;
	};
	bool IdentifierInvoice::operator<(const IdentifierInvoice& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		return false;
	};
	void BaloonProviderArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.default_paygateid.Bind(default_paygateid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.default_fiscalgateid.Bind(default_fiscalgateid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.code_conversion_algo.Bind(code_conversion_algo), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.agent_commission.Bind(agent_commission), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ident_sale_price.Bind(ident_sale_price), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.timeout_before.Bind(timeout_before), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.sun.Bind(sun), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.mon.Bind(mon), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.tue.Bind(tue), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.wed.Bind(wed), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.thu.Bind(thu), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.fri.Bind(fri), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.sat.Bind(sat), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	BaloonProviderArc::BaloonProviderArc() : Domain("BaloonProviderArc", true), Domain_(*this), agent_commission(DNULL) {
		pushColumns();
	};
	BaloonProviderArc::BaloonProviderArc(const BaloonProviderArc& copy) : Domain("BaloonProviderArc", copy.isTenant_), Domain_(*this) 
		, default_paygateid(copy.default_paygateid)
		, default_fiscalgateid(copy.default_fiscalgateid)
		, code_conversion_algo(copy.code_conversion_algo)
		, agent_commission(copy.agent_commission)
		, ident_sale_price(copy.ident_sale_price)
		, timeout_before(copy.timeout_before)
		, sun(copy.sun)
		, mon(copy.mon)
		, tue(copy.tue)
		, wed(copy.wed)
		, thu(copy.thu)
		, fri(copy.fri)
		, sat(copy.sat)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	BaloonProviderArc& BaloonProviderArc::operator=(const BaloonProviderArc& copy) {
		default_paygateid=copy.default_paygateid;
		default_fiscalgateid=copy.default_fiscalgateid;
		code_conversion_algo=copy.code_conversion_algo;
		agent_commission=copy.agent_commission;
		ident_sale_price=copy.ident_sale_price;
		timeout_before=copy.timeout_before;
		sun=copy.sun;
		mon=copy.mon;
		tue=copy.tue;
		wed=copy.wed;
		thu=copy.thu;
		fri=copy.fri;
		sat=copy.sat;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool BaloonProviderArc::operator==(const BaloonProviderArc& other) const {
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool BaloonProviderArc::operator<(const BaloonProviderArc& other) const {
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void BaloonProvider::pushColumns() {
		columns_.push_back(ColSpec(Domain_.default_paygateid.Bind(default_paygateid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.default_fiscalgateid.Bind(default_fiscalgateid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.code_conversion_algo.Bind(code_conversion_algo), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.agent_commission.Bind(agent_commission), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ident_sale_price.Bind(ident_sale_price), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.timeout_before.Bind(timeout_before), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.sun.Bind(sun), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.mon.Bind(mon), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.tue.Bind(tue), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.wed.Bind(wed), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.thu.Bind(thu), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.fri.Bind(fri), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.sat.Bind(sat), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	BaloonProvider::BaloonProvider() : Domain("BaloonProvider", true), Domain_(*this), agent_commission(DNULL) {
		pushColumns();
		AttachArchive(new BaloonProviderArc());
	};
	BaloonProvider::BaloonProvider(const BaloonProvider& copy) : Domain("BaloonProvider", copy.isTenant_), Domain_(*this) 
		, default_paygateid(copy.default_paygateid)
		, default_fiscalgateid(copy.default_fiscalgateid)
		, code_conversion_algo(copy.code_conversion_algo)
		, agent_commission(copy.agent_commission)
		, ident_sale_price(copy.ident_sale_price)
		, timeout_before(copy.timeout_before)
		, sun(copy.sun)
		, mon(copy.mon)
		, tue(copy.tue)
		, wed(copy.wed)
		, thu(copy.thu)
		, fri(copy.fri)
		, sat(copy.sat)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	BaloonProvider& BaloonProvider::operator=(const BaloonProvider& copy) {
		default_paygateid=copy.default_paygateid;
		default_fiscalgateid=copy.default_fiscalgateid;
		code_conversion_algo=copy.code_conversion_algo;
		agent_commission=copy.agent_commission;
		ident_sale_price=copy.ident_sale_price;
		timeout_before=copy.timeout_before;
		sun=copy.sun;
		mon=copy.mon;
		tue=copy.tue;
		wed=copy.wed;
		thu=copy.thu;
		fri=copy.fri;
		sat=copy.sat;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool BaloonProvider::operator==(const BaloonProvider& other) const {
		return true;
	};
	bool BaloonProvider::operator<(const BaloonProvider& other) const {
		return false;
	};
	void ExternalResourceArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.name.Bind(name), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.uri.Bind(uri), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.description.Bind(description), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.enabled.Bind(enabled), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	ExternalResourceArc::ExternalResourceArc() : Domain("ExternalResourceArc", true), Domain_(*this) {
		pushColumns();
	};
	ExternalResourceArc::ExternalResourceArc(const ExternalResourceArc& copy) : Domain("ExternalResourceArc", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, name(copy.name)
		, uri(copy.uri)
		, description(copy.description)
		, enabled(copy.enabled)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	ExternalResourceArc& ExternalResourceArc::operator=(const ExternalResourceArc& copy) {
		id=copy.id;
		name=copy.name;
		uri=copy.uri;
		description=copy.description;
		enabled=copy.enabled;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool ExternalResourceArc::operator==(const ExternalResourceArc& other) const {
		if( id!=other.id ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool ExternalResourceArc::operator<(const ExternalResourceArc& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void ExternalResource::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.name.Bind(name), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.uri.Bind(uri), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.description.Bind(description), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.enabled.Bind(enabled), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	ExternalResource::ExternalResource() : Domain("ExternalResource", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new ExternalResourceArc());
	};
	ExternalResource::ExternalResource(Int __id) : Domain("ExternalResource", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new ExternalResourceArc());
		id=__id;
	}
	ExternalResource::ExternalResource(const ExternalResource& copy) : Domain("ExternalResource", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, name(copy.name)
		, uri(copy.uri)
		, description(copy.description)
		, enabled(copy.enabled)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	ExternalResource& ExternalResource::operator=(const ExternalResource& copy) {
		id=copy.id;
		name=copy.name;
		uri=copy.uri;
		description=copy.description;
		enabled=copy.enabled;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool ExternalResource::operator==(const ExternalResource& other) const {
		if( id!=other.id ) return false;
		return true;
	};
	bool ExternalResource::operator<(const ExternalResource& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		return false;
	};
	void AgentArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.userTokenID.Bind(userTokenID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.name.Bind(name), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.phone.Bind(phone), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.email.Bind(email), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.comments.Bind(comments), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.commission.Bind(commission), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.is_default.Bind(is_default), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	AgentArc::AgentArc() : Domain("AgentArc", true), Domain_(*this), commission(DNULL) {
		pushColumns();
	};
	AgentArc::AgentArc(const AgentArc& copy) : Domain("AgentArc", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, userTokenID(copy.userTokenID)
		, name(copy.name)
		, phone(copy.phone)
		, email(copy.email)
		, comments(copy.comments)
		, commission(copy.commission)
		, is_default(copy.is_default)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	AgentArc& AgentArc::operator=(const AgentArc& copy) {
		id=copy.id;
		userTokenID=copy.userTokenID;
		name=copy.name;
		phone=copy.phone;
		email=copy.email;
		comments=copy.comments;
		commission=copy.commission;
		is_default=copy.is_default;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool AgentArc::operator==(const AgentArc& other) const {
		if( id!=other.id ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool AgentArc::operator<(const AgentArc& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void Agent::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.userTokenID.Bind(userTokenID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.name.Bind(name), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.phone.Bind(phone), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.email.Bind(email), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.comments.Bind(comments), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.commission.Bind(commission), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.is_default.Bind(is_default), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	Agent::Agent() : Domain("Agent", true), Domain_(*this), commission(DNULL) {
		pushColumns();
		AttachArchive(new AgentArc());
	};
	Agent::Agent(Int __id) : Domain("Agent", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new AgentArc());
		id=__id;
	}
	Agent::Agent(const Agent& copy) : Domain("Agent", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, userTokenID(copy.userTokenID)
		, name(copy.name)
		, phone(copy.phone)
		, email(copy.email)
		, comments(copy.comments)
		, commission(copy.commission)
		, is_default(copy.is_default)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	Agent& Agent::operator=(const Agent& copy) {
		id=copy.id;
		userTokenID=copy.userTokenID;
		name=copy.name;
		phone=copy.phone;
		email=copy.email;
		comments=copy.comments;
		commission=copy.commission;
		is_default=copy.is_default;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool Agent::operator==(const Agent& other) const {
		if( id!=other.id ) return false;
		return true;
	};
	bool Agent::operator<(const Agent& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		return false;
	};
	void AgentOrderArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.agentid.Bind(agentid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.code.Bind(code), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.amount.Bind(amount), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.commission.Bind(commission), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.filed_at.Bind(filed_at), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.comment.Bind(comment), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.status.Bind(status), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.AgentOrderType.Bind(AgentOrderType), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	AgentOrderArc::AgentOrderArc() : Domain("AgentOrderArc", true), Domain_(*this) {
		pushColumns();
	};
	AgentOrderArc::AgentOrderArc(const AgentOrderArc& copy) : Domain("AgentOrderArc", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, agentid(copy.agentid)
		, code(copy.code)
		, amount(copy.amount)
		, commission(copy.commission)
		, filed_at(copy.filed_at)
		, comment(copy.comment)
		, status(copy.status)
		, AgentOrderType(copy.AgentOrderType)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	AgentOrderArc& AgentOrderArc::operator=(const AgentOrderArc& copy) {
		id=copy.id;
		agentid=copy.agentid;
		code=copy.code;
		amount=copy.amount;
		commission=copy.commission;
		filed_at=copy.filed_at;
		comment=copy.comment;
		status=copy.status;
		AgentOrderType=copy.AgentOrderType;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool AgentOrderArc::operator==(const AgentOrderArc& other) const {
		if( id!=other.id ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool AgentOrderArc::operator<(const AgentOrderArc& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void AgentOrder::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.agentid.Bind(agentid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.code.Bind(code), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.amount.Bind(amount), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.commission.Bind(commission), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.filed_at.Bind(filed_at), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.comment.Bind(comment), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.status.Bind(status), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.AgentOrderType.Bind(AgentOrderType), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	AgentOrder::AgentOrder() : Domain("AgentOrder", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new AgentOrderArc());
	};
	AgentOrder::AgentOrder(Int __id) : Domain("AgentOrder", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new AgentOrderArc());
		id=__id;
	}
	AgentOrder::AgentOrder(const AgentOrder& copy) : Domain("AgentOrder", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, agentid(copy.agentid)
		, code(copy.code)
		, amount(copy.amount)
		, commission(copy.commission)
		, filed_at(copy.filed_at)
		, comment(copy.comment)
		, status(copy.status)
		, AgentOrderType(copy.AgentOrderType)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	AgentOrder& AgentOrder::operator=(const AgentOrder& copy) {
		id=copy.id;
		agentid=copy.agentid;
		code=copy.code;
		amount=copy.amount;
		commission=copy.commission;
		filed_at=copy.filed_at;
		comment=copy.comment;
		status=copy.status;
		AgentOrderType=copy.AgentOrderType;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool AgentOrder::operator==(const AgentOrder& other) const {
		if( id!=other.id ) return false;
		return true;
	};
	bool AgentOrder::operator<(const AgentOrder& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		return false;
	};
	void IdentifierAgentOrderArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.valid_from.Bind(valid_from), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.valid_to.Bind(valid_to), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.permanent_rulename.Bind(permanent_rulename), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.categoryname.Bind(categoryname), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ratename.Bind(ratename), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.other_card_code.Bind(other_card_code), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	IdentifierAgentOrderArc::IdentifierAgentOrderArc() : Domain("IdentifierAgentOrderArc", true), Domain_(*this) {
		pushColumns();
	};
	IdentifierAgentOrderArc::IdentifierAgentOrderArc(const IdentifierAgentOrderArc& copy) : Domain("IdentifierAgentOrderArc", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, valid_from(copy.valid_from)
		, valid_to(copy.valid_to)
		, permanent_rulename(copy.permanent_rulename)
		, categoryname(copy.categoryname)
		, ratename(copy.ratename)
		, other_card_code(copy.other_card_code)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	IdentifierAgentOrderArc& IdentifierAgentOrderArc::operator=(const IdentifierAgentOrderArc& copy) {
		id=copy.id;
		valid_from=copy.valid_from;
		valid_to=copy.valid_to;
		permanent_rulename=copy.permanent_rulename;
		categoryname=copy.categoryname;
		ratename=copy.ratename;
		other_card_code=copy.other_card_code;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool IdentifierAgentOrderArc::operator==(const IdentifierAgentOrderArc& other) const {
		if( id!=other.id ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool IdentifierAgentOrderArc::operator<(const IdentifierAgentOrderArc& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void IdentifierAgentOrder::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.valid_from.Bind(valid_from), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.valid_to.Bind(valid_to), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.permanent_rulename.Bind(permanent_rulename), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.categoryname.Bind(categoryname), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ratename.Bind(ratename), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.other_card_code.Bind(other_card_code), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	IdentifierAgentOrder::IdentifierAgentOrder() : Domain("IdentifierAgentOrder", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new IdentifierAgentOrderArc());
	};
	IdentifierAgentOrder::IdentifierAgentOrder(Int __id) : Domain("IdentifierAgentOrder", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new IdentifierAgentOrderArc());
		id=__id;
	}
	IdentifierAgentOrder::IdentifierAgentOrder(const IdentifierAgentOrder& copy) : Domain("IdentifierAgentOrder", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, valid_from(copy.valid_from)
		, valid_to(copy.valid_to)
		, permanent_rulename(copy.permanent_rulename)
		, categoryname(copy.categoryname)
		, ratename(copy.ratename)
		, other_card_code(copy.other_card_code)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	IdentifierAgentOrder& IdentifierAgentOrder::operator=(const IdentifierAgentOrder& copy) {
		id=copy.id;
		valid_from=copy.valid_from;
		valid_to=copy.valid_to;
		permanent_rulename=copy.permanent_rulename;
		categoryname=copy.categoryname;
		ratename=copy.ratename;
		other_card_code=copy.other_card_code;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool IdentifierAgentOrder::operator==(const IdentifierAgentOrder& other) const {
		if( id!=other.id ) return false;
		return true;
	};
	bool IdentifierAgentOrder::operator<(const IdentifierAgentOrder& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		return false;
	};
	void PackageAgentOrderArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.service_rulename.Bind(service_rulename), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	PackageAgentOrderArc::PackageAgentOrderArc() : Domain("PackageAgentOrderArc", true), Domain_(*this) {
		pushColumns();
	};
	PackageAgentOrderArc::PackageAgentOrderArc(const PackageAgentOrderArc& copy) : Domain("PackageAgentOrderArc", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, service_rulename(copy.service_rulename)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	PackageAgentOrderArc& PackageAgentOrderArc::operator=(const PackageAgentOrderArc& copy) {
		id=copy.id;
		service_rulename=copy.service_rulename;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool PackageAgentOrderArc::operator==(const PackageAgentOrderArc& other) const {
		if( id!=other.id ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool PackageAgentOrderArc::operator<(const PackageAgentOrderArc& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void PackageAgentOrder::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.service_rulename.Bind(service_rulename), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	PackageAgentOrder::PackageAgentOrder() : Domain("PackageAgentOrder", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new PackageAgentOrderArc());
	};
	PackageAgentOrder::PackageAgentOrder(Int __id) : Domain("PackageAgentOrder", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new PackageAgentOrderArc());
		id=__id;
	}
	PackageAgentOrder::PackageAgentOrder(const PackageAgentOrder& copy) : Domain("PackageAgentOrder", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, service_rulename(copy.service_rulename)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	PackageAgentOrder& PackageAgentOrder::operator=(const PackageAgentOrder& copy) {
		id=copy.id;
		service_rulename=copy.service_rulename;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool PackageAgentOrder::operator==(const PackageAgentOrder& other) const {
		if( id!=other.id ) return false;
		return true;
	};
	bool PackageAgentOrder::operator<(const PackageAgentOrder& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		return false;
	};
	void AgentInvoiceArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.orderid.Bind(orderid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.eid.Bind(eid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	AgentInvoiceArc::AgentInvoiceArc() : Domain("AgentInvoiceArc", true), Domain_(*this) {
		pushColumns();
	};
	AgentInvoiceArc::AgentInvoiceArc(const AgentInvoiceArc& copy) : Domain("AgentInvoiceArc", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, orderid(copy.orderid)
		, eid(copy.eid)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	AgentInvoiceArc& AgentInvoiceArc::operator=(const AgentInvoiceArc& copy) {
		id=copy.id;
		orderid=copy.orderid;
		eid=copy.eid;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool AgentInvoiceArc::operator==(const AgentInvoiceArc& other) const {
		if( id!=other.id ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool AgentInvoiceArc::operator<(const AgentInvoiceArc& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void AgentInvoice::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.orderid.Bind(orderid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.eid.Bind(eid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	AgentInvoice::AgentInvoice() : Domain("AgentInvoice", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new AgentInvoiceArc());
	};
	AgentInvoice::AgentInvoice(Int __id) : Domain("AgentInvoice", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new AgentInvoiceArc());
		id=__id;
	}
	AgentInvoice::AgentInvoice(const AgentInvoice& copy) : Domain("AgentInvoice", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, orderid(copy.orderid)
		, eid(copy.eid)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	AgentInvoice& AgentInvoice::operator=(const AgentInvoice& copy) {
		id=copy.id;
		orderid=copy.orderid;
		eid=copy.eid;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool AgentInvoice::operator==(const AgentInvoice& other) const {
		if( id!=other.id ) return false;
		return true;
	};
	bool AgentInvoice::operator<(const AgentInvoice& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		return false;
	};
	void AccountArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.currency.Bind(currency), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.personid.Bind(personid), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.valid.Bind(valid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.balance.Bind(balance), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	AccountArc::AccountArc() : Domain("AccountArc", true), Domain_(*this) {
		pushColumns();
	};
	AccountArc::AccountArc(const AccountArc& copy) : Domain("AccountArc", copy.isTenant_), Domain_(*this) 
		, currency(copy.currency)
		, personid(copy.personid)
		, valid(copy.valid)
		, balance(copy.balance)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	AccountArc& AccountArc::operator=(const AccountArc& copy) {
		currency=copy.currency;
		personid=copy.personid;
		valid=copy.valid;
		balance=copy.balance;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool AccountArc::operator==(const AccountArc& other) const {
		if( currency!=other.currency ) return false;
		if( personid!=other.personid ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool AccountArc::operator<(const AccountArc& other) const {
		if( currency<other.currency ) return true;
		if( currency>other.currency ) return false;
		if( personid<other.personid ) return true;
		if( personid>other.personid ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void Account::pushColumns() {
		columns_.push_back(ColSpec(Domain_.currency.Bind(currency), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.personid.Bind(personid), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.valid.Bind(valid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.balance.Bind(balance), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	Account::Account() : Domain("Account", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new AccountArc());
	};
	Account::Account(Str __currency, Int __personid) : Domain("Account", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new AccountArc());
		currency=__currency;
		personid=__personid;
	}
	Account::Account(const Account& copy) : Domain("Account", copy.isTenant_), Domain_(*this) 
		, currency(copy.currency)
		, personid(copy.personid)
		, valid(copy.valid)
		, balance(copy.balance)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	Account& Account::operator=(const Account& copy) {
		currency=copy.currency;
		personid=copy.personid;
		valid=copy.valid;
		balance=copy.balance;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool Account::operator==(const Account& other) const {
		if( currency!=other.currency ) return false;
		if( personid!=other.personid ) return false;
		return true;
	};
	bool Account::operator<(const Account& other) const {
		if( currency<other.currency ) return true;
		if( currency>other.currency ) return false;
		if( personid<other.personid ) return true;
		if( personid>other.personid ) return false;
		return false;
	};
	void PackageOrderArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.identifierid.Bind(identifierid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.service_rulename.Bind(service_rulename), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.filed_at.Bind(filed_at), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	PackageOrderArc::PackageOrderArc() : Domain("PackageOrderArc", true), Domain_(*this) {
		pushColumns();
	};
	PackageOrderArc::PackageOrderArc(const PackageOrderArc& copy) : Domain("PackageOrderArc", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, identifierid(copy.identifierid)
		, service_rulename(copy.service_rulename)
		, filed_at(copy.filed_at)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	PackageOrderArc& PackageOrderArc::operator=(const PackageOrderArc& copy) {
		id=copy.id;
		identifierid=copy.identifierid;
		service_rulename=copy.service_rulename;
		filed_at=copy.filed_at;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool PackageOrderArc::operator==(const PackageOrderArc& other) const {
		if( id!=other.id ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool PackageOrderArc::operator<(const PackageOrderArc& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void PackageOrder::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.identifierid.Bind(identifierid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.service_rulename.Bind(service_rulename), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.filed_at.Bind(filed_at), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	PackageOrder::PackageOrder() : Domain("PackageOrder", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new PackageOrderArc());
	};
	PackageOrder::PackageOrder(Int __id) : Domain("PackageOrder", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new PackageOrderArc());
		id=__id;
	}
	PackageOrder::PackageOrder(const PackageOrder& copy) : Domain("PackageOrder", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, identifierid(copy.identifierid)
		, service_rulename(copy.service_rulename)
		, filed_at(copy.filed_at)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	PackageOrder& PackageOrder::operator=(const PackageOrder& copy) {
		id=copy.id;
		identifierid=copy.identifierid;
		service_rulename=copy.service_rulename;
		filed_at=copy.filed_at;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool PackageOrder::operator==(const PackageOrder& other) const {
		if( id!=other.id ) return false;
		return true;
	};
	bool PackageOrder::operator<(const PackageOrder& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		return false;
	};
	void TransactionArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.personid.Bind(personid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.agentid.Bind(agentid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.currency.Bind(currency), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.eid.Bind(eid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.amount.Bind(amount), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.status.Bind(status), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.comment.Bind(comment), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.in_sync.Bind(in_sync), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.valid_until.Bind(valid_until), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.meta.Bind(meta), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	TransactionArc::TransactionArc() : Domain("TransactionArc", true), Domain_(*this) {
		pushColumns();
	};
	TransactionArc::TransactionArc(const TransactionArc& copy) : Domain("TransactionArc", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, personid(copy.personid)
		, agentid(copy.agentid)
		, currency(copy.currency)
		, eid(copy.eid)
		, amount(copy.amount)
		, status(copy.status)
		, comment(copy.comment)
		, in_sync(copy.in_sync)
		, valid_until(copy.valid_until)
		, meta(copy.meta)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	TransactionArc& TransactionArc::operator=(const TransactionArc& copy) {
		id=copy.id;
		personid=copy.personid;
		agentid=copy.agentid;
		currency=copy.currency;
		eid=copy.eid;
		amount=copy.amount;
		status=copy.status;
		comment=copy.comment;
		in_sync=copy.in_sync;
		valid_until=copy.valid_until;
		meta=copy.meta;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool TransactionArc::operator==(const TransactionArc& other) const {
		if( id!=other.id ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool TransactionArc::operator<(const TransactionArc& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void Transaction::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.personid.Bind(personid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.agentid.Bind(agentid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.currency.Bind(currency), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.eid.Bind(eid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.amount.Bind(amount), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.status.Bind(status), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.comment.Bind(comment), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.in_sync.Bind(in_sync), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.valid_until.Bind(valid_until), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.meta.Bind(meta), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	Transaction::Transaction() : Domain("Transaction", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new TransactionArc());
	};
	Transaction::Transaction(Int __id) : Domain("Transaction", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new TransactionArc());
		id=__id;
	}
	Transaction::Transaction(const Transaction& copy) : Domain("Transaction", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, personid(copy.personid)
		, agentid(copy.agentid)
		, currency(copy.currency)
		, eid(copy.eid)
		, amount(copy.amount)
		, status(copy.status)
		, comment(copy.comment)
		, in_sync(copy.in_sync)
		, valid_until(copy.valid_until)
		, meta(copy.meta)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	Transaction& Transaction::operator=(const Transaction& copy) {
		id=copy.id;
		personid=copy.personid;
		agentid=copy.agentid;
		currency=copy.currency;
		eid=copy.eid;
		amount=copy.amount;
		status=copy.status;
		comment=copy.comment;
		in_sync=copy.in_sync;
		valid_until=copy.valid_until;
		meta=copy.meta;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool Transaction::operator==(const Transaction& other) const {
		if( id!=other.id ) return false;
		return true;
	};
	bool Transaction::operator<(const Transaction& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		return false;
	};
	void InfoPlacementArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.widget.Bind(widget), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	InfoPlacementArc::InfoPlacementArc() : Domain("InfoPlacementArc", true), Domain_(*this) {
		pushColumns();
	};
	InfoPlacementArc::InfoPlacementArc(const InfoPlacementArc& copy) : Domain("InfoPlacementArc", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, widget(copy.widget)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	InfoPlacementArc& InfoPlacementArc::operator=(const InfoPlacementArc& copy) {
		id=copy.id;
		widget=copy.widget;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool InfoPlacementArc::operator==(const InfoPlacementArc& other) const {
		if( id!=other.id ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool InfoPlacementArc::operator<(const InfoPlacementArc& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void InfoPlacement::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.widget.Bind(widget), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	InfoPlacement::InfoPlacement() : Domain("InfoPlacement", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new InfoPlacementArc());
	};
	InfoPlacement::InfoPlacement(Int __id) : Domain("InfoPlacement", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new InfoPlacementArc());
		id=__id;
	}
	InfoPlacement::InfoPlacement(const InfoPlacement& copy) : Domain("InfoPlacement", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, widget(copy.widget)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	InfoPlacement& InfoPlacement::operator=(const InfoPlacement& copy) {
		id=copy.id;
		widget=copy.widget;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool InfoPlacement::operator==(const InfoPlacement& other) const {
		if( id!=other.id ) return false;
		return true;
	};
	bool InfoPlacement::operator<(const InfoPlacement& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		return false;
	};
	void HolidayArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.adate.Bind(adate), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.out_of_service.Bind(out_of_service), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.comment.Bind(comment), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	HolidayArc::HolidayArc() : Domain("HolidayArc", true), Domain_(*this) {
		pushColumns();
	};
	HolidayArc::HolidayArc(const HolidayArc& copy) : Domain("HolidayArc", copy.isTenant_), Domain_(*this) 
		, adate(copy.adate)
		, out_of_service(copy.out_of_service)
		, comment(copy.comment)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	HolidayArc& HolidayArc::operator=(const HolidayArc& copy) {
		adate=copy.adate;
		out_of_service=copy.out_of_service;
		comment=copy.comment;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool HolidayArc::operator==(const HolidayArc& other) const {
		if( adate!=other.adate ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool HolidayArc::operator<(const HolidayArc& other) const {
		if( adate<other.adate ) return true;
		if( adate>other.adate ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void Holiday::pushColumns() {
		columns_.push_back(ColSpec(Domain_.adate.Bind(adate), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.out_of_service.Bind(out_of_service), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.comment.Bind(comment), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	Holiday::Holiday() : Domain("Holiday", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new HolidayArc());
	};
	Holiday::Holiday(ADate __adate) : Domain("Holiday", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new HolidayArc());
		adate=__adate;
	}
	Holiday::Holiday(const Holiday& copy) : Domain("Holiday", copy.isTenant_), Domain_(*this) 
		, adate(copy.adate)
		, out_of_service(copy.out_of_service)
		, comment(copy.comment)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	Holiday& Holiday::operator=(const Holiday& copy) {
		adate=copy.adate;
		out_of_service=copy.out_of_service;
		comment=copy.comment;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool Holiday::operator==(const Holiday& other) const {
		if( adate!=other.adate ) return false;
		return true;
	};
	bool Holiday::operator<(const Holiday& other) const {
		if( adate<other.adate ) return true;
		if( adate>other.adate ) return false;
		return false;
	};
	void WeekTariffGroupArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.name.Bind(name), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.description.Bind(description), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.is_default.Bind(is_default), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	WeekTariffGroupArc::WeekTariffGroupArc() : Domain("WeekTariffGroupArc", true), Domain_(*this) {
		pushColumns();
	};
	WeekTariffGroupArc::WeekTariffGroupArc(const WeekTariffGroupArc& copy) : Domain("WeekTariffGroupArc", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, name(copy.name)
		, description(copy.description)
		, is_default(copy.is_default)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	WeekTariffGroupArc& WeekTariffGroupArc::operator=(const WeekTariffGroupArc& copy) {
		id=copy.id;
		name=copy.name;
		description=copy.description;
		is_default=copy.is_default;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool WeekTariffGroupArc::operator==(const WeekTariffGroupArc& other) const {
		if( id!=other.id ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool WeekTariffGroupArc::operator<(const WeekTariffGroupArc& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void WeekTariffGroup::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.name.Bind(name), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.description.Bind(description), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.is_default.Bind(is_default), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	WeekTariffGroup::WeekTariffGroup() : Domain("WeekTariffGroup", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new WeekTariffGroupArc());
	};
	WeekTariffGroup::WeekTariffGroup(Int __id) : Domain("WeekTariffGroup", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new WeekTariffGroupArc());
		id=__id;
	}
	WeekTariffGroup::WeekTariffGroup(const WeekTariffGroup& copy) : Domain("WeekTariffGroup", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, name(copy.name)
		, description(copy.description)
		, is_default(copy.is_default)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	WeekTariffGroup& WeekTariffGroup::operator=(const WeekTariffGroup& copy) {
		id=copy.id;
		name=copy.name;
		description=copy.description;
		is_default=copy.is_default;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool WeekTariffGroup::operator==(const WeekTariffGroup& other) const {
		if( id!=other.id ) return false;
		return true;
	};
	bool WeekTariffGroup::operator<(const WeekTariffGroup& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		return false;
	};
	void WeekTariffArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.agroupid.Bind(agroupid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.week_day.Bind(week_day), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.time_from.Bind(time_from), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.time_upto.Bind(time_upto), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.price.Bind(price), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.pricetype.Bind(pricetype), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.free.Bind(free), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.total.Bind(total), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.counttype.Bind(counttype), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.external_name.Bind(external_name), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.identifier_rulename.Bind(identifier_rulename), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.service_rulename.Bind(service_rulename), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.identifier_categoryname.Bind(identifier_categoryname), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ratename.Bind(ratename), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.enabled.Bind(enabled), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	WeekTariffArc::WeekTariffArc() : Domain("WeekTariffArc", true), Domain_(*this) {
		pushColumns();
	};
	WeekTariffArc::WeekTariffArc(const WeekTariffArc& copy) : Domain("WeekTariffArc", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, agroupid(copy.agroupid)
		, week_day(copy.week_day)
		, time_from(copy.time_from)
		, time_upto(copy.time_upto)
		, price(copy.price)
		, pricetype(copy.pricetype)
		, free(copy.free)
		, total(copy.total)
		, counttype(copy.counttype)
		, external_name(copy.external_name)
		, identifier_rulename(copy.identifier_rulename)
		, service_rulename(copy.service_rulename)
		, identifier_categoryname(copy.identifier_categoryname)
		, ratename(copy.ratename)
		, enabled(copy.enabled)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	WeekTariffArc& WeekTariffArc::operator=(const WeekTariffArc& copy) {
		id=copy.id;
		agroupid=copy.agroupid;
		week_day=copy.week_day;
		time_from=copy.time_from;
		time_upto=copy.time_upto;
		price=copy.price;
		pricetype=copy.pricetype;
		free=copy.free;
		total=copy.total;
		counttype=copy.counttype;
		external_name=copy.external_name;
		identifier_rulename=copy.identifier_rulename;
		service_rulename=copy.service_rulename;
		identifier_categoryname=copy.identifier_categoryname;
		ratename=copy.ratename;
		enabled=copy.enabled;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool WeekTariffArc::operator==(const WeekTariffArc& other) const {
		if( id!=other.id ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool WeekTariffArc::operator<(const WeekTariffArc& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void WeekTariff::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.agroupid.Bind(agroupid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.week_day.Bind(week_day), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.time_from.Bind(time_from), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.time_upto.Bind(time_upto), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.price.Bind(price), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.pricetype.Bind(pricetype), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.free.Bind(free), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.total.Bind(total), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.counttype.Bind(counttype), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.external_name.Bind(external_name), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.identifier_rulename.Bind(identifier_rulename), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.service_rulename.Bind(service_rulename), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.identifier_categoryname.Bind(identifier_categoryname), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ratename.Bind(ratename), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.enabled.Bind(enabled), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	WeekTariff::WeekTariff() : Domain("WeekTariff", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new WeekTariffArc());
	};
	WeekTariff::WeekTariff(Int __id) : Domain("WeekTariff", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new WeekTariffArc());
		id=__id;
	}
	WeekTariff::WeekTariff(const WeekTariff& copy) : Domain("WeekTariff", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, agroupid(copy.agroupid)
		, week_day(copy.week_day)
		, time_from(copy.time_from)
		, time_upto(copy.time_upto)
		, price(copy.price)
		, pricetype(copy.pricetype)
		, free(copy.free)
		, total(copy.total)
		, counttype(copy.counttype)
		, external_name(copy.external_name)
		, identifier_rulename(copy.identifier_rulename)
		, service_rulename(copy.service_rulename)
		, identifier_categoryname(copy.identifier_categoryname)
		, ratename(copy.ratename)
		, enabled(copy.enabled)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	WeekTariff& WeekTariff::operator=(const WeekTariff& copy) {
		id=copy.id;
		agroupid=copy.agroupid;
		week_day=copy.week_day;
		time_from=copy.time_from;
		time_upto=copy.time_upto;
		price=copy.price;
		pricetype=copy.pricetype;
		free=copy.free;
		total=copy.total;
		counttype=copy.counttype;
		external_name=copy.external_name;
		identifier_rulename=copy.identifier_rulename;
		service_rulename=copy.service_rulename;
		identifier_categoryname=copy.identifier_categoryname;
		ratename=copy.ratename;
		enabled=copy.enabled;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool WeekTariff::operator==(const WeekTariff& other) const {
		if( id!=other.id ) return false;
		return true;
	};
	bool WeekTariff::operator<(const WeekTariff& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		return false;
	};
	void SkipassConfigurationArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.groupname.Bind(groupname), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.name.Bind(name), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.identifier_rulename.Bind(identifier_rulename), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.service_rulename.Bind(service_rulename), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.identifier_categoryname.Bind(identifier_categoryname), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ratename.Bind(ratename), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.valid_from.Bind(valid_from), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.valid_to.Bind(valid_to), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.price.Bind(price), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.make_transaction.Bind(make_transaction), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.enabled.Bind(enabled), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	SkipassConfigurationArc::SkipassConfigurationArc() : Domain("SkipassConfigurationArc", true), Domain_(*this) {
		pushColumns();
	};
	SkipassConfigurationArc::SkipassConfigurationArc(const SkipassConfigurationArc& copy) : Domain("SkipassConfigurationArc", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, groupname(copy.groupname)
		, name(copy.name)
		, identifier_rulename(copy.identifier_rulename)
		, service_rulename(copy.service_rulename)
		, identifier_categoryname(copy.identifier_categoryname)
		, ratename(copy.ratename)
		, valid_from(copy.valid_from)
		, valid_to(copy.valid_to)
		, price(copy.price)
		, make_transaction(copy.make_transaction)
		, enabled(copy.enabled)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	SkipassConfigurationArc& SkipassConfigurationArc::operator=(const SkipassConfigurationArc& copy) {
		id=copy.id;
		groupname=copy.groupname;
		name=copy.name;
		identifier_rulename=copy.identifier_rulename;
		service_rulename=copy.service_rulename;
		identifier_categoryname=copy.identifier_categoryname;
		ratename=copy.ratename;
		valid_from=copy.valid_from;
		valid_to=copy.valid_to;
		price=copy.price;
		make_transaction=copy.make_transaction;
		enabled=copy.enabled;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool SkipassConfigurationArc::operator==(const SkipassConfigurationArc& other) const {
		if( id!=other.id ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool SkipassConfigurationArc::operator<(const SkipassConfigurationArc& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void SkipassConfiguration::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.groupname.Bind(groupname), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.name.Bind(name), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.identifier_rulename.Bind(identifier_rulename), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.service_rulename.Bind(service_rulename), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.identifier_categoryname.Bind(identifier_categoryname), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ratename.Bind(ratename), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.valid_from.Bind(valid_from), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.valid_to.Bind(valid_to), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.price.Bind(price), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.make_transaction.Bind(make_transaction), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.enabled.Bind(enabled), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	SkipassConfiguration::SkipassConfiguration() : Domain("SkipassConfiguration", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new SkipassConfigurationArc());
	};
	SkipassConfiguration::SkipassConfiguration(Int __id) : Domain("SkipassConfiguration", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new SkipassConfigurationArc());
		id=__id;
	}
	SkipassConfiguration::SkipassConfiguration(const SkipassConfiguration& copy) : Domain("SkipassConfiguration", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, groupname(copy.groupname)
		, name(copy.name)
		, identifier_rulename(copy.identifier_rulename)
		, service_rulename(copy.service_rulename)
		, identifier_categoryname(copy.identifier_categoryname)
		, ratename(copy.ratename)
		, valid_from(copy.valid_from)
		, valid_to(copy.valid_to)
		, price(copy.price)
		, make_transaction(copy.make_transaction)
		, enabled(copy.enabled)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	SkipassConfiguration& SkipassConfiguration::operator=(const SkipassConfiguration& copy) {
		id=copy.id;
		groupname=copy.groupname;
		name=copy.name;
		identifier_rulename=copy.identifier_rulename;
		service_rulename=copy.service_rulename;
		identifier_categoryname=copy.identifier_categoryname;
		ratename=copy.ratename;
		valid_from=copy.valid_from;
		valid_to=copy.valid_to;
		price=copy.price;
		make_transaction=copy.make_transaction;
		enabled=copy.enabled;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool SkipassConfiguration::operator==(const SkipassConfiguration& other) const {
		if( id!=other.id ) return false;
		return true;
	};
	bool SkipassConfiguration::operator<(const SkipassConfiguration& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		return false;
	};
	void SpdGateArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.name.Bind(name), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.host.Bind(host), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.port.Bind(port), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.enabled.Bind(enabled), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	SpdGateArc::SpdGateArc() : Domain("SpdGateArc", true), Domain_(*this) {
		pushColumns();
	};
	SpdGateArc::SpdGateArc(const SpdGateArc& copy) : Domain("SpdGateArc", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, name(copy.name)
		, host(copy.host)
		, port(copy.port)
		, enabled(copy.enabled)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	SpdGateArc& SpdGateArc::operator=(const SpdGateArc& copy) {
		id=copy.id;
		name=copy.name;
		host=copy.host;
		port=copy.port;
		enabled=copy.enabled;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool SpdGateArc::operator==(const SpdGateArc& other) const {
		if( id!=other.id ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool SpdGateArc::operator<(const SpdGateArc& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void SpdGate::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.name.Bind(name), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.host.Bind(host), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.port.Bind(port), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.enabled.Bind(enabled), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	SpdGate::SpdGate() : Domain("SpdGate", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new SpdGateArc());
	};
	SpdGate::SpdGate(Int __id) : Domain("SpdGate", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new SpdGateArc());
		id=__id;
	}
	SpdGate::SpdGate(const SpdGate& copy) : Domain("SpdGate", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, name(copy.name)
		, host(copy.host)
		, port(copy.port)
		, enabled(copy.enabled)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	SpdGate& SpdGate::operator=(const SpdGate& copy) {
		id=copy.id;
		name=copy.name;
		host=copy.host;
		port=copy.port;
		enabled=copy.enabled;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool SpdGate::operator==(const SpdGate& other) const {
		if( id!=other.id ) return false;
		return true;
	};
	bool SpdGate::operator<(const SpdGate& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		return false;
	};
	void PaidServiceCategoryArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.name.Bind(name), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.status.Bind(status), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	PaidServiceCategoryArc::PaidServiceCategoryArc() : Domain("PaidServiceCategoryArc", true), Domain_(*this) {
		pushColumns();
	};
	PaidServiceCategoryArc::PaidServiceCategoryArc(const PaidServiceCategoryArc& copy) : Domain("PaidServiceCategoryArc", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, name(copy.name)
		, status(copy.status)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	PaidServiceCategoryArc& PaidServiceCategoryArc::operator=(const PaidServiceCategoryArc& copy) {
		id=copy.id;
		name=copy.name;
		status=copy.status;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool PaidServiceCategoryArc::operator==(const PaidServiceCategoryArc& other) const {
		if( id!=other.id ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool PaidServiceCategoryArc::operator<(const PaidServiceCategoryArc& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void PaidServiceCategory::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.name.Bind(name), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.status.Bind(status), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	PaidServiceCategory::PaidServiceCategory() : Domain("PaidServiceCategory", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new PaidServiceCategoryArc());
	};
	PaidServiceCategory::PaidServiceCategory(Int __id) : Domain("PaidServiceCategory", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new PaidServiceCategoryArc());
		id=__id;
	}
	PaidServiceCategory::PaidServiceCategory(const PaidServiceCategory& copy) : Domain("PaidServiceCategory", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, name(copy.name)
		, status(copy.status)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	PaidServiceCategory& PaidServiceCategory::operator=(const PaidServiceCategory& copy) {
		id=copy.id;
		name=copy.name;
		status=copy.status;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool PaidServiceCategory::operator==(const PaidServiceCategory& other) const {
		if( id!=other.id ) return false;
		return true;
	};
	bool PaidServiceCategory::operator<(const PaidServiceCategory& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		return false;
	};
	void PaidServiceTariffArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.name.Bind(name), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.status.Bind(status), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	PaidServiceTariffArc::PaidServiceTariffArc() : Domain("PaidServiceTariffArc", true), Domain_(*this) {
		pushColumns();
	};
	PaidServiceTariffArc::PaidServiceTariffArc(const PaidServiceTariffArc& copy) : Domain("PaidServiceTariffArc", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, name(copy.name)
		, status(copy.status)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	PaidServiceTariffArc& PaidServiceTariffArc::operator=(const PaidServiceTariffArc& copy) {
		id=copy.id;
		name=copy.name;
		status=copy.status;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool PaidServiceTariffArc::operator==(const PaidServiceTariffArc& other) const {
		if( id!=other.id ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool PaidServiceTariffArc::operator<(const PaidServiceTariffArc& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void PaidServiceTariff::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.name.Bind(name), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.status.Bind(status), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	PaidServiceTariff::PaidServiceTariff() : Domain("PaidServiceTariff", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new PaidServiceTariffArc());
	};
	PaidServiceTariff::PaidServiceTariff(Int __id) : Domain("PaidServiceTariff", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new PaidServiceTariffArc());
		id=__id;
	}
	PaidServiceTariff::PaidServiceTariff(const PaidServiceTariff& copy) : Domain("PaidServiceTariff", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, name(copy.name)
		, status(copy.status)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	PaidServiceTariff& PaidServiceTariff::operator=(const PaidServiceTariff& copy) {
		id=copy.id;
		name=copy.name;
		status=copy.status;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool PaidServiceTariff::operator==(const PaidServiceTariff& other) const {
		if( id!=other.id ) return false;
		return true;
	};
	bool PaidServiceTariff::operator<(const PaidServiceTariff& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		return false;
	};
	void PaidServiceHourArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.name.Bind(name), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.status.Bind(status), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	PaidServiceHourArc::PaidServiceHourArc() : Domain("PaidServiceHourArc", true), Domain_(*this) {
		pushColumns();
	};
	PaidServiceHourArc::PaidServiceHourArc(const PaidServiceHourArc& copy) : Domain("PaidServiceHourArc", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, name(copy.name)
		, status(copy.status)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	PaidServiceHourArc& PaidServiceHourArc::operator=(const PaidServiceHourArc& copy) {
		id=copy.id;
		name=copy.name;
		status=copy.status;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool PaidServiceHourArc::operator==(const PaidServiceHourArc& other) const {
		if( id!=other.id ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool PaidServiceHourArc::operator<(const PaidServiceHourArc& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void PaidServiceHour::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.name.Bind(name), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.status.Bind(status), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	PaidServiceHour::PaidServiceHour() : Domain("PaidServiceHour", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new PaidServiceHourArc());
	};
	PaidServiceHour::PaidServiceHour(Int __id) : Domain("PaidServiceHour", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new PaidServiceHourArc());
		id=__id;
	}
	PaidServiceHour::PaidServiceHour(const PaidServiceHour& copy) : Domain("PaidServiceHour", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, name(copy.name)
		, status(copy.status)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	PaidServiceHour& PaidServiceHour::operator=(const PaidServiceHour& copy) {
		id=copy.id;
		name=copy.name;
		status=copy.status;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool PaidServiceHour::operator==(const PaidServiceHour& other) const {
		if( id!=other.id ) return false;
		return true;
	};
	bool PaidServiceHour::operator<(const PaidServiceHour& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		return false;
	};
	void PaidServiceArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.categoryid.Bind(categoryid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.name.Bind(name), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.tariffid.Bind(tariffid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.hourid.Bind(hourid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.description.Bind(description), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.price.Bind(price), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.status.Bind(status), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	PaidServiceArc::PaidServiceArc() : Domain("PaidServiceArc", true), Domain_(*this) {
		pushColumns();
	};
	PaidServiceArc::PaidServiceArc(const PaidServiceArc& copy) : Domain("PaidServiceArc", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, categoryid(copy.categoryid)
		, name(copy.name)
		, tariffid(copy.tariffid)
		, hourid(copy.hourid)
		, description(copy.description)
		, price(copy.price)
		, status(copy.status)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	PaidServiceArc& PaidServiceArc::operator=(const PaidServiceArc& copy) {
		id=copy.id;
		categoryid=copy.categoryid;
		name=copy.name;
		tariffid=copy.tariffid;
		hourid=copy.hourid;
		description=copy.description;
		price=copy.price;
		status=copy.status;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool PaidServiceArc::operator==(const PaidServiceArc& other) const {
		if( id!=other.id ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool PaidServiceArc::operator<(const PaidServiceArc& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void PaidService::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.categoryid.Bind(categoryid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.name.Bind(name), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.tariffid.Bind(tariffid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.hourid.Bind(hourid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.description.Bind(description), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.price.Bind(price), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.status.Bind(status), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	PaidService::PaidService() : Domain("PaidService", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new PaidServiceArc());
	};
	PaidService::PaidService(Int __id) : Domain("PaidService", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new PaidServiceArc());
		id=__id;
	}
	PaidService::PaidService(const PaidService& copy) : Domain("PaidService", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, categoryid(copy.categoryid)
		, name(copy.name)
		, tariffid(copy.tariffid)
		, hourid(copy.hourid)
		, description(copy.description)
		, price(copy.price)
		, status(copy.status)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	PaidService& PaidService::operator=(const PaidService& copy) {
		id=copy.id;
		categoryid=copy.categoryid;
		name=copy.name;
		tariffid=copy.tariffid;
		hourid=copy.hourid;
		description=copy.description;
		price=copy.price;
		status=copy.status;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool PaidService::operator==(const PaidService& other) const {
		if( id!=other.id ) return false;
		return true;
	};
	bool PaidService::operator<(const PaidService& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		return false;
	};
	void PartnerArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.name.Bind(name), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.status.Bind(status), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.legal_address.Bind(legal_address), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.post_address.Bind(post_address), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.inn.Bind(inn), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.kpp.Bind(kpp), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.payment_account.Bind(payment_account), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.correspondent_account.Bind(correspondent_account), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.bank.Bind(bank), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.bik.Bind(bik), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.okpo.Bind(okpo), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.okato.Bind(okato), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.okved.Bind(okved), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ogrn.Bind(ogrn), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.principal.Bind(principal), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.phone.Bind(phone), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.fax.Bind(fax), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.email.Bind(email), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.token.Bind(token), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.notification_email.Bind(notification_email), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	PartnerArc::PartnerArc() : Domain("PartnerArc", true), Domain_(*this) {
		pushColumns();
	};
	PartnerArc::PartnerArc(const PartnerArc& copy) : Domain("PartnerArc", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, name(copy.name)
		, status(copy.status)
		, legal_address(copy.legal_address)
		, post_address(copy.post_address)
		, inn(copy.inn)
		, kpp(copy.kpp)
		, payment_account(copy.payment_account)
		, correspondent_account(copy.correspondent_account)
		, bank(copy.bank)
		, bik(copy.bik)
		, okpo(copy.okpo)
		, okato(copy.okato)
		, okved(copy.okved)
		, ogrn(copy.ogrn)
		, principal(copy.principal)
		, phone(copy.phone)
		, fax(copy.fax)
		, email(copy.email)
		, token(copy.token)
		, notification_email(copy.notification_email)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	PartnerArc& PartnerArc::operator=(const PartnerArc& copy) {
		id=copy.id;
		name=copy.name;
		status=copy.status;
		legal_address=copy.legal_address;
		post_address=copy.post_address;
		inn=copy.inn;
		kpp=copy.kpp;
		payment_account=copy.payment_account;
		correspondent_account=copy.correspondent_account;
		bank=copy.bank;
		bik=copy.bik;
		okpo=copy.okpo;
		okato=copy.okato;
		okved=copy.okved;
		ogrn=copy.ogrn;
		principal=copy.principal;
		phone=copy.phone;
		fax=copy.fax;
		email=copy.email;
		token=copy.token;
		notification_email=copy.notification_email;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool PartnerArc::operator==(const PartnerArc& other) const {
		if( id!=other.id ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool PartnerArc::operator<(const PartnerArc& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void Partner::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.name.Bind(name), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.status.Bind(status), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.legal_address.Bind(legal_address), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.post_address.Bind(post_address), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.inn.Bind(inn), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.kpp.Bind(kpp), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.payment_account.Bind(payment_account), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.correspondent_account.Bind(correspondent_account), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.bank.Bind(bank), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.bik.Bind(bik), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.okpo.Bind(okpo), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.okato.Bind(okato), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.okved.Bind(okved), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ogrn.Bind(ogrn), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.principal.Bind(principal), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.phone.Bind(phone), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.fax.Bind(fax), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.email.Bind(email), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.token.Bind(token), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.notification_email.Bind(notification_email), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	Partner::Partner() : Domain("Partner", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new PartnerArc());
	};
	Partner::Partner(Int __id) : Domain("Partner", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new PartnerArc());
		id=__id;
	}
	Partner::Partner(const Partner& copy) : Domain("Partner", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, name(copy.name)
		, status(copy.status)
		, legal_address(copy.legal_address)
		, post_address(copy.post_address)
		, inn(copy.inn)
		, kpp(copy.kpp)
		, payment_account(copy.payment_account)
		, correspondent_account(copy.correspondent_account)
		, bank(copy.bank)
		, bik(copy.bik)
		, okpo(copy.okpo)
		, okato(copy.okato)
		, okved(copy.okved)
		, ogrn(copy.ogrn)
		, principal(copy.principal)
		, phone(copy.phone)
		, fax(copy.fax)
		, email(copy.email)
		, token(copy.token)
		, notification_email(copy.notification_email)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	Partner& Partner::operator=(const Partner& copy) {
		id=copy.id;
		name=copy.name;
		status=copy.status;
		legal_address=copy.legal_address;
		post_address=copy.post_address;
		inn=copy.inn;
		kpp=copy.kpp;
		payment_account=copy.payment_account;
		correspondent_account=copy.correspondent_account;
		bank=copy.bank;
		bik=copy.bik;
		okpo=copy.okpo;
		okato=copy.okato;
		okved=copy.okved;
		ogrn=copy.ogrn;
		principal=copy.principal;
		phone=copy.phone;
		fax=copy.fax;
		email=copy.email;
		token=copy.token;
		notification_email=copy.notification_email;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool Partner::operator==(const Partner& other) const {
		if( id!=other.id ) return false;
		return true;
	};
	bool Partner::operator<(const Partner& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		return false;
	};
	void PartnerAdministratorArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.auserTokenID.Bind(auserTokenID), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.fullname.Bind(fullname), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	PartnerAdministratorArc::PartnerAdministratorArc() : Domain("PartnerAdministratorArc", true), Domain_(*this) {
		pushColumns();
	};
	PartnerAdministratorArc::PartnerAdministratorArc(const PartnerAdministratorArc& copy) : Domain("PartnerAdministratorArc", copy.isTenant_), Domain_(*this) 
		, auserTokenID(copy.auserTokenID)
		, fullname(copy.fullname)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	PartnerAdministratorArc& PartnerAdministratorArc::operator=(const PartnerAdministratorArc& copy) {
		auserTokenID=copy.auserTokenID;
		fullname=copy.fullname;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool PartnerAdministratorArc::operator==(const PartnerAdministratorArc& other) const {
		if( auserTokenID!=other.auserTokenID ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool PartnerAdministratorArc::operator<(const PartnerAdministratorArc& other) const {
		if( auserTokenID<other.auserTokenID ) return true;
		if( auserTokenID>other.auserTokenID ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void PartnerAdministrator::pushColumns() {
		columns_.push_back(ColSpec(Domain_.auserTokenID.Bind(auserTokenID), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.fullname.Bind(fullname), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	PartnerAdministrator::PartnerAdministrator() : Domain("PartnerAdministrator", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new PartnerAdministratorArc());
	};
	PartnerAdministrator::PartnerAdministrator(Int64 __auserTokenID) : Domain("PartnerAdministrator", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new PartnerAdministratorArc());
		auserTokenID=__auserTokenID;
	}
	PartnerAdministrator::PartnerAdministrator(const PartnerAdministrator& copy) : Domain("PartnerAdministrator", copy.isTenant_), Domain_(*this) 
		, auserTokenID(copy.auserTokenID)
		, fullname(copy.fullname)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	PartnerAdministrator& PartnerAdministrator::operator=(const PartnerAdministrator& copy) {
		auserTokenID=copy.auserTokenID;
		fullname=copy.fullname;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool PartnerAdministrator::operator==(const PartnerAdministrator& other) const {
		if( auserTokenID!=other.auserTokenID ) return false;
		return true;
	};
	bool PartnerAdministrator::operator<(const PartnerAdministrator& other) const {
		if( auserTokenID<other.auserTokenID ) return true;
		if( auserTokenID>other.auserTokenID ) return false;
		return false;
	};
	void PartnerManagerArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.auserTokenID.Bind(auserTokenID), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.partnerid.Bind(partnerid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.fullname.Bind(fullname), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.passport_number.Bind(passport_number), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.issue_date.Bind(issue_date), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.issued_by.Bind(issued_by), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.department_code.Bind(department_code), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.phone.Bind(phone), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	PartnerManagerArc::PartnerManagerArc() : Domain("PartnerManagerArc", true), Domain_(*this) {
		pushColumns();
	};
	PartnerManagerArc::PartnerManagerArc(const PartnerManagerArc& copy) : Domain("PartnerManagerArc", copy.isTenant_), Domain_(*this) 
		, auserTokenID(copy.auserTokenID)
		, partnerid(copy.partnerid)
		, fullname(copy.fullname)
		, passport_number(copy.passport_number)
		, issue_date(copy.issue_date)
		, issued_by(copy.issued_by)
		, department_code(copy.department_code)
		, phone(copy.phone)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	PartnerManagerArc& PartnerManagerArc::operator=(const PartnerManagerArc& copy) {
		auserTokenID=copy.auserTokenID;
		partnerid=copy.partnerid;
		fullname=copy.fullname;
		passport_number=copy.passport_number;
		issue_date=copy.issue_date;
		issued_by=copy.issued_by;
		department_code=copy.department_code;
		phone=copy.phone;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool PartnerManagerArc::operator==(const PartnerManagerArc& other) const {
		if( auserTokenID!=other.auserTokenID ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool PartnerManagerArc::operator<(const PartnerManagerArc& other) const {
		if( auserTokenID<other.auserTokenID ) return true;
		if( auserTokenID>other.auserTokenID ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void PartnerManager::pushColumns() {
		columns_.push_back(ColSpec(Domain_.auserTokenID.Bind(auserTokenID), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.partnerid.Bind(partnerid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.fullname.Bind(fullname), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.passport_number.Bind(passport_number), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.issue_date.Bind(issue_date), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.issued_by.Bind(issued_by), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.department_code.Bind(department_code), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.phone.Bind(phone), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	PartnerManager::PartnerManager() : Domain("PartnerManager", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new PartnerManagerArc());
	};
	PartnerManager::PartnerManager(Int64 __auserTokenID) : Domain("PartnerManager", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new PartnerManagerArc());
		auserTokenID=__auserTokenID;
	}
	PartnerManager::PartnerManager(const PartnerManager& copy) : Domain("PartnerManager", copy.isTenant_), Domain_(*this) 
		, auserTokenID(copy.auserTokenID)
		, partnerid(copy.partnerid)
		, fullname(copy.fullname)
		, passport_number(copy.passport_number)
		, issue_date(copy.issue_date)
		, issued_by(copy.issued_by)
		, department_code(copy.department_code)
		, phone(copy.phone)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	PartnerManager& PartnerManager::operator=(const PartnerManager& copy) {
		auserTokenID=copy.auserTokenID;
		partnerid=copy.partnerid;
		fullname=copy.fullname;
		passport_number=copy.passport_number;
		issue_date=copy.issue_date;
		issued_by=copy.issued_by;
		department_code=copy.department_code;
		phone=copy.phone;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool PartnerManager::operator==(const PartnerManager& other) const {
		if( auserTokenID!=other.auserTokenID ) return false;
		return true;
	};
	bool PartnerManager::operator<(const PartnerManager& other) const {
		if( auserTokenID<other.auserTokenID ) return true;
		if( auserTokenID>other.auserTokenID ) return false;
		return false;
	};
	void PartnerContractArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.partnerid.Bind(partnerid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.barcode.Bind(barcode), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.start_date.Bind(start_date), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.end_date.Bind(end_date), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.benefit_type.Bind(benefit_type), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.benefit_amount.Bind(benefit_amount), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	PartnerContractArc::PartnerContractArc() : Domain("PartnerContractArc", true), Domain_(*this) {
		pushColumns();
	};
	PartnerContractArc::PartnerContractArc(const PartnerContractArc& copy) : Domain("PartnerContractArc", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, partnerid(copy.partnerid)
		, barcode(copy.barcode)
		, start_date(copy.start_date)
		, end_date(copy.end_date)
		, benefit_type(copy.benefit_type)
		, benefit_amount(copy.benefit_amount)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	PartnerContractArc& PartnerContractArc::operator=(const PartnerContractArc& copy) {
		id=copy.id;
		partnerid=copy.partnerid;
		barcode=copy.barcode;
		start_date=copy.start_date;
		end_date=copy.end_date;
		benefit_type=copy.benefit_type;
		benefit_amount=copy.benefit_amount;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool PartnerContractArc::operator==(const PartnerContractArc& other) const {
		if( id!=other.id ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool PartnerContractArc::operator<(const PartnerContractArc& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void PartnerContract::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.partnerid.Bind(partnerid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.barcode.Bind(barcode), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.start_date.Bind(start_date), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.end_date.Bind(end_date), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.benefit_type.Bind(benefit_type), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.benefit_amount.Bind(benefit_amount), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	PartnerContract::PartnerContract() : Domain("PartnerContract", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new PartnerContractArc());
	};
	PartnerContract::PartnerContract(Int __id) : Domain("PartnerContract", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new PartnerContractArc());
		id=__id;
	}
	PartnerContract::PartnerContract(const PartnerContract& copy) : Domain("PartnerContract", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, partnerid(copy.partnerid)
		, barcode(copy.barcode)
		, start_date(copy.start_date)
		, end_date(copy.end_date)
		, benefit_type(copy.benefit_type)
		, benefit_amount(copy.benefit_amount)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	PartnerContract& PartnerContract::operator=(const PartnerContract& copy) {
		id=copy.id;
		partnerid=copy.partnerid;
		barcode=copy.barcode;
		start_date=copy.start_date;
		end_date=copy.end_date;
		benefit_type=copy.benefit_type;
		benefit_amount=copy.benefit_amount;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool PartnerContract::operator==(const PartnerContract& other) const {
		if( id!=other.id ) return false;
		return true;
	};
	bool PartnerContract::operator<(const PartnerContract& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		return false;
	};
	void PartnerAccountArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.partnerid.Bind(partnerid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.name.Bind(name), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.atype.Bind(atype), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.amount.Bind(amount), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.status.Bind(status), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	PartnerAccountArc::PartnerAccountArc() : Domain("PartnerAccountArc", true), Domain_(*this) {
		pushColumns();
	};
	PartnerAccountArc::PartnerAccountArc(const PartnerAccountArc& copy) : Domain("PartnerAccountArc", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, partnerid(copy.partnerid)
		, name(copy.name)
		, atype(copy.atype)
		, amount(copy.amount)
		, status(copy.status)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	PartnerAccountArc& PartnerAccountArc::operator=(const PartnerAccountArc& copy) {
		id=copy.id;
		partnerid=copy.partnerid;
		name=copy.name;
		atype=copy.atype;
		amount=copy.amount;
		status=copy.status;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool PartnerAccountArc::operator==(const PartnerAccountArc& other) const {
		if( id!=other.id ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool PartnerAccountArc::operator<(const PartnerAccountArc& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void PartnerAccount::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.partnerid.Bind(partnerid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.name.Bind(name), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.atype.Bind(atype), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.amount.Bind(amount), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.status.Bind(status), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	PartnerAccount::PartnerAccount() : Domain("PartnerAccount", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new PartnerAccountArc());
	};
	PartnerAccount::PartnerAccount(Int __id) : Domain("PartnerAccount", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new PartnerAccountArc());
		id=__id;
	}
	PartnerAccount::PartnerAccount(const PartnerAccount& copy) : Domain("PartnerAccount", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, partnerid(copy.partnerid)
		, name(copy.name)
		, atype(copy.atype)
		, amount(copy.amount)
		, status(copy.status)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	PartnerAccount& PartnerAccount::operator=(const PartnerAccount& copy) {
		id=copy.id;
		partnerid=copy.partnerid;
		name=copy.name;
		atype=copy.atype;
		amount=copy.amount;
		status=copy.status;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool PartnerAccount::operator==(const PartnerAccount& other) const {
		if( id!=other.id ) return false;
		return true;
	};
	bool PartnerAccount::operator<(const PartnerAccount& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		return false;
	};
	void AccountTransactionArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.accountid.Bind(accountid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.amount.Bind(amount), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.cache_flow.Bind(cache_flow), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.reason.Bind(reason), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	AccountTransactionArc::AccountTransactionArc() : Domain("AccountTransactionArc", true), Domain_(*this) {
		pushColumns();
	};
	AccountTransactionArc::AccountTransactionArc(const AccountTransactionArc& copy) : Domain("AccountTransactionArc", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, accountid(copy.accountid)
		, amount(copy.amount)
		, cache_flow(copy.cache_flow)
		, reason(copy.reason)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	AccountTransactionArc& AccountTransactionArc::operator=(const AccountTransactionArc& copy) {
		id=copy.id;
		accountid=copy.accountid;
		amount=copy.amount;
		cache_flow=copy.cache_flow;
		reason=copy.reason;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool AccountTransactionArc::operator==(const AccountTransactionArc& other) const {
		if( id!=other.id ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool AccountTransactionArc::operator<(const AccountTransactionArc& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void AccountTransaction::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.accountid.Bind(accountid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.amount.Bind(amount), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.cache_flow.Bind(cache_flow), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.reason.Bind(reason), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	AccountTransaction::AccountTransaction() : Domain("AccountTransaction", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new AccountTransactionArc());
	};
	AccountTransaction::AccountTransaction(Int __id) : Domain("AccountTransaction", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new AccountTransactionArc());
		id=__id;
	}
	AccountTransaction::AccountTransaction(const AccountTransaction& copy) : Domain("AccountTransaction", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, accountid(copy.accountid)
		, amount(copy.amount)
		, cache_flow(copy.cache_flow)
		, reason(copy.reason)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	AccountTransaction& AccountTransaction::operator=(const AccountTransaction& copy) {
		id=copy.id;
		accountid=copy.accountid;
		amount=copy.amount;
		cache_flow=copy.cache_flow;
		reason=copy.reason;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool AccountTransaction::operator==(const AccountTransaction& other) const {
		if( id!=other.id ) return false;
		return true;
	};
	bool AccountTransaction::operator<(const AccountTransaction& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		return false;
	};
	void PartnerWeekTariffGroupArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.partnerid.Bind(partnerid), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.tariffgroupid.Bind(tariffgroupid), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	PartnerWeekTariffGroupArc::PartnerWeekTariffGroupArc() : Domain("PartnerWeekTariffGroupArc", true), Domain_(*this) {
		pushColumns();
	};
	PartnerWeekTariffGroupArc::PartnerWeekTariffGroupArc(const PartnerWeekTariffGroupArc& copy) : Domain("PartnerWeekTariffGroupArc", copy.isTenant_), Domain_(*this) 
		, partnerid(copy.partnerid)
		, tariffgroupid(copy.tariffgroupid)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	PartnerWeekTariffGroupArc& PartnerWeekTariffGroupArc::operator=(const PartnerWeekTariffGroupArc& copy) {
		partnerid=copy.partnerid;
		tariffgroupid=copy.tariffgroupid;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool PartnerWeekTariffGroupArc::operator==(const PartnerWeekTariffGroupArc& other) const {
		if( partnerid!=other.partnerid ) return false;
		if( tariffgroupid!=other.tariffgroupid ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool PartnerWeekTariffGroupArc::operator<(const PartnerWeekTariffGroupArc& other) const {
		if( partnerid<other.partnerid ) return true;
		if( partnerid>other.partnerid ) return false;
		if( tariffgroupid<other.tariffgroupid ) return true;
		if( tariffgroupid>other.tariffgroupid ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void PartnerWeekTariffGroup::pushColumns() {
		columns_.push_back(ColSpec(Domain_.partnerid.Bind(partnerid), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.tariffgroupid.Bind(tariffgroupid), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	PartnerWeekTariffGroup::PartnerWeekTariffGroup() : Domain("PartnerWeekTariffGroup", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new PartnerWeekTariffGroupArc());
	};
	PartnerWeekTariffGroup::PartnerWeekTariffGroup(Int __partnerid, Int __tariffgroupid) : Domain("PartnerWeekTariffGroup", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new PartnerWeekTariffGroupArc());
		partnerid=__partnerid;
		tariffgroupid=__tariffgroupid;
	}
	PartnerWeekTariffGroup::PartnerWeekTariffGroup(const PartnerWeekTariffGroup& copy) : Domain("PartnerWeekTariffGroup", copy.isTenant_), Domain_(*this) 
		, partnerid(copy.partnerid)
		, tariffgroupid(copy.tariffgroupid)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	PartnerWeekTariffGroup& PartnerWeekTariffGroup::operator=(const PartnerWeekTariffGroup& copy) {
		partnerid=copy.partnerid;
		tariffgroupid=copy.tariffgroupid;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool PartnerWeekTariffGroup::operator==(const PartnerWeekTariffGroup& other) const {
		if( partnerid!=other.partnerid ) return false;
		if( tariffgroupid!=other.tariffgroupid ) return false;
		return true;
	};
	bool PartnerWeekTariffGroup::operator<(const PartnerWeekTariffGroup& other) const {
		if( partnerid<other.partnerid ) return true;
		if( partnerid>other.partnerid ) return false;
		if( tariffgroupid<other.tariffgroupid ) return true;
		if( tariffgroupid>other.tariffgroupid ) return false;
		return false;
	};
	void PartnerOrderArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.partnerid.Bind(partnerid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.initial_cost.Bind(initial_cost), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.discount.Bind(discount), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.total_cost.Bind(total_cost), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.week_tariffid.Bind(week_tariffid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	PartnerOrderArc::PartnerOrderArc() : Domain("PartnerOrderArc", true), Domain_(*this) {
		pushColumns();
	};
	PartnerOrderArc::PartnerOrderArc(const PartnerOrderArc& copy) : Domain("PartnerOrderArc", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, partnerid(copy.partnerid)
		, initial_cost(copy.initial_cost)
		, discount(copy.discount)
		, total_cost(copy.total_cost)
		, week_tariffid(copy.week_tariffid)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	PartnerOrderArc& PartnerOrderArc::operator=(const PartnerOrderArc& copy) {
		id=copy.id;
		partnerid=copy.partnerid;
		initial_cost=copy.initial_cost;
		discount=copy.discount;
		total_cost=copy.total_cost;
		week_tariffid=copy.week_tariffid;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool PartnerOrderArc::operator==(const PartnerOrderArc& other) const {
		if( id!=other.id ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool PartnerOrderArc::operator<(const PartnerOrderArc& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void PartnerOrder::pushColumns() {
		columns_.push_back(ColSpec(Domain_.id.Bind(id), COLUMN_SK));
		columns_.push_back(ColSpec(Domain_.partnerid.Bind(partnerid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.initial_cost.Bind(initial_cost), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.discount.Bind(discount), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.total_cost.Bind(total_cost), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.week_tariffid.Bind(week_tariffid), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	PartnerOrder::PartnerOrder() : Domain("PartnerOrder", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new PartnerOrderArc());
	};
	PartnerOrder::PartnerOrder(Int __id) : Domain("PartnerOrder", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new PartnerOrderArc());
		id=__id;
	}
	PartnerOrder::PartnerOrder(const PartnerOrder& copy) : Domain("PartnerOrder", copy.isTenant_), Domain_(*this) 
		, id(copy.id)
		, partnerid(copy.partnerid)
		, initial_cost(copy.initial_cost)
		, discount(copy.discount)
		, total_cost(copy.total_cost)
		, week_tariffid(copy.week_tariffid)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	PartnerOrder& PartnerOrder::operator=(const PartnerOrder& copy) {
		id=copy.id;
		partnerid=copy.partnerid;
		initial_cost=copy.initial_cost;
		discount=copy.discount;
		total_cost=copy.total_cost;
		week_tariffid=copy.week_tariffid;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool PartnerOrder::operator==(const PartnerOrder& other) const {
		if( id!=other.id ) return false;
		return true;
	};
	bool PartnerOrder::operator<(const PartnerOrder& other) const {
		if( id<other.id ) return true;
		if( id>other.id ) return false;
		return false;
	};
	void PartnerOrderlineArc::pushColumns() {
		columns_.push_back(ColSpec(Domain_.orderid.Bind(orderid), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.identifierid.Bind(identifierid), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.price.Bind(price), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.service_rulename.Bind(service_rulename), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.ArcType.Bind(ArcType), COLUMN_ARC_TYPE));
	};
	PartnerOrderlineArc::PartnerOrderlineArc() : Domain("PartnerOrderlineArc", true), Domain_(*this) {
		pushColumns();
	};
	PartnerOrderlineArc::PartnerOrderlineArc(const PartnerOrderlineArc& copy) : Domain("PartnerOrderlineArc", copy.isTenant_), Domain_(*this) 
		, orderid(copy.orderid)
		, identifierid(copy.identifierid)
		, price(copy.price)
		, service_rulename(copy.service_rulename)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
		, ArcType(copy.ArcType)
	{
		pushColumns();
	};
	PartnerOrderlineArc& PartnerOrderlineArc::operator=(const PartnerOrderlineArc& copy) {
		orderid=copy.orderid;
		identifierid=copy.identifierid;
		price=copy.price;
		service_rulename=copy.service_rulename;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		ArcType=copy.ArcType;
		return *this;
	};
	bool PartnerOrderlineArc::operator==(const PartnerOrderlineArc& other) const {
		if( orderid!=other.orderid ) return false;
		if( identifierid!=other.identifierid ) return false;
		if( UserArc!=other.UserArc ) return false;
		if( DateArc!=other.DateArc ) return false;
		return true;
	};
	bool PartnerOrderlineArc::operator<(const PartnerOrderlineArc& other) const {
		if( orderid<other.orderid ) return true;
		if( orderid>other.orderid ) return false;
		if( identifierid<other.identifierid ) return true;
		if( identifierid>other.identifierid ) return false;
		if( UserArc<other.UserArc ) return true;
		if( UserArc>other.UserArc ) return false;
		if( DateArc<other.DateArc ) return true;
		if( DateArc>other.DateArc ) return false;
		return false;
	};
	void PartnerOrderline::pushColumns() {
		columns_.push_back(ColSpec(Domain_.orderid.Bind(orderid), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.identifierid.Bind(identifierid), COLUMN_PK));
		columns_.push_back(ColSpec(Domain_.price.Bind(price), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.service_rulename.Bind(service_rulename), COLUMN_ORDINARY));
		columns_.push_back(ColSpec(Domain_.UserArc.Bind(UserArc), COLUMN_USER_ARC));
		columns_.push_back(ColSpec(Domain_.DateArc.Bind(DateArc), COLUMN_DATE_ARC));
		columns_.push_back(ColSpec(Domain_.TenantID.Bind(TenantID), COLUMN_ORDINARY));
	};
	PartnerOrderline::PartnerOrderline() : Domain("PartnerOrderline", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new PartnerOrderlineArc());
	};
	PartnerOrderline::PartnerOrderline(Int __orderid, Int __identifierid) : Domain("PartnerOrderline", true), Domain_(*this) {
		pushColumns();
		AttachArchive(new PartnerOrderlineArc());
		orderid=__orderid;
		identifierid=__identifierid;
	}
	PartnerOrderline::PartnerOrderline(const PartnerOrderline& copy) : Domain("PartnerOrderline", copy.isTenant_), Domain_(*this) 
		, orderid(copy.orderid)
		, identifierid(copy.identifierid)
		, price(copy.price)
		, service_rulename(copy.service_rulename)
		, UserArc(copy.UserArc)
		, DateArc(copy.DateArc)
		, TenantID(copy.TenantID)
	{
		pushColumns();
	};
	PartnerOrderline& PartnerOrderline::operator=(const PartnerOrderline& copy) {
		orderid=copy.orderid;
		identifierid=copy.identifierid;
		price=copy.price;
		service_rulename=copy.service_rulename;
		UserArc=copy.UserArc;
		DateArc=copy.DateArc;
		TenantID=copy.TenantID;
		return *this;
	};
	bool PartnerOrderline::operator==(const PartnerOrderline& other) const {
		if( orderid!=other.orderid ) return false;
		if( identifierid!=other.identifierid ) return false;
		return true;
	};
	bool PartnerOrderline::operator<(const PartnerOrderline& other) const {
		if( orderid<other.orderid ) return true;
		if( orderid>other.orderid ) return false;
		if( identifierid<other.identifierid ) return true;
		if( identifierid>other.identifierid ) return false;
		return false;
	};
} // namespace BALOON
#endif
