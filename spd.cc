#include "baloon.h"
#include "spd.h"
#include <iostream>
#include <string>
#include <algorithm>
#include <openssl/md5.h>
#include <boost/regex.hpp>
#include <boost/algorithm/string.hpp>
#include <curl/curl.h>
#include <interfaces/ahttp.h>
#include <aparser/aparser.h>
#include <acommon/acode.h>
#include <cryptographer/evp.h>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/c_local_time_adjustor.hpp>
#include <boost/date_time/local_time/local_time.hpp>

namespace BALOON {

using namespace Effi;
using namespace std;


string timeToSpdString(Time time) {
    boost::posix_time::time_facet* facet = new boost::posix_time::time_facet();
    facet->format("%Y-%m-%dT%H:%M:%S");
    stringstream stream;
    stream.imbue(std::locale(std::locale::classic(), facet));

//////////////////////////////////////////////////////////////////////////////////////////////////
//    boost::posix_time::time_duration offset = get_utc_offset();
//////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////
    boost::posix_time::time_duration offset(0,0,0);
//////////////////////////////////////////////////////////////////////////////////////////////////

    stream << time << (offset.is_negative() ? "-" : "+") 
            << setfill('0') << setw(2) << offset.hours() << ":" 
            << setfill('0') << setw(2) << offset.minutes();

    return stream.str();
}

enum class time_format {
    HUMAN, CONTOUR
};

Time parseTime(const string& in, const char* const format = "%Y-%m-%dT%H:%M:%S%ZP") {
    istringstream ss(in);
    ss.exceptions(ios_base::failbit);
    boost::local_time::local_time_input_facet *facet = new boost::local_time::local_time_input_facet(format);
    ss.imbue(locale(ss.getloc(), facet));
    Time out(boost::local_time::not_a_date_time);
    ss >> out;
    return out;
}

XMLNode SpdTransaction::serialize() const {
    XMLNode tr("transaction");
    if (!eid.empty()) tr.AddAttribute("eid", eid);
    if (!iid.empty()) tr.AddAttribute("iid", eid);
    if (!currency.empty()) tr.AddAttribute("currency", currency);
    Decimal summ_ = summ;
    tr.AddAttribute("summ", summ_.Trunc(2).ToString());
    if (!comment.empty()) tr.AddAttribute("comment", comment);
    if (!date.is_not_a_date_time()) tr.AddAttribute("date", timeToSpdString( date /*- get_utc_offset()*/ ));
    if (!IsNull(cost) && cost > 0) {
        Decimal cost_ = cost;
        tr.AddAttribute("cost", cost_.Trunc(2).ToString());
    }
    return tr;
}

SpdTransaction SpdTransaction::parse(XMLNode node) {
    SpdTransaction transaction;
    transaction.currency = node.Attribute("currency");
    transaction.summ = node.Attribute("summ");
    transaction.eid = node.Attribute("eid");
    transaction.iid = node.Attribute("iid");
    transaction.comment = node.Attribute("comment");
    transaction.date = parseTime(node.Attribute("date"));
    Log(5) << "  SpdTransaction::parse currency=" << transaction.currency << ", summ="<< transaction.summ << endl;
    return transaction;
}

XMLNode SpdPackage::serialize() const {
    XMLNode node("package");
    if (!identifier.empty()) node.AddAttribute("identifier", identifier);
    if (!rule_service.empty()) node.AddAttribute("rule_service", rule_service);
    string eid_ = (eid.empty() ? to_upper(Effi::GenerateGUID()) : eid);
    if (!comment.empty()) node.AddAttribute("comment", comment);
    node.AddAttribute("eid", eid_);
    if (!IsNull(cost) && cost > 0) {
        Decimal cost_ = cost;
        node.AddAttribute("cost", cost_.Trunc(2).ToString());
    }
    return node;
}

SpdPackage SpdPackage::parse(XMLNode node) {
    SpdPackage package;
    package.identifier = node.Attribute("identifier");
    package.rule_service = node.Attribute("rule_service");
    package.eid = node.Attribute("eid");
    package.comment = node.Attribute("comment");

    package.state = node.Attribute("state");
    package.date = parseTime(node.Attribute("date")) - get_utc_offset();
    package.valid_end = parseTime(node.Attribute("valid_end")) - get_utc_offset();
    package.duration = node.Attribute("duration");
    package.type = node.Attribute("type");
    package.rule_use = node.Attribute("rule_use");
    package.tariff = node.Attribute("tariff");
    package.description = node.Attribute("description");
    string use_count = node.Attribute("use_count"),
            used_count = node.Attribute("used_count"),
            use_start = node.Attribute("use_start"),
            use_end = node.Attribute("use_end");
    if (!use_count.empty()) package.use_count = boost::lexical_cast<int32_t>(use_count);
    if (!used_count.empty()) package.used_count = boost::lexical_cast<int32_t>(used_count);
    if (!use_start.empty()) package.use_start = parseTime(node.Attribute("use_start")) - get_utc_offset();
    if (!use_end.empty()) package.use_end = parseTime(node.Attribute("use_end")) - get_utc_offset();
    Log(5) << "  SpdPackage::parse description=" << package.description << ", state=" << package.state << ", eid=" << package.rule_service << endl;
    return package;
}

XMLNode SpdIdentifier::serialize() const {
    XMLNode node("identifier");
    node.AddAttribute("code", to_upper(code));
    if(!value.empty()) {
        node.AddAttribute("value", to_upper(value));
    }
    if (is_new) {
        node.AddAttribute("new", "True");
        node.AddAttribute("value", to_upper(code));
        string eid_ = (eid.empty() ? to_upper(Effi::GenerateGUID()) : eid);
        node.AddAttribute("eid", eid_);
    }
    if (!valid_from.is_not_a_date_time()) node.AddAttribute("valid_from", timeToSpdString(valid_from /* + get_utc_offset()*/ ));
    if (!valid_to.is_not_a_date_time()) node.AddAttribute("valid_to", timeToSpdString(valid_to /* + get_utc_offset()*/ ));
    if (!permanent_rule.empty()) node.AddAttribute("permanent_rule", permanent_rule);
    if (!category.empty()) node.AddAttribute("category", category);
    if (!tariff.empty()) node.AddAttribute("tariff", tariff);
    if (cancel) node.AddAttribute("cancel", "True");
    if (!comment.empty()) node.AddAttribute("comment", comment);
    if (!IsNull(cost) && cost > 0) {
        Decimal cost_ = cost;
        node.AddAttribute("cost", cost_.Trunc(2).ToString());
    }
    if(calc_time_cost_get) {
        XMLNode calc_time_cost_node("calc_time_cost");
        calc_time_cost_node.AddAttribute("do_pay", calc_time_cost_pay? "True" : "False");
        calc_time_cost_node.AddAttribute("eid", Effi::GenerateGUID());
        node.AddAttribute("info", "True");
        node.AddChild(calc_time_cost_node);
    }
    return node;
}

SpdIdentifier SpdIdentifier::parse(XMLNode node) {
    SpdIdentifier identifier;
    identifier.code = node.Attribute("code");
    identifier.value = node.Attribute("value");
    identifier.valid_from = parseTime(node.Attribute("valid_from")) - get_utc_offset();
    identifier.valid_to = parseTime(node.Attribute("valid_to")) - get_utc_offset();
    identifier.permanent_rule = node.Attribute("permanent_rule");
    identifier.category = node.Attribute("category");
    identifier.tariff = node.Attribute("tariff");
    
    Log(5) << "  SpdIdentifier::parse code=" << identifier.code << ", valid_from=" << identifier.valid_from << ", valid_to=" << identifier.valid_to << endl;

    XMLNodeSet pkgs = node.FindNodes("//package");
    const auto size = pkgs.Size();
    for (unsigned int i = 0; i < size; i++) {
        XMLNode node = pkgs[i];
        SpdPackage package = SpdPackage::parse(node);
        identifier.packages.push_back(package);
    }
    
    const auto calc_time_cost_node = node.FindFirst("calc_time_cost");
    if(!calc_time_cost_node.Empty()) {
        const auto territory_node = calc_time_cost_node.FindFirst("territory");
        identifier.territory = territory_node.Empty()? string() : territory_node.Text();
        
        const auto time_node = calc_time_cost_node.FindFirst("time");
        if(!time_node.Empty()) {
            const char* const format = "%d.%m.%Y %H:%M";
            identifier.start_time = parseTime(time_node.Attribute("start"), format) - get_utc_offset(); 	//????
            identifier.end_time = parseTime(time_node.Attribute("end"), format) - get_utc_offset();		//????
            identifier.pay = Decimal(time_node.Attribute("pay"));
        }
        const auto nopay_node = calc_time_cost_node.FindFirst("nopay");
        if(!nopay_node.Empty()) {
            identifier.comment = nopay_node.Content();
        }
    }
    return identifier;
}

SpdAccountCurrency SpdAccountCurrency::parse(XMLNode node) {
    SpdAccountCurrency currency;
    currency.currency = node.Attribute("name");
    currency.balance = node.Attribute("value");
    Log(5) << "    SpdAccountCurrency::parse currency=" << currency.currency << ", balance=" << currency.balance << endl;
    return currency;
}

SpdAccount SpdAccount::parse(XMLNode node) {
    SpdAccount account;
    if (node.Attribute("valid") == "True") account.valid = true;
    Log(5) << "  SpdAccount::parse valid=" << account.valid << endl;
    XMLNodeSet cs = node.FindNodes("currency");
    for (unsigned int i=0; i<cs.Size(); i++) {
        account.currencies.push_back(SpdAccountCurrency::parse(cs[i]));
    }
    return account;
}

XMLNode SpdClient::serialize(Time from, Time to) const {
    XMLNode client("client");
    if (!oid.empty()) client.AddAttribute("oid", oid);
    else if (!identifier.empty()) client.AddAttribute("identifier", to_upper(identifier));
    if (is_new) client.AddAttribute("new", "True");
    if (!eid.empty()) client.AddAttribute("eid", eid);

    if (transactions.size()>0 || !from.is_not_a_date_time() || !to.is_not_a_date_time()) {
        XMLNode trx = client.AddChild("transactions");
        if (!from.is_not_a_date_time()) trx.AddAttribute("from", timeToSpdString(from /*+ get_utc_offset()*/));
        if (!to.is_not_a_date_time()) trx.AddAttribute("to", timeToSpdString(to /*+ get_utc_offset()*/));

        vector<SpdTransaction>::const_iterator it = transactions.begin();
        for (; it!=transactions.end(); it++) {
            XMLNode tx = it->serialize();
            trx.AddChild(tx);
        }
    }
    XMLNode ids = client.AddChild("identifiers");
    vector<SpdIdentifier>::const_iterator it = identifiers.begin();
    for (; it!=identifiers.end(); it++) {
        XMLNode ident = it->serialize();
        ids.AddChild(ident);
    }
    vector<SpdPackage>::const_iterator itp = packages.begin();
    for (; itp!=packages.end(); itp++) {
        XMLNode pkg = itp->serialize();
        ids.AddChild(pkg);
    }
    return client;
}

SpdClient SpdClient::parse(XMLNode node) {
    SpdClient client;
    XMLNodeSet attributes = node.FindNodes("/attributes/attribute");
    for (unsigned int i=0; i<attributes.Size(); i++) {
        string attrName = attributes[i].Attribute("name"),
               value = attributes[i].Attribute("value");
        Log(5) << "  ** " << attrName << "=" << value << endl;
        if (attrName == "lname") client.last_name = value;
        else if (attrName == "fname") client.first_name = value;
        else if (attrName == "mname") client.middle_name = value;
        else if (attrName == "phone_number") client.phone = value;
        else if (attrName == "date_born") {
            vector<string> dd;
            boost::split(dd, value, boost::is_any_of("-"));
            int32_t y = boost::lexical_cast<int32_t>(dd[0]),
                    m = boost::lexical_cast<int32_t>(dd[1]),
                    d = boost::lexical_cast<int32_t>(dd[2]);
            if (y < 1900) continue;
            client.birthdate = ADate(y, m, d);
        }
    }
    client.oid = node.Attribute("oid");
    client.eid = node.Attribute("eid");

    Log(5) << "SpdClient::parse " << client.last_name << " " << client.first_name << " " << client.middle_name << ", oid=" << client.oid 
            << "\n  content: " << node.Content() << endl;

    XMLNodeSet identifiers = node.FindNodes("//identifiers/identifier");
    for (unsigned int i=0; i<identifiers.Size(); i++) {
        XMLNode node = identifiers[i];
        SpdIdentifier identifier = SpdIdentifier::parse(node);
        client.identifiers.push_back(identifier);
    }

    XMLNodeSet transactions = node.FindNodes("//transactions/transaction");
    for (unsigned int i=0; i<transactions.Size(); i++) {
        XMLNode node = transactions[i];
        SpdTransaction transaction = SpdTransaction::parse(node);
        client.transactions.push_back(transaction);
    }

    XMLNodeSet accounts = node.FindNodes("//account");
    if (0 < accounts.Size()) client.account = SpdAccount::parse(accounts[0]);

    return client;
}

void LogAttributes(XMLNode node) {
    XMLAttrList attrs = node.Attributes();
    XMLAttr attr = attrs.First();
    if (!attr.Empty()) Log(5) << "  attr: " << attr.Name() << "=" << attr.Value();
    while (!(attr = attrs.Next()).Empty()) {
        if (attr.Name() !="CODE" && attr.Name() != "COMMENT" && attr.Name() != "NAME" && attr.Name() != "ID")
        Log(5) << ", " << attr.Name() << "=" << attr.Value();
    }
    Log(5) << endl;
}

SpdDictionaryRecord SpdDictionaryRecord::parse(XMLNode node) {
    LogAttributes(node);
    int id = boost::lexical_cast<int>(node.Attribute("ID"));
    string name = node.Attribute("NAME");
    SpdDictionaryRecord rec(id, name);
    if (node.AttributeExists("CODE")) rec.code = boost::lexical_cast<int>(node.Attribute("CODE"));
    rec.comment = node.Attribute("COMMENT");
    Log(5) << "  " << id << " (" << rec.code << ") " << name << ", comment=" << rec.comment << endl;
    return rec;
}

map<string, vector<SpdDictionaryRecord> > SpdDictionaryRecord::parse(XMLDoc xml) {
    map<string, vector<SpdDictionaryRecord> > dicts;
    XMLNodeSet nodes = xml.FindNodes("//dictionary");
    for (unsigned int i=0; i<nodes.Size(); i++) {
        string name = nodes[i].Attribute("name");
        Log(5) << "DICT " << name << endl;
        XMLNodeSet recs = nodes[i].FindNodes("//rec");
        for (unsigned int j=0; j<recs.Size(); j++) {
            XMLNode node = recs[j];
            SpdDictionaryRecord rec = SpdDictionaryRecord::parse(node);
            dicts[name].push_back(rec);
        }
    }
    return dicts;
}

SpdServiceRuleRecord SpdServiceRuleRecord::parse(XMLNode node) {
    //LogAttributes(node);
    SpdServiceRuleRecord rec;
    rec.id = boost::lexical_cast<int>(node.Attribute("ID"));
    rec.name = node.Attribute("NAME");
    if (node.AttributeExists("CODE")) {
        rec.code = boost::lexical_cast<int>(node.Attribute("CODE"));
    }
    rec.comment = node.Attribute("COMMENT");
    const auto rule = node.FindFirst("rule");
    const auto cost_ = (rule.Empty()? "0.0" : rule.Attribute("cost"));
    rec.cost = Decimal(cost_);
    Log(5) << "  " << rec.id << " (" << rec.code << ") " << rec.name << ", cost = " << rec.cost << ", comment=" << rec.comment << endl;
    return rec;
}

vector<SpdServiceRuleRecord> SpdServiceRuleRecord::parse(const string& reply) {
    XMLDoc xml;
    const auto* reply_cstr = reply.c_str();
    xml.Parse(reply_cstr, ::strlen(reply_cstr));
    vector<SpdServiceRuleRecord> res;
    auto dict = xml.FindFirst("//dictionary[@name='Правила оформления']");
    if(dict.Empty()) {
        return res;
    }
    auto recs = dict.FindNodes("records/rec");
    const auto size = recs.Size();
    for(uint i = 0; i < size; ++i) {
        const auto rec_node = recs[i];
        const auto service_rule_record = SpdServiceRuleRecord::parse(rec_node);
        res.push_back(service_rule_record);
    }
    return res;
}

string SpdRequest::serialize(bool request_transactions) const {

    string rtmp;
#ifdef USE_UTF_8
    rtmp =
    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
    "<spd-xml-api>"
    "  <request version=\"1.0\">"
    "  </request>"
    "</spd-xml-api>";
#else
    rtmp =
    "<?xml version=\"1.0\" encoding=\"Windows-1251\"?>"
    "<spd-xml-api>"
    "  <request version=\"1.0\">"
    "  </request>"
    "</spd-xml-api>";
#endif

    XMLDoc reqDoc;
    reqDoc.Parse(rtmp.data(), rtmp.size());
    XMLNode root = reqDoc.Root();
    XMLNode req = root.FindFirst("request");
    root.AddAttribute("ruid", Effi::GenerateGUID());
    req.AddAttribute("ruid", Effi::GenerateGUID());
    Time from_ = from, 
        to_ = to;
    if (request_transactions) {
        from_=boost::posix_time::time_from_string("1991-01-01 00:00:00");
        to_=TIME_NOW;
    }

    vector<SpdClient>::const_iterator it = clients.begin();
    for (; it!=clients.end(); it++) {
        req.AddChild(it->serialize(from_, to_));
    }
    string data = reqDoc.Serialize();
    return data;
}

void checkSpdErrors(string data) {
    if (data.substr(0, 5) == "<html") {
        string token;
        stringstream ss(data);
        bool found_error = false;
        while (std::getline(ss, token)) {
            if (found_error) throw Exception(token);
            if (token.find("SPD$EXCEPTION.") != std::string::npos) {
                found_error = true;
                continue;
            }
        }
        return;
    }

    XMLDoc xml;
    xml.Parse(data.data(), data.size());
    XMLNodeSet errors = xml.FindNodes("//client/error");
    if (errors.Size() > 0) {
        Exception e(Message("System error: ").What());
        for (unsigned int i=0; i<errors.Size(); i++) {
            Log(4) << "Error response: " << errors[i].ToString() << endl;
            if (errors[i].Attribute("param_name") == "oid") {
                e << errors[i].Attribute("message") << ". ";
            }
        }
        throw e;
    }
}

void ExtractSpdError(const string& data) {
    const char* htmltag("<html>");
    const int HTMLTAGLEN(6);
    if(data.substr(0, HTMLTAGLEN) != htmltag) {
        return;
    }
    istringstream in(data);
    string line;
    while(true) {
        if(in.eof()) {
            break;
        }
        const char* tag("<h1>");
        const int TAGLEN(4);
        
        std::getline(in, line, '\n');
        if(line.substr(0, TAGLEN) == tag) {
           for(int i = 3; i >= 0; --i) {
               std::getline(in, line, '\n');
           }
           throw Exception(line.c_str());
        }
    }
}

SpdResponse SpdResponse::parse(string data) {
    ExtractSpdError(data);
    XMLDoc xml;
    xml.Parse(data.data(), data.size());
    SpdResponse response(xml);
    XMLNodeSet nodes = xml.FindNodes("//client");
    for (unsigned int i=0; i<nodes.Size(); i++) {
        response.clients.push_back(SpdClient::parse(nodes[i]));
    }
    response.dictionaries = SpdDictionaryRecord::parse(xml);
    return response;
}

const string SendSpdRequest(string data, Str host, Str port) {
    const string STANDARD_SPD_PORT("33322");
    string spd_host;
    int spd_port;
    if(Defined(host)) {
        spd_host = *host;
        const auto spd_port_str = port.get_value_or(STANDARD_SPD_PORT);
        spd_port = boost::lexical_cast<int>(spd_port_str);
    }
    else {
        spd_host = MainConfig->Get("SPD-XML-API/host").get_value_or("");
        const auto spd_port_str = MainConfig->Get("SPD-XML-API/port").get_value_or(STANDARD_SPD_PORT);
        spd_port = boost::lexical_cast<int>(spd_port_str);
    }

    Str certfile = MainConfig->Get("SPD-XML-API/certfile");
    Log(5) << "Effi::HTTPConnection " << spd_host << ":" << spd_port << ", certfile=" << certfile << endl;
    
    Effi::HTTPConnection http(spd_host.c_str(), spd_port, (Defined(certfile) ? certfile.get_value_or("").c_str() : 0));
#ifdef USE_UTF_8
    /// Добавить заголовок для контура (по умолчанию заголовк не пишется)
    http.AddHeader("Content-Encoding","UTF-8");
    http.AddHeader("Content-Type","text/xml; charset=UTF-8");
#else
    /// Использовать кодировку 1251, по умолчанию в заголовке можно не указывать кодировку
#endif
    http.SendPOST("/spd-xml-api", data);
    string reply = http.Reply();
    Log(5) << "HTTPConnection::Query(POST):\n=== BEGIN ===\n" << data << "\n=== END ===" << endl;
    Log(5) << "HTTPConnection::Reply:\n=== BEGIN ===\n" << reply << "\n=== END ===" << endl;
    return reply;
}

SpdResponse SendSpdRequest(const SpdRequest &request, bool request_transactions) {
    string data = request.serialize(request_transactions);
    Log(0) << "REQUEST: " << data << endl;

#ifdef USE_UTF_8    
    string reply = SendSpdRequest(data, request.spd_host, request.spd_port);
#else
    const auto data_ = Computerica::utf8_to_win1251_dyn(data);
    string reply = SendSpdRequest(data_, request.spd_host, request.spd_port);
#endif
    
    Log(0) << "RESPONSE: " << reply << endl;
    SpdResponse response = SpdResponse::parse(reply);
    return response;
}

SpdResponse SendSpdRequest(const SpdClient &client, bool request_transactions) {
    SpdRequest request;
    
    request.spd_host = client.spd_host;
    request.spd_port = client.spd_port;
    
    request.clients.push_back(client);
    return SendSpdRequest(request, request_transactions);
}

} // namespace BALOON
