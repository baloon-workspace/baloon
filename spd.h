#ifndef _SPD_AUX_H_
#define _SPD_AUX_H_

#include "baloon.h"
#include "config.h"
#include <aparser/aparser.h>
#include <extrafuncs.h>

namespace BALOON {

using namespace Computerica;

class SpdDoc {

};

class SpdTransaction {
public:
    SpdTransaction() {}

public:
    string currency;
    Decimal summ;
    string eid, iid;
    string comment;
    Time date;
    Decimal cost;

    XMLNode serialize() const;
    static SpdTransaction parse(XMLNode node);
};

class SpdPackage {
public:
    SpdPackage() {}

public:
    std::string identifier; 
    std::string eid; 
    std::string rule_service;
    std::string comment;
    std::string state;
    std::string duration;
    std::string type;
    std::string rule_use;
    std::string tariff;
    std::string description;
    Effi::Time date;
    Effi::Time valid_end;
    Effi::Time use_start;
    Effi::Time use_end;
    int32_t use_count;
    int32_t used_count;
    Effi::Decimal cost;
    
    Effi::XMLNode serialize() const;
    static SpdPackage parse(Effi::XMLNode node);
};

class SpdIdentifier {
public:
    SpdIdentifier() : is_new(false), cancel(false), calc_time_cost_get(false), calc_time_cost_pay(false) {}

public:
    bool is_new;
    bool cancel;
    bool calc_time_cost_get;
    bool calc_time_cost_pay;
    std::string code;
    std::string value;
    std::string eid;
    std::string permanent_rule;
    std::string category;
    std::string tariff;
    std::string comment;
    std::string territory;
    Effi::Time valid_from;
    Effi::Time valid_to;
    Effi::Time start_time;
    Effi::Time end_time;
    std::vector<SpdPackage> packages; // for parse only to request already ordered packages
    Effi::Decimal cost;
    Effi::Decimal pay;

    Effi::XMLNode serialize() const ;
    static SpdIdentifier parse(Effi::XMLNode node);
};

class SpdAccountCurrency {
public:
    SpdAccountCurrency() {}

public:
    std::string currency;
    Effi::Decimal balance;

    static SpdAccountCurrency parse(XMLNode node);
};

class SpdAccount {
public:
    SpdAccount() : valid(false) {}

public:
    bool valid;
    std::vector<SpdAccountCurrency> currencies;

    static SpdAccount parse(Effi::XMLNode node);
};


class SpdClient {
public:
    SpdClient(bool new_=false) : is_new(new_) {
        eid = to_upper(Effi::GenerateGUID());
    }

public:
    bool is_new;
    string oid, eid, identifier;
    vector<SpdTransaction> transactions;
    vector<SpdIdentifier> identifiers;
    vector<SpdPackage> packages; // serialize only to setup new packages
    SpdAccount account;
    string last_name, first_name, middle_name, phone;
    ADate birthdate;

    XMLNode serialize(Time from=boost::posix_time::time_from_string("1991-01-01 00:00:00"), Time to=TIME_NOW) const;
    static SpdClient parse(XMLNode node);
public:
    Effi::Str spd_host;
    Effi::Str spd_port;
};

class SpdDictionaryRecord {
public:
    SpdDictionaryRecord(int id_, string name_) : id(id_), name(name_) {}

    int id, code;
    string name, comment;

    static SpdDictionaryRecord parse(XMLNode node);
    static map<string, vector<SpdDictionaryRecord> > parse(XMLDoc xml);
};

class SpdServiceRuleRecord {
public:
    static SpdServiceRuleRecord parse(Effi::XMLNode node);
    static std::vector<SpdServiceRuleRecord> parse(const std::string& xml);
public:
    int id;
    int code;
    std::string name;
    std::string comment;
    Effi::Decimal cost;
};

class SpdRequest {
public:
    SpdRequest() {}

public:
    vector<SpdClient> clients;
    Time from, to;

    string serialize(bool request_transactions=false) const;
public:
    Effi::Str spd_host;
    Effi::Str spd_port;
};

class SpdResponse {
private:
    SpdResponse(XMLDoc xml) : xml_(xml) {}
public:
    vector<SpdClient> clients;
    map<string, vector<SpdDictionaryRecord> > dictionaries;
    Time from, to;
    XMLDoc xml_;

    static SpdResponse parse(string data);
};


const string SendSpdRequest(string data, Str host = Str(), Str port = Str());
SpdResponse SendSpdRequest(const SpdRequest &request, bool request_transactions=false);
SpdResponse SendSpdRequest(const SpdClient &client, bool request_transactions=false);

// string formSpdRequest(
//         const string regid, 
//         Time from=boost::posix_time::time_from_string("1991-01-01 00:00:00"), 
//         Time to=TIME_NOW, 
//         const vector<Transaction> transactions = vector<Transaction>());

/*
class SpdParser {
public:
    SpdParser(string reply) {
        xml_.Parse(reply.data(), reply.size());
    }

    void checkErrors();
    Person getPerson();

    void parseAndInsertIdentifiers(Connection *rdb, const Int personid);
    void parseAndInsertAccounts(Connection *rdb, const Int personid);
    void parseAndInsertTransactions(Connection *rdb, const Int personid);

private:
    XMLDoc xml_;
};
*/

} // namespace BALOON

#endif
