#!/bin/bash
EFFI_ROOT=/usr/local/effi
REPO_ROOT=/usr/local/reporter
COMPUTERICA_ROOT=/usr/local/computerica

DIR_NAME=`dirname "$0"`

VER=`cat "$DIR_NAME"/version`
EFFI_VER=`cat $EFFI_ROOT/version `
REPO_VER=`cat $REPO_ROOT/version `
COMPUTERICA_VER=`cat $COMPUTERICA_ROOT/version `
DEPFILE="$DIR_NAME/effi-version."`echo -n $VER | sed -re 's/-[0-9]+$//'`
REPO_DEPFILE="$DIR_NAME/reporter-version."`echo -n $VER | sed -re 's/-[0-9d]+$//'`
COMPUTERICA_DEPFILE="$DIR_NAME/computerica-version."`echo -n $VER | sed -re 's/-[0-9d]+$//'`
PATCHER="/usr/local/effi/util/patch.pl"
if ! [ -r CONFIG ] ; then
        cp CONFIG.example CONFIG
fi

RCONF="$EFFI_ROOT/bin/readconf CONFIG"

export DATABASE="MYSQL"
export DBPATCHER_PATCHDIR="$DIR_NAME/dbpatches"

export DB_BASE="`$RCONF DBConnection/Database`"
export DB_USER="`$RCONF DBConnection/UserName`"
export DB_PASS="`$RCONF DBConnection/Password`"
export DB_HOST="`$RCONF DBConnection/Host`"

mysql -u$DB_USER << EOF
	create database IF NOT EXISTS $DB_BASE default character set utf8;
EOF

#CONTAINERS=`xmllint --xpath "//PSSOptions/section[@id='Baloon']/section/@id" CONFIG | tr " " "\n" | awk -F \" '{print $2}'`

$PATCHER --register-module "" $VER
$PATCHER --register-module Baloon $VER
$PATCHER --register-module EFFI $EFFI_VER
$PATCHER --register-module EFFI/PSS $EFFI_VER
$PATCHER --register-module EFFI/Authorizer $EFFI_VER
$PATCHER --register-module EFFI/TaskMan $EFFI_VER
$PATCHER --register-module EFFI/multimedia $EFFI_VER
$PATCHER --register-module Reporter $REPO_VER
$PATCHER --register-module COMPUTERICA $COMPUTERICA_VER
$PATCHER --register-module COMPUTERICA/Mailbox $COMPUTERICA_VER
$PATCHER --register-module COMPUTERICA/Loyalty $COMPUTERICA_VER
$PATCHER --register-module COMPUTERICA/Basicsite $COMPUTERICA_VER
$PATCHER --register-module COMPUTERICA/Paygate $COMPUTERICA_VER
$PATCHER --register-module COMPUTERICA/Paygate/Uniteller $COMPUTERICA_VER
$PATCHER --register-module COMPUTERICA/Paygate/Sberbank $COMPUTERICA_VER
$PATCHER --register-module COMPUTERICA/Paygate/Paykeeper $COMPUTERICA_VER
$PATCHER --register-module COMPUTERICA/Fiscal $COMPUTERICA_VER
$PATCHER --register-module COMPUTERICA/Fiscal/AtolOnline $COMPUTERICA_VER

$PATCHER --dependency "EFFI,$DEPFILE,$EFFI_ROOT/dbpatches" --dependency Reporter,$REPO_DEPFILE,$REPO_ROOT/dbpatches --dependency "COMPUTERICA,$COMPUTERICA_DEPFILE,$COMPUTERICA_ROOT/dbpatches" $@

