ALTER TABLE IdentifierInvoice DROP FOREIGN KEY PersonInvoice_Invoice;

CREATE INDEX IdentifierInvoiceInvoice ON IdentifierInvoice(id);
ALTER TABLE IdentifierInvoice ADD CONSTRAINT IdentifierInvoice_Invoice FOREIGN KEY (id) REFERENCES Invoice (id);
