ALTER TABLE BaloonProvider ADD COLUMN  default_fiscalgateid INT COMMENT 'Gate ID';
ALTER TABLE BaloonProviderArc ADD COLUMN  default_fiscalgateid INT COMMENT 'Gate ID';

CREATE INDEX BaloonProviderdefault_fiscalgate ON BaloonProvider(default_fiscalgateid);
ALTER TABLE BaloonProvider ADD CONSTRAINT BaloonProvider_default_fiscalgate FOREIGN KEY (default_fiscalgateid) REFERENCES FiscalGate (id);

