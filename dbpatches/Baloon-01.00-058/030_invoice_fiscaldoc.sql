ALTER TABLE IdentifierInvoice ADD COLUMN fiscal_docid INT COMMENT 'Fiscal Document ID';
ALTER TABLE IdentifierInvoiceArc ADD COLUMN fiscal_docid INT COMMENT 'Fiscal Document ID';

CREATE INDEX IdentifierInvoicefiscal_doc ON IdentifierInvoice(fiscal_docid);
ALTER TABLE IdentifierInvoice ADD CONSTRAINT IdentifierInvoice_fiscal_doc FOREIGN KEY (fiscal_docid) REFERENCES FiscalDoc (id);
