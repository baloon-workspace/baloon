CREATE TABLE PersonOrderFiscalDoc (
    orderid INT NOT NULL COMMENT 'Order ID',
    fiscal_docid INT NOT NULL COMMENT 'Fiscal Document ID',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (orderid, fiscal_docid, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Fiscal Document';

CREATE TABLE PersonOrderFiscalDocArc (
    orderid INT COMMENT 'Order ID',
    fiscal_docid INT COMMENT 'Fiscal Document ID',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    ArcType VARCHAR(1) COMMENT 'ArcType',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (orderid, fiscal_docid, UserArc, DateArc, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit Fiscal Document';

CREATE INDEX PersonOrderFiscalDocorder ON PersonOrderFiscalDoc(orderid, TenantID);
ALTER TABLE PersonOrderFiscalDoc ADD CONSTRAINT PersonOrderFiscalDoc_order FOREIGN KEY (orderid, TenantID) REFERENCES PersonOrder (id, TenantID);

CREATE INDEX PersonOrderFiscalDocfiscal_doc ON PersonOrderFiscalDoc(fiscal_docid);
ALTER TABLE PersonOrderFiscalDoc ADD CONSTRAINT PersonOrderFiscalDoc_fiscal_doc FOREIGN KEY (fiscal_docid) REFERENCES FiscalDoc (id);

INSERT INTO PersonOrderFiscalDoc(orderid, fiscal_docid) SELECT id, fiscal_docid FROM PersonOrder WHERE fiscal_docid IS NOT NULL;
