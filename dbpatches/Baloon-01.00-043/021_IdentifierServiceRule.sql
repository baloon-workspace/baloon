CREATE TABLE IdentifierServiceRule (
    identifier_rulename VARCHAR(255) NOT NULL COMMENT 'Правило пользования',
    service_rulename VARCHAR(255) NOT NULL COMMENT 'Правило оформления',
    name VARCHAR(255) NOT NULL COMMENT 'Name',
    enabled VARCHAR(40) COMMENT 'Enabled',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (identifier_rulename, service_rulename, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Service Rule Accessibility Settings';

CREATE TABLE IdentifierServiceRuleArc (
    identifier_rulename VARCHAR(255) COMMENT 'Правило пользования',
    service_rulename VARCHAR(255) COMMENT 'Правило оформления',
    name VARCHAR(255) COMMENT 'Name',
    enabled VARCHAR(40) COMMENT 'Enabled',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    ArcType VARCHAR(1) COMMENT 'ArcType',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (identifier_rulename, service_rulename, UserArc, DateArc, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit Service Rule Accessibility Settings';

CREATE INDEX IdentifierServiceRuleidentifier_rule ON IdentifierServiceRule(identifier_rulename, TenantID);
ALTER TABLE IdentifierServiceRule ADD CONSTRAINT IdentifierServiceRule_identifier_rule FOREIGN KEY (identifier_rulename, TenantID) REFERENCES IdentifierRule (name, TenantID);

CREATE INDEX IdentifierServiceRuleservice_rule ON IdentifierServiceRule(service_rulename, TenantID);
ALTER TABLE IdentifierServiceRule ADD CONSTRAINT IdentifierServiceRule_service_rule FOREIGN KEY (service_rulename, TenantID) REFERENCES SpdServiceRule (name, TenantID);
