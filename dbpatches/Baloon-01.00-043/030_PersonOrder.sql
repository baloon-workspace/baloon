CREATE TABLE PersonOrder (
    id INT NOT NULL AUTO_INCREMENT COMMENT 'Order ID',
    personid INT COMMENT 'Person ID',
    filed_at DATETIME NOT NULL COMMENT 'Filed At',
    amount NUMERIC(60,30) NOT NULL COMMENT 'Amount',
    status VARCHAR(40) COMMENT 'Status',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (id, TenantID)
    , INDEX(id)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Order';

CREATE TABLE PersonOrderArc (
    id INT COMMENT 'Order ID',
    personid INT COMMENT 'Person ID',
    filed_at DATETIME COMMENT 'Filed At',
    amount NUMERIC(60,30) COMMENT 'Amount',
    status VARCHAR(40) COMMENT 'Status',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    ArcType VARCHAR(1) COMMENT 'ArcType',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (id, UserArc, DateArc, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit Order';

CREATE INDEX PersonOrderperson ON PersonOrder(personid, TenantID);
ALTER TABLE PersonOrder ADD CONSTRAINT PersonOrder_person FOREIGN KEY (personid, TenantID) REFERENCES Person (id, TenantID);
