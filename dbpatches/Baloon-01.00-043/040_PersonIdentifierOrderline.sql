CREATE TABLE PersonIdentifierOrderline (
    orderid INT NOT NULL COMMENT 'Order ID',
    identifierid INT NOT NULL COMMENT 'ID',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (orderid, identifierid, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Orderline';

CREATE TABLE PersonIdentifierOrderlineArc (
    orderid INT COMMENT 'Order ID',
    identifierid INT COMMENT 'ID',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    ArcType VARCHAR(1) COMMENT 'ArcType',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (orderid, identifierid, UserArc, DateArc, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit Orderline';

CREATE INDEX PersonIdentifierOrderlineorder ON PersonIdentifierOrderline(orderid, TenantID);
ALTER TABLE PersonIdentifierOrderline ADD CONSTRAINT PersonIdentifierOrderline_order FOREIGN KEY (orderid, TenantID) REFERENCES PersonOrder (id, TenantID);

CREATE INDEX PersonIdentifierOrderlineidentifier ON PersonIdentifierOrderline(identifierid, TenantID);
ALTER TABLE PersonIdentifierOrderline ADD CONSTRAINT PersonIdentifierOrderline_identifier FOREIGN KEY (identifierid, TenantID) REFERENCES Identifier (id, TenantID);
