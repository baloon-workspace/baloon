ALTER TABLE Agent ADD COLUMN is_default VARCHAR(5) COMMENT 'Is Default Seller';
ALTER TABLE AgentArc ADD COLUMN is_default VARCHAR(5) COMMENT 'Is Default Seller';
