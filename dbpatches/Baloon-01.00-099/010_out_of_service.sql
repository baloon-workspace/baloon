ALTER TABLE Holiday ADD COLUMN out_of_service VARCHAR(5) COMMENT 'Out Of Service';
ALTER TABLE HolidayArc ADD COLUMN out_of_service VARCHAR(5) COMMENT 'Out Of Service';