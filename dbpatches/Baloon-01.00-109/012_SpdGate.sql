CREATE TABLE SpdGate (
    id INT NOT NULL AUTO_INCREMENT COMMENT 'Gate ID',
    name VARCHAR(40) NOT NULL COMMENT 'Name',
    host VARCHAR(40) NOT NULL COMMENT 'Host',
    port VARCHAR(40) COMMENT 'Port',
    enabled VARCHAR(40) COMMENT 'Enabled',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (id, TenantID)
    , INDEX(id)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Spd Gate';

CREATE TABLE SpdGateArc (
    id INT COMMENT 'Gate ID',
    name VARCHAR(40) COMMENT 'Name',
    host VARCHAR(40) COMMENT 'Host',
    port VARCHAR(40) COMMENT 'Port',
    enabled VARCHAR(40) COMMENT 'Enabled',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    ArcType VARCHAR(1) COMMENT 'ArcType',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (id, UserArc, DateArc, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit Spd Gate';
