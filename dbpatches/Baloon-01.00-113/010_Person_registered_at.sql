ALTER TABLE Person ADD COLUMN registered_at DATETIME COMMENT 'Registration Date';
ALTER TABLE PersonArc ADD COLUMN registered_at DATETIME COMMENT 'Registration Date';