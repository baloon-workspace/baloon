ALTER TABLE PersonOrder ADD COLUMN promocode VARCHAR(40) COMMENT 'Promotion Code';
ALTER TABLE PersonOrder ADD COLUMN final_amount NUMERIC(60,30) COMMENT 'Final Amount';

ALTER TABLE PersonOrderArc ADD COLUMN promocode VARCHAR(40) COMMENT 'Promotion Code';
ALTER TABLE PersonOrderArc ADD COLUMN final_amount NUMERIC(60,30) COMMENT 'Final Amount';
