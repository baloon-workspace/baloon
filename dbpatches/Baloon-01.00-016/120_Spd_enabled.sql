UPDATE SpdServiceRule SET enabled=api_enabled;
UPDATE IdentifierRule SET enabled=api_enabled;
UPDATE IdentifierCategory SET enabled=api_enabled;
UPDATE IdentifierRate SET enabled=api_enabled;

ALTER TABLE SpdServiceRule DROP COLUMN api_enabled;
ALTER TABLE SpdServiceRuleArc DROP COLUMN api_enabled;
ALTER TABLE IdentifierRule DROP COLUMN api_enabled;
ALTER TABLE IdentifierRuleArc DROP COLUMN api_enabled;
ALTER TABLE IdentifierCategory DROP COLUMN api_enabled;
ALTER TABLE IdentifierCategoryArc DROP COLUMN api_enabled;
ALTER TABLE IdentifierRate DROP COLUMN api_enabled;
ALTER TABLE IdentifierRateArc DROP COLUMN api_enabled;

call AddPrivileges (100, 'Baloon','SpdServiceRule', 'SpdServiceRuleEnabledListGet',1,0,0,0,0,1);
call AddPrivileges (100, 'Baloon','IdentifierRule', 'IdentifierRuleEnabledListGet',1,0,0,0,0,1);
call AddPrivileges (100, 'Baloon','IdentifierCategory', 'IdentifierCategoryEnabledListGet',1,0,0,0,0,1);
call AddPrivileges (100, 'Baloon','IdentifierRate', 'IdentifierRateEnabledListGet',1,0,0,0,0,1);

call AddPrivileges (200, 'Baloon','SpdServiceRule', 'SpdServiceRuleEnabledListGet',1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon','IdentifierRule', 'IdentifierRuleEnabledListGet',1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon','IdentifierCategory', 'IdentifierCategoryEnabledListGet',1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon','IdentifierRate', 'IdentifierRateEnabledListGet',1,0,0,0,0,1);

call AddPrivileges (200, 'Baloon', 'IdentifierAgentOrder', 'CurrentGet', 1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon', 'IdentifierAgentOrder', 'CurrentAdd', 1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon', 'IdentifierAgentOrder', 'CurrentUpdate', 1,0,0,0,0,1);

