ALTER TABLE AgentOrder ADD COLUMN commission NUMERIC(60,30) NOT NULL COMMENT 'Commission amount';
ALTER TABLE AgentOrderArc ADD COLUMN commission NUMERIC(60,30) NOT NULL COMMENT 'Commission amount';

UPDATE AgentOrder SET commission=0;
