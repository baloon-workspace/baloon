
CREATE TABLE AgentOrder (
	id INT NOT NULL AUTO_INCREMENT COMMENT 'Refill ID',
	agentid INT NOT NULL COMMENT 'Agent ID',
	code VARCHAR(50) NOT NULL COMMENT 'Card Code',
	amount NUMERIC(60,30) NOT NULL COMMENT 'Amount',
	filed_at DATETIME NOT NULL COMMENT 'Filed at',
	comment TEXT COMMENT 'Comment',
	UserArc BIGINT COMMENT 'Modified by',
	DateArc DATETIME COMMENT 'Modification time',
	AgentOrderType VARCHAR(40) COMMENT 'AgentOrderType',
	TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
	PRIMARY KEY (id, TenantID)
	, INDEX(id)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Refill';

CREATE TABLE AgentOrderArc (
	id INT COMMENT 'Refill ID',
	agentid INT COMMENT 'Agent ID',
	code VARCHAR(50) COMMENT 'Card Code',
	amount NUMERIC(60,30) COMMENT 'Amount',
	filed_at DATETIME COMMENT 'Filed at',
	comment TEXT COMMENT 'Comment',
	UserArc BIGINT COMMENT 'Modified by',
	DateArc DATETIME COMMENT 'Modification time',
	AgentOrderType VARCHAR(40) COMMENT 'AgentOrderType',
	ArcType VARCHAR(1) COMMENT 'ArcType',
	TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
	PRIMARY KEY (id, UserArc, DateArc, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit Refill';

CREATE TABLE PackageAgentOrder (
	id INT NOT NULL COMMENT 'Refill ID',
	service_rulename VARCHAR(255) NOT NULL COMMENT 'Правило пользования',
	UserArc BIGINT COMMENT 'Modified by',
	DateArc DATETIME COMMENT 'Modification time',
	TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
	PRIMARY KEY (id, TenantID)
	, INDEX(id)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Package purchase';

CREATE TABLE PackageAgentOrderArc (
	id INT COMMENT 'Refill ID',
	service_rulename VARCHAR(255) COMMENT 'Правило пользования',
	UserArc BIGINT COMMENT 'Modified by',
	DateArc DATETIME COMMENT 'Modification time',
	ArcType VARCHAR(1) COMMENT 'ArcType',
	TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
	PRIMARY KEY (id, UserArc, DateArc, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit Package purchase';

CREATE INDEX AgentOrderagent ON AgentOrder(agentid, TenantID);
ALTER TABLE AgentOrder ADD CONSTRAINT AgentOrder_agent FOREIGN KEY (agentid, TenantID) REFERENCES Agent (id, TenantID);

ALTER TABLE PackageAgentOrder ADD CONSTRAINT PackageAgentOrder_Parent_ FOREIGN KEY (id, TenantID) REFERENCES AgentOrder (id, TenantID) ON DELETE CASCADE;

CREATE INDEX PackageAgentOrderservice_rule ON PackageAgentOrder(service_rulename, TenantID);
ALTER TABLE PackageAgentOrder ADD CONSTRAINT PackageAgentOrder_service_rule FOREIGN KEY (service_rulename, TenantID) REFERENCES SpdServiceRule (name, TenantID);


