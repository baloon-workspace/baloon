
CREATE TABLE SpdServiceRule (
	name VARCHAR(255) NOT NULL COMMENT 'Правило пользования',
	enabled VARCHAR(40) NOT NULL COMMENT 'Enabled',
	api_enabled VARCHAR(40) NOT NULL COMMENT 'Enabled for API',
	extid INT NOT NULL COMMENT 'External ID',
	extcode INT COMMENT 'External Code',
	comment TEXT COMMENT 'Comment',
	UserArc BIGINT COMMENT 'Modified by',
	DateArc DATETIME COMMENT 'Modification time',
	TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
	PRIMARY KEY (name, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Правила оформления';

CREATE TABLE SpdServiceRuleArc (
	name VARCHAR(255) COMMENT 'Правило пользования',
	enabled VARCHAR(40) COMMENT 'Enabled',
	api_enabled VARCHAR(40) COMMENT 'Enabled for API',
	extid INT COMMENT 'External ID',
	extcode INT COMMENT 'External Code',
	comment TEXT COMMENT 'Comment',
	UserArc BIGINT COMMENT 'Modified by',
	DateArc DATETIME COMMENT 'Modification time',
	ArcType VARCHAR(1) COMMENT 'ArcType',
	TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
	PRIMARY KEY (name, UserArc, DateArc, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit Правила оформления';


