ALTER TABLE BaloonProvider ADD COLUMN code_conversion_algo VARCHAR(40) COMMENT 'Card code conversion algorithm';
ALTER TABLE BaloonProvider ADD COLUMN agent_commission DOUBLE COMMENT 'Agents common commission, %';

ALTER TABLE BaloonProviderArc ADD COLUMN code_conversion_algo VARCHAR(40) COMMENT 'Card code conversion algorithm';
ALTER TABLE BaloonProviderArc ADD COLUMN agent_commission DOUBLE COMMENT 'Agents common commission, %';
