
CREATE TABLE IdentifierAgentOrder (
	id INT NOT NULL COMMENT 'Order ID',
	valid_from DATETIME COMMENT 'Valid from',
	valid_to DATETIME COMMENT 'Valid to',
	permanent_rulename VARCHAR(255) NOT NULL COMMENT 'Правило пользования',
	categoryname VARCHAR(255) COMMENT 'Название категории',
	ratename VARCHAR(255) COMMENT 'Тариф',
	other_card_code VARCHAR(50) COMMENT 'Other user card code',
	UserArc BIGINT COMMENT 'Modified by',
	DateArc DATETIME COMMENT 'Modification time',
	TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
	PRIMARY KEY (id, TenantID)
	, INDEX(id)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Identifier Order';

CREATE TABLE IdentifierAgentOrderArc (
	id INT COMMENT 'Order ID',
	valid_from DATETIME COMMENT 'Valid from',
	valid_to DATETIME COMMENT 'Valid to',
	permanent_rulename VARCHAR(255) COMMENT 'Правило пользования',
	categoryname VARCHAR(255) COMMENT 'Название категории',
	ratename VARCHAR(255) COMMENT 'Тариф',
	other_card_code VARCHAR(50) COMMENT 'Other user card code',
	UserArc BIGINT COMMENT 'Modified by',
	DateArc DATETIME COMMENT 'Modification time',
	ArcType VARCHAR(1) COMMENT 'ArcType',
	TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
	PRIMARY KEY (id, UserArc, DateArc, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit Identifier Order';


ALTER TABLE IdentifierAgentOrder ADD CONSTRAINT IdentifierAgentOrder_Parent_ FOREIGN KEY (id, TenantID) REFERENCES AgentOrder (id, TenantID) ON DELETE CASCADE;

CREATE INDEX IdentifierAgentOrderpermanent_rule ON IdentifierAgentOrder(permanent_rulename, TenantID);
ALTER TABLE IdentifierAgentOrder ADD CONSTRAINT IdentifierAgentOrder_permanent_rule FOREIGN KEY (permanent_rulename, TenantID) REFERENCES IdentifierRule (name, TenantID);

CREATE INDEX IdentifierAgentOrdercategory ON IdentifierAgentOrder(categoryname, TenantID);
ALTER TABLE IdentifierAgentOrder ADD CONSTRAINT IdentifierAgentOrder_category FOREIGN KEY (categoryname, TenantID) REFERENCES IdentifierCategory (name, TenantID);

CREATE INDEX IdentifierAgentOrderrate ON IdentifierAgentOrder(ratename, TenantID);
ALTER TABLE IdentifierAgentOrder ADD CONSTRAINT IdentifierAgentOrder_rate FOREIGN KEY (ratename, TenantID) REFERENCES IdentifierRate (name, TenantID);


