
ALTER TABLE SpdServiceRule ADD COLUMN package_cost NUMERIC(60,30) COMMENT 'Цена пакета';
ALTER TABLE SpdServiceRuleArc ADD COLUMN package_cost NUMERIC(60,30) COMMENT 'Цена пакета';

