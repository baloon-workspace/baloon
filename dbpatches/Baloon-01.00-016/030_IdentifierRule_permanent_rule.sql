
ALTER TABLE Identifier DROP FOREIGN KEY `Identifier_rule_use`;
ALTER TABLE Identifier DROP KEY `Identifierrule_use`;

ALTER TABLE Identifier CHANGE COLUMN rule_usename permanent_rulename VARCHAR(255) NOT NULL COMMENT 'Правило пользования';
ALTER TABLE IdentifierArc CHANGE COLUMN rule_usename permanent_rulename VARCHAR(255) COMMENT 'Правило пользования';

CREATE INDEX Identifierpermanent_rule ON Identifier(permanent_rulename, TenantID);
ALTER TABLE Identifier ADD CONSTRAINT Identifier_permanent_rule FOREIGN KEY (permanent_rulename, TenantID) REFERENCES IdentifierRule (name, TenantID);

