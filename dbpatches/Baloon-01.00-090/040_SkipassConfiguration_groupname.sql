ALTER TABLE SkipassConfiguration ADD COLUMN groupname VARCHAR(40) COMMENT 'Group Name';
ALTER TABLE SkipassConfigurationArc ADD COLUMN groupname VARCHAR(40) COMMENT 'Group Name';