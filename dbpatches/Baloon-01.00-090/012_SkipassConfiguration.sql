CREATE TABLE SkipassConfiguration (
    id INT NOT NULL AUTO_INCREMENT COMMENT 'Conofiguration ID',
    name VARCHAR(40) NOT NULL COMMENT 'External Name',
    identifier_rulename VARCHAR(255) NOT NULL COMMENT 'Правило пользования',
    service_rulename VARCHAR(255) COMMENT 'Правило оформления',
    identifier_categoryname VARCHAR(255) COMMENT 'Название категории',
    ratename VARCHAR(255) COMMENT 'Тариф',
    valid_from DATETIME NOT NULL COMMENT 'Valid From',
    valid_to DATETIME NOT NULL COMMENT 'Valid To',
    price NUMERIC(60,30) COMMENT 'Our Price',
    make_transaction VARCHAR(5) COMMENT 'With Spd Transaction',
    enabled VARCHAR(40) COMMENT 'Enabled',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (id, TenantID)
    , INDEX(id)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Skipass Configuration';

CREATE TABLE SkipassConfigurationArc (
    id INT COMMENT 'Conofiguration ID',
    name VARCHAR(40) COMMENT 'External Name',
    identifier_rulename VARCHAR(255) COMMENT 'Правило пользования',
    service_rulename VARCHAR(255) COMMENT 'Правило оформления',
    identifier_categoryname VARCHAR(255) COMMENT 'Название категории',
    ratename VARCHAR(255) COMMENT 'Тариф',
    valid_from DATETIME COMMENT 'Valid From',
    valid_to DATETIME COMMENT 'Valid To',
    price NUMERIC(60,30) COMMENT 'Our Price',
    make_transaction VARCHAR(5) COMMENT 'With Spd Transaction',
    enabled VARCHAR(40) COMMENT 'Enabled',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    ArcType VARCHAR(1) COMMENT 'ArcType',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (id, UserArc, DateArc, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit Skipass Configuration';

CREATE INDEX SkipassConfigurationidentifier_rule ON SkipassConfiguration(identifier_rulename, TenantID);
ALTER TABLE SkipassConfiguration ADD CONSTRAINT SkipassConfiguration_identifier_rule FOREIGN KEY (identifier_rulename, TenantID) REFERENCES IdentifierRule (name, TenantID);

CREATE INDEX SkipassConfigurationservice_rule ON SkipassConfiguration(service_rulename, TenantID);
ALTER TABLE SkipassConfiguration ADD CONSTRAINT SkipassConfiguration_service_rule FOREIGN KEY (service_rulename, TenantID) REFERENCES SpdServiceRule (name, TenantID);

CREATE INDEX SkipassConfigurationidentifier_category ON SkipassConfiguration(identifier_categoryname, TenantID);
ALTER TABLE SkipassConfiguration ADD CONSTRAINT SkipassConfiguration_identifier_category FOREIGN KEY (identifier_categoryname, TenantID) REFERENCES IdentifierCategory (name, TenantID);

CREATE INDEX SkipassConfigurationrate ON SkipassConfiguration(ratename, TenantID);
ALTER TABLE SkipassConfiguration ADD CONSTRAINT SkipassConfiguration_rate FOREIGN KEY (ratename, TenantID) REFERENCES IdentifierRate (name, TenantID);

