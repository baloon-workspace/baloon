ALTER TABLE IdentifierRule ADD COLUMN replenish_enabled VARCHAR(40) COMMENT 'Enabled';
ALTER TABLE IdentifierRuleArc ADD COLUMN replenish_enabled VARCHAR(40) COMMENT 'Enabled';
