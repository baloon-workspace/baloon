ALTER TABLE PersonIdentifierOrderline ADD COLUMN price NUMERIC(60,30) COMMENT 'Price';
ALTER TABLE PersonIdentifierOrderlineArc ADD COLUMN price NUMERIC(60,30) COMMENT 'Price';
