ALTER TABLE PersonOrder ADD COLUMN fiscal_docid INT COMMENT 'Fiscal Document ID';
ALTER TABLE PersonOrderArc ADD COLUMN fiscal_docid INT COMMENT 'Fiscal Document ID';
CREATE INDEX PersonOrderfiscal_doc ON PersonOrder(fiscal_docid);
ALTER TABLE PersonOrder ADD CONSTRAINT PersonOrder_fiscal_doc FOREIGN KEY (fiscal_docid) REFERENCES FiscalDoc (id);
