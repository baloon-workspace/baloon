
call AddPrivileges(-1, 'Baloon','*','*',1,1,1,1,1,1);


call AddPrivileges(-1, 'Baloon','Transaction','Add', 0,0,0,0,0,0);
call AddPrivileges(-1, 'Baloon','Transaction','Update', 0,0,0,0,0,0); 

call AddPrivileges(0, 'Baloon','Person','Register_API',1,0,0,0,0,1);
call AddPrivileges(0, 'Baloon','Person','CreateNewClient_API',1,0,0,0,0,1);
call AddPrivileges(0, 'Baloon','Person','ShowRegistration',1,0,0,0,0,1);
call AddPrivileges(0, 'Baloon','Person','ShowUserPanel',1,0,0,0,0,1);
call AddPrivileges(0, 'Baloon','Identifier','ShowConvertPrintCode',1,0,0,0,0,1);
call AddPrivileges(0, 'Baloon','Person','ShowTicketsShop',1,0,0,0,0,1);
call AddPrivileges(0, 'Baloon', 'Person', 'ShowSuccessPayment', 1,0,0,0,0,1);
call AddPrivileges(0, 'Baloon', 'Person', 'ShowFailedPayment', 1,0,0,0,0,1);
call AddPrivileges(0, 'Baloon', 'Person', 'ShowSuccessPaymentIOS', 1,0,0,0,0,1);
call AddPrivileges(0, 'Baloon', 'Person', 'ShowSuccessDownloadPayment', 1, 0,0,0,0,1);

call AddPrivileges(0, 'Basicsite','NewsArticle', 'LatestArticleListGet_FE',1,0,0,0,0,1);
call AddPrivileges(0, 'Basicsite','WeatherInfo', 'WeatherInfoGetLast_FE',1,0,0,0,0,1);

call AddPrivileges(0, 'Baloon','IdentifierServiceRule', 'IdentifierServiceRuleListGet_API',1,0,0,0,0,1);
call AddPrivileges(0, 'Baloon','PersonOrder', 'Place_API',1,0,0,0,0,1);
call AddPrivileges(0, 'Baloon','PersonOrder', 'GetPreliminaryOrderAmount_FE',1,0,0,0,0,1);
call AddPrivileges(0, 'Baloon','PersonInvoice', 'RaiseInvoice_FE', 1,0,0,0,0,1);
call AddPrivileges(0, 'Baloon','WeekTariff', 'SeansesByDateListGet_FE', 1,0,0,0,0,1);
call AddPrivileges(0, 'Baloon','WeekTariff', 'SeansesListGet_FE',1,0,0,0,0,1);
call AddPrivileges(0, 'Baloon','SkipassConfiguration', 'SkipassConfigurationListGet_API', 1,0,0,0,0,1);
call AddPrivileges(0, 'Baloon','SkipassConfiguration', 'SkipassConfigurationGroupNameListGet_API', 1,0,0,0,0,1);
call AddPrivileges(0, 'Baloon', 'PersonOrder', 'Place_FE', 1,0,0,0,0,1);
call AddPrivileges(0, 'Baloon', 'PersonOrder', 'Place_WEB', 1,0,0,0,0,1);
call AddPrivileges(0, 'Baloon', 'PersonOrder', 'CheckPromocode_API', 1,0,0,0,0,1);
call AddPrivileges(0, 'Baloon', 'PersonOrder', 'BuySkipass_API', 1,0,0,0,0,1);
call AddPrivileges(0, 'Baloon', 'PersonOrder', 'Download_FE', 1,0,0,0,0,1);

call AddPrivileges(0, 'Baloon','Advertisement', 'LatestArticleListGet_FE',1,0,0,0,0,1);
call AddPrivileges(0, 'Baloon','Advertisement', 'VisualItemListGet_FE',1,0,0,0,0,1);
call AddPrivileges(0, 'Baloon','Advertisement', 'VisualItemListGet_FE_1_13',1,0,0,0,0,1);

call AddPrivileges(0, 'Baloon','IdentifierRate', 'IdentifierRateListGet_API',1,0,0,0,0,1);
call AddPrivileges(0, 'Baloon','IdentifierCategory', 'IdentifierCategoryListGet_API',1,0,0,0,0,1);
call AddPrivileges(0, 'Baloon','ExternalResource', 'ExternalResourceListGet_API',1,0,0,0,0,1);

call AddPrivileges(0, 'Baloon', 'ParkingArea', 'ParkingAreaListGet_API', 1,0,0,0,0,1);
call AddPrivileges(0, 'Baloon', 'PersonOrder', 'ParkingOrderPlace_API', 1,0,0,0,0,1);
call AddPrivileges(0, 'Baloon', 'PersonOrder', 'ParkingIdentifierInfoGet_API', 1,0,0,0,0,1);
call AddPrivileges(0, 'Baloon', 'ParkingTimer', 'TimeLeftGet_API', 1,0,0,0,0,1);
call AddPrivileges(0, 'Baloon', 'ParkingTimer', 'Start_API', 1,0,0,0,0,1);

call AddPrivileges(0, 'Baloon', 'PartnerOrder', 'Place_API', 1,0,0,0,0,1);
call AddPrivileges(0, 'Baloon', 'WeekTariff', 'OurSeansesByDateListGet_API', 1,0,0,0,0,1);

INSERT INTO Token (TokenID, TokenName, TokenStatus, TokenType, TenantID) VALUES (1, 'Администрация', 'enabled', 'Groups', 1);
INSERT INTO Groups (TokenID, TenantID) VALUES (1, 1);

call AddPrivileges(1, 'Baloon','*','*',1,1,1,1,1,1);
call AddPrivileges(1, 'Reporter','*','*',1,1,1,1,1,1);
call AddPrivileges(1, 'TaskMan','*','*',1,1,1,1,1,1);
call AddPrivileges(1, 'Authorizer','*','*',1,1,1,1,1,1);
call AddPrivileges(1, 'Mailbox', '*', '*',1,1,1,1,1,1);

call AddPrivileges(1, 'Baloon','Transaction','Add', 0,0,0,0,0,0);
call AddPrivileges(1, 'Baloon','Transaction','Update', 0,0,0,0,0,0);

INSERT INTO Token (TokenID, TokenName, TokenStatus, TokenType, TenantID) VALUES (2, 'Администратор МКД', 'enabled', 'Groups', 1);
INSERT INTO Groups (TokenID, TenantID) VALUES (2, 1);

call AddPrivileges(2, 'Baloon', 'Partner', '*', 1,1,1,1,1,1);
call AddPrivileges(2, 'Baloon', 'PartnerManager', '*', 1,1,1,1,1,1);
call AddPrivileges(2, 'Baloon', 'PartnerContract', '*', 1,1,1,1,1,1);
call AddPrivileges(2, 'Baloon', 'PartnerAccount', '*', 1,1,1,1,1,1);
call AddPrivileges(2, 'Baloon', 'AccountTransaction', '*', 1,1,1,1,1,1);
call AddPrivileges(2, 'Mailbox', 'MainMenuGenerator', 'GetDynamicMenu',1,1,1,1,1,1);
call AddPrivileges(2, 'Baloon', 'PartnerAdministrator', 'GetCurrent', 1,0,0,0,0,1);
call AddPrivileges(2, 'Baloon', 'WeekTariffGroup', 'PartnerWeekTariffGroupListGet', 1,0,0,0,0,1);
call AddPrivileges(2, 'Baloon', 'WeekTariffGroup', 'ForPartnerAdd', 1,0,0,0,0,1);
call AddPrivileges(2, 'Baloon', 'WeekTariffGroup', 'ForPartnerUpdate', 1,0,0,0,0,1);
call AddPrivileges(2, 'Baloon', 'WeekTariffGroup', 'ForPartnerGet', 1,0,0,0,0,1);

call AddPrivileges(2, 'Baloon', 'SpdServiceRule', 'SpdServiceRuleListGet', 1,0,0,0,0,1);
call AddPrivileges(2, 'Baloon', 'IdentifierCategory', 'IdentifierCategoryListGet', 1,0,0,0,0,1);
call AddPrivileges(2, 'Baloon', 'IdentifierRate', 'IdentifierRateListGet', 1,0,0,0,0,1);
call AddPrivileges(2, 'Baloon', 'IdentifierRule', 'IdentifierRuleListGet', 1,0,0,0,0,1);

call AddPrivileges(2, 'Baloon', 'WeekTariff', 'MassAdd', 1,0,0,0,0,1);
call AddPrivileges(2, 'Baloon', 'WeekTariff', 'PartnerWeekTariffListGet', 1,0,0,0,0,1);
call AddPrivileges(2, 'Baloon', 'WeekTariffGroup', 'ForPartnerDelete', 1,0,0,0,0,1);
call AddPrivileges(2, 'Baloon', 'WeekTariffGroup', 'Get', 1,0,0,0,0,1);
call AddPrivileges(2, 'Baloon', 'WeekTariff', 'ForPartnerDelete', 1,0,0,0,0,1);
call AddPrivileges(2, 'Baloon', 'WeekTariff', 'Get', 1,0,0,0,0,1);
call AddPrivileges(2, 'Baloon', 'WeekTariff', 'Update', 1,0,0,0,0,1);

call AddPrivileges(2, 'Baloon', 'PartnerOrder', 'PartnerOrderListGet', 1,0,0,0,0,1);
call AddPrivileges(2, 'Baloon', 'PartnerOrder', 'Get', 1,0,0,0,0,1);
call AddPrivileges(2, 'Baloon', 'PartnerOrderline', 'PartnerOrderlineListGet', 1,0,0,0,0,1);
call AddPrivileges(2, 'Baloon', 'PartnerOrder', 'TicketsDownload', 1,0,0,0,0,1);

INSERT INTO Token (TokenID, TokenName, TokenStatus, TokenType, TenantID) VALUES (100, 'Клиенты', 'enabled', 'Groups', 1);
INSERT INTO Groups (TokenID, TenantID) VALUES (100, 1);

call AddPrivileges(100, 'Baloon','Person','GetCurrent',1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon','Person','UpdateCurrent',1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon','Person','ChangeCurrentPassword',1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon','Person','GetCurrentAccounts',1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon','IdentifierInvoice','RaiseInvoiceForCurrentUser',1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon','IdentifierInvoice','CurrentInvoicesGetList_FE',1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon','IdentifierInvoice','RaiseInvoice_FE',1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon','Identifier','CurrentIdentifierListGet_FE',1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon','Identifier','IdentifierListDistributedByAccountsGet_FE',1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon','Identifier','BalancedIdentifierListGet_FE',1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon','Identifier','GetDetails_API',1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon', 'Identifier', 'BindIdentifierToCurrentPerson_FE', 1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon','Identifier','GetBalance_FE',1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon','Identifier','IdentifierNameUpdate_FE', 1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon', 'Identifier', 'PackagesListGet_API', 1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon', 'Identifier', 'GenerateQRCode', 1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon', 'Identifier', 'MyPayedIdentifierListGet_FE', 1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon', 'Identifier', 'DownloadBarcode', 1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon', 'Identifier', 'GenerateBarcode', 1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon', 'Identifier', 'Cancel_FE', 1,0,0,0,0,1);

call AddPrivileges(100, 'Baloon','Transaction','CurrentTransactionListGet_FE',1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon', 'Transaction', 'IdentifierTransactionListGet_FE', 1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon', 'Transaction', 'MassSalePackages_FE', 1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon', 'Transaction', 'SalePackage_API', 1,0,0,0,0,1);

call AddPrivileges(100, 'Baloon','SpdServiceRule', 'SpdServiceRuleEnabledListGet',1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon','SpdServiceRule', 'SpdServiceRuleListGet_API',1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon','IdentifierRule', 'IdentifierRuleEnabledListGet',1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon','IdentifierCategory', 'IdentifierCategoryEnabledListGet',1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon','IdentifierRate', 'IdentifierRateEnabledListGet',1,0,0,0,0,1);
call AddPrivileges(100, 'Basicsite','NewsArticle', 'LatestArticleListGet_FE',1,0,0,0,0,1);
call AddPrivileges(100, 'Basicsite','WeatherInfo', 'WeatherInfoGetLast_FE',1,0,0,0,0,1);

call AddPrivileges(100, 'Baloon','IdentifierRate', 'IdentifierRateListGet_API',1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon','IdentifierCategory', 'IdentifierCategoryListGet_API',1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon','IdentifierServiceRule', 'IdentifierServiceRuleListGet_API',1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon','PersonOrder', 'Place_API',1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon','PersonOrder', 'MyPlace_API',1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon','PersonOrder', 'MyBuySkipass_API',1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon','PersonOrder', 'GetPreliminaryOrderAmount_FE',1,0,0,0,0,1);

call AddPrivileges(100, 'Baloon','PersonOrder', 'MyOrderListGet_FE',1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon','PersonOrder', 'MyDownload_FE',1,0,0,0,0,1);

call AddPrivileges(100, 'Baloon','PersonInvoice', 'RaiseInvoice_FE', 1,0,0,0,0,1);

call AddPrivileges(100, 'Baloon','Advertisement', 'LatestArticleListGet_FE',1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon','Advertisement', 'VisualItemListGet_FE',1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon','Advertisement', 'VisualItemListGet_FE_1_13',1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon', 'PersonOrder', 'RequestPaymentStatus', 1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon', 'PersonOrder', 'Place_FE', 1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon', 'PersonOrder', 'MyGetStatus_FE', 1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon', 'PersonOrder', 'MySyncStatus_FE', 1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon', 'PersonOrder', 'ParkingOrderPlace_API', 1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon', 'PersonOrder', 'ParkingIdentifierInfoGet_API', 1,0,0,0,0,1);

call AddPrivileges(100, 'Baloon','PersonIdentifierOrderline', 'PersonIdentifierOrderlineListGet_FE', 1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon','PersonIdentifierOrderline', 'MyOrderlineListGet_FE', 1,0,0,0,0,1);

call AddPrivileges(100, 'Baloon','WeekTariff', 'SeansesByDateListGet_FE', 1,0,0,0,0,1);

call AddPrivileges(100, 'Reporter', 'PublicApi', 'GetReport', 1, 1, 1, 1, 1, 1);
call AddPrivileges(100, 'Baloon','ExternalResource', 'ExternalResourceListGet_API',1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon','PaidServiceCategory', 'PaidServiceCategoryListGet_FE',1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon','PaidService', 'PaidServiceListGet_FE',1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon','PaidService', 'PaidServiceListGet_API',1,0,0,0,0,1);

call AddPrivileges(100, 'Baloon', 'PackageOrder', 'MyPackageListGet_API', 1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon', 'ParkingArea', 'ParkingAreaListGet_API', 1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon', 'ParkingTimer', 'TimeLeftGet_API', 1,0,0,0,0,1);
call AddPrivileges(100, 'Baloon', 'ParkingTimer', 'Start_API', 1,0,0,0,0,1);

INSERT INTO Token (TokenID, TokenName, TokenStatus, TokenType, TenantID) VALUES (200, 'Агенты', 'enabled', 'Groups', 1);
INSERT INTO Groups (TokenID, TenantID) VALUES (200, 1);

call AddPrivileges (200, 'Baloon', 'BaloonProvider', 'IdentifierPriceGet', 1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon', 'SpdServiceRule', 'Get', 1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon', 'IdentifierCategory', 'Get', 1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon', 'IdentifierRate', 'Get', 1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon', 'IdentifierRule', 'Get', 1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon', 'SpdServiceRule', 'SpdServiceRuleEnabledListGet',1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon', 'IdentifierRule', 'IdentifierRuleEnabledListGet',1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon', 'IdentifierCategory', 'IdentifierCategoryEnabledListGet',1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon', 'IdentifierRate', 'IdentifierRateEnabledListGet',1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon', 'Agent', 'GetCurrent', 1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon', 'AgentOrder', 'CurrentAgentOrderListGet', 1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon', 'AgentOrder', 'CurrentGet', 1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon', 'AgentOrder', 'CurrentAdd', 1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon', 'AgentOrder', 'CurrentUpdate', 1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon', 'IdentifierAgentOrder', 'CurrentGet', 1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon', 'IdentifierAgentOrder', 'CurrentAdd', 1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon', 'IdentifierAgentOrder', 'CurrentUpdate', 1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon', 'PackageAgentOrder', 'CurrentGet', 1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon', 'PackageAgentOrder', 'CurrentAdd', 1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon', 'PackageAgentOrder', 'CurrentUpdate', 1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon', 'Reports', 'CurrentAgentSalesReportListGet', 1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon', 'Reports', 'CurrentMonth', 1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon', 'Reports', 'PreviousMonth', 1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon', 'Identifier', 'SetDateTimeFields', 1,0,0,0,0,1);

call AddPrivileges (200, 'Baloon', 'SpdServiceRule', 'SpdServiceRuleListGet_API',1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon', 'IdentifierRule', 'IdentifierRuleListGet_API',1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon', 'IdentifierCategory', 'IdentifierCategoryListGet_API',1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon', 'IdentifierRate', 'IdentifierRateListGet_API',1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon', 'AgentOrder', 'CurrentAdd_API', 1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon', 'AgentOrder', 'OnlineCurrentAdd_API', 1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon', 'IdentifierAgentOrder', 'CurrentAdd_API', 1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon', 'PackageAgentOrder', 'CurrentAdd_API', 1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon', 'PackageAgentOrder', 'OnlineCurrentAdd_API', 1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon', 'AgentOrder', 'CurrentAgentOrderListGet_API', 1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon', 'AgentOrder', 'CurrentGet_API', 1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon', 'AgentOrder', 'CurrentSyncAndGet_API', 1,0,0,0,0,1);

call AddPrivileges (200, 'Baloon', 'Identifier', 'GetBalance_API', 1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon', 'Identifier', 'PackagesListGet_API', 1,0,0,0,0,1);

/*INSERT INTO Token (TokenID, TokenName, TokenStatus, TokenType, TenantID) VALUES (400, 'Оператор партнера', 'enabled', 'Groups', 1);
INSERT INTO Groups (TokenID, TenantID) VALUES (400, 1);

call AddPrivileges(400, 'Baloon', 'PartnerAccount', 'OurDebitGet', 1,0,0,0,0,1);
call AddPrivileges(400, 'Baloon', 'PartnerAccount', 'OurCreditGet', 1,0,0,0,0,1);
call AddPrivileges(400, 'Baloon', 'PartnerContract', 'OurPartnerContractListGet', 1,0,0,0,0,1);
call AddPrivileges(400, 'Baloon', 'PartnerManager', 'GetCurrent', 1,0,0,0,0,1);
call AddPrivileges(400, 'Baloon', 'PartnerAdministrator', 'GetCurrent', 1,0,0,0,0,1);
call AddPrivileges(400, 'Baloon', 'PartnerManager', 'MyGet', 1,0,0,0,0,1);
call AddPrivileges(400, 'Baloon', 'PartnerManager', 'MyUpdate', 1,0,0,0,0,1);
call AddPrivileges(400, 'Baloon', 'Partner', 'OurGet', 1,0,0,0,0,1);
call AddPrivileges(400, 'Baloon', 'AccountTransaction', 'OurDebitAccountTransactionListGet', 1,0,0,0,0,1);
call AddPrivileges(400, 'Baloon', 'AccountTransaction', 'OurCreditAccountTransactionListGet', 1,0,0,0,0,1);

call AddPrivileges(400, 'Baloon', 'PartnerOrder', 'OurPartnerOrderListGet', 1,0,0,0,0,1);
call AddPrivileges(400, 'Baloon', 'WeekTariff', 'OurSeansesByDateListGet', 1,0,0,0,0,1);
call AddPrivileges(400, 'Baloon', 'PartnerAccount', 'OurPartnerAccountListGet', 1,0,0,0,0,1);
call AddPrivileges(400, 'Baloon', 'PartnerOrder', 'OurCreate', 1,0,0,0,0,1);
call AddPrivileges(400, 'Baloon', 'Auxiliary', 'CurrentDate', 1,0,0,0,0,1);
call AddPrivileges(400, 'Baloon', 'PartnerOrderline', 'OurPartnerOrderlineListGet', 1,0,0,0,0,1);
call AddPrivileges(400, 'Baloon', 'PartnerOrder', 'OurTicketsDownload', 1,0,0,0,0,1);*/

INSERT INTO Token (TokenID, TokenName, TokenStatus, TokenType, TenantID) VALUES (20000, 'Оператор партнера', 'enabled', 'Groups', 1);
INSERT INTO Groups (TokenID, TenantID) VALUES (20000, 1);

call AddPrivileges(20000, 'Baloon', 'PartnerAccount', 'OurDebitGet', 1,0,0,0,0,1);
call AddPrivileges(20000, 'Baloon', 'PartnerAccount', 'OurCreditGet', 1,0,0,0,0,1);
call AddPrivileges(20000, 'Baloon', 'PartnerContract', 'OurPartnerContractListGet', 1,0,0,0,0,1);
call AddPrivileges(20000, 'Baloon', 'PartnerManager', 'GetCurrent', 1,0,0,0,0,1);
call AddPrivileges(20000, 'Baloon', 'PartnerAdministrator', 'GetCurrent', 1,0,0,0,0,1);
call AddPrivileges(20000, 'Baloon', 'PartnerManager', 'MyGet', 1,0,0,0,0,1);
call AddPrivileges(20000, 'Baloon', 'PartnerManager', 'MyUpdate', 1,0,0,0,0,1);
call AddPrivileges(20000, 'Baloon', 'Partner', 'OurGet', 1,0,0,0,0,1);
call AddPrivileges(20000, 'Baloon', 'AccountTransaction', 'OurDebitAccountTransactionListGet', 1,0,0,0,0,1);
call AddPrivileges(20000, 'Baloon', 'AccountTransaction', 'OurCreditAccountTransactionListGet', 1,0,0,0,0,1);

call AddPrivileges(20000, 'Baloon', 'PartnerOrder', 'OurPartnerOrderListGet', 1,0,0,0,0,1);
call AddPrivileges(20000, 'Baloon', 'WeekTariff', 'OurSeansesByDateListGet', 1,0,0,0,0,1);
call AddPrivileges(20000, 'Baloon', 'PartnerAccount', 'OurPartnerAccountListGet', 1,0,0,0,0,1);
call AddPrivileges(20000, 'Baloon', 'PartnerOrder', 'OurCreate', 1,0,0,0,0,1);
call AddPrivileges(20000, 'Baloon', 'Auxiliary', 'CurrentDate', 1,0,0,0,0,1);
call AddPrivileges(20000, 'Baloon', 'PartnerOrderline', 'OurPartnerOrderlineListGet', 1,0,0,0,0,1);
call AddPrivileges(20000, 'Baloon', 'PartnerOrder', 'OurTicketsDownload', 1,0,0,0,0,1);
call AddPrivileges(20000, 'Baloon', 'Identifier', 'OurPushToContour', 1,0,0,0,0,1);
call AddPrivileges(20000, 'Baloon', 'Identifier', 'OurIdentifierListGet', 1,0,0,0,0,1);