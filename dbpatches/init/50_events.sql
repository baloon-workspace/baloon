
INSERT INTO `TM_EventHandler` (`KindID`, `Container`, `Object`, `Method`, `SyncStatus`, `ExecDelay`, `RetryCount`, `RetryInterval`, `Priority`) VALUES
('Invoice Status Update', 'Baloon', 'IdentifierInvoice', 'StatusUpdated', 'async', 1, 1, 5, 1);

INSERT INTO `TM_EventHandler` (`KindID`, `Container`, `Object`, `Method`, `SyncStatus`, `ExecDelay`, `RetryCount`, `RetryInterval`, `Priority`) VALUES
('Invoice Status Update', 'Baloon', 'AgentInvoice', 'StatusUpdated', 'async', 1, 1, 5, 1);

INSERT INTO `TM_EventHandler` (`KindID`, `Container`, `Object`, `Method`, `SyncStatus`, `ExecDelay`, `RetryCount`, `RetryInterval`, `Priority`) VALUES
('Invoice Status Update', 'Baloon', 'PersonInvoice', 'StatusUpdated', 'async', 1, 1, 5, 1);


INSERT INTO `TM_EventKind` (`KindID`, `Name`, `LoggingStatus`, `Priority`) VALUES
('Update Weather', NULL, 'off', 20);

INSERT INTO `TM_EventHandler` (`KindID`, `Container`, `Object`, `Method`, `SyncStatus`, `ExecDelay`, `RetryCount`, `RetryInterval`, `Priority`) VALUES
('Update Weather', 'Basicsite', 'WeatherInfo', 'RequestYandex', 'async', 0, 1, 60, 1);

INSERT INTO TM_Scheduler (KindID, StartTime, RepeatInterval, Params, Status)
VALUES('Update Weather', '2018-01-01 00:00:00', 1800, NULL, 'on');


INSERT INTO `TM_EventKind` (`KindID`, `Name`, `LoggingStatus`, `Priority`) VALUES
('Register Identifier In Contour', NULL, 'off', 20);

INSERT INTO `TM_EventHandler` (`KindID`, `Container`, `Object`, `Method`, `SyncStatus`, `ExecDelay`, `RetryCount`, `RetryInterval`, `Priority`) VALUES
('Register Identifier In Contour', 'Baloon', 'PartnerOrderline', 'AsyncRegisterInContour', 'async', 0, 1, 60, 1);

INSERT INTO TM_Scheduler (KindID, StartTime, RepeatInterval, Params, Status)
VALUES('Register Identifier In Contour', '2020-01-01 00:00:00', 180, NULL, 'on');