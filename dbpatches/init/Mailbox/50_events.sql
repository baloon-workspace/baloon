
INSERT INTO `TM_EventKind` (`KindID`, `Name`, `LoggingStatus`, `Priority`) VALUES
('Check Mailbox', NULL, 'off', 1);

INSERT INTO `TM_EventHandler` (`KindID`, `Container`, `Object`, `Method`, `SyncStatus`, `ExecDelay`, `RetryCount`, `RetryInterval`, `Priority`) VALUES
('Check Mailbox', 'Mailbox', 'MailboxMessage', 'CheckMail', 'async', 0, 0, 60, 1);

INSERT INTO `TM_EventHandler` (`KindID`, `Container`, `Object`, `Method`, `SyncStatus`, `ExecDelay`, `RetryCount`, `RetryInterval`, `Priority`) VALUES
('Check Mailbox', 'Mailbox', 'MailboxMessage', 'DeliverMail', 'async', 0, 3, 30, 1);

INSERT INTO TM_Scheduler (KindID, StartTime, RepeatInterval, Params, Status)
VALUES('Check Mailbox', '2015-01-01 00:00:00', 300, NULL, 'on');
