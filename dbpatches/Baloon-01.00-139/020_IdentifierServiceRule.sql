-- Добавляем параметр COST чтобы его потом вставлять в <package>
ALTER TABLE IdentifierServiceRule ADD cost decimal(60,30) NULL COMMENT 'Cost';
ALTER TABLE IdentifierServiceRuleArc ADD cost decimal(60,30) NULL COMMENT 'Cost';
