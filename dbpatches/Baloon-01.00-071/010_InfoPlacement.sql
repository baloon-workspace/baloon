CREATE TABLE InfoPlacement (
    id INT NOT NULL COMMENT 'Article ID',
    widget VARCHAR(40) NOT NULL COMMENT 'Widget',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (id, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Info Placement';

CREATE TABLE InfoPlacementArc (
    id INT COMMENT 'Article ID',
    widget VARCHAR(40) COMMENT 'Widget',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    ArcType VARCHAR(1) COMMENT 'ArcType',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (id, UserArc, DateArc, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit Info Placement';

CREATE INDEX InfoPlacementNewsArticle ON InfoPlacement(id, TenantID);
ALTER TABLE InfoPlacement ADD CONSTRAINT InfoPlacement_NewsArticle FOREIGN KEY (id, TenantID) REFERENCES NewsArticle (id, TenantID);
