ALTER TABLE IdentifierCategory ADD COLUMN is_default VARCHAR(5) COMMENT 'Is Default';
ALTER TABLE IdentifierCategoryArc ADD COLUMN is_default VARCHAR(5) COMMENT 'Is Default';
