INSERT INTO `TM_EventKind` (`KindID`, `Name`, `LoggingStatus`, `Priority`) VALUES
('Update Service Rule', NULL, 'off', 20);

INSERT INTO `TM_EventHandler` (`KindID`, `Container`, `Object`, `Method`, `SyncStatus`, `ExecDelay`, `RetryCount`, `RetryInterval`, `Priority`) VALUES
('Update Service Rule', 'Baloon', 'SpdServiceRule', 'SyncDictionary', 'async', 0, 1, 30, 1);

INSERT INTO TM_Scheduler (KindID, StartTime, RepeatInterval, Params, Status)
VALUES('Update Service Rule', '2018-01-01 00:00:15', 1800, NULL, 'on');


