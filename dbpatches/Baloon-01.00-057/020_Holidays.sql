CREATE TABLE Holiday (
    adate DATETIME NOT NULL COMMENT 'Date',
    comment TEXT COMMENT 'Comment',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (adate, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Holiday';

CREATE TABLE HolidayArc (
    adate DATETIME COMMENT 'Date',
    comment TEXT COMMENT 'Comment',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    ArcType VARCHAR(1) COMMENT 'ArcType',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (adate, UserArc, DateArc, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit Holiday';

INSERT INTO Holiday(adate) VALUES ('2019-01-01 00:00:00');
INSERT INTO Holiday(adate) VALUES ('2019-01-02 00:00:00');
INSERT INTO Holiday(adate) VALUES ('2019-01-03 00:00:00');
INSERT INTO Holiday(adate) VALUES ('2019-01-04 00:00:00');
INSERT INTO Holiday(adate) VALUES ('2019-01-05 00:00:00');
INSERT INTO Holiday(adate) VALUES ('2019-01-06 00:00:00');
INSERT INTO Holiday(adate) VALUES ('2019-01-07 00:00:00');
INSERT INTO Holiday(adate) VALUES ('2019-01-08 00:00:00');

