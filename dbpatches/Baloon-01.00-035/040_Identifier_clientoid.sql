ALTER TABLE Identifier ADD COLUMN clientoid VARCHAR(40) COMMENT 'Spd Client Oid';
ALTER TABLE IdentifierArc ADD COLUMN clientoid VARCHAR(40) COMMENT 'Spd Client Oid';

-- UPDATE Identifier SET clientoid = (SELECT oid FROM Person WHERE Identifier.personid = Person.id);