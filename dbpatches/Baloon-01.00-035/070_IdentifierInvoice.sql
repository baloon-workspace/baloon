ALTER TABLE PersonInvoice ADD COLUMN identifierid INT COMMENT 'ID';
ALTER TABLE PersonInvoiceArc ADD COLUMN identifierid INT COMMENT 'ID';

ALTER TABLE PersonInvoice RENAME TO IdentifierInvoice;
ALTER TABLE PersonInvoiceArc RENAME TO IdentifierInvoiceArc;

UPDATE IdentifierInvoice SET identifierid =
    (SELECT MAX(id) FROM Identifier WHERE Identifier.personid = IdentifierInvoice.personid);

CREATE INDEX IdentifierInvoiceidentifier ON IdentifierInvoice(identifierid, TenantID);
ALTER TABLE IdentifierInvoice ADD CONSTRAINT IdentifierInvoice_identifier FOREIGN KEY (identifierid, TenantID) REFERENCES Identifier (id, TenantID);

ALTER TABLE IdentifierInvoice DROP FOREIGN KEY PersonInvoice_person;
ALTER TABLE IdentifierInvoice DROP COLUMN personid;
ALTER TABLE IdentifierInvoiceArc DROP COLUMN personid;

ALTER TABLE IdentifierInvoice CHANGE identifierid identifierid INT NOT NULL COMMENT 'ID';