
CREATE TABLE AgentInvoice (
	id INT NOT NULL COMMENT 'Invoice ID',
	orderid INT NOT NULL COMMENT 'Order ID',
	eid VARCHAR(40) COMMENT 'EID',
	UserArc BIGINT COMMENT 'Modified by',
	DateArc DATETIME COMMENT 'Modification time',
	TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
	PRIMARY KEY (id, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Invoice';

CREATE TABLE AgentInvoiceArc (
	id INT COMMENT 'Invoice ID',
	orderid INT COMMENT 'Order ID',
	eid VARCHAR(40) COMMENT 'EID',
	UserArc BIGINT COMMENT 'Modified by',
	DateArc DATETIME COMMENT 'Modification time',
	ArcType VARCHAR(1) COMMENT 'ArcType',
	TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
	PRIMARY KEY (id, UserArc, DateArc, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit Invoice';

CREATE INDEX AgentInvoiceInvoice ON AgentInvoice(id);
ALTER TABLE AgentInvoice ADD CONSTRAINT AgentInvoice_Invoice FOREIGN KEY (id) REFERENCES Invoice (id);

CREATE INDEX AgentInvoiceorder ON AgentInvoice(orderid, TenantID);
ALTER TABLE AgentInvoice ADD CONSTRAINT AgentInvoice_order FOREIGN KEY (orderid, TenantID) REFERENCES AgentOrder (id, TenantID);


