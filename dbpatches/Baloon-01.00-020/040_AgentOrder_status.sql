ALTER TABLE AgentOrder ADD COLUMN status VARCHAR(40) NOT NULL COMMENT 'Status';
ALTER TABLE AgentOrderArc ADD COLUMN status VARCHAR(40) COMMENT 'Status';

UPDATE AgentOrder SET status='offline';
