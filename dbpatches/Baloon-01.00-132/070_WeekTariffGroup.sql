CREATE TABLE WeekTariffGroup (
    id INT NOT NULL AUTO_INCREMENT COMMENT 'ID',
    name VARCHAR(40) NOT NULL COMMENT 'Name',
    description TEXT COMMENT 'Description',
    is_default VARCHAR(5) COMMENT 'Is Default',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (id, TenantID)
    , INDEX(id)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Tariff Group';

CREATE TABLE WeekTariffGroupArc (
    id INT COMMENT 'ID',
    name VARCHAR(40) COMMENT 'Name',
    description TEXT COMMENT 'Description',
    is_default VARCHAR(5) COMMENT 'Is Default',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    ArcType VARCHAR(1) COMMENT 'ArcType',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (id, UserArc, DateArc, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit Tariff Group';

ALTER TABLE WeekTariff ADD COLUMN agroupid INT COMMENT 'ID';
ALTER TABLE WeekTariffArc ADD COLUMN agroupid INT COMMENT 'ID';

CREATE INDEX WeekTariffagroup ON WeekTariff(agroupid, TenantID);
ALTER TABLE WeekTariff ADD CONSTRAINT WeekTariff_agroup FOREIGN KEY (agroupid, TenantID) REFERENCES WeekTariffGroup (id, TenantID);

INSERT INTO WeekTariffGroup(id, name, is_default) VALUES (1, 'Тарифная группа по умолчанию', 'true');
UPDATE WeekTariff SET agroupid = 1;

ALTER TABLE WeekTariff CHANGE agroupid agroupid INT NOT NULL COMMENT 'ID';