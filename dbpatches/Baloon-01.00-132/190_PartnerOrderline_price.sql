ALTER TABLE PartnerOrderline ADD COLUMN price NUMERIC(60,30) COMMENT 'Price';
ALTER TABLE PartnerOrderlineArc ADD COLUMN price NUMERIC(60,30) COMMENT 'Price';
