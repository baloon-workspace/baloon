ALTER TABLE ParkingOrderline ADD COLUMN  areaid INT COMMENT 'ID';
ALTER TABLE ParkingOrderlineArc ADD COLUMN  areaid INT COMMENT 'ID';

CREATE INDEX ParkingOrderlinearea ON ParkingOrderline(areaid, TenantID);
ALTER TABLE ParkingOrderline ADD CONSTRAINT ParkingOrderline_area FOREIGN KEY (areaid, TenantID) REFERENCES ParkingArea (id, TenantID);