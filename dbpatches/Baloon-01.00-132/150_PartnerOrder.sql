CREATE TABLE PartnerOrder (
    id INT NOT NULL AUTO_INCREMENT COMMENT 'ID',
    partnerid INT NOT NULL COMMENT 'ID',
    total_cost NUMERIC(60,30) NOT NULL COMMENT 'Total Cost',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (id, TenantID)
    , INDEX(id)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Order';

CREATE TABLE PartnerOrderArc (
    id INT COMMENT 'ID',
    partnerid INT COMMENT 'ID',
    total_cost NUMERIC(60,30) COMMENT 'Total Cost',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    ArcType VARCHAR(1) COMMENT 'ArcType',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (id, UserArc, DateArc, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit Order';

CREATE TABLE PartnerOrderline (
    orderid INT NOT NULL COMMENT 'ID',
    identifierid INT NOT NULL COMMENT 'ID',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (orderid, identifierid, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Orderline';

CREATE TABLE PartnerOrderlineArc (
    orderid INT COMMENT 'ID',
    identifierid INT COMMENT 'ID',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    ArcType VARCHAR(1) COMMENT 'ArcType',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (orderid, identifierid, UserArc, DateArc, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit Orderline';

CREATE INDEX PartnerOrderpartner ON PartnerOrder(partnerid, TenantID);
ALTER TABLE PartnerOrder ADD CONSTRAINT PartnerOrder_partner FOREIGN KEY (partnerid, TenantID) REFERENCES Partner (id, TenantID);

CREATE INDEX PartnerOrderlineorder ON PartnerOrderline(orderid, TenantID);
ALTER TABLE PartnerOrderline ADD CONSTRAINT PartnerOrderline_order FOREIGN KEY (orderid, TenantID) REFERENCES PartnerOrder (id, TenantID);

CREATE INDEX PartnerOrderlineidentifier ON PartnerOrderline(identifierid, TenantID);
ALTER TABLE PartnerOrderline ADD CONSTRAINT PartnerOrderline_identifier FOREIGN KEY (identifierid, TenantID) REFERENCES Identifier (id, TenantID);
