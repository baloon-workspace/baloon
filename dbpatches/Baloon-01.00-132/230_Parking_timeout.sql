ALTER TABLE ParkingArea ADD COLUMN enter_timeout INT COMMENT 'Enter Timout, min';
ALTER TABLE ParkingAreaArc ADD COLUMN enter_timeout INT COMMENT 'Enter Timout, min';

ALTER TABLE ParkingArea ADD COLUMN exit_timeout INT COMMENT 'Exit Timout, min';
ALTER TABLE ParkingAreaArc ADD COLUMN exit_timeout INT COMMENT 'Exit Timout, min';

UPDATE ParkingArea SET enter_timeout = 10, exit_timeout = 10;

ALTER TABLE ParkingArea CHANGE enter_timeout enter_timeout INT NOT NULL COMMENT 'Enter Timout, min';
ALTER TABLE ParkingArea CHANGE exit_timeout exit_timeout INT NOT NULL COMMENT 'Exit Timout, min';