CREATE TABLE Partner (
    id INT NOT NULL AUTO_INCREMENT COMMENT 'ID',
    name VARCHAR(40) NOT NULL COMMENT 'Name',
    status VARCHAR(40) NOT NULL COMMENT 'Status',
    legal_address VARCHAR(500) NOT NULL COMMENT 'Legal Address',
    post_address VARCHAR(500) COMMENT 'Post Address',
    inn VARCHAR(40) COMMENT 'INN',
    kpp VARCHAR(40) COMMENT 'KPP',
    payment_account VARCHAR(40) COMMENT 'Payment Account',
    correspondent_account VARCHAR(40) COMMENT 'Correspondent Account',
    bank VARCHAR(255) COMMENT 'Bank',
    bik VARCHAR(40) COMMENT 'BIK',
    okpo VARCHAR(40) COMMENT 'OKPO',
    okato VARCHAR(40) COMMENT 'OKATO',
    okved VARCHAR(40) COMMENT 'OKVED',
    ogrn VARCHAR(40) COMMENT 'OGRN',
    principal VARCHAR(255) COMMENT 'Principal',
    phone VARCHAR(40) COMMENT 'Phone',
    fax VARCHAR(40) COMMENT 'Fax',
    email VARCHAR(80) COMMENT 'Email',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (id, TenantID)
    , INDEX(id)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Partner';

CREATE TABLE PartnerArc (
    id INT COMMENT 'ID',
    name VARCHAR(40) COMMENT 'Name',
    status VARCHAR(40) COMMENT 'Status',
    legal_address VARCHAR(500) COMMENT 'Legal Address',
    post_address VARCHAR(500) COMMENT 'Post Address',
    inn VARCHAR(40) COMMENT 'INN',
    kpp VARCHAR(40) COMMENT 'KPP',
    payment_account VARCHAR(40) COMMENT 'Payment Account',
    correspondent_account VARCHAR(40) COMMENT 'Correspondent Account',
    bank VARCHAR(255) COMMENT 'Bank',
    bik VARCHAR(40) COMMENT 'BIK',
    okpo VARCHAR(40) COMMENT 'OKPO',
    okato VARCHAR(40) COMMENT 'OKATO',
    okved VARCHAR(40) COMMENT 'OKVED',
    ogrn VARCHAR(40) COMMENT 'OGRN',
    principal VARCHAR(255) COMMENT 'Principal',
    phone VARCHAR(40) COMMENT 'Phone',
    fax VARCHAR(40) COMMENT 'Fax',
    email VARCHAR(80) COMMENT 'Email',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    ArcType VARCHAR(1) COMMENT 'ArcType',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (id, UserArc, DateArc, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit Partner';

CREATE TABLE PartnerContract (
    id INT NOT NULL AUTO_INCREMENT COMMENT 'ID',
    partnerid INT NOT NULL COMMENT 'ID',
    barcode VARCHAR(40) NOT NULL COMMENT 'Contract Number',
    start_date DATETIME NOT NULL COMMENT 'Start Date',
    end_date DATETIME NOT NULL COMMENT 'End Date',
    benefit_type VARCHAR(40) NOT NULL COMMENT 'Benefit Type',
    benefit_amount NUMERIC(60,30) NOT NULL COMMENT 'Benefit Amount',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (id, TenantID)
    , INDEX(id)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Partner Contract';

CREATE TABLE PartnerContractArc (
    id INT COMMENT 'ID',
    partnerid INT COMMENT 'ID',
    barcode VARCHAR(40) COMMENT 'Contract Number',
    start_date DATETIME COMMENT 'Start Date',
    end_date DATETIME COMMENT 'End Date',
    benefit_type VARCHAR(40) COMMENT 'Benefit Type',
    benefit_amount NUMERIC(60,30) COMMENT 'Benefit Amount',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    ArcType VARCHAR(1) COMMENT 'ArcType',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (id, UserArc, DateArc, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit Partner Contract';

CREATE TABLE PartnerAccount (
    id INT NOT NULL AUTO_INCREMENT COMMENT 'ID',
    partnerid INT NOT NULL COMMENT 'ID',
    name VARCHAR(40) NOT NULL COMMENT 'Name',
    atype VARCHAR(40) NOT NULL COMMENT 'Account Type',
    amount NUMERIC(60,30) NOT NULL COMMENT 'Amount',
    status VARCHAR(40) NOT NULL COMMENT 'Status',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (id, TenantID)
    , INDEX(id)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Account';

CREATE TABLE PartnerAccountArc (
    id INT COMMENT 'ID',
    partnerid INT COMMENT 'ID',
    name VARCHAR(40) COMMENT 'Name',
    atype VARCHAR(40) COMMENT 'Account Type',
    amount NUMERIC(60,30) COMMENT 'Amount',
    status VARCHAR(40) COMMENT 'Status',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    ArcType VARCHAR(1) COMMENT 'ArcType',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (id, UserArc, DateArc, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit Account';

CREATE TABLE AccountTransaction (
    id INT NOT NULL AUTO_INCREMENT COMMENT 'ID',
    accountid INT NOT NULL COMMENT 'ID',
    amount NUMERIC(60,30) NOT NULL COMMENT 'Amount',
    cache_flow VARCHAR(40) NOT NULL COMMENT 'Cache Flow',
    reason TEXT COMMENT 'Reason',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (id, TenantID)
    , INDEX(id)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Transaction';

CREATE TABLE AccountTransactionArc (
    id INT COMMENT 'ID',
    accountid INT COMMENT 'ID',
    amount NUMERIC(60,30) COMMENT 'Amount',
    cache_flow VARCHAR(40) COMMENT 'Cache Flow',
    reason TEXT COMMENT 'Reason',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    ArcType VARCHAR(1) COMMENT 'ArcType',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (id, UserArc, DateArc, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit Transaction';

CREATE INDEX PartnerContractpartner ON PartnerContract(partnerid, TenantID);
ALTER TABLE PartnerContract ADD CONSTRAINT PartnerContract_partner FOREIGN KEY (partnerid, TenantID) REFERENCES Partner (id, TenantID);

CREATE INDEX PartnerAccountpartner ON PartnerAccount(partnerid, TenantID);
ALTER TABLE PartnerAccount ADD CONSTRAINT PartnerAccount_partner FOREIGN KEY (partnerid, TenantID) REFERENCES Partner (id, TenantID);

CREATE INDEX AccountTransactionaccount ON AccountTransaction(accountid, TenantID);
ALTER TABLE AccountTransaction ADD CONSTRAINT AccountTransaction_account FOREIGN KEY (accountid, TenantID) REFERENCES PartnerAccount (id, TenantID);

