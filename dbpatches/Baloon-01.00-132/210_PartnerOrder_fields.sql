ALTER TABLE PartnerOrder ADD COLUMN initial_cost NUMERIC(60,30) NOT NULL COMMENT 'Initial Cost';
ALTER TABLE PartnerOrder ADD COLUMN discount NUMERIC(60,30) NOT NULL COMMENT 'Discount';
ALTER TABLE PartnerOrder ADD COLUMN week_tariffid INT NOT NULL COMMENT 'Tariff ID';

ALTER TABLE PartnerOrderArc ADD COLUMN initial_cost NUMERIC(60,30) COMMENT 'Initial Cost';
ALTER TABLE PartnerOrderArc ADD COLUMN discount NUMERIC(60,30) COMMENT 'Discount';
ALTER TABLE PartnerOrderArc ADD COLUMN week_tariffid INT COMMENT 'Tariff ID';

CREATE INDEX PartnerOrderweek_tariff ON PartnerOrder(week_tariffid, TenantID);
ALTER TABLE PartnerOrder ADD CONSTRAINT PartnerOrder_week_tariff FOREIGN KEY (week_tariffid, TenantID) REFERENCES WeekTariff (id, TenantID);

