CREATE TABLE PartnerAdministrator (
    auserTokenID BIGINT NOT NULL COMMENT 'Token ID',
    fullname VARCHAR(255) NOT NULL COMMENT 'Fullname',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (auserTokenID, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Partner Adminstrator';

CREATE TABLE PartnerAdministratorArc (
    auserTokenID BIGINT COMMENT 'Token ID',
    fullname VARCHAR(255) COMMENT 'Fullname',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    ArcType VARCHAR(1) COMMENT 'ArcType',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (auserTokenID, UserArc, DateArc, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit Partner Adminstrator';

CREATE INDEX PartnerAdministratorauser ON PartnerAdministrator(auserTokenID, TenantID);
ALTER TABLE PartnerAdministrator ADD CONSTRAINT PartnerAdministrator_auser FOREIGN KEY (auserTokenID, TenantID) REFERENCES Users (TokenID, TenantID);

