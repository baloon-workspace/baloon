CREATE TABLE OrderSite (
    code VARCHAR(40) NOT NULL COMMENT 'Code',
    name VARCHAR(40) NOT NULL COMMENT 'Name',
    link VARCHAR(255) COMMENT 'Link',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (code, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Order Site';

CREATE TABLE OrderSiteArc (
    code VARCHAR(40) COMMENT 'Code',
    name VARCHAR(40) COMMENT 'Name',
    link VARCHAR(255) COMMENT 'Link',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    ArcType VARCHAR(1) COMMENT 'ArcType',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (code, UserArc, DateArc, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit Order Site';

ALTER TABLE PersonOrder ADD COLUMN sitecode VARCHAR(40) COMMENT 'Code';
ALTER TABLE PersonOrderArc ADD COLUMN sitecode VARCHAR(40) COMMENT 'Code';

CREATE INDEX PersonOrdersite ON PersonOrder(sitecode, TenantID);
ALTER TABLE PersonOrder ADD CONSTRAINT PersonOrder_site FOREIGN KEY (sitecode, TenantID) REFERENCES OrderSite (code, TenantID);
