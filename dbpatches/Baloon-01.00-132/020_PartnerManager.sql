CREATE TABLE PartnerManager (
    auserTokenID BIGINT NOT NULL COMMENT 'Token ID',
    partnerid INT NOT NULL COMMENT 'ID',
    fullname VARCHAR(255) NOT NULL COMMENT 'Fullname',
    passport_number VARCHAR(40) NOT NULL COMMENT 'Passport Number',
    issue_date DATETIME NOT NULL COMMENT 'Issue Date',
    issued_by VARCHAR(500) NOT NULL COMMENT 'Issued By',
    department_code VARCHAR(40) NOT NULL COMMENT 'Department Code',
    phone VARCHAR(40) COMMENT 'Phone',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (auserTokenID, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Person In Charge';

CREATE TABLE PartnerManagerArc (
    auserTokenID BIGINT COMMENT 'Token ID',
    partnerid INT COMMENT 'ID',
    fullname VARCHAR(255) COMMENT 'Fullname',
    passport_number VARCHAR(40) COMMENT 'Passport Number',
    issue_date DATETIME COMMENT 'Issue Date',
    issued_by VARCHAR(500) COMMENT 'Issued By',
    department_code VARCHAR(40) COMMENT 'Department Code',
    phone VARCHAR(40) COMMENT 'Phone',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    ArcType VARCHAR(1) COMMENT 'ArcType',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (auserTokenID, UserArc, DateArc, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit Person In Charge';

CREATE INDEX PartnerManagerauser ON PartnerManager(auserTokenID, TenantID);
ALTER TABLE PartnerManager ADD CONSTRAINT PartnerManager_auser FOREIGN KEY (auserTokenID, TenantID) REFERENCES Users (TokenID, TenantID);

CREATE INDEX PartnerManagerpartner ON PartnerManager(partnerid, TenantID);
ALTER TABLE PartnerManager ADD CONSTRAINT PartnerManager_partner FOREIGN KEY (partnerid, TenantID) REFERENCES Partner (id, TenantID);
