CREATE TABLE PartnerWeekTariffGroup (
    partnerid INT NOT NULL COMMENT 'ID',
    tariffgroupid INT NOT NULL COMMENT 'ID',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (partnerid, tariffgroupid, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Week Tariff Group';

CREATE TABLE PartnerWeekTariffGroupArc (
    partnerid INT COMMENT 'ID',
    tariffgroupid INT COMMENT 'ID',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    ArcType VARCHAR(1) COMMENT 'ArcType',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (partnerid, tariffgroupid, UserArc, DateArc, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit Week Tariff Group';

CREATE INDEX PartnerWeekTariffGrouppartner ON PartnerWeekTariffGroup(partnerid, TenantID);
ALTER TABLE PartnerWeekTariffGroup ADD CONSTRAINT PartnerWeekTariffGroup_partner FOREIGN KEY (partnerid, TenantID) REFERENCES Partner (id, TenantID);

CREATE INDEX PartnerWeekTariffGrouptariffgroup ON PartnerWeekTariffGroup(tariffgroupid, TenantID);
ALTER TABLE PartnerWeekTariffGroup ADD CONSTRAINT PartnerWeekTariffGroup_tariffgroup FOREIGN KEY (tariffgroupid, TenantID) REFERENCES WeekTariffGroup (id, TenantID);
