CREATE TABLE ParkingTimer (
    orderid INT NOT NULL COMMENT 'Order ID',
    end_time DATETIME NOT NULL COMMENT 'End Time',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (orderid, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Parking Timer';

CREATE TABLE ParkingTimerArc (
    orderid INT COMMENT 'Order ID',
    end_time DATETIME COMMENT 'End Time',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    ArcType VARCHAR(1) COMMENT 'ArcType',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (orderid, UserArc, DateArc, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit Parking Timer';

CREATE INDEX ParkingTimerorder ON ParkingTimer(orderid, TenantID);
ALTER TABLE ParkingTimer ADD CONSTRAINT ParkingTimer_order FOREIGN KEY (orderid, TenantID) REFERENCES PersonOrder (id, TenantID);