INSERT INTO Token (TokenID, TokenName, TokenStatus, TokenType, TenantID) VALUES (20000, 'Оператор партнера', 'enabled', 'Groups', 1);
INSERT INTO Groups (TokenID, TenantID) VALUES (20000, 1);

call AddPrivileges(20000, 'Baloon', 'PartnerAccount', 'OurDebitGet', 1,0,0,0,0,1);
call AddPrivileges(20000, 'Baloon', 'PartnerAccount', 'OurCreditGet', 1,0,0,0,0,1);
call AddPrivileges(20000, 'Baloon', 'PartnerContract', 'OurPartnerContractListGet', 1,0,0,0,0,1);
call AddPrivileges(20000, 'Baloon', 'PartnerManager', 'GetCurrent', 1,0,0,0,0,1);
call AddPrivileges(20000, 'Baloon', 'PartnerAdministrator', 'GetCurrent', 1,0,0,0,0,1);
call AddPrivileges(20000, 'Baloon', 'PartnerManager', 'MyGet', 1,0,0,0,0,1);
call AddPrivileges(20000, 'Baloon', 'PartnerManager', 'MyUpdate', 1,0,0,0,0,1);
call AddPrivileges(20000, 'Baloon', 'Partner', 'OurGet', 1,0,0,0,0,1);
call AddPrivileges(20000, 'Baloon', 'AccountTransaction', 'OurDebitAccountTransactionListGet', 1,0,0,0,0,1);
call AddPrivileges(20000, 'Baloon', 'AccountTransaction', 'OurCreditAccountTransactionListGet', 1,0,0,0,0,1);

call AddPrivileges(20000, 'Baloon', 'PartnerOrder', 'OurPartnerOrderListGet', 1,0,0,0,0,1);
call AddPrivileges(20000, 'Baloon', 'WeekTariff', 'OurSeansesByDateListGet', 1,0,0,0,0,1);
call AddPrivileges(20000, 'Baloon', 'PartnerAccount', 'OurPartnerAccountListGet', 1,0,0,0,0,1);
call AddPrivileges(20000, 'Baloon', 'PartnerOrder', 'OurCreate', 1,0,0,0,0,1);
call AddPrivileges(20000, 'Baloon', 'Auxiliary', 'CurrentDate', 1,0,0,0,0,1);
call AddPrivileges(20000, 'Baloon', 'PartnerOrderline', 'OurPartnerOrderlineListGet', 1,0,0,0,0,1);
call AddPrivileges(20000, 'Baloon', 'PartnerOrder', 'OurTicketsDownload', 1,0,0,0,0,1);
