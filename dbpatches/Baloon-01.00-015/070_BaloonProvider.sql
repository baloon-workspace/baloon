
CREATE TABLE BaloonProvider (
	default_paygateid INT COMMENT 'Gate ID',
	UserArc BIGINT COMMENT 'Modified by',
	DateArc DATETIME COMMENT 'Modification time',
	TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID'
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Provider Settings';

INSERT INTO BaloonProvider VALUES();

CREATE TABLE BaloonProviderArc (
	default_paygateid INT COMMENT 'Gate ID',
	UserArc BIGINT COMMENT 'Modified by',
	DateArc DATETIME COMMENT 'Modification time',
	ArcType VARCHAR(1) COMMENT 'ArcType',
	TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
	PRIMARY KEY (UserArc, DateArc, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit Provider Settings';

CREATE INDEX BaloonProviderdefault_paygate ON BaloonProvider(default_paygateid);
ALTER TABLE BaloonProvider ADD CONSTRAINT BaloonProvider_default_paygate FOREIGN KEY (default_paygateid) REFERENCES PayGate (id);

