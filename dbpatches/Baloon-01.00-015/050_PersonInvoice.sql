
CREATE TABLE PersonInvoice (
	id INT NOT NULL COMMENT 'Invoice ID',
	personid INT COMMENT 'Person ID',
	eid VARCHAR(40) COMMENT 'EID',
	UserArc BIGINT COMMENT 'Modified by',
	DateArc DATETIME COMMENT 'Modification time',
	TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
	PRIMARY KEY (id, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Invoice';

CREATE TABLE PersonInvoiceArc (
	id INT COMMENT 'Invoice ID',
	personid INT COMMENT 'Person ID',
	eid VARCHAR(40) COMMENT 'EID',
	UserArc BIGINT COMMENT 'Modified by',
	DateArc DATETIME COMMENT 'Modification time',
	ArcType VARCHAR(1) COMMENT 'ArcType',
	TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
	PRIMARY KEY (id, UserArc, DateArc, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit Invoice';

CREATE INDEX PersonInvoiceInvoice ON PersonInvoice(id);
ALTER TABLE PersonInvoice ADD CONSTRAINT PersonInvoice_Invoice FOREIGN KEY (id) REFERENCES Invoice (id);

CREATE INDEX PersonInvoiceperson ON PersonInvoice(personid, TenantID);
ALTER TABLE PersonInvoice ADD CONSTRAINT PersonInvoice_person FOREIGN KEY (personid, TenantID) REFERENCES Person (id, TenantID);

