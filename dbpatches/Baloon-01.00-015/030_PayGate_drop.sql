
alter table Transaction drop FOREIGN KEY Transaction_gate;
alter table Transaction drop KEY Transactiongate
alter table Transaction drop column gateid;
alter table TransactionArc drop column gateid;

DROP TABLE KkbGate;
DROP TABLE KkbGateArc;
DROP TABLE UnitellerGate;
DROP TABLE UnitellerGateArc;
DROP TABLE AssistGate;
DROP TABLE AssistGateArc;

DROP TABLE PayGate;
DROP TABLE PayGateArc;

update TM_Scheduler set Status='off' where KindID='Synchronize Transactions';

