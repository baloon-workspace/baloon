
ALTER TABLE PayGate ADD COLUMN currency VARCHAR(40) COMMENT 'Currency';
ALTER TABLE PayGateArc ADD COLUMN currency VARCHAR(40) COMMENT 'Currency';

UPDATE PayGate SET currency='RUB';

ALTER TABLE PayGate CHANGE COLUMN currency currency VARCHAR(40) NOT NULL COMMENT 'Currency';

