
CREATE TABLE AuthConfirmationToken (
	uid VARCHAR(40) NOT NULL COMMENT 'ID',
	userTokenID BIGINT NOT NULL COMMENT 'Token ID',
	type VARCHAR(40) NOT NULL COMMENT 'type',
	valid_until DATETIME NOT NULL COMMENT 'valid_until',
	confirmed VARCHAR(5) NOT NULL COMMENT 'Confirmed',
	UserArc BIGINT COMMENT 'Modified by',
	DateArc DATETIME COMMENT 'Modification time',
	TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
	PRIMARY KEY (uid, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Sign up token';

CREATE TABLE AuthConfirmationTokenArc (
	uid VARCHAR(40) COMMENT 'ID',
	userTokenID BIGINT COMMENT 'Token ID',
	type VARCHAR(40) COMMENT 'type',
	valid_until DATETIME COMMENT 'valid_until',
	confirmed VARCHAR(5) COMMENT 'Confirmed',
	UserArc BIGINT COMMENT 'Modified by',
	DateArc DATETIME COMMENT 'Modification time',
	ArcType VARCHAR(1) COMMENT 'ArcType',
	TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
	PRIMARY KEY (uid, UserArc, DateArc, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit Sign up token';

CREATE INDEX AuthConfirmationTokenuser ON AuthConfirmationToken(userTokenID, TenantID);
ALTER TABLE AuthConfirmationToken ADD CONSTRAINT AuthConfirmationToken_user FOREIGN KEY (userTokenID, TenantID) REFERENCES Users (TokenID, TenantID);


call AddPrivileges(0, 'Baloon', 'AuthConfirmationToken', 'SubmitRestorePassword', 1, 0, 0, 0, 0, 1);
call AddPrivileges(0, 'Baloon', 'AuthConfirmationToken', 'ConfirmRestorePassword', 1, 0, 0, 0, 0, 1);

INSERT INTO `TM_EventKind` (`KindID`, `Name`, `LoggingStatus`, `Priority`) VALUES
('Submitted password restoration', NULL, 'on', 10);

INSERT INTO `TM_EventHandler` (`KindID`, `Container`, `Object`, `Method`, `SyncStatus`, `ExecDelay`, `RetryCount`, `RetryInterval`, `Priority`) VALUES
('Submitted password restoration', 'Baloon', 'AuthConfirmationToken', 'CreateRestorePasswordEmail', 'async', 1, 3, 1, 10);

