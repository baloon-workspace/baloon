
CREATE TABLE KkbGate (
	id INT NOT NULL COMMENT 'Gate ID',
	action_url VARCHAR(255) NOT NULL COMMENT 'Action URL',
	command_url VARCHAR(255) NOT NULL COMMENT 'Command URL',
	merchant_certid VARCHAR(10) NOT NULL COMMENT 'Certificate ID',
	merchantid VARCHAR(10) NOT NULL COMMENT 'Merchant ID',
	merchant_name VARCHAR(255) NOT NULL COMMENT 'Merchant Name',
	private_key TEXT NOT NULL COMMENT 'Private Key',
	private_key_pass VARCHAR(40) NOT NULL COMMENT 'Private Key Password',
	public_key TEXT NOT NULL COMMENT 'Public Key',
	test_addition INT COMMENT 'Test Order Addition',
	UserArc BIGINT COMMENT 'Modified by',
	DateArc DATETIME COMMENT 'Modification time',
	TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
	PRIMARY KEY (id, TenantID)
	, INDEX(id)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Казкоммерцбанк';

CREATE TABLE KkbGateArc (
	id INT COMMENT 'Gate ID',
	action_url VARCHAR(255) COMMENT 'Action URL',
	command_url VARCHAR(255) COMMENT 'Command URL',
	merchant_certid VARCHAR(10) COMMENT 'Certificate ID',
	merchantid VARCHAR(10) COMMENT 'Merchant ID',
	merchant_name VARCHAR(255) COMMENT 'Merchant Name',
	private_key TEXT COMMENT 'Private Key',
	private_key_pass VARCHAR(40) COMMENT 'Private Key Password',
	public_key TEXT COMMENT 'Public Key',
	test_addition INT COMMENT 'Test Order Addition',
	UserArc BIGINT COMMENT 'Modified by',
	DateArc DATETIME COMMENT 'Modification time',
	ArcType VARCHAR(1) COMMENT 'ArcType',
	TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
	PRIMARY KEY (id, UserArc, DateArc, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit Казкоммерцбанк';

ALTER TABLE KkbGate ADD CONSTRAINT KkbGate_Parent_ FOREIGN KEY (id, TenantID) REFERENCES PayGate (id, TenantID) ON DELETE CASCADE;

