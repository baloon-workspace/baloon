CREATE TABLE WeekTariff (
    id INT NOT NULL AUTO_INCREMENT COMMENT 'Tariff ID',
    week_day VARCHAR(40) NOT NULL COMMENT 'Week Day',
    time_from TIME NOT NULL COMMENT 'Time From',
    time_upto TIME NOT NULL COMMENT 'Time To',
    price NUMERIC(60,30) COMMENT 'Price',
    identifier_rulename VARCHAR(255) COMMENT 'Правило пользования',
    service_rulename VARCHAR(255) COMMENT 'Правило оформления',
    identifier_categoryname VARCHAR(255) COMMENT 'Название категории',
    ratename VARCHAR(255) COMMENT 'Тариф',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (id, TenantID)
    , INDEX(id)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Week Tariff';

CREATE TABLE WeekTariffArc (
    id INT COMMENT 'Tariff ID',
    week_day VARCHAR(40) COMMENT 'Week Day',
    time_from TIME COMMENT 'Time From',
    time_upto TIME COMMENT 'Time To',
    price NUMERIC(60,30) COMMENT 'Price',
    identifier_rulename VARCHAR(255) COMMENT 'Правило пользования',
    service_rulename VARCHAR(255) COMMENT 'Правило оформления',
    identifier_categoryname VARCHAR(255) COMMENT 'Название категории',
    ratename VARCHAR(255) COMMENT 'Тариф',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    ArcType VARCHAR(1) COMMENT 'ArcType',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (id, UserArc, DateArc, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit Week Tariff';

CREATE INDEX WeekTariffidentifier_rule ON WeekTariff(identifier_rulename, TenantID);
ALTER TABLE WeekTariff ADD CONSTRAINT WeekTariff_identifier_rule FOREIGN KEY (identifier_rulename, TenantID) REFERENCES IdentifierRule (name, TenantID);

CREATE INDEX WeekTariffservice_rule ON WeekTariff(service_rulename, TenantID);
ALTER TABLE WeekTariff ADD CONSTRAINT WeekTariff_service_rule FOREIGN KEY (service_rulename, TenantID) REFERENCES SpdServiceRule (name, TenantID);

CREATE INDEX WeekTariffidentifier_category ON WeekTariff(identifier_categoryname, TenantID);
ALTER TABLE WeekTariff ADD CONSTRAINT WeekTariff_identifier_category FOREIGN KEY (identifier_categoryname, TenantID) REFERENCES IdentifierCategory (name, TenantID);

CREATE INDEX WeekTariffrate ON WeekTariff(ratename, TenantID);
ALTER TABLE WeekTariff ADD CONSTRAINT WeekTariff_rate FOREIGN KEY (ratename, TenantID) REFERENCES IdentifierRate (name, TenantID);