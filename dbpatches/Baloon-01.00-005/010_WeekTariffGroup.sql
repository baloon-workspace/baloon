CREATE TABLE WeekTariffGroup (
	id INT NOT NULL AUTO_INCREMENT COMMENT 'ID',
	name VARCHAR(40) NOT NULL COMMENT 'Name',
	description TEXT COMMENT 'Description',
	is_default VARCHAR(5) COMMENT 'Is Default',
	UserArc BIGINT COMMENT 'Modified by',
	DateArc DATETIME COMMENT 'Modification time',
	TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
	PRIMARY KEY (id, TenantID)
	, INDEX(id)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Tariff Group';

CREATE TABLE WeekTariffGroupArc (
	id INT COMMENT 'ID',
	name VARCHAR(40) NOT NULL COMMENT 'Name',
	description TEXT COMMENT 'Description',
	is_default VARCHAR(5) COMMENT 'Is Default',
	UserArc BIGINT COMMENT 'Modified by',
	DateArc DATETIME COMMENT 'Modification time',
	TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
	PRIMARY KEY (id, UserArc, DateArc, TenantID)
	, INDEX(id)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit Tariff Group';

