ALTER TABLE Partner ADD COLUMN notification_email VARCHAR(40) COMMENT 'Email For Notifications';
ALTER TABLE PartnerArc ADD COLUMN notification_email VARCHAR(40) COMMENT 'Email For Notifications';