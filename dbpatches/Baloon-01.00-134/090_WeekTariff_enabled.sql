ALTER TABLE WeekTariff ADD COLUMN enabled VARCHAR(5) COMMENT 'Enabled';
ALTER TABLE WeekTariffArc ADD COLUMN enabled VARCHAR(5) COMMENT 'Enabled';

UPDATE WeekTariff SET enabled = 'true';
