ALTER TABLE PartnerOrderline ADD COLUMN service_rulename VARCHAR(255) COMMENT 'Правило оформления';
ALTER TABLE PartnerOrderlineArc ADD COLUMN service_rulename VARCHAR(255) COMMENT 'Правило оформления';

CREATE INDEX PartnerOrderlineservice_rule ON PartnerOrderline(service_rulename, TenantID);
ALTER TABLE PartnerOrderline ADD CONSTRAINT PartnerOrderline_service_rule FOREIGN KEY (service_rulename, TenantID) REFERENCES SpdServiceRule (name, TenantID);

