ALTER TABLE Identifier ADD COLUMN groupname VARCHAR(40) COMMENT 'Groupname';
ALTER TABLE IdentifierArc ADD COLUMN groupname VARCHAR(40) COMMENT 'Groupname';