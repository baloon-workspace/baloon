CREATE TABLE ParkingOrderline (
    id INT NOT NULL AUTO_INCREMENT COMMENT 'ID',
    orderid INT NOT NULL COMMENT 'Order ID',
    identifierid INT NOT NULL COMMENT 'ID',
    time_from DATETIME NOT NULL COMMENT 'Time From',
    time_upto DATETIME NOT NULL COMMENT 'Time Upto',
    price NUMERIC(60,30) NOT NULL COMMENT 'Price',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (id, TenantID)
    , INDEX(id)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Parking Ticket';

CREATE TABLE ParkingOrderlineArc (
    id INT COMMENT 'ID',
    orderid INT COMMENT 'Order ID',
    identifierid INT COMMENT 'ID',
    time_from DATETIME COMMENT 'Time From',
    time_upto DATETIME COMMENT 'Time Upto',
    price NUMERIC(60,30) COMMENT 'Price',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    ArcType VARCHAR(1) COMMENT 'ArcType',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (id, UserArc, DateArc, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit Parking Ticket';

CREATE INDEX ParkingOrderlineorder ON ParkingOrderline(orderid, TenantID);
ALTER TABLE ParkingOrderline ADD CONSTRAINT ParkingOrderline_order FOREIGN KEY (orderid, TenantID) REFERENCES PersonOrder (id, TenantID);

CREATE INDEX ParkingOrderlineidentifier ON ParkingOrderline(identifierid, TenantID);
ALTER TABLE ParkingOrderline ADD CONSTRAINT ParkingOrderline_identifier FOREIGN KEY (identifierid, TenantID) REFERENCES Identifier (id, TenantID);
