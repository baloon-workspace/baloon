CREATE TABLE ParkingArea (
    id INT NOT NULL AUTO_INCREMENT COMMENT 'ID',
    name VARCHAR(40) NOT NULL COMMENT 'Name',
    spdgateid INT NOT NULL COMMENT 'Gate ID',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (id, TenantID)
    , INDEX(id)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Parking Area';

CREATE TABLE ParkingAreaArc (
    id INT COMMENT 'ID',
    name VARCHAR(40) COMMENT 'Name',
    spdgateid INT COMMENT 'Gate ID',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    ArcType VARCHAR(1) COMMENT 'ArcType',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (id, UserArc, DateArc, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit Parking Area';

CREATE INDEX ParkingAreaspdgate ON ParkingArea(spdgateid, TenantID);
ALTER TABLE ParkingArea ADD CONSTRAINT ParkingArea_spdgate FOREIGN KEY (spdgateid, TenantID) REFERENCES SpdGate (id, TenantID);

