ALTER TABLE Identifier ADD COLUMN spdgateid INT COMMENT 'Gate ID';
ALTER TABLE IdentifierArc ADD COLUMN spdgateid INT COMMENT 'Gate ID';

CREATE INDEX Identifierspdgate ON Identifier(spdgateid, TenantID);
ALTER TABLE Identifier ADD CONSTRAINT Identifier_spdgate FOREIGN KEY (spdgateid, TenantID) REFERENCES SpdGate (id, TenantID);