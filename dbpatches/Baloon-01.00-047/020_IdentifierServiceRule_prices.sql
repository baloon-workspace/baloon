ALTER TABLE IdentifierServiceRule ADD COLUMN weekday_cost NUMERIC(60,30) COMMENT 'Weekday Cost';
ALTER TABLE IdentifierServiceRule ADD COLUMN weekend_cost NUMERIC(60,30) COMMENT 'Weekend Cost';

ALTER TABLE IdentifierServiceRuleArc ADD COLUMN weekday_cost NUMERIC(60,30) COMMENT 'Weekday Cost';
ALTER TABLE IdentifierServiceRuleArc ADD COLUMN weekend_cost NUMERIC(60,30) COMMENT 'Weekend Cost';
