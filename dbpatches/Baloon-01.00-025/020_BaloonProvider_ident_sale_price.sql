ALTER TABLE BaloonProvider ADD COLUMN ident_sale_price NUMERIC(60,30) COMMENT 'Стоимость продажи карты';
ALTER TABLE BaloonProviderArc ADD COLUMN ident_sale_price NUMERIC(60,30) COMMENT 'Стоимость продажи карты';

UPDATE BaloonProvider SET ident_sale_price=100;
