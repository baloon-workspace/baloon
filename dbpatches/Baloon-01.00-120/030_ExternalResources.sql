CREATE TABLE ExternalResource (
    id INT NOT NULL AUTO_INCREMENT COMMENT 'ID',
    name VARCHAR(40) NOT NULL COMMENT 'Name',
    uri VARCHAR(500) NOT NULL COMMENT 'URI',
    description TEXT COMMENT 'Description',
    enabled VARCHAR(40) NOT NULL COMMENT 'Status',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (id, TenantID)
    , INDEX(id)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='External Resource';

CREATE TABLE ExternalResourceArc (
    id INT COMMENT 'ID',
    name VARCHAR(40) COMMENT 'Name',
    uri VARCHAR(500) COMMENT 'URI',
    description TEXT COMMENT 'Description',
    enabled VARCHAR(40) COMMENT 'Status',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    ArcType VARCHAR(1) COMMENT 'ArcType',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (id, UserArc, DateArc, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit External Resource';

