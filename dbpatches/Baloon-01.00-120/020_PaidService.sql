CREATE TABLE PaidServiceCategory (
    id INT NOT NULL AUTO_INCREMENT COMMENT 'ID',
    name VARCHAR(255) NOT NULL COMMENT 'Name',
    status VARCHAR(40) NOT NULL COMMENT 'status',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (id, TenantID)
    , INDEX(id)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Paid Service Category';

CREATE TABLE PaidServiceCategoryArc (
    id INT COMMENT 'ID',
    name VARCHAR(255) COMMENT 'Name',
    status VARCHAR(40) COMMENT 'status',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    ArcType VARCHAR(1) COMMENT 'ArcType',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (id, UserArc, DateArc, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit Paid Service Category';

CREATE TABLE PaidServiceTariff (
    id INT NOT NULL AUTO_INCREMENT COMMENT 'ID',
    name VARCHAR(255) NOT NULL COMMENT 'Name',
    status VARCHAR(40) NOT NULL COMMENT 'status',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (id, TenantID)
    , INDEX(id)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Paid Service Tariff';

CREATE TABLE PaidServiceTariffArc (
    id INT COMMENT 'ID',
    name VARCHAR(255) COMMENT 'Name',
    status VARCHAR(40) COMMENT 'status',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    ArcType VARCHAR(1) COMMENT 'ArcType',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (id, UserArc, DateArc, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit Paid Service Tariff';

CREATE TABLE PaidServiceHour (
    id INT NOT NULL AUTO_INCREMENT COMMENT 'ID',
    name VARCHAR(255) COMMENT 'Name',
    status VARCHAR(40) NOT NULL COMMENT 'status',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (id, TenantID)
    , INDEX(id)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Paid Service Hour';

CREATE TABLE PaidServiceHourArc (
    id INT COMMENT 'ID',
    name VARCHAR(255) COMMENT 'Name',
    status VARCHAR(40) COMMENT 'status',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    ArcType VARCHAR(1) COMMENT 'ArcType',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (id, UserArc, DateArc, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit Paid Service Hour';

CREATE TABLE PaidService (
    id INT NOT NULL AUTO_INCREMENT COMMENT 'ID',
    categoryid INT NOT NULL COMMENT 'ID',
    name VARCHAR(255) NOT NULL COMMENT 'Name',
    tariffid INT COMMENT 'ID',
    hourid INT COMMENT 'ID',
    description TEXT COMMENT 'Description',
    price NUMERIC(60,30) NOT NULL COMMENT 'Price',
    status VARCHAR(40) NOT NULL COMMENT 'status',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (id, TenantID)
    , INDEX(id)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Paid Service';

CREATE TABLE PaidServiceArc (
    id INT COMMENT 'ID',
    categoryid INT COMMENT 'ID',
    name VARCHAR(255) COMMENT 'Name',
    tariffid INT COMMENT 'ID',
    hourid INT COMMENT 'ID',
    description TEXT COMMENT 'Description',
    price NUMERIC(60,30) COMMENT 'Price',
    status VARCHAR(40) COMMENT 'status',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    ArcType VARCHAR(1) COMMENT 'ArcType',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (id, UserArc, DateArc, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit Paid Service';

CREATE INDEX PaidServicecategory ON PaidService(categoryid, TenantID);
ALTER TABLE PaidService ADD CONSTRAINT PaidService_category FOREIGN KEY (categoryid, TenantID) REFERENCES PaidServiceCategory (id, TenantID);

CREATE INDEX PaidServicetariff ON PaidService(tariffid, TenantID);
ALTER TABLE PaidService ADD CONSTRAINT PaidService_tariff FOREIGN KEY (tariffid, TenantID) REFERENCES PaidServiceTariff (id, TenantID);

CREATE INDEX PaidServicehour ON PaidService(hourid, TenantID);
ALTER TABLE PaidService ADD CONSTRAINT PaidService_hour FOREIGN KEY (hourid, TenantID) REFERENCES PaidServiceHour (id, TenantID);
