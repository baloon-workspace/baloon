CREATE TABLE PackageOrder (
    id INT NOT NULL AUTO_INCREMENT COMMENT 'ID',
    identifierid INT NOT NULL COMMENT 'ID',
    service_rulename VARCHAR(255) NOT NULL COMMENT 'Правило оформления',
    filed_at DATETIME NOT NULL COMMENT 'Filed At',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (id, TenantID)
    , INDEX(id)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Package Order';

CREATE TABLE PackageOrderArc (
    id INT COMMENT 'ID',
    identifierid INT COMMENT 'ID',
    service_rulename VARCHAR(255) COMMENT 'Правило оформления',
    filed_at DATETIME COMMENT 'Filed At',
    UserArc BIGINT COMMENT 'Modified by',
    DateArc DATETIME COMMENT 'Modification time',
    ArcType VARCHAR(1) COMMENT 'ArcType',
    TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
    PRIMARY KEY (id, UserArc, DateArc, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit Package Order';

CREATE INDEX PackageOrderidentifier ON PackageOrder(identifierid, TenantID);
ALTER TABLE PackageOrder ADD CONSTRAINT PackageOrder_identifier FOREIGN KEY (identifierid, TenantID) REFERENCES Identifier (id, TenantID);

CREATE INDEX PackageOrderservice_rule ON PackageOrder(service_rulename, TenantID);
ALTER TABLE PackageOrder ADD CONSTRAINT PackageOrder_service_rule FOREIGN KEY (service_rulename, TenantID) REFERENCES SpdServiceRule (name, TenantID);
