ALTER TABLE SalesOrder ADD COLUMN refill_amount NUMERIC(60,30) DEFAULT 0.0 NOT NULL COMMENT 'Refill amount';
ALTER TABLE SalesOrderArc ADD COLUMN refill_amount NUMERIC(60,30) DEFAULT 0.0 COMMENT 'Refill amount';
