
ALTER TABLE IdentifierRule ADD COLUMN api_enabled VARCHAR(40) NOT NULL COMMENT 'Enabled for API';
ALTER TABLE IdentifierRule ADD COLUMN extid INT NOT NULL COMMENT 'External ID';
ALTER TABLE IdentifierRule ADD COLUMN extcode INT COMMENT 'External Code';
ALTER TABLE IdentifierRule ADD COLUMN comment TEXT COMMENT 'Comment';

ALTER TABLE IdentifierRuleArc ADD COLUMN api_enabled VARCHAR(40) COMMENT 'Enabled for API';
ALTER TABLE IdentifierRuleArc ADD COLUMN extid INT NOT NULL COMMENT 'External ID';
ALTER TABLE IdentifierRuleArc ADD COLUMN extcode INT COMMENT 'External Code';
ALTER TABLE IdentifierRuleArc ADD COLUMN comment TEXT COMMENT 'Comment';

UPDATE IdentifierRule SET api_enabled='disabled';

