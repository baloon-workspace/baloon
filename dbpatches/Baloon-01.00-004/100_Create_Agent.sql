CREATE TABLE Agent (
 agentid INT NOT NULL AUTO_INCREMENT COMMENT 'Agent ID',
 userTokenID BIGINT NOT NULL COMMENT 'Token ID',
 name VARCHAR(40) NOT NULL COMMENT 'name',
 phone VARCHAR(40) COMMENT 'phone',
 email VARCHAR(80) COMMENT 'email',
 comments TEXT COMMENT 'comment',
 UserArc BIGINT COMMENT 'Modified by',
 DateArc DATETIME COMMENT 'Modification time',
 TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
 PRIMARY KEY (agentid, TenantID), INDEX(agentid)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Agent';

CREATE TABLE AgentArc (
 agentid INT COMMENT 'Agent ID',
 userTokenID BIGINT COMMENT 'Token ID',
 name VARCHAR(40) COMMENT 'name',
 phone VARCHAR(40) COMMENT 'phone',
 email VARCHAR(80) COMMENT 'email',
 comments TEXT COMMENT 'comment',
 UserArc BIGINT COMMENT 'Modified by',
 DateArc DATETIME COMMENT 'Modification time',
 ArcType VARCHAR(1) COMMENT 'ArcType',
 TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
 PRIMARY KEY (agentid, UserArc, DateArc, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit Agent';

CREATE INDEX Agentuser ON Agent(userTokenID, TenantID);
ALTER TABLE Agent ADD CONSTRAINT Agent_user FOREIGN KEY (userTokenID, TenantID) REFERENCES Users (TokenID, TenantID);
