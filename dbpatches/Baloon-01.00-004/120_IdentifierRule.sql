
CREATE TABLE IdentifierRule (
	name VARCHAR(255) NOT NULL COMMENT 'Rate',
	UserArc BIGINT COMMENT 'Modified by',
	DateArc DATETIME COMMENT 'Modification time',
	TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
	PRIMARY KEY (name, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='IdentifierRule';

CREATE TABLE IdentifierRuleArc (
	name VARCHAR(255) COMMENT 'Rate',
	UserArc BIGINT COMMENT 'Modified by',
	DateArc DATETIME COMMENT 'Modification time',
	ArcType VARCHAR(1) COMMENT 'ArcType',
	TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
	PRIMARY KEY (name, UserArc, DateArc, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit IdentifierRule';


ALTER TABLE Identifier CHANGE COLUMN rule_use rule_usename VARCHAR(255) COMMENT 'Rule to use';
ALTER TABLE IdentifierArc CHANGE COLUMN rule_use rule_usename VARCHAR(255) COMMENT 'Rule to use';

INSERT INTO IdentifierRule (name) SELECT DISTINCT rule_usename FROM Identifier;

CREATE INDEX Identifierrule_use ON Identifier(rule_usename, TenantID);
ALTER TABLE Identifier ADD CONSTRAINT Identifier_rule_use FOREIGN KEY (rule_usename, TenantID) REFERENCES IdentifierRule (name, TenantID);
