
ALTER TABLE IdentifierRate ADD COLUMN enabled VARCHAR(40) NOT NULL COMMENT 'Enabled';
ALTER TABLE IdentifierRateArc ADD COLUMN enabled VARCHAR(40) COMMENT 'Enabled';
UPDATE IdentifierRate SET enabled='enabled';

ALTER TABLE IdentifierCategory ADD COLUMN enabled VARCHAR(40) NOT NULL COMMENT 'Enabled';
ALTER TABLE IdentifierCategoryArc ADD COLUMN enabled VARCHAR(40) COMMENT 'Enabled';
UPDATE IdentifierCategory SET enabled='enabled';

ALTER TABLE IdentifierRule ADD COLUMN enabled VARCHAR(40) NOT NULL COMMENT 'Enabled';
ALTER TABLE IdentifierRuleArc ADD COLUMN enabled VARCHAR(40) COMMENT 'Enabled';
UPDATE IdentifierRule SET enabled='enabled';

