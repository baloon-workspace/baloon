ALTER TABLE Identifier CHANGE category categoryname VARCHAR(255) COMMENT 'Category name';
ALTER TABLE IdentifierArc CHANGE category categoryname VARCHAR(255) COMMENT 'Category name';

ALTER TABLE IdentifierCategory CHANGE category_name name VARCHAR(255) NOT NULL COMMENT 'Category name';
ALTER TABLE IdentifierCategoryArc CHANGE category_name name VARCHAR(255) COMMENT 'Category name';

INSERT INTO IdentifierCategory(name) SELECT DISTINCT categoryname FROM Identifier WHERE categoryname IS NOT NULL;

CREATE INDEX Identifiercategory ON Identifier(categoryname, TenantID);
ALTER TABLE Identifier ADD CONSTRAINT Identifier_category FOREIGN KEY (categoryname, TenantID) REFERENCES IdentifierCategory (name, TenantID);
