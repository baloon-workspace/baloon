CREATE TABLE IdentifierRate (
 name VARCHAR(255) NOT NULL COMMENT 'Rate',
 UserArc BIGINT COMMENT 'Modified by',
 DateArc DATETIME COMMENT 'Modification time',
 TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
 PRIMARY KEY (name, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='IdentifierRate';

CREATE TABLE IdentifierRateArc (
  name VARCHAR(255) COMMENT 'Rate',
  UserArc BIGINT COMMENT 'Modified by',
  DateArc DATETIME COMMENT 'Modification time',
  ArcType VARCHAR(1) COMMENT 'ArcType',
  TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
  PRIMARY KEY (name, UserArc, DateArc, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit IdentifierRate';

ALTER TABLE Identifier CHANGE rate ratename VARCHAR(255) COMMENT 'Rate';
ALTER TABLE IdentifierArc CHANGE rate ratename VARCHAR(255) COMMENT 'Rate';

INSERT INTO IdentifierRate (name) SELECT DISTINCT ratename FROM Identifier WHERE ratename IS NOT NULL;

CREATE INDEX Identifierrate ON Identifier(ratename, TenantID);
ALTER TABLE Identifier ADD CONSTRAINT Identifier_rate FOREIGN KEY (ratename, TenantID) REFERENCES IdentifierRate (name, TenantID);
