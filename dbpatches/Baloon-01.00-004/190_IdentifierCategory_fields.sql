
ALTER TABLE IdentifierCategory ADD COLUMN api_enabled VARCHAR(40) NOT NULL COMMENT 'Enabled for API';
ALTER TABLE IdentifierCategory ADD COLUMN extid INT NOT NULL COMMENT 'External ID';
ALTER TABLE IdentifierCategory ADD COLUMN extcode INT COMMENT 'External Code';
ALTER TABLE IdentifierCategory ADD COLUMN comment TEXT COMMENT 'Comment';

ALTER TABLE IdentifierCategoryArc ADD COLUMN api_enabled VARCHAR(40) COMMENT 'Enabled for API';
ALTER TABLE IdentifierCategoryArc ADD COLUMN extid INT NOT NULL COMMENT 'External ID';
ALTER TABLE IdentifierCategoryArc ADD COLUMN extcode INT COMMENT 'External Code';
ALTER TABLE IdentifierCategoryArc ADD COLUMN comment TEXT COMMENT 'Comment';

UPDATE IdentifierCategory SET api_enabled='disabled';

