CREATE TABLE IdentifierCategory (
 category_name VARCHAR(255) NOT NULL COMMENT 'Catrgory name',
 UserArc BIGINT COMMENT 'Modified by',
 DateArc DATETIME COMMENT 'Modification time',
 TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
 PRIMARY KEY (category_name, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='IdentifierCategory';

CREATE TABLE IdentifierCategoryArc (
 category_name VARCHAR(255) COMMENT 'Catrgory name',
 UserArc BIGINT COMMENT 'Modified by',
 DateArc DATETIME COMMENT 'Modification time',
 ArcType VARCHAR(1) COMMENT 'ArcType',
 TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
 PRIMARY KEY (category_name, UserArc, DateArc, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit IdentifierCategory';
