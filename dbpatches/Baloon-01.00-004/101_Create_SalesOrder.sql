CREATE TABLE SalesOrder (
 salesorderid INT NOT NULL AUTO_INCREMENT COMMENT 'SalesOrderID',
 agentid INT COMMENT 'Agent ID',
 id INT COMMENT 'Person ID',
 UserArc BIGINT COMMENT 'Modified by',
 DateArc DATETIME COMMENT 'Modification time',
 TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
 PRIMARY KEY (salesorderid, TenantID), INDEX(salesorderid)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Sales';

CREATE TABLE SalesOrderArc (
 salesorderid INT COMMENT 'SalesOrderID',
 agentid INT COMMENT 'Agent ID',
 id INT COMMENT 'Person ID',
 UserArc BIGINT COMMENT 'Modified by',
 DateArc DATETIME COMMENT 'Modification time',
 ArcType VARCHAR(1) COMMENT 'ArcType',
 TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
 PRIMARY KEY (salesorderid, UserArc, DateArc, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit Sales';

CREATE INDEX SalesOrderAgent ON SalesOrder(agentid, TenantID);
ALTER TABLE SalesOrder ADD CONSTRAINT SalesOrder_Agent FOREIGN KEY (agentid, TenantID) REFERENCES Agent (agentid, TenantID);

CREATE INDEX SalesOrderIdentifier ON SalesOrder(id, TenantID);
ALTER TABLE SalesOrder ADD CONSTRAINT SalesOrder_Identifier FOREIGN KEY (id, TenantID) REFERENCES Identifier (id, TenantID);
