ALTER TABLE Agent ADD COLUMN commission DOUBLE COMMENT 'Commission';
ALTER TABLE AgentArc ADD COLUMN commission DOUBLE COMMENT 'Commission';
