
ALTER TABLE SalesOrder DROP FOREIGN KEY SalesOrder_Agent;

ALTER TABLE Agent CHANGE COLUMN agentid id INT NOT NULL AUTO_INCREMENT COMMENT 'Agent ID';
ALTER TABLE AgentArc CHANGE COLUMN agentid id INT COMMENT 'Agent ID';

ALTER TABLE SalesOrder ADD CONSTRAINT SalesOrder_agent FOREIGN KEY (agentid, TenantID) REFERENCES Agent (id, TenantID);

ALTER TABLE Transaction ADD COLUMN agentid INT COMMENT 'Agent ID';
ALTER TABLE TransactionArc ADD COLUMN agentid INT COMMENT 'Agent ID';

CREATE INDEX Transactionagent ON Transaction(agentid, TenantID);
ALTER TABLE Transaction ADD CONSTRAINT Transaction_agent FOREIGN KEY (agentid, TenantID) REFERENCES Agent (id, TenantID);

