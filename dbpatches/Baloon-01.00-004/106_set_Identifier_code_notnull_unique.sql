ALTER TABLE Identifier CHANGE code
code VARCHAR(50) NOT NULL COMMENT 'Code';

ALTER TABLE Identifier ADD CONSTRAINT code_unique UNIQUE (code);
