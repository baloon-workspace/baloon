INSERT INTO Token (TokenID, TokenName, TokenStatus, TokenType, TenantID) VALUES (200, 'Агенты', 'enabled', 'Groups', 1);
INSERT INTO Groups (TokenID, TenantID) VALUES (200, 1);

call AddPrivileges (200, 'Baloon', 'IdentifierCategory', 'IdentifierCategoryListGet', 1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon', 'IdentifierRate', 'IdentifierRateListGet', 1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon', 'IdentifierCategory', 'Get', 1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon', 'IdentifierRate', 'Get', 1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon', 'SalesOrder', 'MyAdd', 1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon', 'SalesOrder', 'MySalesOrderListGet', 1,0,0,0,0,1);
call AddPrivileges (200, 'Baloon', 'SalesOrder', 'Get', 1,0,0,0,0,1);

