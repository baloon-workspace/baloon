
ALTER TABLE IdentifierRate ADD COLUMN api_enabled VARCHAR(40) NOT NULL COMMENT 'Enabled for API';
ALTER TABLE IdentifierRate ADD COLUMN extid INT NOT NULL COMMENT 'External ID';
ALTER TABLE IdentifierRate ADD COLUMN extcode INT COMMENT 'External Code';
ALTER TABLE IdentifierRate ADD COLUMN comment TEXT COMMENT 'Comment';

ALTER TABLE IdentifierRateArc ADD COLUMN api_enabled VARCHAR(40) COMMENT 'Enabled for API';
ALTER TABLE IdentifierRateArc ADD COLUMN extid INT NOT NULL COMMENT 'External ID';
ALTER TABLE IdentifierRateArc ADD COLUMN extcode INT COMMENT 'External Code';
ALTER TABLE IdentifierRateArc ADD COLUMN comment TEXT COMMENT 'Comment';

UPDATE IdentifierRate SET api_enabled='disabled';

