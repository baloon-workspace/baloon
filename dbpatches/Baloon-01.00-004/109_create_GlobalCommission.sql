CREATE TABLE GlobalCommission (
  percent DOUBLE COMMENT 'Global commission',
  UserArc BIGINT COMMENT 'Modified by',
  DateArc DATETIME COMMENT 'Modification time',
  TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID'
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='GlobalCommission';

INSERT INTO GlobalCommission (percent) VALUES(0.0);

CREATE TABLE GlobalCommissionArc (
 percent DOUBLE COMMENT 'Global commission',
 UserArc BIGINT COMMENT 'Modified by',
 DateArc DATETIME COMMENT 'Modification time',
 ArcType VARCHAR(1) COMMENT 'ArcType',
 TenantID BIGINT NOT NULL DEFAULT 1 COMMENT 'Tenant ID',
 PRIMARY KEY (UserArc, DateArc, TenantID)
) ENGINE=Innobase DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for audit GlobalCommission';
