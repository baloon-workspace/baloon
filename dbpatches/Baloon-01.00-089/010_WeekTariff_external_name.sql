ALTER TABLE WeekTariff ADD COLUMN external_name VARCHAR(40) COMMENT 'External Name';
ALTER TABLE WeekTariffArc ADD COLUMN external_name VARCHAR(40) COMMENT 'External Name';