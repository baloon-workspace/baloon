ALTER TABLE Identifier ADD COLUMN status VARCHAR(40) COMMENT 'Status';
ALTER TABLE Identifier ADD COLUMN comment TEXT COMMENT 'Comment';
ALTER TABLE IdentifierArc ADD COLUMN status VARCHAR(40) COMMENT 'Status';
ALTER TABLE IdentifierArc ADD COLUMN comment TEXT COMMENT 'Comment';
