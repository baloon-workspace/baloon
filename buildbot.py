#!/usr/bin/python
import os, sys, os.path, shutil, git, re, subprocess

PROJECT_NAME = "Baloon"
PROJECT = "baloon"
REPO_URL = "git@bitbucket.org:computerica/baloon.git"
BUILD_ROOT = "/home/user/builds"
REPO_DIR = BUILD_ROOT + "/" + PROJECT
PROJECT_SERVER_DIR = REPO_DIR

def pull_repo(branch):
    print "pulling..."
    cloned = False
    if not os.path.exists(BUILD_ROOT):
        os.mkdir(BUILD_ROOT)
    
    if not os.path.exists(REPO_DIR):
        repo = git.Git(BUILD_ROOT).clone(REPO_URL)
        cloned = True
    
    repo = git.Repo(REPO_DIR)
    if not cloned:
        repo.remotes.origin.pull()
    repo.head.reset(index=True, working_tree=True)
    if branch is not None:
        print "checkout", branch
        branch_exists = (0 == len([b.name for b in repo.heads if b.name == "master"]))
        if branch_exists: repo.git.checkout('origin/'+branch)
        else: repo.git.checkout('origin/'+branch, b=branch)
    else:
        print "checkout master"
        repo.heads.master.checkout()

    return repo

def increment_version():
    print "incrementing version..."
    with open(PROJECT_SERVER_DIR + "/version", "r") as f:
        version = f.readline().rstrip()

    p,v,r = version.split("-")
    version = "%s-%03d" % (v, (int(r) + 1))
    
    with open(PROJECT_SERVER_DIR + "/version", "w") as f:
        f.write(PROJECT_NAME + "-" + version)
    
    return version

def update_changelog(new_version):
    print "updating changelog..."
    changelog_file = PROJECT_SERVER_DIR + "/debian/changelog"
    updated_body = ""

    with open(changelog_file, "r") as f:
        line = f.readline()
        line = re.sub(r'\(([\d\.\-]+)\)', '(%s)' % new_version, line)
        updated_body = line + f.read()

    with open(changelog_file, "w") as f:
        f.write(updated_body)

def push_repo(repo, version, branch):
    print "pushing..."
    index = repo.index
    index.add(["version", "debian/changelog"])
    index.commit("building %s" % version)
    if branch is not None:
        subprocess.call(["git", "push", "origin", branch])
    else:
        repo.remotes.origin.push()


def build_deb(branch):
    repo = pull_repo(branch)
    version = increment_version()
    update_changelog(version)
    push_repo(repo, version, branch)
    print "updated to version " + version
    
    os.chdir(PROJECT_SERVER_DIR + "/frontend")
    r = os.system('npm i')
    if r != 0: exit(r)
    r = os.system('NODE_ENV=production webpack')
    if r != 0: exit(r)

    os.chdir(PROJECT_SERVER_DIR)
    r = os.system('dpkg-buildpackage -b')
    if r != 0: exit(r)
    #shutil.move('../%s_%s_amd64.deb' % (PROJECT, version), BUILD_ROOT)
    #shutil.move('../%s_%s.dsc' % (PROJECT, version), BUILD_ROOT)
    #print "moved packages to %s" % BUILD_ROOT
    print "Build done!"
    return "%s_%s_amd64deb" % (PROJECT, version)

def install_deb(package):
    os.system("dpkg -i %s/%s") % (BUILD_ROOT, package)

def run_testing():
    pass

pkg = build_deb(sys.argv[1] if len(sys.argv)>1 else None)

