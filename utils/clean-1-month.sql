
delete from TM_Task where ExecTime<DATE_SUB(NOW(), INTERVAL 1 MONTH) OR DateArc<DATE_SUB(NOW(), INTERVAL 1 MONTH);
delete t from TM_Event e left join TM_Task t on (t.EventID=e.EventID) where e.DateArc<DATE_SUB(NOW(), INTERVAL 1 MONTH);
delete e,t from TM_Event e left join TM_Task t on (t.EventID=e.EventID) where e.DateArc<DATE_SUB(NOW(), INTERVAL 1 MONTH);

delete from EmailAttachment where DateArc<DATE_SUB(NOW(), INTERVAL 1 MONTH);
delete from EmailAttachmentArc where DateArc<DATE_SUB(NOW(), INTERVAL 1 MONTH);
delete from MailboxMessage where DateArc<DATE_SUB(NOW(), INTERVAL 1 MONTH);
delete from MailboxMessageArc where DateArc<DATE_SUB(NOW(), INTERVAL 1 MONTH);

