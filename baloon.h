#ifndef _BALOON_AUX_H_
#define _BALOON_AUX_H_

#include "config.h"
#include <acommon/blobfile.h>
#include "baloon_auto.h"
#include <adata/avalue.h>
#include <boost/filesystem.hpp>


namespace BALOON {

bool loginExists(Connection *rdb, const Str email);
string convertPrintCodeToSpd(string print_code);
string convertSpdCodeToPrint(string code, const Str code_conversion_algo=Str());

std::pair<std::string, std::string> CreateSpdIdentifier(const std::string& code, const Effi::Time& valid_from, const Effi::Time& valid_to, const std::string& permanent_rulename, const std::string& categoryname, const std::string& ratename);
void CheckIdentifierFields(const Effi::Str& code, const Effi::Time& valid_from, const Effi::Time& valid_to, const Effi::Str& permanent_rulename, const Effi::Str& categoryname, const Effi::Str& ratename);
void CheckIdentifierBound(Effi::Connection* rdb, const Effi::Str& code);
void CheckIdentifierRuleEnabled(Effi::Connection* rdb, const Effi::Str& identifier_rulename);
Effi::Int GetDefaultPaygate(Effi::Connection*);
Effi::Int GetDefaultFiscalGate(Effi::Connection*);

extern const Effi::Str ticket_templatename;
//void DeliverNotification(const Effi::Str& email, const Effi::Str& templatename, const PersonOrder& order);
//void UpdateIdentifierStatus(Effi::Connection*, const Effi::Int& orderid, const std::string& status, const Effi::Str& comment, const Effi::Str& clientoid);
std::string NormalizeEmail(const Effi::Str& email);
void GenerateOrderlines(Effi::Connection* rdb, const Effi::Int& personid, const Effi::Int orderid, const Effi::ADate& adate, const WeekTariff& tariff, int qty);

extern const int CLIENT_GROUPID;
extern const int MANAGER_GROUPID;
extern const int PARTNER_ADMIN_GROUPID;

Effi::Value GetDiscount(const Effi::Str& promocode, const Effi::ADate&);
Effi::Value EmptyPromotion();

extern const bool ACTIVE_ONLY;
extern const Effi::Double EPSILON;
extern const Effi::Double ONE_HUNDRED_PERCENT;
void InvalidatePromocode(const Effi::Str&);
std::string createSpdTransaction(const Effi::Str code, const Effi::Decimal amount, const std::string currency,
                                 const std::string comment, const Effi::Decimal cost=Effi::Decimal(0));
Effi::Int SavePackageOrder(Effi::Connection* rdb, const Effi::Int& identifierid, const Effi::Str& service_rulename);

inline bool StrEmpty(const Str& code) {
    return (!Defined(code) || code->empty());
}

Effi::Int CurrentPartnerID(Effi::Connection*);
void WeekTariffMassAdd(Effi::Connection* rdb, const Effi::Int& agroupid, const Effi::Value& week_days, const Effi::Value& tariffs);
Effi::ADate Today();
class SpdClient;
void FullfillClientForRequest(SpdClient& client, const posix_time::time_duration& offset, const PartnerOrderline& oline, const Identifier& identifier);
Effi::Int PartnerByToken(Effi::Connection* rdb, const Effi::Str& token);

} // namespace BALOON

#endif
