#!/bin/bash

# Load generic functions
source /usr/local/effi/util/services

MYSELF="`readlink -f $0`"
PROJECT_ROOT="`dirname $MYSELF`"
PROJECT_NAME="Baloon"

# switch to project's root directory
cd "$PROJECT_ROOT"

create_config CONFIG.example CONFIG || exit $?

$BASH ./stop.sh

check_port "ARB_Port" 1
echo "Starting service arbd..."
$BASH $EFFI_UTIL/process-watcher.sh $EFFI_BIN/arbd file=$CUSTOM_CONF &
check_port "ARB_Port" 0

check_port "WWW_Port" 1
#WWW_STREAM_PORT="`$READCONF $CUSTOM_CONF "/Environment/Set/WWW_STREAM_Port"`"
#echo "WWW STREAM PORT: $WWW_STREAM_PORT"
#if [ -n $WWW_STREAM_Port ]; then
#    check_port "WWW_STREAM_Port" 1
#fi
echo "Starting service sws..."
$BASH $EFFI_UTIL/process-watcher.sh $EFFI_BIN/sws file=$CUSTOM_CONF &
check_port "WWW_Port" 0
#if [ -n $WWW_STREAM_Port ]; then
#    check_port "WWW_STREAM_Port" 0
#fi

echo "Starting service dlloader"
$BASH $EFFI_UTIL/process-watcher.sh $EFFI_BIN/dlloader $PROJECT_NAME file=$CUSTOM_CONF &

