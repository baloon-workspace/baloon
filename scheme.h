#ifndef _BALOON_SCHEME
#define _BALOON_SCHEME

#if defined(_MSC_VER) && defined(Baloon_EXPORTS)
#	define Baloon_DECL __declspec(dllexport)
#elif defined(_MSC_VER)
#	define Baloon_DECL __declspec(dllimport)
#else
#	define Baloon_DECL
#endif

namespace BALOON {
using namespace Effi;

template<typename T>
bool __CheckIDExists(Column c, const T& val, const char* str, Exception& e, Connection* rdb) {
	Selector sel;
	sel << Literal(1);
	sel.Where(c==val);
	if(!sel.Execute(rdb).Fetch()) {
		e << (Message() << Message(str).What() << " with key " << val << " doesn't exist. ").What(); 
		return false;
	}
	return true;
}
template<typename T>
bool __CheckIDNotExists(Column c, const T& val, const char* str, Exception& e, Connection* rdb) {
	Selector sel;
	sel << Literal(1);
	sel.Where(c==val);
	if(sel.Execute(rdb).Fetch()) {
		e << (Message(" Child ") << Message(str).What() << " exists. ").What(); 
		return false;
	}
	return true;
}
class Baloon_DECL PersonArc : public Domain {
private:
	void pushColumns();
public:
	struct PersonArcDomain {
		Column id;
		Column userTokenID;
		Column last_name;
		Column first_name;
		Column middle_name;
		Column birthdate;
		Column gender;
		Column phone;
		Column email;
		Column regcode;
		Column last_sync;
		Column oid;
		Column registered_at;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		PersonArcDomain( const SimpleTable& table )
		: id(table, "id")
		, userTokenID(table, "userTokenID")
		, last_name(table, "last_name")
		, first_name(table, "first_name")
		, middle_name(table, "middle_name")
		, birthdate(table, "birthdate")
		, gender(table, "gender")
		, phone(table, "phone")
		, email(table, "email")
		, regcode(table, "regcode")
		, last_sync(table, "last_sync")
		, oid(table, "oid")
		, registered_at(table, "registered_at")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Int id;
	Int64 userTokenID;
	Str last_name;
	Str first_name;
	Str middle_name;
	ADate birthdate;
	Str gender;
	Str phone;
	Str email;
	Str regcode;
	Time last_sync;
	Str oid;
	ADate registered_at;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	PersonArc();
	PersonArc(const PersonArc& copy);
	PersonArc& operator=(const PersonArc& copy);
	virtual ~PersonArc() {}
	PersonArcDomain* operator->() { return &Domain_; }
	const PersonArcDomain* operator->() const { return &Domain_; }
	bool operator==(const PersonArc& other) const;
	bool operator<(const PersonArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL Person : public Domain {
private:
	void pushColumns();
public:
	struct PersonDomain {
		Column id;
		Column userTokenID;
		Column last_name;
		Column first_name;
		Column middle_name;
		Column birthdate;
		Column gender;
		Column phone;
		Column email;
		Column regcode;
		Column last_sync;
		Column oid;
		Column registered_at;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		PersonDomain( const SimpleTable& table )
		: id(table, "id")
		, userTokenID(table, "userTokenID")
		, last_name(table, "last_name")
		, first_name(table, "first_name")
		, middle_name(table, "middle_name")
		, birthdate(table, "birthdate")
		, gender(table, "gender")
		, phone(table, "phone")
		, email(table, "email")
		, regcode(table, "regcode")
		, last_sync(table, "last_sync")
		, oid(table, "oid")
		, registered_at(table, "registered_at")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Int id;
	Int64 userTokenID;
	Str last_name;
	Str first_name;
	Str middle_name;
	ADate birthdate;
	Str gender;
	Str phone;
	Str email;
	Str regcode;
	Time last_sync;
	Str oid;
	ADate registered_at;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Person();
	explicit Person(Int __id);
	Person(const Person& copy);
	Person& operator=(const Person& copy);
	virtual ~Person() {}
	PersonDomain* operator->() { return &Domain_; }
	const PersonDomain* operator->() const { return &Domain_; }
	bool operator==(const Person& other) const;
	bool operator<(const Person& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL SpdServiceRuleArc : public Domain {
private:
	void pushColumns();
public:
	struct SpdServiceRuleArcDomain {
		Column name;
		Column enabled;
		Column extid;
		Column extcode;
		Column comment;
		Column package_cost;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		SpdServiceRuleArcDomain( const SimpleTable& table )
		: name(table, "name")
		, enabled(table, "enabled")
		, extid(table, "extid")
		, extcode(table, "extcode")
		, comment(table, "comment")
		, package_cost(table, "package_cost")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Str name;
	Str enabled;
	Int extid;
	Int extcode;
	Str comment;
	Decimal package_cost;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	SpdServiceRuleArc();
	SpdServiceRuleArc(const SpdServiceRuleArc& copy);
	SpdServiceRuleArc& operator=(const SpdServiceRuleArc& copy);
	virtual ~SpdServiceRuleArc() {}
	SpdServiceRuleArcDomain* operator->() { return &Domain_; }
	const SpdServiceRuleArcDomain* operator->() const { return &Domain_; }
	bool operator==(const SpdServiceRuleArc& other) const;
	bool operator<(const SpdServiceRuleArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL SpdServiceRule : public Domain {
private:
	void pushColumns();
public:
	struct SpdServiceRuleDomain {
		Column name;
		Column enabled;
		Column extid;
		Column extcode;
		Column comment;
		Column package_cost;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		SpdServiceRuleDomain( const SimpleTable& table )
		: name(table, "name")
		, enabled(table, "enabled")
		, extid(table, "extid")
		, extcode(table, "extcode")
		, comment(table, "comment")
		, package_cost(table, "package_cost")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Str name;
	Str enabled;
	Int extid;
	Int extcode;
	Str comment;
	Decimal package_cost;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	SpdServiceRule();
	explicit SpdServiceRule(Str __name);
	SpdServiceRule(const SpdServiceRule& copy);
	SpdServiceRule& operator=(const SpdServiceRule& copy);
	virtual ~SpdServiceRule() {}
	SpdServiceRuleDomain* operator->() { return &Domain_; }
	const SpdServiceRuleDomain* operator->() const { return &Domain_; }
	bool operator==(const SpdServiceRule& other) const;
	bool operator<(const SpdServiceRule& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL PushNotificationArc : public Domain {
private:
	void pushColumns();
public:
	struct PushNotificationArcDomain {
		Column id;
		Column token;
		Column token_type;
		Column first_name;
		Column last_name;
		Column middle_name;
		Column description;
		Column enabled;
		Column valid_from;
		Column last_register;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		PushNotificationArcDomain( const SimpleTable& table )
		: id(table, "id")
		, token(table, "token")
		, token_type(table, "token_type")
		, first_name(table, "first_name")
		, last_name(table, "last_name")
		, middle_name(table, "middle_name")
		, description(table, "description")
		, enabled(table, "enabled")
		, valid_from(table, "valid_from")
		, last_register(table, "last_register")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Int id;
	Str token;
	Int token_type;
	Str first_name;
	Str last_name;
	Str middle_name;
	Str description;
	Str enabled;
	Time valid_from;
	Time last_register;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	PushNotificationArc();
	PushNotificationArc(const PushNotificationArc& copy);
	PushNotificationArc& operator=(const PushNotificationArc& copy);
	virtual ~PushNotificationArc() {}
	PushNotificationArcDomain* operator->() { return &Domain_; }
	const PushNotificationArcDomain* operator->() const { return &Domain_; }
	bool operator==(const PushNotificationArc& other) const;
	bool operator<(const PushNotificationArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL PushNotification : public Domain {
private:
	void pushColumns();
public:
	struct PushNotificationDomain {
		Column id;
		Column token;
		Column token_type;
		Column first_name;
		Column last_name;
		Column middle_name;
		Column description;
		Column enabled;
		Column valid_from;
		Column last_register;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		PushNotificationDomain( const SimpleTable& table )
		: id(table, "id")
		, token(table, "token")
		, token_type(table, "token_type")
		, first_name(table, "first_name")
		, last_name(table, "last_name")
		, middle_name(table, "middle_name")
		, description(table, "description")
		, enabled(table, "enabled")
		, valid_from(table, "valid_from")
		, last_register(table, "last_register")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Int id;
	Str token;
	Int token_type;
	Str first_name;
	Str last_name;
	Str middle_name;
	Str description;
	Str enabled;
	Time valid_from;
	Time last_register;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	PushNotification();
	explicit PushNotification(Int __id);
	PushNotification(const PushNotification& copy);
	PushNotification& operator=(const PushNotification& copy);
	virtual ~PushNotification() {}
	PushNotificationDomain* operator->() { return &Domain_; }
	const PushNotificationDomain* operator->() const { return &Domain_; }
	bool operator==(const PushNotification& other) const;
	bool operator<(const PushNotification& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL PushNotificationMessagesArc : public Domain {
private:
	void pushColumns();
public:
	struct PushNotificationMessagesArcDomain {
		Column id;
		Column title;
		Column message;
		Column sent;
		Column enabled;
		Column message_date;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		PushNotificationMessagesArcDomain( const SimpleTable& table )
		: id(table, "id")
		, title(table, "title")
		, message(table, "message")
		, sent(table, "sent")
		, enabled(table, "enabled")
		, message_date(table, "message_date")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Int id;
	Str title;
	Str message;
	Str sent;
	Str enabled;
	Time message_date;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	PushNotificationMessagesArc();
	PushNotificationMessagesArc(const PushNotificationMessagesArc& copy);
	PushNotificationMessagesArc& operator=(const PushNotificationMessagesArc& copy);
	virtual ~PushNotificationMessagesArc() {}
	PushNotificationMessagesArcDomain* operator->() { return &Domain_; }
	const PushNotificationMessagesArcDomain* operator->() const { return &Domain_; }
	bool operator==(const PushNotificationMessagesArc& other) const;
	bool operator<(const PushNotificationMessagesArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL PushNotificationMessages : public Domain {
private:
	void pushColumns();
public:
	struct PushNotificationMessagesDomain {
		Column id;
		Column title;
		Column message;
		Column sent;
		Column enabled;
		Column message_date;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		PushNotificationMessagesDomain( const SimpleTable& table )
		: id(table, "id")
		, title(table, "title")
		, message(table, "message")
		, sent(table, "sent")
		, enabled(table, "enabled")
		, message_date(table, "message_date")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Int id;
	Str title;
	Str message;
	Str sent;
	Str enabled;
	Time message_date;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	PushNotificationMessages();
	explicit PushNotificationMessages(Int __id);
	PushNotificationMessages(const PushNotificationMessages& copy);
	PushNotificationMessages& operator=(const PushNotificationMessages& copy);
	virtual ~PushNotificationMessages() {}
	PushNotificationMessagesDomain* operator->() { return &Domain_; }
	const PushNotificationMessagesDomain* operator->() const { return &Domain_; }
	bool operator==(const PushNotificationMessages& other) const;
	bool operator<(const PushNotificationMessages& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL PushNotificationDeliveriesArc : public Domain {
private:
	void pushColumns();
public:
	struct PushNotificationDeliveriesArcDomain {
		Column id;
		Column deviceid;
		Column messageid;
		Column sendresult;
		Column delivery_date;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		PushNotificationDeliveriesArcDomain( const SimpleTable& table )
		: id(table, "id")
		, deviceid(table, "deviceid")
		, messageid(table, "messageid")
		, sendresult(table, "sendresult")
		, delivery_date(table, "delivery_date")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Int id;
	Int deviceid;
	Int messageid;
	Str sendresult;
	Time delivery_date;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	PushNotificationDeliveriesArc();
	PushNotificationDeliveriesArc(const PushNotificationDeliveriesArc& copy);
	PushNotificationDeliveriesArc& operator=(const PushNotificationDeliveriesArc& copy);
	virtual ~PushNotificationDeliveriesArc() {}
	PushNotificationDeliveriesArcDomain* operator->() { return &Domain_; }
	const PushNotificationDeliveriesArcDomain* operator->() const { return &Domain_; }
	bool operator==(const PushNotificationDeliveriesArc& other) const;
	bool operator<(const PushNotificationDeliveriesArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL PushNotificationDeliveries : public Domain {
private:
	void pushColumns();
public:
	struct PushNotificationDeliveriesDomain {
		Column id;
		Column deviceid;
		Column messageid;
		Column sendresult;
		Column delivery_date;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		PushNotificationDeliveriesDomain( const SimpleTable& table )
		: id(table, "id")
		, deviceid(table, "deviceid")
		, messageid(table, "messageid")
		, sendresult(table, "sendresult")
		, delivery_date(table, "delivery_date")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Int id;
	Int deviceid;
	Int messageid;
	Str sendresult;
	Time delivery_date;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	PushNotificationDeliveries();
	explicit PushNotificationDeliveries(Int __id);
	PushNotificationDeliveries(const PushNotificationDeliveries& copy);
	PushNotificationDeliveries& operator=(const PushNotificationDeliveries& copy);
	virtual ~PushNotificationDeliveries() {}
	PushNotificationDeliveriesDomain* operator->() { return &Domain_; }
	const PushNotificationDeliveriesDomain* operator->() const { return &Domain_; }
	bool operator==(const PushNotificationDeliveries& other) const;
	bool operator<(const PushNotificationDeliveries& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL IdentifierRuleArc : public Domain {
private:
	void pushColumns();
public:
	struct IdentifierRuleArcDomain {
		Column name;
		Column enabled;
		Column replenish_enabled;
		Column extid;
		Column extcode;
		Column comment;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		IdentifierRuleArcDomain( const SimpleTable& table )
		: name(table, "name")
		, enabled(table, "enabled")
		, replenish_enabled(table, "replenish_enabled")
		, extid(table, "extid")
		, extcode(table, "extcode")
		, comment(table, "comment")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Str name;
	Str enabled;
	Str replenish_enabled;
	Int extid;
	Int extcode;
	Str comment;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	IdentifierRuleArc();
	IdentifierRuleArc(const IdentifierRuleArc& copy);
	IdentifierRuleArc& operator=(const IdentifierRuleArc& copy);
	virtual ~IdentifierRuleArc() {}
	IdentifierRuleArcDomain* operator->() { return &Domain_; }
	const IdentifierRuleArcDomain* operator->() const { return &Domain_; }
	bool operator==(const IdentifierRuleArc& other) const;
	bool operator<(const IdentifierRuleArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL IdentifierRule : public Domain {
private:
	void pushColumns();
public:
	struct IdentifierRuleDomain {
		Column name;
		Column enabled;
		Column replenish_enabled;
		Column extid;
		Column extcode;
		Column comment;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		IdentifierRuleDomain( const SimpleTable& table )
		: name(table, "name")
		, enabled(table, "enabled")
		, replenish_enabled(table, "replenish_enabled")
		, extid(table, "extid")
		, extcode(table, "extcode")
		, comment(table, "comment")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Str name;
	Str enabled;
	Str replenish_enabled;
	Int extid;
	Int extcode;
	Str comment;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	IdentifierRule();
	explicit IdentifierRule(Str __name);
	IdentifierRule(const IdentifierRule& copy);
	IdentifierRule& operator=(const IdentifierRule& copy);
	virtual ~IdentifierRule() {}
	IdentifierRuleDomain* operator->() { return &Domain_; }
	const IdentifierRuleDomain* operator->() const { return &Domain_; }
	bool operator==(const IdentifierRule& other) const;
	bool operator<(const IdentifierRule& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL IdentifierServiceRuleArc : public Domain {
private:
	void pushColumns();
public:
	struct IdentifierServiceRuleArcDomain {
		Column identifier_rulename;
		Column service_rulename;
		Column name;
		Column enabled;
		Column weekday_cost;
		Column weekend_cost;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		IdentifierServiceRuleArcDomain( const SimpleTable& table )
		: identifier_rulename(table, "identifier_rulename")
		, service_rulename(table, "service_rulename")
		, name(table, "name")
		, enabled(table, "enabled")
		, weekday_cost(table, "weekday_cost")
		, weekend_cost(table, "weekend_cost")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Str identifier_rulename;
	Str service_rulename;
	Str name;
	Str enabled;
	Decimal weekday_cost;
	Decimal weekend_cost;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	IdentifierServiceRuleArc();
	IdentifierServiceRuleArc(const IdentifierServiceRuleArc& copy);
	IdentifierServiceRuleArc& operator=(const IdentifierServiceRuleArc& copy);
	virtual ~IdentifierServiceRuleArc() {}
	IdentifierServiceRuleArcDomain* operator->() { return &Domain_; }
	const IdentifierServiceRuleArcDomain* operator->() const { return &Domain_; }
	bool operator==(const IdentifierServiceRuleArc& other) const;
	bool operator<(const IdentifierServiceRuleArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL IdentifierServiceRule : public Domain {
private:
	void pushColumns();
public:
	struct IdentifierServiceRuleDomain {
		Column identifier_rulename;
		Column service_rulename;
		Column name;
		Column enabled;
		Column weekday_cost;
		Column weekend_cost;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		IdentifierServiceRuleDomain( const SimpleTable& table )
		: identifier_rulename(table, "identifier_rulename")
		, service_rulename(table, "service_rulename")
		, name(table, "name")
		, enabled(table, "enabled")
		, weekday_cost(table, "weekday_cost")
		, weekend_cost(table, "weekend_cost")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Str identifier_rulename;
	Str service_rulename;
	Str name;
	Str enabled;
	Decimal weekday_cost;
	Decimal weekend_cost;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	IdentifierServiceRule();
	explicit IdentifierServiceRule(Str __identifier_rulename, Str __service_rulename);
	IdentifierServiceRule(const IdentifierServiceRule& copy);
	IdentifierServiceRule& operator=(const IdentifierServiceRule& copy);
	virtual ~IdentifierServiceRule() {}
	IdentifierServiceRuleDomain* operator->() { return &Domain_; }
	const IdentifierServiceRuleDomain* operator->() const { return &Domain_; }
	bool operator==(const IdentifierServiceRule& other) const;
	bool operator<(const IdentifierServiceRule& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL IdentifierCategoryArc : public Domain {
private:
	void pushColumns();
public:
	struct IdentifierCategoryArcDomain {
		Column name;
		Column enabled;
		Column extid;
		Column extcode;
		Column comment;
		Column is_default;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		IdentifierCategoryArcDomain( const SimpleTable& table )
		: name(table, "name")
		, enabled(table, "enabled")
		, extid(table, "extid")
		, extcode(table, "extcode")
		, comment(table, "comment")
		, is_default(table, "is_default")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Str name;
	Str enabled;
	Int extid;
	Int extcode;
	Str comment;
	Str is_default;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	IdentifierCategoryArc();
	IdentifierCategoryArc(const IdentifierCategoryArc& copy);
	IdentifierCategoryArc& operator=(const IdentifierCategoryArc& copy);
	virtual ~IdentifierCategoryArc() {}
	IdentifierCategoryArcDomain* operator->() { return &Domain_; }
	const IdentifierCategoryArcDomain* operator->() const { return &Domain_; }
	bool operator==(const IdentifierCategoryArc& other) const;
	bool operator<(const IdentifierCategoryArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL IdentifierCategory : public Domain {
private:
	void pushColumns();
public:
	struct IdentifierCategoryDomain {
		Column name;
		Column enabled;
		Column extid;
		Column extcode;
		Column comment;
		Column is_default;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		IdentifierCategoryDomain( const SimpleTable& table )
		: name(table, "name")
		, enabled(table, "enabled")
		, extid(table, "extid")
		, extcode(table, "extcode")
		, comment(table, "comment")
		, is_default(table, "is_default")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Str name;
	Str enabled;
	Int extid;
	Int extcode;
	Str comment;
	Str is_default;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	IdentifierCategory();
	explicit IdentifierCategory(Str __name);
	IdentifierCategory(const IdentifierCategory& copy);
	IdentifierCategory& operator=(const IdentifierCategory& copy);
	virtual ~IdentifierCategory() {}
	IdentifierCategoryDomain* operator->() { return &Domain_; }
	const IdentifierCategoryDomain* operator->() const { return &Domain_; }
	bool operator==(const IdentifierCategory& other) const;
	bool operator<(const IdentifierCategory& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL IdentifierRateArc : public Domain {
private:
	void pushColumns();
public:
	struct IdentifierRateArcDomain {
		Column name;
		Column enabled;
		Column extid;
		Column extcode;
		Column comment;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		IdentifierRateArcDomain( const SimpleTable& table )
		: name(table, "name")
		, enabled(table, "enabled")
		, extid(table, "extid")
		, extcode(table, "extcode")
		, comment(table, "comment")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Str name;
	Str enabled;
	Int extid;
	Int extcode;
	Str comment;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	IdentifierRateArc();
	IdentifierRateArc(const IdentifierRateArc& copy);
	IdentifierRateArc& operator=(const IdentifierRateArc& copy);
	virtual ~IdentifierRateArc() {}
	IdentifierRateArcDomain* operator->() { return &Domain_; }
	const IdentifierRateArcDomain* operator->() const { return &Domain_; }
	bool operator==(const IdentifierRateArc& other) const;
	bool operator<(const IdentifierRateArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL IdentifierRate : public Domain {
private:
	void pushColumns();
public:
	struct IdentifierRateDomain {
		Column name;
		Column enabled;
		Column extid;
		Column extcode;
		Column comment;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		IdentifierRateDomain( const SimpleTable& table )
		: name(table, "name")
		, enabled(table, "enabled")
		, extid(table, "extid")
		, extcode(table, "extcode")
		, comment(table, "comment")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Str name;
	Str enabled;
	Int extid;
	Int extcode;
	Str comment;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	IdentifierRate();
	explicit IdentifierRate(Str __name);
	IdentifierRate(const IdentifierRate& copy);
	IdentifierRate& operator=(const IdentifierRate& copy);
	virtual ~IdentifierRate() {}
	IdentifierRateDomain* operator->() { return &Domain_; }
	const IdentifierRateDomain* operator->() const { return &Domain_; }
	bool operator==(const IdentifierRate& other) const;
	bool operator<(const IdentifierRate& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL IdentifierArc : public Domain {
private:
	void pushColumns();
public:
	struct IdentifierArcDomain {
		Column id;
		Column personid;
		Column code;
		Column name;
		Column groupname;
		Column valid_from;
		Column valid_to;
		Column permanent_rulename;
		Column categoryname;
		Column ratename;
		Column clientoid;
		Column status;
		Column comment;
		Column spdgateid;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		IdentifierArcDomain( const SimpleTable& table )
		: id(table, "id")
		, personid(table, "personid")
		, code(table, "code")
		, name(table, "name")
		, groupname(table, "groupname")
		, valid_from(table, "valid_from")
		, valid_to(table, "valid_to")
		, permanent_rulename(table, "permanent_rulename")
		, categoryname(table, "categoryname")
		, ratename(table, "ratename")
		, clientoid(table, "clientoid")
		, status(table, "status")
		, comment(table, "comment")
		, spdgateid(table, "spdgateid")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Int id;
	Int personid;
	Str code;
	Str name;
	Str groupname;
	Time valid_from;
	Time valid_to;
	Str permanent_rulename;
	Str categoryname;
	Str ratename;
	Str clientoid;
	Str status;
	Str comment;
	Int spdgateid;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	IdentifierArc();
	IdentifierArc(const IdentifierArc& copy);
	IdentifierArc& operator=(const IdentifierArc& copy);
	virtual ~IdentifierArc() {}
	IdentifierArcDomain* operator->() { return &Domain_; }
	const IdentifierArcDomain* operator->() const { return &Domain_; }
	bool operator==(const IdentifierArc& other) const;
	bool operator<(const IdentifierArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL Identifier : public Domain {
private:
	void pushColumns();
public:
	struct IdentifierDomain {
		Column id;
		Column personid;
		Column code;
		Column name;
		Column groupname;
		Column valid_from;
		Column valid_to;
		Column permanent_rulename;
		Column categoryname;
		Column ratename;
		Column clientoid;
		Column status;
		Column comment;
		Column spdgateid;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		IdentifierDomain( const SimpleTable& table )
		: id(table, "id")
		, personid(table, "personid")
		, code(table, "code")
		, name(table, "name")
		, groupname(table, "groupname")
		, valid_from(table, "valid_from")
		, valid_to(table, "valid_to")
		, permanent_rulename(table, "permanent_rulename")
		, categoryname(table, "categoryname")
		, ratename(table, "ratename")
		, clientoid(table, "clientoid")
		, status(table, "status")
		, comment(table, "comment")
		, spdgateid(table, "spdgateid")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Int id;
	Int personid;
	Str code;
	Str name;
	Str groupname;
	Time valid_from;
	Time valid_to;
	Str permanent_rulename;
	Str categoryname;
	Str ratename;
	Str clientoid;
	Str status;
	Str comment;
	Int spdgateid;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Identifier();
	explicit Identifier(Int __id);
	Identifier(const Identifier& copy);
	Identifier& operator=(const Identifier& copy);
	virtual ~Identifier() {}
	IdentifierDomain* operator->() { return &Domain_; }
	const IdentifierDomain* operator->() const { return &Domain_; }
	bool operator==(const Identifier& other) const;
	bool operator<(const Identifier& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL OrderSiteArc : public Domain {
private:
	void pushColumns();
public:
	struct OrderSiteArcDomain {
		Column code;
		Column name;
		Column link;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		OrderSiteArcDomain( const SimpleTable& table )
		: code(table, "code")
		, name(table, "name")
		, link(table, "link")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Str code;
	Str name;
	Str link;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	OrderSiteArc();
	OrderSiteArc(const OrderSiteArc& copy);
	OrderSiteArc& operator=(const OrderSiteArc& copy);
	virtual ~OrderSiteArc() {}
	OrderSiteArcDomain* operator->() { return &Domain_; }
	const OrderSiteArcDomain* operator->() const { return &Domain_; }
	bool operator==(const OrderSiteArc& other) const;
	bool operator<(const OrderSiteArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL OrderSite : public Domain {
private:
	void pushColumns();
public:
	struct OrderSiteDomain {
		Column code;
		Column name;
		Column link;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		OrderSiteDomain( const SimpleTable& table )
		: code(table, "code")
		, name(table, "name")
		, link(table, "link")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Str code;
	Str name;
	Str link;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	OrderSite();
	explicit OrderSite(Str __code);
	OrderSite(const OrderSite& copy);
	OrderSite& operator=(const OrderSite& copy);
	virtual ~OrderSite() {}
	OrderSiteDomain* operator->() { return &Domain_; }
	const OrderSiteDomain* operator->() const { return &Domain_; }
	bool operator==(const OrderSite& other) const;
	bool operator<(const OrderSite& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL PersonOrderArc : public Domain {
private:
	void pushColumns();
public:
	struct PersonOrderArcDomain {
		Column id;
		Column personid;
		Column filed_at;
		Column amount;
		Column status;
		Column promocode;
		Column final_amount;
		Column sitecode;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		PersonOrderArcDomain( const SimpleTable& table )
		: id(table, "id")
		, personid(table, "personid")
		, filed_at(table, "filed_at")
		, amount(table, "amount")
		, status(table, "status")
		, promocode(table, "promocode")
		, final_amount(table, "final_amount")
		, sitecode(table, "sitecode")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Int id;
	Int personid;
	Time filed_at;
	Decimal amount;
	Str status;
	Str promocode;
	Decimal final_amount;
	Str sitecode;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	PersonOrderArc();
	PersonOrderArc(const PersonOrderArc& copy);
	PersonOrderArc& operator=(const PersonOrderArc& copy);
	virtual ~PersonOrderArc() {}
	PersonOrderArcDomain* operator->() { return &Domain_; }
	const PersonOrderArcDomain* operator->() const { return &Domain_; }
	bool operator==(const PersonOrderArc& other) const;
	bool operator<(const PersonOrderArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL PersonOrder : public Domain {
private:
	void pushColumns();
public:
	struct PersonOrderDomain {
		Column id;
		Column personid;
		Column filed_at;
		Column amount;
		Column status;
		Column promocode;
		Column final_amount;
		Column sitecode;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		PersonOrderDomain( const SimpleTable& table )
		: id(table, "id")
		, personid(table, "personid")
		, filed_at(table, "filed_at")
		, amount(table, "amount")
		, status(table, "status")
		, promocode(table, "promocode")
		, final_amount(table, "final_amount")
		, sitecode(table, "sitecode")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Int id;
	Int personid;
	Time filed_at;
	Decimal amount;
	Str status;
	Str promocode;
	Decimal final_amount;
	Str sitecode;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	PersonOrder();
	explicit PersonOrder(Int __id);
	PersonOrder(const PersonOrder& copy);
	PersonOrder& operator=(const PersonOrder& copy);
	virtual ~PersonOrder() {}
	PersonOrderDomain* operator->() { return &Domain_; }
	const PersonOrderDomain* operator->() const { return &Domain_; }
	bool operator==(const PersonOrder& other) const;
	bool operator<(const PersonOrder& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL ParkingAreaArc : public Domain {
private:
	void pushColumns();
public:
	struct ParkingAreaArcDomain {
		Column id;
		Column name;
		Column spdgateid;
		Column enter_timeout;
		Column exit_timeout;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		ParkingAreaArcDomain( const SimpleTable& table )
		: id(table, "id")
		, name(table, "name")
		, spdgateid(table, "spdgateid")
		, enter_timeout(table, "enter_timeout")
		, exit_timeout(table, "exit_timeout")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Int id;
	Str name;
	Int spdgateid;
	Int enter_timeout;
	Int exit_timeout;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	ParkingAreaArc();
	ParkingAreaArc(const ParkingAreaArc& copy);
	ParkingAreaArc& operator=(const ParkingAreaArc& copy);
	virtual ~ParkingAreaArc() {}
	ParkingAreaArcDomain* operator->() { return &Domain_; }
	const ParkingAreaArcDomain* operator->() const { return &Domain_; }
	bool operator==(const ParkingAreaArc& other) const;
	bool operator<(const ParkingAreaArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL ParkingArea : public Domain {
private:
	void pushColumns();
public:
	struct ParkingAreaDomain {
		Column id;
		Column name;
		Column spdgateid;
		Column enter_timeout;
		Column exit_timeout;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		ParkingAreaDomain( const SimpleTable& table )
		: id(table, "id")
		, name(table, "name")
		, spdgateid(table, "spdgateid")
		, enter_timeout(table, "enter_timeout")
		, exit_timeout(table, "exit_timeout")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Int id;
	Str name;
	Int spdgateid;
	Int enter_timeout;
	Int exit_timeout;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	ParkingArea();
	explicit ParkingArea(Int __id);
	ParkingArea(const ParkingArea& copy);
	ParkingArea& operator=(const ParkingArea& copy);
	virtual ~ParkingArea() {}
	ParkingAreaDomain* operator->() { return &Domain_; }
	const ParkingAreaDomain* operator->() const { return &Domain_; }
	bool operator==(const ParkingArea& other) const;
	bool operator<(const ParkingArea& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL ParkingTimerArc : public Domain {
private:
	void pushColumns();
public:
	struct ParkingTimerArcDomain {
		Column orderid;
		Column end_time;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		ParkingTimerArcDomain( const SimpleTable& table )
		: orderid(table, "orderid")
		, end_time(table, "end_time")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Int orderid;
	Time end_time;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	ParkingTimerArc();
	ParkingTimerArc(const ParkingTimerArc& copy);
	ParkingTimerArc& operator=(const ParkingTimerArc& copy);
	virtual ~ParkingTimerArc() {}
	ParkingTimerArcDomain* operator->() { return &Domain_; }
	const ParkingTimerArcDomain* operator->() const { return &Domain_; }
	bool operator==(const ParkingTimerArc& other) const;
	bool operator<(const ParkingTimerArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL ParkingTimer : public Domain {
private:
	void pushColumns();
public:
	struct ParkingTimerDomain {
		Column orderid;
		Column end_time;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		ParkingTimerDomain( const SimpleTable& table )
		: orderid(table, "orderid")
		, end_time(table, "end_time")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Int orderid;
	Time end_time;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	ParkingTimer();
	explicit ParkingTimer(Int __orderid);
	ParkingTimer(const ParkingTimer& copy);
	ParkingTimer& operator=(const ParkingTimer& copy);
	virtual ~ParkingTimer() {}
	ParkingTimerDomain* operator->() { return &Domain_; }
	const ParkingTimerDomain* operator->() const { return &Domain_; }
	bool operator==(const ParkingTimer& other) const;
	bool operator<(const ParkingTimer& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL ParkingOrderlineArc : public Domain {
private:
	void pushColumns();
public:
	struct ParkingOrderlineArcDomain {
		Column id;
		Column orderid;
		Column identifierid;
		Column time_from;
		Column time_upto;
		Column price;
		Column areaid;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		ParkingOrderlineArcDomain( const SimpleTable& table )
		: id(table, "id")
		, orderid(table, "orderid")
		, identifierid(table, "identifierid")
		, time_from(table, "time_from")
		, time_upto(table, "time_upto")
		, price(table, "price")
		, areaid(table, "areaid")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Int id;
	Int orderid;
	Int identifierid;
	Time time_from;
	Time time_upto;
	Decimal price;
	Int areaid;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	ParkingOrderlineArc();
	ParkingOrderlineArc(const ParkingOrderlineArc& copy);
	ParkingOrderlineArc& operator=(const ParkingOrderlineArc& copy);
	virtual ~ParkingOrderlineArc() {}
	ParkingOrderlineArcDomain* operator->() { return &Domain_; }
	const ParkingOrderlineArcDomain* operator->() const { return &Domain_; }
	bool operator==(const ParkingOrderlineArc& other) const;
	bool operator<(const ParkingOrderlineArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL ParkingOrderline : public Domain {
private:
	void pushColumns();
public:
	struct ParkingOrderlineDomain {
		Column id;
		Column orderid;
		Column identifierid;
		Column time_from;
		Column time_upto;
		Column price;
		Column areaid;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		ParkingOrderlineDomain( const SimpleTable& table )
		: id(table, "id")
		, orderid(table, "orderid")
		, identifierid(table, "identifierid")
		, time_from(table, "time_from")
		, time_upto(table, "time_upto")
		, price(table, "price")
		, areaid(table, "areaid")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Int id;
	Int orderid;
	Int identifierid;
	Time time_from;
	Time time_upto;
	Decimal price;
	Int areaid;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	ParkingOrderline();
	explicit ParkingOrderline(Int __id);
	ParkingOrderline(const ParkingOrderline& copy);
	ParkingOrderline& operator=(const ParkingOrderline& copy);
	virtual ~ParkingOrderline() {}
	ParkingOrderlineDomain* operator->() { return &Domain_; }
	const ParkingOrderlineDomain* operator->() const { return &Domain_; }
	bool operator==(const ParkingOrderline& other) const;
	bool operator<(const ParkingOrderline& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL PersonOrderFiscalDocArc : public Domain {
private:
	void pushColumns();
public:
	struct PersonOrderFiscalDocArcDomain {
		Column orderid;
		Column fiscal_docid;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		PersonOrderFiscalDocArcDomain( const SimpleTable& table )
		: orderid(table, "orderid")
		, fiscal_docid(table, "fiscal_docid")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Int orderid;
	Int fiscal_docid;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	PersonOrderFiscalDocArc();
	PersonOrderFiscalDocArc(const PersonOrderFiscalDocArc& copy);
	PersonOrderFiscalDocArc& operator=(const PersonOrderFiscalDocArc& copy);
	virtual ~PersonOrderFiscalDocArc() {}
	PersonOrderFiscalDocArcDomain* operator->() { return &Domain_; }
	const PersonOrderFiscalDocArcDomain* operator->() const { return &Domain_; }
	bool operator==(const PersonOrderFiscalDocArc& other) const;
	bool operator<(const PersonOrderFiscalDocArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL PersonOrderFiscalDoc : public Domain {
private:
	void pushColumns();
public:
	struct PersonOrderFiscalDocDomain {
		Column orderid;
		Column fiscal_docid;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		PersonOrderFiscalDocDomain( const SimpleTable& table )
		: orderid(table, "orderid")
		, fiscal_docid(table, "fiscal_docid")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Int orderid;
	Int fiscal_docid;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	PersonOrderFiscalDoc();
	explicit PersonOrderFiscalDoc(Int __orderid, Int __fiscal_docid);
	PersonOrderFiscalDoc(const PersonOrderFiscalDoc& copy);
	PersonOrderFiscalDoc& operator=(const PersonOrderFiscalDoc& copy);
	virtual ~PersonOrderFiscalDoc() {}
	PersonOrderFiscalDocDomain* operator->() { return &Domain_; }
	const PersonOrderFiscalDocDomain* operator->() const { return &Domain_; }
	bool operator==(const PersonOrderFiscalDoc& other) const;
	bool operator<(const PersonOrderFiscalDoc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL PersonIdentifierOrderlineArc : public Domain {
private:
	void pushColumns();
public:
	struct PersonIdentifierOrderlineArcDomain {
		Column orderid;
		Column identifierid;
		Column price;
		Column service_rulename;
		Column make_transaction;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		PersonIdentifierOrderlineArcDomain( const SimpleTable& table )
		: orderid(table, "orderid")
		, identifierid(table, "identifierid")
		, price(table, "price")
		, service_rulename(table, "service_rulename")
		, make_transaction(table, "make_transaction")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Int orderid;
	Int identifierid;
	Decimal price;
	Str service_rulename;
	Str make_transaction;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	PersonIdentifierOrderlineArc();
	PersonIdentifierOrderlineArc(const PersonIdentifierOrderlineArc& copy);
	PersonIdentifierOrderlineArc& operator=(const PersonIdentifierOrderlineArc& copy);
	virtual ~PersonIdentifierOrderlineArc() {}
	PersonIdentifierOrderlineArcDomain* operator->() { return &Domain_; }
	const PersonIdentifierOrderlineArcDomain* operator->() const { return &Domain_; }
	bool operator==(const PersonIdentifierOrderlineArc& other) const;
	bool operator<(const PersonIdentifierOrderlineArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL PersonIdentifierOrderline : public Domain {
private:
	void pushColumns();
public:
	struct PersonIdentifierOrderlineDomain {
		Column orderid;
		Column identifierid;
		Column price;
		Column service_rulename;
		Column make_transaction;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		PersonIdentifierOrderlineDomain( const SimpleTable& table )
		: orderid(table, "orderid")
		, identifierid(table, "identifierid")
		, price(table, "price")
		, service_rulename(table, "service_rulename")
		, make_transaction(table, "make_transaction")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Int orderid;
	Int identifierid;
	Decimal price;
	Str service_rulename;
	Str make_transaction;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	PersonIdentifierOrderline();
	explicit PersonIdentifierOrderline(Int __orderid, Int __identifierid);
	PersonIdentifierOrderline(const PersonIdentifierOrderline& copy);
	PersonIdentifierOrderline& operator=(const PersonIdentifierOrderline& copy);
	virtual ~PersonIdentifierOrderline() {}
	PersonIdentifierOrderlineDomain* operator->() { return &Domain_; }
	const PersonIdentifierOrderlineDomain* operator->() const { return &Domain_; }
	bool operator==(const PersonIdentifierOrderline& other) const;
	bool operator<(const PersonIdentifierOrderline& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL AgentIdentifierOrderlineArc : public Domain {
private:
	void pushColumns();
public:
	struct AgentIdentifierOrderlineArcDomain {
		Column orderid;
		Column identifierid;
		Column price;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		AgentIdentifierOrderlineArcDomain( const SimpleTable& table )
		: orderid(table, "orderid")
		, identifierid(table, "identifierid")
		, price(table, "price")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Int orderid;
	Int identifierid;
	Decimal price;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	AgentIdentifierOrderlineArc();
	AgentIdentifierOrderlineArc(const AgentIdentifierOrderlineArc& copy);
	AgentIdentifierOrderlineArc& operator=(const AgentIdentifierOrderlineArc& copy);
	virtual ~AgentIdentifierOrderlineArc() {}
	AgentIdentifierOrderlineArcDomain* operator->() { return &Domain_; }
	const AgentIdentifierOrderlineArcDomain* operator->() const { return &Domain_; }
	bool operator==(const AgentIdentifierOrderlineArc& other) const;
	bool operator<(const AgentIdentifierOrderlineArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL AgentIdentifierOrderline : public Domain {
private:
	void pushColumns();
public:
	struct AgentIdentifierOrderlineDomain {
		Column orderid;
		Column identifierid;
		Column price;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		AgentIdentifierOrderlineDomain( const SimpleTable& table )
		: orderid(table, "orderid")
		, identifierid(table, "identifierid")
		, price(table, "price")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Int orderid;
	Int identifierid;
	Decimal price;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	AgentIdentifierOrderline();
	explicit AgentIdentifierOrderline(Int __orderid, Int __identifierid);
	AgentIdentifierOrderline(const AgentIdentifierOrderline& copy);
	AgentIdentifierOrderline& operator=(const AgentIdentifierOrderline& copy);
	virtual ~AgentIdentifierOrderline() {}
	AgentIdentifierOrderlineDomain* operator->() { return &Domain_; }
	const AgentIdentifierOrderlineDomain* operator->() const { return &Domain_; }
	bool operator==(const AgentIdentifierOrderline& other) const;
	bool operator<(const AgentIdentifierOrderline& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL PersonInvoiceArc : public Domain {
private:
	void pushColumns();
public:
	struct PersonInvoiceArcDomain {
		Column id;
		Column orderid;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		PersonInvoiceArcDomain( const SimpleTable& table )
		: id(table, "id")
		, orderid(table, "orderid")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Int id;
	Int orderid;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	PersonInvoiceArc();
	PersonInvoiceArc(const PersonInvoiceArc& copy);
	PersonInvoiceArc& operator=(const PersonInvoiceArc& copy);
	virtual ~PersonInvoiceArc() {}
	PersonInvoiceArcDomain* operator->() { return &Domain_; }
	const PersonInvoiceArcDomain* operator->() const { return &Domain_; }
	bool operator==(const PersonInvoiceArc& other) const;
	bool operator<(const PersonInvoiceArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL PersonInvoice : public Domain {
private:
	void pushColumns();
public:
	struct PersonInvoiceDomain {
		Column id;
		Column orderid;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		PersonInvoiceDomain( const SimpleTable& table )
		: id(table, "id")
		, orderid(table, "orderid")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Int id;
	Int orderid;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	PersonInvoice();
	explicit PersonInvoice(Int __id);
	PersonInvoice(const PersonInvoice& copy);
	PersonInvoice& operator=(const PersonInvoice& copy);
	virtual ~PersonInvoice() {}
	PersonInvoiceDomain* operator->() { return &Domain_; }
	const PersonInvoiceDomain* operator->() const { return &Domain_; }
	bool operator==(const PersonInvoice& other) const;
	bool operator<(const PersonInvoice& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL IdentifierInvoiceArc : public Domain {
private:
	void pushColumns();
public:
	struct IdentifierInvoiceArcDomain {
		Column id;
		Column identifierid;
		Column fiscal_docid;
		Column eid;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		IdentifierInvoiceArcDomain( const SimpleTable& table )
		: id(table, "id")
		, identifierid(table, "identifierid")
		, fiscal_docid(table, "fiscal_docid")
		, eid(table, "eid")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Int id;
	Int identifierid;
	Int fiscal_docid;
	Str eid;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	IdentifierInvoiceArc();
	IdentifierInvoiceArc(const IdentifierInvoiceArc& copy);
	IdentifierInvoiceArc& operator=(const IdentifierInvoiceArc& copy);
	virtual ~IdentifierInvoiceArc() {}
	IdentifierInvoiceArcDomain* operator->() { return &Domain_; }
	const IdentifierInvoiceArcDomain* operator->() const { return &Domain_; }
	bool operator==(const IdentifierInvoiceArc& other) const;
	bool operator<(const IdentifierInvoiceArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL IdentifierInvoice : public Domain {
private:
	void pushColumns();
public:
	struct IdentifierInvoiceDomain {
		Column id;
		Column identifierid;
		Column fiscal_docid;
		Column eid;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		IdentifierInvoiceDomain( const SimpleTable& table )
		: id(table, "id")
		, identifierid(table, "identifierid")
		, fiscal_docid(table, "fiscal_docid")
		, eid(table, "eid")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Int id;
	Int identifierid;
	Int fiscal_docid;
	Str eid;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	IdentifierInvoice();
	explicit IdentifierInvoice(Int __id);
	IdentifierInvoice(const IdentifierInvoice& copy);
	IdentifierInvoice& operator=(const IdentifierInvoice& copy);
	virtual ~IdentifierInvoice() {}
	IdentifierInvoiceDomain* operator->() { return &Domain_; }
	const IdentifierInvoiceDomain* operator->() const { return &Domain_; }
	bool operator==(const IdentifierInvoice& other) const;
	bool operator<(const IdentifierInvoice& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL BaloonProviderArc : public Domain {
private:
	void pushColumns();
public:
	struct BaloonProviderArcDomain {
		Column default_paygateid;
		Column default_fiscalgateid;
		Column code_conversion_algo;
		Column agent_commission;
		Column ident_sale_price;
		Column timeout_before;
		Column sun;
		Column mon;
		Column tue;
		Column wed;
		Column thu;
		Column fri;
		Column sat;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		BaloonProviderArcDomain( const SimpleTable& table )
		: default_paygateid(table, "default_paygateid")
		, default_fiscalgateid(table, "default_fiscalgateid")
		, code_conversion_algo(table, "code_conversion_algo")
		, agent_commission(table, "agent_commission")
		, ident_sale_price(table, "ident_sale_price")
		, timeout_before(table, "timeout_before")
		, sun(table, "sun")
		, mon(table, "mon")
		, tue(table, "tue")
		, wed(table, "wed")
		, thu(table, "thu")
		, fri(table, "fri")
		, sat(table, "sat")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Int default_paygateid;
	Int default_fiscalgateid;
	Str code_conversion_algo;
	Double agent_commission;
	Decimal ident_sale_price;
	Int timeout_before;
	Str sun;
	Str mon;
	Str tue;
	Str wed;
	Str thu;
	Str fri;
	Str sat;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	BaloonProviderArc();
	BaloonProviderArc(const BaloonProviderArc& copy);
	BaloonProviderArc& operator=(const BaloonProviderArc& copy);
	virtual ~BaloonProviderArc() {}
	BaloonProviderArcDomain* operator->() { return &Domain_; }
	const BaloonProviderArcDomain* operator->() const { return &Domain_; }
	bool operator==(const BaloonProviderArc& other) const;
	bool operator<(const BaloonProviderArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL BaloonProvider : public Domain {
private:
	void pushColumns();
public:
	struct BaloonProviderDomain {
		Column default_paygateid;
		Column default_fiscalgateid;
		Column code_conversion_algo;
		Column agent_commission;
		Column ident_sale_price;
		Column timeout_before;
		Column sun;
		Column mon;
		Column tue;
		Column wed;
		Column thu;
		Column fri;
		Column sat;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		BaloonProviderDomain( const SimpleTable& table )
		: default_paygateid(table, "default_paygateid")
		, default_fiscalgateid(table, "default_fiscalgateid")
		, code_conversion_algo(table, "code_conversion_algo")
		, agent_commission(table, "agent_commission")
		, ident_sale_price(table, "ident_sale_price")
		, timeout_before(table, "timeout_before")
		, sun(table, "sun")
		, mon(table, "mon")
		, tue(table, "tue")
		, wed(table, "wed")
		, thu(table, "thu")
		, fri(table, "fri")
		, sat(table, "sat")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Int default_paygateid;
	Int default_fiscalgateid;
	Str code_conversion_algo;
	Double agent_commission;
	Decimal ident_sale_price;
	Int timeout_before;
	Str sun;
	Str mon;
	Str tue;
	Str wed;
	Str thu;
	Str fri;
	Str sat;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	BaloonProvider();
	BaloonProvider(const BaloonProvider& copy);
	BaloonProvider& operator=(const BaloonProvider& copy);
	virtual ~BaloonProvider() {}
	BaloonProviderDomain* operator->() { return &Domain_; }
	const BaloonProviderDomain* operator->() const { return &Domain_; }
	bool operator==(const BaloonProvider& other) const;
	bool operator<(const BaloonProvider& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL ExternalResourceArc : public Domain {
private:
	void pushColumns();
public:
	struct ExternalResourceArcDomain {
		Column id;
		Column name;
		Column uri;
		Column description;
		Column enabled;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		ExternalResourceArcDomain( const SimpleTable& table )
		: id(table, "id")
		, name(table, "name")
		, uri(table, "uri")
		, description(table, "description")
		, enabled(table, "enabled")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Int id;
	Str name;
	Str uri;
	Str description;
	Str enabled;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	ExternalResourceArc();
	ExternalResourceArc(const ExternalResourceArc& copy);
	ExternalResourceArc& operator=(const ExternalResourceArc& copy);
	virtual ~ExternalResourceArc() {}
	ExternalResourceArcDomain* operator->() { return &Domain_; }
	const ExternalResourceArcDomain* operator->() const { return &Domain_; }
	bool operator==(const ExternalResourceArc& other) const;
	bool operator<(const ExternalResourceArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL ExternalResource : public Domain {
private:
	void pushColumns();
public:
	struct ExternalResourceDomain {
		Column id;
		Column name;
		Column uri;
		Column description;
		Column enabled;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		ExternalResourceDomain( const SimpleTable& table )
		: id(table, "id")
		, name(table, "name")
		, uri(table, "uri")
		, description(table, "description")
		, enabled(table, "enabled")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Int id;
	Str name;
	Str uri;
	Str description;
	Str enabled;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	ExternalResource();
	explicit ExternalResource(Int __id);
	ExternalResource(const ExternalResource& copy);
	ExternalResource& operator=(const ExternalResource& copy);
	virtual ~ExternalResource() {}
	ExternalResourceDomain* operator->() { return &Domain_; }
	const ExternalResourceDomain* operator->() const { return &Domain_; }
	bool operator==(const ExternalResource& other) const;
	bool operator<(const ExternalResource& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL AgentArc : public Domain {
private:
	void pushColumns();
public:
	struct AgentArcDomain {
		Column id;
		Column userTokenID;
		Column name;
		Column phone;
		Column email;
		Column comments;
		Column commission;
		Column is_default;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		AgentArcDomain( const SimpleTable& table )
		: id(table, "id")
		, userTokenID(table, "userTokenID")
		, name(table, "name")
		, phone(table, "phone")
		, email(table, "email")
		, comments(table, "comments")
		, commission(table, "commission")
		, is_default(table, "is_default")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Int id;
	Int64 userTokenID;
	Str name;
	Str phone;
	Str email;
	Str comments;
	Double commission;
	Str is_default;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	AgentArc();
	AgentArc(const AgentArc& copy);
	AgentArc& operator=(const AgentArc& copy);
	virtual ~AgentArc() {}
	AgentArcDomain* operator->() { return &Domain_; }
	const AgentArcDomain* operator->() const { return &Domain_; }
	bool operator==(const AgentArc& other) const;
	bool operator<(const AgentArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL Agent : public Domain {
private:
	void pushColumns();
public:
	struct AgentDomain {
		Column id;
		Column userTokenID;
		Column name;
		Column phone;
		Column email;
		Column comments;
		Column commission;
		Column is_default;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		AgentDomain( const SimpleTable& table )
		: id(table, "id")
		, userTokenID(table, "userTokenID")
		, name(table, "name")
		, phone(table, "phone")
		, email(table, "email")
		, comments(table, "comments")
		, commission(table, "commission")
		, is_default(table, "is_default")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Int id;
	Int64 userTokenID;
	Str name;
	Str phone;
	Str email;
	Str comments;
	Double commission;
	Str is_default;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Agent();
	explicit Agent(Int __id);
	Agent(const Agent& copy);
	Agent& operator=(const Agent& copy);
	virtual ~Agent() {}
	AgentDomain* operator->() { return &Domain_; }
	const AgentDomain* operator->() const { return &Domain_; }
	bool operator==(const Agent& other) const;
	bool operator<(const Agent& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL AgentOrderArc : public Domain {
private:
	void pushColumns();
public:
	struct AgentOrderArcDomain {
		Column id;
		Column agentid;
		Column code;
		Column amount;
		Column commission;
		Column filed_at;
		Column comment;
		Column status;
		Column AgentOrderType;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		AgentOrderArcDomain( const SimpleTable& table )
		: id(table, "id")
		, agentid(table, "agentid")
		, code(table, "code")
		, amount(table, "amount")
		, commission(table, "commission")
		, filed_at(table, "filed_at")
		, comment(table, "comment")
		, status(table, "status")
		, AgentOrderType(table, "AgentOrderType")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Int id;
	Int agentid;
	Str code;
	Decimal amount;
	Decimal commission;
	Time filed_at;
	Str comment;
	Str status;
	Str AgentOrderType;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	AgentOrderArc();
	AgentOrderArc(const AgentOrderArc& copy);
	AgentOrderArc& operator=(const AgentOrderArc& copy);
	virtual ~AgentOrderArc() {}
	AgentOrderArcDomain* operator->() { return &Domain_; }
	const AgentOrderArcDomain* operator->() const { return &Domain_; }
	bool operator==(const AgentOrderArc& other) const;
	bool operator<(const AgentOrderArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL AgentOrder : public Domain {
private:
	void pushColumns();
public:
	struct AgentOrderDomain {
		Column id;
		Column agentid;
		Column code;
		Column amount;
		Column commission;
		Column filed_at;
		Column comment;
		Column status;
		Column AgentOrderType;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		AgentOrderDomain( const SimpleTable& table )
		: id(table, "id")
		, agentid(table, "agentid")
		, code(table, "code")
		, amount(table, "amount")
		, commission(table, "commission")
		, filed_at(table, "filed_at")
		, comment(table, "comment")
		, status(table, "status")
		, AgentOrderType(table, "AgentOrderType")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Int id;
	Int agentid;
	Str code;
	Decimal amount;
	Decimal commission;
	Time filed_at;
	Str comment;
	Str status;
	Str AgentOrderType;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	AgentOrder();
	explicit AgentOrder(Int __id);
	AgentOrder(const AgentOrder& copy);
	AgentOrder& operator=(const AgentOrder& copy);
	virtual ~AgentOrder() {}
	AgentOrderDomain* operator->() { return &Domain_; }
	const AgentOrderDomain* operator->() const { return &Domain_; }
	bool operator==(const AgentOrder& other) const;
	bool operator<(const AgentOrder& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL IdentifierAgentOrderArc : public Domain {
private:
	void pushColumns();
public:
	struct IdentifierAgentOrderArcDomain {
		Column id;
		Column valid_from;
		Column valid_to;
		Column permanent_rulename;
		Column categoryname;
		Column ratename;
		Column other_card_code;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		IdentifierAgentOrderArcDomain( const SimpleTable& table )
		: id(table, "id")
		, valid_from(table, "valid_from")
		, valid_to(table, "valid_to")
		, permanent_rulename(table, "permanent_rulename")
		, categoryname(table, "categoryname")
		, ratename(table, "ratename")
		, other_card_code(table, "other_card_code")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Int id;
	Time valid_from;
	Time valid_to;
	Str permanent_rulename;
	Str categoryname;
	Str ratename;
	Str other_card_code;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	IdentifierAgentOrderArc();
	IdentifierAgentOrderArc(const IdentifierAgentOrderArc& copy);
	IdentifierAgentOrderArc& operator=(const IdentifierAgentOrderArc& copy);
	virtual ~IdentifierAgentOrderArc() {}
	IdentifierAgentOrderArcDomain* operator->() { return &Domain_; }
	const IdentifierAgentOrderArcDomain* operator->() const { return &Domain_; }
	bool operator==(const IdentifierAgentOrderArc& other) const;
	bool operator<(const IdentifierAgentOrderArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL IdentifierAgentOrder : public Domain {
private:
	void pushColumns();
public:
	struct IdentifierAgentOrderDomain {
		Column id;
		Column valid_from;
		Column valid_to;
		Column permanent_rulename;
		Column categoryname;
		Column ratename;
		Column other_card_code;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		IdentifierAgentOrderDomain( const SimpleTable& table )
		: id(table, "id")
		, valid_from(table, "valid_from")
		, valid_to(table, "valid_to")
		, permanent_rulename(table, "permanent_rulename")
		, categoryname(table, "categoryname")
		, ratename(table, "ratename")
		, other_card_code(table, "other_card_code")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Int id;
	Time valid_from;
	Time valid_to;
	Str permanent_rulename;
	Str categoryname;
	Str ratename;
	Str other_card_code;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	IdentifierAgentOrder();
	explicit IdentifierAgentOrder(Int __id);
	IdentifierAgentOrder(const IdentifierAgentOrder& copy);
	IdentifierAgentOrder& operator=(const IdentifierAgentOrder& copy);
	virtual ~IdentifierAgentOrder() {}
	IdentifierAgentOrderDomain* operator->() { return &Domain_; }
	const IdentifierAgentOrderDomain* operator->() const { return &Domain_; }
	bool operator==(const IdentifierAgentOrder& other) const;
	bool operator<(const IdentifierAgentOrder& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL PackageAgentOrderArc : public Domain {
private:
	void pushColumns();
public:
	struct PackageAgentOrderArcDomain {
		Column id;
		Column service_rulename;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		PackageAgentOrderArcDomain( const SimpleTable& table )
		: id(table, "id")
		, service_rulename(table, "service_rulename")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Int id;
	Str service_rulename;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	PackageAgentOrderArc();
	PackageAgentOrderArc(const PackageAgentOrderArc& copy);
	PackageAgentOrderArc& operator=(const PackageAgentOrderArc& copy);
	virtual ~PackageAgentOrderArc() {}
	PackageAgentOrderArcDomain* operator->() { return &Domain_; }
	const PackageAgentOrderArcDomain* operator->() const { return &Domain_; }
	bool operator==(const PackageAgentOrderArc& other) const;
	bool operator<(const PackageAgentOrderArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL PackageAgentOrder : public Domain {
private:
	void pushColumns();
public:
	struct PackageAgentOrderDomain {
		Column id;
		Column service_rulename;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		PackageAgentOrderDomain( const SimpleTable& table )
		: id(table, "id")
		, service_rulename(table, "service_rulename")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Int id;
	Str service_rulename;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	PackageAgentOrder();
	explicit PackageAgentOrder(Int __id);
	PackageAgentOrder(const PackageAgentOrder& copy);
	PackageAgentOrder& operator=(const PackageAgentOrder& copy);
	virtual ~PackageAgentOrder() {}
	PackageAgentOrderDomain* operator->() { return &Domain_; }
	const PackageAgentOrderDomain* operator->() const { return &Domain_; }
	bool operator==(const PackageAgentOrder& other) const;
	bool operator<(const PackageAgentOrder& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL AgentInvoiceArc : public Domain {
private:
	void pushColumns();
public:
	struct AgentInvoiceArcDomain {
		Column id;
		Column orderid;
		Column eid;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		AgentInvoiceArcDomain( const SimpleTable& table )
		: id(table, "id")
		, orderid(table, "orderid")
		, eid(table, "eid")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Int id;
	Int orderid;
	Str eid;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	AgentInvoiceArc();
	AgentInvoiceArc(const AgentInvoiceArc& copy);
	AgentInvoiceArc& operator=(const AgentInvoiceArc& copy);
	virtual ~AgentInvoiceArc() {}
	AgentInvoiceArcDomain* operator->() { return &Domain_; }
	const AgentInvoiceArcDomain* operator->() const { return &Domain_; }
	bool operator==(const AgentInvoiceArc& other) const;
	bool operator<(const AgentInvoiceArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL AgentInvoice : public Domain {
private:
	void pushColumns();
public:
	struct AgentInvoiceDomain {
		Column id;
		Column orderid;
		Column eid;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		AgentInvoiceDomain( const SimpleTable& table )
		: id(table, "id")
		, orderid(table, "orderid")
		, eid(table, "eid")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Int id;
	Int orderid;
	Str eid;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	AgentInvoice();
	explicit AgentInvoice(Int __id);
	AgentInvoice(const AgentInvoice& copy);
	AgentInvoice& operator=(const AgentInvoice& copy);
	virtual ~AgentInvoice() {}
	AgentInvoiceDomain* operator->() { return &Domain_; }
	const AgentInvoiceDomain* operator->() const { return &Domain_; }
	bool operator==(const AgentInvoice& other) const;
	bool operator<(const AgentInvoice& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL AccountArc : public Domain {
private:
	void pushColumns();
public:
	struct AccountArcDomain {
		Column currency;
		Column personid;
		Column valid;
		Column balance;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		AccountArcDomain( const SimpleTable& table )
		: currency(table, "currency")
		, personid(table, "personid")
		, valid(table, "valid")
		, balance(table, "balance")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Str currency;
	Int personid;
	Str valid;
	Decimal balance;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	AccountArc();
	AccountArc(const AccountArc& copy);
	AccountArc& operator=(const AccountArc& copy);
	virtual ~AccountArc() {}
	AccountArcDomain* operator->() { return &Domain_; }
	const AccountArcDomain* operator->() const { return &Domain_; }
	bool operator==(const AccountArc& other) const;
	bool operator<(const AccountArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL Account : public Domain {
private:
	void pushColumns();
public:
	struct AccountDomain {
		Column currency;
		Column personid;
		Column valid;
		Column balance;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		AccountDomain( const SimpleTable& table )
		: currency(table, "currency")
		, personid(table, "personid")
		, valid(table, "valid")
		, balance(table, "balance")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Str currency;
	Int personid;
	Str valid;
	Decimal balance;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Account();
	explicit Account(Str __currency, Int __personid);
	Account(const Account& copy);
	Account& operator=(const Account& copy);
	virtual ~Account() {}
	AccountDomain* operator->() { return &Domain_; }
	const AccountDomain* operator->() const { return &Domain_; }
	bool operator==(const Account& other) const;
	bool operator<(const Account& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL PackageOrderArc : public Domain {
private:
	void pushColumns();
public:
	struct PackageOrderArcDomain {
		Column id;
		Column identifierid;
		Column service_rulename;
		Column filed_at;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		PackageOrderArcDomain( const SimpleTable& table )
		: id(table, "id")
		, identifierid(table, "identifierid")
		, service_rulename(table, "service_rulename")
		, filed_at(table, "filed_at")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Int id;
	Int identifierid;
	Str service_rulename;
	Time filed_at;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	PackageOrderArc();
	PackageOrderArc(const PackageOrderArc& copy);
	PackageOrderArc& operator=(const PackageOrderArc& copy);
	virtual ~PackageOrderArc() {}
	PackageOrderArcDomain* operator->() { return &Domain_; }
	const PackageOrderArcDomain* operator->() const { return &Domain_; }
	bool operator==(const PackageOrderArc& other) const;
	bool operator<(const PackageOrderArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL PackageOrder : public Domain {
private:
	void pushColumns();
public:
	struct PackageOrderDomain {
		Column id;
		Column identifierid;
		Column service_rulename;
		Column filed_at;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		PackageOrderDomain( const SimpleTable& table )
		: id(table, "id")
		, identifierid(table, "identifierid")
		, service_rulename(table, "service_rulename")
		, filed_at(table, "filed_at")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Int id;
	Int identifierid;
	Str service_rulename;
	Time filed_at;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	PackageOrder();
	explicit PackageOrder(Int __id);
	PackageOrder(const PackageOrder& copy);
	PackageOrder& operator=(const PackageOrder& copy);
	virtual ~PackageOrder() {}
	PackageOrderDomain* operator->() { return &Domain_; }
	const PackageOrderDomain* operator->() const { return &Domain_; }
	bool operator==(const PackageOrder& other) const;
	bool operator<(const PackageOrder& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL TransactionArc : public Domain {
private:
	void pushColumns();
public:
	struct TransactionArcDomain {
		Column id;
		Column personid;
		Column agentid;
		Column currency;
		Column eid;
		Column amount;
		Column status;
		Column comment;
		Column in_sync;
		Column valid_until;
		Column meta;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		TransactionArcDomain( const SimpleTable& table )
		: id(table, "id")
		, personid(table, "personid")
		, agentid(table, "agentid")
		, currency(table, "currency")
		, eid(table, "eid")
		, amount(table, "amount")
		, status(table, "status")
		, comment(table, "comment")
		, in_sync(table, "in_sync")
		, valid_until(table, "valid_until")
		, meta(table, "meta")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Int id;
	Int personid;
	Int agentid;
	Str currency;
	Str eid;
	Decimal amount;
	Str status;
	Str comment;
	Str in_sync;
	Time valid_until;
	Str meta;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	TransactionArc();
	TransactionArc(const TransactionArc& copy);
	TransactionArc& operator=(const TransactionArc& copy);
	virtual ~TransactionArc() {}
	TransactionArcDomain* operator->() { return &Domain_; }
	const TransactionArcDomain* operator->() const { return &Domain_; }
	bool operator==(const TransactionArc& other) const;
	bool operator<(const TransactionArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL Transaction : public Domain {
private:
	void pushColumns();
public:
	struct TransactionDomain {
		Column id;
		Column personid;
		Column agentid;
		Column currency;
		Column eid;
		Column amount;
		Column status;
		Column comment;
		Column in_sync;
		Column valid_until;
		Column meta;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		TransactionDomain( const SimpleTable& table )
		: id(table, "id")
		, personid(table, "personid")
		, agentid(table, "agentid")
		, currency(table, "currency")
		, eid(table, "eid")
		, amount(table, "amount")
		, status(table, "status")
		, comment(table, "comment")
		, in_sync(table, "in_sync")
		, valid_until(table, "valid_until")
		, meta(table, "meta")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Int id;
	Int personid;
	Int agentid;
	Str currency;
	Str eid;
	Decimal amount;
	Str status;
	Str comment;
	Str in_sync;
	Time valid_until;
	Str meta;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Transaction();
	explicit Transaction(Int __id);
	Transaction(const Transaction& copy);
	Transaction& operator=(const Transaction& copy);
	virtual ~Transaction() {}
	TransactionDomain* operator->() { return &Domain_; }
	const TransactionDomain* operator->() const { return &Domain_; }
	bool operator==(const Transaction& other) const;
	bool operator<(const Transaction& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL InfoPlacementArc : public Domain {
private:
	void pushColumns();
public:
	struct InfoPlacementArcDomain {
		Column id;
		Column widget;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		InfoPlacementArcDomain( const SimpleTable& table )
		: id(table, "id")
		, widget(table, "widget")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Int id;
	Str widget;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	InfoPlacementArc();
	InfoPlacementArc(const InfoPlacementArc& copy);
	InfoPlacementArc& operator=(const InfoPlacementArc& copy);
	virtual ~InfoPlacementArc() {}
	InfoPlacementArcDomain* operator->() { return &Domain_; }
	const InfoPlacementArcDomain* operator->() const { return &Domain_; }
	bool operator==(const InfoPlacementArc& other) const;
	bool operator<(const InfoPlacementArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL InfoPlacement : public Domain {
private:
	void pushColumns();
public:
	struct InfoPlacementDomain {
		Column id;
		Column widget;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		InfoPlacementDomain( const SimpleTable& table )
		: id(table, "id")
		, widget(table, "widget")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Int id;
	Str widget;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	InfoPlacement();
	explicit InfoPlacement(Int __id);
	InfoPlacement(const InfoPlacement& copy);
	InfoPlacement& operator=(const InfoPlacement& copy);
	virtual ~InfoPlacement() {}
	InfoPlacementDomain* operator->() { return &Domain_; }
	const InfoPlacementDomain* operator->() const { return &Domain_; }
	bool operator==(const InfoPlacement& other) const;
	bool operator<(const InfoPlacement& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL HolidayArc : public Domain {
private:
	void pushColumns();
public:
	struct HolidayArcDomain {
		Column adate;
		Column out_of_service;
		Column comment;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		HolidayArcDomain( const SimpleTable& table )
		: adate(table, "adate")
		, out_of_service(table, "out_of_service")
		, comment(table, "comment")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	ADate adate;
	Str out_of_service;
	Str comment;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	HolidayArc();
	HolidayArc(const HolidayArc& copy);
	HolidayArc& operator=(const HolidayArc& copy);
	virtual ~HolidayArc() {}
	HolidayArcDomain* operator->() { return &Domain_; }
	const HolidayArcDomain* operator->() const { return &Domain_; }
	bool operator==(const HolidayArc& other) const;
	bool operator<(const HolidayArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL Holiday : public Domain {
private:
	void pushColumns();
public:
	struct HolidayDomain {
		Column adate;
		Column out_of_service;
		Column comment;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		HolidayDomain( const SimpleTable& table )
		: adate(table, "adate")
		, out_of_service(table, "out_of_service")
		, comment(table, "comment")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	ADate adate;
	Str out_of_service;
	Str comment;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Holiday();
	explicit Holiday(ADate __adate);
	Holiday(const Holiday& copy);
	Holiday& operator=(const Holiday& copy);
	virtual ~Holiday() {}
	HolidayDomain* operator->() { return &Domain_; }
	const HolidayDomain* operator->() const { return &Domain_; }
	bool operator==(const Holiday& other) const;
	bool operator<(const Holiday& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL WeekTariffGroupArc : public Domain {
private:
	void pushColumns();
public:
	struct WeekTariffGroupArcDomain {
		Column id;
		Column name;
		Column description;
		Column is_default;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		WeekTariffGroupArcDomain( const SimpleTable& table )
		: id(table, "id")
		, name(table, "name")
		, description(table, "description")
		, is_default(table, "is_default")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Int id;
	Str name;
	Str description;
	Str is_default;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	WeekTariffGroupArc();
	WeekTariffGroupArc(const WeekTariffGroupArc& copy);
	WeekTariffGroupArc& operator=(const WeekTariffGroupArc& copy);
	virtual ~WeekTariffGroupArc() {}
	WeekTariffGroupArcDomain* operator->() { return &Domain_; }
	const WeekTariffGroupArcDomain* operator->() const { return &Domain_; }
	bool operator==(const WeekTariffGroupArc& other) const;
	bool operator<(const WeekTariffGroupArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL WeekTariffGroup : public Domain {
private:
	void pushColumns();
public:
	struct WeekTariffGroupDomain {
		Column id;
		Column name;
		Column description;
		Column is_default;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		WeekTariffGroupDomain( const SimpleTable& table )
		: id(table, "id")
		, name(table, "name")
		, description(table, "description")
		, is_default(table, "is_default")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Int id;
	Str name;
	Str description;
	Str is_default;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	WeekTariffGroup();
	explicit WeekTariffGroup(Int __id);
	WeekTariffGroup(const WeekTariffGroup& copy);
	WeekTariffGroup& operator=(const WeekTariffGroup& copy);
	virtual ~WeekTariffGroup() {}
	WeekTariffGroupDomain* operator->() { return &Domain_; }
	const WeekTariffGroupDomain* operator->() const { return &Domain_; }
	bool operator==(const WeekTariffGroup& other) const;
	bool operator<(const WeekTariffGroup& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL WeekTariffArc : public Domain {
private:
	void pushColumns();
public:
	struct WeekTariffArcDomain {
		Column id;
		Column agroupid;
		Column week_day;
		Column time_from;
		Column time_upto;
		Column price;
		Column pricetype;
		Column free;
		Column total;
		Column counttype;
		Column external_name;
		Column identifier_rulename;
		Column service_rulename;
		Column identifier_categoryname;
		Column ratename;
		Column enabled;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		WeekTariffArcDomain( const SimpleTable& table )
		: id(table, "id")
		, agroupid(table, "agroupid")
		, week_day(table, "week_day")
		, time_from(table, "time_from")
		, time_upto(table, "time_upto")
		, price(table, "price")
		, pricetype(table, "pricetype")
		, free(table, "free")
		, total(table, "total")
		, counttype(table, "counttype")
		, external_name(table, "external_name")
		, identifier_rulename(table, "identifier_rulename")
		, service_rulename(table, "service_rulename")
		, identifier_categoryname(table, "identifier_categoryname")
		, ratename(table, "ratename")
		, enabled(table, "enabled")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Int id;
	Int agroupid;
	Str week_day;
	ATime time_from;
	ATime time_upto;
	Decimal price;
	Str pricetype;
	Int free;
	Int total;
	Str counttype;
	Str external_name;
	Str identifier_rulename;
	Str service_rulename;
	Str identifier_categoryname;
	Str ratename;
	Str enabled;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	WeekTariffArc();
	WeekTariffArc(const WeekTariffArc& copy);
	WeekTariffArc& operator=(const WeekTariffArc& copy);
	virtual ~WeekTariffArc() {}
	WeekTariffArcDomain* operator->() { return &Domain_; }
	const WeekTariffArcDomain* operator->() const { return &Domain_; }
	bool operator==(const WeekTariffArc& other) const;
	bool operator<(const WeekTariffArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL WeekTariff : public Domain {
private:
	void pushColumns();
public:
	struct WeekTariffDomain {
		Column id;
		Column agroupid;
		Column week_day;
		Column time_from;
		Column time_upto;
		Column price;
		Column pricetype;
		Column free;
		Column total;
		Column counttype;
		Column external_name;
		Column identifier_rulename;
		Column service_rulename;
		Column identifier_categoryname;
		Column ratename;
		Column enabled;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		WeekTariffDomain( const SimpleTable& table )
		: id(table, "id")
		, agroupid(table, "agroupid")
		, week_day(table, "week_day")
		, time_from(table, "time_from")
		, time_upto(table, "time_upto")
		, price(table, "price")
		, pricetype(table, "pricetype")
		, free(table, "free")
		, total(table, "total")
		, counttype(table, "counttype")
		, external_name(table, "external_name")
		, identifier_rulename(table, "identifier_rulename")
		, service_rulename(table, "service_rulename")
		, identifier_categoryname(table, "identifier_categoryname")
		, ratename(table, "ratename")
		, enabled(table, "enabled")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Int id;
	Int agroupid;
	Str week_day;
	ATime time_from;
	ATime time_upto;
	Decimal price;
	Str pricetype;
	Int free;
	Int total;
	Str counttype;
	Str external_name;
	Str identifier_rulename;
	Str service_rulename;
	Str identifier_categoryname;
	Str ratename;
	Str enabled;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	WeekTariff();
	explicit WeekTariff(Int __id);
	WeekTariff(const WeekTariff& copy);
	WeekTariff& operator=(const WeekTariff& copy);
	virtual ~WeekTariff() {}
	WeekTariffDomain* operator->() { return &Domain_; }
	const WeekTariffDomain* operator->() const { return &Domain_; }
	bool operator==(const WeekTariff& other) const;
	bool operator<(const WeekTariff& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL SkipassConfigurationArc : public Domain {
private:
	void pushColumns();
public:
	struct SkipassConfigurationArcDomain {
		Column id;
		Column groupname;
		Column name;
		Column identifier_rulename;
		Column service_rulename;
		Column identifier_categoryname;
		Column ratename;
		Column valid_from;
		Column valid_to;
		Column price;
		Column make_transaction;
		Column enabled;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		SkipassConfigurationArcDomain( const SimpleTable& table )
		: id(table, "id")
		, groupname(table, "groupname")
		, name(table, "name")
		, identifier_rulename(table, "identifier_rulename")
		, service_rulename(table, "service_rulename")
		, identifier_categoryname(table, "identifier_categoryname")
		, ratename(table, "ratename")
		, valid_from(table, "valid_from")
		, valid_to(table, "valid_to")
		, price(table, "price")
		, make_transaction(table, "make_transaction")
		, enabled(table, "enabled")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Int id;
	Str groupname;
	Str name;
	Str identifier_rulename;
	Str service_rulename;
	Str identifier_categoryname;
	Str ratename;
	Time valid_from;
	Time valid_to;
	Decimal price;
	Str make_transaction;
	Str enabled;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	SkipassConfigurationArc();
	SkipassConfigurationArc(const SkipassConfigurationArc& copy);
	SkipassConfigurationArc& operator=(const SkipassConfigurationArc& copy);
	virtual ~SkipassConfigurationArc() {}
	SkipassConfigurationArcDomain* operator->() { return &Domain_; }
	const SkipassConfigurationArcDomain* operator->() const { return &Domain_; }
	bool operator==(const SkipassConfigurationArc& other) const;
	bool operator<(const SkipassConfigurationArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL SkipassConfiguration : public Domain {
private:
	void pushColumns();
public:
	struct SkipassConfigurationDomain {
		Column id;
		Column groupname;
		Column name;
		Column identifier_rulename;
		Column service_rulename;
		Column identifier_categoryname;
		Column ratename;
		Column valid_from;
		Column valid_to;
		Column price;
		Column make_transaction;
		Column enabled;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		SkipassConfigurationDomain( const SimpleTable& table )
		: id(table, "id")
		, groupname(table, "groupname")
		, name(table, "name")
		, identifier_rulename(table, "identifier_rulename")
		, service_rulename(table, "service_rulename")
		, identifier_categoryname(table, "identifier_categoryname")
		, ratename(table, "ratename")
		, valid_from(table, "valid_from")
		, valid_to(table, "valid_to")
		, price(table, "price")
		, make_transaction(table, "make_transaction")
		, enabled(table, "enabled")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Int id;
	Str groupname;
	Str name;
	Str identifier_rulename;
	Str service_rulename;
	Str identifier_categoryname;
	Str ratename;
	Time valid_from;
	Time valid_to;
	Decimal price;
	Str make_transaction;
	Str enabled;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	SkipassConfiguration();
	explicit SkipassConfiguration(Int __id);
	SkipassConfiguration(const SkipassConfiguration& copy);
	SkipassConfiguration& operator=(const SkipassConfiguration& copy);
	virtual ~SkipassConfiguration() {}
	SkipassConfigurationDomain* operator->() { return &Domain_; }
	const SkipassConfigurationDomain* operator->() const { return &Domain_; }
	bool operator==(const SkipassConfiguration& other) const;
	bool operator<(const SkipassConfiguration& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL SpdGateArc : public Domain {
private:
	void pushColumns();
public:
	struct SpdGateArcDomain {
		Column id;
		Column name;
		Column host;
		Column port;
		Column enabled;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		SpdGateArcDomain( const SimpleTable& table )
		: id(table, "id")
		, name(table, "name")
		, host(table, "host")
		, port(table, "port")
		, enabled(table, "enabled")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Int id;
	Str name;
	Str host;
	Str port;
	Str enabled;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	SpdGateArc();
	SpdGateArc(const SpdGateArc& copy);
	SpdGateArc& operator=(const SpdGateArc& copy);
	virtual ~SpdGateArc() {}
	SpdGateArcDomain* operator->() { return &Domain_; }
	const SpdGateArcDomain* operator->() const { return &Domain_; }
	bool operator==(const SpdGateArc& other) const;
	bool operator<(const SpdGateArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL SpdGate : public Domain {
private:
	void pushColumns();
public:
	struct SpdGateDomain {
		Column id;
		Column name;
		Column host;
		Column port;
		Column enabled;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		SpdGateDomain( const SimpleTable& table )
		: id(table, "id")
		, name(table, "name")
		, host(table, "host")
		, port(table, "port")
		, enabled(table, "enabled")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Int id;
	Str name;
	Str host;
	Str port;
	Str enabled;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	SpdGate();
	explicit SpdGate(Int __id);
	SpdGate(const SpdGate& copy);
	SpdGate& operator=(const SpdGate& copy);
	virtual ~SpdGate() {}
	SpdGateDomain* operator->() { return &Domain_; }
	const SpdGateDomain* operator->() const { return &Domain_; }
	bool operator==(const SpdGate& other) const;
	bool operator<(const SpdGate& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL PaidServiceCategoryArc : public Domain {
private:
	void pushColumns();
public:
	struct PaidServiceCategoryArcDomain {
		Column id;
		Column name;
		Column status;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		PaidServiceCategoryArcDomain( const SimpleTable& table )
		: id(table, "id")
		, name(table, "name")
		, status(table, "status")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Int id;
	Str name;
	Str status;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	PaidServiceCategoryArc();
	PaidServiceCategoryArc(const PaidServiceCategoryArc& copy);
	PaidServiceCategoryArc& operator=(const PaidServiceCategoryArc& copy);
	virtual ~PaidServiceCategoryArc() {}
	PaidServiceCategoryArcDomain* operator->() { return &Domain_; }
	const PaidServiceCategoryArcDomain* operator->() const { return &Domain_; }
	bool operator==(const PaidServiceCategoryArc& other) const;
	bool operator<(const PaidServiceCategoryArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL PaidServiceCategory : public Domain {
private:
	void pushColumns();
public:
	struct PaidServiceCategoryDomain {
		Column id;
		Column name;
		Column status;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		PaidServiceCategoryDomain( const SimpleTable& table )
		: id(table, "id")
		, name(table, "name")
		, status(table, "status")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Int id;
	Str name;
	Str status;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	PaidServiceCategory();
	explicit PaidServiceCategory(Int __id);
	PaidServiceCategory(const PaidServiceCategory& copy);
	PaidServiceCategory& operator=(const PaidServiceCategory& copy);
	virtual ~PaidServiceCategory() {}
	PaidServiceCategoryDomain* operator->() { return &Domain_; }
	const PaidServiceCategoryDomain* operator->() const { return &Domain_; }
	bool operator==(const PaidServiceCategory& other) const;
	bool operator<(const PaidServiceCategory& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL PaidServiceTariffArc : public Domain {
private:
	void pushColumns();
public:
	struct PaidServiceTariffArcDomain {
		Column id;
		Column name;
		Column status;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		PaidServiceTariffArcDomain( const SimpleTable& table )
		: id(table, "id")
		, name(table, "name")
		, status(table, "status")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Int id;
	Str name;
	Str status;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	PaidServiceTariffArc();
	PaidServiceTariffArc(const PaidServiceTariffArc& copy);
	PaidServiceTariffArc& operator=(const PaidServiceTariffArc& copy);
	virtual ~PaidServiceTariffArc() {}
	PaidServiceTariffArcDomain* operator->() { return &Domain_; }
	const PaidServiceTariffArcDomain* operator->() const { return &Domain_; }
	bool operator==(const PaidServiceTariffArc& other) const;
	bool operator<(const PaidServiceTariffArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL PaidServiceTariff : public Domain {
private:
	void pushColumns();
public:
	struct PaidServiceTariffDomain {
		Column id;
		Column name;
		Column status;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		PaidServiceTariffDomain( const SimpleTable& table )
		: id(table, "id")
		, name(table, "name")
		, status(table, "status")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Int id;
	Str name;
	Str status;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	PaidServiceTariff();
	explicit PaidServiceTariff(Int __id);
	PaidServiceTariff(const PaidServiceTariff& copy);
	PaidServiceTariff& operator=(const PaidServiceTariff& copy);
	virtual ~PaidServiceTariff() {}
	PaidServiceTariffDomain* operator->() { return &Domain_; }
	const PaidServiceTariffDomain* operator->() const { return &Domain_; }
	bool operator==(const PaidServiceTariff& other) const;
	bool operator<(const PaidServiceTariff& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL PaidServiceHourArc : public Domain {
private:
	void pushColumns();
public:
	struct PaidServiceHourArcDomain {
		Column id;
		Column name;
		Column status;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		PaidServiceHourArcDomain( const SimpleTable& table )
		: id(table, "id")
		, name(table, "name")
		, status(table, "status")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Int id;
	Str name;
	Str status;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	PaidServiceHourArc();
	PaidServiceHourArc(const PaidServiceHourArc& copy);
	PaidServiceHourArc& operator=(const PaidServiceHourArc& copy);
	virtual ~PaidServiceHourArc() {}
	PaidServiceHourArcDomain* operator->() { return &Domain_; }
	const PaidServiceHourArcDomain* operator->() const { return &Domain_; }
	bool operator==(const PaidServiceHourArc& other) const;
	bool operator<(const PaidServiceHourArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL PaidServiceHour : public Domain {
private:
	void pushColumns();
public:
	struct PaidServiceHourDomain {
		Column id;
		Column name;
		Column status;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		PaidServiceHourDomain( const SimpleTable& table )
		: id(table, "id")
		, name(table, "name")
		, status(table, "status")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Int id;
	Str name;
	Str status;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	PaidServiceHour();
	explicit PaidServiceHour(Int __id);
	PaidServiceHour(const PaidServiceHour& copy);
	PaidServiceHour& operator=(const PaidServiceHour& copy);
	virtual ~PaidServiceHour() {}
	PaidServiceHourDomain* operator->() { return &Domain_; }
	const PaidServiceHourDomain* operator->() const { return &Domain_; }
	bool operator==(const PaidServiceHour& other) const;
	bool operator<(const PaidServiceHour& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL PaidServiceArc : public Domain {
private:
	void pushColumns();
public:
	struct PaidServiceArcDomain {
		Column id;
		Column categoryid;
		Column name;
		Column tariffid;
		Column hourid;
		Column description;
		Column price;
		Column status;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		PaidServiceArcDomain( const SimpleTable& table )
		: id(table, "id")
		, categoryid(table, "categoryid")
		, name(table, "name")
		, tariffid(table, "tariffid")
		, hourid(table, "hourid")
		, description(table, "description")
		, price(table, "price")
		, status(table, "status")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Int id;
	Int categoryid;
	Str name;
	Int tariffid;
	Int hourid;
	Str description;
	Decimal price;
	Str status;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	PaidServiceArc();
	PaidServiceArc(const PaidServiceArc& copy);
	PaidServiceArc& operator=(const PaidServiceArc& copy);
	virtual ~PaidServiceArc() {}
	PaidServiceArcDomain* operator->() { return &Domain_; }
	const PaidServiceArcDomain* operator->() const { return &Domain_; }
	bool operator==(const PaidServiceArc& other) const;
	bool operator<(const PaidServiceArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL PaidService : public Domain {
private:
	void pushColumns();
public:
	struct PaidServiceDomain {
		Column id;
		Column categoryid;
		Column name;
		Column tariffid;
		Column hourid;
		Column description;
		Column price;
		Column status;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		PaidServiceDomain( const SimpleTable& table )
		: id(table, "id")
		, categoryid(table, "categoryid")
		, name(table, "name")
		, tariffid(table, "tariffid")
		, hourid(table, "hourid")
		, description(table, "description")
		, price(table, "price")
		, status(table, "status")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Int id;
	Int categoryid;
	Str name;
	Int tariffid;
	Int hourid;
	Str description;
	Decimal price;
	Str status;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	PaidService();
	explicit PaidService(Int __id);
	PaidService(const PaidService& copy);
	PaidService& operator=(const PaidService& copy);
	virtual ~PaidService() {}
	PaidServiceDomain* operator->() { return &Domain_; }
	const PaidServiceDomain* operator->() const { return &Domain_; }
	bool operator==(const PaidService& other) const;
	bool operator<(const PaidService& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL PartnerArc : public Domain {
private:
	void pushColumns();
public:
	struct PartnerArcDomain {
		Column id;
		Column name;
		Column status;
		Column legal_address;
		Column post_address;
		Column inn;
		Column kpp;
		Column payment_account;
		Column correspondent_account;
		Column bank;
		Column bik;
		Column okpo;
		Column okato;
		Column okved;
		Column ogrn;
		Column principal;
		Column phone;
		Column fax;
		Column email;
		Column token;
		Column notification_email;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		PartnerArcDomain( const SimpleTable& table )
		: id(table, "id")
		, name(table, "name")
		, status(table, "status")
		, legal_address(table, "legal_address")
		, post_address(table, "post_address")
		, inn(table, "inn")
		, kpp(table, "kpp")
		, payment_account(table, "payment_account")
		, correspondent_account(table, "correspondent_account")
		, bank(table, "bank")
		, bik(table, "bik")
		, okpo(table, "okpo")
		, okato(table, "okato")
		, okved(table, "okved")
		, ogrn(table, "ogrn")
		, principal(table, "principal")
		, phone(table, "phone")
		, fax(table, "fax")
		, email(table, "email")
		, token(table, "token")
		, notification_email(table, "notification_email")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Int id;
	Str name;
	Str status;
	Str legal_address;
	Str post_address;
	Str inn;
	Str kpp;
	Str payment_account;
	Str correspondent_account;
	Str bank;
	Str bik;
	Str okpo;
	Str okato;
	Str okved;
	Str ogrn;
	Str principal;
	Str phone;
	Str fax;
	Str email;
	Str token;
	Str notification_email;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	PartnerArc();
	PartnerArc(const PartnerArc& copy);
	PartnerArc& operator=(const PartnerArc& copy);
	virtual ~PartnerArc() {}
	PartnerArcDomain* operator->() { return &Domain_; }
	const PartnerArcDomain* operator->() const { return &Domain_; }
	bool operator==(const PartnerArc& other) const;
	bool operator<(const PartnerArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL Partner : public Domain {
private:
	void pushColumns();
public:
	struct PartnerDomain {
		Column id;
		Column name;
		Column status;
		Column legal_address;
		Column post_address;
		Column inn;
		Column kpp;
		Column payment_account;
		Column correspondent_account;
		Column bank;
		Column bik;
		Column okpo;
		Column okato;
		Column okved;
		Column ogrn;
		Column principal;
		Column phone;
		Column fax;
		Column email;
		Column token;
		Column notification_email;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		PartnerDomain( const SimpleTable& table )
		: id(table, "id")
		, name(table, "name")
		, status(table, "status")
		, legal_address(table, "legal_address")
		, post_address(table, "post_address")
		, inn(table, "inn")
		, kpp(table, "kpp")
		, payment_account(table, "payment_account")
		, correspondent_account(table, "correspondent_account")
		, bank(table, "bank")
		, bik(table, "bik")
		, okpo(table, "okpo")
		, okato(table, "okato")
		, okved(table, "okved")
		, ogrn(table, "ogrn")
		, principal(table, "principal")
		, phone(table, "phone")
		, fax(table, "fax")
		, email(table, "email")
		, token(table, "token")
		, notification_email(table, "notification_email")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Int id;
	Str name;
	Str status;
	Str legal_address;
	Str post_address;
	Str inn;
	Str kpp;
	Str payment_account;
	Str correspondent_account;
	Str bank;
	Str bik;
	Str okpo;
	Str okato;
	Str okved;
	Str ogrn;
	Str principal;
	Str phone;
	Str fax;
	Str email;
	Str token;
	Str notification_email;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Partner();
	explicit Partner(Int __id);
	Partner(const Partner& copy);
	Partner& operator=(const Partner& copy);
	virtual ~Partner() {}
	PartnerDomain* operator->() { return &Domain_; }
	const PartnerDomain* operator->() const { return &Domain_; }
	bool operator==(const Partner& other) const;
	bool operator<(const Partner& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL PartnerAdministratorArc : public Domain {
private:
	void pushColumns();
public:
	struct PartnerAdministratorArcDomain {
		Column auserTokenID;
		Column fullname;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		PartnerAdministratorArcDomain( const SimpleTable& table )
		: auserTokenID(table, "auserTokenID")
		, fullname(table, "fullname")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Int64 auserTokenID;
	Str fullname;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	PartnerAdministratorArc();
	PartnerAdministratorArc(const PartnerAdministratorArc& copy);
	PartnerAdministratorArc& operator=(const PartnerAdministratorArc& copy);
	virtual ~PartnerAdministratorArc() {}
	PartnerAdministratorArcDomain* operator->() { return &Domain_; }
	const PartnerAdministratorArcDomain* operator->() const { return &Domain_; }
	bool operator==(const PartnerAdministratorArc& other) const;
	bool operator<(const PartnerAdministratorArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL PartnerAdministrator : public Domain {
private:
	void pushColumns();
public:
	struct PartnerAdministratorDomain {
		Column auserTokenID;
		Column fullname;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		PartnerAdministratorDomain( const SimpleTable& table )
		: auserTokenID(table, "auserTokenID")
		, fullname(table, "fullname")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Int64 auserTokenID;
	Str fullname;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	PartnerAdministrator();
	explicit PartnerAdministrator(Int64 __auserTokenID);
	PartnerAdministrator(const PartnerAdministrator& copy);
	PartnerAdministrator& operator=(const PartnerAdministrator& copy);
	virtual ~PartnerAdministrator() {}
	PartnerAdministratorDomain* operator->() { return &Domain_; }
	const PartnerAdministratorDomain* operator->() const { return &Domain_; }
	bool operator==(const PartnerAdministrator& other) const;
	bool operator<(const PartnerAdministrator& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL PartnerManagerArc : public Domain {
private:
	void pushColumns();
public:
	struct PartnerManagerArcDomain {
		Column auserTokenID;
		Column partnerid;
		Column fullname;
		Column passport_number;
		Column issue_date;
		Column issued_by;
		Column department_code;
		Column phone;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		PartnerManagerArcDomain( const SimpleTable& table )
		: auserTokenID(table, "auserTokenID")
		, partnerid(table, "partnerid")
		, fullname(table, "fullname")
		, passport_number(table, "passport_number")
		, issue_date(table, "issue_date")
		, issued_by(table, "issued_by")
		, department_code(table, "department_code")
		, phone(table, "phone")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Int64 auserTokenID;
	Int partnerid;
	Str fullname;
	Str passport_number;
	ADate issue_date;
	Str issued_by;
	Str department_code;
	Str phone;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	PartnerManagerArc();
	PartnerManagerArc(const PartnerManagerArc& copy);
	PartnerManagerArc& operator=(const PartnerManagerArc& copy);
	virtual ~PartnerManagerArc() {}
	PartnerManagerArcDomain* operator->() { return &Domain_; }
	const PartnerManagerArcDomain* operator->() const { return &Domain_; }
	bool operator==(const PartnerManagerArc& other) const;
	bool operator<(const PartnerManagerArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL PartnerManager : public Domain {
private:
	void pushColumns();
public:
	struct PartnerManagerDomain {
		Column auserTokenID;
		Column partnerid;
		Column fullname;
		Column passport_number;
		Column issue_date;
		Column issued_by;
		Column department_code;
		Column phone;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		PartnerManagerDomain( const SimpleTable& table )
		: auserTokenID(table, "auserTokenID")
		, partnerid(table, "partnerid")
		, fullname(table, "fullname")
		, passport_number(table, "passport_number")
		, issue_date(table, "issue_date")
		, issued_by(table, "issued_by")
		, department_code(table, "department_code")
		, phone(table, "phone")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Int64 auserTokenID;
	Int partnerid;
	Str fullname;
	Str passport_number;
	ADate issue_date;
	Str issued_by;
	Str department_code;
	Str phone;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	PartnerManager();
	explicit PartnerManager(Int64 __auserTokenID);
	PartnerManager(const PartnerManager& copy);
	PartnerManager& operator=(const PartnerManager& copy);
	virtual ~PartnerManager() {}
	PartnerManagerDomain* operator->() { return &Domain_; }
	const PartnerManagerDomain* operator->() const { return &Domain_; }
	bool operator==(const PartnerManager& other) const;
	bool operator<(const PartnerManager& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL PartnerContractArc : public Domain {
private:
	void pushColumns();
public:
	struct PartnerContractArcDomain {
		Column id;
		Column partnerid;
		Column barcode;
		Column start_date;
		Column end_date;
		Column benefit_type;
		Column benefit_amount;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		PartnerContractArcDomain( const SimpleTable& table )
		: id(table, "id")
		, partnerid(table, "partnerid")
		, barcode(table, "barcode")
		, start_date(table, "start_date")
		, end_date(table, "end_date")
		, benefit_type(table, "benefit_type")
		, benefit_amount(table, "benefit_amount")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Int id;
	Int partnerid;
	Str barcode;
	ADate start_date;
	ADate end_date;
	Str benefit_type;
	Decimal benefit_amount;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	PartnerContractArc();
	PartnerContractArc(const PartnerContractArc& copy);
	PartnerContractArc& operator=(const PartnerContractArc& copy);
	virtual ~PartnerContractArc() {}
	PartnerContractArcDomain* operator->() { return &Domain_; }
	const PartnerContractArcDomain* operator->() const { return &Domain_; }
	bool operator==(const PartnerContractArc& other) const;
	bool operator<(const PartnerContractArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL PartnerContract : public Domain {
private:
	void pushColumns();
public:
	struct PartnerContractDomain {
		Column id;
		Column partnerid;
		Column barcode;
		Column start_date;
		Column end_date;
		Column benefit_type;
		Column benefit_amount;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		PartnerContractDomain( const SimpleTable& table )
		: id(table, "id")
		, partnerid(table, "partnerid")
		, barcode(table, "barcode")
		, start_date(table, "start_date")
		, end_date(table, "end_date")
		, benefit_type(table, "benefit_type")
		, benefit_amount(table, "benefit_amount")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Int id;
	Int partnerid;
	Str barcode;
	ADate start_date;
	ADate end_date;
	Str benefit_type;
	Decimal benefit_amount;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	PartnerContract();
	explicit PartnerContract(Int __id);
	PartnerContract(const PartnerContract& copy);
	PartnerContract& operator=(const PartnerContract& copy);
	virtual ~PartnerContract() {}
	PartnerContractDomain* operator->() { return &Domain_; }
	const PartnerContractDomain* operator->() const { return &Domain_; }
	bool operator==(const PartnerContract& other) const;
	bool operator<(const PartnerContract& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL PartnerAccountArc : public Domain {
private:
	void pushColumns();
public:
	struct PartnerAccountArcDomain {
		Column id;
		Column partnerid;
		Column name;
		Column atype;
		Column amount;
		Column status;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		PartnerAccountArcDomain( const SimpleTable& table )
		: id(table, "id")
		, partnerid(table, "partnerid")
		, name(table, "name")
		, atype(table, "atype")
		, amount(table, "amount")
		, status(table, "status")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Int id;
	Int partnerid;
	Str name;
	Str atype;
	Decimal amount;
	Str status;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	PartnerAccountArc();
	PartnerAccountArc(const PartnerAccountArc& copy);
	PartnerAccountArc& operator=(const PartnerAccountArc& copy);
	virtual ~PartnerAccountArc() {}
	PartnerAccountArcDomain* operator->() { return &Domain_; }
	const PartnerAccountArcDomain* operator->() const { return &Domain_; }
	bool operator==(const PartnerAccountArc& other) const;
	bool operator<(const PartnerAccountArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL PartnerAccount : public Domain {
private:
	void pushColumns();
public:
	struct PartnerAccountDomain {
		Column id;
		Column partnerid;
		Column name;
		Column atype;
		Column amount;
		Column status;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		PartnerAccountDomain( const SimpleTable& table )
		: id(table, "id")
		, partnerid(table, "partnerid")
		, name(table, "name")
		, atype(table, "atype")
		, amount(table, "amount")
		, status(table, "status")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Int id;
	Int partnerid;
	Str name;
	Str atype;
	Decimal amount;
	Str status;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	PartnerAccount();
	explicit PartnerAccount(Int __id);
	PartnerAccount(const PartnerAccount& copy);
	PartnerAccount& operator=(const PartnerAccount& copy);
	virtual ~PartnerAccount() {}
	PartnerAccountDomain* operator->() { return &Domain_; }
	const PartnerAccountDomain* operator->() const { return &Domain_; }
	bool operator==(const PartnerAccount& other) const;
	bool operator<(const PartnerAccount& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL AccountTransactionArc : public Domain {
private:
	void pushColumns();
public:
	struct AccountTransactionArcDomain {
		Column id;
		Column accountid;
		Column amount;
		Column cache_flow;
		Column reason;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		AccountTransactionArcDomain( const SimpleTable& table )
		: id(table, "id")
		, accountid(table, "accountid")
		, amount(table, "amount")
		, cache_flow(table, "cache_flow")
		, reason(table, "reason")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Int id;
	Int accountid;
	Decimal amount;
	Str cache_flow;
	Str reason;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	AccountTransactionArc();
	AccountTransactionArc(const AccountTransactionArc& copy);
	AccountTransactionArc& operator=(const AccountTransactionArc& copy);
	virtual ~AccountTransactionArc() {}
	AccountTransactionArcDomain* operator->() { return &Domain_; }
	const AccountTransactionArcDomain* operator->() const { return &Domain_; }
	bool operator==(const AccountTransactionArc& other) const;
	bool operator<(const AccountTransactionArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL AccountTransaction : public Domain {
private:
	void pushColumns();
public:
	struct AccountTransactionDomain {
		Column id;
		Column accountid;
		Column amount;
		Column cache_flow;
		Column reason;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		AccountTransactionDomain( const SimpleTable& table )
		: id(table, "id")
		, accountid(table, "accountid")
		, amount(table, "amount")
		, cache_flow(table, "cache_flow")
		, reason(table, "reason")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Int id;
	Int accountid;
	Decimal amount;
	Str cache_flow;
	Str reason;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	AccountTransaction();
	explicit AccountTransaction(Int __id);
	AccountTransaction(const AccountTransaction& copy);
	AccountTransaction& operator=(const AccountTransaction& copy);
	virtual ~AccountTransaction() {}
	AccountTransactionDomain* operator->() { return &Domain_; }
	const AccountTransactionDomain* operator->() const { return &Domain_; }
	bool operator==(const AccountTransaction& other) const;
	bool operator<(const AccountTransaction& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL PartnerWeekTariffGroupArc : public Domain {
private:
	void pushColumns();
public:
	struct PartnerWeekTariffGroupArcDomain {
		Column partnerid;
		Column tariffgroupid;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		PartnerWeekTariffGroupArcDomain( const SimpleTable& table )
		: partnerid(table, "partnerid")
		, tariffgroupid(table, "tariffgroupid")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Int partnerid;
	Int tariffgroupid;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	PartnerWeekTariffGroupArc();
	PartnerWeekTariffGroupArc(const PartnerWeekTariffGroupArc& copy);
	PartnerWeekTariffGroupArc& operator=(const PartnerWeekTariffGroupArc& copy);
	virtual ~PartnerWeekTariffGroupArc() {}
	PartnerWeekTariffGroupArcDomain* operator->() { return &Domain_; }
	const PartnerWeekTariffGroupArcDomain* operator->() const { return &Domain_; }
	bool operator==(const PartnerWeekTariffGroupArc& other) const;
	bool operator<(const PartnerWeekTariffGroupArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL PartnerWeekTariffGroup : public Domain {
private:
	void pushColumns();
public:
	struct PartnerWeekTariffGroupDomain {
		Column partnerid;
		Column tariffgroupid;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		PartnerWeekTariffGroupDomain( const SimpleTable& table )
		: partnerid(table, "partnerid")
		, tariffgroupid(table, "tariffgroupid")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Int partnerid;
	Int tariffgroupid;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	PartnerWeekTariffGroup();
	explicit PartnerWeekTariffGroup(Int __partnerid, Int __tariffgroupid);
	PartnerWeekTariffGroup(const PartnerWeekTariffGroup& copy);
	PartnerWeekTariffGroup& operator=(const PartnerWeekTariffGroup& copy);
	virtual ~PartnerWeekTariffGroup() {}
	PartnerWeekTariffGroupDomain* operator->() { return &Domain_; }
	const PartnerWeekTariffGroupDomain* operator->() const { return &Domain_; }
	bool operator==(const PartnerWeekTariffGroup& other) const;
	bool operator<(const PartnerWeekTariffGroup& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL PartnerOrderArc : public Domain {
private:
	void pushColumns();
public:
	struct PartnerOrderArcDomain {
		Column id;
		Column partnerid;
		Column initial_cost;
		Column discount;
		Column total_cost;
		Column week_tariffid;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		PartnerOrderArcDomain( const SimpleTable& table )
		: id(table, "id")
		, partnerid(table, "partnerid")
		, initial_cost(table, "initial_cost")
		, discount(table, "discount")
		, total_cost(table, "total_cost")
		, week_tariffid(table, "week_tariffid")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Int id;
	Int partnerid;
	Decimal initial_cost;
	Decimal discount;
	Decimal total_cost;
	Int week_tariffid;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	PartnerOrderArc();
	PartnerOrderArc(const PartnerOrderArc& copy);
	PartnerOrderArc& operator=(const PartnerOrderArc& copy);
	virtual ~PartnerOrderArc() {}
	PartnerOrderArcDomain* operator->() { return &Domain_; }
	const PartnerOrderArcDomain* operator->() const { return &Domain_; }
	bool operator==(const PartnerOrderArc& other) const;
	bool operator<(const PartnerOrderArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL PartnerOrder : public Domain {
private:
	void pushColumns();
public:
	struct PartnerOrderDomain {
		Column id;
		Column partnerid;
		Column initial_cost;
		Column discount;
		Column total_cost;
		Column week_tariffid;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		PartnerOrderDomain( const SimpleTable& table )
		: id(table, "id")
		, partnerid(table, "partnerid")
		, initial_cost(table, "initial_cost")
		, discount(table, "discount")
		, total_cost(table, "total_cost")
		, week_tariffid(table, "week_tariffid")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Int id;
	Int partnerid;
	Decimal initial_cost;
	Decimal discount;
	Decimal total_cost;
	Int week_tariffid;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	PartnerOrder();
	explicit PartnerOrder(Int __id);
	PartnerOrder(const PartnerOrder& copy);
	PartnerOrder& operator=(const PartnerOrder& copy);
	virtual ~PartnerOrder() {}
	PartnerOrderDomain* operator->() { return &Domain_; }
	const PartnerOrderDomain* operator->() const { return &Domain_; }
	bool operator==(const PartnerOrder& other) const;
	bool operator<(const PartnerOrder& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL PartnerOrderlineArc : public Domain {
private:
	void pushColumns();
public:
	struct PartnerOrderlineArcDomain {
		Column orderid;
		Column identifierid;
		Column price;
		Column service_rulename;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		Column ArcType;
		PartnerOrderlineArcDomain( const SimpleTable& table )
		: orderid(table, "orderid")
		, identifierid(table, "identifierid")
		, price(table, "price")
		, service_rulename(table, "service_rulename")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		, ArcType(table, "ArcType")
		{}
	} Domain_;
	Int orderid;
	Int identifierid;
	Decimal price;
	Str service_rulename;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	Str ArcType;
	PartnerOrderlineArc();
	PartnerOrderlineArc(const PartnerOrderlineArc& copy);
	PartnerOrderlineArc& operator=(const PartnerOrderlineArc& copy);
	virtual ~PartnerOrderlineArc() {}
	PartnerOrderlineArcDomain* operator->() { return &Domain_; }
	const PartnerOrderlineArcDomain* operator->() const { return &Domain_; }
	bool operator==(const PartnerOrderlineArc& other) const;
	bool operator<(const PartnerOrderlineArc& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

class Baloon_DECL PartnerOrderline : public Domain {
private:
	void pushColumns();
public:
	struct PartnerOrderlineDomain {
		Column orderid;
		Column identifierid;
		Column price;
		Column service_rulename;
		Column UserArc;
		Column DateArc;
		Column TenantID;
		PartnerOrderlineDomain( const SimpleTable& table )
		: orderid(table, "orderid")
		, identifierid(table, "identifierid")
		, price(table, "price")
		, service_rulename(table, "service_rulename")
		, UserArc(table, "UserArc")
		, DateArc(table, "DateArc")
		, TenantID(table, "TenantID")
		{}
	} Domain_;
	Int orderid;
	Int identifierid;
	Decimal price;
	Str service_rulename;
	Int64 UserArc;
	Time DateArc;
	Int64 TenantID;
	PartnerOrderline();
	explicit PartnerOrderline(Int __orderid, Int __identifierid);
	PartnerOrderline(const PartnerOrderline& copy);
	PartnerOrderline& operator=(const PartnerOrderline& copy);
	virtual ~PartnerOrderline() {}
	PartnerOrderlineDomain* operator->() { return &Domain_; }
	const PartnerOrderlineDomain* operator->() const { return &Domain_; }
	bool operator==(const PartnerOrderline& other) const;
	bool operator<(const PartnerOrderline& other) const;
	DomainCacheIF* GetCache(DomainConnectionCache* dcc) const {
		if(dcc) return dcc->GetDomainCache(*this);
		return 0;
	}
	virtual bool IsTenant() {return true;};
};

} // namespace BALOON

#endif
